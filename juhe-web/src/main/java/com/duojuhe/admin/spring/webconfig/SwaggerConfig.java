package com.duojuhe.admin.spring.webconfig;


import com.duojuhe.common.utils.token.TokenUtils;
import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;



/**
 * Swagger文档配置
 *
 * @date 2018/2/9.
 */
@Profile("default")
@EnableSwagger2
@EnableKnife4j
@Component
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "swagger")
public class SwaggerConfig {
    /** 是否开启swagger */
    private boolean enabled;
   /** 接口说明 */
    private final static String description = "<p>当前所有接口请求Body都是json格式</p>" +
            "<p>接口返回结果中errorCode（0=成功，其他一律弹窗提示message参数内容）</p>" +
            "<p>建议：当errorCode小于0 时请弹出提示后，重新定位到登录界面</p>" +
            "温馨提示：当接口文档中标识有参数的时候，如果所有参数都不填写的时候，请在body中传空json体{}，否则@RequestBody注解报异常"+
           "<p>签名算法生成步骤如下：</p>" +
           "<p>把所有需要请求参数（排除空值参数或””空字符串参数的字段后），按参数名称ASCII码从小到大排序（字典序），使用URL键值对的格式（即key1=value1&key2=value2…）拼接成字符串fieldStr；</p>" +
           "<p>在fieldStr的前面连接上appId后面连接上 &appSecret得到signStr字符串，并对signStr字符串进行MD5运算，转成小写，得到签名sign值； 注意：如果请求参数中有数组参数，签名时需要注意，例如 {\"roleId\":\"1\",\"menuId\":[\"1\",\"2\"]}，那么拼接在sign中的字符串为：menuId=[1,2]&roleId=1；</p>" +
           "<p>注意： appId 和 appSecret 值是开放平台提供的； 签名加密（MD5）</p>" +
           "<p>示例【MD5结果转成小写】：MD5(\"appId&+key1=value1&key2=value2&keyArr=[x,x]&...&keyN=valueN+&appSecret\") </p>" +
           "<p>例如：</p>" +
           "<p>appId: 202205271038360001</p>" +
           "<p>appSecret: dc6f9e3a7f5e40ce99503f14956fd16b</p>" +
           "<p>请求body参数jsonStr为：</p>" +
           "<p>{\n" +
           "<p>  \"condition\": \"1\",\n" +
           "<p>  \"maxReturnNum\": \"10\",\n" +
           "<p>  \"pageIndex\": \"1\"\n" +
           "<p>}\n" +
           "<p>则最终拼接需要MD532加密的signStr字符串为：202205271038360001&condition=1&maxReturnNum=10&pageIndex=1&dc6f9e3a7f5e40ce99503f14956fd16b";

    /** 需要token接口统一参数构建 */
    private static List<Parameter> parameters = new ArrayList<>();
    static  {
        parameters.add(new ParameterBuilder()
                .name(TokenUtils.TOKEN_HEADER)
                .description("认证token，登录成功后获得的返回值")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .defaultValue("1")
                .required(true)
                .build());
    }

    @Bean(value = "basicApi")
    public Docket basicApi() {
        //分组名称
        String groupName = "01、无须登录公共接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.0"))
                //分组名称
                .groupName(groupName)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.basic"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean(value = "systemApi")
    public Docket systemApi() {
        //分组名称
        String groupName = "02、系统模块接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.1"))
                //分组名称
                .groupName(groupName)
                .globalOperationParameters(parameters)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.coremodule.system"))
                .paths(PathSelectors.any())
                .build();
    }


    @Bean(value = "formApi")
    public Docket formApi() {
        //分组名称
        String groupName = "03、问卷调查模块接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.0"))
                //分组名称
                .groupName(groupName)
                .globalOperationParameters(parameters)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.coremodule.form"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean(value = "chatApi")
    public Docket chatApi() {
        //分组名称
        String groupName = "04、多聊模块接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.0"))
                //分组名称
                .groupName(groupName)
                .globalOperationParameters(parameters)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.coremodule.chat"))
                .paths(PathSelectors.any())
                .build();
    }


    @Bean(value = "workflowApi")
    public Docket workflowApi() {
        //分组名称
        String groupName = "05、工作流模块接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.0"))
                //分组名称
                .groupName(groupName)
                .globalOperationParameters(parameters)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.coremodule.workflow"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean(value = "helpApi")
    public Docket helpApi() {
        //分组名称
        String groupName = "06、帮助中心模块接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.0"))
                //分组名称
                .groupName(groupName)
                .globalOperationParameters(parameters)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.coremodule.help"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean(value = "infoApi")
    public Docket infoApi() {
        //分组名称
        String groupName = "07、信息中心模块接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.0"))
                //分组名称
                .groupName(groupName)
                .globalOperationParameters(parameters)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.coremodule.info"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean(value = "noticeApi")
    public Docket noticeApi() {
        //分组名称
        String groupName = "08、通知公告模块接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.0"))
                //分组名称
                .groupName(groupName)
                .globalOperationParameters(parameters)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.coremodule.notice"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean(value = "quartzApi")
    public Docket quartzApi() {
        //分组名称
        String groupName = "09、定时调度任务模块接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.0"))
                //分组名称
                .groupName(groupName)
                .globalOperationParameters(parameters)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.coremodule.quartz"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean(value = "smsApi")
    public Docket smsApi() {
        //分组名称
        String groupName = "10、短信平台模块接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.0"))
                //分组名称
                .groupName(groupName)
                .globalOperationParameters(parameters)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.coremodule.sms"))
                .paths(PathSelectors.any())
                .build();
    }


    @Bean(value = "payApi")
    public Docket payApi() {
        //分组名称
        String groupName = "11、支付中心模块接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.0"))
                //分组名称
                .groupName(groupName)
                .globalOperationParameters(parameters)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.coremodule.pay"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean(value = "topoApi")
    public Docket topoApi() {
        //分组名称
        String groupName = "12、拓扑图模块接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.0"))
                //分组名称
                .groupName(groupName)
                .globalOperationParameters(parameters)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.coremodule.topo"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean(value = "trackingApi")
    public Docket trackingApi() {
        //分组名称
        String groupName = "13、产品溯源模块接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.0"))
                //分组名称
                .groupName(groupName)
                .globalOperationParameters(parameters)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.coremodule.tracking"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean(value = "monitorApi")
    public Docket monitorApi() {
        //分组名称
        String groupName = "14、系统监控模块接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.0"))
                //分组名称
                .groupName(groupName)
                .globalOperationParameters(parameters)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.coremodule.monitor"))
                .paths(PathSelectors.any())
                .build();
    }


    @Bean(value = "appApi")
    public Docket appApi() {
        //分组名称
        String groupName = "15、应用管理模块接口";
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo(groupName, "1.0.0"))
                //分组名称
                .groupName(groupName)
                .globalOperationParameters(parameters)
                .enable(enabled)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.duojuhe.coremodule.app"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo(String name,String version) {
        return new ApiInfoBuilder()
                .title(name)
                .version(version)
                .termsOfServiceUrl("http://api.duojuhe.com")
                .contact(new Contact("多聚合", "http://www.duojuhe.com", "13637050677@qq.com"))
                .description(description)
                .license("README.md")
                .licenseUrl("https://gitee.com/duojuhe/duojuheAdmin")
                .build();
    }

}
