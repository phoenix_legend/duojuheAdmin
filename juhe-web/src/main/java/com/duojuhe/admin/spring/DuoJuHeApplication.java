package com.duojuhe.admin.spring;

import com.duojuhe.common.utils.springcontext.SpringContextUtil;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.SessionCookieConfig;
import javax.servlet.SessionTrackingMode;
import java.util.Collections;

/**
 * 应用入口
 *
 * @date 2018/2/9.
 */
@Slf4j
//开启配置文件加密注解
@EnableEncryptableProperties
@EnableScheduling
@EnableTransactionManagement
@EnableCaching
@MapperScan({"com.duojuhe.**.mapper*"})
@ComponentScan(basePackages = "com.duojuhe*")
@SpringBootApplication
public class DuoJuHeApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(DuoJuHeApplication.class);
        ConfigurableApplicationContext context = application.run(args);
        // spring Context
        SpringContextUtil.setApplicationContext(context);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        // 设置启动类,用于独立tomcat运行的入口
        return builder.sources(DuoJuHeApplication.class);
    }

    @Override
    public void onStartup(ServletContext servletContext)throws ServletException {
        super.onStartup(servletContext);
        servletContext.setSessionTrackingModes(Collections.singleton(SessionTrackingMode.COOKIE));
        SessionCookieConfig sessionCookieConfig = servletContext.getSessionCookieConfig();
        sessionCookieConfig.setHttpOnly(true);
    }

    @PostConstruct
    public void init() {
        log.info("应用正在启动");
    }

    @PreDestroy
    public void destroy() {
        log.info("应用正在关闭");
    }
}