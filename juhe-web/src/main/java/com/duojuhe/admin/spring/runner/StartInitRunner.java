package com.duojuhe.admin.spring.runner;

import com.duojuhe.cache.SystemConfigCache;
import com.duojuhe.common.config.DuoJuHeConfig;
import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.common.utils.iputil.IPUtils;
import com.duojuhe.coremodule.system.entity.SystemConfig;
import com.duojuhe.coremodule.system.mapper.SystemConfigMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;


/**
 * 初始化用户数据
 *
 * @date 2018/5/25.
 */
@Component
@Order(value = 1)
@Slf4j
public class StartInitRunner implements CommandLineRunner {
    @Resource
    private ShutdownContext shutdownContext;
    @Resource
    private SystemConfigMapper systemConfigMapper;
    @Resource
    private DuoJuHeConfig duoJuHeConfig;
    @Resource
    private SystemConfigCache systemConfigCache;
    @Override
    public void run(String... strings) {
        try {
            //0、初始化api域名配置
            if (StringUtils.isBlank(duoJuHeConfig.getApiDomain())) {
                log.error("初始化数据失败，请检查api域名参数是否配置，具体详见：apiDomain这个节点!");
                //关闭项目
                shutdownContext.showdown();
                return;
            }
            //1、初始化IP请求头
            if (StringUtils.isBlank(duoJuHeConfig.getGetAdminIpHeader())) {
                log.error("初始化数据失败，请检查获取ip请求头参数是否配置，具体详见：getAdminIpHeader这个节点!");
                //关闭项目
                shutdownContext.showdown();
                return;
            }
            //2、初始化ip离线区域库
            IPUtils.initIp2regionXdbSearcher(duoJuHeConfig.getIp2regionXdb());
            //3、查询系统配置表
            List<SystemConfig> systemConfigList = systemConfigMapper.selectAll();
            if (systemConfigList.size()!=1){
                log.error("初始化数据失败，请检查系统配置表【SystemConfig】是否仅此只有一条记录!");
                //关闭项目
                shutdownContext.showdown();
                return;
            }
            //4、把系统配置数据初始到缓存中
            systemConfigCache.putSystemConfigCache(systemConfigList.get(0));
            //5、系统启动成功标识
            SystemConstants.SYSTEM_INIT_SUCCESS=true;
            log.info("\n////////////////////////////////////////////////////////////////////\n" +
                    "//                          _ooOoo_                               //\n" +
                    "//                         o8888888o                              //\n" +
                    "//                         88\" . \"88                              //\n" +
                    "//                         (| ^_^ |)                              //\n" +
                    "//                         O\\  =  /O                              //\n" +
                    "//                      ____/`---'\\____                           //\n" +
                    "//                    .'  \\\\|     |//  `.                         //\n" +
                    "//                   /  \\\\|||  :  |||//  \\                        //\n" +
                    "//                  /  _||||| -:- |||||-  \\                       //\n" +
                    "//                  |   | \\\\\\  -  /// |   |                       //\n" +
                    "//                  | \\_|  ''\\---/''  |   |                       //\n" +
                    "//                  \\  .-\\__  `-`  ___/-. /                       //\n" +
                    "//                ___`. .'  /--.--\\  `. . ___                     //\n" +
                    "//              .\"\" '<  `.___\\_<|>_/___.'  >'\"\".                  //\n" +
                    "//            | | :  `- \\`.;`\\ _ /`;.`/ - ` : | |                 //\n" +
                    "//            \\  \\ `-.   \\_ __\\ /__ _/   .-` /  /                 //\n" +
                    "//      ========`-.____`-.___\\_____/___.-`____.-'========         //\n" +
                    "//                           `=---='                              //\n" +
                    "//      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        //\n" +
                    "//             佛祖保佑       永不宕机      永无BUG               //\n" +
                    "///////////////////初始化数据成功，项目可正常使用////////////////////");
        } catch (Exception e) {
            //关闭项目
            shutdownContext.showdown();
            log.error("初始化数据失败", e);
        }
    }

}
