package com.duojuhe.admin.spring.schedule.task;


import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.common.utils.dateutils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.Date;

@Slf4j
@Component
public class SystemTask {
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.00");

    /**
     * 每30分钟执行一次
     */
    @Scheduled(cron = "0 */30 * * * ?")
    public void systemExecute() {
        if (!SystemConstants.SYSTEM_INIT_SUCCESS) {
            return;
        }
        //当前时间
        Date nowDate = new Date();
        //系统启动时间
        Date startDate = SystemConstants.SYSTEM_START_DATE;
        float running = (nowDate.getTime() - startDate.getTime()) / (float) (1000 * 60 * 60);
        log.info("系统已经运行：{} 小时, 启动时间：{}", DECIMAL_FORMAT.format(running), DateUtils.dateToString(startDate));
    }
}
