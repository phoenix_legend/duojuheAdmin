<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">多聚合Admin v1.0.1</h1>
<h4 align="center">SpringBoot+Vue前后端分离的Java快速开发框架</h4>

## 平台简介

多聚合是一套全部开源的Java快速开发平台，毫无保留给个人及企业免费使用。
我们集成了常见的功能模块。
* 前端采用Vue、Element UI。
* 后端采用Spring Boot、Shiro、Redis。
* 权限认证使用Shiro。
* 支持加载动态权限菜单，多方式轻松权限控制。

## 工作流和多聊前端部分已经开源（前端全量同步演示站，欢迎下载参考）


## 特色功能

1、可视化工作流，仿钉钉工作流。

2、自定义表单问卷调查，数据收集系统。

3、Vue网页版在线即时聊天。

4、网络拓扑图在线设计。

5、在线客服系统。

6、售后工单系统，可配合客服系统

## 在线体验

体验地址：http://demo.duojuhe.com  
 

## 注意说明

1、初始数据库：文件在doc目录中

2、与演示站区别：开源部分不含工作流和多聊模块

3、Vue前端项目：juhe-web-ui目录中（编译打包后如果需要修改正式环境地址可在public目录中config.js进行修改）

4、后端api启动：juhe-web目录中的 DuoJuHeApplication.java

5、接口文档地址：http://ip:port/doc.html  

6、工作流和多聊前端部分已经开源（前端全量同步演示站）

## 演示图
![输入图片说明](doc/img/1.png)
![输入图片说明](doc/img/2.png)
![输入图片说明](doc/img/3.png)
![输入图片说明](doc/img/4.png)
![输入图片说明](doc/img/5.png)
![输入图片说明](doc/img/6.png)
![输入图片说明](doc/img/7.png)
![输入图片说明](doc/img/8.png)
