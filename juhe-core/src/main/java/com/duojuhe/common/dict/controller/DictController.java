package com.duojuhe.common.dict.controller;


import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.dict.SelectChildrenDictRes;
import com.duojuhe.coremodule.system.pojo.dto.dict.SystemDictCodeListReq;
import com.duojuhe.coremodule.system.service.SystemDictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = ApiPathConstants.COMMON_PATH + "/dict/")
@Api(tags = {"获取字典信息"})
@Slf4j
public class DictController {
    @Resource
    private SystemDictService systemDictService;

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【根据code集合获取数据字典】根据数据字典父级code编码获取数据字典")
    @PostMapping(value = "queryCommonDictListByParentDictCodeList")
    public ServiceResult<Map<String,List<SelectChildrenDictRes>>> queryCommonDictListByParentDictCodeList(@Valid @RequestBody @ApiParam(value = "入参类") SystemDictCodeListReq req) {
        return systemDictService.queryCommonDictListByParentDictCodeList(req);
    }
}
