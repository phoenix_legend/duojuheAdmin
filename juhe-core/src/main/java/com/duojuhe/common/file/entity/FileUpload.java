package com.duojuhe.common.file.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("文件附件管理")
@Table(name = "file_upload")
public class FileUpload extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "file_id")
    @Id
    private String fileId;

    @ApiModelProperty(value = "文件类型，取数据字典")
    @Column(name = "file_type_code")
    private String fileTypeCode;

    @ApiModelProperty(value = "文件名称")
    @Column(name = "file_name")
    private String fileName;

    @ApiModelProperty(value = "文件地址")
    @Column(name = "file_url")
    private String fileUrl;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "关联归属id")
    @Column(name = "relation_id")
    private String relationId;

    @ApiModelProperty(value = "关联id来源表")
    @Column(name = "relation_id_source")
    private String relationIdSource;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "文件后缀名")
    @Column(name = "file_suffix")
    private String fileSuffix;

    @ApiModelProperty(value = "原文件名")
    @Column(name = "original_name")
    private String originalName;

    @ApiModelProperty(value = "文件大小")
    @Column(name = "file_size")
    private Long fileSize;

    @ApiModelProperty(value = "文件分类id")
    @Column(name = "file_category_id")
    private String fileCategoryId;

    @ApiModelProperty(value = "文件相对路径")
    @Column(name = "file_relative_path")
    private String fileRelativePath;

    @ApiModelProperty(value = "文件存储路径")
    @Column(name = "file_storage_path")
    private String fileStoragePath;

}