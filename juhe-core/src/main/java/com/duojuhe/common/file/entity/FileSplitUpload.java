package com.duojuhe.common.file.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("系统文件分割上传分片表")
@Table(name = "file_split_upload")
public class FileSplitUpload extends BaseBean {
    @ApiModelProperty(value = "主键id", required = true)
    @Column(name = "upload_id")
    @Id
    private String uploadId;

    @ApiModelProperty(value = "-1表示初始大文件")
    @Column(name = "parent_id")
    private String parentId;

    @ApiModelProperty(value = "临时文件hash名")
    @Column(name = "hash_name")
    private String hashName;

    @ApiModelProperty(value = "上传用户id")
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty(value = "创建用户id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "原文件名")
    @Column(name = "original_name")
    private String originalName;

    @ApiModelProperty(value = "当前索引块")
    @Column(name = "split_index")
    private Integer splitIndex;

    @ApiModelProperty(value = "总共索引块")
    @Column(name = "split_num")
    private Integer splitNum;

    @ApiModelProperty(value = "文件后缀名")
    @Column(name = "file_suffix")
    private String fileSuffix;

    @ApiModelProperty(value = "文件大小")
    @Column(name = "file_size")
    private Long fileSize;


    @ApiModelProperty(value = "文件分割大小规则")
    @Column(name = "split_size")
    private Double splitSize;


    @ApiModelProperty(value = "创建毫秒时间")
    @Column(name = "create_milli_second")
    private Long createMilliSecond;

    @ApiModelProperty(value = "文件内容")
    @Column(name = "file_content")
    private byte[] fileContent;
}