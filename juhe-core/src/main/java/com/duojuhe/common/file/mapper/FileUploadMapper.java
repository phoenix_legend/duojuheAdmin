package com.duojuhe.common.file.mapper;


import com.duojuhe.common.file.entity.FileUpload;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

public interface FileUploadMapper extends TkMapper<FileUpload> {

}