package com.duojuhe.common.file.service;

import com.duojuhe.common.bean.UserTokenInfoVo;
import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.common.exception.base.DuoJuHeException;
import com.duojuhe.common.file.entity.FileUpload;
import com.duojuhe.common.file.mapper.FileUploadMapper;
import com.duojuhe.common.result.ErrorCodes;
import com.duojuhe.common.utils.file.UploadFileBean;
import com.duojuhe.common.utils.idgenerator.UUIDUtils;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
public class FileUploadHandler {
    @Resource
    private FileUploadMapper fileUploadMapper;

    /**
     * 新增一个上传文件
     * @param uploadFileBean 文件上传对象
     * @param userTokenInfoVo 当前登录人
     */
    public void addNewFileUpload(UploadFileBean uploadFileBean, UserTokenInfoVo userTokenInfoVo){
        fileUploadMapper.insertSelective(buildFileUpload(uploadFileBean,userTokenInfoVo));
    }



    /**
     * 新增一个上传文件
     * @param relationId 关联id
     * @param relationIdSource 关联id来源，一般是表名
     * @param uploadFileBean 文件上传对象
     * @param userTokenInfoVo 当前登录人
     */
    public void addRelationNewFileUpload(String relationId,String relationIdSource,UploadFileBean uploadFileBean, UserTokenInfoVo userTokenInfoVo){
        //文件对象
        FileUpload fileUpload = buildFileUpload(uploadFileBean,userTokenInfoVo);
        fileUpload.setRelationId(relationId);
        fileUpload.setRelationIdSource(relationIdSource);
        fileUploadMapper.insertSelective(fileUpload);
    }

    /**
     * 新增文件上传
     * @param fileCategoryId 所属分类id
     * @param relationId 关联id
     * @param relationIdSource 关联id来源，一般是表名
     * @param uploadFileBean 文件上传对象
     * @param userTokenInfoVo 当前登录人
     */
    public void addCategoryRelationNewFileUpload(String fileCategoryId,String relationId,String relationIdSource,UploadFileBean uploadFileBean, UserTokenInfoVo userTokenInfoVo){
        //文件对象
        FileUpload fileUpload = buildFileUpload(uploadFileBean,userTokenInfoVo);
        fileUpload.setRelationId(relationId);
        fileUpload.setRelationIdSource(relationIdSource);
        fileUpload.setFileCategoryId(fileCategoryId);
        fileUploadMapper.insertSelective(fileUpload);
    }

    /**
     * 新增一个带分类的文件上传
     * @param fileCategoryId 所属分类id
     * @param uploadFileBean 文件上传对象
     * @param userTokenInfoVo 当前登录人
     */
    public void addCategoryNewFileUpload(String fileCategoryId,UploadFileBean uploadFileBean, UserTokenInfoVo userTokenInfoVo){
        //文件对象
        FileUpload fileUpload = buildFileUpload(uploadFileBean,userTokenInfoVo);
        fileUpload.setFileCategoryId(fileCategoryId);
        fileUploadMapper.insertSelective(fileUpload);
    }

    /**
     * 构建上传文件
     * @param uploadFileBean
     * @param userTokenInfoVo
     * @return
     */
    private FileUpload buildFileUpload(UploadFileBean uploadFileBean, UserTokenInfoVo userTokenInfoVo){
        //当前时间
        Date nowDate = new Date();
        //原始文件名
        String originalName = uploadFileBean.getOriginalName();
        if (StringUtils.isBlank(originalName)||originalName.length()>255) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_NAME_LENGTH_ERROR);
        }
        //文件对象
        FileUpload fileUpload = new FileUpload();
        fileUpload.setFileId(UUIDUtils.getUUID32());
        fileUpload.setFileTypeCode(uploadFileBean.getFileTypeCode());
        fileUpload.setFileUrl(uploadFileBean.getFileAbsolutePath());
        fileUpload.setFileSize(uploadFileBean.getFileSize());
        fileUpload.setFileName(uploadFileBean.getNewFileName());
        fileUpload.setOriginalName(originalName);
        fileUpload.setFileRelativePath(uploadFileBean.getFileRelativePath());
        fileUpload.setFileSuffix(uploadFileBean.getFileSuffix());
        fileUpload.setCreateTime(nowDate);
        fileUpload.setCreateUserId(userTokenInfoVo.getUserId());
        fileUpload.setCreateDeptId(userTokenInfoVo.getCreateDeptId());
        fileUpload.setTenantId(userTokenInfoVo.getTenantId());
        fileUpload.setFileCategoryId(SystemConstants.UNKNOWN_ID);
        fileUpload.setFileStoragePath(uploadFileBean.getFileStoragePath());
        return fileUpload;
    }
}
