package com.duojuhe.common.file.service;

import com.duojuhe.common.file.dto.*;
import com.duojuhe.common.result.ServiceResult;
import org.springframework.web.multipart.MultipartFile;


public interface FileUploadService {
    /**
     * 单图片上传
     * @param file
     * @return
     */
    ServiceResult<SingleFileUploadRes> imageSingleUpload(MultipartFile file);


    /**
     * 单图片上传返回文件绝对路径
     * @param file
     * @return
     */
    ServiceResult<String> imageSingleUploadToFileAbsolutePath(MultipartFile file);



    /**
     * 单图片上传返回base64图片格式
     * @param file
     * @return
     */
    ServiceResult<String> imageSingleUploadToImageBase64(MultipartFile file);

    /**
     * 单文件上传
     * @param file
     * @return
     */
    ServiceResult<SingleFileUploadRes> fileSingleUpload(MultipartFile file);

    /**
     * 单文件支付证书上传返回Sm4加密文件存储路径加密值
     * @param file
     * @return
     */
    ServiceResult<SingleFileUploadRes> fileSinglePayCertUploadSm4EncryptionPath(MultipartFile file);


    /**
     * 单文件上传返回并且支持加密
     * @param file
     * @param req
     * @return
     */
    ServiceResult<SingleFileUploadRes> fileSingleEncryptionUpload(MultipartFile file,UploadFileEncryptionModeReq req);

    /**
     * 单文件上传返回文件绝对路径
     * @param file
     * @return
     */
    ServiceResult<String> fileSingleUploadToFileAbsolutePath(MultipartFile file);

    /**
     * 获取大文件上传拆分信息
     * @param req
     * @return
     */
    ServiceResult<BigFileSplitUploadRes> getBigFileUploadSplitInfo(GetBigFileUploadSplitInfoReq req) throws Exception;


    /**
     * 大文件分片上传
     * @param req
     * @return
     */
    ServiceResult<BigFileSliceUploadRes> bigFileSliceUpload(MultipartFile file, BigFileSliceUploadReq req) throws Exception;


    /**
     * 根据文件id下载文件
     * @param req
     * @return
     */
    ServiceResult<String> downloadUploadFileByFileId(UploadFileIdReq req);
}
