package com.duojuhe.common.file.controller;

import com.duojuhe.common.annotation.RepeatSubmit;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.file.dto.*;
import com.duojuhe.common.file.service.FileUploadService;
import com.duojuhe.common.result.ServiceResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;


@Controller
@RequestMapping(value = ApiPathConstants.COMMON_PATH + "/upload/")
@Api(tags = {"图片文件上传相关接口"})
@Slf4j
public class FileUploadController {
    @Resource
    private FileUploadService fileUploadService;

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "单图片上传，返回文件名称和文件绝对路径等信息")
    @PostMapping(value = "/imageSingleUpload")
    @ResponseBody
    public ServiceResult<SingleFileUploadRes> imageSingleUpload(@RequestParam("file") MultipartFile file) {
        return fileUploadService.imageSingleUpload(file);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "单图片上传返回文件绝对路径")
    @PostMapping(value = "/imageSingleUploadToFileAbsolutePath")
    @ResponseBody
    public ServiceResult<String> imageSingleUploadToFileAbsolutePath(@RequestParam("file") MultipartFile file) {
        return fileUploadService.imageSingleUploadToFileAbsolutePath(file);
    }

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "单图片上传返回base64图片格式")
    @PostMapping(value = "/imageSingleUploadToImageBase64")
    @ResponseBody
    public ServiceResult<String> imageSingleUploadToImageBase64(@RequestParam("file") MultipartFile file) {
        return fileUploadService.imageSingleUploadToImageBase64(file);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "单文件上传")
    @PostMapping(value = "/fileSingleUpload")
    @ResponseBody
    public ServiceResult<SingleFileUploadRes> fileSingleUpload(@RequestParam("file") MultipartFile file) {
        return fileUploadService.fileSingleUpload(file);
    }



    @SuppressWarnings("unchecked")
    @ApiOperation(value = "单文件支付证书上传返回Sm4加密文件存储路径加密值")
    @PostMapping(value = "/fileSinglePayCertUploadSm4EncryptionPath")
    @ResponseBody
    public ServiceResult<SingleFileUploadRes> fileSinglePayCertUploadSm4EncryptionPath(@RequestParam("file") MultipartFile file) {
        return fileUploadService.fileSinglePayCertUploadSm4EncryptionPath(file);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "单文件上传返回并且支持加密")
    @PostMapping(value = "/fileSingleEncryptionUpload")
    @ResponseBody
    public ServiceResult<SingleFileUploadRes> fileSingleEncryptionUpload(@RequestParam("file") MultipartFile file, @Valid @ApiParam(value = "入参类") UploadFileEncryptionModeReq req) {
        return fileUploadService.fileSingleEncryptionUpload(file,req);
    }


    @SuppressWarnings("unchecked")
    @RepeatSubmit
    @ApiOperation(value = "获取大文件上传拆分信息")
    @PostMapping(value = "/getBigFileUploadSplitInfo")
    @ResponseBody
    public ServiceResult<BigFileSplitUploadRes> getBigFileUploadSplitInfo(@Valid @RequestBody @ApiParam(value = "入参类")GetBigFileUploadSplitInfoReq req) throws Exception {
        return fileUploadService.getBigFileUploadSplitInfo(req);
    }

    @ApiOperation(value = "【大文件分片上传】")
    @PostMapping(value = "bigFileSliceUpload")
    @ResponseBody
    public ServiceResult<BigFileSliceUploadRes> bigFileSliceUpload(@RequestParam("file") MultipartFile file, @Valid @ApiParam(value = "入参类") BigFileSliceUploadReq req) throws Exception{
        return fileUploadService.bigFileSliceUpload(file,req);
    }

    @ApiOperation(value = "【根据文件id下载文件】")
    @PostMapping(value = "downloadUploadFileByFileId")
    public ServiceResult<String> downloadUploadFileByFileId(@Valid @ApiParam(value = "入参类") UploadFileIdReq req){
        return fileUploadService.downloadUploadFileByFileId(req);
    }
}
