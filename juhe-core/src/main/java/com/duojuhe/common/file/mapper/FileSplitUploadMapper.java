package com.duojuhe.common.file.mapper;

import com.duojuhe.common.file.entity.FileSplitUpload;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface FileSplitUploadMapper extends TkMapper<FileSplitUpload> {
    /**
     * 根据问文件父级id查询分片信息
     * @param  parentId
     * @return
     */
    List<FileSplitUpload> queryFileSplitUploadListByParentId(@Param("parentId") String parentId);

}