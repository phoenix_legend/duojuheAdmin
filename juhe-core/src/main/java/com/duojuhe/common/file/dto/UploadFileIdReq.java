package com.duojuhe.common.file.dto;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UploadFileIdReq extends BaseBean {
    @ApiModelProperty(value = "文件id", example = "1",required=true)
    @NotBlank(message = "文件ID不能为空")
    private String fileId;
}
