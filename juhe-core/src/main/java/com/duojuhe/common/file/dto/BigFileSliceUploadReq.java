package com.duojuhe.common.file.dto;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BigFileSliceUploadReq extends BaseBean {
    @ApiModelProperty(value = "文件系统名称", example = "3",required=true)
    @NotBlank(message = "文件系统名称不能为空")
    @Length(max = 255, message = "文件系统名称不得超过{max}位字符")
    private String hashName;

    @ApiModelProperty(value = "文件拆分总数量", example = "1",required=true)
    @NotNull(message = "文件拆分总数量不能为空")
    @Range(min = 1, max = Integer.MAX_VALUE, message = "文件拆分总数量参数不正确")
    private Integer splitNum;

    @ApiModelProperty(value = "文件拆分分片当前索引块，下标从0开始", example = "1",required=true)
    @NotNull(message = "文件拆分分片当前索引块不能为空")
    @Range(min = 0, max = Integer.MAX_VALUE, message = "文件拆分分片当前索引块参数不正确")
    private Integer splitIndex;

    @ApiModelProperty(value = "文件名称", example = "3",required=true)
    @NotBlank(message = "文件名称不能为空")
    @Length(max = 255, message = "文件名称不得超过{max}位字符")
    private String fileName;

    @ApiModelProperty(value = "文件大小", example = "1",required=true)
    @NotNull(message = "文件大小参数不正确")
    @Range(min = 0, max = Integer.MAX_VALUE, message = "文件大小参数不正确")
    private Long fileSize;
}
