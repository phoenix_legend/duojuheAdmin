package com.duojuhe.common.file.dto;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BigFileSplitUploadRes extends BaseBean {
    @ApiModelProperty(value = "文件系统命名的名称", example = "3",required=true)
    private String hashName;

    @ApiModelProperty(value = "系统分割大小", example = "1",required=true)
    private Double splitSize;

    @ApiModelProperty(value = "文件大小", example = "1",required=true)
    private Integer fileSize;

    @ApiModelProperty(value = "用户ID", example = "1",required=true)
    private String userId;

    @ApiModelProperty(value = "原文件名", example = "1",required=true)
    private String originalName;

    @ApiModelProperty(value = "文件拆分数量", example = "1",required=true)
    private Integer splitNum;

    @ApiModelProperty(value = "文件拆分", example = "1",required=true)
    private Integer splitIndex;

    @ApiModelProperty(value = "数据类型[1:合并文件;2:拆分文件]", example = "1",required=true)
    private Integer fileType;


}
