package com.duojuhe.common.file.dto;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BigFileSliceUploadRes extends BaseBean {
    @ApiModelProperty(value = "是否上传完成", example = "3",required=true)
    private Boolean isFileMerge;

    @ApiModelProperty(value = "临时文件hash名")
    private String hashName;
}
