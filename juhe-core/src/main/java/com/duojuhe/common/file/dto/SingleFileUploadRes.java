package com.duojuhe.common.file.dto;

import com.duojuhe.common.bean.BaseBean;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SingleFileUploadRes extends BaseBean {
    @ApiModelProperty(value = "文件名称", example = "1")
    private String fileName ;

    @ApiModelProperty(value = "文件绝对路径", example = "1")
    private String  fileAbsolutePath;

    @ApiModelProperty(value = "文件磁盘存储路径", example = "1")
    private String fileStoragePath;

    public SingleFileUploadRes(String fileName,String fileAbsolutePath){
        this.fileName=fileName;
        this.fileAbsolutePath=fileAbsolutePath;
    }

    public SingleFileUploadRes(String fileName, String fileAbsolutePath, String fileStoragePath){
        this.fileName=fileName;
        this.fileAbsolutePath=fileAbsolutePath;
        this.fileStoragePath=fileStoragePath;
    }
}
