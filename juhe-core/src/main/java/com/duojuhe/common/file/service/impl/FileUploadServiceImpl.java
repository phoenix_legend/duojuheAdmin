package com.duojuhe.common.file.service.impl;


import com.duojuhe.common.bean.UserTokenInfoVo;
import com.duojuhe.common.config.FileUploadConfig;
import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.common.exception.base.DuoJuHeException;
import com.duojuhe.common.file.dto.*;
import com.duojuhe.common.file.entity.FileSplitUpload;
import com.duojuhe.common.file.mapper.FileSplitUploadMapper;
import com.duojuhe.common.file.service.FileUploadHandler;
import com.duojuhe.common.file.service.FileUploadService;
import com.duojuhe.common.result.ErrorCodes;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.encryption.md5.MD5Util;
import com.duojuhe.common.utils.encryption.sm4.Sm4Util;
import com.duojuhe.common.utils.file.UploadFileBean;
import com.duojuhe.common.utils.file.UploadFileUtil;
import com.duojuhe.common.utils.idgenerator.UUIDUtils;
import com.duojuhe.coremodule.BaseService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.ContentType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Date;


@Service
@Slf4j
public class FileUploadServiceImpl extends BaseService implements FileUploadService {
    /**
     * 文件拆分的大小
     */
    private final static Double SPLIT_SIZE = 1024D * 1024;
    @Resource
    private FileUploadConfig fileUploadConfig;
    @Resource
    private FileUploadHandler fileUploadHandler;
    @Resource
    private FileSplitUploadMapper fileSplitUploadMapper;
    /**
     * 单图片上传
     * @param file
     * @return
     */
    @Override
    public ServiceResult<SingleFileUploadRes> imageSingleUpload(MultipartFile file) {
        if (file==null||file.isEmpty()) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_NOT_FOUND);
        }
        // 判断文件大小
        long fileSize = file.getSize();
        long maxImageSize = fileUploadConfig.getMaxSize(fileUploadConfig.getMaxImageSize());
        if (fileSize > maxImageSize) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_TOO_LARGE);
        }
        //允许图片文件类型
        String[] allowFiles = fileUploadConfig.getImageAllowFiles();
        //当前用户
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //用户目录
        String userDir = userTokenInfoVo.getTenantNumber()+File.separator+userTokenInfoVo.getUserNumber();
        //用户文件目录dir
        String fileDir = File.separator+"images"+File.separator+userDir;
        //文件根目录
        String rootPath = fileUploadConfig.getRootPath()+fileDir;
        //文件前缀
        String imageUrlPrefix = fileUploadConfig.getFileUrlPrefix()+fileDir;
        //附件上传前缀域名
        String filePrefixDomain = fileUploadConfig.getFilePrefixDomain();
        UploadFileBean uploadFileBean = UploadFileUtil.writeSingleImageToDisk(file,allowFiles,filePrefixDomain,rootPath, imageUrlPrefix);
        //请求路径
        String fileAbsolutePath = uploadFileBean.getFileAbsolutePath();
        if (StringUtils.isEmpty(fileAbsolutePath)) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_FORMAT_ERROR);
        }
        //保存文件到文件库
        fileUploadHandler.addNewFileUpload(uploadFileBean,userTokenInfoVo);
        //文件名称
        String filename = file.getOriginalFilename();
        return ServiceResult.ok(new SingleFileUploadRes(filename,fileAbsolutePath));
    }

    /**
     * 单图片上传返回文件绝对路径
     * @param file
     * @return
     */
    @Override
    public ServiceResult<String> imageSingleUploadToFileAbsolutePath(MultipartFile file) {
        if (file==null||file.isEmpty()) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_NOT_FOUND);
        }
        // 判断文件大小
        long fileSize = file.getSize();
        long maxImageSize = fileUploadConfig.getMaxSize(fileUploadConfig.getMaxImageSize());
        if (fileSize > maxImageSize) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_TOO_LARGE);
        }
        //允许图片文件类型
        String[] allowFiles = fileUploadConfig.getImageAllowFiles();
        //当前用户
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //用户目录
        String userDir = userTokenInfoVo.getTenantNumber()+File.separator+userTokenInfoVo.getUserNumber();
        //用户文件目录dir
        String fileDir = File.separator+"images"+File.separator+userDir;
        //文件根目录
        String rootPath = fileUploadConfig.getRootPath()+fileDir;
        //文件前缀
        String imageUrlPrefix = fileUploadConfig.getFileUrlPrefix()+fileDir;
        //附件上传前缀域名
        String filePrefixDomain = fileUploadConfig.getFilePrefixDomain();
        UploadFileBean uploadFileBean = UploadFileUtil.writeSingleImageToDisk(file,allowFiles,filePrefixDomain,rootPath, imageUrlPrefix);
        //请求路径
        String fileAbsolutePath = uploadFileBean.getFileAbsolutePath();
        if (StringUtils.isEmpty(fileAbsolutePath)) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_FORMAT_ERROR);
        }
        //保存文件到文件库
        fileUploadHandler.addNewFileUpload(uploadFileBean,userTokenInfoVo);
        return ServiceResult.ok(fileAbsolutePath);
    }

    /**
     * 单图片上传返回base64图片格式
     * @param file
     * @return
     */
    @Override
    public ServiceResult<String> imageSingleUploadToImageBase64(MultipartFile file) {
        if (file==null||file.isEmpty()){
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_NOT_FOUND);
        }
        // 判断文件大小
        long fileSize = file.getSize();
        long maxImageSize = fileUploadConfig.getMaxSize(fileUploadConfig.getMaxImageSize());
        if (fileSize > maxImageSize) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_TOO_LARGE);
        }
        return ServiceResult.ok(UploadFileUtil.writeSingleImageToImageBase64(file));
    }

    /**
     * 单文件上传
     * @param file
     * @return
     */
    @Override
    public ServiceResult<SingleFileUploadRes> fileSingleUpload(MultipartFile file) {
        if (file==null||file.isEmpty()) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_NOT_FOUND);
        }
        // 判断文件大小
        long fileSize = file.getSize();
        long maxFileSize = fileUploadConfig.getMaxSize(fileUploadConfig.getMaxFileSize());
        if (fileSize > maxFileSize) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_TOO_LARGE);
        }
        //当前用户
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //用户目录
        String userDir = userTokenInfoVo.getTenantNumber()+File.separator+userTokenInfoVo.getUserNumber();
        //用户文件目录dir
        String fileDir = File.separator+"files"+File.separator+userDir;
        //文件根目录
        String rootPath = fileUploadConfig.getRootPath()+fileDir;
        //文件前缀
        String fileUrlPrefix = fileUploadConfig.getFileUrlPrefix()+fileDir;
        //附件上传前缀域名
        String filePrefixDomain = fileUploadConfig.getFilePrefixDomain();
        //允许文件类型
        String[] allowFiles = fileUploadConfig.getFileAllowFiles();
        //构建文件上传对象
        UploadFileBean uploadFileBean = UploadFileUtil.writeSingleFileToDisk(file,allowFiles,filePrefixDomain,rootPath, fileUrlPrefix);
        //请求路径
        String fileUrl = uploadFileBean.getFileAbsolutePath();
        if (StringUtils.isEmpty(fileUrl)) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_FORMAT_ERROR);
        }
        //保存文件到文件库
        fileUploadHandler.addNewFileUpload(uploadFileBean,userTokenInfoVo);
        //文件名称
        String filename = file.getOriginalFilename();
        return ServiceResult.ok(new SingleFileUploadRes(filename,fileUrl));
    }

    /**
     * 单文件支付证书上传返回Sm4加密文件存储路径加密值
     * @param file
     * @return
     */
    @Override
    public ServiceResult<SingleFileUploadRes> fileSinglePayCertUploadSm4EncryptionPath(MultipartFile file) {
        if (file==null||file.isEmpty()) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_NOT_FOUND);
        }
        // 判断文件大小
        long fileSize = file.getSize();
        long maxPayCertFileSize = fileUploadConfig.getMaxSize(fileUploadConfig.getMaxPayCertFileSize());
        if (fileSize > maxPayCertFileSize) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_TOO_LARGE);
        }
        //当前用户
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //用户目录
        String userDir = userTokenInfoVo.getTenantNumber()+File.separator+userTokenInfoVo.getUserNumber();
        //用户文件目录dir
        String fileDir = File.separator+"payCertFiles"+File.separator+userDir;
        //文件根目录
        String rootPath = fileUploadConfig.getRootPath()+fileDir;
        //文件前缀
        String fileUrlPrefix = fileUploadConfig.getFileUrlPrefix()+fileDir;
        //附件上传前缀域名
        String filePrefixDomain = fileUploadConfig.getFilePrefixDomain();
        //国密加密秘钥
        String sm4Key = userTokenInfoVo.getSm4Key();
        //允许支付证书文件类型
        String[] allowPayCertFiles = fileUploadConfig.getFilePayCertAllowFiles();
        //开始上传
        UploadFileBean uploadFileBean = UploadFileUtil.writeSingleFileToDisk(file,allowPayCertFiles,filePrefixDomain,rootPath, fileUrlPrefix);
        //请求路径
        String fileUrl = uploadFileBean.getFileAbsolutePath();
        //文件存储路径
        String fileStoragePath = uploadFileBean.getFileStoragePath();
        if (StringUtils.isEmpty(fileUrl)||StringUtils.isEmpty(fileStoragePath)) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_FORMAT_ERROR);
        }
        //保存文件到文件库
        fileUploadHandler.addNewFileUpload(uploadFileBean,userTokenInfoVo);
        //文件名称
        String filename = file.getOriginalFilename();
        return ServiceResult.ok(new SingleFileUploadRes(filename,null, Sm4Util.encryptCbc(sm4Key,fileStoragePath)));
    }

    /**
     * 单文件上传返回并且支持加密，加密文件加密文件
     * @param file
     * @param req
     * @return
     */
    @Override
    public ServiceResult<SingleFileUploadRes> fileSingleEncryptionUpload(MultipartFile file, UploadFileEncryptionModeReq req) {
        if (file==null||file.isEmpty()) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_NOT_FOUND);
        }
        // 判断文件大小
        long fileSize = file.getSize();
        long maxFileSize = fileUploadConfig.getMaxSize(fileUploadConfig.getMaxFileSize());
        if (fileSize > maxFileSize) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_TOO_LARGE);
        }
        //当前用户
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //用户目录
        String userDir = userTokenInfoVo.getTenantNumber()+File.separator+userTokenInfoVo.getUserNumber();
        //用户文件目录dir
        String fileDir = File.separator+"encryptionFiles"+File.separator+userDir;
        //文件根目录
        String rootPath = fileUploadConfig.getRootPath()+fileDir;
        //文件前缀
        String fileUrlPrefix = fileUploadConfig.getFileUrlPrefix()+fileDir;
        //附件上传前缀域名
        String filePrefixDomain = fileUploadConfig.getFilePrefixDomain();
        //国密加密秘钥
        String sm4Key = userTokenInfoVo.getSm4Key();
        //开始上传
        UploadFileBean uploadFileBean = UploadFileUtil.writeSm4SingleFileToDisk(file,filePrefixDomain,rootPath, fileUrlPrefix,sm4Key);
        //请求路径
        String fileUrl = uploadFileBean.getFileAbsolutePath();
        //文件存储路径
        String fileStoragePath = uploadFileBean.getFileStoragePath();
        if (StringUtils.isEmpty(fileUrl)||StringUtils.isEmpty(fileStoragePath)) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_FORMAT_ERROR);
        }
        //保存文件到文件库
        fileUploadHandler.addNewFileUpload(uploadFileBean,userTokenInfoVo);
        //文件名称
        String filename = file.getOriginalFilename();
        return ServiceResult.ok(new SingleFileUploadRes(filename,fileUrl,fileStoragePath));
    }


    /**
     * 单文件上传返回文件绝对路径
     * @param file
     * @return
     */
    @Override
    public ServiceResult<String> fileSingleUploadToFileAbsolutePath(MultipartFile file) {
        if (file==null||file.isEmpty()) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_NOT_FOUND);
        }
        // 判断文件大小
        long fileSize = file.getSize();
        long maxFileSize = fileUploadConfig.getMaxSize(fileUploadConfig.getMaxFileSize());
        if (fileSize > maxFileSize) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_TOO_LARGE);
        }
        //当前用户
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //用户目录
        String userDir = userTokenInfoVo.getTenantNumber()+File.separator+userTokenInfoVo.getUserNumber();
        //用户文件目录dir
        String fileDir = File.separator+"files"+File.separator+userDir;
        //文件根目录
        String rootPath = fileUploadConfig.getRootPath()+fileDir;
        //文件前缀
        String fileUrlPrefix = fileUploadConfig.getFileUrlPrefix()+fileDir;
        //附件上传前缀域名
        String filePrefixDomain = fileUploadConfig.getFilePrefixDomain();
        //允许文件类型
        String[] allowFiles = fileUploadConfig.getFileAllowFiles();
        //开始上传
        UploadFileBean uploadFileBean = UploadFileUtil.writeSingleFileToDisk(file,allowFiles,filePrefixDomain,rootPath, fileUrlPrefix);
        //请求路径
        String fileUrl = uploadFileBean.getFileAbsolutePath();
        if (StringUtils.isEmpty(fileUrl)) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_FORMAT_ERROR);
        }
        //保存文件到文件库
        fileUploadHandler.addNewFileUpload(uploadFileBean,userTokenInfoVo);
        return ServiceResult.ok(fileUrl);
    }

    /**
     * 获取大文件上传拆分信息
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServiceResult<BigFileSplitUploadRes> getBigFileUploadSplitInfo(GetBigFileUploadSplitInfoReq req) throws Exception {
        // 判断文件大小
        long fileSize = req.getFileSize();
        if (fileSize<=0){
            return ServiceResult.fail (ErrorCodes.PARAM_ERROR);
        }
        long maxSize = fileUploadConfig.getMaxSize(fileUploadConfig.getMaxFileSize());
        if (fileSize > maxSize) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_TOO_LARGE);
        }
        String fileSuffix = UploadFileUtil.getFileSuffixByFileName(req.getFileName());
        //当前用户id
        String userId = getCurrentLoginUserId();
        //系统文件名
        String hashName = UUIDUtils.getUUID32();
        //系统拆分
        Integer splitNum = (int) Math.ceil(req.getFileSize() / SPLIT_SIZE);
        //当前时间
        Date nowDate = new Date();
        FileSplitUpload splitUpload = new FileSplitUpload();
        splitUpload.setUploadId(hashName);
        splitUpload.setParentId(SystemConstants.UNKNOWN_ID);
        splitUpload.setHashName(hashName);
        splitUpload.setUserId(userId);
        splitUpload.setCreateUserId(userId);
        splitUpload.setCreateTime(nowDate);
        splitUpload.setOriginalName(req.getFileName());
        splitUpload.setSplitIndex(0);
        splitUpload.setSplitNum(splitNum);
        splitUpload.setFileSize(req.getFileSize());
        splitUpload.setSplitSize(SPLIT_SIZE);
        splitUpload.setCreateMilliSecond(nowDate.getTime());
        splitUpload.setFileSuffix(fileSuffix);
        fileSplitUploadMapper.insertSelective(splitUpload);
        //代码片段对象
        BigFileSplitUploadRes res = new BigFileSplitUploadRes();
        //拷贝对象到新对象中
        BeanUtils.copyProperties(res,splitUpload);
        return ServiceResult.ok(res);
    }

    /**
     * 大文件分片上传
     * @param req
     * @return
     */
    @Override
    public ServiceResult<BigFileSliceUploadRes> bigFileSliceUpload(MultipartFile file, BigFileSliceUploadReq req) throws Exception {
        // 判断文件大小
        long fileSize = req.getFileSize();
        if (fileSize<=0){
            return ServiceResult.fail ("分片文件大小小于等于0");
        }
        long maxSize = fileUploadConfig.getMaxSize(fileUploadConfig.getMaxFileSize());
        if (fileSize > maxSize) {
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_TOO_LARGE);
        }
        //当前用户id
        String userId = getCurrentLoginUserId();
        //系统文件名
        FileSplitUpload splitUploadOld = fileSplitUploadMapper.selectByPrimaryKey(req.getHashName());
        if (splitUploadOld==null || !userId.equals(splitUploadOld.getUserId())){
            return ServiceResult.fail ("预上传文件信息不存在");
        }
        //文件大小
        if (!splitUploadOld.getFileSize().equals(req.getFileSize())){
            return ServiceResult.fail ("文件大小和预上传文件大小不一致");
        }
        //分片文件数量
        if (!splitUploadOld.getSplitNum().equals(req.getSplitNum())){
            return ServiceResult.fail ("分片文件分片数量和预上传文件分片数量不一致");
        }
        //原文件名不相同
        if (!splitUploadOld.getOriginalName().equals(req.getFileName())){
            return ServiceResult.fail ("分片文件名称和预上传文件名称不一致");
        }
        if (splitUploadOld.getSplitNum()<req.getSplitIndex()){
            return ServiceResult.fail (ErrorCodes.PARAM_ERROR);
        }
        //构建返回对象
        BigFileSliceUploadRes res = new BigFileSliceUploadRes();
        res.setHashName(req.getHashName());
        res.setIsFileMerge(false);
        //当前分割索引块
        Integer splitIndex = req.getSplitIndex()+1;
        //分割记录id
        String uploadId = MD5Util.getMD532(splitUploadOld.getUploadId()+req.getSplitIndex());
        //不存在
        if (fileSplitUploadMapper.selectByPrimaryKey(uploadId)==null){
            //当前时间
            Date nowDate = new Date();
            FileSplitUpload splitUpload = new FileSplitUpload();
            splitUpload.setUploadId(uploadId);
            splitUpload.setParentId(splitUploadOld.getUploadId());
            splitUpload.setHashName(splitUploadOld.getHashName());
            splitUpload.setUserId(userId);
            splitUpload.setCreateUserId(userId);
            splitUpload.setCreateTime(nowDate);
            splitUpload.setOriginalName(req.getFileName());
            splitUpload.setSplitIndex(splitIndex);
            splitUpload.setSplitNum(splitUploadOld.getSplitNum());
            splitUpload.setFileSize(file.getSize());
            splitUpload.setFileSuffix(splitUploadOld.getFileSuffix());
            splitUpload.setSplitSize(splitUploadOld.getSplitSize());
            splitUpload.setCreateMilliSecond(nowDate.getTime());
            splitUpload.setFileContent(file.getBytes());
            fileSplitUploadMapper.insertSelective(splitUpload);
        }
        //如果分片数量和分割数量相等标识全部上传完了，准备合并分片
        if (splitIndex.equals(splitUploadOld.getSplitNum())){
            //如果相等则合并文件开始
            res.setIsFileMerge(true);
        }
        return ServiceResult.ok(res);
    }

    /**
     * 根据文件id下载文件
     * @param req
     * @return
     */
    @Override
    public ServiceResult<String> downloadUploadFileByFileId(UploadFileIdReq req) {
        return null;
    }


    /**
     * 根据拆分信息合并文件 并返回文件的
     * @param userId
     * @param splitUpload
     * @return
     */
    private SingleFileUploadRes mergeUploadFile(String userId,String userNumber,FileSplitUpload splitUpload){
        try {
            if (StringUtils.isBlank(userId)||StringUtils.isBlank(userNumber)){
                throw new DuoJuHeException(ErrorCodes.PARAM_ERROR);
            }
            if (splitUpload==null || !userId.equals(splitUpload.getUserId())){
                throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_NOT_FOUND);
            }
            int byteSize = Integer.parseInt(String.valueOf(splitUpload.getFileSize()));
            byte[] fileBytes = new byte[byteSize];
            int begin = 0;
            for (FileSplitUpload upload:fileSplitUploadMapper.queryFileSplitUploadListByParentId(splitUpload.getUploadId())){
                byte[] fileContent = upload.getFileContent();
                System.arraycopy(fileContent, 0, fileBytes, begin, fileContent.length);
                begin += fileContent.length;
            }
            //文件名称
            String originalFilename = splitUpload.getOriginalName();
            InputStream inputStream = new ByteArrayInputStream(fileBytes);
            MultipartFile file = new MockMultipartFile(ContentType.APPLICATION_OCTET_STREAM.toString(),originalFilename,null, inputStream);
            // 判断文件大小
            long fileSize = file.getSize();
            long maxFileSize = fileUploadConfig.getMaxSize(fileUploadConfig.getMaxFileSize());
            if (fileSize > maxFileSize) {
                throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_TOO_LARGE);
            }
            //用户聊天目录dir
            String fileDir = File.separator+"uploadFile"+File.separator+userNumber;
            //文件根目录
            String rootPath = fileUploadConfig.getRootPath()+fileDir;
            //文件前缀
            String fileUrlPrefix = fileUploadConfig.getFileUrlPrefix()+fileDir;
            //文件名称
            String filename = file.getOriginalFilename();
            //附件上传前缀域名
            String filePrefixDomain = fileUploadConfig.getFilePrefixDomain();
            //允许文件类型
            String[] allowFiles = fileUploadConfig.getFileAllowFiles();
            //开始上传
            UploadFileBean uploadFileBean = UploadFileUtil.writeSingleFileToDisk(file,allowFiles,filePrefixDomain, rootPath, fileUrlPrefix);
            //请求路径
            String fileUrl = uploadFileBean.getFileAbsolutePath();
            if (StringUtils.isBlank(fileUrl)) {
                throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_FORMAT_ERROR);
            }
            //当前用户
            UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
            //保存文件到文件库
            fileUploadHandler.addNewFileUpload(uploadFileBean,userTokenInfoVo);
            //合并完了删除对应的拆分记录beg
            String uploadId = splitUpload.getUploadId();
            //删除拆分子记录
            FileSplitUpload deleteSplitUpload = new FileSplitUpload();
            deleteSplitUpload.setParentId(uploadId);
            fileSplitUploadMapper.delete(deleteSplitUpload);
            //删除拆分主信息记录
            fileSplitUploadMapper.deleteByPrimaryKey(uploadId);
            //合并完了删除对应的拆分记录end
            return new SingleFileUploadRes(filename,fileUrl);
        }catch (Exception e){
            log.error("大文件拆分合并出现异常",e);
            throw new DuoJuHeException(ErrorCodes.UPLOAD_FILE_NOT_FOUND);
        }
    }
}
