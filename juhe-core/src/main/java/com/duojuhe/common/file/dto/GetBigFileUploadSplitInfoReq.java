package com.duojuhe.common.file.dto;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GetBigFileUploadSplitInfoReq extends BaseBean {
    @ApiModelProperty(value = "文件名称", example = "3",required=true)
    @NotBlank(message = "文件名称不能为空")
    @Length(max = 255, message = "文件名称不得超过{max}位字符")
    private String fileName;

    @ApiModelProperty(value = "文件大小", example = "1",required=true)
    @NotNull(message = "文件大小参数不正确")
    @Range(min = 0, max = Integer.MAX_VALUE, message = "文件大小参数不正确")
    private Long fileSize;
}
