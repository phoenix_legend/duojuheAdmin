package com.duojuhe.common.file.dto;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UploadFileEncryptionModeReq extends BaseBean {
    @ApiModelProperty(value = "上传文件加密方式，Sm4=国密4", example = "Sm4",required=true)
    @NotBlank(message = "上传文件加密方式不能为空")
    private String encryptionMode;
}
