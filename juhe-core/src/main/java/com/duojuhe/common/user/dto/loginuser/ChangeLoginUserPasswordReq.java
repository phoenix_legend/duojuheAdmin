package com.duojuhe.common.user.dto.loginuser;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;


/**
 * 用户修改密码请求参数
 *
 * @date 2018/5/25.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChangeLoginUserPasswordReq extends BaseBean {
    @ApiModelProperty(value = "新登录密码至少包含数字和英文", example = "1",required=true)
    @NotBlank(message = "新登录密码不能为空")
    @Length(max = 16, message = "新登录密码不得超过{max}位字符")
    private String password;

    @ApiModelProperty(value = "旧密码", example = "1",required=true)
    @NotBlank(message = "旧密码不能为空")
    @Length(max = 16, message = "旧密码不得超过{max}位字符")
    private String oldPassword;
}
