package com.duojuhe.common.user.dto.loginuser;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


/**
 * 用户修改基本信息请求参数
 *
 * @date 2018/5/25.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChangeLoginUserInfoReq extends BaseBean {
    @ApiModelProperty(value = "姓名", example = "1",required = true)
    @Length(max = 32, message = "姓名不得超过{max}位字符")
    @NotBlank(message = "姓名不能为空")
    private String realName;

    @ApiModelProperty(value = "手机号码", example = "1",required = true)
    @Pattern(regexp = RegexpConstants.MOBILE, message = "手机号码不合法")
    @NotBlank(message = "手机号码不能为空")
    private String mobileNumber;

    @ApiModelProperty(value = "个性签名")
    @Length(max = 200, message = "个性签名不得超过{max}位字符")
    private String motto;

    @ApiModelProperty(value = "性别，取数据字典",required = true)
    @Length(max = 50, message = "性别不得超过{max}位字符")
    @NotBlank(message = "性别不能为空")
    private String genderCode;
}
