package com.duojuhe.common.user.dto.loginuser;

import com.duojuhe.common.annotation.FieldFormatting;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.enums.FormattingTypeEnum;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class QueryLoginUserInfoRes extends BaseBean {
    @ApiModelProperty(value = "用户id", example = "1")
    private String userId;

    @ApiModelProperty(value = "个性签名", example = "1")
    private String motto;

    @ApiModelProperty(value = "当前用户类型中文名", example = "1")
    private String userTypeName;

    @ApiModelProperty(value = "当前用户类型", example = "1")
    private String userTypeCode;

    @ApiModelProperty(value = "登录名", example = "1")
    private String loginName;

    @ApiModelProperty(value = "姓名", example = "1")
    private String realName;

    @ApiModelProperty(value = "头像", example = "1")
    private String headPortrait;

    @ApiModelProperty(value = "角色名称", example = "1")
    private String roleName;

    @ApiModelProperty(value = "手机号码", example = "1")
    @FieldFormatting(formattingType = FormattingTypeEnum.MOBILE_PHONE)
    private String mobileNumber;

    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ApiModelProperty(value = "创建时间", example = "1")
    private Date createTime;

    @ApiModelProperty(value = "性别")
    private String genderCode;

    @ApiModelProperty(value = "性别中文")
    private String genderName;

    @ApiModelProperty(value = "租户名称")
    private String tenantName;

    @ApiModelProperty(value = "租户简称")
    private String tenantAbbreviation;
}
