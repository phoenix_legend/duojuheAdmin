package com.duojuhe.common.user.service;

import com.duojuhe.common.bean.SystemMenuInfoVo;
import com.duojuhe.common.user.dto.UserAuthorizationVo;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.user.dto.loginuser.ChangeLoginUserInfoReq;
import com.duojuhe.common.user.dto.loginuser.ChangeLoginUserPasswordReq;
import com.duojuhe.common.user.dto.loginuser.QueryLoginUserInfoRes;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface CurrentLoginUserService {


    /**
     * 【获取当前用户菜单权限】【tree型结构】获取当前登录用户的权限菜单
     * @return
     */
    ServiceResult<List<SystemMenuInfoVo>> queryCurrentLoginUserMenuTree();



    /**
     * 【获取当前用户权限】获取当前用户拥有的权限
     * @return
     */
    ServiceResult<UserAuthorizationVo> queryCurrentLoginUserAuthorization();


    /**
     *【修改当前用户密码】修改当前登录用户的密码
     * @param req
     * @return
     */
    ServiceResult changeCurrentLoginUserPassword(ChangeLoginUserPasswordReq req);


    /**
     *【修改当前用户头像】修改当前登录用户的头像
     * @param file
     * @return
     */
    ServiceResult<String> changeCurrentLoginUserHeadPortrait(MultipartFile file);



    /**
     *【修改当前用户基本信息】修改当前登录用户的基本信息
     * @param req
     * @return
     */
    ServiceResult changeCurrentLoginUserInfo(ChangeLoginUserInfoReq req);



    /**
     *【获取当前登录用户基本信息】获取当前登录用户基本信息
     * @return
     */
    ServiceResult<QueryLoginUserInfoRes> queryCurrentLoginUserInfoRes();


    /**
     * 用户注销登录
     * @return
     */
    ServiceResult currentLoginUserLogout();
}
