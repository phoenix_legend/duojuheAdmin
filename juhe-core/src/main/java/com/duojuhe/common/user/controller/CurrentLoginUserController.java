package com.duojuhe.common.user.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.bean.SystemMenuInfoVo;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.user.dto.loginuser.ChangeLoginUserInfoReq;
import com.duojuhe.common.user.dto.loginuser.ChangeLoginUserPasswordReq;
import com.duojuhe.common.user.dto.loginuser.QueryLoginUserInfoRes;
import com.duojuhe.common.user.service.CurrentLoginUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.COMMON_PATH + "/currentLoginUser/")
@Api(tags = {"【数据字典】数据字典管理相关接口"})
@Slf4j
public class CurrentLoginUserController {
    @Resource
    private CurrentLoginUserService currentLoginUserService;

    @ApiOperation(value = "【注销登录】当前登录用户退出登录接口")
    @PostMapping(value = "currentLoginUserLogout")
    @OperationLog(moduleName = LogEnum.ModuleName.SELF_SET, operationType = LogEnum.OperationType.LOG_OUT)
    public ServiceResult currentLoginUserLogout() {
        return currentLoginUserService.currentLoginUserLogout();
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【获取当前用户菜单权限】【tree型结构】获取当前登录用户的权限菜单")
    @PostMapping(value = "queryCurrentLoginUserMenuTree")
    public ServiceResult<List<SystemMenuInfoVo>> queryCurrentLoginUserMenuTree() {
        return currentLoginUserService.queryCurrentLoginUserMenuTree();
    }


    @ApiOperation(value = "【修改当前用户密码】修改当前登录用户的密码")
    @PostMapping(value = "changeCurrentLoginUserPassword")
    @OperationLog(moduleName = LogEnum.ModuleName.SELF_SET, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult changeCurrentLoginUserPassword(@Valid @RequestBody @ApiParam(value = "入参类") ChangeLoginUserPasswordReq req) {
        return currentLoginUserService.changeCurrentLoginUserPassword(req);
    }

    @ApiOperation(value = "【修改当前用户头像】修改当前登录用户的头像")
    @PostMapping(value = "changeCurrentLoginUserHeadPortrait")
    @OperationLog(moduleName = LogEnum.ModuleName.SELF_SET, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult<String> changeCurrentLoginUserHeadPortrait(@RequestParam("file") MultipartFile file) {
        return currentLoginUserService.changeCurrentLoginUserHeadPortrait(file);
    }

    @ApiOperation(value = "【修改当前用户基本信息】修改当前登录用户的基本信息")
    @PostMapping(value = "changeCurrentLoginUserInfo")
    @OperationLog(moduleName = LogEnum.ModuleName.SELF_SET, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult changeCurrentLoginUserInfo(@Valid @RequestBody @ApiParam(value = "入参类") ChangeLoginUserInfoReq req) {
        return currentLoginUserService.changeCurrentLoginUserInfo(req);
    }

    @ApiOperation(value = "【获取当前登录用户基本信息】获取当前登录用户基本信息")
    @PostMapping(value = "queryCurrentLoginUserInfoRes")
    public ServiceResult<QueryLoginUserInfoRes> queryCurrentLoginUserInfoRes() {
        return currentLoginUserService.queryCurrentLoginUserInfoRes();
    }
}
