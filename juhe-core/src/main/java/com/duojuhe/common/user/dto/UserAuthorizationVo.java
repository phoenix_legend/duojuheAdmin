package com.duojuhe.common.user.dto;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserAuthorizationVo extends BaseBean {
    @ApiModelProperty(value = "拥有的权限编码", example = "1")
    private List<String> permissionsList;
}
