package com.duojuhe.common.component.dingding;

import com.duojuhe.cache.SystemConfigCache;
import com.duojuhe.common.enums.SystemEnum;
import com.duojuhe.common.utils.dingding.DingDingUtil;
import com.duojuhe.common.utils.thread.ThreadUtils;
import com.duojuhe.coremodule.system.entity.SystemConfig;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DingDingService {
    @Resource
    private SystemConfigCache systemConfigCache;
    /**
     * 发送钉钉通知
     * @param title
     * @param content
     */
    public void dingSendMessage(String title, String content){
        //取出缓存中系统配置
        SystemConfig systemConfig = systemConfigCache.getSystemConfigCache();
        if (systemConfig==null){
            log.debug("【系统错误钉钉通知】系统未开启钉钉通知发送，需要发送的title：{}，content：{}",title,content);
            return;
        }
        if (!SystemEnum.STATUS.NORMAL.getKey().equals(systemConfig.getDingEnable())){
            log.debug("【系统错误钉钉通知】系统未开启钉钉通知发送，需要发送的title：{}，content：{}",title,content);
            return;
        }
        //取出钉钉通知的token
        String accessToken = systemConfig.getDingAccessToken();
        ThreadUtils.execute(() -> {
            try {
                DingDingUtil.sendToRobot(accessToken,title,content);
            }catch (Exception e){
                log.error("【系统错误钉钉通知】发送钉钉通知出现异常",e);
            }
        });
    }
}
