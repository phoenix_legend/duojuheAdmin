package com.duojuhe.common.component.checkauth;


import com.duojuhe.common.bean.UserTokenInfoVo;
import com.duojuhe.common.constant.DataScopeConstants;
import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.common.enums.user.UserEnum;
import com.duojuhe.coremodule.system.entity.SystemDept;
import com.duojuhe.coremodule.system.entity.SystemRole;
import com.duojuhe.coremodule.system.entity.SystemUser;
import com.duojuhe.coremodule.system.mapper.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class DataAuthCheck {
    @Resource
    private SystemRoleMapper systemRoleMapper;
    @Resource
    private SystemDeptMapper systemDeptMapper;
    @Resource
    private SystemUserMapper systemUserMapper;
    @Resource
    private SystemRoleDeptMapper systemRoleDeptMapper;

    /**
     * 获取允许查询的用户记录id
     * @param checkDataScope
     * @param currentUser
     * @return
     */
    public List<String> getAllowOperationUserIdList(String checkDataScope,UserTokenInfoVo currentUser){
        //未知标识
        String unknownId = SystemConstants.UNKNOWN_ID;
        //用户角色ID
        String roleId = StringUtils.isNotBlank(currentUser.getRoleId())?currentUser.getRoleId():unknownId;
        //租户id
        String tenantId = StringUtils.isNotBlank(currentUser.getTenantId())?currentUser.getTenantId():unknownId;
        //用户自己的所在部门ID
        String deptId = StringUtils.isNotBlank(currentUser.getCreateDeptId())?currentUser.getCreateDeptId():unknownId;
        //用户数据范围
        String dataScope = getDataScope(checkDataScope,currentUser);
        //最终用户id集合
        List<String> userIdList = new ArrayList<>();
        //用户数据范围编码
        if (DataScopeConstants.ALL_DATA.equals(dataScope)){
            //全系统全部数据
            return null;
        }else if (DataScopeConstants.TENANT_ALL_DATA.equals(dataScope)){
            //租户全部数据，查询出租户下面的所有用户
            userIdList = systemUserMapper.querySystemUserListByTenantId(tenantId).stream().map(SystemUser::getUserId).collect(Collectors.toList());
        }else if (DataScopeConstants.CUSTOM_DATA.equals(dataScope)){
            //自定义数据权限
            //先根据角色id查询出所有部门id
            List<String>  deptIdList = systemRoleDeptMapper.querySystemDeptIdListByRoleId(roleId);
            //再根据部门id集合查询出用户id
            userIdList = systemUserMapper.querySystemUserIdListByDeptIdList(deptIdList);
        }else if (DataScopeConstants.SELF_DEPT.equals(dataScope)) {
            //自己部门数据权限
            userIdList = systemUserMapper.querySystemUserListByDeptId(deptId).stream().map(SystemUser::getUserId).collect(Collectors.toList());
        }else if (DataScopeConstants.SELF_DEPT_SUBORDINATE.equals(dataScope)) {
            //本部门及以下数据 //查询本部门及以下部门id集合
            SystemDept systemDept= systemDeptMapper.selectByPrimaryKey(deptId);
            if (systemDept!=null && StringUtils.isNotBlank(systemDept.getDeptPath())){
                //机构层级路径，查询向下的部门数据
                String deptPath = systemDept.getDeptPath();
                //先根据部门路径查询出所有部门id
                List<String> deptIdList = systemDeptMapper.queryLikeSystemDeptIdListByDeptPath(deptPath);
                //再根据部门id集合查询出用户id
                userIdList = systemUserMapper.querySystemUserIdListByDeptIdList(deptIdList);
            }
        } else if (DataScopeConstants.SELF_DATA.equals(dataScope)){
            userIdList.add(currentUser.getUserId());
        }
        return userIdList;
    }


    /**
     * 获取用户数据权限范围
     * @param checkDataScope
     * @return
     */
    private String getDataScope(String checkDataScope,UserTokenInfoVo currentUser){
        //未知标识
        String unknownId = SystemConstants.UNKNOWN_ID;
        //用户角色ID
        String roleId = StringUtils.isNotBlank(currentUser.getRoleId())?currentUser.getRoleId():unknownId;
        //用户类型
        String userTypeCode = currentUser.getUserTypeCode();
        //用户数据范围
        String dataScope = DataScopeConstants.SELF_DATA;
        if(StringUtils.isNotBlank(checkDataScope)){
            dataScope = checkDataScope;
        }else{
            if (UserEnum.USER_TYPE.SUPER_ADMIN.getKey().equals(userTypeCode)){
                dataScope = DataScopeConstants.ALL_DATA;
            }else if (UserEnum.USER_TYPE.TENANT_ADMIN.getKey().equals(userTypeCode)){
                dataScope = DataScopeConstants.TENANT_ALL_DATA;
            }else{
                //获取当前用户的角色
                if (!unknownId.equals(roleId)){
                    SystemRole systemRole = systemRoleMapper.selectByPrimaryKey(roleId);
                    dataScope = systemRole==null?DataScopeConstants.SELF_DATA:systemRole.getDataScopeCode();
                }else {
                    dataScope = DataScopeConstants.SELF_DATA;
                }
            }
        }
        return dataScope;
    }
}
