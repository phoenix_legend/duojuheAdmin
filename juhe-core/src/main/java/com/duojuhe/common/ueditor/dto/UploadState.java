package com.duojuhe.common.ueditor.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 上传文件成功返回数据对象
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UploadState extends State {

    private String url;
    private String title;
    private String original;

    public UploadState(String state, String url, String title, String original) {
        super(state);
        this.url = url;
        this.title = title;
        this.original = original;
    }
}
