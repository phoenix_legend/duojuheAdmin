package com.duojuhe.common.ueditor.config;

import com.duojuhe.common.config.FileUploadConfig;
import com.duojuhe.common.ueditor.constant.UEditorEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UEditorConfig {
    /* 上传图片配置项 */
    private Long imageMaxSize;
    private String[] imageAllowFiles;
    private boolean imageCompressEnable = true;
    private Integer imageCompressBorder = 1600;
    private String imageInsertAlign = "none";
    private String imagePathFormat = "/image/";

    /* 涂鸦图片上传配置项 */
    private Long scrawlMaxSize;
    private String scrawlPathFormat = "/image/";

    /* 截图工具上传 */
    private String snapscreenPathFormat = "/image/";

    /* 抓取远程图片配置 */
    private String[] catcherLocalDomain = {"127.0.0.1", "localhost", "img.baidu.com"};
    private Long catcherMaxSize;
    private String[] catcherAllowFiles;
    private String catcherPathFormat = "/image/";

    /* 上传视频配置 */
    private Long videoMaxSize;
    private String[] videoAllowFiles;
    private String videoPathFormat = "/video/";

    /* 上传文件配置 */
    private Long fileMaxSize;
    private String[] fileAllowFiles;
    private String filePathFormat = "/file/";

    /* 列出指定目录下的图片 */
    private String imageManagerListPath = "/image/";
    private Integer imageManagerListSize = 20;
    private String[] imageManagerAllowFiles;

    /* 列出指定目录下的文件 */
    private String fileManagerListPath = "/file/";
    private Integer fileManagerListSize = 20;
    private String[] fileManagerAllowFiles;

    /* 上传图片配置项 */
    private String imageActionName = UEditorEnum.ACTION.UPLOAD_IMAGE.getValue();
    private String imageFieldName = "upfile";
    private String imageUrlPrefix = "";

    /* 涂鸦图片上传配置项 */
    private String scrawlActionName = UEditorEnum.ACTION.UPLOAD_SCRAWL.getValue();
    private String scrawlFieldName = "scrawlBase64";
    private String scrawlUrlPrefix = "";
    private String scrawlInsertAlign = "none";

    /* 截图工具上传 */
    private String snapscreenActionName = UEditorEnum.ACTION.UPLOAD_IMAGE.getValue();
    private String snapscreenUrlPrefix = "";
    private String snapscreenInsertAlign = "none";

    /* 抓取远程图片配置 */
    private String catcherActionName = UEditorEnum.ACTION.CATCH_IMAGE.getValue();
    private String catcherFieldName = "source";
    private String catcherUrlPrefix = "";

    /* 上传视频配置 */
    private String videoActionName = UEditorEnum.ACTION.UPLOAD_VIDEO.getValue();
    private String videoFieldName = "upfile";
    private String videoUrlPrefix = "";

    /* 上传文件配置 */
    private String fileActionName = UEditorEnum.ACTION.UPLOAD_FILE.getValue();
    private String fileFieldName = "upfile";
    private String fileUrlPrefix = "";


    /* 列出指定目录下的图片 */
    private String imageManagerActionName = UEditorEnum.ACTION.LIST_IMAGE.getValue();
    private String imageManagerUrlPrefix = "";
    private String imageManagerInsertAlign = "none";


    /* 列出指定目录下的文件 */
    private String fileManagerActionName = UEditorEnum.ACTION.LIST_FILE.getValue();
    private String fileManagerUrlPrefix = "";


    public UEditorConfig(FileUploadConfig config){
        this.imageMaxSize = config.getMaxSize(config.getMaxImageSize());
        this.imageAllowFiles = config.getImageAllowFiles();
        this.scrawlMaxSize = config.getMaxSize(config.getScrawlMaxSize());
        this.catcherMaxSize = config.getMaxSize(config.getCatcherMaxSize());
        this.catcherAllowFiles = config.getCatcherAllowFiles();
        this.videoMaxSize = config.getMaxSize(config.getVideoMaxSize());
        this.videoAllowFiles = config.getVideoAllowFiles();
        this.fileMaxSize = config.getMaxSize(config.getMaxFileSize());
        this.fileAllowFiles = config.getFileAllowFiles();
        this.imageManagerAllowFiles = config.getImageAllowFiles();
        this.fileManagerAllowFiles = config.getFileAllowFiles();
    }

}
