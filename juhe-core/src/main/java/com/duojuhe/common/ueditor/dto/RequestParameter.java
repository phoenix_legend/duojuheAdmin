package com.duojuhe.common.ueditor.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 统一请求参数
 */
@Data
public class RequestParameter {

    private String action;          // 功能名称
    private String callback;   // jsonp请求
    private Integer start;          // 查询分页起始页
    private Integer size;           // 查询分页大小
    private String scrawlBase64;   // 涂鸦上传
    private List<String> source;    // 远程抓图

    // 一定要放在最后面。不然和涂鸦base64数据冲突。数据绑定的时候按照顺序来绑定。
    private MultipartFile upfile;   // 文件

}
