package com.duojuhe.common.ueditor.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 判断成功失败基本对象
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class State {

    private String state;// 成功返回SUCCESS，是否返回其他字符串

}
