package com.duojuhe.common.ueditor.controller;

import com.duojuhe.common.config.FileUploadConfig;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.ueditor.dto.RequestParameter;
import com.duojuhe.common.ueditor.main.DefaultAction;
import com.duojuhe.common.ueditor.main.UEditorMain;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@Api(tags={"百度富文本编辑器"})
@Slf4j
public class UeditorController {
    @Resource
    private FileUploadConfig fileUploadConfig;
    @Resource
    private DefaultAction action;
    /**
     *ueditor 后台主方法
     */
    @ApiOperation(value = "【百度富文本编辑】后台主方法初始接口")
    @RequestMapping(value = ApiPathConstants.COMMON_PATH + "/ueditor", produces = MediaType.APPLICATION_JSON_VALUE)
    public String ueditor(RequestParameter parameter, HttpServletRequest request, HttpServletResponse response) {
        return UEditorMain.main(parameter, action,fileUploadConfig, request, response);
    }

}