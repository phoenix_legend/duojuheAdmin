package com.duojuhe.coremodule.form.utils.handle;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.duojuhe.common.bean.struct.ItemDefaultValueStruct;
import com.duojuhe.common.utils.jsonutils.JsonUtils;
import com.duojuhe.coremodule.form.entity.FormProjectItem;
import com.duojuhe.coremodule.form.pojo.projectitem.QueryFormProjectItemPageRes;
import com.duojuhe.coremodule.form.pojo.projectitem.QueryFormProjectItemRes;
import com.duojuhe.coremodule.form.pojo.projectitem.SaveFormProjectItemReq;
import com.duojuhe.coremodule.form.pojo.projectlogic.FormProjectLogicCondition;
import com.duojuhe.coremodule.form.pojo.projectlogic.QueryFormProjectLogicPageRes;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class HandleFormUtils {

    /**
     * 格式化项目Item的数据
     */
    public static FormProjectItem buildFormProjectItem(SaveFormProjectItemReq req) {
        //把Map转换成Bean 在转换成Map 去除不在bean字段列表的多字段
        FormProjectItem entity = new FormProjectItem();
        entity.setFormItemExpand(JsonUtils.objToJson(req.getExpand()));
        if (req.getRegList()!=null){
            entity.setFormItemRegex(JSONArray.parseArray(JSON.toJSONString(req.getRegList())).toJSONString());
        }
        //默认值格式化 1判断是否是Json
        Object defaultValue = req.getDefaultValue();
        if (defaultValue!=null){
            if (ObjectUtil.isNotEmpty(defaultValue)) {
                String jsonStr = JsonUtils.objToJson(defaultValue);
                boolean json = JSONUtil.isJson(jsonStr);
                if (json) {
                    entity.setFormItemDefaultValue(JSON.toJSONString(new ItemDefaultValueStruct(true, jsonStr)));
                }
            }
            entity.setFormItemDefaultValue(JSON.toJSONString(new ItemDefaultValueStruct(false, defaultValue)));
        }else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("json",false);
            entity.setFormItemDefaultValue(jsonObject.toJSONString());
        }
        return entity;
    }

    /**
     * 格式化项目Item的数据
     */
    public static void setHandleFormProjectItemPageRes(QueryFormProjectItemPageRes res) {
        //扩展字段
        if (StringUtils.isNotBlank(res.getFormItemExpand())&&!"null".equals(res.getFormItemExpand().toLowerCase())){
            res.setExpand(JSONObject.parseObject(res.getFormItemExpand(), Map.class));
        }
        //默认值
        if (StringUtils.isNotBlank(res.getFormItemDefaultValue())){
            res.setDefaultValue(JSONObject.parseObject(res.getFormItemDefaultValue()));
        }
        //正则
        if (StringUtils.isNotBlank(res.getFormItemRegex())){
            List<Map<String, Object>> regList = JSON.parseObject(res.getFormItemRegex(), new TypeReference<List<Map<String, Object>>>(){});
            res.setRegList(regList);
        }
    }



    /**
     * 格式化项目Item的数据
     */
    public static void setHandleFormProjectItemRes(QueryFormProjectItemRes res) {
        //扩展字段
        if (StringUtils.isNotBlank(res.getFormItemExpand())&&!"null".equals(res.getFormItemExpand().toLowerCase())){
            res.setExpand(JSONObject.parseObject(res.getFormItemExpand(), Map.class));
        }
        //默认值
        if (StringUtils.isNotBlank(res.getFormItemDefaultValue())){
            res.setDefaultValue(JSONObject.parseObject(res.getFormItemDefaultValue()));
        }
        //正则
        if (StringUtils.isNotBlank(res.getFormItemRegex())){
            List<Map<String, Object>> regList = JSON.parseObject(res.getFormItemRegex(), new TypeReference<List<Map<String, Object>>>(){});
            res.setRegList(regList);
        }
    }

    /**
     * 获取表单逻辑配置json字符串
     * @param logicResList
     * @return
     */
    public static String getFormLogicConfigJsonString(List<QueryFormProjectLogicPageRes> logicResList){
        String formLogicConfig = "";
        if (logicResList!=null&&!logicResList.isEmpty()){
            for (QueryFormProjectLogicPageRes res:logicResList){
                if (StringUtils.isNotBlank(res.getConditionJson())){
                    res.setConditionList(JSONObject.parseArray(res.getConditionJson(), FormProjectLogicCondition.class));
                }
                formLogicConfig = JSON.toJSONString(logicResList);
            }
        }
        return  formLogicConfig;
    }


}
