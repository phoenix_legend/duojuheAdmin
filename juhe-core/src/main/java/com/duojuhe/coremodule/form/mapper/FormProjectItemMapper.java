package com.duojuhe.coremodule.form.mapper;

import com.duojuhe.coremodule.form.entity.FormProjectItem;
import com.duojuhe.coremodule.form.pojo.projectitem.*;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FormProjectItemMapper extends TkMapper<FormProjectItem> {

    /**
     * 根据项目id查询表单子项目list
     *
     * @return
     */
    List<FormProjectItem> queryFormProjectItemListByProjectId(@Param("projectId") String projectId);



    /**
     * 分页查询 根据条件查询表单子项目list
     *
     * @return
     */
    List<QueryFormProjectItemPageRes> queryFormProjectItemPageResList(@Param("projectId") String projectId);

    /**
     * 根据项目id查询表单子项目list
     *
     * @return
     */
    List<QueryFormProjectItemRes> queryFormProjectItemResListByProjectId(@Param("projectId") String projectId);


    /**
     * 根据项目id查询表单的表头信息
     *
     * @return
     */
    List<QueryFormProjectItemTableHeadRes> queryFormProjectItemTableHeadResByProjectId(@Param("projectId") String projectId);

}