package com.duojuhe.coremodule.form.pojo.project;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.coremodule.form.pojo.projectitem.QueryFormProjectItemRes;
import com.duojuhe.coremodule.form.pojo.projectstyle.QueryFormProjectStyleRes;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryFormProjectDetailsRes extends BaseBean {
    @ApiModelProperty(value = "项目详情", required = true)
    private QueryFormProjectRes formProjectRes;

    @ApiModelProperty(value = "项目子表单集合", required = true)
    private List<QueryFormProjectItemRes> formProjectItemResList;

    @ApiModelProperty(value = "项目表单风格详情", required = true)
    private QueryFormProjectStyleRes formProjectStyleRes;
}