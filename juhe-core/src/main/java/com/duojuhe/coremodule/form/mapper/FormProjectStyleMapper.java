package com.duojuhe.coremodule.form.mapper;

import com.duojuhe.coremodule.form.entity.FormProjectStyle;
import com.duojuhe.coremodule.form.pojo.projectstyle.QueryFormProjectStyleRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

public interface FormProjectStyleMapper extends TkMapper<FormProjectStyle> {
    /**
     * 根据项目id详情查询详细
     */
    QueryFormProjectStyleRes queryFormProjectStyleResByProjectId(@Param(value = "projectId") String projectId);
}