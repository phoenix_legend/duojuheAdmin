package com.duojuhe.coremodule.form.service.impl;

import com.duojuhe.common.annotation.KeyLock;
import com.duojuhe.common.enums.SystemEnum;
import com.duojuhe.common.result.ErrorCodes;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.form.entity.FormProject;
import com.duojuhe.coremodule.form.entity.FormProjectSetting;
import com.duojuhe.coremodule.form.mapper.FormProjectMapper;
import com.duojuhe.coremodule.form.mapper.FormProjectSettingMapper;
import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import com.duojuhe.coremodule.form.pojo.project.QueryFormProjectRes;
import com.duojuhe.coremodule.form.pojo.projectsetting.QueryFormProjectSettingRes;
import com.duojuhe.coremodule.form.pojo.projectsetting.QueryPublicFormProjectSettingRes;
import com.duojuhe.coremodule.form.pojo.projectsetting.UpdateFormProjectSettingReq;
import com.duojuhe.coremodule.form.service.FormProjectSettingService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.Date;

@Slf4j
@Service
public class FormProjectSettingServiceImpl extends BaseService implements FormProjectSettingService {
    @Resource
    private FormProjectMapper formProjectMapper;

    @Resource
    private FormProjectSettingMapper formProjectSettingMapper;

    /**
     * 根据项目id查询表单项目设置详情
     * @param req
     * @return
     */
    @KeyLock(lockKeyParts = "projectId")
    @Override
    public ServiceResult<QueryFormProjectSettingRes> queryFormProjectSettingResByProjectId(FormProjectIdReq req) {
        String projectId = req.getProjectId();
        QueryFormProjectSettingRes res = formProjectSettingMapper.queryFormProjectSettingResByProjectId(projectId);
        if (res == null) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        return ServiceResult.ok(res);
    }

    /**
     * 【更新】表单项目设置更新
     * @param req
     * @return
     */
    @KeyLock(lockKeyParts = "projectId")
    @Override
    public ServiceResult updateFormProjectSetting(UpdateFormProjectSettingReq req) {
        String projectId = req.getProjectId();
        FormProject formProjectOld = formProjectMapper.selectByPrimaryKey(projectId);
        if (formProjectOld == null) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        FormProjectSetting formProjectSettingOld = formProjectSettingMapper.selectByPrimaryKey(projectId);
        if (formProjectSettingOld == null) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        //是
        Integer YES = SystemEnum.IS_YES.YES.getKey();
        //否
        Integer NO = SystemEnum.IS_YES.NO.getKey();
        //当前登录人id
        String userId = getCurrentLoginUserId();
        //当前时间
        Date nowDate = new Date();
        FormProjectSetting formProjectSetting = new FormProjectSetting();
        formProjectSetting.setProjectId(projectId);
        formProjectSetting.setUpdateUserId(userId);
        formProjectSetting.setUpdateTime(nowDate);
        formProjectSetting.setShowQuantitative(req.getShowQuantitative()?YES:NO);
        formProjectSetting.setShowTimedCollection(req.getShowTimedCollection()?YES:NO);
        formProjectSetting.setLimitWrite(req.getLimitWrite()?YES:NO);
        formProjectSetting.setResultPublic(req.getResultPublic()?YES:NO);
        formProjectSetting.setSubmitPromptImg(req.getSubmitPromptImg());
        formProjectSetting.setSubmitJumpUrl(req.getSubmitJumpUrl());
        formProjectSetting.setSubmitPromptText(req.getSubmitPromptText());
        formProjectSetting.setEveryoneWriteNum(req.getEveryoneWriteNum());
        formProjectSetting.setLimitWriteNumMode(req.getLimitWriteNumMode());
        formProjectSetting.setShareImg(req.getShareImg());
        formProjectSetting.setShareDesc(req.getShareDesc());
        formProjectSetting.setShareTitle(req.getShareTitle());
        formProjectSetting.setTimedCollectionBeginTime(DateUtils.parseDateTime(req.getTimedCollectionBeginTime()));
        formProjectSetting.setTimedCollectionEndTime(DateUtils.parseDateTime(req.getTimedCollectionEndTime()));
        formProjectSetting.setTimedDeactivatePromptText(req.getTimedDeactivatePromptText());
        formProjectSetting.setTimedNotEnabledPromptText(req.getTimedNotEnabledPromptText());
        formProjectSetting.setQuantitativeEndPromptText(req.getQuantitativeEndPromptText());
        formProjectSetting.setQuantitativeTotalQuantity(req.getQuantitativeTotalQuantity());
        formProjectSettingMapper.updateByPrimaryKeySelective(formProjectSetting);
        return ServiceResult.ok(ErrorCodes.SUCCESS);
    }
}
