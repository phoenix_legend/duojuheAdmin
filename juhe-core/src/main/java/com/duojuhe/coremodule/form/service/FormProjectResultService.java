package com.duojuhe.coremodule.form.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import com.duojuhe.coremodule.form.pojo.projectresult.*;
import com.duojuhe.coremodule.form.pojo.projectresult.report.QueryProjectResultReportDeviceRes;
import com.duojuhe.coremodule.form.pojo.projectresult.report.QueryProjectResultReportSituationRes;
import com.duojuhe.coremodule.form.pojo.projectresult.report.QueryProjectResultReportSourceRes;
import com.duojuhe.coremodule.form.pojo.projectresult.report.QueryProjectResultReportStatsRes;

import java.util.List;

public interface FormProjectResultService {
    /**
     * 【保存】表单结果
     * @param req
     * @return
     */
    ServiceResult<String> saveFormProjectResult(SaveFormProjectResultReq req);

    /**
     * 【修改】表单结果
     * @param req
     * @return
     */
    ServiceResult<String> updateFormProjectResult(UpdateFormProjectResultReq req);

    /**
     * 【彻底删除】删除表单结果记录
     * @param req
     * @return
     */
    ServiceResult deleteFormProjectResultById(FormProjectResultIdReq req);

    /**
     * 【分页查询】根据条件查询表单项目提交结果集合
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryFormProjectResultPageRes>>> queryFormProjectResultPageResList(QueryFormProjectResultPageReq req);


    /**
     * 【查询项目收集设备分析】根据项目id查询提交来源统计分析
     * @param req
     * @return
     */
    ServiceResult<List<QueryProjectResultReportDeviceRes>> queryProjectResultReportDeviceResByProjectId(FormProjectIdReq req);




    /**
     * 【项目收集情况 按周查看】根据项目id查询项目收集情况 按周查看分析
     * @param req
     * @return
     */
    ServiceResult<List<QueryProjectResultReportSituationRes>> queryProjectResultReportSituationResByProjectId(FormProjectIdReq req);




    /**
     * 【查询项目收集来源分析】根据项目id查询项目收集来源分析
     * @param req
     * @return
     */
    ServiceResult<List<QueryProjectResultReportSourceRes>> queryProjectResultReportSourceResByProjectId(FormProjectIdReq req);



    /**
     * 【查询项目收集信息】根据项目id查询项目收集信息
     * @param req
     * @return
     */
    ServiceResult<QueryProjectResultReportStatsRes> queryProjectResultReportStatsResByProjectId(FormProjectIdReq req);



    /**
     * 【查询问卷记录编辑初始值】根据记录id查询查询记录编辑初始值明细
     * @param req
     * @return
     */
    ServiceResult<QueryFormProjectResultDetailsEditRes> queryFormProjectResultDetailsEditResById(FormProjectResultIdReq req);

}
