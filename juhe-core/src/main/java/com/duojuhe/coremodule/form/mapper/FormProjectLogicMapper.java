package com.duojuhe.coremodule.form.mapper;

import com.duojuhe.coremodule.form.entity.FormProjectLogic;
import com.duojuhe.coremodule.form.pojo.projectlogic.QueryFormProjectLogicPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FormProjectLogicMapper extends TkMapper<FormProjectLogic> {

    /**
     * 分页查询根据项目id查询表单项目逻辑集合
     * @param projectId
     * @return
     */
    List<QueryFormProjectLogicPageRes> queryFormProjectLogicPageResByProjectId(@Param("projectId") String projectId);


    /**
     * 根据项目id查询表单项目逻辑集合list
     * @param projectId
     * @return
     */
    List<FormProjectLogic> queryFormProjectLogicListByProjectId(@Param("projectId") String projectId);

}