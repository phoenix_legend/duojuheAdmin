package com.duojuhe.coremodule.form.pojo.projecttemplate;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveFormProjectTemplateReq extends BaseBean {
    @ApiModelProperty(value = "模板名称", required = true)
    @NotBlank(message = "模板名称不能为空")
    @Length(max = 50, message = "模板名称不得超过{max}位字符")
    private String projectName;

    @ApiModelProperty(value = "封面图片base64格式", required = true)
    @NotBlank(message = "模板封面图片不能为空")
    private String coverImg;

    @ApiModelProperty(value = "分类id", required = true)
    @NotBlank(message = "模板所属分类不能为空")
    private String categoryId;


}