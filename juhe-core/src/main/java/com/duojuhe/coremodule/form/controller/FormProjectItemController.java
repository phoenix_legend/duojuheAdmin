package com.duojuhe.coremodule.form.controller;

import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import com.duojuhe.coremodule.form.pojo.projectitem.*;
import com.duojuhe.coremodule.form.service.FormProjectItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/formProjectItem/")
@Api(tags = {"【表单子项目】表单子项目管理相关接口"})
@Slf4j
public class FormProjectItemController {

    @Resource
    public FormProjectItemService formProjectItemService;

    @ApiOperation(value = "【分页查询】分页查询子表单项目list")
    @PostMapping(value = "queryFormProjectItemPageResList")
    public ServiceResult<PageResult<List<QueryFormProjectItemPageRes>>> queryFormProjectItemPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryFormProjectItemPageReq req) {
        return formProjectItemService.queryFormProjectItemPageResList(req);
    }


    @ApiOperation(value = "【查询本项目最大的表单id】根据项目ID查询本项目最大的表单id")
    @PostMapping(value = "queryFormProjectItemMaxFormItemIdByProjectId")
    public ServiceResult<String> queryFormProjectItemMaxFormItemIdByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectItemService.queryFormProjectItemMaxFormItemIdByProjectId(req);
    }

    @ApiOperation(value = "【项目表头信息】根据项目Id查询表单项目的表头信息")
    @PostMapping(value = "queryFormProjectItemTableHeadResByProjectId")
    public ServiceResult<List<QueryFormProjectItemTableHeadRes>> queryFormProjectItemTableHeadResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectItemService.queryFormProjectItemTableHeadResByProjectId(req);
    }


    @ApiOperation(value = "【保存和修改】保存和修改表单项目")
    @PostMapping(value = "saveUpdateFormProjectItem")
    public ServiceResult<Integer> saveUpdateFormProjectItem(@Valid @RequestBody @ApiParam(value = "入参类") SaveFormProjectItemReq req) {
        return formProjectItemService.saveUpdateFormProjectItem(req);
    }



    @ApiOperation(value = "【删除】删除表单项目")
    @PostMapping(value = "deleteFormProjectItem")
    public ServiceResult deleteFormProjectItem(@Valid @RequestBody @ApiParam(value = "入参类") DeleteFormProjectItemReq req) {
        return formProjectItemService.deleteFormProjectItem(req);
    }

    @ApiOperation(value = "【修改排序】修改表单项目排序值")
    @PostMapping(value = "updateFormProjectItemSort")
    public ServiceResult<Long> updateFormProjectItemSort(@Valid @RequestBody @ApiParam(value = "入参类") UpdateFormProjectItemSortReq req) {
        return formProjectItemService.updateFormProjectItemSort(req);
    }

}
