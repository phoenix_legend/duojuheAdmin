package com.duojuhe.coremodule.form.controller;

import com.duojuhe.common.annotation.RepeatSubmit;
import com.duojuhe.common.constant.ApiPathConstants;

import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.entity.FormProjectLogic;
import com.duojuhe.coremodule.form.pojo.project.*;
import com.duojuhe.coremodule.form.pojo.projectlogic.DeleteFormProjectLogicReq;
import com.duojuhe.coremodule.form.pojo.projectlogic.QueryFormProjectLogicPageRes;
import com.duojuhe.coremodule.form.pojo.projectlogic.SaveFormProjectLogicReq;
import com.duojuhe.coremodule.form.service.FormProjectLogicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/formProjectLogic/")
@Api(tags = {"【表单项目逻辑】表单项目逻辑管理相关接口"})
@Slf4j
public class FormProjectLogicController {
    @Resource
    public FormProjectLogicService formProjectLogicService;

    @ApiOperation(value = "【根据项目id查询逻辑集合】根据项目id查询逻辑集合")
    @PostMapping(value = "queryFormProjectLogicPageResByProjectId")
    public ServiceResult<List<QueryFormProjectLogicPageRes>> queryFormProjectLogicPageResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectLogicService.queryFormProjectLogicPageResByProjectId(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【保存】表单项目逻辑")
    @PostMapping(value = "saveFormProjectLogic")
    public ServiceResult<FormProjectLogic> saveFormProjectLogic(@Valid  @RequestBody  @ApiParam(value = "入参类") SaveFormProjectLogicReq req) {
        return formProjectLogicService.saveFormProjectLogic(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【删除】根据ID删除表单项目逻辑，注意一次仅能删除一个表单项目逻辑")
    @PostMapping(value = "deleteFormProjectLogic")
    public ServiceResult deleteFormProjectLogic(@Valid @RequestBody @ApiParam(value = "入参类") DeleteFormProjectLogicReq req) {
        return formProjectLogicService.deleteFormProjectLogic(req);
    }
}
