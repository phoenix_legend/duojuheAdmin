package com.duojuhe.coremodule.form.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.annotation.RepeatSubmit;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.project.*;
import com.duojuhe.coremodule.form.service.FormProjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/formProject/")
@Api(tags = {"【表单项目】表单项目管理相关接口"})
@Slf4j
public class FormProjectController {
    @Resource
    public FormProjectService formProjectService;

    @ApiOperation(value = "【分页查询】分页查询表单项目list")
    @PostMapping(value = "queryFormProjectPageResList")
    public ServiceResult<PageResult<List<QueryFormProjectPageRes>>> queryFormProjectPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryFormProjectPageReq req) {
        return formProjectService.queryFormProjectPageResList(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【保存】表单项目")
    @PostMapping(value = "saveFormProject")
    @OperationLog(moduleName = LogEnum.ModuleName.FORM_SURVEY_PROJECT, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveFormProject(@Valid  @RequestBody  @ApiParam(value = "入参类") SaveFormProjectReq req) {
        return formProjectService.saveFormProject(req);
    }


    @RepeatSubmit
    @ApiOperation(value = "【保存】根据模板id创建新表单项目")
    @PostMapping(value = "createFormProjectByTemplate")
    @OperationLog(moduleName = LogEnum.ModuleName.FORM_SURVEY_PROJECT, operationType = LogEnum.OperationType.ADD)
    public ServiceResult createFormProjectByTemplate(@Valid  @RequestBody  @ApiParam(value = "入参类") CreateFormProjectByTemplateReq req) {
        return formProjectService.createFormProjectByTemplate(req);
    }


    @ApiOperation(value = "【修改】表单项目")
    @PostMapping(value = "updateFormProject")
    @OperationLog(moduleName = LogEnum.ModuleName.FORM_SURVEY_PROJECT, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateFormProject(@Valid  @RequestBody  @ApiParam(value = "入参类") UpdateFormProjectReq req) {
        return formProjectService.updateFormProject(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【更新发布状态】表单项目更新发布状态")
    @PostMapping(value = "updateFormProjectStatusByProjectId")
    @OperationLog(moduleName = LogEnum.ModuleName.FORM_SURVEY_PROJECT, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateFormProjectStatusByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectService.updateFormProjectStatusByProjectId(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【删除】根据ID删除表单项目，注意一次仅能删除一个表单项目")
    @PostMapping(value = "deleteFormProjectByProjectId")
    @OperationLog(moduleName = LogEnum.ModuleName.FORM_SURVEY_PROJECT, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteFormProjectByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectService.deleteFormProjectByProjectId(req);
    }

    @ApiOperation(value = "【查询详情】根据项目key查询详情")
    @PostMapping(value = "queryFormProjectResByProjectId")
    public ServiceResult<QueryFormProjectRes> queryFormProjectResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectService.queryFormProjectResByProjectId(req);
    }


    @ApiOperation(value = "【查询明细】根据项目id查询项目明细")
    @PostMapping(value = "queryFormProjectDetailsResByProjectId")
    public ServiceResult<QueryFormProjectDetailsRes> queryFormProjectDetailsResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectService.queryFormProjectDetailsResByProjectId(req);
    }


}
