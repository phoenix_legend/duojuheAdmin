package com.duojuhe.coremodule.form.pojo.projectresult;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;



@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FormProjectResultIdReq extends BaseBean {

    @ApiModelProperty(value = "记录ID", required = true)
    @NotBlank(message = "记录ID不可为空")
    private String id;
}