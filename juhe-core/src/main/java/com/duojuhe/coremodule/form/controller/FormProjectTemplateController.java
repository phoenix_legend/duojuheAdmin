package com.duojuhe.coremodule.form.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.annotation.RepeatSubmit;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.project.*;
import com.duojuhe.coremodule.form.pojo.projecttemplate.SaveFormProjectTemplateReq;
import com.duojuhe.coremodule.form.pojo.projecttemplate.SaveFormProjectTransferTemplateReq;
import com.duojuhe.coremodule.form.pojo.projecttemplate.UpdateFormProjectTemplateReq;
import com.duojuhe.coremodule.form.service.FormProjectTemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/formProjectTemplate/")
@Api(tags = {"【表单项目模板】表单项目模板管理相关接口"})
@Slf4j
public class FormProjectTemplateController {
    @Resource
    public FormProjectTemplateService formProjectTemplateService;

    @ApiOperation(value = "【分页查询】分页查询表单项目私有模板list")
    @PostMapping(value = "queryFormProjectPrivateTemplatePageResList")
    public ServiceResult<PageResult<List<QueryFormProjectPageRes>>> queryFormProjectPrivateTemplatePageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryFormProjectPageReq req) {
        return formProjectTemplateService.queryFormProjectPrivateTemplatePageResList(req);
    }


    @ApiOperation(value = "【分页查询】分页查询表单项目公共模板list")
    @PostMapping(value = "queryFormProjectPublicTemplatePageResList")
    public ServiceResult<PageResult<List<QueryFormProjectPageRes>>> queryFormProjectPublicTemplatePageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryFormProjectPageReq req) {
        return formProjectTemplateService.queryFormProjectPublicTemplatePageResList(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【保存】表单项目模板")
    @PostMapping(value = "saveFormProjectTemplate")
    @OperationLog(moduleName = LogEnum.ModuleName.FORM_SURVEY_TEMPLATE, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveFormProjectTemplate(@Valid  @RequestBody  @ApiParam(value = "入参类") SaveFormProjectTemplateReq req) {
        return formProjectTemplateService.saveFormProjectTemplate(req);
    }



    @RepeatSubmit
    @ApiOperation(value = "【保存】项目转成模板")
    @PostMapping(value = "saveFormProjectTransferTemplate")
    @OperationLog(moduleName = LogEnum.ModuleName.FORM_SURVEY_TEMPLATE, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveFormProjectTransferTemplate(@Valid  @RequestBody  @ApiParam(value = "入参类") SaveFormProjectTransferTemplateReq req) {
        return formProjectTemplateService.saveFormProjectTransferTemplate(req);
    }

    @ApiOperation(value = "【修改】表单项目模板")
    @PostMapping(value = "updateFormProjectTemplate")
    @OperationLog(moduleName = LogEnum.ModuleName.FORM_SURVEY_TEMPLATE, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateFormProjectTemplate(@Valid  @RequestBody  @ApiParam(value = "入参类") UpdateFormProjectTemplateReq req) {
        return formProjectTemplateService.updateFormProjectTemplate(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【删除】根据ID删除表单项目模板，注意一次仅能删除一个表单项目模板")
    @PostMapping(value = "deleteFormProjectTemplateByProjectId")
    @OperationLog(moduleName = LogEnum.ModuleName.FORM_SURVEY_TEMPLATE, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteFormProjectTemplateByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectTemplateService.deleteFormProjectTemplateByProjectId(req);
    }

    @ApiOperation(value = "【查询详情】根据项目模板key查询详情")
    @PostMapping(value = "queryFormProjectTemplateResByProjectId")
    public ServiceResult<QueryFormProjectRes> queryFormProjectTemplateResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectTemplateService.queryFormProjectTemplateResByProjectId(req);
    }


    @ApiOperation(value = "【查询明细】根据项目模板id查询项目模板明细")
    @PostMapping(value = "queryFormProjectTemplateDetailsResByProjectId")
    public ServiceResult<QueryFormProjectDetailsRes> queryFormProjectTemplateDetailsResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectTemplateService.queryFormProjectTemplateDetailsResByProjectId(req);
    }


}
