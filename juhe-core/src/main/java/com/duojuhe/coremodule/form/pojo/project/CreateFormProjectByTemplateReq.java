package com.duojuhe.coremodule.form.pojo.project;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CreateFormProjectByTemplateReq extends FormProjectIdReq {
    @ApiModelProperty(value = "项目名称", required = true)
    @NotBlank(message = "项目名称不能为空")
    @Length(max = 50, message = "项目名称不得超过{max}位字符")
    private String projectName;

    @ApiModelProperty(value = "描述")
    private String description;
}
