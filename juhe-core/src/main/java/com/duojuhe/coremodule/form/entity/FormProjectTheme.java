package com.duojuhe.coremodule.form.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("表单项目外观风格")
@Table(name = "form_project_theme")
public class FormProjectTheme extends BaseBean {
    @ApiModelProperty(value = "主键id", required = true)
    @Column(name = "theme_id")
    @Id
    private String themeId;

    @ApiModelProperty(value = "主题名称")
    @Column(name = "theme_name")
    private String themeName;

    @ApiModelProperty(value = "主题风格样式逗号分隔")
    @Column(name = "theme_style")
    private String themeStyle;

    @ApiModelProperty(value = "主题颜色逗号分隔")
    @Column(name = "theme_color")
    private String themeColor;

    @ApiModelProperty(value = "按钮颜色")
    @Column(name = "button_color")
    private String buttonColor;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "背景颜色")
    @Column(name = "background_color")
    private String backgroundColor;

    @ApiModelProperty(value = "背景图片")
    @Column(name = "background_img")
    private String backgroundImg;

    @ApiModelProperty(value = "是否显示标题")
    @Column(name = "show_title")
    private Integer showTitle;

    @ApiModelProperty(value = "提交按钮显示字")
    @Column(name = "submit_button_text")
    private String submitButtonText;

    @ApiModelProperty(value = "是否显示描述语")
    @Column(name = "show_describe")
    private Integer showDescribe;

    @ApiModelProperty(value = "是否显示序号")
    @Column(name = "show_number")
    private Integer showNumber;

    @ApiModelProperty(value = "是否显示重置按钮")
    @Column(name = "show_reset_button")
    private Integer showResetButton;

    @ApiModelProperty(value = "重置按钮文字")
    @Column(name = "reset_button_text")
    private String resetButtonText;

    @ApiModelProperty(value = "重置按钮颜色")
    @Column(name = "reset_button_color")
    private String resetButtonColor;

    @ApiModelProperty(value = "头部图片")
    @Column(name = "head_image")
    private String headImage;
}