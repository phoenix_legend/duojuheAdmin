package com.duojuhe.coremodule.form.pojo.projectsetting;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateFormProjectSettingReq extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String projectId;

    @ApiModelProperty(value = "提交提示图片")
    private String submitPromptImg;

    @ApiModelProperty(value = "提交提示文字")
    private String submitPromptText;

    @ApiModelProperty(value = "提交后跳转地址")
    private String submitJumpUrl;

    @ApiModelProperty(value = "是否公开提交结果")
    private Boolean resultPublic;

    @ApiModelProperty(value = "是否限制填写次数")
    private Boolean limitWrite;

    @ApiModelProperty(value = "每人允许填写次数")
    private Integer everyoneWriteNum;

    @ApiModelProperty(value = "限制填写次数方式每天，总共")
    private String limitWriteNumMode;

    @ApiModelProperty(value = "分享图片")
    private String shareImg;

    @ApiModelProperty(value = "分享标题")
    private String shareTitle;

    @ApiModelProperty(value = "分享描述")
    private String shareDesc;

    @ApiModelProperty(value = "是否定时收集")
    private Boolean showTimedCollection;

    @ApiModelProperty(value = "定时收集开始时间")
    private String timedCollectionBeginTime;

    @ApiModelProperty(value = "定时收集结束时间")
    private String timedCollectionEndTime;

    @ApiModelProperty(value = "定时未开始提示文字")
    private String timedNotEnabledPromptText;

    @ApiModelProperty(value = "定时结束提示文字")
    private String timedDeactivatePromptText;

    @ApiModelProperty(value = "是否启用定量收集")
    private Boolean showQuantitative;

    @ApiModelProperty(value = "定量收集总数量")
    private Integer quantitativeTotalQuantity;

    @ApiModelProperty(value = "定量收集完成提示文字")
    private String quantitativeEndPromptText;
}