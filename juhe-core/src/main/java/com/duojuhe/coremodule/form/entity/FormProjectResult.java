package com.duojuhe.coremodule.form.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("表单项目提交结果表")
@Table(name = "form_project_result")
public class FormProjectResult extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "id")
    @Id
    private String id;

    @ApiModelProperty(value = "提交编号")
    @Column(name = "serial_number")
    private String serialNumber;

    @ApiModelProperty(value = "项目id")
    @Column(name = "project_id")
    private String projectId;

    @ApiModelProperty(value = "项目唯一标识")
    @Column(name = "project_key")
    private String projectKey;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "提交ip")
    @Column(name = "submit_ip")
    private String submitIp;

    @ApiModelProperty(value = "提交用户id")
    @Column(name = "submit_user_id")
    private String submitUserId;

    @ApiModelProperty(value = "创建用户id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "创建租户id")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "提交用户操作系统")
    @Column(name = "submit_os")
    private String submitOs;

    @ApiModelProperty(value = "提交用户浏览器")
    @Column(name = "submit_browser")
    private String submitBrowser;

    @ApiModelProperty(value = "填写结果原始数据")
    @Column(name = "original_data")
    private String originalData;

    @ApiModelProperty(value = "填写结果处理后的数据")
    @Column(name = "process_data")
    private String processData;

    @ApiModelProperty(value = "提交用户客户端信息")
    @Column(name = "submit_ua")
    private String submitUa;

    @ApiModelProperty(value = "总完成时间 毫秒")
    @Column(name = "complete_time")
    private Long completeTime;


    @ApiModelProperty(value = "该记录对应的表单内容")
    @Column(name = "form_content_config")
    private String formContentConfig;

    @ApiModelProperty(value = "该记录对应表单的逻辑配置")
    @Column(name = "form_logic_config")
    private String formLogicConfig;

    @ApiModelProperty(value = "提交来源，取数据字典")
    @Column(name = "submit_source")
    private String submitSource;


}