package com.duojuhe.coremodule.form.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("表单项目")
@Table(name = "form_project")
public class FormProject extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "project_id")
    @Id
    private String projectId;

    @ApiModelProperty(value = "项目唯一标识", required = true)
    @Column(name = "project_key")
    private String projectKey;

    @ApiModelProperty(value = "项目名称")
    @Column(name = "project_name")
    private String projectName;

    @ApiModelProperty(value = "项目类型，取数据字典，问卷项目")
    @Column(name = "project_type_code")
    private String projectTypeCode;

    @ApiModelProperty(value = "项目所属模块，工作流模块，问卷调查模块")
    @Column(name = "project_module_code")
    private String projectModuleCode;

    @ApiModelProperty(value = "封面图片base64格式")
    @Column(name = "cover_img")
    private String coverImg;

    @ApiModelProperty(value = "使用次数")
    @Column(name = "use_count")
    private Integer useCount;

    @ApiModelProperty(value = "分类id")
    @Column(name = "category_id")
    private String categoryId;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户id")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "备注说明")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "状态")
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "描述")
    @Column(name = "description")
    private String description;

    @ApiModelProperty(value = "最大表单项id")
    @Column(name = "max_form_item_id")
    private Long maxFormItemId;

    @ApiModelProperty(value = "最大表单项id")
    @Column(name = "max_form_item_sort")
    private Long maxFormItemSort;

    @ApiModelProperty(value = "浏览点击次数")
    @Column(name = "hits")
    private Long hits;

}