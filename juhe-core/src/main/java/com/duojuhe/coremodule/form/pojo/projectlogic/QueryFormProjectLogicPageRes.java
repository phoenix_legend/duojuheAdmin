package com.duojuhe.coremodule.form.pojo.projectlogic;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryFormProjectLogicPageRes extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String id;

    @ApiModelProperty(value = "项目id")
    private String projectId;

    @ApiModelProperty(value = "项目key")
    private String projectKey;

    @ApiModelProperty(value = "子项目id")
    private String itemId;

    @ApiModelProperty(value = "子项目表单id")
    private String formItemId;

    @ApiModelProperty(value = "表单项类型")
    private String formItemType;

    @ApiModelProperty(value = "条件选项1全部2任意")
    private Integer expression;

    @ApiModelProperty(value = "条件list集合")
    private String conditionJson;

    @ApiModelProperty(value = "条件list集合")
    private List<FormProjectLogicCondition> conditionList;

}