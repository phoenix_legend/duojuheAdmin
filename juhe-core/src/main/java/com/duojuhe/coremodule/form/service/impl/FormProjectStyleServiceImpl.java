package com.duojuhe.coremodule.form.service.impl;

import com.duojuhe.common.annotation.KeyLock;
import com.duojuhe.common.enums.SystemEnum;
import com.duojuhe.common.result.ErrorCodes;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.form.entity.FormProject;
import com.duojuhe.coremodule.form.entity.FormProjectStyle;
import com.duojuhe.coremodule.form.mapper.FormProjectMapper;
import com.duojuhe.coremodule.form.mapper.FormProjectStyleMapper;
import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import com.duojuhe.coremodule.form.pojo.projectstyle.QueryFormProjectStyleRes;
import com.duojuhe.coremodule.form.pojo.projectstyle.UpdateFormProjectStyleReq;
import com.duojuhe.coremodule.form.service.FormProjectStyleService;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.Date;

@Slf4j
@Service
public class FormProjectStyleServiceImpl  extends BaseService implements FormProjectStyleService {
    @Resource
    private FormProjectMapper formProjectMapper;
    @Resource
    private FormProjectStyleMapper formProjectStyleMapper;
    /**
     * 根据项目id查询风格详情
     * @param req
     * @return
     */
    @KeyLock(lockKeyParts = "projectId")
    @Override
    public ServiceResult<QueryFormProjectStyleRes> queryFormProjectStyleResByProjectId(FormProjectIdReq req) {
        String projectId = req.getProjectId();
        QueryFormProjectStyleRes res = formProjectStyleMapper.queryFormProjectStyleResByProjectId(projectId);
        if (res == null) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        return ServiceResult.ok(res);
    }

    /**
     * 【更新】表单项目更新
     * @param req
     * @return
     */
    @Override
    public ServiceResult updateFormProjectStyle(UpdateFormProjectStyleReq req) {
        String projectId = req.getProjectId();
        FormProject formProjectOld = formProjectMapper.selectByPrimaryKey(projectId);
        if (formProjectOld == null) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        FormProjectStyle formProjectStyleOld = formProjectStyleMapper.selectByPrimaryKey(projectId);
        if (formProjectStyleOld == null) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        //是
        Integer YES = SystemEnum.IS_YES.YES.getKey();
        //否
        Integer NO = SystemEnum.IS_YES.NO.getKey();
        //当前登录人id
        String userId = getCurrentLoginUserId();
        //当前时间
        Date nowDate = new Date();
        FormProjectStyle updateFormProjectStyle = new FormProjectStyle();
        updateFormProjectStyle.setProjectId(projectId);
        updateFormProjectStyle.setLogoImage(req.getLogoImage());
        updateFormProjectStyle.setLogoPosition(req.getLogoPosition());
        updateFormProjectStyle.setBackgroundColor(req.getBackgroundColor());
        updateFormProjectStyle.setBackgroundImg(req.getBackgroundImg());
        updateFormProjectStyle.setUpdateUserId(userId);
        updateFormProjectStyle.setUpdateTime(nowDate);
        updateFormProjectStyle.setShowDescribe(req.getShowDescribe()?YES:NO);
        updateFormProjectStyle.setShowNumber(req.getShowNumber()?YES:NO);
        updateFormProjectStyle.setShowTitle(req.getShowTitle()?YES:NO);
        updateFormProjectStyle.setSubmitButtonText(req.getSubmitButtonText());
        updateFormProjectStyle.setButtonColor(req.getButtonColor());
        updateFormProjectStyle.setHeadImage(req.getHeadImage());
        updateFormProjectStyle.setResetButtonColor(req.getResetButtonColor());
        updateFormProjectStyle.setResetButtonText(req.getResetButtonText());
        updateFormProjectStyle.setShowResetButton(req.getShowResetButton()?YES:NO);
        formProjectStyleMapper.updateByPrimaryKeySelective(updateFormProjectStyle);
        return ServiceResult.ok(ErrorCodes.SUCCESS);
    }
}
