package com.duojuhe.coremodule.form.mapper;


import com.duojuhe.coremodule.form.entity.FormCategory;
import com.duojuhe.coremodule.form.pojo.category.SelectFormCategoryRes;
import com.tkmapper.TkMapper;

import java.util.List;

public interface FormCategoryMapper extends TkMapper<FormCategory> {

    /**
     * 根据条件查询所有可供前端选择使用的表单分类
     *
     * @return
     */
    List<SelectFormCategoryRes> queryNormalSelectFormCategoryResList();
}