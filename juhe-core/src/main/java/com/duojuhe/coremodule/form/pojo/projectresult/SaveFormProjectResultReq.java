package com.duojuhe.coremodule.form.pojo.projectresult;

import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Map;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveFormProjectResultReq extends FormProjectIdReq {

    @ApiModelProperty(value = "填写结果原始数据",required = true)
    @NotNull(message = "填写结果原始数据不能为空")
    private Map<String, Object> originalData;

    @ApiModelProperty(value = "填写结果处理后的数据" ,required = true)
    @NotNull(message = "填写结果处理后的数据不能为空")
    private Map<String, Object> processData;

    @ApiModelProperty(value = "填写用户Ua" ,required = true)
    private Map<String, Object> submitUa;

    @ApiModelProperty(value = "提交系统" ,required = true)
    @Length(max = 255, message = "提交系统不得超过{max}位字符")
    @NotBlank(message = "提交系统不能为空")
    private String submitOs;

    @ApiModelProperty(value = "提交浏览器" ,required = true)
    @Length(max = 255, message = "提交浏览器不得超过{max}位字符")
    @NotBlank(message = "提交浏览器不能为空")
    private String submitBrowser;

    @ApiModelProperty(value = "总完成时间 毫秒",required = true)
    @NotNull(message = "总完成时间不能为空")
    @Range(min = 0, max = Integer.MAX_VALUE, message = "总完成时间值不正确")
    private Long completeTime;
}