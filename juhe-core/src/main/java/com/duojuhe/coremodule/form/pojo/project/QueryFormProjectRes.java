package com.duojuhe.coremodule.form.pojo.project;

import com.duojuhe.common.bean.BaseBean;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryFormProjectRes extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String projectId;

    @ApiModelProperty(value = "项目唯一标识", required = true)
    private String projectKey;

    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "浏览点击次数")
    private Long hits;

    @ApiModelProperty(value = "项目类型，取数据字典，问卷项目")
    private String projectTypeCode;

    @ApiModelProperty(value = "项目所属模块，工作流模块，问卷调查模块")
    private String projectModuleCode;

    @ApiModelProperty(value = "分类id")
    private String categoryId;

    @ApiModelProperty(value = "封面图片base64格式")
    private String coverImg;

    @ApiModelProperty(value = "使用次数")
    private Integer useCount;

    @ApiModelProperty(value = "备注说明")
    private String remark;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "最大表单id")
    private String maxFormItemId;

    @ApiModelProperty(value = "状态 FORM_PROJECT_UNPUBLISHED 未发布 FORM_PROJECT_PUBLISHED 已发布")
    private String statusCode;

    @ApiModelProperty(value = "排序")
    private Integer sort;


}