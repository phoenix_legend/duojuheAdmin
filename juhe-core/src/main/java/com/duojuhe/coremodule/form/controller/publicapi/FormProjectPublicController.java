package com.duojuhe.coremodule.form.controller.publicapi;

import com.duojuhe.common.annotation.RepeatSubmit;
import com.duojuhe.common.file.service.FileUploadService;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import com.duojuhe.coremodule.form.pojo.project.QueryFormProjectDetailsRes;
import com.duojuhe.coremodule.form.pojo.project.QueryFormProjectRes;
import com.duojuhe.coremodule.form.pojo.projectitem.QueryFormProjectItemTableHeadRes;
import com.duojuhe.coremodule.form.pojo.projectlogic.QueryFormProjectLogicPageRes;
import com.duojuhe.coremodule.form.pojo.projectresult.QueryFormProjectResultPageRes;
import com.duojuhe.coremodule.form.pojo.projectresult.QueryPublicFormProjectResultPageReq;
import com.duojuhe.coremodule.form.pojo.projectresult.SaveFormProjectResultReq;
import com.duojuhe.coremodule.form.pojo.projectsetting.QueryPublicFormProjectSettingRes;
import com.duojuhe.coremodule.form.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/formProjectPublic/")
@Api(tags = {"【表单项目公共开放接口】表单项目公共开放接口管理相关接口"})
@Slf4j
public class FormProjectPublicController {
    @Resource
    private FileUploadService fileUploadService;
    @Resource
    public FormProjectPublicService formProjectPublicService;

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "单图片上传返回base64图片格式")
    @PostMapping(value = "/upload/imageSingleUploadToImageBase64")
    @ResponseBody
    public ServiceResult<String> imageSingleUploadToImageBase64(@RequestParam("file") MultipartFile file) {
        return fileUploadService.imageSingleUploadToImageBase64(file);
    }

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "单文件上传返回文件绝对路径")
    @PostMapping(value = "/upload/fileSingleUploadToFileAbsolutePath")
    @ResponseBody
    public ServiceResult<String> fileSingleUploadToFileAbsolutePath(@RequestParam("file") MultipartFile file) {
        return fileUploadService.fileSingleUploadToFileAbsolutePath(file);
    }


    @RepeatSubmit
    @ApiOperation(value = "【保存】公开保存表单结果值")
    @PostMapping(value = "savePublicFormProjectResult")
    public ServiceResult<String> savePublicFormProjectResult(@Valid @RequestBody @ApiParam(value = "入参类") SaveFormProjectResultReq req) {
        return formProjectPublicService.savePublicFormProjectResult(req);
    }


    @ApiOperation(value = "【项目详情】根据项目key查询详情")
    @PostMapping(value = "queryPublicFormProjectResByProjectId")
    public ServiceResult<QueryFormProjectRes> queryPublicFormProjectResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectPublicService.queryPublicFormProjectResByProjectId(req);
    }



    @ApiOperation(value = "【项目明细查询】根据项目id查询项目明细")
    @PostMapping(value = "queryPublicFormProjectDetailsResByProjectId")
    public ServiceResult<QueryFormProjectDetailsRes> queryPublicFormProjectDetailsResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectPublicService.queryPublicFormProjectDetailsResByProjectId(req);
    }


    @RepeatSubmit
    @ApiOperation(value = "【项目设置详情】根据项目ID查询项目设置详情")
    @PostMapping(value = "queryPublicFormProjectSettingResByProjectId")
    public ServiceResult<QueryPublicFormProjectSettingRes> queryPublicFormProjectSettingResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectPublicService.queryPublicFormProjectSettingResByProjectId(req);
    }


    @ApiOperation(value = "【项目逻辑集合】根据项目id查询项目逻辑集合")
    @PostMapping(value = "queryPublicFormProjectLogicPageResByProjectId")
    public ServiceResult<List<QueryFormProjectLogicPageRes>> queryPublicFormProjectLogicPageResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectPublicService.queryPublicFormProjectLogicPageResByProjectId(req);
    }


    @ApiOperation(value = "【项目表头信息】根据项目Id查询表单项目的表头信息")
    @PostMapping(value = "queryPublicFormProjectItemTableHeadResByProjectId")
    public ServiceResult<List<QueryFormProjectItemTableHeadRes>> queryPublicFormProjectItemTableHeadResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectPublicService.queryPublicFormProjectItemTableHeadResByProjectId(req);
    }


    @ApiOperation(value = "【分页查询填写结果】根据条件查询表单项目提交结果集合")
    @PostMapping(value = "queryPublicFormProjectResultPageResList")
    public ServiceResult<PageResult<List<QueryFormProjectResultPageRes>>> queryPublicFormProjectResultPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryPublicFormProjectResultPageReq req) {
        return formProjectPublicService.queryPublicFormProjectResultPageResList(req);
    }

}
