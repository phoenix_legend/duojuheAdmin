package com.duojuhe.coremodule.form.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("表单项目风格显示表")
@Table(name = "form_project_style")
public class FormProjectStyle extends BaseBean {
    @ApiModelProperty(value = "主键id", required = true)
    @Column(name = "project_id")
    @Id
    private String projectId;

    @ApiModelProperty(value = "项目唯一标识")
    @Column(name = "project_key")
    private String projectKey;

    @ApiModelProperty(value = "logo图片")
    @Column(name = "logo_image")
    private String logoImage;

    @ApiModelProperty(value = "logo位置")
    @Column(name = "logo_position")
    private String logoPosition;

    @ApiModelProperty(value = "背景颜色")
    @Column(name = "background_color")
    private String backgroundColor;

    @ApiModelProperty(value = "按钮颜色")
    @Column(name = "button_color")
    private String buttonColor;


    @ApiModelProperty(value = "提交按钮文字")
    @Column(name = "submit_button_text")
    private String submitButtonText;

    @ApiModelProperty(value = "头部图片")
    @Column(name = "head_image")
    private String headImage;

    @ApiModelProperty(value = "背景图片")
    @Column(name = "background_img")
    private String backgroundImg;

    @ApiModelProperty(value = "是否显示标题")
    @Column(name = "show_title")
    private Integer showTitle;

    @ApiModelProperty(value = "是否显示描述语")
    @Column(name = "show_describe")
    private Integer showDescribe;

    @ApiModelProperty(value = "是否显示序号")
    @Column(name = "show_number")
    private Integer showNumber;

    @ApiModelProperty(value = "是否显示重置按钮")
    @Column(name = "show_reset_button")
    private Integer showResetButton;

    @ApiModelProperty(value = "重置按钮文字")
    @Column(name = "reset_button_text")
    private String resetButtonText;

    @ApiModelProperty(value = "重置按钮颜色")
    @Column(name = "reset_button_color")
    private String resetButtonColor;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "create_user_id")
    private String createUserId;
}