package com.duojuhe.coremodule.form.mapper;


import com.duojuhe.coremodule.form.entity.FormProjectTheme;
import com.duojuhe.coremodule.form.pojo.projecttheme.QueryFormProjectThemePageReq;
import com.duojuhe.coremodule.form.pojo.projecttheme.QueryFormProjectThemePageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FormProjectThemeMapper extends TkMapper<FormProjectTheme> {
    /**
     * 分页查询 根据条件查询表单主题list
     *
     * @return
     */
    List<QueryFormProjectThemePageRes> queryFormProjectThemePageResList(@Param("req") QueryFormProjectThemePageReq req);

}