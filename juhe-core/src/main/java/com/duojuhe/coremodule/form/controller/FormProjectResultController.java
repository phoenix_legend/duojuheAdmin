package com.duojuhe.coremodule.form.controller;

import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import com.duojuhe.coremodule.form.pojo.projectresult.*;
import com.duojuhe.coremodule.form.pojo.projectresult.report.QueryProjectResultReportDeviceRes;
import com.duojuhe.coremodule.form.pojo.projectresult.report.QueryProjectResultReportSituationRes;
import com.duojuhe.coremodule.form.pojo.projectresult.report.QueryProjectResultReportSourceRes;
import com.duojuhe.coremodule.form.pojo.projectresult.report.QueryProjectResultReportStatsRes;
import com.duojuhe.coremodule.form.service.FormProjectResultService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/formProjectResult/")
@Api(tags = {"【表单项目提交结果】表单项目提交结果管理相关接口"})
@Slf4j
public class FormProjectResultController {

    @Resource
    public FormProjectResultService formProjectResultService;

    @ApiOperation(value = "【分页查询】根据条件查询表单项目提交结果集合")
    @PostMapping(value = "queryFormProjectResultPageResList")
    public ServiceResult<PageResult<List<QueryFormProjectResultPageRes>>> queryFormProjectResultPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryFormProjectResultPageReq req) {
        return formProjectResultService.queryFormProjectResultPageResList(req);
    }

    @ApiOperation(value = "【查询项目收集设备分析】根据项目id查询提交来源统计分析")
    @PostMapping(value = "queryProjectResultReportDeviceResByProjectId")
    public ServiceResult<List<QueryProjectResultReportDeviceRes>> queryProjectResultReportDeviceResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectResultService.queryProjectResultReportDeviceResByProjectId(req);
    }


    @ApiOperation(value = "【项目收集情况 按周查看】根据项目id查询项目收集情况 按周查看分析")
    @PostMapping(value = "queryProjectResultReportSituationResByProjectId")
    public ServiceResult<List<QueryProjectResultReportSituationRes>> queryProjectResultReportSituationResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectResultService.queryProjectResultReportSituationResByProjectId(req);
    }


    @ApiOperation(value = "【查询项目收集来源分析】根据项目id查询项目收集来源分析")
    @PostMapping(value = "queryProjectResultReportSourceResByProjectId")
    public ServiceResult<List<QueryProjectResultReportSourceRes>> queryProjectResultReportSourceResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectResultService.queryProjectResultReportSourceResByProjectId(req);
    }

    @ApiOperation(value = "【查询项目收集信息】根据项目id查询项目收集信息")
    @PostMapping(value = "queryProjectResultReportStatsResByProjectId")
    public ServiceResult<QueryProjectResultReportStatsRes> queryProjectResultReportStatsResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectResultService.queryProjectResultReportStatsResByProjectId(req);
    }


    @ApiOperation(value = "【保存】后台新增问卷记录")
    @PostMapping(value = "saveFormProjectResult")
    public ServiceResult<String> saveFormProjectResult(@Valid @RequestBody @ApiParam(value = "入参类") SaveFormProjectResultReq req) {
        return formProjectResultService.saveFormProjectResult(req);
    }

    @ApiOperation(value = "【修改】后台修改问卷记录")
    @PostMapping(value = "updateFormProjectResult")
    public ServiceResult<String> updateFormProjectResult(@Valid @RequestBody @ApiParam(value = "入参类") UpdateFormProjectResultReq req) {
        return formProjectResultService.updateFormProjectResult(req);
    }

    @ApiOperation(value = "【删除】后台删除问卷记录")
    @PostMapping(value = "deleteFormProjectResultById")
    public ServiceResult deleteFormProjectResultById(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectResultIdReq req) {
        return formProjectResultService.deleteFormProjectResultById(req);
    }

    @ApiOperation(value = "【查询问卷记录编辑初始值】根据记录id查询查询记录编辑初始值明细")
    @PostMapping(value = "queryFormProjectResultDetailsEditResById")
    public ServiceResult<QueryFormProjectResultDetailsEditRes> queryFormProjectResultDetailsEditResById(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectResultIdReq req) {
        return formProjectResultService.queryFormProjectResultDetailsEditResById(req);
    }

}
