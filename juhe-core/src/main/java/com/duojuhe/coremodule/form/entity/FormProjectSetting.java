package com.duojuhe.coremodule.form.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("表单项目设置表")
@Table(name = "form_project_setting")
public class FormProjectSetting extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "project_id")
    @Id
    private String projectId;

    @ApiModelProperty(value = "项目唯一标识", required = true)
    @Column(name = "project_key")
    private String projectKey;

    @ApiModelProperty(value = "提交提示图片")
    @Column(name = "submit_prompt_img")
    private String submitPromptImg;

    @ApiModelProperty(value = "提交提示文字")
    @Column(name = "submit_prompt_text")
    private String submitPromptText;

    @ApiModelProperty(value = "提交后跳转地址")
    @Column(name = "submit_jump_url")
    private String submitJumpUrl;

    @ApiModelProperty(value = "是否公开提交结果")
    @Column(name = "result_public")
    private Integer resultPublic;

    @ApiModelProperty(value = "是否限制填写次数")
    @Column(name = "limit_write")
    private Integer limitWrite;

    @ApiModelProperty(value = "每人允许填写次数")
    @Column(name = "everyone_write_num")
    private Integer everyoneWriteNum;

    @ApiModelProperty(value = "限制填写次数方式，1=总共,2=每天")
    @Column(name = "limit_write_num_mode")
    private String limitWriteNumMode;

    @ApiModelProperty(value = "分享图片")
    @Column(name = "share_img")
    private String shareImg;

    @ApiModelProperty(value = "分享标题")
    @Column(name = "share_title")
    private String shareTitle;

    @ApiModelProperty(value = "分享描述")
    @Column(name = "share_desc")
    private String shareDesc;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "显示定时收集")
    @Column(name = "show_timed_collection")
    private Integer showTimedCollection;

    @ApiModelProperty(value = "定时收集开始时间")
    @Column(name = "timed_collection_begin_time")
    private Date timedCollectionBeginTime;

    @ApiModelProperty(value = "定时收集结束时间")
    @Column(name = "timed_collection_end_time")
    private Date timedCollectionEndTime;

    @ApiModelProperty(value = "定时未启动提示文字")
    @Column(name = "timed_not_enabled_prompt_text")
    private String timedNotEnabledPromptText;

    @ApiModelProperty(value = "定时停用会提示文字")
    @Column(name = "timed_deactivate_prompt_text")
    private String timedDeactivatePromptText;

    @ApiModelProperty(value = "显示定量收集")
    @Column(name = "show_quantitative")
    private Integer showQuantitative;

    @ApiModelProperty(value = "定量收集总数量")
    @Column(name = "quantitative_total_quantity")
    private Integer quantitativeTotalQuantity;

    @ApiModelProperty(value = "定量收集完成提示文字")
    @Column(name = "quantitative_end_prompt_text")
    private String quantitativeEndPromptText;
}