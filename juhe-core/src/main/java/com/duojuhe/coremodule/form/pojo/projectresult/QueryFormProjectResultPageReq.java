package com.duojuhe.coremodule.form.pojo.projectresult;

import cn.hutool.core.util.StrUtil;
import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.duojuhe.common.utils.jsonutils.JsonUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryFormProjectResultPageReq extends PageHead {

    @ApiModelProperty(value = "项目ID", required = true)
    @NotBlank(message = "项目ID不可为空")
    private String projectId;

    @ApiModelProperty(value = "提交编号")
    private String serialNumber;

    @ApiModelProperty(value = "开始日期yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }


    /**
     * 动态字段 参数json
     */
    private String extParams;
    /**
     * 动态字段比较符 参数json
     */
    private String extComparisons;

    public Map<String, Object> getExtParamsMap() {
        if (StrUtil.isNotEmpty(extParams)) {
            return JsonUtils.jsonToMap(extParams);
        }
        return null;
    }

    public Map<String, Object> getExtComparisonsMap() {
        if (StrUtil.isNotEmpty(extComparisons)) {
            return JsonUtils.jsonToMap(extComparisons);
        }
        return null;
    }



    @Getter
    @AllArgsConstructor
    public enum QueryComparison {
        EQ("eq", "="),
        LIKE("like", "like");
        private String name;
        private String key;

        public static QueryComparison get(String name) {
            return Arrays.stream(QueryComparison.values()).filter(item -> item.name.equals(name)).findFirst().get();
        }
    }
}