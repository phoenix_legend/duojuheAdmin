package com.duojuhe.coremodule.form.controller;

import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import com.duojuhe.coremodule.form.pojo.projectstyle.QueryFormProjectStyleRes;
import com.duojuhe.coremodule.form.pojo.projectstyle.UpdateFormProjectStyleReq;
import com.duojuhe.coremodule.form.service.FormProjectStyleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/formProjectStyle/")
@Api(tags = {"【表单项目风格】表单项目风格管理相关接口"})
@Slf4j
public class FormProjectStyleController {
    @Resource
    public FormProjectStyleService formProjectStyleService;

    @ApiOperation(value = "【修改】表单项目风格")
    @PostMapping(value = "updateFormProjectStyle")
    public ServiceResult updateFormProjectStyle(@Valid @RequestBody @ApiParam(value = "入参类") UpdateFormProjectStyleReq req) {
        return formProjectStyleService.updateFormProjectStyle(req);
    }

    @ApiOperation(value = "【查询详情】根据项目ID查询风格详情")
    @PostMapping(value = "queryFormProjectStyleResByProjectId")
    public ServiceResult<QueryFormProjectStyleRes> queryFormProjectStyleResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectStyleService.queryFormProjectStyleResByProjectId(req);
    }
}
