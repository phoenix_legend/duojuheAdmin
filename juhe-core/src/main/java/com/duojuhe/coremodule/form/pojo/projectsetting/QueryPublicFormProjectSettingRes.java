package com.duojuhe.coremodule.form.pojo.projectsetting;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.coremodule.form.pojo.project.QueryFormProjectRes;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryPublicFormProjectSettingRes extends BaseBean {
    @ApiModelProperty(value = "项目详情")
    private QueryFormProjectRes formProjectRes;

    @ApiModelProperty(value = "项目设置项详情")
    private QueryFormProjectSettingRes formProjectSettingRes;

    @ApiModelProperty(value = "项目显示状态")
    private String writeShowStatus;

    @ApiModelProperty(value = "项目显示提示文字")
    private String writeShowMessage;
}
