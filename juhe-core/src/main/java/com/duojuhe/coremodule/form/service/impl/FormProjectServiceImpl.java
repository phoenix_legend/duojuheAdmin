package com.duojuhe.coremodule.form.service.impl;


import com.duojuhe.cache.SystemDictCache;
import com.duojuhe.common.annotation.DataScopeFilter;
import com.duojuhe.common.annotation.KeyLock;
import com.duojuhe.common.bean.UserTokenInfoVo;
import com.duojuhe.common.enums.SystemEnum;
import com.duojuhe.coremodule.form.enums.FormEnum;
import com.duojuhe.common.result.ErrorCodes;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.encryption.md5.MD5Util;
import com.duojuhe.common.utils.idgenerator.UUIDUtils;
import com.duojuhe.common.utils.page.PageHelperUtil;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.form.entity.*;
import com.duojuhe.coremodule.form.mapper.*;
import com.duojuhe.coremodule.form.pojo.project.*;
import com.duojuhe.coremodule.form.pojo.projectitem.QueryFormProjectItemRes;
import com.duojuhe.coremodule.form.pojo.projectstyle.QueryFormProjectStyleRes;
import com.duojuhe.coremodule.form.service.FormProjectService;
import com.duojuhe.coremodule.form.utils.handle.HandleFormUtils;
import com.duojuhe.coremodule.system.entity.SystemDict;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class FormProjectServiceImpl extends BaseService implements FormProjectService {
    @Resource
    private SystemDictCache systemDictCache;
    @Resource
    private FormProjectMapper formProjectMapper;
    @Resource
    private FormProjectItemMapper formProjectItemMapper;
    @Resource
    private FormProjectStyleMapper formProjectStyleMapper;
    @Resource
    private FormProjectSettingMapper formProjectSettingMapper;
    @Resource
    private FormProjectLogicMapper formProjectLogicMapper;
    /**
     * 【分页查询】根据条件查询表单项目list
     * @param req
     * @return
     */
    @DataScopeFilter
    @Override
    public ServiceResult<PageResult<List<QueryFormProjectPageRes>>> queryFormProjectPageResList(QueryFormProjectPageReq req) {
        //查询指定模块
        String projectModule = FormEnum.FormProjectModule.FORM_SURVEY_PROJECT.getKey();
        PageHelperUtil.orderByAndStartPage(req, "sort desc,updateTime desc, projectId desc");
        List<QueryFormProjectPageRes> list = formProjectMapper.queryFormProjectPageResList(req,projectModule,null);
        for (QueryFormProjectPageRes res:list){
            SystemDict statusCode = systemDictCache.getSystemDictByDictCode(res.getStatusCode());
            res.setStatusName(statusCode.getDictName());
            res.setStatusColor(statusCode.getDictColor());
        }
        return PageHelperUtil.returnServiceResult(req, list);
    }
    /**
     * 【保存】表单项目
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServiceResult saveFormProject(SaveFormProjectReq req) {
        //获取封面
        String coverImg = req.getCoverImg();
        //项目id
        String projectId = UUIDUtils.getUUID32();
        //项目key
        String projectKey = UUIDUtils.getSequenceNo();
        //当前登录人
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //当前登录人id
        String userId = userTokenInfoVo.getUserId();
        //指定模块
        String projectModule = FormEnum.FormProjectModule.FORM_SURVEY_PROJECT.getKey();
        //是
        Integer YES = SystemEnum.IS_YES.YES.getKey();
        //否
        Integer NO = SystemEnum.IS_YES.NO.getKey();
        //当前时间
        Date nowDate = new Date();
        FormProject formProject = new FormProject();
        formProject.setProjectId(projectId);
        formProject.setProjectKey(projectKey);
        formProject.setCoverImg(coverImg);
        formProject.setProjectName(req.getProjectName());
        formProject.setRemark(req.getRemark());
        formProject.setSort(req.getSort());
        formProject.setCreateTime(nowDate);
        formProject.setCreateUserId(userId);
        formProject.setUpdateTime(nowDate);
        formProject.setUpdateUserId(userId);
        formProject.setCreateDeptId(userTokenInfoVo.getCreateDeptId());
        formProject.setTenantId(userTokenInfoVo.getTenantId());
        formProject.setStatusCode(FormEnum.FormProjectStatus.FORM_PROJECT_UNPUBLISHED.getKey());
        formProject.setDescription(req.getDescription());
        formProject.setMaxFormItemId(1L);
        formProject.setMaxFormItemSort(1L);
        formProject.setHits(0L);
        formProject.setProjectModuleCode(projectModule);
        formProject.setProjectTypeCode(FormEnum.FormProjectType.FORM_SURVEY_QUESTIONNAIRE.getKey());
        //构建表单风格对象
        FormProjectStyle formProjectStyle = new FormProjectStyle();
        formProjectStyle.setProjectId(projectId);
        formProjectStyle.setProjectKey(projectKey);
        formProjectStyle.setCreateTime(nowDate);
        formProjectStyle.setCreateUserId(userId);
        formProjectStyle.setUpdateUserId(userId);
        formProjectStyle.setUpdateTime(nowDate);
        formProjectStyle.setShowDescribe(YES);
        formProjectStyle.setShowNumber(YES);
        formProjectStyle.setShowTitle(YES);
        //表单基础设置对象
        FormProjectSetting formProjectSetting = new FormProjectSetting();
        formProjectSetting.setProjectId(projectId);
        formProjectSetting.setProjectKey(projectKey);
        formProjectSetting.setCreateTime(nowDate);
        formProjectSetting.setCreateUserId(userId);
        formProjectSetting.setUpdateUserId(userId);
        formProjectSetting.setUpdateTime(nowDate);
        formProjectSetting.setShowQuantitative(NO);
        formProjectSetting.setShowTimedCollection(NO);
        formProjectSetting.setLimitWrite(NO);
        formProjectSetting.setResultPublic(NO);
        //插入项目表
        formProjectMapper.insertSelective(formProject);
        //插入项目风格表
        formProjectStyleMapper.insertSelective(formProjectStyle);
        //插入项目设置表
        formProjectSettingMapper.insertSelective(formProjectSetting);
        return ServiceResult.ok(projectId);
    }

    /**
     * 【保存】根据项目模板id创建一个新的表单项目
     * @param req
     * @return
     */
    @KeyLock(lockKeyParts = "projectId")
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServiceResult createFormProjectByTemplate(CreateFormProjectByTemplateReq req) {
        //模板项目id
        String templateProjectId = req.getProjectId();
        //查询项目是否存在
        FormProject formProject = formProjectMapper.selectByPrimaryKey(templateProjectId);
        if (formProject == null || !FormEnum.FormProjectModule.FORM_SURVEY_TEMPLATE.getKey().equals(formProject.getProjectModuleCode())) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        //模板项目使用点击率
        Long templateProjectHits = formProject.getHits();

        //查询项目风格
        FormProjectStyle formProjectStyle = formProjectStyleMapper.selectByPrimaryKey(templateProjectId);
        if (formProjectStyle == null) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //当前登录人id
        String userId = userTokenInfoVo.getUserId();
        //指定模块
        String projectModule = FormEnum.FormProjectModule.FORM_SURVEY_PROJECT.getKey();
        //当前时间
        Date nowDate = new Date();
        //项目id
        String projectId = UUIDUtils.getUUID32();
        //项目模板key
        String projectKey = UUIDUtils.getSequenceNo();
        //项目名称
        String projectName = req.getProjectName();
        //是
        Integer YES = SystemEnum.IS_YES.YES.getKey();
        //否
        Integer NO = SystemEnum.IS_YES.NO.getKey();
        //插入项目模板表
        formProject.setProjectName(projectName);
        formProject.setProjectId(projectId);
        formProject.setProjectKey(projectKey);
        formProject.setSort(0);
        formProject.setCreateTime(nowDate);
        formProject.setCreateUserId(userId);
        formProject.setUpdateTime(nowDate);
        formProject.setUpdateUserId(userId);
        formProject.setCreateDeptId(userTokenInfoVo.getCreateDeptId());
        formProject.setTenantId(userTokenInfoVo.getTenantId());
        formProject.setStatusCode(FormEnum.FormProjectStatus.FORM_PROJECT_UNPUBLISHED.getKey());
        formProject.setHits(0L);
        formProject.setProjectModuleCode(projectModule);
        formProject.setDescription(req.getDescription());
        formProject.setProjectTypeCode(FormEnum.FormProjectType.FORM_SURVEY_QUESTIONNAIRE.getKey());
        formProjectMapper.insertSelective(formProject);
        //更新原来模板项目的使用次数
        FormProject updateTemplateProject = new FormProject();
        updateTemplateProject.setProjectId(templateProjectId);
        updateTemplateProject.setHits(templateProjectHits+1);
        formProjectMapper.updateByPrimaryKeySelective(updateTemplateProject);
        //表单基础设置对象
        FormProjectSetting formProjectSetting = new FormProjectSetting();
        formProjectSetting.setProjectId(projectId);
        formProjectSetting.setProjectKey(projectKey);
        formProjectSetting.setCreateTime(nowDate);
        formProjectSetting.setCreateUserId(userId);
        formProjectSetting.setUpdateUserId(userId);
        formProjectSetting.setUpdateTime(nowDate);
        formProjectSetting.setShowQuantitative(NO);
        formProjectSetting.setShowTimedCollection(NO);
        formProjectSetting.setLimitWrite(NO);
        formProjectSetting.setResultPublic(NO);
        //插入项目设置表
        formProjectSettingMapper.insertSelective(formProjectSetting);
        //插入项目模板风格表
        formProjectStyle.setProjectId(projectId);
        formProjectStyle.setProjectKey(projectKey);
        formProjectStyle.setCreateTime(nowDate);
        formProjectStyle.setCreateUserId(userId);
        formProjectStyle.setUpdateUserId(userId);
        formProjectStyle.setUpdateTime(nowDate);
        formProjectStyleMapper.insertSelective(formProjectStyle);
        //构建子项目
        List<FormProjectItem>  formProjectItemList = formProjectItemMapper.queryFormProjectItemListByProjectId(templateProjectId);
        if (!formProjectItemList.isEmpty()){
            for (FormProjectItem formProjectItem:formProjectItemList){
                //itemId 是有项目id表单id md5组成
                String itemId = MD5Util.getMD532(projectId+formProjectItem.getFormItemId());
                formProjectItem.setItemId(itemId);
                formProjectItem.setUpdateTime(nowDate);
                formProjectItem.setCreateTime(nowDate);
                formProjectItem.setProjectId(projectId);
                formProjectItem.setProjectKey(projectKey);
            }
            formProjectItemMapper.batchInsertListUseAllCols(formProjectItemList);
        }
        //构建显示逻辑
        List<FormProjectLogic> formProjectLogicList = formProjectLogicMapper.queryFormProjectLogicListByProjectId(templateProjectId);
        if (!formProjectLogicList.isEmpty()){
            for (FormProjectLogic formProjectLogic:formProjectLogicList){
                String itemId = MD5Util.getMD532(projectId+formProjectLogic.getFormItemId());
                formProjectLogic.setId(itemId);
                formProjectLogic.setProjectId(projectId);
                formProjectLogic.setProjectKey(projectKey);
                formProjectLogic.setItemId(itemId);
                formProjectLogic.setCreateTime(nowDate);
                formProjectLogic.setUpdateTime(nowDate);
            }
            formProjectLogicMapper.batchInsertListUseAllCols(formProjectLogicList);
        }
        return ServiceResult.ok(projectId);
    }


    /**
     * 【修改】表单项目
     * @param req
     * @return
     */
    @KeyLock(lockKeyParts = "projectId")
    @Override
    public ServiceResult updateFormProject(UpdateFormProjectReq req) {
        //查询指定模块
        String projectModule = FormEnum.FormProjectModule.FORM_SURVEY_PROJECT.getKey();
        //项目id
        String projectId = req.getProjectId();
        FormProject formProjectOld = formProjectMapper.selectByPrimaryKey(projectId);
        if (formProjectOld == null || !projectModule.equals(formProjectOld.getProjectModuleCode())) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        //获取封面
        String coverImg = req.getCoverImg();
        //当前登录人id
        String userId = getCurrentLoginUserId();
        //当前时间
        Date nowDate = new Date();
        FormProject formProject = new FormProject();
        formProject.setProjectId(projectId);
        formProject.setProjectName(req.getProjectName());
        formProject.setCoverImg(coverImg);
        formProject.setRemark(req.getRemark());
        formProject.setSort(req.getSort());
        formProject.setUpdateTime(nowDate);
        formProject.setUpdateUserId(userId);
        formProject.setDescription(req.getDescription());
        formProjectMapper.updateByPrimaryKeySelective(formProject);
        return ServiceResult.ok(projectId);
    }

    /**
     * 【更新发布状态】表单项目更新发布状态
     * @param req
     * @return
     */
    @KeyLock(lockKeyParts = "projectId")
    @Override
    public ServiceResult updateFormProjectStatusByProjectId(FormProjectIdReq req) {
        //查询指定模块
        String projectModule = FormEnum.FormProjectModule.FORM_SURVEY_PROJECT.getKey();
        //项目id
        String projectId = req.getProjectId();
        FormProject formProjectOld = formProjectMapper.selectByPrimaryKey(projectId);
        if (formProjectOld == null || !projectModule.equals(formProjectOld.getProjectModuleCode())) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        //表单项目状态
        String oldStatus = formProjectOld.getStatusCode();
        //停止了
        String stop = FormEnum.FormProjectStatus.FORM_PROJECT_STOP.getKey();
        if (stop.equals(oldStatus)){
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        //未发布 待更新状态
        String newStatus = FormEnum.FormProjectStatus.FORM_PROJECT_UNPUBLISHED.getKey();
        if (newStatus.equals(oldStatus)){
            newStatus = FormEnum.FormProjectStatus.FORM_PROJECT_PUBLISHED.getKey();
        }
        FormProject updateFormProject = new FormProject();
        updateFormProject.setProjectId(projectId);
        updateFormProject.setStatusCode(newStatus);
        formProjectMapper.updateByPrimaryKeySelective(updateFormProject);
        return ServiceResult.ok(projectId);
    }

    /**
     * 【删除】根据表单项目id删除项目
     * @param req
     * @return
     */
    @KeyLock(lockKeyParts = "projectId")
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServiceResult deleteFormProjectByProjectId(FormProjectIdReq req) {
        //查询指定模块
        String projectModule = FormEnum.FormProjectModule.FORM_SURVEY_PROJECT.getKey();
        //项目id
        String projectId = req.getProjectId();
        FormProject formProjectOld = formProjectMapper.selectByPrimaryKey(projectId);
        if (formProjectOld == null || !projectModule.equals(formProjectOld.getProjectModuleCode())) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        //删除项目
        formProjectMapper.deleteByPrimaryKey(projectId);
        //删除项目风格表
        formProjectStyleMapper.deleteByPrimaryKey(projectId);
        //删除项目设置表
        formProjectSettingMapper.deleteByPrimaryKey(projectId);
        return ServiceResult.ok(ErrorCodes.SUCCESS);
    }


    /**
     * 根据项目key查询详情
     * @param req
     * @return
     */
    @Override
    @KeyLock(lockKeyParts = "projectId")
    public ServiceResult<QueryFormProjectRes> queryFormProjectResByProjectId(FormProjectIdReq req) {
        //查询指定模块
        String projectModule = FormEnum.FormProjectModule.FORM_SURVEY_PROJECT.getKey();
        //项目id
        String projectId = req.getProjectId();
        QueryFormProjectRes res = formProjectMapper.queryFormProjectResByProjectId(projectId);
        if (res == null  || !projectModule.equals(res.getProjectModuleCode())) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        return ServiceResult.ok(res);
    }

    /**
     * 根据项目id查询项目明细
     * @param req
     * @return
     */
    @Override
    @KeyLock(lockKeyParts = "projectId")
    public ServiceResult<QueryFormProjectDetailsRes> queryFormProjectDetailsResByProjectId(FormProjectIdReq req) {
        //查询指定模块
        String projectModule = FormEnum.FormProjectModule.FORM_SURVEY_PROJECT.getKey();
        //项目id
        String projectId = req.getProjectId();
        QueryFormProjectRes projectRes = formProjectMapper.queryFormProjectResByProjectId(projectId);
        if (projectRes == null  || !projectModule.equals(projectRes.getProjectModuleCode())) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        QueryFormProjectStyleRes styleRes = formProjectStyleMapper.queryFormProjectStyleResByProjectId(projectId);
        if (styleRes == null) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        QueryFormProjectDetailsRes detailsRes = new QueryFormProjectDetailsRes();
        //项目详情
        detailsRes.setFormProjectRes(projectRes);
        //项目风格详情
        detailsRes.setFormProjectStyleRes(styleRes);
        //项目明细集合
        PageHelperUtil.defaultOrderBy("sort asc,updateTime desc, projectId desc");
        List<QueryFormProjectItemRes> itemResList = formProjectItemMapper.queryFormProjectItemResListByProjectId(projectId);
        for (QueryFormProjectItemRes res:itemResList){
            HandleFormUtils.setHandleFormProjectItemRes(res);
        }
        detailsRes.setFormProjectItemResList(itemResList);
        return ServiceResult.ok(detailsRes);
    }

}
