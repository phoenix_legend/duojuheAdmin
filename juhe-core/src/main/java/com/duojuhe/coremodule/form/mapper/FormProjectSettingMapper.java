package com.duojuhe.coremodule.form.mapper;

import com.duojuhe.coremodule.form.entity.FormProjectSetting;
import com.duojuhe.coremodule.form.pojo.projectsetting.QueryFormProjectSettingRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

public interface FormProjectSettingMapper extends TkMapper<FormProjectSetting> {
    /**
     * 根据项目id详情查询表单项目设置详细
     */
    QueryFormProjectSettingRes queryFormProjectSettingResByProjectId(@Param(value = "projectId") String projectId);
}