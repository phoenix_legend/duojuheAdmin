package com.duojuhe.coremodule.form.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.project.*;
import com.duojuhe.coremodule.form.pojo.projecttemplate.SaveFormProjectTransferTemplateReq;

import java.util.List;

public interface FormProjectService {

    /**
     * 【分页查询】根据条件查询表单项目list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryFormProjectPageRes>>> queryFormProjectPageResList(QueryFormProjectPageReq req);

    /**
     * 【保存】表单项目
     * @param req
     * @return
     */
    ServiceResult saveFormProject(SaveFormProjectReq req);

    /**
     * 【保存】根据项目模板id创建一个新的表单项目
     * @param req
     * @return
     */
    ServiceResult createFormProjectByTemplate(CreateFormProjectByTemplateReq req);

    /**
     * 【修改】表单项目
     * @param req
     * @return
     */
    ServiceResult updateFormProject(UpdateFormProjectReq req);


    /**
     * 【更新发布状态】表单项目更新发布状态
     * @param req
     * @return
     */
    ServiceResult updateFormProjectStatusByProjectId(FormProjectIdReq req);


    /**
     * 【删除】根据表单项目id删除项目
     * @param req
     * @return
     */
    ServiceResult deleteFormProjectByProjectId(FormProjectIdReq req);


    /**
     * 根据项目id查询详情
     * @param req
     * @return
     */
    ServiceResult<QueryFormProjectRes> queryFormProjectResByProjectId(FormProjectIdReq req);



    /**
     * 根据项目id查询项目明细
     * @param req
     * @return
     */
    ServiceResult<QueryFormProjectDetailsRes> queryFormProjectDetailsResByProjectId(FormProjectIdReq req);
}
