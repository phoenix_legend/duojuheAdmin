package com.duojuhe.coremodule.form.pojo.projectstyle;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryFormProjectStyleRes extends BaseBean {

    @ApiModelProperty(value = "主键id")
    private String projectId;

    @ApiModelProperty(value = "项目唯一标识")
    private String projectKey;

    @ApiModelProperty(value = "logo图片")
    private String logoImage;

    @ApiModelProperty(value = "logo位置")
    private String logoPosition;

    @ApiModelProperty(value = "背景颜色")
    private String backgroundColor;

    @ApiModelProperty(value = "背景图片")
    private String backgroundImg;

    @ApiModelProperty(value = "是否显示标题")
    private Boolean showTitle;

    @ApiModelProperty(value = "是否显示描述语")
    private Boolean showDescribe;

    @ApiModelProperty(value = "是否显示序号")
    private Boolean showNumber;

    @ApiModelProperty(value = "按钮颜色")
    private String buttonColor;

    @ApiModelProperty(value = "提交按钮文字")
    private String submitButtonText;

    @ApiModelProperty(value = "头部图片")
    private String headImage;

    @ApiModelProperty(value = "是否显示重置按钮")
    private Boolean showResetButton;

    @ApiModelProperty(value = "重置按钮文字")
    private String resetButtonText;

    @ApiModelProperty(value = "重置按钮颜色")
    private String resetButtonColor;
}