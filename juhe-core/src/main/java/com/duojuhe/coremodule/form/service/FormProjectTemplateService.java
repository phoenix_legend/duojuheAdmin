package com.duojuhe.coremodule.form.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.project.*;
import com.duojuhe.coremodule.form.pojo.projecttemplate.SaveFormProjectTemplateReq;
import com.duojuhe.coremodule.form.pojo.projecttemplate.SaveFormProjectTransferTemplateReq;
import com.duojuhe.coremodule.form.pojo.projecttemplate.UpdateFormProjectTemplateReq;

import java.util.List;

public interface FormProjectTemplateService {

    /**
     * 【分页查询】根据条件查询私有表单项目模板list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryFormProjectPageRes>>> queryFormProjectPrivateTemplatePageResList(QueryFormProjectPageReq req);

    /**
     * 【分页查询】根据条件查询公共表单项目模板list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryFormProjectPageRes>>> queryFormProjectPublicTemplatePageResList(QueryFormProjectPageReq req);



    /**
     * 【保存】表单项目模板
     * @param req
     * @return
     */
    ServiceResult saveFormProjectTemplate(SaveFormProjectTemplateReq req);


    /**
     * 【保存】根据项目id转成模板
     * @param req
     * @return
     */
    ServiceResult saveFormProjectTransferTemplate(SaveFormProjectTransferTemplateReq req);

    /**
     * 【修改】表单项目模板
     * @param req
     * @return
     */
    ServiceResult updateFormProjectTemplate(UpdateFormProjectTemplateReq req);


    /**
     * 【删除】根据表单项目模板id删除项目模板
     * @param req
     * @return
     */
    ServiceResult deleteFormProjectTemplateByProjectId(FormProjectIdReq req);


    /**
     * 根据项目模板id查询详情
     * @param req
     * @return
     */
    ServiceResult<QueryFormProjectRes> queryFormProjectTemplateResByProjectId(FormProjectIdReq req);



    /**
     * 根据项目模板id查询项目模板明细
     * @param req
     * @return
     */
    ServiceResult<QueryFormProjectDetailsRes> queryFormProjectTemplateDetailsResByProjectId(FormProjectIdReq req);
}
