package com.duojuhe.coremodule.form.pojo.projectitem;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MaxFormItemIdAndSortRes extends BaseBean {

    @ApiModelProperty(value = "最大表单id")
    private Long maxFormItemId;

    @ApiModelProperty(value = "最大表单排序")
    private Long maxFormItemSort;

    public MaxFormItemIdAndSortRes(Long maxFormItemId,Long maxFormItemSort){
        this.maxFormItemSort = maxFormItemSort;
        this.maxFormItemId = maxFormItemId;
    }
}
