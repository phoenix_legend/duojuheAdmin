package com.duojuhe.coremodule.form.pojo.projectlogic;

import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveFormProjectLogicReq extends FormProjectIdReq {
    @ApiModelProperty(value = "子项目表单id", required = true)
    @NotBlank(message = "子项目表单ID不能为空")
    private String formItemId;

    @ApiModelProperty(value = "条件选项1全部2任意",required=true)
    @NotNull(message = "条件选项不能为空")
    @Range(min = 1, max = 2, message = "条件选项参数不正确")
    private Integer expression;

    @ApiModelProperty(value = "条件list集合")
    @NotNull(message = "条件不能为空")
    private Set<FormProjectLogicCondition> conditionList;
}
