package com.duojuhe.coremodule.form.controller;

import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.category.SelectFormCategoryRes;
import com.duojuhe.coremodule.form.service.FormCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/formCategory/")
@Api(tags = {"【表单分类】表单分类管理相关接口"})
@Slf4j
public class FormCategoryController {
    @Resource
    private FormCategoryService formCategoryService;

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【下拉选择使用-查询所有表单分类】查询可供前端选择使用的表单分类")
    @PostMapping(value = "queryNormalSelectFormCategoryResList")
    public ServiceResult<List<SelectFormCategoryRes>> queryNormalSelectFormCategoryResList() {
        return formCategoryService.queryNormalSelectFormCategoryResList(new DataScopeFilterBean());
    }
}
