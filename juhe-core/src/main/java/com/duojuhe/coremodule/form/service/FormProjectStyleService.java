package com.duojuhe.coremodule.form.service;

import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import com.duojuhe.coremodule.form.pojo.projectstyle.QueryFormProjectStyleRes;
import com.duojuhe.coremodule.form.pojo.projectstyle.UpdateFormProjectStyleReq;

public interface FormProjectStyleService {

    /**
     * 根据项目id查询风格详情
     * @param req
     * @return
     */
    ServiceResult<QueryFormProjectStyleRes> queryFormProjectStyleResByProjectId(FormProjectIdReq req);



    /**
     * 【更新】表单项目更新
     * @param req
     * @return
     */
    ServiceResult updateFormProjectStyle(UpdateFormProjectStyleReq req);


}
