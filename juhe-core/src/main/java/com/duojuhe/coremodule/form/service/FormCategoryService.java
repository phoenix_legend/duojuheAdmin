package com.duojuhe.coremodule.form.service;

import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.category.SelectFormCategoryRes;

import java.util.List;

public interface FormCategoryService {
    /**
     * 【下拉选择使用-查询所有表单分类】查询可供前端选择使用的表单分类
     * @param dataScope
     * @return
     */
    ServiceResult<List<SelectFormCategoryRes>> queryNormalSelectFormCategoryResList(DataScopeFilterBean dataScope);

}
