package com.duojuhe.coremodule.form.controller;

import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import com.duojuhe.coremodule.form.pojo.projectsetting.QueryFormProjectSettingRes;
import com.duojuhe.coremodule.form.pojo.projectsetting.UpdateFormProjectSettingReq;
import com.duojuhe.coremodule.form.service.FormProjectSettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/formProjectSetting/")
@Api(tags = {"【表单项目设置】表单项目设置管理相关接口"})
@Slf4j
public class FormProjectSettingController {
    @Resource
    public FormProjectSettingService formProjectSettingService;

    @ApiOperation(value = "【修改】表单项目设置")
    @PostMapping(value = "updateFormProjectSetting")
    public ServiceResult updateFormProjectSetting(@Valid @RequestBody @ApiParam(value = "入参类") UpdateFormProjectSettingReq req) {
        return formProjectSettingService.updateFormProjectSetting(req);
    }

    @ApiOperation(value = "【查询详情】根据项目ID查询设置详情")
    @PostMapping(value = "queryFormProjectSettingResByProjectId")
    public ServiceResult<QueryFormProjectSettingRes> queryFormProjectSettingResByProjectId(@Valid @RequestBody @ApiParam(value = "入参类") FormProjectIdReq req) {
        return formProjectSettingService.queryFormProjectSettingResByProjectId(req);
    }
}
