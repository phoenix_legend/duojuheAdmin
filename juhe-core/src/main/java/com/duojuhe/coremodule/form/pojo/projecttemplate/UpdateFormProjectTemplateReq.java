package com.duojuhe.coremodule.form.pojo.projecttemplate;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateFormProjectTemplateReq extends SaveFormProjectTemplateReq {
    @ApiModelProperty(value = "模板项目ID", required = true)
    @NotBlank(message = "模板ID不可为空")
    private String projectId;
}