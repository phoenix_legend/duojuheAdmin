package com.duojuhe.coremodule.form.pojo.projectresult.report;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryProjectResultReportStatsRes extends BaseBean {
    @ApiModelProperty(value = "有效回收量")
    private Long  completeCount;

    @ApiModelProperty(value = "总浏览量")
    private Long  hitsCount;

    @ApiModelProperty(value = "回收率")
    private BigDecimal completeRate;

    @ApiModelProperty(value = "平均完成时间")
    private BigDecimal avgCompleteTime;
}
