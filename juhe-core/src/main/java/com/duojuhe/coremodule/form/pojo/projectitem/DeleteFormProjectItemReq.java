package com.duojuhe.coremodule.form.pojo.projectitem;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DeleteFormProjectItemReq extends BaseBean {

    @ApiModelProperty(value = "项目ID", required = true)
    @NotBlank(message = "项目ID不能为空")
    private String projectId;

    @ApiModelProperty(value = "表单项Id", required = true)
    @NotBlank(message = "表单项Id不能为空")
    private String formItemId;

    @ApiModelProperty(value = "表单项类型", required = true)
    @NotBlank(message = "表单项类型不能为空")
    private String type;
}
