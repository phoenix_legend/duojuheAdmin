package com.duojuhe.coremodule.form.pojo.project;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryFormProjectPageReq extends PageHead {
    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "分类id")
    private String categoryId;

    @ApiModelProperty(value = "备注说明")
    private String remark;

    @ApiModelProperty(value = "状态")
    private String statusCode;

    @ApiModelProperty(value = "开始日期yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}