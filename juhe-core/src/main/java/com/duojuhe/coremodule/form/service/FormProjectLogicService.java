package com.duojuhe.coremodule.form.service;

import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.entity.FormProjectLogic;
import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import com.duojuhe.coremodule.form.pojo.projectlogic.DeleteFormProjectLogicReq;
import com.duojuhe.coremodule.form.pojo.projectlogic.QueryFormProjectLogicPageRes;
import com.duojuhe.coremodule.form.pojo.projectlogic.SaveFormProjectLogicReq;

import java.util.List;

public interface FormProjectLogicService {

    /**
     * 【保存】表单项目逻辑
     * @param req
     * @return
     */
    ServiceResult<FormProjectLogic> saveFormProjectLogic(SaveFormProjectLogicReq req);


    /**
     * 【删除】根据表单项目逻辑id删除项目逻辑
     * @param req
     * @return
     */
    ServiceResult deleteFormProjectLogic(DeleteFormProjectLogicReq req);



    /**
     * 根据项目id查询表单项目逻辑集合
     * @param req
     * @return
     */
    ServiceResult<List<QueryFormProjectLogicPageRes>> queryFormProjectLogicPageResByProjectId(FormProjectIdReq req);
}
