package com.duojuhe.coremodule.form.mapper;

import com.duojuhe.coremodule.form.entity.FormProjectResult;
import com.duojuhe.coremodule.form.pojo.projectresult.QueryFormProjectResultPageReq;
import com.duojuhe.coremodule.form.pojo.projectresult.QueryFormProjectResultPageRes;
import com.duojuhe.coremodule.form.pojo.projectresult.QueryPublicFormProjectResultPageReq;
import com.duojuhe.coremodule.form.pojo.projectresult.report.QueryProjectResultReportDeviceRes;
import com.duojuhe.coremodule.form.pojo.projectresult.report.QueryProjectResultReportSituationRes;
import com.duojuhe.coremodule.form.pojo.projectresult.report.QueryProjectResultReportSourceRes;
import com.duojuhe.coremodule.form.pojo.projectresult.report.QueryProjectResultReportStatsRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FormProjectResultMapper extends TkMapper<FormProjectResult> {


    /**
     *  根据项目id查询项目收集信息集合
     * @param projectId
     * @return
     */
    List<FormProjectResult> queryFormProjectResultListByProjectId(@Param("projectId") String projectId);


    /**
     * 分页查询 根据条件查询表单项目提交结果集合
     *
     * @return
     */
    List<QueryFormProjectResultPageRes> queryFormProjectResultPageResList(@Param("req") QueryFormProjectResultPageReq req,@Param("sqlString") String sqlString);

    /**
     * 【查询项目收集设备分析】根据项目id查询提交来源集合
     * @param projectId
     * @return
     */
    List<QueryProjectResultReportDeviceRes> queryProjectResultReportDeviceResByProjectId(@Param("projectId") String projectId);



    /**
     * 【项目收集情况 按周查看】根据项目id查询项目收集情况 按周查看分析
     * @param projectId
     * @return
     */
    List<QueryProjectResultReportSituationRes> queryProjectResultReportSituationResByProjectId(@Param("projectId") String projectId);


    /**
     * 【项目收集情况 根据项目id查询项目收集来源分析】根据项目id查询项目收集来源分析
     * @param projectId
     * @return
     */
    List<QueryProjectResultReportSourceRes> queryProjectResultReportSourceResByProjectId(@Param("projectId") String projectId);


    /**
     *  【查询项目收集信息】根据项目id查询项目收集信息
     * @param projectId
     * @return
     */
    QueryProjectResultReportStatsRes queryProjectResultReportStatsResByProjectId(@Param("projectId") String projectId);

    /**
     * 分页查询 对外公共接口根据条件查询表单项目提交结果集合
     *
     * @return
     */
    List<QueryFormProjectResultPageRes> queryPublicFormProjectResultPageResList(@Param("req") QueryPublicFormProjectResultPageReq req);

}