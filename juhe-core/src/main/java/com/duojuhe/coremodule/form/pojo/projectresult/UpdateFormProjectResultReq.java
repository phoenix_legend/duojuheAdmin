package com.duojuhe.coremodule.form.pojo.projectresult;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Map;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateFormProjectResultReq extends FormProjectResultIdReq {

    @ApiModelProperty(value = "填写结果原始数据",required = true)
    @NotNull(message = "填写结果原始数据不能为空")
    private Map<String, Object> originalData;

    @ApiModelProperty(value = "填写结果处理后的数据" ,required = true)
    @NotNull(message = "填写结果处理后的数据不能为空")
    private Map<String, Object> processData;
}