package com.duojuhe.coremodule.form.pojo.projectitem;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveFormProjectItemReq  extends BaseBean {

    @ApiModelProperty(value = "项目ID", required = true)
    @NotBlank(message = "项目ID不能为空")
    private String projectId;

    @ApiModelProperty(value = "表单项Id", required = true)
    @NotBlank(message = "表单项Id不能为空")
    private String formItemId;

    @ApiModelProperty(value = "表单项类型", required = true)
    @NotBlank(message = "表单项类型不能为空")
    private String type;

    @ApiModelProperty(value = "表单项标题", required = true)
    @NotBlank(message = "表单项标题不能为空")
    private String label;

    @ApiModelProperty(value = "表单项目字段标识", required = true)
    @NotBlank(message = "表单项目字段标识不能为空")
    @Length(max = 255, message = "表单项目字段标识不得超过{max}位字符")
    private String formItemField;

    @ApiModelProperty(value = "表单项原始配置json", required = true)
    @NotNull(message = "表单项原始配置json不能为空")
    private Object itemConfig;

    @ApiModelProperty(value = "展示类型组件", required = true)
    @NotNull(message = "展示类型组件不能为空")
    private Boolean displayType;

    @ApiModelProperty(value = "是否显示正则组件", required = true)
    @NotNull(message = "是否显示正则组件不能为空")
    private Boolean showRegList;

    @ApiModelProperty(value = "是否显示标签", required = true)
    @NotNull(message = "是否显示标签不能为空")
    private Boolean showLabel;

    @ApiModelProperty(value = "表单项默认值")
    private Object defaultValue;

    @ApiModelProperty(value = "是否必填", required = true)
    @NotNull(message = "是否必填不能为空")
    private Boolean required;

    @ApiModelProperty(value = "输入型提示文字")
    private String placeholder;

    @ApiModelProperty(value = "排序")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;

    @ApiModelProperty(value = "栅格宽度")
    private Integer span;

    @ApiModelProperty(value = "扩展字段 表单项独有字段")
    private Map<String, Object> expand;


    @ApiModelProperty(value = "正则表达式")
    private List<Map<String, Object>> regList;
}
