package com.duojuhe.coremodule.form.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("表单项目逻辑配置表")
@Table(name = "form_project_logic")
public class FormProjectLogic extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "id")
    @Id
    private String id;

    @ApiModelProperty(value = "项目id")
    @Column(name = "project_id")
    private String projectId;

    @ApiModelProperty(value = "项目key")
    @Column(name = "project_key")
    private String projectKey;

    @ApiModelProperty(value = "子项目id")
    @Column(name = "item_id")
    private String itemId;

    @ApiModelProperty(value = "子项目表单id")
    @Column(name = "form_item_id")
    private String formItemId;

    @ApiModelProperty(value = "表单项类型")
    @Column(name = "form_item_type")
    private String formItemType;

    @ApiModelProperty(value = "条件选项1全部2任意")
    @Column(name = "expression")
    private Integer expression;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "条件list集合")
    @Column(name = "condition_list")
    private String conditionList;
}