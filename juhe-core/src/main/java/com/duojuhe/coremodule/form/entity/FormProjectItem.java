package com.duojuhe.coremodule.form.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldNameConstants
@ApiModel("表单项目子项")
@Table(name = "form_project_item")
public class FormProjectItem extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "item_id")
    @Id
    private String itemId;

    @ApiModelProperty(value = "项目id", required = true)
    @Column(name = "project_id")
    private String projectId;

    @ApiModelProperty(value = "项目key", required = true)
    @Column(name = "project_key")
    private String projectKey;

    @ApiModelProperty(value = "表单项Id")
    @Column(name = "form_item_id")
    private String formItemId;

    @ApiModelProperty(value = "表单项类型")
    @Column(name = "form_item_type")
    private String formItemType;

    @ApiModelProperty(value = "表单项标题")
    @Column(name = "form_item_label")
    private String formItemLabel;

    @ApiModelProperty(value = "表单项目字段标识")
    @Column(name = "form_item_field")
    private String formItemField;

    @ApiModelProperty(value = "是否显示标签")
    @Column(name = "form_item_show_label")
    private Integer formItemShowLabel;

    @ApiModelProperty(value = "表单项是否必填")
    @Column(name = "form_item_required")
    private Integer formItemRequired;

    @ApiModelProperty(value = "输入型提示文字")
    @Column(name = "form_item_placeholder")
    private String formItemPlaceholder;

    @ApiModelProperty(value = "栅格宽度")
    @Column(name = "form_item_span")
    private Integer formItemSpan;

    @ApiModelProperty(value = "展示类型组件")
    @Column(name = "form_item_display_type")
    private Integer formItemDisplayType;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Long sort;

    @ApiModelProperty(value = "表单项默认值")
    @Column(name = "form_item_default_value")
    private String formItemDefaultValue;

    @ApiModelProperty(value = "扩展字段 表单项独有字段")
    @Column(name = "form_item_expand")
    private String formItemExpand;

    @ApiModelProperty(value = "正则表达式")
    @Column(name = "form_item_regex")
    private String formItemRegex;

    @ApiModelProperty(value = "是否显示正则表达式")
    @Column(name = "form_item_show_regex")
    private Integer formItemShowRegex;

    @ApiModelProperty(value = "表单项原始配置json")
    @Column(name = "form_item_config")
    private String formItemConfig;
}