package com.duojuhe.coremodule.form.pojo.projectresult;

import com.duojuhe.coremodule.form.pojo.project.QueryFormProjectDetailsRes;
import com.duojuhe.coremodule.form.pojo.projectlogic.QueryFormProjectLogicPageRes;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryFormProjectResultDetailsEditRes extends QueryFormProjectDetailsRes {
    @ApiModelProperty(value = "填写结果原始数据")
    private String originalData;

    @ApiModelProperty(value = "填写结果处理后的数据")
    private String processData;

    @ApiModelProperty(value = "表单逻辑集合")
    private List<QueryFormProjectLogicPageRes> logicPageResList;
}
