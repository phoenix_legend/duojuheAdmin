package com.duojuhe.coremodule.form.pojo.projectitem;

import com.duojuhe.common.bean.PageHead;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryFormProjectItemPageReq extends PageHead {
    @ApiModelProperty(value = "项目ID", required = true)
    @NotBlank(message = "项目Id不可为空")
    private String projectId;
}