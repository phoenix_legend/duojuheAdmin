package com.duojuhe.coremodule.form.pojo.projectstyle;

import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateFormProjectStyleReq extends FormProjectIdReq {
    @ApiModelProperty(value = "logo图片")
    private String logoImage;

    @ApiModelProperty(value = "logo位置")
    private String logoPosition;

    @ApiModelProperty(value = "背景颜色")
    private String backgroundColor;

    @ApiModelProperty(value = "背景图片")
    private String backgroundImg;

    @ApiModelProperty(value = "是否显示标题", required = true)
    @NotNull(message = "是否显示标题不能为空")
    private Boolean showTitle;

    @ApiModelProperty(value = "是否显示描述语", required = true)
    @NotNull(message = "是否显示描述语不能为空")
    private Boolean showDescribe;

    @ApiModelProperty(value = "是否显示序号", required = true)
    @NotNull(message = "是否显示序号不能为空")
    private Boolean showNumber;

    @ApiModelProperty(value = "提交按钮颜色", required = true)
    @Length(max = 50, message = "提交按钮颜色不得超过{max}位字符")
    @NotBlank(message = "提交按钮颜色不能为空")
    private String buttonColor;

    @ApiModelProperty(value = "提交按钮文字", required = true)
    @Length(max = 20, message = "提交按钮文字不得超过{max}位字符")
    @NotBlank(message = "提交按钮文字不能为空")
    private String submitButtonText;

    @ApiModelProperty(value = "头部图片")
    private String headImage;


    @ApiModelProperty(value = "是否显示重置按钮", required = true)
    @NotNull(message = "是否显示重置按钮不能为空")
    private Boolean showResetButton;

    @ApiModelProperty(value = "重置按钮文字", required = true)
    @Length(max = 20, message = "重置按钮文字不得超过{max}位字符")
    @NotBlank(message = "重置按钮文字不能为空")
    private String resetButtonText;

    @ApiModelProperty(value = "重置按钮颜色", required = true)
    @Length(max = 50, message = "重置按钮颜色不得超过{max}位字符")
    @NotBlank(message = "重置按钮颜色不能为空")
    private String resetButtonColor;
}