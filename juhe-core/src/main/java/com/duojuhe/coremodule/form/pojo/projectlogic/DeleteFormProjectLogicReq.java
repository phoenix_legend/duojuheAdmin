package com.duojuhe.coremodule.form.pojo.projectlogic;

import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DeleteFormProjectLogicReq extends FormProjectIdReq {
    @ApiModelProperty(value = "主键ID", required = true)
    @NotBlank(message = "ID不可为空")
    private String id;
}
