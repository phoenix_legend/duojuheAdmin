package com.duojuhe.coremodule.form.pojo.projecttheme;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryFormProjectThemePageRes extends BaseBean {
    @ApiModelProperty(value = "主键id", required = true)
    private String themeId;

    @ApiModelProperty(value = "主题名称")
    private String themeName;

    @ApiModelProperty(value = "主题风格样式逗号分隔")
    private String themeStyle;

    @ApiModelProperty(value = "主题颜色逗号分隔")
    private String themeColor;

    @ApiModelProperty(value = "按钮颜色")
    private String buttonColor;

    @ApiModelProperty(value = "背景颜色")
    private String backgroundColor;

    @ApiModelProperty(value = "背景图片")
    private String backgroundImg;

    @ApiModelProperty(value = "是否显示标题")
    private Boolean showTitle;

    @ApiModelProperty(value = "提交按钮显示字")
    private String submitButtonText;

    @ApiModelProperty(value = "是否显示描述语")
    private Boolean showDescribe;

    @ApiModelProperty(value = "是否显示序号")
    private Boolean showNumber;

    @ApiModelProperty(value = "是否显示重置按钮")
    private Boolean showResetButton;

    @ApiModelProperty(value = "重置按钮文字")
    private String resetButtonText;

    @ApiModelProperty(value = "重置按钮颜色")
    private String resetButtonColor;

    @ApiModelProperty(value = "头部图片")
    private String headImage;
}