package com.duojuhe.coremodule.form.mapper;

import com.duojuhe.coremodule.form.entity.FormProject;
import com.duojuhe.coremodule.form.pojo.project.QueryFormProjectPageReq;
import com.duojuhe.coremodule.form.pojo.project.QueryFormProjectPageRes;
import com.duojuhe.coremodule.form.pojo.project.QueryFormProjectRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FormProjectMapper extends TkMapper<FormProject> {
    /**
     * 分页查询 根据条件查询表单项目list
     *
     * @return
     */
    List<QueryFormProjectPageRes> queryFormProjectPageResList(@Param("req") QueryFormProjectPageReq req,@Param("projectModuleCode") String projectModuleCode,@Param("projectTypeCode") String projectTypeCode);

    /**
     * 根据项目Id查询详情
     * @param projectId
     * @return
     */
    QueryFormProjectRes queryFormProjectResByProjectId(@Param("projectId") String projectId);

}