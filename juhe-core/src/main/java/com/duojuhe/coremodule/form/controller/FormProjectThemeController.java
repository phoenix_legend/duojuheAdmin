package com.duojuhe.coremodule.form.controller;

import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.projecttheme.QueryFormProjectThemePageReq;
import com.duojuhe.coremodule.form.pojo.projecttheme.QueryFormProjectThemePageRes;
import com.duojuhe.coremodule.form.service.FormProjectThemeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/formProjectTheme/")
@Api(tags = {"【表单项目主题】表单项目主题管理相关接口"})
@Slf4j
public class FormProjectThemeController {
    @Resource
    public FormProjectThemeService formProjectThemeService;

    @ApiOperation(value = "【分页查询】分页查询子表单项目主题list")
    @PostMapping(value = "queryFormProjectThemePageResList")
    public ServiceResult<PageResult<List<QueryFormProjectThemePageRes>>> queryFormProjectThemePageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryFormProjectThemePageReq req) {
        return formProjectThemeService.queryFormProjectThemePageResList(req);
    }

}
