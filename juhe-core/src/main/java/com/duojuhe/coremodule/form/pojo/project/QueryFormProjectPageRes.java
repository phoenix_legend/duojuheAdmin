package com.duojuhe.coremodule.form.pojo.project;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryFormProjectPageRes extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String projectId;

    @ApiModelProperty(value = "项目唯一标识", required = true)
    private String projectKey;

    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "封面图片base64格式")
    private String coverImg;

    @ApiModelProperty(value = "使用次数")
    private Integer useCount;

    @ApiModelProperty(value = "分类id")
    private String categoryId;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;


    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date updateTime;

    @ApiModelProperty(value = "备注说明")
    private String remark;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "浏览点击次数")
    private Long hits;

    @ApiModelProperty(value = "状态")
    private String statusCode;

    @ApiModelProperty(value = "状态名称")
    private String statusName;

    @ApiModelProperty(value = "状态颜色")
    private String statusColor;
}