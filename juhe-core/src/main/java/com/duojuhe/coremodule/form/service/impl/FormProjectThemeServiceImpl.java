package com.duojuhe.coremodule.form.service.impl;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.page.PageHelperUtil;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.form.mapper.FormProjectThemeMapper;
import com.duojuhe.coremodule.form.pojo.projecttheme.QueryFormProjectThemePageReq;
import com.duojuhe.coremodule.form.pojo.projecttheme.QueryFormProjectThemePageRes;
import com.duojuhe.coremodule.form.service.FormProjectThemeService;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class FormProjectThemeServiceImpl extends BaseService implements FormProjectThemeService {
    @Resource
    private FormProjectThemeMapper formProjectThemeMapper;

    /**
     * 【分页查询】根据条件查询表单主题list
     * @param req
     * @return
     */
    @Override
    public ServiceResult<PageResult<List<QueryFormProjectThemePageRes>>> queryFormProjectThemePageResList(QueryFormProjectThemePageReq req) {
        PageHelperUtil.orderByAndStartPage(req, "createTime desc, themeId desc");
        List<QueryFormProjectThemePageRes> list = formProjectThemeMapper.queryFormProjectThemePageResList(req);
        return PageHelperUtil.returnServiceResult(req, list);
    }
}
