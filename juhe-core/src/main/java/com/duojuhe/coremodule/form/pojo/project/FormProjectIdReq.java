package com.duojuhe.coremodule.form.pojo.project;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FormProjectIdReq extends BaseBean {
    @ApiModelProperty(value = "项目ID", required = true)
    @NotBlank(message = "项目ID不可为空")
    private String projectId;
}