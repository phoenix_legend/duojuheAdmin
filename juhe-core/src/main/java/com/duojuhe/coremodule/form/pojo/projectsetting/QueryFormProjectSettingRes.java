package com.duojuhe.coremodule.form.pojo.projectsetting;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.bean.UserTokenInfoVo;
import com.duojuhe.common.enums.SystemEnum;
import com.duojuhe.coremodule.form.entity.FormProjectSetting;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryFormProjectSettingRes extends BaseBean {

    @ApiModelProperty(value = "主键")
    private String projectId;

    @ApiModelProperty(value = "项目唯一标识")
    private String projectKey;

    @ApiModelProperty(value = "提交提示图片")
    private String submitPromptImg;

    @ApiModelProperty(value = "提交提示文字")
    private String submitPromptText;

    @ApiModelProperty(value = "提交后跳转地址")
    private String submitJumpUrl;

    @ApiModelProperty(value = "是否公开提交结果")
    private Boolean resultPublic;

    @ApiModelProperty(value = "是否限制填写次数")
    private Boolean limitWrite;

    @ApiModelProperty(value = "每人允许填写次数")
    private Integer everyoneWriteNum;

    @ApiModelProperty(value = "限制填写次数方式每天，总共")
    private String limitWriteNumMode;

    @ApiModelProperty(value = "分享图片")
    private String shareImg;

    @ApiModelProperty(value = "分享标题")
    private String shareTitle;

    @ApiModelProperty(value = "分享描述")
    private String shareDesc;

    @ApiModelProperty(value = "是否定时收集")
    private Boolean showTimedCollection;

    @ApiModelProperty(value = "定时收集开始时间")
    @JsonFormat(pattern = DateUtils.DEFAULT_DATETIME_FORMAT, timezone = "GMT+8")
    private Date timedCollectionBeginTime;

    @ApiModelProperty(value = "定时收集结束时间")
    @JsonFormat(pattern = DateUtils.DEFAULT_DATETIME_FORMAT, timezone = "GMT+8")
    private Date timedCollectionEndTime;

    @ApiModelProperty(value = "定时未开始提示文字")
    private String timedNotEnabledPromptText;

    @ApiModelProperty(value = "定时停用会提示文字")
    private String timedDeactivatePromptText;

    @ApiModelProperty(value = "是否启用定量收集")
    private Boolean showQuantitative;

    @ApiModelProperty(value = "定量收集总数量")
    private Integer quantitativeTotalQuantity;

    @ApiModelProperty(value = "定量收集完成提示文字")
    private String quantitativeEndPromptText;


    public QueryFormProjectSettingRes(FormProjectSetting formProjectSetting){
        Integer yes = SystemEnum.IS_YES.YES.getKey();
        this.projectId = formProjectSetting.getProjectId();
        this.projectKey = formProjectSetting.getProjectKey();
        this.submitPromptImg = formProjectSetting.getSubmitPromptImg();
        this.submitPromptText = formProjectSetting.getSubmitPromptText();
        this.submitJumpUrl = formProjectSetting.getSubmitJumpUrl();
        this.resultPublic = yes.equals(formProjectSetting.getResultPublic());
        this.limitWrite = yes.equals(formProjectSetting.getLimitWrite());
        this.everyoneWriteNum = formProjectSetting.getEveryoneWriteNum();
        this.limitWriteNumMode = formProjectSetting.getLimitWriteNumMode();
        this.shareImg = formProjectSetting.getShareImg();
        this.shareTitle = formProjectSetting.getShareTitle();
        this.shareDesc = formProjectSetting.getShareDesc();
        this.showTimedCollection = yes.equals(formProjectSetting.getShowTimedCollection());
        this.timedCollectionBeginTime = formProjectSetting.getTimedCollectionBeginTime();
        this.timedCollectionEndTime = formProjectSetting.getTimedCollectionEndTime();
        this.timedNotEnabledPromptText = formProjectSetting.getTimedNotEnabledPromptText();
        this.timedDeactivatePromptText = formProjectSetting.getTimedDeactivatePromptText();
        this.showQuantitative = yes.equals(formProjectSetting.getShowQuantitative());
        this.quantitativeTotalQuantity = formProjectSetting.getQuantitativeTotalQuantity();
        this.quantitativeEndPromptText = formProjectSetting.getQuantitativeEndPromptText();
    }
}