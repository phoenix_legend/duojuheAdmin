package com.duojuhe.coremodule.form.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.duojuhe.common.annotation.KeyLock;
import com.duojuhe.common.result.ErrorCodes;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.encryption.md5.MD5Util;
import com.duojuhe.common.utils.idgenerator.UUIDUtils;
import com.duojuhe.common.utils.page.PageHelperUtil;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.form.entity.FormProject;
import com.duojuhe.coremodule.form.entity.FormProjectItem;
import com.duojuhe.coremodule.form.entity.FormProjectLogic;
import com.duojuhe.coremodule.form.mapper.FormProjectItemMapper;
import com.duojuhe.coremodule.form.mapper.FormProjectLogicMapper;
import com.duojuhe.coremodule.form.mapper.FormProjectMapper;
import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import com.duojuhe.coremodule.form.pojo.projectlogic.DeleteFormProjectLogicReq;
import com.duojuhe.coremodule.form.pojo.projectlogic.FormProjectLogicCondition;
import com.duojuhe.coremodule.form.pojo.projectlogic.QueryFormProjectLogicPageRes;
import com.duojuhe.coremodule.form.pojo.projectlogic.SaveFormProjectLogicReq;
import com.duojuhe.coremodule.form.service.FormProjectLogicService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class FormProjectLogicServiceImpl  extends BaseService implements FormProjectLogicService {
    @Resource
    private FormProjectLogicMapper formProjectLogicMapper;
    @Resource
    private FormProjectMapper formProjectMapper;
    @Resource
    private FormProjectItemMapper formProjectItemMapper;
    /**
     * 【保存】表单项目逻辑
     * @param req
     * @return
     */
    @KeyLock(lockKeyParts = "projectId")
    @Override
    public ServiceResult<FormProjectLogic> saveFormProjectLogic(SaveFormProjectLogicReq req) {
        String projectId = req.getProjectId();
        FormProject formProjectOld = formProjectMapper.selectByPrimaryKey(projectId);
        if (formProjectOld == null) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        //表单id
        String formItemId = req.getFormItemId();
        //表单类型
        //itemId 是有项目id表单id md5组成
        String itemId = MD5Util.getMD532(projectId+formItemId);
        FormProjectItem formProjectItem = formProjectItemMapper.selectByPrimaryKey(itemId);
        if (formProjectItem==null){
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        FormProjectLogic formProjectLogicOld = formProjectLogicMapper.selectByPrimaryKey(itemId);
        //当前时间
        Date nowDate = new Date();
        FormProjectLogic formProjectLogic = new FormProjectLogic();
        formProjectLogic.setId(itemId);
        formProjectLogic.setExpression(req.getExpression());
        formProjectLogic.setProjectId(projectId);
        formProjectLogic.setProjectKey(formProjectOld.getProjectKey());
        formProjectLogic.setItemId(formProjectItem.getItemId());
        formProjectLogic.setFormItemType(formProjectItem.getFormItemType());
        formProjectLogic.setFormItemId(formProjectItem.getFormItemId());
        formProjectLogic.setCreateTime(nowDate);
        formProjectLogic.setUpdateTime(nowDate);
        formProjectLogic.setConditionList(JSON.toJSONString(req.getConditionList()));
        if (formProjectLogicOld==null){
            formProjectLogicMapper.insertSelective(formProjectLogic);
        }else {
            formProjectLogicMapper.updateByPrimaryKeySelective(formProjectLogic);
        }
        return ServiceResult.ok(formProjectLogic);
    }

    /**
     * 【删除】根据表单项目逻辑id删除项目逻辑
     * @param req
     * @return
     */
    @KeyLock(lockKeyParts = "projectId")
    @Override
    public ServiceResult deleteFormProjectLogic(DeleteFormProjectLogicReq req) {
        formProjectLogicMapper.deleteByPrimaryKey(req.getId());
        return ServiceResult.ok(req);
    }

    /**
     * 根据项目id查询表单项目逻辑集合
     * @param req
     * @return
     */
    @KeyLock(lockKeyParts = "projectId")
    @Override
    public ServiceResult<List<QueryFormProjectLogicPageRes>> queryFormProjectLogicPageResByProjectId(FormProjectIdReq req) {
        PageHelperUtil.defaultOrderBy("createTime asc, id desc");
        List<QueryFormProjectLogicPageRes> logicPageResList = formProjectLogicMapper.queryFormProjectLogicPageResByProjectId(req.getProjectId());
        for (QueryFormProjectLogicPageRes res:logicPageResList){
            if (StringUtils.isNotBlank(res.getConditionJson())){
                res.setConditionList(JSONObject.parseArray(res.getConditionJson(), FormProjectLogicCondition.class));
            }
        }
        return ServiceResult.ok(logicPageResList);
    }
}
