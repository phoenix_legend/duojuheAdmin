package com.duojuhe.coremodule.form.pojo.projecttheme;

import com.duojuhe.common.bean.PageHead;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryFormProjectThemePageReq extends PageHead {
    @ApiModelProperty(value = "主题名称")
    private String themeName;

    @ApiModelProperty(value = "主题风格样式json")
    private String themeStyle;

    @ApiModelProperty(value = "主题颜色json")
    private String themeColor;
}