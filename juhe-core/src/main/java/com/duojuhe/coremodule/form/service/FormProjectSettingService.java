package com.duojuhe.coremodule.form.service;

import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import com.duojuhe.coremodule.form.pojo.projectsetting.QueryFormProjectSettingRes;
import com.duojuhe.coremodule.form.pojo.projectsetting.QueryPublicFormProjectSettingRes;
import com.duojuhe.coremodule.form.pojo.projectsetting.UpdateFormProjectSettingReq;

public interface FormProjectSettingService {



    /**
     * 【更新】表单项目设置更新
     * @param req
     * @return
     */
    ServiceResult updateFormProjectSetting(UpdateFormProjectSettingReq req);


    /**
     * 根据项目id查询表单项目设置详情
     * @param req
     * @return
     */
    ServiceResult<QueryFormProjectSettingRes> queryFormProjectSettingResByProjectId(FormProjectIdReq req);


}
