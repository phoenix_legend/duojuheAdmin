package com.duojuhe.coremodule.form.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.projecttheme.QueryFormProjectThemePageReq;
import com.duojuhe.coremodule.form.pojo.projecttheme.QueryFormProjectThemePageRes;

import java.util.List;

public interface FormProjectThemeService {

    /**
     * 【分页查询】根据条件查询表单主题list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryFormProjectThemePageRes>>> queryFormProjectThemePageResList(QueryFormProjectThemePageReq req);
}
