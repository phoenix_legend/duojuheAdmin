package com.duojuhe.coremodule.form.pojo.projectitem;

import com.duojuhe.common.bean.BaseBean;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryFormProjectItemRes extends BaseBean {
    @ApiModelProperty(value = "itemId")
    private String id;

    @ApiModelProperty(value = "itemId")
    private String itemId;

    @ApiModelProperty(value = "项目id")
    private String projectId;

    @ApiModelProperty(value = "项目key")
    private String projectKey;

    @ApiModelProperty(value = "表单项Id")
    private String formItemId;

    @ApiModelProperty(value = "表单项类型")
    private String type;

    @ApiModelProperty(value = "表单项标题")
    private String label;

    @ApiModelProperty(value = "展示类型组件")
    private Boolean displayType;

    @ApiModelProperty(value = "是否显示标签")
    private Boolean showLabel;

    @ApiModelProperty(value = "表单项默认值",hidden = true)
    @JsonIgnore
    private String formItemDefaultValue;

    @ApiModelProperty(value = "表单项默认值")
    private Object defaultValue;

    @ApiModelProperty(value = "是否必填")
    private Boolean required;

    @ApiModelProperty(value = "输入型提示文字")
    private String placeholder;

    @ApiModelProperty(value = "排序")
    private Long sort;

    @ApiModelProperty(value = "栅格宽度")
    private Integer span;

    @JsonIgnore
    @ApiModelProperty(value = "扩展字段 表单项独有字段",hidden = true)
    private String formItemExpand;

    @ApiModelProperty(value = "扩展字段 表单项独有字段")
    private Map<String, Object> expand;

    @JsonIgnore
    @ApiModelProperty(value = "正则表达式",hidden = true)
    private String formItemRegex;

    @ApiModelProperty(value = "正则表达式")
    private List<Map<String, Object>> regList;

    @ApiModelProperty(value = "是否显示正则表达式")
    private Boolean showRegList;

    @ApiModelProperty(value = "表单项复原成json配置json")
    private String itemConfig;
}