package com.duojuhe.coremodule.form.pojo.projectresult.report;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryProjectResultReportSituationRes extends BaseBean {
    @ApiModelProperty(value = "有效回收量")
    private Long  completeCount;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = DateUtils.DEFAULT_DATE_FORMAT, timezone = "GMT+8")
    private Date createTime;
}
