package com.duojuhe.coremodule.form.enums;

import lombok.Getter;

/**
 * 表单枚举
 */
public class FormEnum {

    /**
     * 表单项目状态枚举 FORM_PROJECT_UNPUBLISHED 未发布 FORM_PROJECT_PUBLISHED 已发布
     */
    public enum FormProjectStatus {
        FORM_PROJECT_UNPUBLISHED("FORM_PROJECT_UNPUBLISHED", "未发布"),
        FORM_PROJECT_PUBLISHED("FORM_PROJECT_PUBLISHED", "已发布"),
        FORM_PROJECT_STOP("FORM_PROJECT_STOP", "已结束");
        @Getter
        private String key;
        @Getter
        private String value;

        FormProjectStatus(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }


    /**
     * 表单项目类型 问卷调查项目
     */
    public enum FormProjectType {
        FORM_SURVEY_QUESTIONNAIRE("FORM_SURVEY_QUESTIONNAIRE", "问卷调查"),
        FORM_SURVEY_PUBLIC_TEMPLATE("FORM_SURVEY_PUBLIC_TEMPLATE", "问卷公有模板"),
        FORM_SURVEY_PRIVATE_TEMPLATE("FORM_SURVEY_PRIVATE_TEMPLATE", "问卷私有模板");
        @Getter
        private String key;
        @Getter
        private String value;

        FormProjectType(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 表单项目所属模块
     */
    public enum FormProjectModule {
        FORM_SURVEY_PROJECT("FORM_SURVEY_PROJECT", "问卷调查模块"),
        FORM_SURVEY_TEMPLATE("FORM_SURVEY_TEMPLATE", "问卷模板模块");
        @Getter
        private String key;
        @Getter
        private String value;

        FormProjectModule(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }



}
