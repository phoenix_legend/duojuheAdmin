package com.duojuhe.coremodule.form.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import com.duojuhe.coremodule.form.pojo.project.QueryFormProjectDetailsRes;
import com.duojuhe.coremodule.form.pojo.project.QueryFormProjectRes;
import com.duojuhe.coremodule.form.pojo.projectitem.QueryFormProjectItemTableHeadRes;
import com.duojuhe.coremodule.form.pojo.projectlogic.QueryFormProjectLogicPageRes;
import com.duojuhe.coremodule.form.pojo.projectresult.QueryFormProjectResultPageReq;
import com.duojuhe.coremodule.form.pojo.projectresult.QueryFormProjectResultPageRes;
import com.duojuhe.coremodule.form.pojo.projectresult.QueryPublicFormProjectResultPageReq;
import com.duojuhe.coremodule.form.pojo.projectresult.SaveFormProjectResultReq;
import com.duojuhe.coremodule.form.pojo.projectsetting.QueryPublicFormProjectSettingRes;

import java.util.List;

public interface FormProjectPublicService {

    /**
     * 【保存】表单结果
     * @param req
     * @return
     */
    ServiceResult<String> savePublicFormProjectResult(SaveFormProjectResultReq req);


    /**
     * 根据项目id查询详情
     * @param req
     * @return
     */
    ServiceResult<QueryFormProjectRes> queryPublicFormProjectResByProjectId(FormProjectIdReq req);


    /**
     * 根据项目id查询项目明细
     * @param req
     * @return
     */
    ServiceResult<QueryFormProjectDetailsRes> queryPublicFormProjectDetailsResByProjectId(FormProjectIdReq req);



    /**
     * 【项目设置详情】根据项目ID查询项目设置详情
     * @param req
     * @return
     */
    ServiceResult<QueryPublicFormProjectSettingRes> queryPublicFormProjectSettingResByProjectId(FormProjectIdReq req);




    /**
     * 根据项目id查询表单项目逻辑集合
     * @param req
     * @return
     */
    ServiceResult<List<QueryFormProjectLogicPageRes>> queryPublicFormProjectLogicPageResByProjectId(FormProjectIdReq req);

    /**
     * 根据项目Id查询表单项目的表头信息
     * @param req
     * @return
     */
    ServiceResult<List<QueryFormProjectItemTableHeadRes>> queryPublicFormProjectItemTableHeadResByProjectId(FormProjectIdReq req);



    /**
     * 【分页查询】根据条件查询表单项目提交结果集合
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryFormProjectResultPageRes>>> queryPublicFormProjectResultPageResList(QueryPublicFormProjectResultPageReq req);

}
