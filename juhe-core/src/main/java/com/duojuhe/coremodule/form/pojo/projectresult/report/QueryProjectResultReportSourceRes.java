package com.duojuhe.coremodule.form.pojo.projectresult.report;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryProjectResultReportSourceRes extends BaseBean {
    @ApiModelProperty(value = "有效回收量")
    private Long  completeCount;

    @ApiModelProperty(value = "提交浏览器名称")
    private String submitBrowser;
}
