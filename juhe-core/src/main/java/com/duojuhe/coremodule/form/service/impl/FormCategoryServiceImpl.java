package com.duojuhe.coremodule.form.service.impl;

import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.page.PageHelperUtil;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.form.mapper.FormCategoryMapper;
import com.duojuhe.coremodule.form.pojo.category.SelectFormCategoryRes;
import com.duojuhe.coremodule.form.service.FormCategoryService;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;
@Slf4j
@Service
public class FormCategoryServiceImpl extends BaseService implements FormCategoryService {
    @Resource
    private FormCategoryMapper formCategoryMapper;

    @Override
    public ServiceResult<List<SelectFormCategoryRes>> queryNormalSelectFormCategoryResList(DataScopeFilterBean dataScope) {
        PageHelperUtil.defaultOrderBy("sort desc,createTime desc,categoryId desc");
        List<SelectFormCategoryRes> list = formCategoryMapper.queryNormalSelectFormCategoryResList();
        return ServiceResult.ok(list);
    }
}
