package com.duojuhe.coremodule.form.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.form.pojo.project.FormProjectIdReq;
import com.duojuhe.coremodule.form.pojo.projectitem.*;

import java.util.List;

public interface FormProjectItemService {
    /**
     * 【分页查询】根据条件查询表单项目list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryFormProjectItemPageRes>>> queryFormProjectItemPageResList(QueryFormProjectItemPageReq req);

    /**
     * 根据项目Id查询本项目最大的表单id
     * @param req
     * @return
     */
    ServiceResult<String> queryFormProjectItemMaxFormItemIdByProjectId(FormProjectIdReq req);

    /**
     * 根据项目Id查询表单项目的表头信息
     * @param req
     * @return
     */
    ServiceResult<List<QueryFormProjectItemTableHeadRes>> queryFormProjectItemTableHeadResByProjectId(FormProjectIdReq req);

    /**
     * 保存和修改表单项目
     * @param req
     * @return
     */
    ServiceResult saveUpdateFormProjectItem(SaveFormProjectItemReq req);


    /**
     * 删除表单项目
     * @param req
     * @return
     */
    ServiceResult deleteFormProjectItem(DeleteFormProjectItemReq req);


    /**
     * 修改表单项目排序值
     * @param req
     * @return
     */
    ServiceResult<Long> updateFormProjectItemSort(UpdateFormProjectItemSortReq req);
}
