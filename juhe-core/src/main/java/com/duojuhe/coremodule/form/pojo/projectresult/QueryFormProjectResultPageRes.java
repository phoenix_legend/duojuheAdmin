package com.duojuhe.coremodule.form.pojo.projectresult;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryFormProjectResultPageRes extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String id;

    @ApiModelProperty(value = "提交编号")
    private String serialNumber;

    @ApiModelProperty(value = "项目id")
    private String projectId;

    @ApiModelProperty(value = "项目唯一标识")
    private String projectKey;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = DateUtils.DEFAULT_DATETIME_FORMAT, timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = DateUtils.DEFAULT_DATETIME_FORMAT, timezone = "GMT+8")
    private Date updateTime;

    @ApiModelProperty(value = "提交ip")
    private String submitIp;

    @ApiModelProperty(value = "提交用户操作系统")
    private String submitOs;

    @ApiModelProperty(value = "提交用户浏览器")
    private String submitBrowser;

    @JsonIgnore
    @ApiModelProperty(value = "填写结果处理后的数据")
    private String  processData;

    @JsonIgnore
    @ApiModelProperty(value = "提交用户客户端信息")
    private String  submitUa;

    @ApiModelProperty(value = "填写结果处理后的数据")
    private Map<String, Object>  processDataMap;

    @ApiModelProperty(value = "提交用户客户端信息")
    private Map<String, Object>  submitUaMap;


}