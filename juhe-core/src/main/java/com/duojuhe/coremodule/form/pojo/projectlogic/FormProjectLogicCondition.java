package com.duojuhe.coremodule.form.pojo.projectlogic;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FormProjectLogicCondition extends BaseBean {
    @ApiModelProperty(value = "表单项Id", required = true)
    private String formItemId;

    @ApiModelProperty(value = "表达式条件", required = true)
    private String expression;

    @ApiModelProperty(value = "选项", required = true)
    private Object optionValue;
}
