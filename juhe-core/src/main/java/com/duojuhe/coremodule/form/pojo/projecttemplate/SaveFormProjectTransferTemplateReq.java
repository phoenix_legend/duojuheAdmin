package com.duojuhe.coremodule.form.pojo.projecttemplate;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveFormProjectTransferTemplateReq extends SaveFormProjectTemplateReq {
    @ApiModelProperty(value = "项目ID", required = true)
    @NotBlank(message = "项目ID不可为空")
    private String projectId;
}
