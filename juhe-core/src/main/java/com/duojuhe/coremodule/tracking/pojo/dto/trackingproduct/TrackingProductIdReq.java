package com.duojuhe.coremodule.tracking.pojo.dto.trackingproduct;

import com.duojuhe.common.bean.PageHead;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TrackingProductIdReq extends PageHead {
    @ApiModelProperty(value = "产品ID", example = "1",required=true)
    @NotBlank(message = "产品ID不能为空")
    private String productId;
}