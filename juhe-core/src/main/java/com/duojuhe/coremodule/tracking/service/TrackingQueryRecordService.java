package com.duojuhe.coremodule.tracking.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingqueryrecord.QueryTrackingQueryRecordPageReq;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingqueryrecord.QueryTrackingQueryRecordPageRes;

import java.util.List;


public interface TrackingQueryRecordService {
    /**
     * 防伪码查询记录分页查询
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryTrackingQueryRecordPageRes>>> queryTrackingQueryRecordPageResList(QueryTrackingQueryRecordPageReq req);

}
