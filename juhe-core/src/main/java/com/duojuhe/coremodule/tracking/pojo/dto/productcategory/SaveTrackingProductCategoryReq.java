package com.duojuhe.coremodule.tracking.pojo.dto.productcategory;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveTrackingProductCategoryReq extends BaseBean {
    @ApiModelProperty(value = "分类名称", required = true)
    @Length(max = 20, message = "分类名称不能超过{max}位字符")
    @NotBlank(message = "分类名称不能为空")
    private String categoryName;

    @ApiModelProperty(value = "排序，只能为纯数字", example = "1",required=true)
    @NotNull(message = "排序不能为空")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;

    @ApiModelProperty(value = "分类状态，状态FORBID禁用 NORMAL正常", example = "NORMAL",required = true)
    @Length(max = 50, message = "分类状态不能超过{max}位字符")
    @Pattern(regexp = RegexpConstants.STATUS, message = "分类状态参数错误!")
    @NotBlank(message = "分类状态不能为空")
    private String statusCode;

}