package com.duojuhe.coremodule.tracking.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("溯源流程项")
@Table(name = "tracking_process_item")
public class TrackingProcessItem extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "item_id")
    @Id
    private String itemId;

    @ApiModelProperty(value = "物流码", required = true)
    @Column(name = "transport_code")
    private String transportCode;

    @ApiModelProperty(value = "所属分类")
    @Column(name = "tracking_category_id")
    private String trackingCategoryId;

    @ApiModelProperty(value = "流程状态启用禁用", required = true)
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间id")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "所属租户id")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "流程描述")
    @Column(name = "description")
    private String description;

    @ApiModelProperty(value = "操作人员")
    @Column(name = "operator_name")
    private String operatorName;

    @ApiModelProperty(value = "操作时间")
    @Column(name = "operation_time")
    private Date operationTime;
}