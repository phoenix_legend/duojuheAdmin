package com.duojuhe.coremodule.tracking.pojo.dto.trackingproduct;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryTrackingProductPageRes extends BaseBean {

    @ApiModelProperty(value = "主键")
    private String productId;

    @ApiModelProperty(value = "产品分类")
    private String productCategoryId;

    @ApiModelProperty(value = "产品分类名称")
    private String productCategoryName;

    @ApiModelProperty(value = "产品编号")
    private String productNumber;

    @ApiModelProperty(value = "产品名称")
    private String productName;

    @ApiModelProperty(value = "条形码")
    private String productBarcode;

    @ApiModelProperty(value = "产品编码")
    private String productCode;

    @ApiModelProperty(value = "产品状态")
    private String statusCode;

    @ApiModelProperty(value = "状态中文 FORBID禁用 NORMAL正常", example = "1")
    private String statusName;

    @ApiModelProperty(value = "状态显示颜色", example = "1")
    private String statusColor;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "产品图片")
    private String productImg;

    @ApiModelProperty(value = "产品描述")
    private String description;

    @ApiModelProperty(value = "创建时间", required = true)
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间", required = true)
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date updateTime;

    @ApiModelProperty(value = "排序")
    private Integer sort;
}