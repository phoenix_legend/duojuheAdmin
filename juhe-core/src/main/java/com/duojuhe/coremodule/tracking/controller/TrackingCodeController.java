package com.duojuhe.coremodule.tracking.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.annotation.RepeatSubmit;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingcode.*;
import com.duojuhe.coremodule.tracking.service.TrackingCodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/trackingCode/")
@Api(tags = {"【一物一码，产品溯源 溯源防伪码管理】溯源防伪码管理相关接口"})
@Slf4j
public class TrackingCodeController {
    @Resource
    private TrackingCodeService trackingCodeService;



    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【查询溯源防伪码】管理界面使用")
    @PostMapping(value = "queryTrackingCodePageResList")
    public ServiceResult<PageResult<List<QueryTrackingCodePageRes>>> queryTrackingCodePageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryTrackingCodePageReq req) {
        return trackingCodeService.queryTrackingCodePageResList(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【保存】批量溯源防伪码")
    @PostMapping(value = "batchSaveTrackingCode")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_CODE, operationType = LogEnum.OperationType.ADD)
    public ServiceResult batchSaveTrackingCode(@Valid @RequestBody @ApiParam(value = "入参类") BatchSaveTrackingCodeReq req) {
        return trackingCodeService.batchSaveTrackingCode(req);
    }


    @RepeatSubmit
    @ApiOperation(value = "【保存】溯源防伪码")
    @PostMapping(value = "saveTrackingCode")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_CODE, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveTrackingCode(@Valid @RequestBody @ApiParam(value = "入参类") SaveTrackingCodeReq req) {
        return trackingCodeService.saveTrackingCode(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【修改】溯源防伪码")
    @PostMapping(value = "updateTrackingCode")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_CODE, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateTrackingCode(@Valid @RequestBody @ApiParam(value = "入参类") UpdateTrackingCodeReq req) {
        return trackingCodeService.updateTrackingCode(req);
    }


    @RepeatSubmit
    @ApiOperation(value = "【删除】根据溯源防伪码ID删除溯源防伪码")
    @PostMapping(value = "deleteTrackingCodeByTrackingCodeId")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_CODE, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteTrackingCodeByTrackingCodeId(@Valid @RequestBody @ApiParam(value = "入参类") TrackingCodeIdReq req) {
        return trackingCodeService.deleteTrackingCodeByTrackingCodeId(req);
    }
}
