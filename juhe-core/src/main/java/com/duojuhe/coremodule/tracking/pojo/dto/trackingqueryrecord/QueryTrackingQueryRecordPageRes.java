package com.duojuhe.coremodule.tracking.pojo.dto.trackingqueryrecord;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryTrackingQueryRecordPageRes extends BaseBean {

    @ApiModelProperty(value = "主键")
    private String recordId;

    @ApiModelProperty(value = "查询编号")
    private String queryNumber;

    @ApiModelProperty(value = "查询IP")
    private String queryIp;

    @ApiModelProperty(value = "查询浏览器相关信息")
    private String queryUa;

    @ApiModelProperty(value = "防伪码编号")
    private String trackingNumber;

    @ApiModelProperty(value = "防伪码")
    private String trackingCode;

    @ApiModelProperty(value = "防伪码")
    private String trackingValue;

    @ApiModelProperty(value = "创建时间", required = true)
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

}