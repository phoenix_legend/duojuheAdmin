package com.duojuhe.coremodule.tracking.pojo.dto.trackingsupplier;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveTrackingProductSupplierReq extends BaseBean {

    @ApiModelProperty(value = "供货商名称", required = true)
    @Length(max = 255, message = "供货商名称不能超过{max}位字符")
    @NotBlank(message = "供货商名称不能为空")
    private String supplierName;

    @ApiModelProperty(value = "供货商编码", required = true)
    @Length(max = 50, message = "供货商编码不能超过{max}位字符")
    @NotBlank(message = "供货商编码不能为空")
    private String supplierCode;

    @ApiModelProperty(value = "手机号码", example = "1",required = true)
    @Pattern(regexp = RegexpConstants.MOBILE, message = "手机号码不合法")
    @NotBlank(message = "手机号码不能为空")
    private String mobileNumber;

    @ApiModelProperty(value = "备注")
    @Length(max = 255, message = "备注不能超过{max}位字符")
    private String remark;

    @ApiModelProperty(value = "排序，只能为纯数字", example = "1",required=true)
    @NotNull(message = "排序不能为空")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;

    @ApiModelProperty(value = "供货商状态，状态FORBID禁用 NORMAL正常", example = "NORMAL",required = true)
    @Length(max = 50, message = "供货商状态不能超过{max}位字符")
    @Pattern(regexp = RegexpConstants.STATUS, message = "供货商状态参数错误!")
    @NotBlank(message = "供货商状态不能为空")
    private String statusCode;

    @ApiModelProperty(value = "供货商描述说明",required = true)
    @NotBlank(message = "供货商描述说明不能为空")
    private String description;

}