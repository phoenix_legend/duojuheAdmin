package com.duojuhe.coremodule.tracking.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingproduct.*;


import java.util.List;

public interface TrackingProductService {

    /**
     * 【查询所有产品】产品管理
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryTrackingProductPageRes>>> queryTrackingProductPageResList(QueryTrackingProductPageReq req);


    /**
     * 【查询所有产品】供前端选择产品
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryTrackingProductPageRes>>> querySelectTrackingProductPageResList(QueryTrackingProductPageReq req);


    /**
     * 【保存】产品
     * @param req
     * @return
     */
    ServiceResult saveTrackingProduct(SaveTrackingProductReq req);

    /**
     * 【修改】产品
     * @param req
     * @return
     */
    ServiceResult updateTrackingProduct(UpdateTrackingProductReq req);

    /**
     * 【删除】根据产品ID删除产品，注意一次仅能删除一个产品，存在绑定关系的则不能删除
     * @param req
     * @return
     */
    ServiceResult deleteTrackingProductByProductId(TrackingProductIdReq req);
}
