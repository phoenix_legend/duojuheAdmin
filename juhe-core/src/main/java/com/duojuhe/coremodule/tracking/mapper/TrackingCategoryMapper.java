package com.duojuhe.coremodule.tracking.mapper;

import com.duojuhe.coremodule.tracking.entity.TrackingCategory;
import com.duojuhe.coremodule.tracking.pojo.dto.category.QuerySelectTrackingCategoryReq;
import com.duojuhe.coremodule.tracking.pojo.dto.category.QuerySelectTrackingCategoryRes;
import com.duojuhe.coremodule.tracking.pojo.dto.category.QueryTrackingCategoryPageReq;
import com.duojuhe.coremodule.tracking.pojo.dto.category.QueryTrackingCategoryPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TrackingCategoryMapper extends TkMapper<TrackingCategory> {
    /**
     * 溯源流程类别分页查询
     * @param req
     * @return
     */
    List<QueryTrackingCategoryPageRes> queryTrackingCategoryPageResList(@Param("req") QueryTrackingCategoryPageReq req);

    /**
     * 溯源流程类别查询 一般供前端下拉选择使用
     * @param req
     * @return
     */
    List<QuerySelectTrackingCategoryRes> querySelectNormalTrackingCategoryResList(@Param("req") QuerySelectTrackingCategoryReq req);


}