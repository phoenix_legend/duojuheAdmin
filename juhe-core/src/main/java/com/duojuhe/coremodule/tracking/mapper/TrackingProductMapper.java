package com.duojuhe.coremodule.tracking.mapper;

import com.duojuhe.coremodule.tracking.entity.TrackingProduct;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingproduct.QueryTrackingProductPageReq;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingproduct.QueryTrackingProductPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TrackingProductMapper extends TkMapper<TrackingProduct> {
    /**
     * 产品分页查询
     * @param req
     * @return
     */
    List<QueryTrackingProductPageRes> queryTrackingProductPageResList(@Param("req") QueryTrackingProductPageReq req);

}