package com.duojuhe.coremodule.tracking.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.tracking.pojo.dto.category.*;

import java.util.List;

public interface TrackingCategoryService {
    /**
     * 【查询所有溯源流程类别分类】溯源流程类别分类管理
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryTrackingCategoryPageRes>>> queryTrackingCategoryPageResList(QueryTrackingCategoryPageReq req);


    /**
     * 【查询所有溯源流程类别分类】供前端选择溯源流程类别分类
     * @param req
     * @return
     */
    ServiceResult<List<QuerySelectTrackingCategoryRes>> querySelectNormalTrackingCategoryResList(QuerySelectTrackingCategoryReq req);


    /**
     * 【保存】溯源流程类别分类溯源流程类别
     * @param req
     * @return
     */
    ServiceResult saveTrackingCategory(SaveTrackingCategoryReq req);

    /**
     * 【修改】溯源流程类别分类溯源流程类别
     * @param req
     * @return
     */
    ServiceResult updateTrackingCategory(UpdateTrackingCategoryReq req);

    /**
     * 【删除】根据溯源流程类别分类ID删除溯源流程类别分类，注意一次仅能删除一个溯源流程类别分类，存在绑定关系的则不能删除
     * @param req
     * @return
     */
    ServiceResult deleteTrackingCategoryByCategoryId(TrackingCategoryIdReq req);
}
