package com.duojuhe.coremodule.tracking.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("产品供货商")
@Table(name = "tracking_product_supplier")
public class TrackingProductSupplier extends BaseBean {
    @ApiModelProperty(value = "主键id", required = true)
    @Column(name = "supplier_id")
    @Id
    private String supplierId;

    @ApiModelProperty(value = "供货商名称", required = true)
    @Column(name = "supplier_name")
    private String supplierName;

    @ApiModelProperty(value = "供货商编号", required = true)
    @Column(name = "supplier_number")
    private String supplierNumber;

    @ApiModelProperty(value = "供货商编码", required = true)
    @Column(name = "supplier_code")
    private String supplierCode;

    @ApiModelProperty(value = "手机号码", required = true)
    @Column(name = "mobile_number")
    private String mobileNumber;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "所属租户id")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "供货商状态")
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "供货商描述说明")
    @Column(name = "description")
    private String description;
}