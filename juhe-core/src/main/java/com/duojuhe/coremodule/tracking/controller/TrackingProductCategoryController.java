package com.duojuhe.coremodule.tracking.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.tracking.pojo.dto.productcategory.*;
import com.duojuhe.coremodule.tracking.service.TrackingProductCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/trackingProductCategory/")
@Api(tags = {"【一物一码，产品溯源 产品分类管理】产品分类管理相关接口"})
@Slf4j
public class TrackingProductCategoryController {
    @Resource
    private TrackingProductCategoryService trackingProductCategoryService;
    
    
    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【查询产品分类】分类管理界面使用")
    @PostMapping(value = "queryTrackingProductCategoryPageResList")
    public ServiceResult<PageResult<List<QueryTrackingProductCategoryPageRes>>> queryTrackingProductCategoryPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryTrackingProductCategoryPageReq req) {
        return trackingProductCategoryService.queryTrackingProductCategoryPageResList(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【查询所有产品分类】查询可供前端选择使用的产品分类")
    @PostMapping(value = "querySelectNormalTrackingProductCategoryResList")
    public ServiceResult<List<QuerySelectTrackingProductCategoryRes>> querySelectNormalTrackingProductCategoryResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySelectTrackingProductCategoryReq req) {
        return trackingProductCategoryService.querySelectNormalTrackingProductCategoryResList(req);
    }

    @ApiOperation(value = "【保存】产品分类")
    @PostMapping(value = "saveTrackingProductCategory")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_PRODUCT_CATEGORY, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveTrackingProductCategory(@Valid @RequestBody @ApiParam(value = "入参类") SaveTrackingProductCategoryReq req) {
        return trackingProductCategoryService.saveTrackingProductCategory(req);
    }


    @ApiOperation(value = "【修改】产品分类")
    @PostMapping(value = "updateTrackingProductCategory")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_PRODUCT_CATEGORY, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateTrackingProductCategory(@Valid @RequestBody @ApiParam(value = "入参类") UpdateTrackingProductCategoryReq req) {
        return trackingProductCategoryService.updateTrackingProductCategory(req);
    }


    @ApiOperation(value = "【删除】根据产品分类ID删除产品分类，注意一次仅能删除一个产品分类，存在绑定关系的则不能删除")
    @PostMapping(value = "deleteTrackingProductCategoryByCategoryId")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_PRODUCT_CATEGORY, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteTrackingProductCategoryByCategoryId(@Valid @RequestBody @ApiParam(value = "入参类") TrackingProductCategoryIdReq req) {
        return trackingProductCategoryService.deleteTrackingProductCategoryByCategoryId(req);
    }
}
