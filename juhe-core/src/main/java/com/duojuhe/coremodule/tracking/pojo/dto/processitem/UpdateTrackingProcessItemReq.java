package com.duojuhe.coremodule.tracking.pojo.dto.processitem;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateTrackingProcessItemReq extends BaseBean {
    @ApiModelProperty(value = "溯源流程ID", example = "1",required=true)
    @NotBlank(message = "溯源流程ID不能为空")
    private String itemId;

    @ApiModelProperty(value = "所属分类", required = true)
    @Length(max = 32, message = "所属分类不能超过{max}位字符")
    @NotBlank(message = "所属分类不能为空")
    private String trackingCategoryId;

    @ApiModelProperty(value = "流程说明",required=true)
    @Length(max = 255, message = "流程说明不能超过{max}位字符")
    @NotBlank(message = "流程说明不能为空")
    private String description;

    @ApiModelProperty(value = "操作人员",required=true)
    @Length(max = 30, message = "操作人员不能超过{max}位字符")
    @NotBlank(message = "操作人员不能为空")
    private String operatorName;

    @ApiModelProperty(value = "操作时间",required=true)
    @NotNull(message = "操作时间不能为空")
    @DateTimeFormat(pattern = DateUtils.DEFAULT_DATETIME_FORMAT)
    @Past(message = "操作时间必须为过去时间")
    private Date operationTime;
}