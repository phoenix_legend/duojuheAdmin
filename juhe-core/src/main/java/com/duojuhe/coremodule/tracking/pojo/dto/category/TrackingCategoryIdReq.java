package com.duojuhe.coremodule.tracking.pojo.dto.category;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TrackingCategoryIdReq extends BaseBean {
    @ApiModelProperty(value = "溯源流程类别ID", example = "1",required=true)
    @NotBlank(message = "溯源流程类别ID不能为空")
    private String categoryId;
}