package com.duojuhe.coremodule.tracking.pojo.dto.productcategory;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySelectTrackingProductCategoryRes extends BaseBean {
    @ApiModelProperty(value = "主键")
    private String categoryId;

    @ApiModelProperty(value = "分类名称")
    private String categoryName;
}