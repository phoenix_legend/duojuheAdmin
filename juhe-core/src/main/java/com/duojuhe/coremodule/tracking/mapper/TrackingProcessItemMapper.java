package com.duojuhe.coremodule.tracking.mapper;

import com.duojuhe.coremodule.tracking.entity.TrackingProcessItem;
import com.duojuhe.coremodule.tracking.pojo.dto.processitem.QueryTrackingProcessItemPageReq;
import com.duojuhe.coremodule.tracking.pojo.dto.processitem.QueryTrackingProcessItemPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TrackingProcessItemMapper extends TkMapper<TrackingProcessItem> {
    /**
     * 溯源流程项分页查询
     * @param req
     * @return
     */
    List<QueryTrackingProcessItemPageRes> queryTrackingProcessItemPageResList(@Param("req") QueryTrackingProcessItemPageReq req);
}