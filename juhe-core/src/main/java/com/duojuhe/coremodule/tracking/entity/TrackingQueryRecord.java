package com.duojuhe.coremodule.tracking.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("防伪码查询记录")
@Table(name = "tracking_query_record")
public class TrackingQueryRecord extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "record_id")
    @Id
    private String recordId;

    @ApiModelProperty(value = "查询编号", required = true)
    @Column(name = "query_number")
    private String queryNumber;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "所属租户id")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "查询IP")
    @Column(name = "query_ip")
    private String queryIp;

    @ApiModelProperty(value = "查询浏览器相关信息")
    @Column(name = "query_ua")
    private String queryUa;

    @ApiModelProperty(value = "防伪码id")
    @Column(name = "tracking_code_id")
    private String trackingCodeId;

    @ApiModelProperty(value = "防伪码编号")
    @Column(name = "tracking_number")
    private String trackingNumber;

    @ApiModelProperty(value = "防伪码")
    @Column(name = "tracking_code")
    private String trackingCode;
}