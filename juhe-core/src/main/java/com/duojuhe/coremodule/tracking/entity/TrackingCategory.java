package com.duojuhe.coremodule.tracking.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("溯源流程类别")
@Table(name = "tracking_category")
public class TrackingCategory extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "category_id")
    @Id
    private String categoryId;

    @ApiModelProperty(value = "分类名称", required = true)
    @Column(name = "category_name")
    private String categoryName;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "所属租户id")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "分类状态，启用，禁用")
    @Column(name = "status_code")
    private String statusCode;
}