package com.duojuhe.coremodule.tracking.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.tracking.pojo.dto.productcategory.*;

import java.util.List;

public interface TrackingProductCategoryService {
    /**
     * 【查询所有产品分类】产品分类管理
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryTrackingProductCategoryPageRes>>> queryTrackingProductCategoryPageResList(QueryTrackingProductCategoryPageReq req);


    /**
     * 【查询所有产品分类】供前端选择产品分类
     * @param req
     * @return
     */
    ServiceResult<List<QuerySelectTrackingProductCategoryRes>> querySelectNormalTrackingProductCategoryResList(QuerySelectTrackingProductCategoryReq req);


    /**
     * 【保存】产品分类产品
     * @param req
     * @return
     */
    ServiceResult saveTrackingProductCategory(SaveTrackingProductCategoryReq req);

    /**
     * 【修改】产品分类产品
     * @param req
     * @return
     */
    ServiceResult updateTrackingProductCategory(UpdateTrackingProductCategoryReq req);

    /**
     * 【删除】根据产品分类ID删除产品分类，注意一次仅能删除一个产品分类，存在绑定关系的则不能删除
     * @param req
     * @return
     */
    ServiceResult deleteTrackingProductCategoryByCategoryId(TrackingProductCategoryIdReq req);
}
