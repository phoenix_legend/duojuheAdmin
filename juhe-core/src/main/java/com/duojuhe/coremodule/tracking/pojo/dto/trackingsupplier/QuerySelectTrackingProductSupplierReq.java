package com.duojuhe.coremodule.tracking.pojo.dto.trackingsupplier;

import com.duojuhe.common.bean.DataScopeFilterBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySelectTrackingProductSupplierReq extends DataScopeFilterBean {
    @ApiModelProperty(value = "供货商名称")
    private String supplierName;
}