package com.duojuhe.coremodule.tracking.pojo.dto.processitem;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryTrackingProcessItemPageRes extends BaseBean {

    @ApiModelProperty(value = "主键")
    private String itemId;

    @ApiModelProperty(value = "物流码")
    private String transportCode;

    @ApiModelProperty(value = "流程类别ID")
    private String trackingCategoryId;

    @ApiModelProperty(value = "流程类别")
    private String trackingCategoryName;

    @ApiModelProperty(value = "流程描述")
    private String description;

    @ApiModelProperty(value = "操作人员")
    private String operatorName;

    @ApiModelProperty(value = "操作时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date operationTime;

}