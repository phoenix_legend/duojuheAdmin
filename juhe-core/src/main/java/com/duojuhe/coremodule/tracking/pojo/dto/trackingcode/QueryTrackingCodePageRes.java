package com.duojuhe.coremodule.tracking.pojo.dto.trackingcode;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryTrackingCodePageRes extends BaseBean {

    @ApiModelProperty(value = "主键")
    private String trackingCodeId;

    @ApiModelProperty(value = "防伪编号")
    private String trackingNumber;

    @ApiModelProperty(value = "防伪码地址")
    private String trackingCodeUrl;

    @ApiModelProperty(value = "防伪码")
    private String trackingCode;

    @ApiModelProperty(value = "批次")
    private String trackingBatch;

    @ApiModelProperty(value = "物流码")
    private String transportCode;

    @ApiModelProperty(value = "所属产品")
    private String productId;

    @ApiModelProperty(value = "所属产品名称")
    private String productName;

    @ApiModelProperty(value = "所属供货商")
    private String supplierId;

    @ApiModelProperty(value = "所属供货商")
    private String supplierName;

    @ApiModelProperty(value = "防伪码状态")
    private String statusCode;

    @ApiModelProperty(value = "状态中文 FORBID禁用 NORMAL正常", example = "1")
    private String statusName;

    @ApiModelProperty(value = "状态显示颜色", example = "1")
    private String statusColor;

    @ApiModelProperty(value = "创建时间", required = true)
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

}