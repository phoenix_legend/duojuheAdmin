package com.duojuhe.coremodule.tracking.service.impl;

import com.duojuhe.common.annotation.DataScopeFilter;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.page.PageHelperUtil;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.tracking.mapper.TrackingQueryRecordMapper;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingqueryrecord.QueryTrackingQueryRecordPageReq;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingqueryrecord.QueryTrackingQueryRecordPageRes;
import com.duojuhe.coremodule.tracking.service.TrackingQueryRecordService;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;
@Slf4j
@Service
public class TrackingQueryRecordServiceImpl extends BaseService implements TrackingQueryRecordService {
    @Resource
    private TrackingQueryRecordMapper trackingQueryRecordMapper;

    /**
     * 防伪码查询记录分页查询
     * @param req
     * @return
     */
    @DataScopeFilter
    @Override
    public ServiceResult<PageResult<List<QueryTrackingQueryRecordPageRes>>> queryTrackingQueryRecordPageResList(QueryTrackingQueryRecordPageReq req) {
        PageHelperUtil.orderByAndStartPage(req, "createTime desc, recordId desc");
        List<QueryTrackingQueryRecordPageRes> list = trackingQueryRecordMapper.queryTrackingQueryRecordPageResList(req);
        return PageHelperUtil.returnServiceResult(req, list);
    }
}
