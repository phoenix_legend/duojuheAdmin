package com.duojuhe.coremodule.tracking.pojo.dto.trackingqueryrecord;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryTrackingQueryRecordPageReq extends PageHead {
    @ApiModelProperty(value = "查询编号")
    private String queryNumber;

    @ApiModelProperty(value = "查询IP")
    private String queryIp;

    @ApiModelProperty(value = "防伪码编号")
    private String trackingNumber;

    @ApiModelProperty(value = "防伪码")
    private String trackingCode;

    @ApiModelProperty(value = "开始日期yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }

}