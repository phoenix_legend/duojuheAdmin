package com.duojuhe.coremodule.tracking.pojo.dto.trackingsupplier;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySelectTrackingProductSupplierRes extends BaseBean {

    @ApiModelProperty(value = "主键id")
    private String supplierId;

    @ApiModelProperty(value = "供货商名称")
    private String supplierName;

    @ApiModelProperty(value = "供货商编码")
    private String supplierCode;
}