package com.duojuhe.coremodule.tracking.pojo.dto.trackingcode;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TrackingCodeIdReq extends BaseBean {
    @ApiModelProperty(value = "防伪码ID", required = true)
    @Length(max = 32, message = "防伪码ID不能超过{max}位字符")
    @NotBlank(message = "防伪码ID不能为空")
    private String trackingCodeId;
}