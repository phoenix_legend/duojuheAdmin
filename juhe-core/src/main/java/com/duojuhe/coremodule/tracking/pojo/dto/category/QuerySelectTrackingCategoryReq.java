package com.duojuhe.coremodule.tracking.pojo.dto.category;

import com.duojuhe.common.bean.DataScopeFilterBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySelectTrackingCategoryReq extends DataScopeFilterBean {
    @ApiModelProperty(value = "类别名称")
    private String categoryName;
}