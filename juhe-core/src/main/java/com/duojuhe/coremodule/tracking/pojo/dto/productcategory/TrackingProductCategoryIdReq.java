package com.duojuhe.coremodule.tracking.pojo.dto.productcategory;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TrackingProductCategoryIdReq extends BaseBean {
    @ApiModelProperty(value = "产品分类ID", example = "1",required=true)
    @NotBlank(message = "产品分类ID不能为空")
    private String categoryId;
}