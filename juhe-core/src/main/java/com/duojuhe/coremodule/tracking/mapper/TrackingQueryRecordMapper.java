package com.duojuhe.coremodule.tracking.mapper;


import com.duojuhe.coremodule.tracking.entity.TrackingQueryRecord;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingqueryrecord.QueryTrackingQueryRecordPageReq;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingqueryrecord.QueryTrackingQueryRecordPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TrackingQueryRecordMapper extends TkMapper<TrackingQueryRecord> {
    /**
     * 防伪码查询记录分页查询
     */
    List<QueryTrackingQueryRecordPageRes> queryTrackingQueryRecordPageResList(@Param(value = "req") QueryTrackingQueryRecordPageReq req);
}