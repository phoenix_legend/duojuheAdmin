package com.duojuhe.coremodule.tracking.pojo.dto.trackingsupplier;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryTrackingProductSupplierPageRes extends BaseBean {

    @ApiModelProperty(value = "主键id")
    private String supplierId;

    @ApiModelProperty(value = "供货商名称")
    private String supplierName;

    @ApiModelProperty(value = "供货商编号")
    private String supplierNumber;

    @ApiModelProperty(value = "供货商编码")
    private String supplierCode;

    @ApiModelProperty(value = "手机号码")
    private String mobileNumber;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "状态：FORBID禁用 NORMAL正常", example = "1")
    private String statusCode;

    @ApiModelProperty(value = "状态中文 FORBID禁用 NORMAL正常", example = "1")
    private String statusName;

    @ApiModelProperty(value = "状态显示颜色", example = "1")
    private String statusColor;

    @ApiModelProperty(value = "供货商描述说明")
    private String description;

    @ApiModelProperty(value = "创建时间", required = true)
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间", required = true)
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date updateTime;

}