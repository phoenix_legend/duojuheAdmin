package com.duojuhe.coremodule.tracking.mapper;

import com.duojuhe.coremodule.tracking.entity.TrackingCode;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingcode.QueryTrackingCodePageReq;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingcode.QueryTrackingCodePageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TrackingCodeMapper extends TkMapper<TrackingCode> {
    /**
     * 溯源防伪码分页查询
     * @param req
     * @return
     */
    List<QueryTrackingCodePageRes> queryTrackingCodePageResList(@Param("req") QueryTrackingCodePageReq req);

}