package com.duojuhe.coremodule.tracking.pojo.dto.trackingproduct;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveTrackingProductReq extends BaseBean {

    @ApiModelProperty(value = "产品分类")
    @Length(max = 32, message = "产品分类不能超过{max}位字符")
    @NotBlank(message = "产品分类不能为空")
    private String productCategoryId;

    @ApiModelProperty(value = "产品名称", required = true)
    @Length(max = 255, message = "产品名称不能超过{max}位字符")
    @NotBlank(message = "产品名称不能为空")
    private String productName;

    @ApiModelProperty(value = "条形码", required = true)
    @Length(max = 30, message = "产品条码不能超过{max}位字符")
    @NotBlank(message = "产品条码不能为空")
    private String productBarcode;

    @ApiModelProperty(value = "产品编码", required = true)
    @Length(max = 30, message = "产品编码不能超过{max}位字符")
    @NotBlank(message = "产品编码不能为空")
    private String productCode;

    @ApiModelProperty(value = "产品状态，PRODUCT_STOP_SALE停售中 PRODUCT_START_SALE销售中", example = "START_SALE",required = true)
    @Length(max = 50, message = "产品状态不能超过{max}位字符")
    @Pattern(regexp = "^[PRODUCT_START_SALE|PRODUCT_STOP_SALE]*$", message = "产品状态参数错误!")
    @NotBlank(message = "产品状态不能为空")
    private String statusCode;

    @ApiModelProperty(value = "备注")
    @Length(max = 255, message = "备注不能超过{max}位字符")
    private String remark;

    @ApiModelProperty(value = "产品图片", required = true)
    @NotBlank(message = "产品图片不能为空")
    @Length(max = 255, message = "产品图片不能超过{max}位字符")
    private String productImg;

    @ApiModelProperty(value = "产品描述")
    private String description;

    @ApiModelProperty(value = "产品排序，只能为纯数字", example = "1",required=true)
    @NotNull(message = "产品排序不能为空")
    @Range(min = 1, max = 10000, message = "产品排序数值不正确")
    private Integer sort;
}