package com.duojuhe.coremodule.tracking.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.tracking.pojo.dto.category.*;
import com.duojuhe.coremodule.tracking.service.TrackingCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/trackingCategory/")
@Api(tags = {"【一物一码，产品溯源 溯源流程类别管理】溯源流程类别管理相关接口"})
@Slf4j
public class TrackingCategoryController {
    @Resource
    private TrackingCategoryService trackingCategoryService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【查询溯源流程类别】分类管理界面使用")
    @PostMapping(value = "queryTrackingCategoryPageResList")
    public ServiceResult<PageResult<List<QueryTrackingCategoryPageRes>>> queryTrackingCategoryPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryTrackingCategoryPageReq req) {
        return trackingCategoryService.queryTrackingCategoryPageResList(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【查询所有溯源流程类别】查询可供前端选择使用的溯源流程类别")
    @PostMapping(value = "querySelectNormalTrackingCategoryResList")
    public ServiceResult<List<QuerySelectTrackingCategoryRes>> querySelectNormalTrackingCategoryResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySelectTrackingCategoryReq req) {
        return trackingCategoryService.querySelectNormalTrackingCategoryResList(req);
    }
    

    @ApiOperation(value = "【保存】溯源流程类别")
    @PostMapping(value = "saveTrackingCategory")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_CATEGORY, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveTrackingCategory(@Valid @RequestBody @ApiParam(value = "入参类") SaveTrackingCategoryReq req) {
        return trackingCategoryService.saveTrackingCategory(req);
    }


    @ApiOperation(value = "【修改】溯源流程类别")
    @PostMapping(value = "updateTrackingCategory")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_CATEGORY, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateTrackingCategory(@Valid @RequestBody @ApiParam(value = "入参类") UpdateTrackingCategoryReq req) {
        return trackingCategoryService.updateTrackingCategory(req);
    }


    @ApiOperation(value = "【删除】根据溯源流程类别ID删除溯源流程类别，注意一次仅能删除一个溯源流程类别，存在绑定关系的则不能删除")
    @PostMapping(value = "deleteTrackingCategoryByCategoryId")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_CATEGORY, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteTrackingCategoryByCategoryId(@Valid @RequestBody @ApiParam(value = "入参类") TrackingCategoryIdReq req) {
        return trackingCategoryService.deleteTrackingCategoryByCategoryId(req);
    }
}
