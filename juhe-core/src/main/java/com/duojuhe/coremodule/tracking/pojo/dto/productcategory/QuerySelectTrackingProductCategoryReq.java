package com.duojuhe.coremodule.tracking.pojo.dto.productcategory;

import com.duojuhe.common.bean.DataScopeFilterBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySelectTrackingProductCategoryReq extends DataScopeFilterBean {
    @ApiModelProperty(value = "分类名称")
    private String categoryName;
}