package com.duojuhe.coremodule.tracking.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.tracking.pojo.dto.processitem.*;

import java.util.List;

public interface TrackingProcessItemService {
    /**
     * 【查询所有溯源流程项目】溯源流程项目管理
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryTrackingProcessItemPageRes>>> queryTrackingProcessItemPageResList(QueryTrackingProcessItemPageReq req);

    /**
     * 【批量保存】批量保存溯源流程项目溯源流程项目
     * @param req
     * @return
     */
    ServiceResult batchSaveTrackingProcessItem(BatchSaveTrackingProcessItemReq req);


    /**
     * 【保存】溯源流程项目溯源流程项目
     * @param req
     * @return
     */
    ServiceResult saveTrackingProcessItem(SaveTrackingProcessItemReq req);

    /**
     * 【修改】溯源流程项目溯源流程项目
     * @param req
     * @return
     */
    ServiceResult updateTrackingProcessItem(UpdateTrackingProcessItemReq req);

    /**
     * 【删除】根据溯源流程项目ID删除溯源流程项目，注意一次仅能删除一个溯源流程项目，存在绑定关系的则不能删除
     * @param req
     * @return
     */
    ServiceResult deleteTrackingProcessItemByItemId(TrackingProcessItemIdReq req);
}
