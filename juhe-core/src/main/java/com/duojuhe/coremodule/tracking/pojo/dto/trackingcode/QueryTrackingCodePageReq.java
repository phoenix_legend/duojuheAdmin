package com.duojuhe.coremodule.tracking.pojo.dto.trackingcode;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryTrackingCodePageReq extends PageHead {
    @ApiModelProperty(value = "防伪编号")
    private String trackingNumber;

    @ApiModelProperty(value = "防伪码")
    private String trackingCode;

    @ApiModelProperty(value = "批次")
    private String trackingBatch;

    @ApiModelProperty(value = "物流码")
    private String transportCode;

    @ApiModelProperty(value = "所属产品")
    private String productName;

    @ApiModelProperty(value = "所属供货商")
    private String supplierName;

    @ApiModelProperty(value = "防伪码状态")
    private String statusCode;

    @ApiModelProperty(value = "开始日期yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}