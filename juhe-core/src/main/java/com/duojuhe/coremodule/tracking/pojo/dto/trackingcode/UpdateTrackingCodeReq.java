package com.duojuhe.coremodule.tracking.pojo.dto.trackingcode;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateTrackingCodeReq extends BaseBean {
    @ApiModelProperty(value = "防伪码ID", required = true)
    @Length(max = 32, message = "防伪码ID不能超过{max}位字符")
    @NotBlank(message = "防伪码ID不能为空")
    private String trackingCodeId;

    @ApiModelProperty(value = "批次", required = true)
    @Length(max = 30, message = "批次不能超过{max}位字符")
    @NotBlank(message = "批次不能为空")
    private String trackingBatch;

    @ApiModelProperty(value = "物流码", required = true)
    @Length(max = 30, message = "物流码不能超过{max}位字符")
    @NotBlank(message = "物流码不能为空")
    private String transportCode;

    @ApiModelProperty(value = "所属产品", required = true)
    @Length(max = 32, message = "所属产品不能超过{max}位字符")
    @NotBlank(message = "所属产品不能为空")
    private String productId;

    @ApiModelProperty(value = "所属供货商", required = true)
    @Length(max = 32, message = "所属供货商不能超过{max}位字符")
    @NotBlank(message = "所属供货商不能为空")
    private String supplierId;

    @ApiModelProperty(value = "防伪码状态，状态FORBID禁用 NORMAL正常", example = "NORMAL",required = true)
    @Length(max = 50, message = "防伪码状态不能超过{max}位字符")
    @Pattern(regexp = RegexpConstants.STATUS, message = "防伪码状态参数错误!")
    @NotBlank(message = "防伪码状态不能为空")
    private String statusCode;
}