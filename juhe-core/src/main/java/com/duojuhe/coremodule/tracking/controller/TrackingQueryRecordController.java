package com.duojuhe.coremodule.tracking.controller;

import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingqueryrecord.QueryTrackingQueryRecordPageReq;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingqueryrecord.QueryTrackingQueryRecordPageRes;
import com.duojuhe.coremodule.tracking.service.TrackingQueryRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/trackingQueryRecord/")
@Api(tags = {"【一物一码，产品溯源 防伪码查询记录】防伪码查询记录管理相关接口"})
@Slf4j
public class TrackingQueryRecordController {

    @Resource
    public TrackingQueryRecordService trackingQueryRecordService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "防伪码查询记录分页查询")
    @PostMapping(value = "queryTrackingQueryRecordPageResList")
    public ServiceResult<PageResult<List<QueryTrackingQueryRecordPageRes>>> queryTrackingQueryRecordPageResList(@Valid @RequestBody @ApiParam(value = "分页查询参数") QueryTrackingQueryRecordPageReq req) {
        return trackingQueryRecordService.queryTrackingQueryRecordPageResList(req);
    }

}