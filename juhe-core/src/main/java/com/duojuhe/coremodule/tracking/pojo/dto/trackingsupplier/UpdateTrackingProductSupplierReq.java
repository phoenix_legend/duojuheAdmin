package com.duojuhe.coremodule.tracking.pojo.dto.trackingsupplier;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateTrackingProductSupplierReq extends SaveTrackingProductSupplierReq {
    @ApiModelProperty(value = "供货商ID", example = "1",required=true)
    @NotBlank(message = "供货商ID不能为空")
    private String supplierId;

}