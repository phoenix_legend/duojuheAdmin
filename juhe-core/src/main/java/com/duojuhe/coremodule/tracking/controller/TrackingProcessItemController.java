package com.duojuhe.coremodule.tracking.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.annotation.RepeatSubmit;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.tracking.pojo.dto.processitem.*;
import com.duojuhe.coremodule.tracking.service.TrackingProcessItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/trackingProcessItem/")
@Api(tags = {"【一物一码，产品溯源 溯源流程项管理】溯源流程项管理相关接口"})
@Slf4j
public class TrackingProcessItemController {
    @Resource
    private TrackingProcessItemService trackingProcessItemService;



    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【查询溯源流程项目】管理界面使用")
    @PostMapping(value = "queryTrackingProcessItemPageResList")
    public ServiceResult<PageResult<List<QueryTrackingProcessItemPageRes>>> queryTrackingProcessItemPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryTrackingProcessItemPageReq req) {
        return trackingProcessItemService.queryTrackingProcessItemPageResList(req);
    }


    @RepeatSubmit
    @ApiOperation(value = "【批量保存】溯源流程项目")
    @PostMapping(value = "batchSaveTrackingProcessItem")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_PROCESS_ITEM, operationType = LogEnum.OperationType.ADD)
    public ServiceResult batchSaveTrackingProcessItem(@Valid @RequestBody @ApiParam(value = "入参类") BatchSaveTrackingProcessItemReq req) {
        return trackingProcessItemService.batchSaveTrackingProcessItem(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【修改】溯源流程项目")
    @PostMapping(value = "updateTrackingProcessItem")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_PROCESS_ITEM, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateTrackingProcessItem(@Valid @RequestBody @ApiParam(value = "入参类") UpdateTrackingProcessItemReq req) {
        return trackingProcessItemService.updateTrackingProcessItem(req);
    }


    @RepeatSubmit
    @ApiOperation(value = "【删除】根据溯源流程项目ID删除溯源流程项目，注意一次仅能删除一个溯源流程项目，存在绑定关系的则不能删除")
    @PostMapping(value = "deleteTrackingProcessItemByItemId")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_PROCESS_ITEM, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteTrackingProcessItemByItemId(@Valid @RequestBody @ApiParam(value = "入参类") TrackingProcessItemIdReq req) {
        return trackingProcessItemService.deleteTrackingProcessItemByItemId(req);
    }
}
