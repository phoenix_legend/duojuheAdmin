package com.duojuhe.coremodule.tracking.mapper;

import com.duojuhe.coremodule.tracking.entity.TrackingProductCategory;
import com.duojuhe.coremodule.tracking.pojo.dto.productcategory.QuerySelectTrackingProductCategoryReq;
import com.duojuhe.coremodule.tracking.pojo.dto.productcategory.QuerySelectTrackingProductCategoryRes;
import com.duojuhe.coremodule.tracking.pojo.dto.productcategory.QueryTrackingProductCategoryPageReq;
import com.duojuhe.coremodule.tracking.pojo.dto.productcategory.QueryTrackingProductCategoryPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TrackingProductCategoryMapper extends TkMapper<TrackingProductCategory> {
    /**
     * 产品分类分页查询
     * @param req
     * @return
     */
    List<QueryTrackingProductCategoryPageRes> queryTrackingProductCategoryPageResList(@Param("req") QueryTrackingProductCategoryPageReq req);

    /**
     * 【查询所有产品分类】供前端选择产品分类
     * @param req
     * @return
     */
    List<QuerySelectTrackingProductCategoryRes> querySelectNormalTrackingProductCategoryResList(@Param("req") QuerySelectTrackingProductCategoryReq req);

}