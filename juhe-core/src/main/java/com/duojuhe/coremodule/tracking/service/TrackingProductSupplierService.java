package com.duojuhe.coremodule.tracking.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingsupplier.*;

import java.util.List;

public interface TrackingProductSupplierService {

    /**
     * 【查询所有产品供货商】产品供货商管理
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryTrackingProductSupplierPageRes>>> queryTrackingProductSupplierPageResList(QueryTrackingProductSupplierPageReq req);


    /**
     * 【查询所有产品供货商】供前端选择产品供货商
     * @param req
     * @return
     */
    ServiceResult<List<QuerySelectTrackingProductSupplierRes>> querySelectNormalTrackingProductSupplierResList(QuerySelectTrackingProductSupplierReq req);


    /**
     * 【保存】产品供货商
     * @param req
     * @return
     */
    ServiceResult saveTrackingProductSupplier(SaveTrackingProductSupplierReq req);

    /**
     * 【修改】产品供货商
     * @param req
     * @return
     */
    ServiceResult updateTrackingProductSupplier(UpdateTrackingProductSupplierReq req);


    /**
     * 【删除】根据产品供货商ID删除产品供货商，注意一次仅能删除一个产品供货商，存在绑定关系的则不能删除
     * @param req
     * @return
     */
    ServiceResult deleteTrackingProductSupplierBySupplierId(TrackingProductSupplierIdReq req);
}
