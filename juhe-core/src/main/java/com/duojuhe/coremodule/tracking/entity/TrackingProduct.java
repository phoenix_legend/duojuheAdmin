package com.duojuhe.coremodule.tracking.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("产品记录表")
@Table(name = "tracking_product")
public class TrackingProduct extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "product_id")
    @Id
    private String productId;

    @ApiModelProperty(value = "产品分类")
    @Column(name = "product_category_id")
    private String productCategoryId;

    @ApiModelProperty(value = "产品编号", required = true)
    @Column(name = "product_number")
    private String productNumber;

    @ApiModelProperty(value = "产品名称", required = true)
    @Column(name = "product_name")
    private String productName;

    @ApiModelProperty(value = "条形码")
    @Column(name = "product_barcode")
    private String productBarcode;

    @ApiModelProperty(value = "产品编码")
    @Column(name = "product_code")
    private String productCode;

    @ApiModelProperty(value = "产品状态")
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "所属租户id")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "产品图片")
    @Column(name = "product_img")
    private String productImg;

    @ApiModelProperty(value = "产品描述")
    @Column(name = "description")
    private String description;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;
}