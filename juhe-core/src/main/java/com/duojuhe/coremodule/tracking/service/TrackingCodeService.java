package com.duojuhe.coremodule.tracking.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingcode.*;

import java.util.List;

public interface TrackingCodeService {
    /**
     * 【查询所有溯源防伪码】溯源防伪码
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryTrackingCodePageRes>>> queryTrackingCodePageResList(QueryTrackingCodePageReq req);

    /**
     * 【保存】批量溯源防伪码
     * @param req
     * @return
     */
    ServiceResult batchSaveTrackingCode(BatchSaveTrackingCodeReq req);

    /**
     * 【保存】溯源防伪码
     * @param req
     * @return
     */
    ServiceResult saveTrackingCode(SaveTrackingCodeReq req);

    /**
     * 【修改】溯源防伪码
     * @param req
     * @return
     */
    ServiceResult updateTrackingCode(UpdateTrackingCodeReq req);

    /**
     * 【删除】根据防伪码ID删除防伪码
     * @param req
     * @return
     */
    ServiceResult deleteTrackingCodeByTrackingCodeId(TrackingCodeIdReq req);
}
