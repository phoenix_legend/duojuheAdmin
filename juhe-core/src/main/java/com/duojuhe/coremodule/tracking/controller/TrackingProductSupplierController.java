package com.duojuhe.coremodule.tracking.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingsupplier.*;
import com.duojuhe.coremodule.tracking.service.TrackingProductSupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/trackingProductSupplier/")
@Api(tags = {"【一物一码，产品溯源 产品供货商管理】产品供货商管理相关接口"})
@Slf4j
public class TrackingProductSupplierController {
    @Resource
    private TrackingProductSupplierService trackingProductSupplierService;



    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【查询产品供货商】产品供货商管理界面使用")
    @PostMapping(value = "queryTrackingProductSupplierPageResList")
    public ServiceResult<PageResult<List<QueryTrackingProductSupplierPageRes>>> queryTrackingProductSupplierPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryTrackingProductSupplierPageReq req) {
        return trackingProductSupplierService.queryTrackingProductSupplierPageResList(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【查询所有产品供货商】查询可供前端选择使用的产品供货商")
    @PostMapping(value = "querySelectNormalTrackingProductSupplierResList")
    public ServiceResult<List<QuerySelectTrackingProductSupplierRes>> querySelectNormalTrackingProductSupplierResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySelectTrackingProductSupplierReq req) {
        return trackingProductSupplierService.querySelectNormalTrackingProductSupplierResList(req);
    }

    @ApiOperation(value = "【保存】产品供货商")
    @PostMapping(value = "saveTrackingProductSupplier")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_SUPPLIER_ADMIN, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveTrackingProductSupplier(@Valid @RequestBody @ApiParam(value = "入参类") SaveTrackingProductSupplierReq req) {
        return trackingProductSupplierService.saveTrackingProductSupplier(req);
    }


    @ApiOperation(value = "【修改】产品供货商")
    @PostMapping(value = "updateTrackingProductSupplier")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_SUPPLIER_ADMIN, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateTrackingProductSupplier(@Valid @RequestBody @ApiParam(value = "入参类") UpdateTrackingProductSupplierReq req) {
        return trackingProductSupplierService.updateTrackingProductSupplier(req);
    }


    @ApiOperation(value = "【删除】根据产品供货商ID删除产品供货商，注意一次仅能删除一个产品供货商，存在绑定关系的则不能删除")
    @PostMapping(value = "deleteTrackingProductSupplierBySupplierId")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_SUPPLIER_ADMIN, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteTrackingProductSupplierBySupplierId(@Valid @RequestBody @ApiParam(value = "入参类") TrackingProductSupplierIdReq req) {
        return trackingProductSupplierService.deleteTrackingProductSupplierBySupplierId(req);
    }
}
