package com.duojuhe.coremodule.tracking.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingproduct.*;
import com.duojuhe.coremodule.tracking.service.TrackingProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/trackingProduct/")
@Api(tags = {"【一物一码，产品溯源 产品管理】产品管理相关接口"})
@Slf4j
public class TrackingProductController {
    @Resource
    private TrackingProductService trackingProductService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【查询产品】产品管理界面使用")
    @PostMapping(value = "queryTrackingProductPageResList")
    public ServiceResult<PageResult<List<QueryTrackingProductPageRes>>> queryTrackingProductPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryTrackingProductPageReq req) {
        return trackingProductService.queryTrackingProductPageResList(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【查询所有产品】查询可供前端选择使用的产品")
    @PostMapping(value = "querySelectTrackingProductPageResList")
    public ServiceResult<PageResult<List<QueryTrackingProductPageRes>>> querySelectTrackingProductPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryTrackingProductPageReq req) {
        return trackingProductService.querySelectTrackingProductPageResList(req);
    }

    @ApiOperation(value = "【保存】产品")
    @PostMapping(value = "saveTrackingProduct")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_PRODUCT_ADMIN, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveTrackingProduct(@Valid @RequestBody @ApiParam(value = "入参类") SaveTrackingProductReq req) {
        return trackingProductService.saveTrackingProduct(req);
    }


    @ApiOperation(value = "【修改】产品")
    @PostMapping(value = "updateTrackingProduct")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_PRODUCT_ADMIN, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateTrackingProduct(@Valid @RequestBody @ApiParam(value = "入参类") UpdateTrackingProductReq req) {
        return trackingProductService.updateTrackingProduct(req);
    }


    @ApiOperation(value = "【删除】根据产品ID删除产品，注意一次仅能删除一个产品，存在绑定关系的则不能删除")
    @PostMapping(value = "deleteTrackingProductByProductId")
    @OperationLog(moduleName = LogEnum.ModuleName.TRACKING_PRODUCT_ADMIN, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteTrackingProductByProductId(@Valid @RequestBody @ApiParam(value = "入参类") TrackingProductIdReq req) {
        return trackingProductService.deleteTrackingProductByProductId(req);
    }
}
