package com.duojuhe.coremodule.tracking.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("防伪码表")
@Table(name = "tracking_code")
public class TrackingCode extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "tracking_code_id")
    @Id
    private String trackingCodeId;

    @ApiModelProperty(value = "防伪编号", required = true)
    @Column(name = "tracking_number")
    private String trackingNumber;

    @ApiModelProperty(value = "防伪码地址", required = true)
    @Column(name = "tracking_code_url")
    private String trackingCodeUrl;

    @ApiModelProperty(value = "防伪码", required = true)
    @Column(name = "tracking_code")
    private String trackingCode;

    @ApiModelProperty(value = "批次", required = true)
    @Column(name = "tracking_batch")
    private String trackingBatch;

    @ApiModelProperty(value = "物流码", required = true)
    @Column(name = "transport_code")
    private String transportCode;

    @ApiModelProperty(value = "所属产品", required = true)
    @Column(name = "product_id")
    private String productId;

    @ApiModelProperty(value = "所属供货商", required = true)
    @Column(name = "supplier_id")
    private String supplierId;

    @ApiModelProperty(value = "防伪码状态")
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "所属租户id")
    @Column(name = "tenant_id")
    private String tenantId;
}