package com.duojuhe.coremodule.tracking.mapper;

import com.duojuhe.coremodule.tracking.entity.TrackingProductSupplier;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingsupplier.QuerySelectTrackingProductSupplierReq;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingsupplier.QuerySelectTrackingProductSupplierRes;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingsupplier.QueryTrackingProductSupplierPageReq;
import com.duojuhe.coremodule.tracking.pojo.dto.trackingsupplier.QueryTrackingProductSupplierPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TrackingProductSupplierMapper extends TkMapper<TrackingProductSupplier> {
    /**
     * 供货商分页查询
     * @param req
     * @return
     */
    List<QueryTrackingProductSupplierPageRes> queryTrackingProductSupplierPageResList(@Param("req") QueryTrackingProductSupplierPageReq req);

    /**
     * 溯源供货商查询 一般供前端下拉选择使用
     * @param req
     * @return
     */
    List<QuerySelectTrackingProductSupplierRes> querySelectNormalTrackingProductSupplierResList(@Param("req") QuerySelectTrackingProductSupplierReq req);


}