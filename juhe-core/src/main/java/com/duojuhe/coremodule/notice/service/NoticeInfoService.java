package com.duojuhe.coremodule.notice.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.notice.pojo.dto.noticeinfo.*;

import java.util.List;

public interface NoticeInfoService {

    /**
     * 【通知公告详情】根据通知公告发布ID获取通知公告详情
     * @param req
     * @return
     */
    ServiceResult<QueryNoticeInfoRes> queryNoticeInfoResByNoticeId(NoticeInfoIdReq req);

    /**
     * 【分页查询通知公告发布list】分页查询通知公告发布list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryNoticeInfoPageRes>>> queryNoticeInfoPageResList(QueryNoticeInfoPageReq req);


    /**
     * 【分页查询我发布的list】分页查询我发布的list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryMyNoticeInfoPageRes>>> queryMyNoticeInfoPageResList(QueryMyNoticeInfoPageReq req);

    /**
     * 【保存】通知公告发布通知公告
     * @param req
     * @return
     */
    ServiceResult saveNoticeInfo(SaveNoticeInfoReq req);

    /**
     * 【修改】通知公告发布通知公告
     * @param req
     * @return
     */
    ServiceResult updateNoticeInfo(UpdateNoticeInfoReq req);

    /**
     * 【彻底删除】根据通知公告发布ID彻底删除通知公告发布，注意一次仅能彻底删除一个通知公告发布
     * @param req
     * @return
     */
    ServiceResult deleteNoticeInfoByNoticeId(NoticeInfoIdReq req);

}
