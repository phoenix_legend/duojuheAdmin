package com.duojuhe.coremodule.notice.mapper;

import com.duojuhe.coremodule.notice.entity.NoticeInfo;
import com.duojuhe.coremodule.notice.pojo.dto.noticeinfo.*;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NoticeInfoMapper extends TkMapper<NoticeInfo> {
    /**
     * 【通知公告详情】根据通知公告发布ID获取通知公告详情
     * @param noticeId
     * @return
     */
    QueryNoticeInfoRes queryNoticeInfoResByNoticeId(@Param("noticeId") String noticeId);

    /**
     * 分页查询 根据条件查询发布通知公告list
     *
     * @return
     */
    List<QueryNoticeInfoPageRes> queryNoticeInfoPageResList(@Param("req") QueryNoticeInfoPageReq req);


    /**
     * 分页查询 根据条件查询我的发布通知公告list
     *
     * @return
     */
    List<QueryMyNoticeInfoPageRes> queryMyNoticeInfoPageResList(@Param("req") QueryMyNoticeInfoPageReq req);
}