package com.duojuhe.coremodule.notice.pojo.dto.noticeinfo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateNoticeInfoReq extends SaveNoticeInfoReq {
    @ApiModelProperty(value = "公告ID", example = "1",required=true)
    @NotBlank(message = "公告ID不能为空")
    private String noticeId;
}