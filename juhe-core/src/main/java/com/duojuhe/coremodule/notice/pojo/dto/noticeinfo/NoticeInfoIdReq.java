package com.duojuhe.coremodule.notice.pojo.dto.noticeinfo;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NoticeInfoIdReq extends BaseBean {
    @ApiModelProperty(value = "公告ID", example = "1",required=true)
    @NotBlank(message = "公告ID不能为空")
    private String noticeId;
}
