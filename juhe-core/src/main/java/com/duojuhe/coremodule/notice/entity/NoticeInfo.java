package com.duojuhe.coremodule.notice.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("通知信息记录表")
@Table(name = "notice_info")
public class NoticeInfo extends BaseBean {
    @ApiModelProperty(value = "主键id", required = true)
    @Column(name = "notice_id")
    @Id
    private String noticeId;

    @ApiModelProperty(value = "所属分类id", required = true)
    @Column(name = "category_id")
    private String categoryId;

    @ApiModelProperty(value = "通知标题", required = true)
    @Column(name = "notice_title")
    private String noticeTitle;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "创建租户id")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "数据允许查询权限，ALL_AUTH 所有用户,其他字符时表示指定用户可见")
    @Column(name = "data_allow_auth")
    private String dataAllowAuth;

    @ApiModelProperty(value = "通知内容")
    @Column(name = "notice_content")
    private String noticeContent;
}