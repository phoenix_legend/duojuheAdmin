package com.duojuhe.coremodule.notice.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.notice.pojo.dto.noticeinfo.*;
import com.duojuhe.coremodule.notice.service.NoticeInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/noticeInfo/")
@Api(tags = {"【通知公告发布管理】通知公告发布管理相关接口"})
@Slf4j
public class NoticeInfoController {
    @Resource
    private NoticeInfoService noticeInfoService;

    @ApiOperation(value = "【通知公告详情】根据通知公告发布ID获取通知公告详情")
    @PostMapping(value = "queryNoticeInfoResByNoticeId")
    public ServiceResult<QueryNoticeInfoRes> queryNoticeInfoResByNoticeId(@Valid @RequestBody @ApiParam(value = "入参类") NoticeInfoIdReq req) {
        return noticeInfoService.queryNoticeInfoResByNoticeId(req);
    }


    @ApiOperation(value = "【分页查询】分页查询通知公告发布list")
    @PostMapping(value = "queryNoticeInfoPageResList")
    public ServiceResult<PageResult<List<QueryNoticeInfoPageRes>>> queryNoticeInfoPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryNoticeInfoPageReq req) {
        return noticeInfoService.queryNoticeInfoPageResList(req);
    }


    @ApiOperation(value = "【分页查询我发布的】分页查询我发布的list")
    @PostMapping(value = "queryMyNoticeInfoPageResList")
    public ServiceResult<PageResult<List<QueryMyNoticeInfoPageRes>>> queryMyNoticeInfoPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryMyNoticeInfoPageReq req) {
        return noticeInfoService.queryMyNoticeInfoPageResList(req);
    }


    @ApiOperation(value = "【保存】通知公告发布")
    @PostMapping(value = "saveNoticeInfo")
    @OperationLog(moduleName = LogEnum.ModuleName.NOTICE_INFO, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveNoticeInfo(@Valid @RequestBody @ApiParam(value = "入参类") SaveNoticeInfoReq req) {
        return noticeInfoService.saveNoticeInfo(req);
    }

    @ApiOperation(value = "【修改】通知公告发布")
    @PostMapping(value = "updateNoticeInfo")
    @OperationLog(moduleName = LogEnum.ModuleName.NOTICE_INFO, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateNoticeInfo(@Valid @RequestBody @ApiParam(value = "入参类") UpdateNoticeInfoReq req) {
        return noticeInfoService.updateNoticeInfo(req);
    }


    @ApiOperation(value = "【删除】根据ID删除通知公告发布，注意一次仅能删除一个通知公告发布")
    @PostMapping(value = "deleteNoticeInfoByNoticeId")
    @OperationLog(moduleName = LogEnum.ModuleName.NOTICE_INFO, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteNoticeInfoByNoticeId(@Valid @RequestBody @ApiParam(value = "入参类") NoticeInfoIdReq req) {
        return noticeInfoService.deleteNoticeInfoByNoticeId(req);
    }
}
