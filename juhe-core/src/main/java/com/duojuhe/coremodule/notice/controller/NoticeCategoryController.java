package com.duojuhe.coremodule.notice.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.notice.pojo.dto.category.*;
import com.duojuhe.coremodule.notice.service.NoticeCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/noticeCategory/")
@Api(tags = {"【通知公告分类管理】通知公告分类管理相关接口"})
@Slf4j
public class NoticeCategoryController {
    @Resource
    private NoticeCategoryService noticeCategoryService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【通知公告分类】分类管理界面使用")
    @PostMapping(value = "queryNoticeCategoryPageResList")
    public ServiceResult<PageResult<List<QueryNoticeCategoryPageRes>>> queryNoticeCategoryPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryNoticeCategoryPageReq req) {
        return noticeCategoryService.queryNoticeCategoryPageResList(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【通知公告分类】查询可供前端选择使用的分类")
    @PostMapping(value = "querySelectNormalNoticeCategoryResList")
    public ServiceResult<List<QuerySelectNoticeCategoryRes>> querySelectNormalNoticeCategoryResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySelectNoticeCategoryReq req) {
        return noticeCategoryService.querySelectNormalNoticeCategoryResList(req);
    }


    @ApiOperation(value = "【保存】通知公告分类")
    @PostMapping(value = "saveNoticeCategory")
    @OperationLog(moduleName = LogEnum.ModuleName.NOTICE_CATEGORY, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveNoticeCategory(@Valid @RequestBody @ApiParam(value = "入参类") SaveNoticeCategoryReq req) {
        return noticeCategoryService.saveNoticeCategory(req);
    }


    @ApiOperation(value = "【修改】通知公告分类")
    @PostMapping(value = "updateNoticeCategory")
    @OperationLog(moduleName = LogEnum.ModuleName.NOTICE_CATEGORY, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateNoticeCategory(@Valid @RequestBody @ApiParam(value = "入参类") UpdateNoticeCategoryReq req) {
        return noticeCategoryService.updateNoticeCategory(req);
    }


    @ApiOperation(value = "【删除】根据通知公告分类ID删除通知公告分类，注意一次仅能删除一个溯源流程类别，存在绑定关系的则不能删除")
    @PostMapping(value = "deleteNoticeCategoryByCategoryId")
    @OperationLog(moduleName = LogEnum.ModuleName.NOTICE_CATEGORY, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteNoticeCategoryByCategoryId(@Valid @RequestBody @ApiParam(value = "入参类") NoticeCategoryIdReq req) {
        return noticeCategoryService.deleteNoticeCategoryByCategoryId(req);
    }
}
