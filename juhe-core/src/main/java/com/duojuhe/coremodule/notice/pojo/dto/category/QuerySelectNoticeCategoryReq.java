package com.duojuhe.coremodule.notice.pojo.dto.category;

import com.duojuhe.common.bean.DataScopeFilterBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySelectNoticeCategoryReq extends DataScopeFilterBean {
    @ApiModelProperty(value = "类别名称")
    private String categoryName;
}