package com.duojuhe.coremodule.notice.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.notice.pojo.dto.category.*;

import java.util.List;


public interface NoticeCategoryService {
    /**
     * 【查询所有公告分类】公告分类管理
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryNoticeCategoryPageRes>>> queryNoticeCategoryPageResList(QueryNoticeCategoryPageReq req);


    /**
     * 【查询所有公告分类】供前端选择公告分类
     * @param req
     * @return
     */
    ServiceResult<List<QuerySelectNoticeCategoryRes>> querySelectNormalNoticeCategoryResList(QuerySelectNoticeCategoryReq req);


    /**
     * 【保存】公告分类
     * @param req
     * @return
     */
    ServiceResult saveNoticeCategory(SaveNoticeCategoryReq req);

    /**
     * 【修改】公告分类
     * @param req
     * @return
     */
    ServiceResult updateNoticeCategory(UpdateNoticeCategoryReq req);

    /**
     * 【删除】公告分类，存在绑定关系的则不能删除
     * @param req
     * @return
     */
    ServiceResult deleteNoticeCategoryByCategoryId(NoticeCategoryIdReq req);
}
