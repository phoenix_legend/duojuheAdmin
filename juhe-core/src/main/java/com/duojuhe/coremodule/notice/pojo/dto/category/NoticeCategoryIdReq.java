package com.duojuhe.coremodule.notice.pojo.dto.category;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class NoticeCategoryIdReq extends BaseBean {
    @ApiModelProperty(value = "分类ID", example = "1",required=true)
    @NotBlank(message = "分类ID不能为空")
    private String categoryId;
}