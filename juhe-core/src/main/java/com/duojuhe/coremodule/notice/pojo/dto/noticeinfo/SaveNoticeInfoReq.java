package com.duojuhe.coremodule.notice.pojo.dto.noticeinfo;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveNoticeInfoReq extends BaseBean {

    @ApiModelProperty(value = "公告分类id", required = true)
    @NotBlank(message = "公告分类ID不能为空")
    private String categoryId;

    @ApiModelProperty(value = "公告标题", required = true)
    @NotBlank(message = "公告标题不能为空")
    @Length(max = 250, message = "公告标题不得超过{max}位字符")
    private String noticeTitle;

    @ApiModelProperty(value = "排序，只能为纯数字", example = "1",required=true)
    @NotNull(message = "排序不能为空")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;

    @ApiModelProperty(value = "公告内容",required=true)
    @NotBlank(message = "公告内容不能为空")
    private String noticeContent;
}