package com.duojuhe.coremodule.notice.pojo.dto.category;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateNoticeCategoryReq extends SaveNoticeCategoryReq {
    @ApiModelProperty(value = "通知公告分类ID", example = "1",required=true)
    @NotBlank(message = "通知公告分类ID不能为空")
    private String categoryId;

}