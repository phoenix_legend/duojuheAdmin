package com.duojuhe.coremodule.notice.mapper;


import com.duojuhe.coremodule.notice.entity.NoticeCategory;
import com.duojuhe.coremodule.notice.pojo.dto.category.QueryNoticeCategoryPageReq;

import com.duojuhe.coremodule.notice.pojo.dto.category.QueryNoticeCategoryPageRes;
import com.duojuhe.coremodule.notice.pojo.dto.category.QuerySelectNoticeCategoryReq;
import com.duojuhe.coremodule.notice.pojo.dto.category.QuerySelectNoticeCategoryRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NoticeCategoryMapper extends TkMapper<NoticeCategory> {
    /**
     * 公告分类分页查询
     * @param req
     * @return
     */
    List<QueryNoticeCategoryPageRes> queryNoticeCategoryPageResList(@Param("req") QueryNoticeCategoryPageReq req);

    /**
     * 公告分类查询 一般供前端下拉选择使用
     * @param req
     * @return
     */
    List<QuerySelectNoticeCategoryRes> querySelectNormalNoticeCategoryResList(@Param("req") QuerySelectNoticeCategoryReq req);

}