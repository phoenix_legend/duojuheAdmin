package com.duojuhe.coremodule;


import com.duojuhe.common.bean.UserTokenInfoVo;
import com.duojuhe.cache.LoginUserTokenCache;


public class BaseService {
    /**
     * 获取当前登录用户的相关数据信息
     *
     * @return
     */
    protected UserTokenInfoVo getCurrentLoginUserInfoDTO() {
        return LoginUserTokenCache.getCurrentLoginUserInfoDTO();
    }

    /**
     * 获取当前登录用户的ID
     *
     * @return
     */
    protected String getCurrentLoginUserId() {
        return getCurrentLoginUserInfoDTO().getUserId();
    }
}
