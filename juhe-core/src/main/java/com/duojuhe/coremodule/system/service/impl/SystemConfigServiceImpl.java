package com.duojuhe.coremodule.system.service.impl;

import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.system.service.SystemConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


/**
 * 系统配置
 */
@Slf4j
@Service
public class SystemConfigServiceImpl extends BaseService implements SystemConfigService {

}
