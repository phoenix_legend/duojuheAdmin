package com.duojuhe.coremodule.system.pojo.dto.dept;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * 部门修改请求类
 *
 * @date 2018/5/23.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateSystemDeptReq extends SaveSystemDeptReq {
    @ApiModelProperty(value = "部门ID", example = "1",required=true)
    @NotBlank(message = "部门ID不能为空")
    private String deptId;
}
