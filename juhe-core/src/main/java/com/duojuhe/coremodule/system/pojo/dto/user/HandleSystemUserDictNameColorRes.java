package com.duojuhe.coremodule.system.pojo.dto.user;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HandleSystemUserDictNameColorRes extends BaseBean {

    @ApiModelProperty(value = "状态：FORBID禁用 NORMAL正常", example = "1")
    private String statusCode;

    @ApiModelProperty(value = "状态中文 FORBID禁用 NORMAL正常", example = "1")
    private String statusName;

    @ApiModelProperty(value = "状态显示颜色", example = "1")
    private String statusColor;

    @ApiModelProperty(value = "用户类型", example = "1")
    private String userTypeCode;

    @ApiModelProperty(value = "用户类型", example = "1")
    private String userTypeName;

    @ApiModelProperty(value = "用户类型显示颜色", example = "1")
    private String userTypeColor;

    @ApiModelProperty(value = "性别")
    private String genderCode;

    @ApiModelProperty(value = "性别中文")
    private String genderName;

    @ApiModelProperty(value = "性别显示颜色")
    private String genderColor;

}
