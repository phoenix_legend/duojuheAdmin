package com.duojuhe.coremodule.system.pojo.dto.dept;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SelectSystemDeptRes extends BaseBean {

    @ApiModelProperty(value = "主键", example = "1")
    private String deptId;

    @ApiModelProperty(value = "部门名称", example = "1")
    private String deptName;
}
