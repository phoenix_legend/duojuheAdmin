package com.duojuhe.coremodule.system.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.role.SystemRoleIdReq;
import com.duojuhe.coremodule.system.pojo.dto.tenant.*;
import com.duojuhe.coremodule.system.pojo.dto.user.ResetSystemUserPasswordReq;

import java.util.List;

public interface SystemTenantService {
    /**
     * 【分页查询】分页查询租户list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QuerySystemTenantPageRes>>> querySystemTenantPageResList(QuerySystemTenantPageReq req);

    /**
     * 【保存】租户信息
     * @param req
     * @return
     */
    ServiceResult saveSystemTenant(SaveSystemTenantReq req);


    /**
     * 【修改】租户信息
     * @param req
     * @return
     */
    ServiceResult updateSystemTenant(UpdateSystemTenantReq req);


    /**
     * 【改变状态】根据租户id改变租户状态
     * @param req
     * @return
     */
    ServiceResult updateSystemTenantStatusByTenantId(SystemTenantIdReq req);


    /**
     * 【租户绑定菜单-渲染租户绑定菜单】查询TenantId对应的菜单权限集合
     * @param req
     * @return
     */
    ServiceResult<List<String>> querySystemMenuIdListByTenantId(SystemTenantIdReq req);

    /**
     * 【重置密码】根据租户用户id重置密码
     * @param req
     * @return
     */
    ServiceResult resetSystemTenantUserPassword(ResetSystemTenantUserPasswordReq req);

}
