package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("角色和部门的关系表")
@Table(name = "system_role_dept")
public class SystemRoleDept extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "id")
    @Id
    private String id;

    @ApiModelProperty(value = "角色id", required = true)
    @Column(name = "role_id")
    private String roleId;

    @ApiModelProperty(value = "部门id", required = true)
    @Column(name = "dept_id")
    private String deptId;

    @ApiModelProperty(value = "所属租户id", required = true)
    @Column(name = "tenant_id")
    private String tenantId;
}