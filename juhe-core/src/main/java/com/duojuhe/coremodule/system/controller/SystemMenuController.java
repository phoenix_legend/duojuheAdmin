package com.duojuhe.coremodule.system.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.enums.SystemEnum;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.menu.*;
import com.duojuhe.coremodule.system.service.SystemMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/systemMenu/")
@Api(tags = {"【菜单管理】菜单管理相关接口"})
@Slf4j
public class SystemMenuController {

    @Resource
    private SystemMenuService systemMenuService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【菜单管理列表-查询tree型结构】 查询所有菜单列表，一般用于菜单管理列表界面【包含功能按钮类型】")
    @PostMapping(value = "querySystemMenuTreeResList")
    public ServiceResult<List<QuerySystemMenuTreeRes>> querySystemMenuTreeResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySystemMenuTreeReq req) {
        return systemMenuService.querySystemMenuTreeResList(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【角色绑定权限-查询tree型结构】获取状态是可用的菜单资源，一般用于角色绑定权限使用【包含功能按钮类型】")
    @PostMapping(value = "queryBindingRoleUseMenuTreeResList")
    public ServiceResult<List<SelectSystemMenuTreeRes>> queryBindingRoleUseMenuTreeResList() {
        return systemMenuService.querySelectNormalSystemMenuTreeResListByMenuType(null);
    }



    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【租户绑定菜单权限-查询tree型结构】获取状态是可用的菜单资源，一般用于租户绑定菜单使用【包含功能按钮类型】")
    @PostMapping(value = "queryTenantBindingUseMenuTreeResList")
    public ServiceResult<List<SelectSystemMenuTreeRes>> queryTenantBindingUseMenuTreeResList() {
        return systemMenuService.queryTenantBindingUseMenuTreeResList();
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【下拉选择使用-查询tree型结构】获取状态是可用的导航菜单资源，一般用于菜单管理下拉选择上级菜单【不包含功能按钮】")
    @PostMapping(value = "querySelectNavigationMenuTreeResList")
    public ServiceResult<List<SelectSystemMenuTreeRes>> querySelectNavigationMenuTreeResList() {
        return systemMenuService.querySelectNormalSystemMenuTreeResListByMenuType(SystemEnum.MENU_TYPE.NAVIGATION.getKey());
    }

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【根据ID查询详情】根据菜单ID查询菜单详情")
    @PostMapping(value = "querySystemMenuResByMenuId")
    public ServiceResult<QuerySystemMenuRes> querySystemMenuResByMenuId(@Valid @RequestBody @ApiParam(value = "入参类") SystemMenuIdReq req) {
        return systemMenuService.querySystemMenuResByMenuId(req);
    }

    @ApiOperation(value = "【保存】菜单信息")
    @PostMapping(value = "saveSystemMenu")
    @OperationLog(moduleName = LogEnum.ModuleName.MENU_ADMIN, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveSystemMenu(@Valid @RequestBody @ApiParam(value = "入参类") SaveSystemMenuReq req) {
        return systemMenuService.saveSystemMenu(req);
    }

    @ApiOperation(value = "【修改】菜单信息")
    @PostMapping(value = "updateSystemMenu")
    @OperationLog(moduleName = LogEnum.ModuleName.MENU_ADMIN, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateSystemMenu(@Valid @RequestBody @ApiParam(value = "入参类") UpdateSystemMenuReq req) {
        return systemMenuService.updateSystemMenu(req);
    }

    @ApiOperation(value = "【删除】根据id删除菜单，注意一次仅能删除一个菜单，存在子项则不能删除")
    @PostMapping(value = "deleteSystemMenuByMenuId")
    @OperationLog(moduleName = LogEnum.ModuleName.MENU_ADMIN, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteSystemMenuByMenuId(@Valid @RequestBody @ApiParam(value = "入参类") SystemMenuIdReq req){
        return systemMenuService.deleteSystemMenuByMenuId(req);
    }
}
