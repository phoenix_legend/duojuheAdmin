package com.duojuhe.coremodule.system.pojo.dto.log;

import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemLogRes extends HandleSystemLogDictNameColorRes {
    @ApiModelProperty(value = "主键")
    private String logId;

    @ApiModelProperty(value = "登录名", example = "1")
    private String loginName;

    @ApiModelProperty(value = "创建用户姓名")
    private String createUserName;

    @ApiModelProperty(value = "操作时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "方法名称")
    private String methodName;

    @ApiModelProperty(value = "操作IP")
    private String operationIp;

    @ApiModelProperty(value = "操作IP所属区域")
    private String ipRegion;

    @ApiModelProperty(value = "请求方法")
    private String requestMethod;

    @ApiModelProperty(value = "模块名称")
    private String moduleName;

    @ApiModelProperty(value = "创建部门名称")
    private String createDeptName;

    @ApiModelProperty(value = "请求url")
    private String requestUrl;

    @ApiModelProperty(value = "异常内容")
    private String exceptionContent;

    @ApiModelProperty(value = "请求参数")
    private String requestParameter;

    @ApiModelProperty(value = "返回结果")
    private String returnResult;

    @ApiModelProperty(value = "备注")
    private String remark;
}
