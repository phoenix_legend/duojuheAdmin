package com.duojuhe.coremodule.system.pojo.dto.role;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * 角色id
 *
 * @Date:2020/5/21
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SystemRoleIdReq extends BaseBean {
    @ApiModelProperty(value = "角色", example = "1",required=true)
    @NotBlank(message = "角色ID不能为空")
    private String roleId;
}
