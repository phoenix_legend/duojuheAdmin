package com.duojuhe.coremodule.system.pojo.dto.dict;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HandleSystemDictDictNameColorRes extends BaseBean {
    @ApiModelProperty(value = "字典类型:BUSINESS_DICT业务字典 SYSTEM_DICT系统字典")
    private String dictTypeCode;

    @ApiModelProperty(value = "字典类型中文")
    private String dictTypeName;

    @ApiModelProperty(value = "字典类型显示颜色")
    private String dictTypeColor;

    @ApiModelProperty(value = "状态：FORBID禁用 NORMAL正常", example = "1")
    private String statusCode;

    @ApiModelProperty(value = "状态中文 FORBID禁用 NORMAL正常", example = "1")
    private String statusName;

    @ApiModelProperty(value = "状态显示颜色", example = "1")
    private String statusColor;
}
