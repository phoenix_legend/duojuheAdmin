package com.duojuhe.coremodule.system.pojo.dto.dept;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


/**
 * 部门id
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SystemDeptIdReq extends BaseBean {
    @ApiModelProperty(value = "部门ID", example = "1",required=true)
    @NotBlank(message = "部门ID不能为空")
    private String deptId;
}
