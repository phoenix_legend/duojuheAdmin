package com.duojuhe.coremodule.system.pojo.dto.dict;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


/**
 * 保存字典
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveSystemDictReq extends BaseBean {
    @ApiModelProperty(value = "父级ID，长度1-32 -1标识是一级数据字典", example = "-1",required=true)
    @Length(max = 32, message = "父级ID不得超过{max}位字符")
    @NotBlank(message = "父级ID不能为空")
    private String parentId;

    @ApiModelProperty(value = "字典名称，只能填写中文英文数字和下划线，长度1-50", example = "1",required=true)
    @NotBlank(message = "字典名称不能为空")
    @Length(max = 50, message = "字典名称不得超过{max}位字符")
    private String dictName;

    @ApiModelProperty(value = "字典编码，只能填写英文数字和下划线，长度1-50个字符", example = "1",required=true)
    @NotBlank(message = "字典编码不能为空")
    @Pattern(regexp = RegexpConstants.EN_LENGTH_NUMBER_LINE, message = "字典编码只能填写英文数字和下划线!")
    @Length(min = 1, max = 50, message = "字典编码不得超过{max}位字符")
    private String dictCode;

    @ApiModelProperty(value = "字典状态，状态FORBID禁用 NORMAL正常", example = "NORMAL",required = true)
    @Pattern(regexp = RegexpConstants.STATUS, message = "字典状态参数错误!")
    @NotBlank(message = "字典状态不能为空")
    private String statusCode;

    @ApiModelProperty(value = "排序，只能为纯数字", example = "1",required=true)
    @NotNull(message = "排序不能为空")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;

    @ApiModelProperty(value = "备注", example = "1")
    @Pattern(regexp = RegexpConstants.REMARK, message = "备注禁止输入特殊字符!")
    @Length(max = 100, message = "备注不得超过{max}位字符")
    private  String remark;

    @ApiModelProperty(value = "数据字典显示颜色", example = "#000000")
    @Length(max = 50, message = "数据字典显示颜色长度不得超过{max}位字符")
    private String dictColor;
}
