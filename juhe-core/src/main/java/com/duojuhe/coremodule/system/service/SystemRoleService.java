package com.duojuhe.coremodule.system.service;

import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.role.*;

import java.util.List;

/**
 * 系统角色相关接口
 */
public interface SystemRoleService {

    /**
     * 【分页查询】分页查询角色list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QuerySystemRolePageRes>>> querySystemRolePageResList(QuerySystemRolePageReq req);


    /**
     * 【下拉选择使用-查询所有角色】查询可供前端选择使用的角色，一般用于用户添加下拉选择使用
     * @param req
     * @return
     */
    ServiceResult<List<SelectSystemRoleRes>> queryNormalSelectSystemRoleResList(SelectSystemRoleReq req);


    /**
     * 【根据ID查询详情】根据角色ID查询角色详情
     * @param req
     * @return
     */
    ServiceResult<QuerySystemRoleRes> querySystemRoleResByRoleId(SystemRoleIdReq req);


    /**
     * 【保存】角色信息
     * @param req
     * @return
     */
    ServiceResult saveSystemRole(SaveSystemRoleReq req);

    /**
     * 【修改】角色信息
     * @param req
     * @return
     */
    ServiceResult updateSystemRole(UpdateSystemRoleReq req);

    /**
     * 【删除】根据角色ID删除角色，注意一次仅能删除一个角色，存在绑定关系的则不能删除
     * @param req
     * @return
     */
    ServiceResult deleteSystemRoleByRoleId(SystemRoleIdReq req);

    /**
     * 【角色授权】给角色赋权限，menuIdList为空表示清除该角色所有权限
     * @param req
     * @return
     */
    ServiceResult saveRoleAuthorization(SaveRoleAuthorizationReq req);

    /**
     * 【角色绑定菜单-渲染角色绑定菜单】查询roleId对应的菜单权限集合，一般用于角色功能授权编辑时渲染已经用于的权限勾选
     * @param req
     * @return
     */
    ServiceResult<List<String>> querySystemMenuIdListByRoleId(SystemRoleIdReq req);

    /**
     * 【角色绑定部门-渲染角色绑定部门】查询roleId对应的部门ID权限集合，一般用于角色数据授权编辑时渲染已经用于的权限勾选
     * @param req
     * @return
     */
    ServiceResult<List<String>> querySystemDeptIdListByRoleId(SystemRoleIdReq req);
}
