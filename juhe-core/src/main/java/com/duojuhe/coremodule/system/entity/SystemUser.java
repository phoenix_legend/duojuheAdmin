package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("system管理人员")
@Table(name = "system_user")
public class SystemUser extends BaseBean {
    @ApiModelProperty(value = "主键字段", required = true)
    @Column(name = "user_id")
    @Id
    private String userId;

    @ApiModelProperty(value = "用户所属模块编码", required = true)
    @Column(name = "module_code")
    private String moduleCode;

    @ApiModelProperty(value = "用户类型DEPT_USER部门用户,TENANT_ADMIN租户超管,SUPER_ADMIN超管", required = true)
    @Column(name = "user_type_code")
    private String userTypeCode;

    @ApiModelProperty(value = "头像")
    @Column(name = "head_portrait")
    private String headPortrait;

    @ApiModelProperty(value = "登录名唯一", required = true)
    @Column(name = "login_name")
    private String loginName;

    @ApiModelProperty(value = "用户编号，具有唯一", required = true)
    @Column(name = "user_number")
    private String userNumber;

    @ApiModelProperty(value = "真实姓名")
    @Column(name = "real_name")
    private String realName;

    @JsonIgnore
    @ApiModelProperty(value = "密码", required = true)
    @Column(name = "password")
    private String password;

    @ApiModelProperty(value = "手机号码唯一", required = true)
    @Column(name = "mobile_number")
    private String mobileNumber;

    @ApiModelProperty(value = "角色ID")
    @Column(name = "role_id")
    private String roleId;

    @ApiModelProperty(value = "岗位id")
    @Column(name = "post_id")
    private String postId;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常")
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "最后更新用户")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "创建用户")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建部门ID")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "座右铭")
    @Column(name = "motto")
    private String motto;

    @ApiModelProperty(value = "性别")
    @Column(name = "gender_code")
    private String genderCode;
}