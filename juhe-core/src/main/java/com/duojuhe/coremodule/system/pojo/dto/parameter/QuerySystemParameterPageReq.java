package com.duojuhe.coremodule.system.pojo.dto.parameter;

import com.duojuhe.common.bean.PageHead;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemParameterPageReq extends PageHead {
    @ApiModelProperty(value = "参数名称")
    private String parameterName;

    @ApiModelProperty(value = "参数编码")
    private String parameterCode;

    @ApiModelProperty(value = "参数取值")
    private String parameterValue;

    @ApiModelProperty(value = "参数描述")
    private String description;

    @ApiModelProperty(value = "是否是内置:YES是，NO否")
    private String builtIn;
}
