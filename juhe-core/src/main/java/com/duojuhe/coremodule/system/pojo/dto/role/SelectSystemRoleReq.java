package com.duojuhe.coremodule.system.pojo.dto.role;

import com.duojuhe.common.bean.DataScopeFilterBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SelectSystemRoleReq extends DataScopeFilterBean {
    @ApiModelProperty(value = "角色名称", example = "1")
    private String roleName;
}
