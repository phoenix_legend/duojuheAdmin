package com.duojuhe.coremodule.system.mapper;

import com.duojuhe.coremodule.system.entity.SystemRole;
import com.duojuhe.coremodule.system.pojo.dto.role.*;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemRoleMapper extends TkMapper<SystemRole> {
    /**
     * 根据系统角色ID查询角色详情信息
     *
     * @return
     */
    QuerySystemRoleRes querySystemRoleResByRoleId(@Param("roleId") String roleId);


    /**
     * 分页查询 根据条件查询角色list
     *
     * @return
     */
    List<QuerySystemRolePageRes> querySystemRolePageResList(@Param("req") QuerySystemRolePageReq req);


    /**
     * 根据条件查询所有可供前端选择使用的角色
     *
     * @return
     */
    List<SelectSystemRoleRes> querySelectSystemRoleResListByStatusCode(@Param("req") SelectSystemRoleReq req, @Param("statusCode") String statusCode);
}