package com.duojuhe.coremodule.system.service;

import com.duojuhe.common.bean.struct.NodeUserStruct;
import com.duojuhe.coremodule.system.entity.SystemDataFilter;

import java.util.List;

public interface SystemDataFilterService {

    /**
     * 检查用户节点
     * @param nodeUserReqList
     */
    List<SystemDataFilter> buildDataFilterListByNodeUserList(String targetId,
                                                             String targetIdSource,
                                                             List<NodeUserStruct> nodeUserReqList);


    /**
     * 批量插入
     * @param dataFilterList
     */
    void batchInsertSystemDataFilterList(String targetId,
                                         String targetIdSource,
                                         List<SystemDataFilter> dataFilterList);
}
