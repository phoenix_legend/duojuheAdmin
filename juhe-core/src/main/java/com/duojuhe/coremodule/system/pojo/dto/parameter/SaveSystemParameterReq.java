package com.duojuhe.coremodule.system.pojo.dto.parameter;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveSystemParameterReq extends BaseBean {
    @ApiModelProperty(value = "参数名称",required=true)
    @NotBlank(message = "参数名称不能为空")
    @Length(max = 100, message = "参数名称不得超过{max}位字符")
    private String parameterName;

    @ApiModelProperty(value = "参数编码",required=true)
    @NotBlank(message = "参数编码不能为空")
    @Length(max = 50, message = "参数编码不得超过{max}位字符")
    @Pattern(regexp = RegexpConstants.EN_LENGTH_NUMBER_LINE, message = "参数编码只能填写英文字母和数字下划线!")
    private String parameterCode;

    @ApiModelProperty(value = "参数取值",required=true)
    @NotBlank(message = "参数取值不能为空")
    @Length(max = 100, message = "参数取值不得超过{max}位字符")
    private String parameterValue;

    @ApiModelProperty(value = "参数描述")
    @Length(max = 255, message = "参数描述不得超过{max}位字符")
    private String description;
}
