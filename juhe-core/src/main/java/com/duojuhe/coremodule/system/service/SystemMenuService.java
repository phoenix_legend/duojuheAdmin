package com.duojuhe.coremodule.system.service;

import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.menu.*;

import java.util.List;

/**
 * 系统菜单相关接口
 */
public interface SystemMenuService {
    /**
     * 根据菜单主键查询菜单详情数据
     */
    ServiceResult<QuerySystemMenuRes>  querySystemMenuResByMenuId(SystemMenuIdReq req);

    /**
     * 获取所有菜单资源【tree型结构】，一般用于菜单管理列表界面
     */
    ServiceResult<List<QuerySystemMenuTreeRes>> querySystemMenuTreeResList(QuerySystemMenuTreeReq req);

    /**
     * 【租户绑定菜单权限-查询tree型结构】获取状态是可用的菜单资源，一般用于租户绑定菜单使用【包含功能按钮类型】
     */
    ServiceResult<List<SelectSystemMenuTreeRes>> queryTenantBindingUseMenuTreeResList();


    /**
     * 获取所有菜单资源【tree型结构】，一般用于角色绑定权限使用
     */
    ServiceResult<List<SelectSystemMenuTreeRes>> querySelectNormalSystemMenuTreeResListByMenuType(String menuType);


    /**
     * 保存admin菜单
     */
    ServiceResult saveSystemMenu(SaveSystemMenuReq req);


    /**
     * 修改admin菜单
     */
    ServiceResult updateSystemMenu(UpdateSystemMenuReq req);


    /**
     * 根据主键删除菜单
     */
    ServiceResult deleteSystemMenuByMenuId(SystemMenuIdReq req);

}
