package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("系统通知")
@Table(name = "system_message")
public class SystemMessage extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "message_id")
    @Id
    private String messageId;

    @ApiModelProperty(value = "消息类型，取数据字典MESSAGE_TYPE")
    @Column(name = "message_type_code")
    private String messageTypeCode;

    @ApiModelProperty(value = "消息标题")
    @Column(name = "message_title")
    private String messageTitle;

    @ApiModelProperty(value = "处理状态取数据字典HANDLE_STATUS")
    @Column(name = "handle_status_code")
    private String handleStatusCode;

    @ApiModelProperty(value = "处理人id")
    @Column(name = "handle_user_id")
    private String handleUserId;

    @ApiModelProperty(value = "处理时间")
    @Column(name = "handle_time")
    private Date handleTime;

    @ApiModelProperty(value = "创建人id归属人")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "消息内容")
    @Column(name = "message_content")
    private String messageContent;
}