package com.duojuhe.coremodule.system.pojo.dto.role;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HandleSystemRoleDictNameColorRes extends BaseBean {

    @ApiModelProperty(value = "状态：FORBID禁用 NORMAL正常", example = "1")
    private String statusCode;

    @ApiModelProperty(value = "状态中文 FORBID禁用 NORMAL正常", example = "1")
    private String statusName;

    @ApiModelProperty(value = "状态显示颜色", example = "1")
    private String statusColor;


    @ApiModelProperty(value = "数据范围 ALL_DATA=所有数据权限, CUSTOM_DATA=自定义数据权限,SELF_DEPT=本部门数据权限,SELF_DEPT_SUBORDINATE=本部门及以下数据权限,SELF_DATA=自己的数据", example = "1")
    private String dataScopeCode;

    @ApiModelProperty(value = "数据范围中文", example = "1")
    private String dataScopeName;

    @ApiModelProperty(value = "数据范围显示颜色", example = "1")
    private String dataScopeColor;
}
