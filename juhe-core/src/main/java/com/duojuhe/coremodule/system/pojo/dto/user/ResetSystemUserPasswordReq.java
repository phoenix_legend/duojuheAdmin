package com.duojuhe.coremodule.system.pojo.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ResetSystemUserPasswordReq extends SystemUserIdReq {
    @ApiModelProperty(value = "登录密码", example = "1",required=true)
    @NotBlank(message = "登录密码不能为空")
    @Length(max = 16, message = "登录密码不得超过{max}位字符")
    private String password;
}
