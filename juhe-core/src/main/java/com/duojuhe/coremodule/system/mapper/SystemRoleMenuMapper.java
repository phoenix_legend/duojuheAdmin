package com.duojuhe.coremodule.system.mapper;


import com.duojuhe.coremodule.system.entity.SystemRoleMenu;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemRoleMenuMapper extends TkMapper<SystemRoleMenu> {

    /**
     * 根据角色id查询菜单 id list集合
     *
     * @param roleId
     * @return
     */
    List<String> querySystemMenuIdListByRoleId(@Param(value = "roleId") String roleId);

    /**
     * 根据租户id删除租户没有的菜单关联记录
     * @param tenantId
     * @param menuIdList
     */
    void deleteSystemRoleMenuNotMenuIdListByTenantId(@Param(value = "tenantId") String tenantId,@Param("menuIdList")  List<String> menuIdList);
}