package com.duojuhe.coremodule.system.pojo.dto.dict;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryChildrenSystemDictPageReq extends PageHead {
    @ApiModelProperty(value = "字典父级ID", example = "1",required=true)
    @NotBlank(message = "字典父级ID不能为空")
    private String parentId;

    @ApiModelProperty(value = "字典名称", example = "1")
    private String dictName;

    @ApiModelProperty(value = "字典编码", example = "1")
    private String dictCode;

    @ApiModelProperty(value = "备注", example = "备注")
    private  String remark;

    @ApiModelProperty(value = "状态：FORBID禁用 NORMAL正常", example = "FORBID")
    private String statusCode;

    @ApiModelProperty(value = "开始日期 yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期 yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}
