package com.duojuhe.coremodule.system.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.entity.SystemParameter;
import com.duojuhe.coremodule.system.pojo.dto.parameter.*;
import com.duojuhe.coremodule.system.service.SystemParameterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/systemParameter/")
@Api(tags = {"【系统参数】系统参数相关接口"})
@Slf4j
public class SystemParameterController {
    @Resource
    private SystemParameterService systemParameterService;

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【根据code集合获取系统参数】根据code集合获取系统参数")
    @PostMapping(value = "querySystemParameterListByParameterCodeList")
    public ServiceResult<Map<String, SystemParameter>> querySystemParameterListByParameterCodeList(@Valid @RequestBody @ApiParam(value = "入参类") SystemParameterCodeListReq req) {
        return systemParameterService.querySystemParameterListByParameterCodeList(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【分页查询】分页查询系统参数list")
    @PostMapping(value = "querySystemParameterPageResList")
    public ServiceResult<PageResult<List<QuerySystemParameterPageRes>>> querySystemParameterPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySystemParameterPageReq req) {
        return systemParameterService.querySystemParameterPageResList(req);
    }

    @ApiOperation(value = "【新增】新增系统参数")
    @PostMapping(value = "saveSystemParameter")
    @OperationLog(moduleName = LogEnum.ModuleName.SYSTEM_PARAMETER, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveSystemParameter(@Valid @RequestBody @ApiParam(value = "入参类") SaveSystemParameterReq req) {
        return systemParameterService.saveSystemParameter(req);
    }

    @ApiOperation(value = "【修改】修改系统参数")
    @PostMapping(value = "updateSystemParameter")
    @OperationLog(moduleName = LogEnum.ModuleName.SYSTEM_PARAMETER, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateSystemParameter(@Valid @RequestBody @ApiParam(value = "入参类") UpdateSystemParameterReq req) {
        return systemParameterService.updateSystemParameter(req);
    }

    @ApiOperation(value = "【删除】删除系统参数")
    @PostMapping(value = "deleteSystemParameterByParameterId")
    @OperationLog(moduleName = LogEnum.ModuleName.SYSTEM_PARAMETER, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteSystemParameterByParameterId(@Valid @RequestBody @ApiParam(value = "入参类") SystemParameterIdReq req) {
        return systemParameterService.deleteSystemParameterByParameterId(req);
    }

    @ApiOperation(value = "【重置缓存】重置系统缓存")
    @PostMapping(value = "resetSystemParameterCache")
    public ServiceResult resetSystemParameterCache(){
        return systemParameterService.resetSystemParameterCache();
    }
}
