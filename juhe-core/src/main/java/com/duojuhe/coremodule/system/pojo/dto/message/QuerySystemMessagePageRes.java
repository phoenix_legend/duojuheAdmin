package com.duojuhe.coremodule.system.pojo.dto.message;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemMessagePageRes extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String messageId;

    @ApiModelProperty(value = "消息标题")
    private String messageTitle;

    @ApiModelProperty(value = "消息内容")
    private String messageContent;

    @ApiModelProperty(value = "消息类型，取数据字典MESSAGE_TYPE")
    private String messageTypeCode;

    @ApiModelProperty(value = "消息类型，取数据字典MESSAGE_TYPE")
    private String messageTypeName;

    @ApiModelProperty(value = "消息类型，取数据字典MESSAGE_TYPE")
    private String messageTypeColor;

    @ApiModelProperty(value = "通知时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "处理时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date handleTime;

    @ApiModelProperty(value = "处理状态取数据字典")
    private String handleStatusCode;

    @ApiModelProperty(value = "处理状态取数据字典")
    private String handleStatusName;

    @ApiModelProperty(value = "处理状态取数据字典")
    private String handleStatusColor;
}
