package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("系统操作日志")
@Table(name = "system_log")
public class SystemLog extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "log_id")
    @Id
    private String logId;

    @ApiModelProperty(value = "登录名")
    @Column(name = "login_name")
    private String loginName;

    @ApiModelProperty(value = "创建用户姓名")
    @Column(name = "create_user_name")
    private String createUserName;

    @ApiModelProperty(value = "创建用户ID")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建部门ID")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "方法名称")
    @Column(name = "method_name")
    private String methodName;

    @ApiModelProperty(value = "请求方法")
    @Column(name = "request_method")
    private String requestMethod;

    @ApiModelProperty(value = "模块名称")
    @Column(name = "module_name")
    private String moduleName;

    @ApiModelProperty(value = "操作类型编码，取数据字典OPERATION_TYPE")
    @Column(name = "operation_type_code")
    private String operationTypeCode;

    @ApiModelProperty(value = "请求url")
    @Column(name = "request_url")
    private String requestUrl;

    @ApiModelProperty(value = "操作状态， SUCCESS=操作成功，FAILED=操作失败取数据字典OPERATION_STATUS")
    @Column(name = "operation_status_code")
    private String operationStatusCode;

    @ApiModelProperty(value = "操作IP")
    @Column(name = "operation_ip")
    private String operationIp;

    @ApiModelProperty(value = "操作IP所属区域")
    @Column(name = "ip_region")
    private String ipRegion;

    @ApiModelProperty(value = "请求参数")
    @Column(name = "request_parameter")
    private String requestParameter;

    @ApiModelProperty(value = "返回结果")
    @Column(name = "return_result")
    private String returnResult;

    @ApiModelProperty(value = "异常内容")
    @Column(name = "exception_content")
    private String exceptionContent;

    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;
}