package com.duojuhe.coremodule.system.pojo.dto.log;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HandleSystemLogDictNameColorRes extends BaseBean {

    @ApiModelProperty(value = "操作状态，取数据字典，SUCCESS=操作成功，FAILED=操作失败，EXCEPTION=操作异常")
    private String operationStatusCode;

    @ApiModelProperty(value = "操作状态中文")
    private String operationStatusName;

    @ApiModelProperty(value = "操作状态显示颜色")
    private String operationStatusColor;

    @ApiModelProperty(value = "操作类型编码，取数据字典")
    private String operationTypeCode;

    @ApiModelProperty(value = "操作类型中文")
    private String operationTypeName;

    @ApiModelProperty(value = "操作类型显示颜色")
    private String operationTypeColor;
}
