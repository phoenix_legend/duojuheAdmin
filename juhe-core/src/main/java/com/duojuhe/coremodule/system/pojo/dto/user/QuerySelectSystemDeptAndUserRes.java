package com.duojuhe.coremodule.system.pojo.dto.user;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.coremodule.system.entity.SystemUser;
import com.duojuhe.coremodule.system.pojo.dto.dept.SelectSystemDeptRes;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySelectSystemDeptAndUserRes extends BaseBean {

    @ApiModelProperty(value = "用户员工集合", example = "1")
    private List<SystemUser> employees;

    @ApiModelProperty(value = "用户部门集合", example = "1")
    private List<SelectSystemDeptRes> childDepartments;

    @ApiModelProperty(value = "标题部门节点", example = "1")
    private List<SelectSystemDeptRes> titleDepartments;

}
