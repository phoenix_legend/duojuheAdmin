package com.duojuhe.coremodule.system.pojo.dto.post;

import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateSystemPostReq extends SystemPostIdReq {
    @ApiModelProperty(value = "岗位编码",required=true)
    @NotBlank(message = "岗位编码不能为空")
    @Pattern(regexp = RegexpConstants.BIG_EN_LENGTH, message = "岗位编码只能填写大写英文!")
    @Length(min = 1, max = 50, message = "岗位编码不得超过{max}位字符")
    private String postCode;

    @ApiModelProperty(value = "岗位名称",required=true)
    @NotBlank(message = "岗位名称不能为空")
    @Length(max = 50, message = "岗位名称不得超过{max}位字符")
    private String postName;

    @ApiModelProperty(value = "备注", example = "1")
    @Pattern(regexp = RegexpConstants.REMARK, message = "备注禁止输入特殊字符!")
    @Length(max = 100, message = "备注不得超过{max}位字符")
    private  String remark;

    @ApiModelProperty(value = "状态：FORBID禁用 NORMAL正常", example = "1",required=true)
    @Pattern(regexp = RegexpConstants.STATUS, message = "岗位状态参数错误!")
    @NotBlank(message = "岗位状态不能为空")
    private String statusCode;

    @ApiModelProperty(value = "排序，只能为纯数字", example = "1",required=true)
    @NotNull(message = "排序不能为空")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;
}
