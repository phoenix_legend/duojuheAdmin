package com.duojuhe.coremodule.system.pojo.dto.parameter;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;


/**
 * 系统参数code
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SystemParameterCodeListReq extends BaseBean {
    @ApiModelProperty(value = "系统参数CODE集合",required=true)
    @NotEmpty(message = "系统参数CODE集合不能为空")
    @Valid
    @NotNull
    @Size(min = 1, message = "系统参数CODE集合不能为空")
    private List<String> parameterCodeList;
}
