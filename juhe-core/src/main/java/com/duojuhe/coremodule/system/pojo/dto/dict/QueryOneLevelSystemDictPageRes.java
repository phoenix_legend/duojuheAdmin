package com.duojuhe.coremodule.system.pojo.dto.dict;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryOneLevelSystemDictPageRes extends HandleSystemDictDictNameColorRes {
    @ApiModelProperty(value = "主键", example = "1")
    private String dictId;

    @ApiModelProperty(value = "父级id", example = "1")
    private String parentId;

    @ApiModelProperty(value = "字典名称", example = "1")
    private String dictName;

    @ApiModelProperty(value = "字典编码", example = "1")
    private String dictCode;

    @ApiModelProperty(value = "字典显示颜色", example = "1")
    private String dictColor;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注", example = "备注")
    private  String remark;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;
}
