package com.duojuhe.coremodule.system.controller;


import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.dict.*;
import com.duojuhe.coremodule.system.service.SystemDictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/systemDict/")
@Api(tags = {"【数据字典】数据字典管理相关接口"})
@Slf4j
public class SystemDictController {
    @Resource
    private SystemDictService systemDictService;

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【根据ID查询详情】根据数据字典ID查询数据字典详情")
    @PostMapping(value = "querySystemDictResByDictId")
    public ServiceResult<QuerySystemDictRes> querySystemDictResByDictId(@Valid @RequestBody @ApiParam(value = "入参类") SystemDictIdReq req) {
        return systemDictService.querySystemDictResByDictId(req);
    }

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【分页查询】分页查询数据字典一级list")
    @PostMapping(value = "queryOneLevelSystemDictPageResList")
    public ServiceResult<PageResult<List<QueryOneLevelSystemDictPageRes>>> queryOneLevelSystemDictPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryOneLevelSystemDictPageReq req) {
        return systemDictService.queryOneLevelSystemDictPageResList(req);
    }

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【分页查询】分页查询数据字典子级list")
    @PostMapping(value = "queryChildrenSystemDictPageResList")
    public ServiceResult<PageResult<List<QueryChildrenSystemDictPageRes>>> queryChildrenSystemDictPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryChildrenSystemDictPageReq req) {
        return systemDictService.queryChildrenSystemDictPageResList(req);
    }

    @ApiOperation(value = "【保存】数据字典信息")
    @PostMapping(value = "saveSystemDict")
    @OperationLog(moduleName = LogEnum.ModuleName.DICT_ADMIN, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveSystemDict(@Valid @RequestBody @ApiParam(value = "入参类") SaveSystemDictReq req) {
        return systemDictService.saveSystemDict(req);
    }

    @ApiOperation(value = "【修改】数据字典信息")
    @PostMapping(value = "updateSystemDict")
    @OperationLog(moduleName = LogEnum.ModuleName.DICT_ADMIN, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateSystemDict(@Valid @RequestBody @ApiParam(value = "入参类") UpdateSystemDictReq req) {
        return systemDictService.updateSystemDict(req);
    }

    @ApiOperation(value = "【删除】根据id删除数据字典,内置数据字典不可删除")
    @PostMapping(value = "deleteSystemDictByDictId")
    @OperationLog(moduleName = LogEnum.ModuleName.DICT_ADMIN, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteSystemDictByDictId(@Valid @RequestBody @ApiParam(value = "入参类") SystemDictIdReq req){
        return systemDictService.deleteSystemDictByDictId(req);
    }

    @ApiOperation(value = "【重置缓存】重置数据字典缓存")
    @PostMapping(value = "resetSystemDictCache")
    public ServiceResult resetSystemDictCache(){
        return systemDictService.resetSystemDictCache();
    }
}
