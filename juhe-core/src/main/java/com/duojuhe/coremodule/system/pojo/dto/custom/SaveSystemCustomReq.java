package com.duojuhe.coremodule.system.pojo.dto.custom;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


/**
 * 保存自定义布局参数
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveSystemCustomReq extends SystemCustomKeyReq {
    @ApiModelProperty(value = "自定义json串",required=true)
    @NotBlank(message = "自定义布局内容不可为空")
    private String content;
}
