package com.duojuhe.coremodule.system.mapper;

import com.duojuhe.coremodule.system.entity.SystemUser;
import com.duojuhe.coremodule.system.pojo.dto.user.*;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemUserMapper extends TkMapper<SystemUser> {

    /**
     * 根据用户登录名获取一个用户信息
     */
    SystemUser querySystemUserByLoginName(@Param(value = "loginName") String loginName);

    /**
     * 根据用户手机号码获取一个用户信息
     */
    SystemUser querySystemUserByMobileNumber(@Param(value = "mobileNumber") String mobileNumber);

    /**
     * 分页查询 根据条件查询非超管用户list
     *
     * @return
     */
    List<QuerySystemUserPageRes> querySystemUserPageResList(@Param("req") QuerySystemUserPageReq req,@Param("noUserId") String noUserId);

    /**
     * 查询 根据条件选择用户
     *
     * @return
     */
    List<QuerySelectSystemUserRes> querySelectSystemUserResList(@Param("req") QuerySelectSystemUserReq req,@Param("noUserId") String noUserId);




    /**
     * 根据用户ID查询用户详情数据
     *
     * @return
     */
    QuerySystemUserRes querySystemUserResByUserId(@Param("userId") String userId);


    /**
     * 根据系统岗位ID查询用户集合信息
     *
     * @return
     */
    List<SystemUser> querySystemUserListByPostId(@Param("postId") String postId);

    /**
     * 根据系统角色ID查询用户集合信息
     *
     * @return
     */
    List<SystemUser> querySystemUserListByRoleId(@Param("roleId") String roleId);


    /**
     * 根据用户部门ID查询用户集合信息
     *
     * @return
     */
    List<SystemUser> querySystemUserListByDeptId(@Param("deptId") String deptId);


    /**
     * 根据用户租户ID查询用户集合信息
     *
     * @return
     */
    List<SystemUser> querySystemUserListByTenantId(@Param("tenantId") String tenantId);

    /**
     * 根据用户部门 , 角色id，岗位id 查询用户集合信息,不包含当前登录人，和超管及租户管理员
     *
     * @return
     */
    List<SystemUser> querySelectSystemUserList(@Param("req") QuerySelectSystemUserReq req,@Param("noUserId") String noUserId);


    /**
     * 查询包含当前登录人，和超管及租户管理员 用户集合信息
     *
     * @return
     */
    List<SystemUser> querySelectMyAllSystemUserList(@Param("req") QuerySelectSystemUserReq req);



    /**
     * 根据用户id集合查询用户ID list
     *
     * @return
     */
    List<String> querySystemUserIdListByUserIdList(@Param("userIdList") List<String> userIdList);


    /**
     * 根据用户id集合查询用户list
     *
     * @return
     */
    List<SystemUser> querySystemUserListByUserIdList(@Param("userIdList") List<String> userIdList);


    /**
     * 根据系统岗位ID 集合 查询用户ID集合信息
     *
     * @return
     */
    List<String> querySystemUserIdListByPostIdList(@Param("postIdList") List<String> postIdList);


    /**
     * 根据岗位 id集合 查询用户list
     *
     * @return
     */
    List<SystemUser> querySystemUserListByPostIdList(@Param("postIdList") List<String> postIdList);


    /**
     * 根据系统角色ID 集合 查询用户ID集合信息
     *
     * @return
     */
    List<String> querySystemUserIdListByRoleIdList(@Param("roleIdList") List<String> roleIdList);

    /**
     * 根据系统角色ID 集合 查询用户集合信息
     *
     * @return
     */
    List<SystemUser> querySystemUserListByRoleIdList(@Param("roleIdList") List<String> roleIdList);




    /**
     * 根据用户部门ID 集合 查询用户ID集合信息
     *
     * @return
     */
    List<String> querySystemUserIdListByDeptIdList(@Param("deptIdList") List<String> deptIdList);


    /**
     * 根据部门 id集合 查询用户list
     *
     * @return
     */
    List<SystemUser> querySystemUserListByDeptIdList(@Param("deptIdList") List<String> deptIdList);
}