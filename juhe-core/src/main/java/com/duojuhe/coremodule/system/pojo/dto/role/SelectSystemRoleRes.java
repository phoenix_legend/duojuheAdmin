package com.duojuhe.coremodule.system.pojo.dto.role;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SelectSystemRoleRes extends BaseBean {

    @ApiModelProperty(value = "主键", example = "1")
    private String roleId;

    @ApiModelProperty(value = "角色名称", example = "1")
    private String roleName;
}
