package com.duojuhe.coremodule.system.pojo.dto.tenant;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveSystemTenantReq extends BaseBean {

    @ApiModelProperty(value = "租户名称唯一", required = true)
    @Length(max = 200, message = "租户名称不得超过{max}位字符")
    @NotBlank(message = "租户名称不能为空")
    private String tenantName;

    @ApiModelProperty(value = "租户简称", required = true)
    @Length(max = 30, message = "租户简称不得超过{max}位字符")
    @NotBlank(message = "租户简称不能为空")
    private String tenantAbbreviation;

    @ApiModelProperty(value = "管理帐号，只能为小写字母和数字，长度2-15", example = "1",required=true)
    @NotBlank(message = "管理帐号不能为空")
    @Pattern(regexp = RegexpConstants.LOGIN_NAME, message = "管理帐号只能为小写字母和数字，长度6-15")
    @Length(max = 15, message = "管理帐号不得超过{max}位字符")
    private String loginName;

    @ApiModelProperty(value = "管理密码", example = "1",required=true)
    @NotBlank(message = "管理密码不能为空")
    @Length(max = 16, message = "管理密码不得超过{max}位字符")
    private String password;

    @ApiModelProperty(value = "手机号码", example = "1",required = true)
    @Pattern(regexp = RegexpConstants.MOBILE, message = "手机号码不合法")
    @NotBlank(message = "手机号码不能为空")
    private String mobileNumber;

    @ApiModelProperty(value = "租户所对应的菜单id集合，留空表示清除该租户所有权限", example = "1")
    private List<String> menuIdList;
}
