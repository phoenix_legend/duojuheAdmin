package com.duojuhe.coremodule.system.pojo.dto.role;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemRolePageReq extends PageHead {
    @ApiModelProperty(value = "角色名称", example = "1")
    private String roleName;

    @ApiModelProperty(value = "角色备注描述", example = "1")
    private String remark;

    @ApiModelProperty(value = "开始日期 yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期 yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常", example = "1")
    private String statusCode;

    @ApiModelProperty(value = "创建者姓名", example = "1")
    private String createUserName;

    @ApiModelProperty(value = "创建部门id")
    private String createDeptId;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}
