package com.duojuhe.coremodule.system.pojo.dto.role;


import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * 修改角色
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateSystemRoleReq extends SystemRoleIdReq {
    @ApiModelProperty(value = "角色名称，只能填写中文英文数字和下划线，长度1-50", example = "1",required=true)
    @NotBlank(message = "角色名称不能为空")
    @Length(max = 50, message = "角色名称不得超过{max}位字符")
    private String roleName;

    @ApiModelProperty(value = "备注", example = "1")
    @Pattern(regexp = RegexpConstants.REMARK, message = "备注禁止输入特殊字符!")
    @Length(max = 100, message = "备注不得超过{max}位字符")
    private  String remark;

    @ApiModelProperty(value = "角色状态FORBID禁用 NORMAL正常", example = "NORMAL",required=true)
    @Pattern(regexp = RegexpConstants.STATUS, message = "角色状态参数输入错误!")
    @NotBlank(message = "角色状态不能为空")
    private  String  statusCode;

    @ApiModelProperty(value = "排序，只能为纯数字", example = "1",required=true)
    @NotNull(message = "排序不能为空")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;
}
