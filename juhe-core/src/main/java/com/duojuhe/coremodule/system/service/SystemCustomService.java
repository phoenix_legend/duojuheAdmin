package com.duojuhe.coremodule.system.service;

import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.custom.SaveSystemCustomReq;
import com.duojuhe.coremodule.system.pojo.dto.custom.SystemCustomKeyReq;

/**
 * 用户自定义喜好风格
 */
public interface SystemCustomService {
    /**
     * 根据key查询自定义布局详情
     */
    ServiceResult<String> querySystemCustomContentJsonByCustomKey(SystemCustomKeyReq req);

    /**
     * 根据key删除自定义布局
     */
    ServiceResult deleteSystemCustomByCustomKey(SystemCustomKeyReq req);

    /**
     * 保存自定义布局参数
     */
    ServiceResult<String> saveSystemCustom(SaveSystemCustomReq req);
}
