package com.duojuhe.coremodule.system.pojo.dto.dict;


import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.coremodule.system.entity.SystemDict;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


/**
 * 选择数据字典响应类
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@Getter
@Setter
public class SelectChildrenDictRes extends BaseBean {
    @ApiModelProperty(value = "字典名称", example = "1")
    private String dictName;

    @ApiModelProperty(value = "字典编码", example = "1")
    private String dictCode;

    @ApiModelProperty(value = "字典颜色", example = "1")
    private String dictColor;

    @ApiModelProperty(value = "排序", example = "1")
    private  Integer sort;

    public SelectChildrenDictRes(SystemDict systemDict){
        this.dictName=systemDict.getDictName();
        this.dictCode=systemDict.getDictCode();
        this.dictColor=systemDict.getDictColor();
        this.sort=systemDict.getSort();
    }
}
