package com.duojuhe.coremodule.system.pojo.dto.dept;

import com.duojuhe.common.bean.DataScopeFilterBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * 部门查询tree结构
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemDeptTreeReq extends DataScopeFilterBean {

    @ApiModelProperty(value = "部门领导", example = "1")
    private String deptLeader;

    @ApiModelProperty(value = "部门名称", example = "1")
    private String deptName;

    @ApiModelProperty(value = "部门编码", example = "1")
    private String deptCode;

    @ApiModelProperty(value = "备注", example = "1")
    private String remark;
}
