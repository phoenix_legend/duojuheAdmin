package com.duojuhe.coremodule.system.pojo.dto.menu;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemMenuTreeRes extends HandleSystemMenuDictNameColorRes {
    @ApiModelProperty(value = "主键", example = "1")
    private String menuId;

    @ApiModelProperty(value = "菜单编码", example = "1")
    private String menuCode;

    @ApiModelProperty(value = "菜单名称", example = "1")
    private String menuName;

    @ApiModelProperty(value = "菜单请求地址", example = "1")
    private String apiUrl;

    @ApiModelProperty(value = "前端组件地址", example = "1")
    private String component;

    @ApiModelProperty(value = "菜单小图标", example = "1")
    private String menuIcon;

    @ApiModelProperty(value = "备注", example = "1")
    private String remark;

    @ApiModelProperty(value = "菜单描述", example = "1")
    private String description;

    @ApiModelProperty(value = "排序", example = "1")
    private Integer sort;

    @ApiModelProperty(value = "是否租户菜单:YES是，NO否")
    private String tenantMenu;

    @ApiModelProperty(value = "是否是外链:YES是，NO否")
    private String menuFrameCode;
}
