package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("岗位")
@Table(name = "system_post")
public class SystemPost extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "post_id")
    @Id
    private String postId;

    @ApiModelProperty(value = "岗位编码", required = true)
    @Column(name = "post_code")
    private String postCode;

    @ApiModelProperty(value = "岗位名称", required = true)
    @Column(name = "post_name")
    private String postName;

    @ApiModelProperty(value = "状态：FORBID禁用 NORMAL正常STATUS")
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "修改人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "修改时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;
}