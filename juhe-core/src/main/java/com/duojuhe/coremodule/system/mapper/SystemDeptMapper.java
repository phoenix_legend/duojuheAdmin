package com.duojuhe.coremodule.system.mapper;

import com.duojuhe.coremodule.system.entity.SystemDept;
import com.duojuhe.coremodule.system.pojo.dto.dept.*;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemDeptMapper extends TkMapper<SystemDept> {
    /**
     * 根据部门路径查询相关的包含下级的部门信息集合
     * @return
     */
    List<SystemDept> queryLikeSystemDeptListByDeptPath(@Param(value = "deptPath") String deptPath);

    /**
     * 根据部门路径查询相关的包含下级的部门ID集合
     * @return
     */
    List<String> queryLikeSystemDeptIdListByDeptPath(@Param(value = "deptPath") String deptPath);


    /**
     * 根据部门ID获取部门资源信息
     * @return
     */
    QuerySystemDeptRes querySystemDeptResByDeptId(@Param(value = "deptId") String deptId);


    /**
     * 【部门管理-tree型结构查询】查询部门列表，一般用于部门管理界面
     * @param req
     * @return
     */
    List<QuerySystemDeptTreeRes> querySystemDeptTreeResList(@Param("req") QuerySystemDeptTreeReq req);


    /**
     * 【下拉选择使用-tree型结构查询】一般用于部门下拉选择界面使用
     * @param req
     * @return
     */
    List<SelectSystemDeptTreeRes> queryNormalSelectSystemDeptTreeResList(@Param("req") SelectSystemDeptTreeReq req);


    /**
     * 根据条件查询部门信息
     * @param req
     * @return
     */
    List<SelectSystemDeptRes> querySelectSystemDeptResList(@Param("req") SelectSystemDeptReq req);


    /**
     * 判断部门Id是否真实有效
     *根绝ids中的id查询有效的Ids
     * @param
     * @return
     */
    List<SystemDept> queryDeptListBySystemDeptIdList(@Param(value = "deptIdList") List<String> deptIdList);


}