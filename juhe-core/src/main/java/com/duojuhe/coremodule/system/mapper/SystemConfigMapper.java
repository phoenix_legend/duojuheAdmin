package com.duojuhe.coremodule.system.mapper;

import com.duojuhe.coremodule.system.entity.SystemConfig;
import com.tkmapper.TkMapper;

public interface SystemConfigMapper extends TkMapper<SystemConfig> {
}
