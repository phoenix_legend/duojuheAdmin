package com.duojuhe.coremodule.system.service.impl;

import com.duojuhe.common.bean.UserTokenInfoVo;
import com.duojuhe.common.enums.IdResourceEnum;
import com.duojuhe.common.enums.NodeUserEnum;
import com.duojuhe.common.exception.base.DuoJuHeException;
import com.duojuhe.common.result.ErrorCodes;
import com.duojuhe.common.utils.idgenerator.UUIDUtils;
import com.duojuhe.common.bean.struct.NodeUserStruct;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.system.entity.SystemDataFilter;
import com.duojuhe.coremodule.system.mapper.*;
import com.duojuhe.coremodule.system.service.SystemDataFilterService;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class SystemDataFilterServiceImpl extends BaseService implements SystemDataFilterService {
    @Resource
    private SystemDeptMapper systemDeptMapper;
    @Resource
    private SystemPostMapper systemPostMapper;
    @Resource
    private SystemRoleMapper systemRoleMapper;
    @Resource
    private SystemUserMapper systemUserMapper;
    @Resource
    private SystemDataFilterMapper systemDataFilterMapper;


    /**
     * 检查用户节点
     * @param nodeUserReqList
     */
    @Override
    public List<SystemDataFilter> buildDataFilterListByNodeUserList(String targetId,
                                                                    String targetIdSource,
                                                                    List<NodeUserStruct> nodeUserReqList) {
        List<SystemDataFilter> systemDataFilterList = new ArrayList<>();
        //当前登录人
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //用户id
        String userId = userTokenInfoVo.getUserId();
        //创建部门id
        String createDeptId = userTokenInfoVo.getCreateDeptId();
        //创建租户
        String tenantId = userTokenInfoVo.getTenantId();
        //当前时间
        Date nowDate = new Date();

        if (nodeUserReqList!=null&&!nodeUserReqList.isEmpty()){
            for (NodeUserStruct nodeUser:nodeUserReqList){
                String relationIdSource = getIdSourceByNodeUser(nodeUser);
                //构建数据权限对象
                SystemDataFilter systemDataFilter = new SystemDataFilter();
                systemDataFilter.setId(UUIDUtils.getUUID32());
                systemDataFilter.setTargetId(targetId);
                systemDataFilter.setTargetIdSource(targetIdSource);
                systemDataFilter.setRelationId(nodeUser.getTargetId());
                systemDataFilter.setRelationIdSource(relationIdSource);
                systemDataFilter.setCreateUserId(userId);
                systemDataFilter.setCreateTime(nowDate);
                systemDataFilter.setCreateDeptId(createDeptId);
                systemDataFilter.setTenantId(tenantId);
                systemDataFilterList.add(systemDataFilter);
            }
       }
        return systemDataFilterList;
    }

    /**
     * 批量插入
     * @param dataFilterList
     */
    @Override
    public void batchInsertSystemDataFilterList(String targetId,
                                                String targetIdSource,
                                                List<SystemDataFilter> dataFilterList) {
        //先删除
        SystemDataFilter deleteSystemDataFilter = new SystemDataFilter();
        deleteSystemDataFilter.setTargetId(targetId);
        deleteSystemDataFilter.setTargetIdSource(targetIdSource);
        systemDataFilterMapper.delete(deleteSystemDataFilter);
        //批量插入
        if (dataFilterList!=null&&!dataFilterList.isEmpty()){
            systemDataFilterMapper.batchInsertListUseAllCols(dataFilterList);
        }
    }

    /**
     * 私有方法检查用户节点
     * @param nodeUser
     */
    private String getIdSourceByNodeUser(NodeUserStruct nodeUser){
        //来源id所属表
        String relationIdSource = "";
        //节点名称
        String name = nodeUser.getName();
        //节点类型
        Integer type = nodeUser.getType();
        //目标id
        String targetId = nodeUser.getTargetId();
        if (NodeUserEnum.NODE_USER_TYPE.USER.getKey().equals(type)){
            if (systemUserMapper.selectByPrimaryKey(targetId)==null){
                throw new DuoJuHeException(name+"【用户不存在,请重新选择】");
            }
            relationIdSource = IdResourceEnum.IdResource.system_user.getKey();
        }else if (NodeUserEnum.NODE_USER_TYPE.DEPT.getKey().equals(type)){
            if (systemDeptMapper.selectByPrimaryKey(targetId)==null){
                throw new DuoJuHeException(name+"【部门不存在,请重新选择】");
            }
            relationIdSource = IdResourceEnum.IdResource.system_dept.getKey();
        }else if (NodeUserEnum.NODE_USER_TYPE.POST.getKey().equals(type)){
            if (systemPostMapper.selectByPrimaryKey(targetId)==null){
                throw new DuoJuHeException(name+"【岗位不存在,请重新选择】");
            }
            relationIdSource = IdResourceEnum.IdResource.system_post.getKey();
        }else if (NodeUserEnum.NODE_USER_TYPE.ROLE.getKey().equals(type)){
            if (systemRoleMapper.selectByPrimaryKey(targetId)==null){
                throw new DuoJuHeException(name+"【角色不存在,请重新选择】");
            }
            relationIdSource = IdResourceEnum.IdResource.system_role.getKey();
        }else {
            throw new DuoJuHeException(ErrorCodes.PARAM_ERROR);
        }

        return relationIdSource;
    }
}
