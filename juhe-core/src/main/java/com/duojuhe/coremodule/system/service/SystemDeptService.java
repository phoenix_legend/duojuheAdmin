package com.duojuhe.coremodule.system.service;

import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.dept.*;
import com.duojuhe.coremodule.system.pojo.dto.user.QuerySelectSystemDeptAndUserRes;
import com.duojuhe.coremodule.system.pojo.dto.user.QuerySelectSystemUserReq;

import java.util.List;


/**
 * 系统部门相关接口
 */
public interface SystemDeptService {



    /**
     *【下拉选择使用-查询部门】查询可供前端选择使用的部门，一般用于添加下拉选择使用
     * @param req
     * @return
     */
    ServiceResult<List<SelectSystemDeptRes>> queryNormalSelectSystemDeptResList(SelectSystemDeptReq req);

    /**
     *根据部门ID获取部门资源信息
     * @param req
     * @return
     */
    ServiceResult<QuerySystemDeptRes> querySystemDeptResByDeptId(SystemDeptIdReq req);

    /**
     *【部门管理-tree型结构查询】查询部门列表，一般用于部门管理界面
     * @param req
     * @return
     */
    ServiceResult<List<QuerySystemDeptTreeRes>> querySystemDeptTreeResList(QuerySystemDeptTreeReq req);




    /**
     * 【分页查询】分页选择系统用户list
     * @param req
     * @return
     */
    ServiceResult<QuerySelectSystemDeptAndUserRes> querySelectSystemDeptAndUserResList(SelectSystemDeptAndUserReq req);



    /**
     * 【下拉选择使用-tree型结构查询】一般用于部门下拉选择界面使用
     * @param req
     * @return
     */
    ServiceResult<List<SelectSystemDeptTreeRes>> queryNormalSelectSystemDeptTreeResList(SelectSystemDeptTreeReq req);

    /**
     * 保存admin部门
     *
     * @param req
     * @return
     */
    ServiceResult saveSystemDept(SaveSystemDeptReq req);


    /**
     * 修改admin部门
     *
     * @param req
     * @return
     */
    ServiceResult updateSystemDept(UpdateSystemDeptReq req);


    /**
     * 删除admin部门
     *
     * @param req
     * @return
     * @throws Exception
     */
    ServiceResult deleteSystemDeptByDeptId(SystemDeptIdReq req);
}
