package com.duojuhe.coremodule.system.pojo.dto.menu;


import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


/**
 * 保存菜单
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveSystemMenuReq extends BaseBean {

    @ApiModelProperty(value = "父级ID，留空表示是顶级菜单栏，长度1-32 填写-1标识顶级菜单栏", example = "1",required=true)
    @Length(max = 32, message = "父级ID不得超过{max}位字符")
    @NotBlank(message = "父级ID不能为空")
    private String parentId;

    @ApiModelProperty(value = "菜单名称，只能填写中文英文数字和下划线，长度1-30", example = "1",required=true)
    @NotBlank(message = "菜单名称不能为空")
    @Pattern(regexp = RegexpConstants.CH_EN_NUMBER_LINE, message = "菜单名称只能填写中文英文数字和下划线!")
    @Length(max = 30, message = "菜单名称不得超过{max}位字符")
    private String menuName;

    @ApiModelProperty(value = "菜单编码，只能填写英文，长度1-50个字符", example = "1",required=true)
    @NotBlank(message = "菜单编码不能为空")
    @Pattern(regexp = RegexpConstants.EN_LENGTH, message = "菜单编码只能填写英文!")
    @Length(min = 1, max = 50, message = "菜单编码不得超过{max}位字符")
    private String menuCode;

    @ApiModelProperty(value = "菜单api接口URL，长度1-200个字符", example = "1")
    @Length(max = 200, message = "菜单API不得超过{max}位字符")
    private String apiUrl;

    @ApiModelProperty(value = "菜单类型 NAVIGATION 导航 BUTTON 功能按钮", example = "NAVIGATION",required=true)
    @Pattern(regexp = "^[NAVIGATION|BUTTON]*$", message = "菜单类型参数错误!")
    @NotBlank(message = "菜单类型不能为空")
    private String menuTypeCode;

    @ApiModelProperty(value = "菜单状态，状态FORBID禁用 NORMAL正常", example = "NORMAL",required = true)
    @Pattern(regexp = RegexpConstants.STATUS, message = "菜单状态参数错误!")
    @NotBlank(message = "菜单状态不能为空")
    private String statusCode;

    @ApiModelProperty(value = "排序，只能为纯数字", example = "1",required=true)
    @NotNull(message = "排序不能为空")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;

    @ApiModelProperty(value = "菜单图标", example = "1")
    @Length(max = 50, message = "菜单图标不得超过{max}位字符")
    private String menuIcon;

    @ApiModelProperty(value = "路由地址", example = "1")
    @Length(max = 250, message = "路由地址长度不得超过{max}位字符")
    private String component;

    @ApiModelProperty(value = "备注", example = "1")
    @Length(max = 200, message = "备注长度不得超过{max}位字符")
    private String remark;

    @ApiModelProperty(value = "菜单描述", example = "1")
    @Length(max = 1000, message = "菜单描述长度不得超过{max}位字符")
    private String description;

    @ApiModelProperty(value = "租户可用:YES是，NO否", example = "YES",required = true)
    @Pattern(regexp = RegexpConstants.YES_NO, message = "租户可用参数错误!")
    @NotBlank(message = "是否租户可用不能为空")
    private String tenantMenu;

    @ApiModelProperty(value = "是否外链:YES是，NO否", example = "YES",required = true)
    @Pattern(regexp = RegexpConstants.YES_NO, message = "是否外链参数错误!")
    @NotBlank(message = "是否外链不能为空")
    private String menuFrameCode;

}
