package com.duojuhe.coremodule.system.controller;


import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.post.*;
import com.duojuhe.coremodule.system.service.SystemPostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/systemPost/")
@Api(tags = {"【岗位管理】岗位管理相关接口"})
@Slf4j
public class SystemPostController {
    @Resource
    private SystemPostService systemPostService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【分页查询】分页查询岗位list")
    @PostMapping(value = "querySystemPostPageResList")
    public ServiceResult<PageResult<List<QuerySystemPostPageRes>>> querySystemPostPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySystemPostPageReq req) {
        return systemPostService.querySystemPostPageResList(req);
    }

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【下拉选择使用-查询所有岗位】查询可供前端选择使用的岗位，一般用于用户添加下拉选择使用")
    @PostMapping(value = "queryNormalSelectSystemPostResList")
    public ServiceResult<List<SelectSystemPostRes>> queryNormalSelectSystemPostResList(@Valid @RequestBody @ApiParam(value = "入参类") SelectSystemPostReq req) {
        return systemPostService.queryNormalSelectSystemPostResList(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【根据ID查询详情】根据岗位ID查询岗位详情")
    @PostMapping(value = "querySystemPostResByPostId")
    public ServiceResult<QuerySystemPostRes> querySystemPostResByPostId(@Valid @RequestBody @ApiParam(value = "入参类") SystemPostIdReq req) {
        return systemPostService.querySystemPostResByPostId(req);
    }

    @ApiOperation(value = "【保存】岗位信息")
    @PostMapping(value = "saveSystemPost")
    @OperationLog(moduleName = LogEnum.ModuleName.POST_ADMIN, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveSystemPost(@Valid @RequestBody @ApiParam(value = "入参类") SaveSystemPostReq req) {
        return systemPostService.saveSystemPost(req);
    }


    @ApiOperation(value = "【修改】岗位信息")
    @PostMapping(value = "updateSystemPost")
    @OperationLog(moduleName = LogEnum.ModuleName.POST_ADMIN, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateSystemPost(@Valid @RequestBody @ApiParam(value = "入参类") UpdateSystemPostReq req) {
        return systemPostService.updateSystemPost(req);
    }


    @ApiOperation(value = "【删除】根据岗位ID删除岗位，注意一次仅能删除一个岗位，存在绑定关系的则不能删除")
    @PostMapping(value = "deleteSystemPostByPostId")
    @OperationLog(moduleName = LogEnum.ModuleName.POST_ADMIN, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteSystemPostByPostId(@Valid @RequestBody @ApiParam(value = "入参类") SystemPostIdReq req) {
        return systemPostService.deleteSystemPostByPostId(req);
    }
}
