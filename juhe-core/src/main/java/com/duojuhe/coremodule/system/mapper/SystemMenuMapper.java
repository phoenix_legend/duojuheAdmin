package com.duojuhe.coremodule.system.mapper;


import com.duojuhe.common.bean.SystemMenuInfoVo;
import com.duojuhe.coremodule.system.entity.SystemMenu;
import com.duojuhe.coremodule.system.pojo.dto.menu.QuerySystemMenuTreeReq;
import com.duojuhe.coremodule.system.pojo.dto.menu.SelectSystemMenuTreeRes;
import com.duojuhe.coremodule.system.pojo.dto.menu.QuerySystemMenuRes;
import com.duojuhe.coremodule.system.pojo.dto.menu.QuerySystemMenuTreeRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemMenuMapper extends TkMapper<SystemMenu> {
    /**
     * 根据角色id list查询菜单list
     *
     * @param roleId
     * @return
     */
    List<SystemMenuInfoVo> querySystemMenuInfoVoListByRoleId(@Param(value = "roleId") String roleId);


    /**
     * 查询系统租户可用的所有菜单
     *
     * @return
     */
    List<SystemMenuInfoVo> querySystemTenantAllSystemMenuInfoVoListByTenantId(@Param("tenantId") String tenantId);


    /**
     * 根据系统菜单ID查询菜单详情信息
     *
     * @return
     */
    QuerySystemMenuRes querySystemMenuResByMenuId(@Param("menuId") String menuId);


    /**
     * 获取所有菜单资源【tree型结构】，一般用于菜单管理列表界面
     *
     * @return
     */
    List<QuerySystemMenuTreeRes> querySystemMenuTreeResList(@Param("req") QuerySystemMenuTreeReq req);

    /**
     * 获取所有状态正常菜单资源或者根据角色id获取【tree型结构】
     *
     * @return
     */
    List<SelectSystemMenuTreeRes> queryNormalSystemMenuTreeResListByMenuTypeOrRoleId(@Param("menuType") String menuType,@Param(value = "roleId") String roleId);


    /**
     * 【租户】获取租户所有状态正常菜单资源【tree型结构】
     * tenantId 租户id
     * @return
     */
    List<SelectSystemMenuTreeRes> querySystemTenantNormalSystemMenuTreeResListByTenantId(@Param("tenantId") String tenantId);



    /**
     * 判断菜单Id是否真实有效
     *根绝ids中的id查询有效的Ids
     * @param
     * @return
     */
    List<String> queryMenuIdListBySystemMenuIdList(@Param(value = "menuIdList") List<String> menuIdList);
}