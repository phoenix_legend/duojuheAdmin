package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("系统用户个人喜好设置")
@Table(name = "system_custom")
public class SystemCustom extends BaseBean {
    @ApiModelProperty(value = "主键id自定义key和用户id的MD5值", required = true)
    @Column(name = "custom_id")
    @Id
    private String customId;

    @ApiModelProperty(value = "自定义key", required = true)
    @Column(name = "custom_key")
    private String customKey;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "用户ID")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "用户ID来源")
    @Column(name = "create_user_id_source")
    private String createUserIdSource;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "创建部门ID")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "自定义json串")
    @Column(name = "content")
    private String content;
}