package com.duojuhe.coremodule.system.controller;


import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.custom.SaveSystemCustomReq;
import com.duojuhe.coremodule.system.pojo.dto.custom.SystemCustomKeyReq;
import com.duojuhe.coremodule.system.service.SystemCustomService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/systemCustom/")
@Api(tags = {"【自定义喜好布局】自定义布局喜好管理相关接口"})
@Slf4j
public class SystemCustomController {
    @Resource
    private SystemCustomService systemCustomService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【根据标识key查询】根据key查询自定义喜好布局内容详情")
    @PostMapping(value = "querySystemCustomContentJsonByCustomKey")
    public ServiceResult<String> querySystemCustomContentJsonByCustomKey(@Valid @RequestBody @ApiParam(value = "入参类") SystemCustomKeyReq req) {
        return systemCustomService.querySystemCustomContentJsonByCustomKey(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【保存】自定义风格喜好布局参数，系统中根据key(前端开发自主定义) 和用户id区分")
    @PostMapping(value = "saveSystemCustom")
    public ServiceResult<String> saveSystemCustom(@Valid @RequestBody @ApiParam(value = "入参类") SaveSystemCustomReq req) {
        return systemCustomService.saveSystemCustom(req);
    }

    @ApiOperation(value = "【删除】根据key删除自定义布局内容")
    @PostMapping(value = "deleteSystemCustomByCustomKey")
    public ServiceResult deleteSystemCustomByCustomKey(@Valid @RequestBody @ApiParam(value = "入参类") SystemCustomKeyReq req) {
        return systemCustomService.deleteSystemCustomByCustomKey(req);
    }
}
