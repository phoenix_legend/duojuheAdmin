package com.duojuhe.coremodule.system.pojo.dto.dept;

import com.duojuhe.common.bean.DataScopeFilterBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SelectSystemDeptAndUserReq extends DataScopeFilterBean {
    @ApiModelProperty(value = "关键词", example = "1")
    private String keyword;

    @ApiModelProperty(value = "上级部门id，-1表示顶级部门", required = true)
    private String parentId;
}
