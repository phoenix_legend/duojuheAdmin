package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("数据字典表")
@Table(name = "system_dict")
public class SystemDict extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "dict_id")
    @Id
    private String dictId;

    @ApiModelProperty(value = "父级Id-1表示顶级", required = true)
    @Column(name = "parent_id")
    private String parentId;

    @ApiModelProperty(value = "字典编码", required = true)
    @Column(name = "dict_code")
    private String dictCode;

    @ApiModelProperty(value = "字典名称")
    @Column(name = "dict_name")
    private String dictName;

    @ApiModelProperty(value = "字典显示颜色")
    @Column(name = "dict_color")
    private String dictColor;

    @ApiModelProperty(value = "状态：FORBID禁用 NORMAL正常", required = true)
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "字典类型:BUSINESS_DICT业务字典 SYSTEM_DICT系统字典")
    @Column(name = "dict_type_code")
    private String dictTypeCode;
}