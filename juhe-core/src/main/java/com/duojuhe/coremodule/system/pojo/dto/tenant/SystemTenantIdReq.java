package com.duojuhe.coremodule.system.pojo.dto.tenant;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SystemTenantIdReq extends BaseBean {
    @ApiModelProperty(value = "租户ID", example = "1",required=true)
    @NotBlank(message = "租户ID不能为空")
    private String tenantId;
}
