package com.duojuhe.coremodule.system.controller;


import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.user.*;
import com.duojuhe.coremodule.system.service.SystemUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/systemUser/")
@Api(tags = {"【系统用户管理】系统用户管理相关接口"})
@Slf4j
public class SystemUserController {
    @Resource
    private SystemUserService systemUserService;

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【分页查询】分页查询系统用户list")
    @PostMapping(value = "querySystemUserPageResList")
    public ServiceResult<PageResult<List<QuerySystemUserPageRes>>> querySystemUserPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySystemUserPageReq req) {
        return systemUserService.querySystemUserPageResList(req);
    }



    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【选择员工】分页选择系统用户list")
    @PostMapping(value = "querySelectSystemUserResList")
    public ServiceResult<PageResult<List<QuerySelectSystemUserRes>>> querySelectSystemUserResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySelectSystemUserReq req) {
        return systemUserService.querySelectSystemUserResList(req);
    }



    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【根据ID查询详情】根据系统用户Id查询用户详情")
    @PostMapping(value = "querySystemUserResByUserId")
    public ServiceResult<QuerySystemUserRes> querySystemUserResByUserId(@Valid @RequestBody @ApiParam(value = "入参类") SystemUserIdReq req) {
        return systemUserService.querySystemUserResByUserId(req);
    }


    @ApiOperation(value = "【保存】系统用户信息")
    @PostMapping(value = "saveSystemUser")
    @OperationLog(moduleName = LogEnum.ModuleName.USER_ADMIN, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveSystemUser(@Valid @RequestBody @ApiParam(value = "入参类") SaveSystemUserReq req) {
        return systemUserService.saveSystemUser(req);
    }


    @ApiOperation(value = "【修改】系统用户信息")
    @PostMapping(value = "updateSystemUser")
    @OperationLog(moduleName = LogEnum.ModuleName.USER_ADMIN, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateSystemUser(@Valid @RequestBody @ApiParam(value = "入参类") UpdateSystemUserReq req) {
        return systemUserService.updateSystemUser(req);
    }


    @ApiOperation(value = "【删除】根据系统用户id删除用户")
    @PostMapping(value = "deleteSystemUserByUserId")
    @OperationLog(moduleName = LogEnum.ModuleName.USER_ADMIN, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteSystemUserByUserId(@Valid @RequestBody @ApiParam(value = "入参类") SystemUserIdReq req) {
        return systemUserService.deleteSystemUserByUserId(req);
    }

    @ApiOperation(value = "【改变状态】根据用户id改变用户状态")
    @PostMapping(value = "updateSystemUserStatusByUserId")
    @OperationLog(moduleName = LogEnum.ModuleName.USER_ADMIN, operationType = LogEnum.OperationType.UPDATE_STATUS)
    public ServiceResult updateSystemUserStatusByUserId(@Valid @RequestBody @ApiParam(value = "入参类") SystemUserIdReq req){
        return systemUserService.updateSystemUserStatusByUserId(req);
    }


    @ApiOperation(value = "【重置密码】根据用户id改变重置密码")
    @PostMapping(value = "resetSystemUserPassword")
    @OperationLog(moduleName = LogEnum.ModuleName.USER_ADMIN, operationType = LogEnum.OperationType.RESET_PASSWORD)
    public ServiceResult resetSystemUserPassword(@Valid @RequestBody @ApiParam(value = "入参类") ResetSystemUserPasswordReq req){
        return systemUserService.resetSystemUserPassword(req);
    }
}
