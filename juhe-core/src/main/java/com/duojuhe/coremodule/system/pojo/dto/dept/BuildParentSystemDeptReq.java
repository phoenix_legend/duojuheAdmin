package com.duojuhe.coremodule.system.pojo.dto.dept;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BuildParentSystemDeptReq extends BaseBean {
    @ApiModelProperty(value = "上级部门id，-1表示顶级部门", required = true)
    private String parentId;

    @ApiModelProperty(value = "部门层级path", required = true)
    private String deptPath;

    @ApiModelProperty(value = "租户ID")
    private String tenantId;
}
