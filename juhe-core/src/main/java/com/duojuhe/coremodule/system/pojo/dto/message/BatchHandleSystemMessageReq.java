package com.duojuhe.coremodule.system.pojo.dto.message;

import com.duojuhe.common.bean.DataScopeFilterBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BatchHandleSystemMessageReq extends DataScopeFilterBean {
    @ApiModelProperty(value = "处理状态取数据字典")
    private String handleStatusCode;

    @ApiModelProperty(value = "处理人id")
    private String handleUserId;

    @ApiModelProperty(value = "处理时间")
    private Date handleTime;
}
