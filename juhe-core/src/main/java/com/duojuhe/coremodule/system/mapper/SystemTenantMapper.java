package com.duojuhe.coremodule.system.mapper;

import com.duojuhe.coremodule.system.entity.SystemTenant;
import com.duojuhe.coremodule.system.pojo.dto.tenant.QuerySystemTenantPageReq;
import com.duojuhe.coremodule.system.pojo.dto.tenant.QuerySystemTenantPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemTenantMapper extends TkMapper<SystemTenant> {

    /**
     * 分页查询 根据条件查询租户list
     *
     * @return
     */
    List<QuerySystemTenantPageRes> querySystemTenantPageResList(@Param("req") QuerySystemTenantPageReq req);

}
