package com.duojuhe.coremodule.system.mapper;


import com.duojuhe.coremodule.system.entity.SystemDict;
import com.duojuhe.coremodule.system.pojo.dto.dict.*;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemDictMapper extends TkMapper<SystemDict> {

    /**
     * 根据主键查询
     * @param dictId
     * @return
     */
    QuerySystemDictRes querySystemDictResByDictId(String dictId);

    /**
     * 分页查询 根据条件查询一级数据字典list
     *
     * @return
     */
    List<QueryOneLevelSystemDictPageRes> queryOneLevelSystemDictPageResList(@Param("req") QueryOneLevelSystemDictPageReq req);


    /**
     * 分页查询 根据条件查询子级数据字典list
     *
     * @return
     */
    List<QueryChildrenSystemDictPageRes> queryChildrenSystemDictPageResList(@Param("req") QueryChildrenSystemDictPageReq req);
}