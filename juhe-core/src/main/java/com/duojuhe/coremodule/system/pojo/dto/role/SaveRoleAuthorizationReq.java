package com.duojuhe.coremodule.system.pojo.dto.role;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveRoleAuthorizationReq extends SystemRoleIdReq {
    @ApiModelProperty(value = "角色所对应的部门id集合，留空表示清除该角色所有权限", example = "1")
    private List<String> deptIdList;

    @ApiModelProperty(value = "角色所对应的菜单id集合，留空表示清除该角色所有权限", example = "1")
    private List<String> menuIdList;

    @ApiModelProperty(value = "数据范围:TENANT_ALL_DATA=所有数据权限, CUSTOM_DATA=自定义数据权限,SELF_DEPT=本部门数据权限,SELF_DEPT_SUBORDINATE=本部门及以下数据权限,SELF_DATA=自己的数据", example = "SELF_DATA",required=true)
    @NotBlank(message = "数据范围不能为空")
    @Length(max = 50, message = "数据范围不得超过{max}位字符")
    @Pattern(regexp = "^[TENANT_ALL_DATA|CUSTOM_DATA|SELF_DEPT|SELF_DEPT_SUBORDINATE|SELF_DATA]*$", message = "数据范围参数错误!")
    private String dataScopeCode;
}
