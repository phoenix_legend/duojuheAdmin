package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("系统租户表")
@Table(name = "system_tenant")
public class SystemTenant extends BaseBean {
    @ApiModelProperty(value = "主键字段", required = true)
    @Column(name = "tenant_id")
    @Id
    private String tenantId;

    @ApiModelProperty(value = "租户名称唯一", required = true)
    @Column(name = "tenant_name")
    private String tenantName;

    @ApiModelProperty(value = "租户简称", required = true)
    @Column(name = "tenant_abbreviation")
    private String tenantAbbreviation;

    @ApiModelProperty(value = "租户编码，具有唯一", required = true)
    @Column(name = "tenant_number")
    private String tenantNumber;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常")
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "最后更新用户")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "创建用户")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "是否是内置:YES是，NO否", required = true)
    @Column(name = "built_in")
    private String builtIn;

    @ApiModelProperty(value = "sm4 加解密密钥key", required = true)
    @Column(name = "sm4_key")
    private String sm4Key;
}
