package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("权限角色表")
@Table(name = "system_role")
public class SystemRole extends BaseBean {
    @ApiModelProperty(value = "角色id", required = true)
    @Column(name = "role_id")
    @Id
    private String roleId;

    @ApiModelProperty(value = "角色名称", required = true)
    @Column(name = "role_name")
    private String roleName;

    @ApiModelProperty(value = "创建用户Id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "角色创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "修改用户id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "角色更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常")
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "ALL_DATA=所有数据权限, CUSTOM_DATA=自定义数据权限,SELF_DEPT=本部门数据权限,SELF_DEPT_SUBORDINATE=本部门及以下数据权限,SELF_DATA=自己的数据")
    @Column(name = "data_scope_code")
    private String dataScopeCode;
}