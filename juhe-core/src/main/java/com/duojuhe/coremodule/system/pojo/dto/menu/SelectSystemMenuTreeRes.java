package com.duojuhe.coremodule.system.pojo.dto.menu;


import com.duojuhe.common.utils.tree.TreeBaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * 该接口特殊处理，由于前段需要这种格式属性
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SelectSystemMenuTreeRes extends TreeBaseBean {
    @ApiModelProperty(value = "菜单ID", example = "1")
    private String menuId;

    @ApiModelProperty(value = "菜单名称", example = "1")
    private String menuName;
}
