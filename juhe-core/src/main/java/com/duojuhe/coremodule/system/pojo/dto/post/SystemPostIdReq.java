package com.duojuhe.coremodule.system.pojo.dto.post;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SystemPostIdReq  extends BaseBean {
    @ApiModelProperty(value = "岗位ID", example = "1",required=true)
    @NotBlank(message = "岗位ID不能为空")
    private String postId;
}
