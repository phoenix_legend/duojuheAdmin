package com.duojuhe.coremodule.system.pojo.dto.user;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SystemUserIdReq extends BaseBean {
    @ApiModelProperty(value = "用户ID", example = "1",required=true)
    @NotBlank(message = "UserID不能为空")
    private String userId;
}
