package com.duojuhe.coremodule.system.pojo.dto.tenant;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemTenantPageReq extends PageHead {

    @ApiModelProperty(value = "租户名称唯一", required = true)
    private String tenantName;

    @ApiModelProperty(value = "租户简称", required = true)
    private String tenantAbbreviation;

    @ApiModelProperty(value = "租户编码，具有唯一", required = true)
    private String tenantNumber;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常")
    private String statusCode;

    @ApiModelProperty(value = "开始日期 yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期 yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    @ApiModelProperty(value = "管理帐号，只能为小写字母和数字，长度2-15", example = "1")
    private String loginName;

    @ApiModelProperty(value = "手机号码", example = "1")
    private String mobileNumber;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}
