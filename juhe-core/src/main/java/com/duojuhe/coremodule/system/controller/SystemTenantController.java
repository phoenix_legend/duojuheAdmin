package com.duojuhe.coremodule.system.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.annotation.RepeatSubmit;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.tenant.*;
import com.duojuhe.coremodule.system.service.SystemTenantService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/systemTenant/")
@Api(tags = {"【租户管理】租户管理相关接口"})
@Slf4j
public class SystemTenantController {
    @Resource
    private SystemTenantService systemTenantService;

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【分页查询】分页查询租户list")
    @PostMapping(value = "querySystemTenantPageResList")
    public ServiceResult<PageResult<List<QuerySystemTenantPageRes>>> querySystemTenantPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySystemTenantPageReq req) {
        return systemTenantService.querySystemTenantPageResList(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【保存】租户信息")
    @PostMapping(value = "saveSystemTenant")
    @OperationLog(moduleName = LogEnum.ModuleName.TENANT_ADMIN, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveSystemTenant(@Valid @RequestBody @ApiParam(value = "入参类") SaveSystemTenantReq req) {
        return systemTenantService.saveSystemTenant(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【修改】租户信息")
    @PostMapping(value = "updateSystemTenant")
    @OperationLog(moduleName = LogEnum.ModuleName.TENANT_ADMIN, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateSystemTenant(@Valid @RequestBody @ApiParam(value = "入参类") UpdateSystemTenantReq req) {
        return systemTenantService.updateSystemTenant(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【改变状态】根据租户id改变租户状态")
    @PostMapping(value = "updateSystemTenantStatusByTenantId")
    @OperationLog(moduleName = LogEnum.ModuleName.TENANT_ADMIN, operationType = LogEnum.OperationType.UPDATE_STATUS)
    public ServiceResult updateSystemTenantStatusByTenantId(@Valid @RequestBody @ApiParam(value = "入参类") SystemTenantIdReq req){
        return systemTenantService.updateSystemTenantStatusByTenantId(req);
    }



    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【租户绑定菜单-渲染租户绑定菜单】查询TenantId对应的菜单权限集合")
    @PostMapping(value = "querySystemMenuIdListByTenantId")
    public ServiceResult<List<String>> querySystemMenuIdListByTenantId(@Valid @RequestBody @ApiParam(value = "入参类") SystemTenantIdReq req) {
        return systemTenantService.querySystemMenuIdListByTenantId(req);
    }


    @ApiOperation(value = "【重置密码】根据租户id改变重置密码")
    @PostMapping(value = "resetSystemTenantUserPassword")
    @OperationLog(moduleName = LogEnum.ModuleName.TENANT_ADMIN, operationType = LogEnum.OperationType.RESET_PASSWORD)
    public ServiceResult resetSystemTenantUserPassword(@Valid @RequestBody @ApiParam(value = "入参类") ResetSystemTenantUserPasswordReq req){
        return systemTenantService.resetSystemTenantUserPassword(req);
    }
}
