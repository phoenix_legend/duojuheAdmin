package com.duojuhe.coremodule.system.pojo.dto.post;

import com.duojuhe.common.bean.DataScopeFilterBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SelectSystemPostReq extends DataScopeFilterBean {
    @ApiModelProperty(value = "岗位名称", example = "1")
    private String postName;
}
