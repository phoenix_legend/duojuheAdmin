package com.duojuhe.coremodule.system.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.log.QuerySystemLogPageReq;
import com.duojuhe.coremodule.system.pojo.dto.log.QuerySystemLogPageRes;
import com.duojuhe.coremodule.system.pojo.dto.log.QuerySystemLogRes;
import com.duojuhe.coremodule.system.pojo.dto.log.SystemLogIdReq;

import java.util.List;

/**
 * 系统操作日志相关接口
 */
public interface SystemLogService {


    /**
     * 分页查询 根据条件查询日志list
     */
    ServiceResult<PageResult<List<QuerySystemLogPageRes>>> querySystemLogPageResList(QuerySystemLogPageReq req);


    /**
     * 根据日志ID查询日志详情数据
     */
    ServiceResult<QuerySystemLogRes> querySystemLogResByLogId(SystemLogIdReq req);
}
