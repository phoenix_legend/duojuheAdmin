package com.duojuhe.coremodule.system.mapper;


import com.duojuhe.coremodule.system.entity.SystemParameter;
import com.duojuhe.coremodule.system.pojo.dto.parameter.QuerySystemParameterPageReq;
import com.duojuhe.coremodule.system.pojo.dto.parameter.QuerySystemParameterPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemParameterMapper extends TkMapper<SystemParameter> {
    /**
     * 分页查询系统参数list
     *
     * @return
     */
    List<QuerySystemParameterPageRes> querySystemParameterPageResList(@Param("req") QuerySystemParameterPageReq req);

}