package com.duojuhe.coremodule.system.service;


import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.entity.SystemParameter;
import com.duojuhe.coremodule.system.pojo.dto.parameter.*;

import java.util.List;
import java.util.Map;

/**
 * 系统参数相关接口
 */
public interface SystemParameterService {

    /**
     * 【根据code集合获取系统参数】根据code集合获取系统参数
     * @param req
     * @return
     */
    ServiceResult<Map<String, SystemParameter>> querySystemParameterListByParameterCodeList(SystemParameterCodeListReq req);

    /**
     * 【分页查询】分页查询系统参数list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QuerySystemParameterPageRes>>> querySystemParameterPageResList(QuerySystemParameterPageReq req);

    /**
     * 【新增】修改系统参数
     * @param req
     * @return
     */
    ServiceResult saveSystemParameter(SaveSystemParameterReq req);

    /**
     * 【修改】修改系统参数
     * @param req
     * @return
     */
    ServiceResult updateSystemParameter(UpdateSystemParameterReq req);


    /**
     * 删除系统参数
     * @param req
     * @return
     */
    ServiceResult deleteSystemParameterByParameterId(SystemParameterIdReq req);


    /**
     * 加载系统参数缓存数据
     */
    void loadingSystemParameterCache();

    /**
     * 清空系统参数缓存数据
     */
    void clearSystemParameterCache();

    /**
     * 重置系统参数缓存数据
     */
    ServiceResult resetSystemParameterCache();
}
