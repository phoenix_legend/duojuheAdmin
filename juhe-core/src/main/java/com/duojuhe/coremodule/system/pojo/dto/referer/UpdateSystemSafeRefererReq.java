package com.duojuhe.coremodule.system.pojo.dto.referer;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateSystemSafeRefererReq extends SaveSystemSafeRefererReq {
    @ApiModelProperty(value = "安全白名单ID", example = "1",required=true)
    @NotBlank(message = "安全白名单ID不能为空")
    private String id;
}
