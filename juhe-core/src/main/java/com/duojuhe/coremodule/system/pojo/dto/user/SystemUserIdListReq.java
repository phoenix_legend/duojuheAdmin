package com.duojuhe.coremodule.system.pojo.dto.user;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SystemUserIdListReq extends BaseBean {
    @ApiModelProperty(value = "用户ID集合",required=true)
    @NotEmpty(message = "用户ID集合不能为空")
    @Valid
    @NotNull
    @Size(min = 1, message = "用户ID集合不能为空")
    private List<String> userIdList;
}
