package com.duojuhe.coremodule.system.pojo.dto.dept;

import com.duojuhe.common.utils.tree.TreeBaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * 部门查询tree结构
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SelectSystemDeptTreeRes extends TreeBaseBean {

    @ApiModelProperty(value = "主键", example = "1")
    private String deptId;

    @ApiModelProperty(value = "部门名称", example = "1")
    private String deptName;
}
