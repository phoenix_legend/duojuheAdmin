package com.duojuhe.coremodule.system.pojo.dto.tenant;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemTenantPageRes extends BaseBean {
    @ApiModelProperty(value = "主键字段", required = true)
    private String tenantId;

    @ApiModelProperty(value = "租户名称唯一", required = true)
    private String tenantName;

    @ApiModelProperty(value = "租户简称", required = true)
    private String tenantAbbreviation;

    @ApiModelProperty(value = "租户编码，具有唯一", required = true)
    private String tenantNumber;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常")
    private String statusCode;

    @ApiModelProperty(value = "状态中文 FORBID禁用 NORMAL正常", example = "1")
    private String statusName;

    @ApiModelProperty(value = "状态显示颜色", example = "1")
    private String statusColor;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date updateTime;


    @ApiModelProperty(value = "管理帐号，只能为小写字母和数字，长度2-15", example = "1")
    private String loginName;

    @ApiModelProperty(value = "手机号码", example = "1")
    private String mobileNumber;
}
