package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("系统数据过滤权限表")
@Table(name = "system_data_filter")
public class SystemDataFilter extends BaseBean {
    @ApiModelProperty(value = "主键字段", required = true)
    @Column(name = "id")
    @Id
    private String id;

    @ApiModelProperty(value = "目标id，不同表的主键", required = true)
    @Column(name = "target_id")
    private String targetId;

    @ApiModelProperty(value = "目标id表", required = true)
    @Column(name = "target_id_source")
    private String targetIdSource;

    @ApiModelProperty(value = "关联资源id", required = true)
    @Column(name = "relation_id")
    private String relationId;

    @ApiModelProperty(value = "关联资源id表", required = true)
    @Column(name = "relation_id_source")
    private String relationIdSource;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;
}