package com.duojuhe.coremodule.system.pojo.dto.referer;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveSystemSafeRefererReq extends BaseBean {
    @ApiModelProperty(value = "安全白名单地址，限255个字符，HTTP/HTTPS开头字符串", example = "http://www.duojuhe.com",required=true)
    @NotBlank(message = "安全白名单地址不能为空")
    @Length(max = 255, message = "安全白名单地址不得超过{max}位字符")
    @Pattern(regexp = RegexpConstants.HTTP_URL, message = "安全白名单地址格式错误!")
    private String allowReferer;
}
