package com.duojuhe.coremodule.system.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.referer.*;

import java.util.List;

public interface SystemSafeRefererService {

    /**
     * 【分页查询】分页查询系统安全白名单list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QuerySystemSafeRefererPageRes>>> querySystemSafeRefererPageResList(QuerySystemSafeRefererPageReq req);

    /**
     * 【新增】修改系统安全白名单
     * @param req
     * @return
     */
    ServiceResult saveSystemSafeReferer(SaveSystemSafeRefererReq req);

    /**
     * 【修改】修改系统安全白名单
     * @param req
     * @return
     */
    ServiceResult updateSystemSafeReferer(UpdateSystemSafeRefererReq req);


    /**
     * 删除系统安全白名单
     * @param req
     * @return
     */
    ServiceResult deleteSystemSafeRefererById(SystemSafeRefererIdReq req);


    /**
     * 加载系统安全白名单缓存数据
     */
    void loadingSystemSafeRefererCache();

    /**
     * 清空系统安全白名单缓存数据
     */
    void clearSystemSafeRefererCache();

    /**
     * 重置系统安全白名单缓存数据
     */
    ServiceResult resetSystemSafeRefererCache();
}
