package com.duojuhe.coremodule.system.controller;

import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.message.MessageNumRes;
import com.duojuhe.coremodule.system.pojo.dto.message.QuerySystemMessagePageReq;
import com.duojuhe.coremodule.system.pojo.dto.message.QuerySystemMessagePageRes;
import com.duojuhe.coremodule.system.pojo.dto.message.SystemMessageIdReq;
import com.duojuhe.coremodule.system.service.SystemMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/systemMessage/")
@Api(tags = {"【系统消息】系统消息相关接口"})
@Slf4j
public class SystemMessageController {
    @Resource
    private SystemMessageService systemMessageService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【分页查询】分页查询系统消息list")
    @PostMapping(value = "querySystemMessagePageResList")
    public ServiceResult<PageResult<List<QuerySystemMessagePageRes>>> querySystemMessagePageResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySystemMessagePageReq req) {
        return systemMessageService.querySystemMessagePageResList(req);
    }

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【统计未处理 数量】统计未读数量")
    @PostMapping(value = "queryCountUntreatedSystemMessage")
    public ServiceResult<MessageNumRes> queryCountUntreatedSystemMessage() {
        return systemMessageService.queryCountUntreatedSystemMessage(new DataScopeFilterBean());
    }


    @ApiOperation(value = "【根据id设置未处理】根据id设置未处理")
    @PostMapping(value = "updateSystemMessageHandleStatusByMessageId")
    public ServiceResult updateSystemMessageHandleStatusByMessageId(@Valid @RequestBody @ApiParam(value = "入参类") SystemMessageIdReq req) {
        return systemMessageService.updateSystemMessageHandleStatusByMessageId(req);
    }

    @ApiOperation(value = "【一键未处理】一键未处理")
    @PostMapping(value = "updateAllSystemMessageHandleStatus")
    public ServiceResult updateAllSystemMessageHandleStatus() {
        return systemMessageService.updateAllSystemMessageHandleStatus(new DataScopeFilterBean());
    }

}
