package com.duojuhe.coremodule.system.controller;


import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.dept.*;
import com.duojuhe.coremodule.system.pojo.dto.user.QuerySelectSystemDeptAndUserRes;
import com.duojuhe.coremodule.system.service.SystemDeptService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/systemDept/")
@Api(tags = {"【部门管理】部门管理相关接口"})
@Slf4j
public class SystemDeptController {
    @Resource
    private SystemDeptService systemDeptService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【下拉选择使用-查询部门】查询可供前端选择使用的角色，一般用于用户添加下拉选择使用")
    @PostMapping(value = "queryNormalSelectSystemDeptResList")
    public ServiceResult<List<SelectSystemDeptRes>> queryNormalSelectSystemDeptResList(@Valid @RequestBody @ApiParam(value = "入参类") SelectSystemDeptReq req) {
        return systemDeptService.queryNormalSelectSystemDeptResList(req);
    }

    @ApiOperation(value = "【根据ID查询详情】根据部门ID获取部门资源信息")
    @PostMapping(value = "querySystemDeptResByDeptId")
    public ServiceResult<QuerySystemDeptRes> querySystemDeptResByDeptId(@Valid @RequestBody @ApiParam(value = "入参类") SystemDeptIdReq req) {
        return systemDeptService.querySystemDeptResByDeptId(req);
    }

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【部门管理-tree型结构查询】查询部门列表，一般用于部门管理界面")
    @PostMapping(value = "querySystemDeptTreeResList")
    public ServiceResult<List<QuerySystemDeptTreeRes>> querySystemDeptTreeResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySystemDeptTreeReq req) {
        return systemDeptService.querySystemDeptTreeResList(req);
    }





    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【选择员工返回结果包含部门信息】选择员工返回结果包含部门信息")
    @PostMapping(value = "querySelectSystemDeptAndUserResList")
    public ServiceResult<QuerySelectSystemDeptAndUserRes> querySelectSystemDeptAndUserResList(@Valid @RequestBody @ApiParam(value = "入参类") SelectSystemDeptAndUserReq req) {
        return systemDeptService.querySelectSystemDeptAndUserResList(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【部门选择使用-tree型结构查询】一般用于部门下拉选择界面使用")
    @PostMapping(value = "queryNormalSelectSystemDeptTreeResList")
    public ServiceResult<List<SelectSystemDeptTreeRes>> queryNormalSelectSystemDeptTreeResList(@Valid @RequestBody @ApiParam(value = "入参类") SelectSystemDeptTreeReq req) {
        return systemDeptService.queryNormalSelectSystemDeptTreeResList(req);
    }

    @ApiOperation(value = "【保存】部门信息")
    @PostMapping(value = "saveSystemDept")
    @OperationLog(moduleName = LogEnum.ModuleName.DEPT_ADMIN, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveSystemDept(@Valid @RequestBody @ApiParam(value = "入参类") SaveSystemDeptReq req) {
        return systemDeptService.saveSystemDept(req);
    }

    @ApiOperation(value = "【修改】部门信息")
    @PostMapping(value = "updateSystemDept")
    @OperationLog(moduleName = LogEnum.ModuleName.DEPT_ADMIN, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateSystemDept(@Valid @RequestBody @ApiParam(value = "入参类") UpdateSystemDeptReq req) {
        return systemDeptService.updateSystemDept(req);
    }

    @ApiOperation(value = "【删除】根据id删除部门，存在子项则不能删除")
    @PostMapping(value = "deleteSystemDeptByDeptId")
    @OperationLog(moduleName = LogEnum.ModuleName.DEPT_ADMIN, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteSystemDeptByDeptId(@Valid @RequestBody @ApiParam(value = "入参类") SystemDeptIdReq req) {
        return systemDeptService.deleteSystemDeptByDeptId(req);
    }


}
