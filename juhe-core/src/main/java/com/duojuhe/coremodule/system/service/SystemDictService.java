package com.duojuhe.coremodule.system.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.dict.*;

import java.util.List;
import java.util.Map;

/**
 * 系统数据字典相关接口
 */
public interface SystemDictService {

    /**
     * 根据主键查询
     * @param req
     * @return
     */
    ServiceResult<QuerySystemDictRes> querySystemDictResByDictId(SystemDictIdReq req);

    /**
     * 查询一级数据字典
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryOneLevelSystemDictPageRes>>> queryOneLevelSystemDictPageResList(QueryOneLevelSystemDictPageReq req);


    /**
     * 查询子级数据字典
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryChildrenSystemDictPageRes>>> queryChildrenSystemDictPageResList(QueryChildrenSystemDictPageReq req);

    /**
     * 保存数据字典
     * @param req
     * @return
     */
    ServiceResult saveSystemDict(SaveSystemDictReq req);

    /**
     * 修改数据字典
     * @param req
     * @return
     */
    ServiceResult updateSystemDict(UpdateSystemDictReq req);


    /**
     * 删除数据字典
     * @param req
     * @return
     */
    ServiceResult deleteSystemDictByDictId(SystemDictIdReq req);


    /**
     * 加载数据字典缓存数据
     */
    void loadingSystemDictCache();

    /**
     * 清空数据字典缓存数据
     */
    void clearSystemDictCache();

    /**
     * 重置数据字典缓存数据
     */
    ServiceResult resetSystemDictCache();


    /**
     * 【根据code集合获取数据字典】根据数据字典父级code编码获取数据字典
     * @param req
     * @return
     */
    ServiceResult<Map<String,List<SelectChildrenDictRes>>> queryCommonDictListByParentDictCodeList(SystemDictCodeListReq req);
}
