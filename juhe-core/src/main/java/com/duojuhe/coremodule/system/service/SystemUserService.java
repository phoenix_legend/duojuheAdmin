package com.duojuhe.coremodule.system.service;


import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.user.*;

import java.util.List;

/**
 * 系统用户相关接口
 */
public interface SystemUserService {

    /**
     * 【分页查询】分页查询用户list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QuerySystemUserPageRes>>> querySystemUserPageResList(QuerySystemUserPageReq req);



    /**
     * 【分页查询】分页选择系统用户list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QuerySelectSystemUserRes>>> querySelectSystemUserResList(QuerySelectSystemUserReq req);



    /**
     * 【根据ID查询详情】根据用户Id查询用户详情
     * @param req
     * @return
     */
    ServiceResult<QuerySystemUserRes> querySystemUserResByUserId(SystemUserIdReq req);


    /**
     * 【保存】用户信息
     * @param req
     * @return
     */
    ServiceResult saveSystemUser(SaveSystemUserReq req);


    /**
     * 【修改】用户信息
     * @param req
     * @return
     */
    ServiceResult updateSystemUser(UpdateSystemUserReq req);


    /**
     * 【删除】根据用户id删除用户
     * @param req
     * @return
     */
    ServiceResult deleteSystemUserByUserId(SystemUserIdReq req);


    /**
     * 【改变状态】根据用户id改变用户状态
     * @param req
     * @return
     */
    ServiceResult updateSystemUserStatusByUserId(SystemUserIdReq req);


    /**
     * 【重置密码】根据用户id重置密码
     * @param req
     * @return
     */
    ServiceResult resetSystemUserPassword(ResetSystemUserPasswordReq req);
}
