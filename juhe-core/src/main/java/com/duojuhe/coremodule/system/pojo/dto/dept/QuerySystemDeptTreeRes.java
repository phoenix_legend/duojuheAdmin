package com.duojuhe.coremodule.system.pojo.dto.dept;

import com.duojuhe.common.utils.tree.TreeBaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * 部门查询tree结构
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemDeptTreeRes extends TreeBaseBean {
    @ApiModelProperty(value = "主键", example = "1")
    private String deptId;

    @ApiModelProperty(value = "部门领导", example = "1")
    private String deptLeader;

    @ApiModelProperty(value = "部门名称", example = "1")
    private String deptName;

    @ApiModelProperty(value = "部门编码", example = "1")
    private String deptCode;

    @ApiModelProperty(value = "部门电话", example = "1")
    private String deptTelephone;

    @ApiModelProperty(value = "部门传真", example = "1")
    private String deptFax;

    @ApiModelProperty(value = "备注", example = "1")
    private String remark;

    @ApiModelProperty(value = "排序", example = "1")
    private Integer sort;

    @ApiModelProperty(value = "是否是内置:YES是，NO否")
    private String builtIn;

    @ApiModelProperty(value = "是否是内置")
    private String builtInName;

    @ApiModelProperty(value = "是否是内置显示颜色")
    private String builtInColor;
}
