package com.duojuhe.coremodule.system.pojo.dto.menu;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


/**
 * 菜单id
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SystemMenuIdReq extends BaseBean {
    @ApiModelProperty(value = "菜单ID", example = "1",required=true)
    @NotBlank(message = "菜单ID不能为空")
    private String menuId;
}
