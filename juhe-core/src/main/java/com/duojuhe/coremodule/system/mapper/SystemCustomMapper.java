package com.duojuhe.coremodule.system.mapper;

import com.duojuhe.coremodule.system.entity.SystemCustom;
import com.tkmapper.TkMapper;

public interface SystemCustomMapper extends TkMapper<SystemCustom> {

}