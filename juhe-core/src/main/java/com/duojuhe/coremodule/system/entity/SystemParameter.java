package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("系统参数表")
@Table(name = "system_parameter")
public class SystemParameter extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "parameter_id")
    @Id
    private String parameterId;

    @ApiModelProperty(value = "参数编码", required = true)
    @Column(name = "parameter_code")
    private String parameterCode;

    @ApiModelProperty(value = "参数名称", required = true)
    @Column(name = "parameter_name")
    private String parameterName;

    @ApiModelProperty(value = "参数取值")
    @Column(name = "parameter_value")
    private String parameterValue;

    @ApiModelProperty(value = "参数描述")
    @Column(name = "description")
    private String description;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "是否是内置:YES是，NO否", required = true)
    @Column(name = "built_in")
    private String builtIn;
}