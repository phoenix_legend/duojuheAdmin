package com.duojuhe.coremodule.system.service.impl;

import com.duojuhe.common.bean.UserTokenInfoVo;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.encryption.md5.MD5Util;
import com.duojuhe.coremodule.system.entity.SystemCustom;
import com.duojuhe.coremodule.system.mapper.SystemCustomMapper;
import com.duojuhe.coremodule.system.pojo.dto.custom.SaveSystemCustomReq;
import com.duojuhe.coremodule.system.pojo.dto.custom.SystemCustomKeyReq;
import com.duojuhe.coremodule.system.service.SystemCustomService;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 用户自定义喜好风格
 */
@Slf4j
@Service
public class SystemCustomServiceImpl extends BaseService implements SystemCustomService {
    @Resource
    private SystemCustomMapper systemCustomMapper;

    /**
     * 根据key查询自定义布局详情
     */
    @Override
    public ServiceResult<String> querySystemCustomContentJsonByCustomKey(SystemCustomKeyReq req) {
        //当前登录人
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //主键ID 自定义key和用户id的MD5值
        String customId = MD5Util.getMD532(userTokenInfoVo.getUserId()+req.getCustomKey());
        //主键查询
        SystemCustom systemCustom = systemCustomMapper.selectByPrimaryKey(customId);
        return ServiceResult.ok(systemCustom!=null?systemCustom.getContent():"");
    }

    /**
     * 保存自定义布局参数
     */
    @Override
    public ServiceResult<String> saveSystemCustom(SaveSystemCustomReq req) {
        //自定义布局内容
        String content = req.getContent();
        //当前登录人
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //用户ID
        String userId = userTokenInfoVo.getUserId();
        //主键ID 自定义key和用户id的MD5值
        String customId = MD5Util.getMD532(userId+req.getCustomKey());
        //当前时间
        Date date = new Date();
        //先查询用户是否设置过改key的数据
        SystemCustom systemCustomOld = systemCustomMapper.selectByPrimaryKey(customId);
        //如果存在则更新
        SystemCustom systemCustom = new SystemCustom();
        systemCustom.setCustomId(customId);
        systemCustom.setCustomKey(req.getCustomKey());
        systemCustom.setUpdateTime(date);
        systemCustom.setContent(req.getContent());
        if (systemCustomOld!=null){
            systemCustomMapper.updateByPrimaryKeySelective(systemCustom);
        }else{
            //如果不存在则插入
            systemCustom.setCreateUserIdSource(userTokenInfoVo.getUserIdSource());
            systemCustom.setCreateDeptId(userTokenInfoVo.getCreateDeptId());
            //租户id
            systemCustom.setTenantId(userTokenInfoVo.getTenantId());
            systemCustom.setCreateUserId(userId);
            systemCustom.setCreateTime(date);
            systemCustomMapper.insertSelective(systemCustom);
        }
        return ServiceResult.ok(content);
    }


    /**
     * 根据key删除自定义布局
     */
    @Override
    public ServiceResult deleteSystemCustomByCustomKey(SystemCustomKeyReq req) {
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //用户ID
        String userId = userTokenInfoVo.getUserId();
        //主键ID 自定义key和用户id的MD5值
        String customId = MD5Util.getMD532(userId+req.getCustomKey());
        systemCustomMapper.deleteByPrimaryKey(customId);
        return ServiceResult.ok();
    }


}
