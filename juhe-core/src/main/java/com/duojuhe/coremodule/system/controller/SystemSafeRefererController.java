package com.duojuhe.coremodule.system.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.referer.*;
import com.duojuhe.coremodule.system.service.SystemSafeRefererService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/systemSafeReferer/")
@Api(tags = {"【系统安全白名单】系统安全白名单相关接口"})
@Slf4j
public class SystemSafeRefererController {
    @Resource
    private SystemSafeRefererService systemSafeRefererService;

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【分页查询】分页查询系统安全白名单list")
    @PostMapping(value = "querySystemSafeRefererPageResList")
    public ServiceResult<PageResult<List<QuerySystemSafeRefererPageRes>>> querySystemSafeRefererPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySystemSafeRefererPageReq req) {
        return systemSafeRefererService.querySystemSafeRefererPageResList(req);
    }

    @ApiOperation(value = "【新增】新增系统安全白名单")
    @PostMapping(value = "saveSystemSafeReferer")
    @OperationLog(moduleName = LogEnum.ModuleName.SYSTEM_SAFE_REFERER, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveSystemSafeReferer(@Valid @RequestBody @ApiParam(value = "入参类") SaveSystemSafeRefererReq req) {
        return systemSafeRefererService.saveSystemSafeReferer(req);
    }

    @ApiOperation(value = "【修改】修改系统安全白名单")
    @PostMapping(value = "updateSystemSafeReferer")
    @OperationLog(moduleName = LogEnum.ModuleName.SYSTEM_SAFE_REFERER, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateSystemSafeReferer(@Valid @RequestBody @ApiParam(value = "入参类") UpdateSystemSafeRefererReq req) {
        return systemSafeRefererService.updateSystemSafeReferer(req);
    }

    @ApiOperation(value = "【删除】删除系统安全白名单")
    @PostMapping(value = "deleteSystemSafeRefererById")
    @OperationLog(moduleName = LogEnum.ModuleName.SYSTEM_SAFE_REFERER, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteSystemSafeRefererById(@Valid @RequestBody @ApiParam(value = "入参类") SystemSafeRefererIdReq req) {
        return systemSafeRefererService.deleteSystemSafeRefererById(req);
    }

    @ApiOperation(value = "【重置缓存】重置系统缓存")
    @PostMapping(value = "resetSystemSafeRefererCache")
    public ServiceResult resetSystemSafeRefererCache(){
        return systemSafeRefererService.resetSystemSafeRefererCache();
    }
}
