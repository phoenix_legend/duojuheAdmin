package com.duojuhe.coremodule.system.pojo.dto.dept;


import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * 部门保存请求类
 *
 * @date 2018/5/23.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveSystemDeptReq extends BaseBean {
    @ApiModelProperty(value = "父级ID，留空表示是顶级部门栏，长度0-32", example = "1")
    @Length(max = 32, message = "不得超过{max}位字符")
    private String parentId;

    @ApiModelProperty(value = "部门领导，长度1-20", example = "1",required=true)
    @NotBlank(message = "部门领导不能为空")
    @Length(max = 20, message = "部门负责人不得超过{max}位字符")
    private String deptLeader;

    @ApiModelProperty(value = "部门名称，长度1-200", example = "1",required=true)
    @NotBlank(message = "部门名称不能为空")
    @Length(max = 200, message = "部门名称不得超过{max}位字符")
    private String deptName;

    @ApiModelProperty(value = "部门电话", example = "1")
    @Length(max = 50, message = "部门电话不得超过{max}位字符")
    private String deptTelephone;

    @ApiModelProperty(value = "部门传真", example = "1")
    @Length(max = 50, message = "部门传真不得超过{max}位字符")
    private String deptFax;

    @ApiModelProperty(value = "备注", example = "1")
    @Pattern(regexp = RegexpConstants.REMARK, message = "备注禁止输入特殊字符!")
    @Length(max = 100, message = "备注不得超过{max}位字符")
    private  String remark;

    @ApiModelProperty(value = "排序，只能为纯数字", example = "1",required=true)
    @NotNull(message = "排序不能为空")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;
}
