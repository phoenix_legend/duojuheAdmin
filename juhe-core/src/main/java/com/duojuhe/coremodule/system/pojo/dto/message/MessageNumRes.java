package com.duojuhe.coremodule.system.pojo.dto.message;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MessageNumRes extends BaseBean {
    @ApiModelProperty(value = "系统消息未读数量", example = "1")
    private Integer systemMessageNum = 0;

    @ApiModelProperty(value = "chat多聊未读数量", example = "1")
    private Integer chatMessageNum = 0;

    @ApiModelProperty(value = "好友申请数量", example = "1")
    private Integer friendApplyNum = 0;
}
