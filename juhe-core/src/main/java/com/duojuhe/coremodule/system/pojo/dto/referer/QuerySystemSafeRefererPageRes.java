package com.duojuhe.coremodule.system.pojo.dto.referer;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemSafeRefererPageRes extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String id;

    @ApiModelProperty(value = "允许请求的域名或者ip")
    private String allowReferer;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "是否是内置:YES是，NO否")
    private String builtIn;

    @ApiModelProperty(value = "是否是内置")
    private String builtInName;

    @ApiModelProperty(value = "是否是内置显示颜色")
    private String builtInColor;
}
