package com.duojuhe.coremodule.system.controller;


import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.role.*;
import com.duojuhe.coremodule.system.service.SystemRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/systemRole/")
@Api(tags = {"【角色管理】角色管理相关接口"})
@Slf4j
public class SystemRoleController {
    @Resource
    private SystemRoleService systemRoleService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【分页查询】分页查询角色list")
    @PostMapping(value = "querySystemRolePageResList")
    public ServiceResult<PageResult<List<QuerySystemRolePageRes>>> querySystemRolePageResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySystemRolePageReq req) {
        return systemRoleService.querySystemRolePageResList(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【下拉选择使用-查询所有角色】查询可供前端选择使用的角色，一般用于用户添加下拉选择使用")
    @PostMapping(value = "queryNormalSelectSystemRoleResList")
    public ServiceResult<List<SelectSystemRoleRes>> queryNormalSelectSystemRoleResList(@Valid @RequestBody @ApiParam(value = "入参类") SelectSystemRoleReq req) {
        return systemRoleService.queryNormalSelectSystemRoleResList(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【根据ID查询详情】根据角色ID查询角色详情")
    @PostMapping(value = "querySystemRoleResByRoleId")
    public ServiceResult<QuerySystemRoleRes> querySystemRoleResByRoleId(@Valid @RequestBody @ApiParam(value = "入参类") SystemRoleIdReq req) {
        return systemRoleService.querySystemRoleResByRoleId(req);
    }

    @ApiOperation(value = "【保存】角色信息")
    @PostMapping(value = "saveSystemRole")
    @OperationLog(moduleName = LogEnum.ModuleName.ROLE_ADMIN, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveSystemRole(@Valid @RequestBody @ApiParam(value = "入参类") SaveSystemRoleReq req) {
        return systemRoleService.saveSystemRole(req);
    }


    @ApiOperation(value = "【修改】角色信息")
    @PostMapping(value = "updateSystemRole")
    @OperationLog(moduleName = LogEnum.ModuleName.ROLE_ADMIN, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateSystemRole(@Valid @RequestBody @ApiParam(value = "入参类") UpdateSystemRoleReq req) {
        return systemRoleService.updateSystemRole(req);
    }


    @ApiOperation(value = "【删除】根据角色ID删除角色，注意一次仅能删除一个角色，存在绑定关系的则不能删除")
    @PostMapping(value = "deleteSystemRoleByRoleId")
    @OperationLog(moduleName = LogEnum.ModuleName.ROLE_ADMIN, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteSystemRoleByRoleId(@Valid @RequestBody @ApiParam(value = "入参类") SystemRoleIdReq req) {
        return systemRoleService.deleteSystemRoleByRoleId(req);
    }


    @ApiOperation(value = "【角色授权】给角色赋权限，menuIdList为空表示清除该角色所有权限")
    @PostMapping(value = "saveRoleAuthorization")
    @OperationLog(moduleName = LogEnum.ModuleName.ROLE_ADMIN, operationType = LogEnum.OperationType.OTHER)
    public ServiceResult saveRoleAuthorization(@Valid @RequestBody @ApiParam(value = "入参类") SaveRoleAuthorizationReq req) {
        return systemRoleService.saveRoleAuthorization(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【角色绑定菜单-渲染角色绑定菜单】查询roleId对应的菜单权限集合，一般用于角色功能授权编辑时渲染已经用于的权限勾选")
    @PostMapping(value = "querySystemMenuIdListByRoleId")
    public ServiceResult<List<String>> querySystemMenuIdListByRoleId(@Valid @RequestBody @ApiParam(value = "入参类") SystemRoleIdReq req) {
        return systemRoleService.querySystemMenuIdListByRoleId(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【角色绑定部门-渲染角色绑定部门】查询roleId对应的部门ID权限集合，一般用于角色数据授权编辑时渲染已经用于的权限勾选")
    @PostMapping(value = "querySystemDeptIdListByRoleId")
    public ServiceResult<List<String>> querySystemDeptIdListByRoleId(@Valid @RequestBody @ApiParam(value = "入参类") SystemRoleIdReq req) {
        return systemRoleService.querySystemDeptIdListByRoleId(req);
    }
}
