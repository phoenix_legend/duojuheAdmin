package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "system_safe_referer")
public class SystemSafeReferer extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "id")
    @Id
    private String id;

    @ApiModelProperty(value = "允许请求的域名或者ip")
    @Column(name = "allow_referer")
    private String allowReferer;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "修改人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "是否是内置:YES是，NO否")
    @Column(name = "built_in")
    private String builtIn;
}