package com.duojuhe.coremodule.system.mapper;


import com.duojuhe.coremodule.system.entity.SystemLog;
import com.duojuhe.coremodule.system.pojo.dto.log.QuerySystemLogPageReq;
import com.duojuhe.coremodule.system.pojo.dto.log.QuerySystemLogPageRes;
import com.duojuhe.coremodule.system.pojo.dto.log.QuerySystemLogRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemLogMapper extends TkMapper<SystemLog> {
    /**
     * 分页查询 根据条件查询操作日志list
     *
     * @return
     */
    List<QuerySystemLogPageRes> querySystemLogPageResList(@Param("req") QuerySystemLogPageReq req);

    /**
     * 根据日志ID查询日志详情数据
     *
     * @return
     */
    QuerySystemLogRes querySystemLogResByLogId(@Param("logId") String logId);
}