package com.duojuhe.coremodule.system.service.impl;

import com.duojuhe.cache.SystemDictCache;
import com.duojuhe.common.annotation.DataScopeFilter;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.page.PageHelperUtil;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.system.entity.SystemDict;
import com.duojuhe.coremodule.system.mapper.SystemLogMapper;
import com.duojuhe.coremodule.system.pojo.dto.log.*;
import com.duojuhe.coremodule.system.service.SystemLogService;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class SystemLogServiceImpl extends BaseService implements SystemLogService {
    @Resource
    private SystemDictCache systemDictCache;
    @Resource
    private SystemLogMapper systemLogMapper;

    /**
     * 分页查询 根据条件查询日志list
     */
    @DataScopeFilter
    @Override
    public ServiceResult<PageResult<List<QuerySystemLogPageRes>>> querySystemLogPageResList(QuerySystemLogPageReq req) {
        PageHelperUtil.orderByAndStartPage(req, "createTime desc, logId desc");
        List<QuerySystemLogPageRes> list = systemLogMapper.querySystemLogPageResList(req);
        list.forEach(this::setHandleSystemLogDictNameColor);
        return PageHelperUtil.returnServiceResult(req,list);
    }

    /**
     * 根据日志ID查询日志详情数据
     */
    @Override
    public ServiceResult<QuerySystemLogRes> querySystemLogResByLogId(SystemLogIdReq req) {
        String logId = req.getLogId();
        QuerySystemLogRes res = systemLogMapper.querySystemLogResByLogId(logId);
        setHandleSystemLogDictNameColor(res);
        return ServiceResult.ok(res);
    }


    /*====================================私有方法beg================================================**/
    /**
     * 处理返回值的数据字典
     * @param res
     */
    private void setHandleSystemLogDictNameColor(HandleSystemLogDictNameColorRes res) {
        if (res==null){
            return;
        }
        SystemDict operationStatus = systemDictCache.getSystemDictByDictCode(res.getOperationStatusCode());
        res.setOperationStatusName(operationStatus.getDictName());
        res.setOperationStatusColor(operationStatus.getDictColor());

        SystemDict operationType = systemDictCache.getSystemDictByDictCode(res.getOperationTypeCode());
        res.setOperationTypeName(operationType.getDictName());
        res.setOperationTypeColor(operationType.getDictColor());
    }
    /*====================================私有方法end================================================**/
}
