package com.duojuhe.coremodule.system.pojo.dto.role;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemRolePageRes extends HandleSystemRoleDictNameColorRes {

    @ApiModelProperty(value = "主键", example = "1")
    private String roleId;

    @ApiModelProperty(value = "角色名称", example = "1")
    private String roleName;

    @ApiModelProperty(value = "备注", example = "1")
    private String remark;

    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    @ApiModelProperty(value = "创建时间", example = "1")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date updateTime;

    @ApiModelProperty(value = "创建者Id", example = "1")
    private String createUserId;

    @ApiModelProperty(value = "创建者姓名", example = "1")
    private String createUserName;

    @ApiModelProperty(value = "创建部门id")
    private String createDeptId;

    @ApiModelProperty(value = "创建部门名称")
    private String createDeptName;

    @ApiModelProperty(value = "排序，只能为纯数字", example = "1")
    private Integer sort;

}
