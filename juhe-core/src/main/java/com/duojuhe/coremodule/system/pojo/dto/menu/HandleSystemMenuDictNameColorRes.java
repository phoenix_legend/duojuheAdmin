package com.duojuhe.coremodule.system.pojo.dto.menu;

import com.duojuhe.common.utils.tree.TreeBaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HandleSystemMenuDictNameColorRes extends TreeBaseBean {
    @ApiModelProperty(value = "是否是内置:YES是，NO否")
    private String builtIn;

    @ApiModelProperty(value = "是否是内置")
    private String builtInName;

    @ApiModelProperty(value = "是否是内置显示颜色")
    private String builtInColor;

    @ApiModelProperty(value = "状态：FORBID禁用 NORMAL正常", example = "1")
    private String statusCode;

    @ApiModelProperty(value = "状态中文 FORBID禁用 NORMAL正常", example = "1")
    private String statusName;

    @ApiModelProperty(value = "状态显示颜色", example = "1")
    private String statusColor;

    @ApiModelProperty(value = "功能类型 NAVIGATION 导航 BUTTON 功能按钮", example = "1")
    private String menuTypeCode;

    @ApiModelProperty(value = "功能类型 NAVIGATION 导航 BUTTON 功能按钮", example = "1")
    private String menuTypeName;

    @ApiModelProperty(value = "功能类型显示颜色", example = "1")
    private String menuTypeColor;
}
