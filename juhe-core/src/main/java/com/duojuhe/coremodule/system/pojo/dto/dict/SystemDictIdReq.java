package com.duojuhe.coremodule.system.pojo.dto.dict;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


/**
 * 字典id
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SystemDictIdReq extends BaseBean {
    @ApiModelProperty(value = "字典ID", example = "1",required=true)
    @NotBlank(message = "字典ID不能为空")
    private String dictId;
}
