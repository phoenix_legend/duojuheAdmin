package com.duojuhe.coremodule.system.pojo.dto.post;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemPostPageRes extends HandleSystemPostDictNameColorRes {
    @ApiModelProperty(value = "主键")
    private String postId;

    @ApiModelProperty(value = "岗位编码")
    private String postCode;

    @ApiModelProperty(value = "岗位名称")
    private String postName;

    @ApiModelProperty(value = "创建人id")
    private String createUserId;

    @ApiModelProperty(value = "创建人名称")
    private String createUserName;

    @ApiModelProperty(value = "创建部门id")
    private String createDeptId;

    @ApiModelProperty(value = "创建部门名称")
    private String createDeptName;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date updateTime;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "备注", example = "备注")
    private  String remark;
}
