package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("系统菜单表")
@Table(name = "system_menu")
public class SystemMenu extends BaseBean {
    @ApiModelProperty(value = "菜单id", required = true)
    @Column(name = "menu_id")
    @Id
    private String menuId;

    @ApiModelProperty(value = "父级id", required = true)
    @Column(name = "parent_id")
    private String parentId;

    @ApiModelProperty(value = "菜单编码", required = true)
    @Column(name = "menu_code")
    private String menuCode;

    @ApiModelProperty(value = "菜单名称", required = true)
    @Column(name = "menu_name")
    private String menuName;

    @ApiModelProperty(value = "菜单小图标")
    @Column(name = "menu_icon")
    private String menuIcon;

    @ApiModelProperty(value = "请求地址API需要使用的api接口多个请逗号分割")
    @Column(name = "api_url")
    private String apiUrl;

    @ApiModelProperty(value = "前端组件地址")
    @Column(name = "component")
    private String component;

    @ApiModelProperty(value = "状态：FORBID禁用 NORMAL正常取数据字典 STATUS")
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "功能类型 NAVIGATION 导航 BUTTON 功能按钮取数据字典 MENU_TYPE")
    @Column(name = "menu_type_code")
    private String menuTypeCode;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "是否是内置:YES是，NO否", required = true)
    @Column(name = "built_in")
    private String builtIn;

    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "菜单描述")
    @Column(name = "description")
    private String description;

    @ApiModelProperty(value = "是否是外链:YES是，NO否")
    @Column(name = "menu_frame_code")
    private String menuFrameCode;

    @ApiModelProperty(value = "是否缓存:YES是，NO否")
    @Column(name = "menu_cache_code")
    private String menuCacheCode;

    @ApiModelProperty(value = "是否租户菜单:YES是，NO否")
    @Column(name = "tenant_menu")
    private String tenantMenu;
}