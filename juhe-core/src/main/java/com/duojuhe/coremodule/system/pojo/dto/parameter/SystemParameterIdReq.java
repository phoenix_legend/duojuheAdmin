package com.duojuhe.coremodule.system.pojo.dto.parameter;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SystemParameterIdReq extends BaseBean {
    @ApiModelProperty(value = "参数ID", example = "1",required=true)
    @NotBlank(message = "参数ID不能为空")
    private String parameterId;
}
