package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("租户菜单关系表")
@Table(name = "system_tenant_menu")
public class SystemTenantMenu extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "id")
    @Id
    private String id;

    @ApiModelProperty(value = "菜单id", required = true)
    @Column(name = "menu_id")
    private String menuId;

    @ApiModelProperty(value = "所属租户id", required = true)
    @Column(name = "tenant_id")
    private String tenantId;

}