package com.duojuhe.coremodule.system.pojo.dto.log;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SystemLogIdReq extends BaseBean {
    @ApiModelProperty(value = "日志ID", example = "1",required=true)
    @NotBlank(message = "日志ID不能为空")
    private String logId;
}
