package com.duojuhe.coremodule.system.pojo.dto.message;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemMessagePageReq extends PageHead {
    @ApiModelProperty(value = "处理状态取数据字典,PENDING =待处理", example = "PENDING")
    private String handleStatusCode;

    @ApiModelProperty(value = "消息类型，取数据字典MESSAGE_TYPE")
    private String messageTypeCode;

    @ApiModelProperty(value = "消息标题")
    private String messageTitle;

    @ApiModelProperty(value = "消息内容")
    private String messageContent;

    @ApiModelProperty(value = "消息创建开始日期 yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "消息创建结束日期 yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;


    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}
