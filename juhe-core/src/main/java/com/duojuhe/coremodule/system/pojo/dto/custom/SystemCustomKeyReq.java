package com.duojuhe.coremodule.system.pojo.dto.custom;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


/**
 * 系统自定义布局key
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SystemCustomKeyReq extends BaseBean {
    @ApiModelProperty(value = "自定义Key", example = "1",required=true)
    @NotBlank(message = "自定义Key不能为空")
    private String customKey;
}
