package com.duojuhe.coremodule.system.service;


import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.entity.SystemMessage;
import com.duojuhe.coremodule.system.pojo.dto.message.MessageNumRes;
import com.duojuhe.coremodule.system.pojo.dto.message.QuerySystemMessagePageReq;
import com.duojuhe.coremodule.system.pojo.dto.message.QuerySystemMessagePageRes;
import com.duojuhe.coremodule.system.pojo.dto.message.SystemMessageIdReq;

import java.util.List;

/**
 * 系统消息相关接口
 */
public interface SystemMessageService{


    /**
     * 【分页查询】分页查询系统消息list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QuerySystemMessagePageRes>>> querySystemMessagePageResList(QuerySystemMessagePageReq req);

    /**
     * 【统计未处理 数量】统计未读数量
     * @param dataScope
     * @return
     */
    ServiceResult<MessageNumRes> queryCountUntreatedSystemMessage(DataScopeFilterBean dataScope);


    /**
     * 【根据id设置未处理】根据id设置未处理
     * @param req
     * @return
     */
    ServiceResult updateSystemMessageHandleStatusByMessageId(SystemMessageIdReq req);

    /**
     * 【一键未处理】一键未处理
     * @param dataScope
     * @return
     */
    ServiceResult updateAllSystemMessageHandleStatus(DataScopeFilterBean dataScope);

    /**
     * 保存发送系统消息
     */
    void saveSystemSendMessage(SystemMessage systemMessage);
}
