package com.duojuhe.coremodule.system.mapper;

import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.coremodule.system.entity.SystemMessage;
import com.duojuhe.coremodule.system.pojo.dto.message.BatchHandleSystemMessageReq;
import com.duojuhe.coremodule.system.pojo.dto.message.QuerySystemMessagePageReq;
import com.duojuhe.coremodule.system.pojo.dto.message.QuerySystemMessagePageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemMessageMapper extends TkMapper<SystemMessage> {
    /**
     * 根据条件查询消息项
     *
     * @return
     */
    List<QuerySystemMessagePageRes> querySystemMessagePageResList(@Param("req") QuerySystemMessagePageReq req);

    /**
     * 根据条件统计系统处理消息总数
     *
     * @return
     */
    int queryCountUntreatedSystemMessage(@Param("req") DataScopeFilterBean req, @Param("handlingStatus") String handlingStatus);

    /**
     * 根据用户id查询用户未处理的消息总数
     *
     * @return
     */
    int queryCountUntreatedSystemMessageByUserId(@Param("userId") String userId);


    /**
     * 根据条件设置已处理
     *
     * @return
     */
    void updateAllSystemMessageHandleStatus(@Param("req") BatchHandleSystemMessageReq req);
}