package com.duojuhe.coremodule.system.mapper;

import com.duojuhe.coremodule.system.entity.SystemTenantMenu;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemTenantMenuMapper extends TkMapper<SystemTenantMenu> {

    /**
     * 根据租户id查询菜单 id list集合
     *
     * @param tenantId
     * @return
     */
    List<String> querySystemMenuIdListByTenantId(@Param(value = "tenantId") String tenantId);

}
