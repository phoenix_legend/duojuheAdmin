package com.duojuhe.coremodule.system.pojo.dto.menu;


import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemMenuTreeReq extends BaseBean {
    @ApiModelProperty(value = "菜单编码", example = "1")
    private String menuCode;

    @ApiModelProperty(value = "菜单名称", example = "1")
    private String menuName;
}
