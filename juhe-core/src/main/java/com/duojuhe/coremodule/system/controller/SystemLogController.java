package com.duojuhe.coremodule.system.controller;


import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.log.QuerySystemLogPageReq;
import com.duojuhe.coremodule.system.pojo.dto.log.QuerySystemLogPageRes;
import com.duojuhe.coremodule.system.pojo.dto.log.QuerySystemLogRes;
import com.duojuhe.coremodule.system.pojo.dto.log.SystemLogIdReq;
import com.duojuhe.coremodule.system.service.SystemLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/systemLog/")
@Api(tags = {"【日志管理】日志管理相关接口"})
@Slf4j
public class SystemLogController {
    @Resource
    private SystemLogService systemLogService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【分页查询】分页查询日志list")
    @PostMapping(value = "querySystemLogPageResList")
    public ServiceResult<PageResult<List<QuerySystemLogPageRes>>> querySystemLogPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySystemLogPageReq req) {
        return systemLogService.querySystemLogPageResList(req);
    }

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【根据ID查询详情】根据日志ID查询日志详情")
    @PostMapping(value = "querySystemLogResByLogId")
    public ServiceResult<QuerySystemLogRes> querySystemLogResByLogId(@Valid @RequestBody @ApiParam(value = "入参类") SystemLogIdReq req) {
        return systemLogService.querySystemLogResByLogId(req);
    }
}
