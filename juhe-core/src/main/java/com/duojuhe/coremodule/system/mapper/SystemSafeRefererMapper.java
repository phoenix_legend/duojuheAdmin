package com.duojuhe.coremodule.system.mapper;

import com.duojuhe.coremodule.system.entity.SystemSafeReferer;
import com.duojuhe.coremodule.system.pojo.dto.referer.QuerySystemSafeRefererPageReq;
import com.duojuhe.coremodule.system.pojo.dto.referer.QuerySystemSafeRefererPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemSafeRefererMapper extends TkMapper<SystemSafeReferer> {

    /**
     * 分页查询系统安全白名单list
     *
     * @return
     */
    List<QuerySystemSafeRefererPageRes> querySystemSafeRefererPageResList(@Param("req") QuerySystemSafeRefererPageReq req);
}