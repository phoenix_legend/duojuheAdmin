package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("系统配置表")
@Table(name = "system_config")
public class SystemConfig extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "config_id")
    @Id
    private String configId;

    @ApiModelProperty(value = "系统错误钉钉通知状态，FORBID禁用 NORMAL正常")
    @Column(name = "ding_enable")
    private String dingEnable;

    @ApiModelProperty(value = "系统错误调用钉钉接口的token")
    @Column(name = "ding_access_token")
    private String dingAccessToken;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "最后更新用户")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "创建用户")
    @Column(name = "create_user_id")
    private String createUserId;
}
