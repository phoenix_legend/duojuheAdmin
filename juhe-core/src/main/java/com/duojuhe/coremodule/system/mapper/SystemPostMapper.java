package com.duojuhe.coremodule.system.mapper;


import com.duojuhe.coremodule.system.entity.SystemPost;
import com.duojuhe.coremodule.system.pojo.dto.post.*;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemPostMapper extends TkMapper<SystemPost> {
    /**
     * 分页查询 根据条件查询岗位list
     *
     * @return
     */
    List<QuerySystemPostPageRes> querySystemPostPageResList(@Param("req") QuerySystemPostPageReq req);


    /**
     * 根据系统岗位ID查询岗位详情信息
     *
     * @return
     */
    QuerySystemPostRes querySystemPostResByPostId(@Param("postId") String postId);

    /**
     * 根据条件查询所有可供前端选择使用的岗位
     *
     * @return
     */
    List<SelectSystemPostRes> querySelectSystemPostResListByStatusCode(@Param("req") SelectSystemPostReq req, @Param("statusCode") String statusCode);
}