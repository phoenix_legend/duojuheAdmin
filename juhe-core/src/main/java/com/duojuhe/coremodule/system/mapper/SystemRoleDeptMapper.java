package com.duojuhe.coremodule.system.mapper;


import com.duojuhe.coremodule.system.entity.SystemRoleDept;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SystemRoleDeptMapper extends TkMapper<SystemRoleDept> {
    /**
     * 根据角色id查询部门 id list集合
     *
     * @param roleId
     * @return
     */
    List<String> querySystemDeptIdListByRoleId(@Param(value = "roleId") String roleId);
}