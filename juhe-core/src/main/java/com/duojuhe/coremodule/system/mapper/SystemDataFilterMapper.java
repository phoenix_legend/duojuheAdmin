package com.duojuhe.coremodule.system.mapper;


import com.duojuhe.coremodule.system.entity.SystemDataFilter;
import com.tkmapper.TkMapper;

public interface SystemDataFilterMapper extends TkMapper<SystemDataFilter> {

}