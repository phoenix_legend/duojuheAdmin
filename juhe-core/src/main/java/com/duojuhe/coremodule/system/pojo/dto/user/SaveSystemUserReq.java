package com.duojuhe.coremodule.system.pojo.dto.user;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


@Getter
@Setter
public class SaveSystemUserReq extends BaseBean {
    @ApiModelProperty(value = "登录帐号，只能为小写字母和数字，长度2-15", example = "1",required=true)
    @NotBlank(message = "登录帐号不能为空")
    @Pattern(regexp = RegexpConstants.LOGIN_NAME, message = "登录帐号只能为小写字母和数字，长度6-15")
    @Length(max = 15, message = "登录帐号不得超过{max}位字符")
    private String loginName;

    @ApiModelProperty(value = "登录密码", example = "1",required=true)
    @NotBlank(message = "登录密码不能为空")
    @Length(max = 16, message = "登录密码不得超过{max}位字符")
    private String password;

    @ApiModelProperty(value = "姓名", example = "1",required = true)
    @Length(max = 32, message = "姓名不得超过{max}位字符")
    @NotBlank(message = "姓名不能为空")
    private String realName;

    @ApiModelProperty(value = "手机号码", example = "1",required = true)
    @Pattern(regexp = RegexpConstants.MOBILE, message = "手机号码不合法")
    @NotBlank(message = "手机号码不能为空")
    private String mobileNumber;

    @ApiModelProperty(value = "帐号状态，状态FORBID禁用 NORMAL正常", example = "NORMAL",required = true)
    @Pattern(regexp = RegexpConstants.STATUS, message = "帐号状态参数错误!")
    @NotBlank(message = "帐号状态不能为空")
    private String statusCode;

    @ApiModelProperty(value = "角色id", example = "1",required = true)
    @NotBlank(message = "角色不能为空")
    private String roleId;

    @ApiModelProperty(value = "创建部门id",required=true)
    @NotBlank(message = "创建部门不能为空")
    @Length(max = 32, message = "创建部门不得超过{max}位字符")
    private String createDeptId;

    @ApiModelProperty(value = "岗位ID", example = "1",required=true)
    @NotBlank(message = "岗位ID不能为空")
    private String postId;


    @ApiModelProperty(value = "性别",required = true)
    @Length(max = 50, message = "性别不得超过{max}位字符")
    @NotBlank(message = "性别不能为空")
    private String genderCode;
}
