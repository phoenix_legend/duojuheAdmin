package com.duojuhe.coremodule.system.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("部门表")
@Table(name = "system_dept")
public class SystemDept extends BaseBean {
    @ApiModelProperty(value = "部门id", required = true)
    @Column(name = "dept_id")
    @Id
    private String deptId;

    @ApiModelProperty(value = "上级部门id，-1表示顶级部门", required = true)
    @Column(name = "parent_id")
    private String parentId;

    @ApiModelProperty(value = "部门层级path", required = true)
    @Column(name = "dept_path")
    private String deptPath;

    @ApiModelProperty(value = "部门名称", required = true)
    @Column(name = "dept_name")
    private String deptName;

    @ApiModelProperty(value = "部门编码")
    @Column(name = "dept_code")
    private String deptCode;

    @ApiModelProperty(value = "部门简称别名")
    @Column(name = "dept_alias")
    private String deptAlias;

    @ApiModelProperty(value = "部门电话")
    @Column(name = "dept_telephone")
    private String deptTelephone;

    @ApiModelProperty(value = "部门负责人")
    @Column(name = "dept_leader")
    private String deptLeader;

    @ApiModelProperty(value = "部门传真")
    @Column(name = "dept_fax")
    private String deptFax;

    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "创建部门ID")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "是否是内置:YES是，NO否", required = true)
    @Column(name = "built_in")
    private String builtIn;
}