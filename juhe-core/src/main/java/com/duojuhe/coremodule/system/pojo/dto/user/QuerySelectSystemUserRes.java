package com.duojuhe.coremodule.system.pojo.dto.user;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySelectSystemUserRes extends HandleSystemUserDictNameColorRes {
    @ApiModelProperty(value = "主键", example = "1")
    private String userId;

    @ApiModelProperty(value = "姓名", example = "1")
    private String realName;

    @ApiModelProperty(value = "登录名", example = "1")
    private String loginName;

    @ApiModelProperty(value = "手机号码", example = "1")
    private String mobileNumber;

    @ApiModelProperty(value = "头像", example = "1")
    private String headPortrait;

    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    @ApiModelProperty(value = "创建时间", example = "1")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date updateTime;
}
