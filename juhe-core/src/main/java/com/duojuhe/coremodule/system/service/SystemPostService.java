package com.duojuhe.coremodule.system.service;

import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.post.*;

import java.util.List;

/**
 * 系统工作岗位相关接口
 */
public interface SystemPostService {
    /**
     * 【分页查询】分页查询岗位list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QuerySystemPostPageRes>>> querySystemPostPageResList(QuerySystemPostPageReq req);


    /**
     * 【下拉选择使用-查询所有岗位】查询可供前端选择使用的岗位，一般用于用户添加下拉选择使用
     * @param req
     * @return
     */
    ServiceResult<List<SelectSystemPostRes>> queryNormalSelectSystemPostResList(SelectSystemPostReq req);

    /**
     * 【根据ID查询详情】根据岗位ID查询岗位详情
     * @param req
     * @return
     */
    ServiceResult<QuerySystemPostRes> querySystemPostResByPostId(SystemPostIdReq req);


    /**
     * 【保存】岗位信息
     * @param req
     * @return
     */
    ServiceResult saveSystemPost(SaveSystemPostReq req);


    /**
     * 【修改】岗位信息
     * @param req
     * @return
     */
    ServiceResult updateSystemPost(UpdateSystemPostReq req);


    /**
     * 【删除】根据岗位ID删除岗位，注意一次仅能删除一个岗位，存在绑定关系的则不能删除
     * @param req
     * @return
     */
    ServiceResult deleteSystemPostByPostId(SystemPostIdReq req);
}
