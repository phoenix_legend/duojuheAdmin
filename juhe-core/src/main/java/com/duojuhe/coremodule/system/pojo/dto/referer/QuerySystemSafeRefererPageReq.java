package com.duojuhe.coremodule.system.pojo.dto.referer;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySystemSafeRefererPageReq extends PageHead {
    @ApiModelProperty(value = "允许请求的域名或者ip")
    private String allowReferer;

    @ApiModelProperty(value = "开始日期 yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期 yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    @ApiModelProperty(value = "是否是内置:YES是，NO否")
    private String builtIn;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}
