package com.duojuhe.coremodule.help.mapper;

import com.duojuhe.coremodule.help.entity.HelpInfo;
import com.duojuhe.coremodule.help.pojo.dto.info.*;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HelpInfoMapper extends TkMapper<HelpInfo> {

    /**
     * 【帮助详情】根据帮助发布ID获取帮助详情
     * @param infoId
     * @return
     */
    QueryHelpInfoRes queryHelpInfoResByInfoId(@Param("infoId") String infoId);

    /**
     * 分页查询 根据条件查询发布帮助list
     *
     * @return
     */
    List<QueryHelpInfoPageRes> queryHelpInfoPageResList(@Param("req") QueryHelpInfoPageReq req, @Param("isDelete") Integer isDelete);


    /**
     * 分页查询 根据条件查询我的发布帮助list
     *
     * @return
     */
    List<QueryMyHelpInfoPageRes> queryMyHelpInfoPageResList(@Param("req") QueryMyHelpInfoPageReq req, @Param("isDelete") Integer isDelete);


}