package com.duojuhe.coremodule.help.pojo.dto.category;

import com.duojuhe.common.bean.DataScopeFilterBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryHelpCategoryTreeReq extends DataScopeFilterBean {
    @ApiModelProperty(value = "分类名称")
    private String categoryName;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}