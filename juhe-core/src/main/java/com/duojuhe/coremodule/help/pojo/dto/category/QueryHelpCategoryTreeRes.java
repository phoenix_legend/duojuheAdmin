package com.duojuhe.coremodule.help.pojo.dto.category;

import com.duojuhe.common.utils.tree.TreeBaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryHelpCategoryTreeRes extends TreeBaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String categoryId;

    @ApiModelProperty(value = "分类名称", required = true)
    private String categoryName;

    @ApiModelProperty(value = "分类路径")
    private String categoryPath;

    @ApiModelProperty(value = "排序")
    private Integer sort;
}