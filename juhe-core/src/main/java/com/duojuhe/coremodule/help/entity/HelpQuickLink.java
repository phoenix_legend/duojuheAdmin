package com.duojuhe.coremodule.help.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("帮助中心快捷链接")
@Table(name = "help_quick_link")
public class HelpQuickLink extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "link_id")
    @Id
    private String linkId;

    @ApiModelProperty(value = "链接名称", required = true)
    @Column(name = "link_name")
    private String linkName;

    @ApiModelProperty(value = "链接图标")
    @Column(name = "link_image")
    private String linkImage;

    @ApiModelProperty(value = "备注说明")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "链接地址")
    @Column(name = "link_url")
    private String linkUrl;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常")
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "打开方式，取数据字典")
    @Column(name = "open_type_code")
    private String openTypeCode;

    @ApiModelProperty(value = "链接位置，导航，首页中部")
    @Column(name = "link_position_code")
    private String linkPositionCode;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "创建租户id")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;
}