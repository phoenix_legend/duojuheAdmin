package com.duojuhe.coremodule.help.pojo.dto.quicklink;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveHelpQuickLinkReq extends BaseBean {

    @ApiModelProperty(value = "链接名称", required = true)
    @NotBlank(message = "链接名称不能为空")
    @Pattern(regexp = "^[\\u4E00-\\u9FA5a-zA-Z0-9_]*$", message = "链接名称只能填写中文英文数字和下划线")
    @Length(max = 50, message = "链接名称不得超过{max}位字符")
    private String linkName;

    @ApiModelProperty(value = "链接图标", required = true)
    @NotBlank(message = "链接图标不能为空")
    private String linkImage;

    @ApiModelProperty(value = "链接地址", required = true)
    @Length(max = 255, message = "链接地址不能超过{max}位字符")
    @NotBlank(message = "链接地址不能为空")
    private String linkUrl;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常", required = true)
    @Pattern(regexp = RegexpConstants.STATUS, message = "状态参数错误!")
    @NotBlank(message = "状态不能为空")
    private String statusCode;

    @ApiModelProperty(value = "打开方式，取数据字典", required = true)
    @Length(max = 50, message = "打开方式，取数据字典不能超过{max}位字符")
    @NotBlank(message = "打开方式，取数据字典不能为空")
    private String openTypeCode;

    @ApiModelProperty(value = "链接位置，导航，首页中部", required = true)
    @Length(max = 50, message = "链接位置，导航，首页中部不能超过{max}位字符")
    @NotBlank(message = "链接位置，导航，首页中部不能为空")
    private String linkPositionCode;

    @ApiModelProperty(value = "排序", required = true)
    @NotNull(message = "排序不能为空")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;

    @ApiModelProperty(value = "备注")
    @Pattern(regexp = RegexpConstants.REMARK, message = "备注禁止输入特殊字符!")
    @Length(max = 100, message = "备注不得超过{max}位字符")
    private String remark;

}