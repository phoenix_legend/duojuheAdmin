package com.duojuhe.coremodule.help.pojo.dto.quicklink;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryHelpQuickLinkPageReq extends PageHead {
    @ApiModelProperty(value = "链接名称")
    private String linkName;

    @ApiModelProperty(value = "备注说明")
    private String remark;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常")
    private String statusCode;

    @ApiModelProperty(value = "打开方式，取数据字典")
    private String openTypeCode;

    @ApiModelProperty(value = "链接位置，导航，首页中部")
    private String linkPositionCode;

    @ApiModelProperty(value = "开始日期yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}