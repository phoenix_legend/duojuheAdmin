package com.duojuhe.coremodule.help.pojo.dto.category;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BuildParentHelpCategoryReq extends BaseBean {
    @ApiModelProperty(value = "上级部门id，-1表示一级分类", required = true)
    private String parentId;

    @ApiModelProperty(value = "分类层级path", required = true)
    private String categoryPath;

    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    @ApiModelProperty(value = "创建分类所属部门")
    String createDeptId;
}
