package com.duojuhe.coremodule.help.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;

import com.duojuhe.coremodule.help.pojo.dto.quicklink.*;

import java.util.List;


public interface HelpQuickLinkService {


    /**
     * 分页查询 根据条件查询帮助快捷链接list
     */
    ServiceResult<PageResult<List<QueryHelpQuickLinkPageRes>>> queryHelpQuickLinkPageResList(QueryHelpQuickLinkPageReq req);

    /**
     * 【保存】帮助快捷链接
     * @param req
     * @return
     */
    ServiceResult saveHelpQuickLink(SaveHelpQuickLinkReq req);

    /**
     * 【修改】帮助快捷链接
     * @param req
     * @return
     */
    ServiceResult updateHelpQuickLink(UpdateHelpQuickLinkReq req);

    /**
     * 【彻底删除】根据帮助发布ID彻底删除快捷链接，注意一次仅能彻底删除一个快捷链接
     * @param req
     * @return
     */
    ServiceResult deleteHelpQuickLinkByLinkId(HelpQuickLinkIdReq req);
}
