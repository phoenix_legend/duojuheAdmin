package com.duojuhe.coremodule.help.pojo.dto.category;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveHelpCategoryReq extends BaseBean {
    @ApiModelProperty(value = "父级ID，留空表示是一级分类，长度0-32", example = "1")
    @Length(max = 32, message = "不得超过{max}位字符")
    private String parentId;

    @ApiModelProperty(value = "分类名称", required = true)
    @NotBlank(message = "分类名称不能为空")
    @Length(max = 50, message = "分类名称不得超过{max}位字符")
    private String categoryName;

    @ApiModelProperty(value = "排序，只能为纯数字", example = "1",required=true)
    @NotNull(message = "排序不能为空")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;
}