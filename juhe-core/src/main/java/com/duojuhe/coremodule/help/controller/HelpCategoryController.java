package com.duojuhe.coremodule.help.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.help.pojo.dto.category.*;
import com.duojuhe.coremodule.help.service.HelpCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/helpCategory/")
@Api(tags = {"【帮助分类管理】帮助分类管理相关接口"})
@Slf4j
public class HelpCategoryController {
    @Resource
    private HelpCategoryService helpCategoryService;

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【查询帮助分类】分类管理界面使用")
    @PostMapping(value = "queryHelpCategoryTreeResList")
    public ServiceResult<List<QueryHelpCategoryTreeRes>> queryHelpCategoryTreeResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryHelpCategoryTreeReq req) {
        return helpCategoryService.queryHelpCategoryTreeResList(req);
    }



    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【查询所有帮助分类】查询可供前端选择使用的帮助分类")
    @PostMapping(value = "querySelectHelpCategoryTreeResList")
    public ServiceResult<List<QueryHelpCategoryTreeRes>> querySelectHelpCategoryTreeResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryHelpCategoryTreeReq req) {
        return helpCategoryService.querySelectHelpCategoryTreeResList(req);
    }

    @ApiOperation(value = "【保存】帮助分类")
    @PostMapping(value = "saveHelpCategory")
    @OperationLog(moduleName = LogEnum.ModuleName.HELP_CATEGORY, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveHelpCategory(@Valid @RequestBody @ApiParam(value = "入参类") SaveHelpCategoryReq req) {
        return helpCategoryService.saveHelpCategory(req);
    }


    @ApiOperation(value = "【修改】帮助分类")
    @PostMapping(value = "updateHelpCategory")
    @OperationLog(moduleName = LogEnum.ModuleName.HELP_CATEGORY, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateHelpCategory(@Valid @RequestBody @ApiParam(value = "入参类") UpdateHelpCategoryReq req) {
        return helpCategoryService.updateHelpCategory(req);
    }

    @ApiOperation(value = "【删除】根据帮助分类ID删除帮助分类，注意一次仅能删除一个帮助分类，存在绑定关系的则不能删除")
    @PostMapping(value = "deleteHelpCategoryByCategoryId")
    @OperationLog(moduleName = LogEnum.ModuleName.HELP_CATEGORY, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteHelpCategoryByCategoryId(@Valid @RequestBody @ApiParam(value = "入参类") HelpCategoryIdReq req) {
        return helpCategoryService.deleteHelpCategoryByCategoryId(req);
    }
}
