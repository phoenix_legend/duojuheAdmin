package com.duojuhe.coremodule.help.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.help.pojo.dto.info.*;

import java.util.List;

public interface HelpInfoService {

    /**
     * 【帮助详情】根据帮助发布ID获取帮助详情
     * @param req
     * @return
     */
    ServiceResult<QueryHelpInfoRes> queryHelpInfoResByInfoId(HelpInfoIdReq req);

    /**
     * 【分页查询帮助发布list】分页查询帮助发布list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryHelpInfoPageRes>>> queryHelpInfoPageResList(QueryHelpInfoPageReq req);


    /**
     * 【分页查询我发布的list】分页查询我发布的list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryMyHelpInfoPageRes>>> queryMyHelpInfoPageResList(QueryMyHelpInfoPageReq req);


    /**
     * 【分页查询我的回收站帮助list】分页查询我的回收站帮助list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryMyHelpInfoPageRes>>> queryRecycleHelpInfoPageResList(QueryMyHelpInfoPageReq req);

    /**
     * 【保存】帮助发布帮助
     * @param req
     * @return
     */
    ServiceResult saveHelpInfo(SaveHelpInfoReq req);

    /**
     * 【修改】帮助发布帮助
     * @param req
     * @return
     */
    ServiceResult updateHelpInfo(UpdateHelpInfoReq req);

    /**
     * 【彻底删除】根据帮助发布ID彻底删除帮助发布，注意一次仅能彻底删除一个帮助发布
     * @param req
     * @return
     */
    ServiceResult deleteHelpInfoByInfoId(HelpInfoIdReq req);


    /**
     * 【回收站中】根据帮助发布ID把发布帮助放到回收站中
     * @param req
     * @return
     */
    ServiceResult putRecycleHelpInfoByInfoId(HelpInfoIdReq req);

    /**
     * 【把回收站中的帮助恢复】根据帮助发布ID把回收站中的帮助恢复
     * @param req
     * @return
     */
    ServiceResult recoveryRecycleHelpInfoByInfoId(HelpInfoIdReq req);



}
