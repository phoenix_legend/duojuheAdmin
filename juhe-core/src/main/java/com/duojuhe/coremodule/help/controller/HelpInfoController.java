package com.duojuhe.coremodule.help.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.help.pojo.dto.info.*;
import com.duojuhe.coremodule.help.service.HelpInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/helpInfo/")
@Api(tags = {"【帮助信息发布管理】帮助信息发布管理相关接口"})
@Slf4j
public class HelpInfoController {
    @Resource
    private HelpInfoService helpInfoService;

    @ApiOperation(value = "【帮助信息详情】根据帮助信息发布ID获取帮助信息详情")
    @PostMapping(value = "queryHelpInfoResByInfoId")
    public ServiceResult<QueryHelpInfoRes> queryHelpInfoResByInfoId(@Valid @RequestBody @ApiParam(value = "入参类") HelpInfoIdReq req) {
        return helpInfoService.queryHelpInfoResByInfoId(req);
    }


    @ApiOperation(value = "【分页查询】分页查询帮助信息发布list")
    @PostMapping(value = "queryHelpInfoPageResList")
    public ServiceResult<PageResult<List<QueryHelpInfoPageRes>>> queryHelpInfoPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryHelpInfoPageReq req) {
        return helpInfoService.queryHelpInfoPageResList(req);
    }



    @ApiOperation(value = "【分页查询我发布的】分页查询我发布的list")
    @PostMapping(value = "queryMyHelpInfoPageResList")
    public ServiceResult<PageResult<List<QueryMyHelpInfoPageRes>>> queryMyHelpInfoPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryMyHelpInfoPageReq req) {
        return helpInfoService.queryMyHelpInfoPageResList(req);
    }

    @ApiOperation(value = "【分页查询我的回收站帮助信息list】分页查询我的回收站帮助信息list")
    @PostMapping(value = "queryRecycleHelpInfoPageResList")
    public ServiceResult<PageResult<List<QueryMyHelpInfoPageRes>>> queryRecycleHelpInfoPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryMyHelpInfoPageReq req) {
        return helpInfoService.queryRecycleHelpInfoPageResList(req);
    }


    @ApiOperation(value = "【保存】帮助信息发布")
    @PostMapping(value = "saveHelpInfo")
    @OperationLog(moduleName = LogEnum.ModuleName.HELP_INFO, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveHelpInfo(@Valid @RequestBody @ApiParam(value = "入参类") SaveHelpInfoReq req) {
        return helpInfoService.saveHelpInfo(req);
    }

    @ApiOperation(value = "【修改】帮助信息发布")
    @PostMapping(value = "updateHelpInfo")
    @OperationLog(moduleName = LogEnum.ModuleName.HELP_INFO, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateHelpInfo(@Valid @RequestBody @ApiParam(value = "入参类") UpdateHelpInfoReq req) {
        return helpInfoService.updateHelpInfo(req);
    }

    @ApiOperation(value = "【删除】根据ID删除帮助信息发布，注意一次仅能删除一个帮助信息发布")
    @PostMapping(value = "deleteHelpInfoByInfoId")
    @OperationLog(moduleName = LogEnum.ModuleName.HELP_INFO, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteHelpInfoByInfoId(@Valid @RequestBody @ApiParam(value = "入参类") HelpInfoIdReq req) {
        return helpInfoService.deleteHelpInfoByInfoId(req);
    }

    @ApiOperation(value = "【回收站中】根据帮助信息发布ID把发布帮助信息放到回收站中")
    @PostMapping(value = "putRecycleHelpInfoByInfoId")
    @OperationLog(moduleName = LogEnum.ModuleName.HELP_INFO, operationType = LogEnum.OperationType.RECYCLE)
    public ServiceResult putRecycleHelpInfoByInfoId(@Valid @RequestBody @ApiParam(value = "入参类") HelpInfoIdReq req) {
        return helpInfoService.putRecycleHelpInfoByInfoId(req);
    }

    @ApiOperation(value = "【回收站中的帮助信息恢复】根据帮助信息发布ID把回收站中的帮助信息恢复")
    @PostMapping(value = "recoveryRecycleHelpInfoByInfoId")
    @OperationLog(moduleName = LogEnum.ModuleName.HELP_INFO, operationType = LogEnum.OperationType.RECYCLE_RECOVERY)
    public ServiceResult recoveryRecycleHelpInfoByInfoId(@Valid @RequestBody @ApiParam(value = "入参类") HelpInfoIdReq req) {
        return helpInfoService.recoveryRecycleHelpInfoByInfoId(req);
    }

}
