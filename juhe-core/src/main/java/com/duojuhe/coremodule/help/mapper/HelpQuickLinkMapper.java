package com.duojuhe.coremodule.help.mapper;


import com.duojuhe.coremodule.help.entity.HelpQuickLink;
import com.duojuhe.coremodule.help.pojo.dto.quicklink.QueryHelpQuickLinkPageReq;
import com.duojuhe.coremodule.help.pojo.dto.quicklink.QueryHelpQuickLinkPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HelpQuickLinkMapper extends TkMapper<HelpQuickLink> {
    /**
     * 分页查询 根据条件查询发布帮助list
     *
     * @return
     */
    List<QueryHelpQuickLinkPageRes> queryHelpQuickLinkPageResList(@Param("req") QueryHelpQuickLinkPageReq req);


}