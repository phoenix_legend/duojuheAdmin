package com.duojuhe.coremodule.help.service.impl;

import com.duojuhe.cache.SystemDictCache;
import com.duojuhe.common.annotation.DataScopeFilter;
import com.duojuhe.common.bean.UserTokenInfoVo;
import com.duojuhe.common.constant.DataScopeConstants;
import com.duojuhe.common.result.ErrorCodes;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.idgenerator.UUIDUtils;
import com.duojuhe.common.utils.page.PageHelperUtil;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.help.entity.HelpQuickLink;
import com.duojuhe.coremodule.help.mapper.HelpQuickLinkMapper;
import com.duojuhe.coremodule.help.pojo.dto.quicklink.*;
import com.duojuhe.coremodule.help.service.HelpQuickLinkService;
import com.duojuhe.coremodule.system.entity.SystemDict;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class HelpQuickLinkServiceImpl extends BaseService implements HelpQuickLinkService {
    @Resource
    private SystemDictCache systemDictCache;
    @Resource
    private HelpQuickLinkMapper helpQuickLinkMapper;
    /**
     * 分页查询 根据条件查询帮助快捷链接list
     */
    @DataScopeFilter(dataScope = DataScopeConstants.TENANT_ALL_DATA)
    @Override
    public ServiceResult<PageResult<List<QueryHelpQuickLinkPageRes>>> queryHelpQuickLinkPageResList(QueryHelpQuickLinkPageReq req) {
        PageHelperUtil.orderByAndStartPage(req, "sort desc,updateTime desc,createTime desc, linkId desc");
        List<QueryHelpQuickLinkPageRes> list = helpQuickLinkMapper.queryHelpQuickLinkPageResList(req);
        for (QueryHelpQuickLinkPageRes res:list){
            SystemDict dictStatus = systemDictCache.getSystemDictByDictCode(res.getStatusCode());
            res.setStatusName(dictStatus.getDictName());
            res.setStatusColor(dictStatus.getDictColor());

            SystemDict openType = systemDictCache.getSystemDictByDictCode(res.getOpenTypeCode());
            res.setOpenTypeName(openType.getDictName());
            res.setOpenTypeColor(openType.getDictColor());

            SystemDict linkPosition = systemDictCache.getSystemDictByDictCode(res.getLinkPositionCode());
            res.setLinkPositionName(linkPosition.getDictName());
            res.setLinkPositionColor(linkPosition.getDictColor());
        }
        return PageHelperUtil.returnServiceResult(req, list);
    }


    /**
     * 【保存】帮助快捷链接
     * @param req
     * @return
     */
    @Override
    public ServiceResult saveHelpQuickLink(SaveHelpQuickLinkReq req) {
        //链接id
        String linkId = UUIDUtils.getUUID32();
        //当前登录用户
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //租户id
        String tenantId = userTokenInfoVo.getTenantId();
        //创建分类所属部门
        String createDeptId = userTokenInfoVo.getCreateDeptId();
        //当前时间
        Date date = new Date();
        HelpQuickLink helpQuickLink = new HelpQuickLink();
        helpQuickLink.setLinkId(linkId);
        helpQuickLink.setLinkName(req.getLinkName());
        helpQuickLink.setLinkImage(req.getLinkImage());
        helpQuickLink.setRemark(req.getRemark());
        helpQuickLink.setLinkUrl(req.getLinkUrl());
        helpQuickLink.setLinkPositionCode(req.getLinkPositionCode());
        helpQuickLink.setStatusCode(req.getStatusCode());
        helpQuickLink.setOpenTypeCode(req.getOpenTypeCode());
        helpQuickLink.setCreateTime(date);
        helpQuickLink.setUpdateTime(date);
        helpQuickLink.setSort(req.getSort());
        helpQuickLink.setCreateUserId(userTokenInfoVo.getUserId());
        helpQuickLink.setUpdateUserId(userTokenInfoVo.getUserId());
        helpQuickLink.setCreateDeptId(createDeptId);
        helpQuickLink.setTenantId(tenantId);
        helpQuickLinkMapper.insertSelective(helpQuickLink);
        return ServiceResult.ok(linkId);
    }

    /**
     * 【修改】帮助快捷链接
     * @param req
     * @return
     */
    @Override
    public ServiceResult updateHelpQuickLink(UpdateHelpQuickLinkReq req) {
        HelpQuickLink helpQuickLinkOld = helpQuickLinkMapper.selectByPrimaryKey(req.getLinkId());
        if (helpQuickLinkOld==null){
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        //链接id
        String linkId = helpQuickLinkOld.getLinkId();
        //当前登录用户
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //当前时间
        Date date = new Date();
        HelpQuickLink helpQuickLink = new HelpQuickLink();
        helpQuickLink.setLinkId(linkId);
        helpQuickLink.setLinkName(req.getLinkName());
        helpQuickLink.setLinkImage(req.getLinkImage());
        helpQuickLink.setRemark(req.getRemark());
        helpQuickLink.setLinkUrl(req.getLinkUrl());
        helpQuickLink.setLinkPositionCode(req.getLinkPositionCode());
        helpQuickLink.setStatusCode(req.getStatusCode());
        helpQuickLink.setOpenTypeCode(req.getOpenTypeCode());
        helpQuickLink.setUpdateTime(date);
        helpQuickLink.setSort(req.getSort());
        helpQuickLink.setUpdateUserId(userTokenInfoVo.getUserId());
        helpQuickLinkMapper.updateByPrimaryKeySelective(helpQuickLink);
        return ServiceResult.ok(linkId);
    }

    /**
     * 【彻底删除】根据帮助发布ID彻底删除快捷链接，注意一次仅能彻底删除一个快捷链接
     * @param req
     * @return
     */
    @Override
    public ServiceResult deleteHelpQuickLinkByLinkId(HelpQuickLinkIdReq req) {
        HelpQuickLink helpQuickLink = helpQuickLinkMapper.selectByPrimaryKey(req.getLinkId());
        if (helpQuickLink==null){
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        //删除帮助
        helpQuickLinkMapper.deleteByPrimaryKey(helpQuickLink.getLinkId());
        return ServiceResult.ok(helpQuickLink.getLinkId());
    }
}
