package com.duojuhe.coremodule.help.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("帮助发布记录表")
@Table(name = "help_info")
public class HelpInfo extends BaseBean {
    @ApiModelProperty(value = "主键id", required = true)
    @Column(name = "info_id")
    @Id
    private String infoId;

    @ApiModelProperty(value = "帮助所属分类id", required = true)
    @Column(name = "category_id")
    private String categoryId;

    @ApiModelProperty(value = "帮助标题", required = true)
    @Column(name = "info_title")
    private String infoTitle;

    @ApiModelProperty(value = "帮助简介")
    @Column(name = "introduction")
    private String introduction;

    @ApiModelProperty(value = "删除时间")
    @Column(name = "delete_time")
    private Date deleteTime;

    @ApiModelProperty(value = "是否删除[0:否;1:是;]")
    @Column(name = "is_delete")
    private Integer isDelete;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "创建租户id")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "帮助内容")
    @Column(name = "info_content")
    private String infoContent;

    @ApiModelProperty(value = "数据允许查询权限，ALL_AUTH 所有用户,其他字符时表示指定用户可见")
    @Column(name = "data_allow_auth")
    private String dataAllowAuth;

}