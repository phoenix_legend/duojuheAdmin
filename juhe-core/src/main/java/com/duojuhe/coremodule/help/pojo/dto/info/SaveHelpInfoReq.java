package com.duojuhe.coremodule.help.pojo.dto.info;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveHelpInfoReq extends BaseBean {

    @ApiModelProperty(value = "帮助所属分类id", required = true)
    @NotBlank(message = "所属分类ID不能为空")
    private String categoryId;

    @ApiModelProperty(value = "帮助标题", required = true)
    @NotBlank(message = "帮助标题不能为空")
    @Length(max = 250, message = "帮助标题不得超过{max}位字符")
    private String infoTitle;

    @ApiModelProperty(value = "帮助简介",required=true)
    @Length(max = 250, message = "帮助简介不得超过{max}位字符")
    @NotBlank(message = "帮助简介不能为空")
    private String introduction;

    @ApiModelProperty(value = "排序，只能为纯数字", example = "1",required=true)
    @NotNull(message = "排序不能为空")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;

    @ApiModelProperty(value = "帮助内容",required=true)
    @NotBlank(message = "帮助内容不能为空")
    private String infoContent;
}