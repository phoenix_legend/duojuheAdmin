package com.duojuhe.coremodule.help.pojo.dto.category;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateHelpCategoryReq extends SaveHelpCategoryReq {
    @ApiModelProperty(value = "帮助分类ID", example = "1",required=true)
    @NotBlank(message = "帮助分类ID不能为空")
    private String categoryId;
}