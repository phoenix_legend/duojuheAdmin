package com.duojuhe.coremodule.help.pojo.dto.info;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateHelpInfoReq extends SaveHelpInfoReq {
    @ApiModelProperty(value = "帮助ID", example = "1",required=true)
    @NotBlank(message = "帮助ID不能为空")
    private String infoId;
}