package com.duojuhe.coremodule.help.pojo.dto.quicklink;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HelpQuickLinkIdReq extends BaseBean {

    @ApiModelProperty(value = "主键", example = "1", required = true)
    @NotBlank(message = "主键id 不能为空")
    private String linkId;

}