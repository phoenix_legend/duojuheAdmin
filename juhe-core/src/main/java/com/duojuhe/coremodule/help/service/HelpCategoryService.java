package com.duojuhe.coremodule.help.service;

import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.help.pojo.dto.category.*;

import java.util.List;

public interface HelpCategoryService {
    /**
     * 【查询所有帮助分类】查询可供前端选择使用的帮助分类
     * @param req
     * @return
     */
    ServiceResult<List<QueryHelpCategoryTreeRes>> queryHelpCategoryTreeResList(QueryHelpCategoryTreeReq req);


    /**
     * 【查询所有帮助分类】查询可供前端选择使用的帮助分类
     * @param req
     * @return
     */
    ServiceResult<List<QueryHelpCategoryTreeRes>> querySelectHelpCategoryTreeResList(QueryHelpCategoryTreeReq req);


    /**
     * 【保存】帮助分类帮助
     * @param req
     * @return
     */
    ServiceResult saveHelpCategory(SaveHelpCategoryReq req);

    /**
     * 【修改】帮助分类帮助
     * @param req
     * @return
     */
    ServiceResult updateHelpCategory(UpdateHelpCategoryReq req);

    /**
     * 【删除】根据帮助分类ID删除帮助分类，注意一次仅能删除一个帮助分类，存在绑定关系的则不能删除
     * @param req
     * @return
     */
    ServiceResult deleteHelpCategoryByCategoryId(HelpCategoryIdReq req);
}
