package com.duojuhe.coremodule.help.mapper;

import com.duojuhe.coremodule.help.entity.HelpCategory;
import com.duojuhe.coremodule.help.pojo.dto.category.QueryHelpCategoryTreeReq;
import com.duojuhe.coremodule.help.pojo.dto.category.QueryHelpCategoryTreeRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface HelpCategoryMapper extends TkMapper<HelpCategory> {
    /**
     * 【下拉选择使用-tree型结构查询】查询可供前端选择使用的帮助分类
     * @param req
     * @return
     */
    List<QueryHelpCategoryTreeRes> queryHelpCategoryTreeResList(@Param("req") QueryHelpCategoryTreeReq req);

}