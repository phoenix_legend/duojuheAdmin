package com.duojuhe.coremodule.help.pojo.dto.quicklink;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryHelpQuickLinkPageRes extends BaseBean {

    @ApiModelProperty(value = "主键")
    private String linkId;

    @ApiModelProperty(value = "链接名称")
    private String linkName;

    @ApiModelProperty(value = "链接图标")
    private String linkImage;

    @ApiModelProperty(value = "备注说明")
    private String remark;

    @ApiModelProperty(value = "链接地址")
    private String linkUrl;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常")
    private String statusCode;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常")
    private String statusName;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常")
    private String statusColor;

    @ApiModelProperty(value = "打开方式，取数据字典")
    private String openTypeCode;

    @ApiModelProperty(value = "打开方式，取数据字典")
    private String openTypeName;

    @ApiModelProperty(value = "打开方式，取数据字典")
    private String openTypeColor;

    @ApiModelProperty(value = "链接位置，导航，首页中部")
    private String linkPositionCode;

    @ApiModelProperty(value = "链接位置，导航，首页中部")
    private String linkPositionName;

    @ApiModelProperty(value = "链接位置，导航，首页中部")
    private String linkPositionColor;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date updateTime;

    @ApiModelProperty(value = "排序")
    private Integer sort;

}