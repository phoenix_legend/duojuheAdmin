package com.duojuhe.coremodule.help.pojo.dto.info;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryHelpInfoPageRes extends QueryMyHelpInfoPageRes {

    @ApiModelProperty(value = "创建部门id")
    private String createDeptId;

    @ApiModelProperty(value = "创建部门名称")
    private String createDeptName;

    @ApiModelProperty(value = "创建者Id", example = "1")
    private String createUserId;

    @ApiModelProperty(value = "创建者姓名", example = "1")
    private String createUserName;
}