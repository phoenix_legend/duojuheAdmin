package com.duojuhe.coremodule.help.pojo.dto.info;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryHelpInfoPageReq extends QueryMyHelpInfoPageReq {
    @ApiModelProperty(value = "创建部门名称")
    private String createDeptName;

    @ApiModelProperty(value = "创建者姓名", example = "1")
    private String createUserName;
}