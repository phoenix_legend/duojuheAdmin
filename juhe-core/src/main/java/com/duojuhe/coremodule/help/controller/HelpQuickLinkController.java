package com.duojuhe.coremodule.help.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;

import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.help.pojo.dto.quicklink.*;
import com.duojuhe.coremodule.help.service.HelpQuickLinkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/helpQuickLink/")
@Api(tags = "帮助中心快捷链接")
@Slf4j
public class HelpQuickLinkController {
    @Resource
    public HelpQuickLinkService helpQuickLinkService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "分页查询帮助中心快捷链接")
    @PostMapping(value = "queryHelpQuickLinkPageResList")
    public ServiceResult<PageResult<List<QueryHelpQuickLinkPageRes>>> queryHelpQuickLinkPageResList(@Valid @RequestBody @ApiParam(value = "分页查询参数") QueryHelpQuickLinkPageReq req) {
        return helpQuickLinkService.queryHelpQuickLinkPageResList(req);
    }


    @ApiOperation(value = "增加 帮助中心快捷链接 信息")
    @PostMapping(value = "saveHelpQuickLink")
    @OperationLog(moduleName = LogEnum.ModuleName.HELP_QUICK_LINK, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveHelpQuickLink(@Valid @RequestBody @ApiParam(value = "保存 帮助中心快捷链接 入参") SaveHelpQuickLinkReq req) {
        return helpQuickLinkService.saveHelpQuickLink(req);
    }

    @ApiOperation(value = "修改 帮助中心快捷链接 信息")
    @PostMapping(value = "updateHelpQuickLink")
    @OperationLog(moduleName = LogEnum.ModuleName.HELP_QUICK_LINK, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateHelpQuickLink(@Valid @RequestBody @ApiParam(value = "修改 帮助中心快捷链接 入参") UpdateHelpQuickLinkReq req) {
        return helpQuickLinkService.updateHelpQuickLink(req);
    }

    @ApiOperation(value = "删除 帮助中心快捷链接 信息")
    @PostMapping(value = "deleteHelpQuickLinkByLinkId")
    @OperationLog(moduleName = LogEnum.ModuleName.HELP_QUICK_LINK, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteHelpQuickLinkByLinkId(@Valid @RequestBody @ApiParam(value = "id信息") HelpQuickLinkIdReq id) {
        return helpQuickLinkService.deleteHelpQuickLinkByLinkId(id);
    }


}