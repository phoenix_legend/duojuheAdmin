package com.duojuhe.coremodule.topo.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.topo.pojo.dto.elementinfo.*;
import com.duojuhe.coremodule.topo.service.TopoElementInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/topo/elementInfo/")
@Api(tags = {"【拓扑图元管理】拓扑图元管理相关接口"})
@Slf4j
public class TopoElementInfoController {
    @Resource
    private TopoElementInfoService topoElementInfoService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【分页查询】分页查询拓扑图元list")
    @PostMapping(value = "queryTopoElementInfoPageResList")
    public ServiceResult<PageResult<List<QueryTopoElementInfoPageRes>>> queryTopoElementInfoPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryTopoElementInfoPageReq req) {
        return topoElementInfoService.queryTopoElementInfoPageResList(req);
    }


    @ApiOperation(value = "【保存】拓扑图元信息")
    @PostMapping(value = "saveTopoElementInfo")
    @OperationLog(moduleName = LogEnum.ModuleName.TIPO_ELEMENT, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveTopoElementInfo(@Valid @RequestBody @ApiParam(value = "入参类") SaveTopoElementInfoReq req) {
        return topoElementInfoService.saveTopoElementInfo(req);
    }


    @ApiOperation(value = "【修改】拓扑图元信息")
    @PostMapping(value = "updateTopoElementInfo")
    @OperationLog(moduleName = LogEnum.ModuleName.TIPO_ELEMENT, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateTopoElementInfo(@Valid @RequestBody @ApiParam(value = "入参类") UpdateTopoElementInfoReq req) {
        return topoElementInfoService.updateTopoElementInfo(req);
    }


    @ApiOperation(value = "【删除】根据拓扑图元ID删除拓扑图元，注意一次仅能删除一个拓扑图元，存在绑定关系的则不能删除")
    @PostMapping(value = "deleteTopoElementInfoByElementId")
    @OperationLog(moduleName = LogEnum.ModuleName.TIPO_ELEMENT, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteTopoElementInfoByElementId(@Valid @RequestBody @ApiParam(value = "入参类") TopoElementInfoId req) {
        return topoElementInfoService.deleteTopoElementInfoByElementId(req);
    }
}
