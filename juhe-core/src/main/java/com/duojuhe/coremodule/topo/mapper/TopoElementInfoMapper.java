package com.duojuhe.coremodule.topo.mapper;

import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.coremodule.topo.entity.TopoElementInfo;
import com.duojuhe.coremodule.topo.pojo.dto.elementinfo.QueryTopoElementInfoPageReq;
import com.duojuhe.coremodule.topo.pojo.dto.elementinfo.QueryTopoElementInfoPageRes;
import com.duojuhe.coremodule.topo.pojo.dto.elementinfo.QueryTopoElementInfoRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TopoElementInfoMapper extends TkMapper<TopoElementInfo> {
    /**
     * 分页查询 根据条件查询拓扑图图元list
     *
     * @return
     */
    List<QueryTopoElementInfoPageRes> queryTopoElementInfoPageResList(@Param("req") QueryTopoElementInfoPageReq req);


    /**
     * 根据条件查询所有图元信息
     * @return
     */
    List<QueryTopoElementInfoRes> queryTopoElementInfoResListByStatus(@Param("req") DataScopeFilterBean req, @Param("statusCode") String statusCode);
}