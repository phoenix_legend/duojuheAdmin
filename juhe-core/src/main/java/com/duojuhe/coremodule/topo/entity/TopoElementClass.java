package com.duojuhe.coremodule.topo.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("拓扑图元分类表")
@Table(name = "topo_element_class")
public class TopoElementClass extends BaseBean {
    @ApiModelProperty(value = "分类主键", required = true)
    @Column(name = "class_id")
    @Id
    private String classId;

    @ApiModelProperty(value = "分类名称", required = true)
    @Column(name = "class_name")
    private String className;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常", required = true)
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "是否展开，0=否1=是", required = true)
    @Column(name = "expand_flag")
    private Integer expandFlag;

    @ApiModelProperty(value = "排序", required = true)
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;
}