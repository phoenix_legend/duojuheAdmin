package com.duojuhe.coremodule.topo.service;

import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.topo.pojo.dto.elementclass.*;

import java.util.List;

public interface TopoElementClassService {
    /**
     * 【分页查询】分页查询拓扑图元分类分类list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryTopoElementClassPageRes>>> queryTopoElementClassPageResList(QueryTopoElementClassPageReq req);

    /**
     * 查询正常可用的拓扑图元分类
     * @return
     */
    ServiceResult<List<SelectTopoElementClassRes>> queryNormalSelectTopoElementClassResList(DataScopeFilterBean dataScope);

    /**
     * 【保存】拓扑图元分类分类信息
     * @param req
     * @return
     */
    ServiceResult saveTopoElementClass(SaveTopoElementClassReq req);


    /**
     * 【修改】拓扑图元分类分类信息
     * @param req
     * @return
     */
    ServiceResult updateTopoElementClass(UpdateTopoElementClassReq req);


    /**
     * 【删除】根据拓扑图元分类分类ID删除拓扑分类，注意一次仅能删除一个拓扑分类，存在绑定关系的则不能删除
     * @param req
     * @return
     */
    ServiceResult deleteTopoElementClassByClassId(TopoElementClassIdReq req);
}
