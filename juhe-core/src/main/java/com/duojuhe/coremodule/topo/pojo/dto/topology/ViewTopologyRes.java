package com.duojuhe.coremodule.topo.pojo.dto.topology;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * 拓扑图渲染
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ViewTopologyRes extends BaseBean {
    @ApiModelProperty(value = "拓扑图名称", example = "1")
    private String topoName;

    @ApiModelProperty(value = "拓扑信息", example = "1")
    private String xmlData;
}
