package com.duojuhe.coremodule.topo.service;

import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.topo.pojo.dto.topology.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface TopoTopologyService {
    /**
     * 【分页查询】分页查询拓扑图list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryTopoTopologyPageRes>>> queryTopoTopologyPageResList(QueryTopoTopologyPageReq req);

    /**
     * 【保存】拓扑图信息
     * @param req
     * @return
     */
    ServiceResult saveTopoTopology(SaveTopoTopologyReq req);


    /**
     * 【修改】拓扑图信息
     * @param req
     * @return
     */
    ServiceResult updateTopoTopology(UpdateTopoTopologyReq req);


    /**
     * 【删除】根据拓扑图ID删除拓扑图，注意一次仅能删除一个
     * @param req
     * @return
     */
    ServiceResult deleteTopoTopologyByTopoId(TopoTopologyIdReq req);

    /**
     * 【设计】设计拓扑图信息
     * @param req
     * @return
     */
    ServiceResult designTopology(DesignTopologyReq req);

    /**
     * 另存为拓扑图xml
     * @param req
     * @return
     */
    void saveAsTopology(SaveAsTopologyReq req, HttpServletResponse response) throws Exception;


    /**
     * 根据拓扑图id查询拓扑图和分类组合信息
     * @param req
     * @return
     */
    ServiceResult<InitEditTopologyRes> initEditTopologyByTopoId(TopoTopologyIdReq req, DataScopeFilterBean dataScope);


    /**
     * 根据拓扑图id查询拓扑图信息
     * @param req
     * @return
     */
    ServiceResult<ViewTopologyRes> queryViewTopologyByTopoId(TopoTopologyIdReq req);
}
