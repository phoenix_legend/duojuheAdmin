package com.duojuhe.coremodule.topo.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.topo.pojo.dto.topology.*;
import com.duojuhe.coremodule.topo.service.TopoTopologyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/topo/topology/")
@Api(tags = {"【拓扑图管理】拓扑图管理相关接口"})
@Slf4j
public class TopoTopologyController {
    @Resource
    private TopoTopologyService topoTopologyService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【分页查询】分页查询拓扑图list")
    @PostMapping(value = "queryTopoTopologyPageResList")
    public ServiceResult<PageResult<List<QueryTopoTopologyPageRes>>> queryTopoTopologyPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryTopoTopologyPageReq req) {
        return topoTopologyService.queryTopoTopologyPageResList(req);
    }


    @ApiOperation(value = "【保存】拓扑图")
    @PostMapping(value = "saveTopoTopology")
    @OperationLog(moduleName = LogEnum.ModuleName.TIPO_TOPOLOGY, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveTopoTopology(@Valid @RequestBody @ApiParam(value = "入参类") SaveTopoTopologyReq req) {
        return topoTopologyService.saveTopoTopology(req);
    }


    @ApiOperation(value = "【修改】拓扑图")
    @PostMapping(value = "updateTopoTopology")
    @OperationLog(moduleName = LogEnum.ModuleName.TIPO_TOPOLOGY, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateTopoTopology(@Valid @RequestBody @ApiParam(value = "入参类") UpdateTopoTopologyReq req) {
        return topoTopologyService.updateTopoTopology(req);
    }


    @ApiOperation(value = "【删除】根据拓扑ID删除拓扑图，注意一次仅能删除一个拓扑图")
    @PostMapping(value = "deleteTopoTopologyByTopoId")
    @OperationLog(moduleName = LogEnum.ModuleName.TIPO_TOPOLOGY, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteTopoTopologyByTopoId(@Valid @RequestBody @ApiParam(value = "入参类") TopoTopologyIdReq req) {
        return topoTopologyService.deleteTopoTopologyByTopoId(req);
    }


    @ApiOperation(value = "设计布局拓扑图")
    @PostMapping(value = "designTopology")
    @OperationLog(moduleName = LogEnum.ModuleName.TIPO_TOPOLOGY, operationType = LogEnum.OperationType.OTHER)
    public ServiceResult designTopology(@Valid @RequestBody @ApiParam(value = "设计布局拓扑图入参类") DesignTopologyReq req){
        return topoTopologyService.designTopology(req);
    }

    @ApiOperation(value = "另存为拓扑图")
    @GetMapping(value = "saveAsTopology")
    public void saveAsTopology(@ApiParam(value = "另存为拓扑图入参类") SaveAsTopologyReq req, HttpServletResponse response) throws Exception {
        topoTopologyService.saveAsTopology(req,response);
    }



    @SuppressWarnings("unchecked")
    @ApiOperation(value = "根据拓扑图id查询拓扑图和分类组合信息")
    @PostMapping(value = "initEditTopologyByTopoId")
    public ServiceResult<InitEditTopologyRes> initEditTopologyByTopoId(@Valid @RequestBody @ApiParam(value = "根据拓扑图id查询拓扑图和分类组合信息入参类") TopoTopologyIdReq req){
        return topoTopologyService.initEditTopologyByTopoId(req,new DataScopeFilterBean());
    }

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "根据拓扑图id查询拓扑图信息")
    @PostMapping(value = "queryViewTopologyByTopoId")
    public ServiceResult<ViewTopologyRes> queryViewTopologyByTopoId(@Valid @RequestBody @ApiParam(value = "根据拓扑图id查询拓扑图信息入参类") TopoTopologyIdReq req) {
        return topoTopologyService.queryViewTopologyByTopoId(req);
    }
}
