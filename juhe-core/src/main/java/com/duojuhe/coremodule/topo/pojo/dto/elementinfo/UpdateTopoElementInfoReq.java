package com.duojuhe.coremodule.topo.pojo.dto.elementinfo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateTopoElementInfoReq extends SaveTopoElementInfoReq {
    @ApiModelProperty(value = "图元ID", required = true)
    @NotBlank(message = "图元ID不可为空")
    private String elementId;
}