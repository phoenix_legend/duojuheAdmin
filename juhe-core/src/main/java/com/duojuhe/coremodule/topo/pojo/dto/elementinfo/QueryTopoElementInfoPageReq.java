package com.duojuhe.coremodule.topo.pojo.dto.elementinfo;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryTopoElementInfoPageReq extends PageHead {
    @ApiModelProperty(value = "图元名称", required = true)
    private String elementTitle;

    @ApiModelProperty(value = "图元归属分类", required = true)
    private String classId;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常", required = true)
    private String statusCode;

    @ApiModelProperty(value = "开始日期 yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期 yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}