package com.duojuhe.coremodule.topo.pojo.dto.topology;

import com.duojuhe.coremodule.topo.pojo.dto.elementclass.InitTopoElementClassRes;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class InitEditTopologyRes  extends ViewTopologyRes {
    @ApiModelProperty(value = "分类信息", example = "1")
    private List<InitTopoElementClassRes> classResList;
}
