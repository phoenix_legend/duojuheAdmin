package com.duojuhe.coremodule.topo.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.topo.pojo.dto.elementclass.*;
import com.duojuhe.coremodule.topo.service.TopoElementClassService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/topo/elementClass/")
@Api(tags = {"【拓扑图元分类管理】拓扑图元分类管理相关接口"})
@Slf4j
public class TopoElementClassController {
    @Resource
    private TopoElementClassService topoElementClassService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【分页查询】分页查询拓扑图元分类list")
    @PostMapping(value = "queryTopoElementClassPageResList")
    public ServiceResult<PageResult<List<QueryTopoElementClassPageRes>>> queryTopoElementClassPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryTopoElementClassPageReq req) {
        return topoElementClassService.queryTopoElementClassPageResList(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【图元分类选择】查询正常可用的拓扑图元分类")
    @PostMapping(value = "queryNormalSelectTopoElementClassResList")
    public ServiceResult<List<SelectTopoElementClassRes>> queryNormalSelectTopoElementClassResList() {
        return topoElementClassService.queryNormalSelectTopoElementClassResList(new DataScopeFilterBean());
    }

    @ApiOperation(value = "【保存】拓扑图元分类信息")
    @PostMapping(value = "saveTopoElementClass")
    @OperationLog(moduleName = LogEnum.ModuleName.TIPO_ELEMENT_CLASS, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveTopoElementClass(@Valid @RequestBody @ApiParam(value = "入参类") SaveTopoElementClassReq req) {
        return topoElementClassService.saveTopoElementClass(req);
    }


    @ApiOperation(value = "【修改】拓扑图元分类信息")
    @PostMapping(value = "updateTopoElementClass")
    @OperationLog(moduleName = LogEnum.ModuleName.TIPO_ELEMENT_CLASS, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateTopoElementClass(@Valid @RequestBody @ApiParam(value = "入参类") UpdateTopoElementClassReq req) {
        return topoElementClassService.updateTopoElementClass(req);
    }


    @ApiOperation(value = "【删除】根据拓扑分类ID删除拓扑图元分类，注意一次仅能删除一个拓扑图元分类，存在绑定关系的则不能删除")
    @PostMapping(value = "deleteTopoElementClassByClassId")
    @OperationLog(moduleName = LogEnum.ModuleName.TIPO_ELEMENT_CLASS, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteTopoElementClassByClassId(@Valid @RequestBody @ApiParam(value = "入参类") TopoElementClassIdReq req) {
        return topoElementClassService.deleteTopoElementClassByClassId(req);
    }
}
