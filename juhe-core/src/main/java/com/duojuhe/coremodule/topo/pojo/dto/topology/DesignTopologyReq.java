package com.duojuhe.coremodule.topo.pojo.dto.topology;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


/**
 * 设计拓扑图
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DesignTopologyReq extends TopoTopologyIdReq {
    @ApiModelProperty(value = "拓扑图XML内容", example = "123",required = true)
    @NotBlank(message = "拓扑图XML内容不能为空")
    private String xmlData;
}
