package com.duojuhe.coremodule.topo.pojo.dto.elementinfo;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TopoElementInfoId extends BaseBean {
    @ApiModelProperty(value = "图元ID", required = true)
    @NotBlank(message = "图元ID不可为空")
    private String elementId;
}