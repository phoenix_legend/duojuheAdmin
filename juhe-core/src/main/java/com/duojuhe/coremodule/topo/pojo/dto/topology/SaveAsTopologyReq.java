package com.duojuhe.coremodule.topo.pojo.dto.topology;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * 另存为拓扑图
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveAsTopologyReq extends BaseBean {
    @ApiModelProperty(value = "拓扑ID名称", example = "1",required=true)
    private String topoId;

    @ApiModelProperty(value = "拓扑图名称", example = "123",required=true)
    private String filename;
}
