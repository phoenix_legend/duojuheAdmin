package com.duojuhe.coremodule.topo.mapper;

import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.coremodule.topo.entity.TopoElementClass;
import com.duojuhe.coremodule.topo.pojo.dto.elementclass.QueryTopoElementClassPageReq;
import com.duojuhe.coremodule.topo.pojo.dto.elementclass.QueryTopoElementClassPageRes;
import com.duojuhe.coremodule.topo.pojo.dto.elementclass.SelectTopoElementClassRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TopoElementClassMapper extends TkMapper<TopoElementClass> {
    /**
     * 分页查询 根据条件查询拓扑图图元分类list
     *
     * @return
     */
    List<QueryTopoElementClassPageRes> queryTopoElementClassPageResList(@Param("req") QueryTopoElementClassPageReq req);

    /**
     * 根据状态查询拓扑图图元分类 list
     *
     * @return
     */
    List<SelectTopoElementClassRes> querySelectTopoElementClassResListByStatus(@Param("req") DataScopeFilterBean req, @Param("statusCode") String statusCode);

}