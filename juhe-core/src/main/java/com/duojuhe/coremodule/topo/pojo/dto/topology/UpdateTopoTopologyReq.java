package com.duojuhe.coremodule.topo.pojo.dto.topology;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateTopoTopologyReq extends SaveTopoTopologyReq {
    @ApiModelProperty(value = "拓扑图ID", required = true)
    @NotBlank(message = "拓扑图ID不可为空")
    private String topoId;
}