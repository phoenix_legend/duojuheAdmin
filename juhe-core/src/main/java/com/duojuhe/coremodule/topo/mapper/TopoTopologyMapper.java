package com.duojuhe.coremodule.topo.mapper;

import com.duojuhe.coremodule.topo.entity.TopoTopology;
import com.duojuhe.coremodule.topo.pojo.dto.topology.QueryTopoTopologyPageReq;
import com.duojuhe.coremodule.topo.pojo.dto.topology.QueryTopoTopologyPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TopoTopologyMapper extends TkMapper<TopoTopology> {
    /**
     * 分页查询 根据条件查询拓扑图list
     *
     * @return
     */
    List<QueryTopoTopologyPageRes> queryTopoTopologyPageResList(@Param("req") QueryTopoTopologyPageReq req);

}