package com.duojuhe.coremodule.topo.pojo.dto.elementclass;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TopoElementClassIdReq extends BaseBean {
    @ApiModelProperty(value = "分类ID", required = true)
    @NotBlank(message = "分类ID不可为空")
    private String classId;
}
