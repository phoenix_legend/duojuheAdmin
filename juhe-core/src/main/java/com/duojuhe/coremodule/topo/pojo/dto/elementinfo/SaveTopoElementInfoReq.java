package com.duojuhe.coremodule.topo.pojo.dto.elementinfo;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveTopoElementInfoReq extends BaseBean {
    @ApiModelProperty(value = "图元名称", required = true)
    @Length(max = 30, message = "图元名称不得超过{max}位字符")
    @NotBlank(message = "图元名称不能为空")
    private String elementTitle;

    @ApiModelProperty(value = "图元分类", required = true)
    @NotBlank(message = "图元分类不能为空")
    private String classId;

    @ApiModelProperty(value = "状态：FORBID禁用 NORMAL正常", example = "1",required=true)
    @Pattern(regexp = RegexpConstants.STATUS, message = "状态参数错误!")
    @NotBlank(message = "状态不能为空")
    private String statusCode;

    @ApiModelProperty(value = "排序，只能为纯数字", example = "1",required=true)
    @NotNull(message = "排序不能为空")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;

    @ApiModelProperty(value = "允许绑定设备，0不允许 1允许", required = true)
    @NotNull(message = "允许绑定设备不能为空")
    @Range(min = 0, max = 1, message = "允许绑定设备参数值不正确")
    private Integer bindDevice;

    @ApiModelProperty(value = "允许编辑链接，0不允许 1允许", required = true)
    @NotNull(message = "允许编辑链接参数不能为空")
    @Range(min = 0, max = 1, message = "允许编辑链接数值不正确")
    private Integer editLink;

    @ApiModelProperty(value = "图元图标", required = true)
    @NotBlank(message = "图元图标不能为空")
    @Length(max = 500, message = "图元图标不得超过{max}位字符")
    private String elementImg;

    @ApiModelProperty(value = "图标高度", required = true)
    @Range(max = 10000, message = "图标高度不正确")
    @NotNull(message = "图标高度参数不能为空")
    private Integer imgHeight;

    @ApiModelProperty(value = "图标宽度", required = true)
    @Range(max = 10000, message = "图标宽度不正确")
    @NotNull(message = "图标宽度参数不能为空")
    private Integer imgWidth;
}