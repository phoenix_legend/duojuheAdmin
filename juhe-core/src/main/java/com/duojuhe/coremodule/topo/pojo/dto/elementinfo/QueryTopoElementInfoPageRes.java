package com.duojuhe.coremodule.topo.pojo.dto.elementinfo;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryTopoElementInfoPageRes extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String elementId;

    @ApiModelProperty(value = "图元名称", required = true)
    private String elementTitle;

    @ApiModelProperty(value = "图元归属分类", required = true)
    private String classId;

    @ApiModelProperty(value = "图元归属分类名称", required = true)
    private String className;

    @ApiModelProperty(value = "排序", required = true)
    private Integer sort;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常", required = true)
    private String statusCode;

    @ApiModelProperty(value = "状态中文 FORBID禁用 NORMAL正常", example = "1")
    private String statusName;

    @ApiModelProperty(value = "状态显示颜色", example = "1")
    private String statusColor;

    @ApiModelProperty(value = "允许绑定设备，0不允许 1允许", required = true)
    private Integer bindDevice;

    @ApiModelProperty(value = "允许编辑链接，0不允许 1允许", required = true)
    private Integer editLink;

    @ApiModelProperty(value = "图元小图标", required = true)
    private String elementImg;

    @ApiModelProperty(value = "图片高度", required = true)
    private Integer imgHeight;

    @ApiModelProperty(value = "图片宽度", required = true)
    private Integer imgWidth;

    @ApiModelProperty(value = "创建部门id")
    private String createDeptId;

    @ApiModelProperty(value = "创建部门名称")
    private String createDeptName;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date updateTime;
}