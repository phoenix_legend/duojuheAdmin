package com.duojuhe.coremodule.topo.pojo.dto.topology;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveTopoTopologyReq extends BaseBean {
    @ApiModelProperty(value = "拓扑图名称", required = true)
    @NotBlank(message = "拓扑图名称不能为空")
    @Length(max = 250, message = "拓扑图名称不得超过{max}位字符")
    private String topoName;

    @ApiModelProperty(value = "备注说明")
    @Pattern(regexp = RegexpConstants.REMARK, message = "备注禁止输入特殊字符!")
    @Length(max = 100, message = "备注不得超过{max}位字符")
    private String remark;


    @ApiModelProperty(value = "封面图片base64格式", required = true)
    @NotBlank(message = "封面图片不能为空")
    private String coverImg;
}