package com.duojuhe.coremodule.topo.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("拓扑图表")
@Table(name = "topo_topology")
public class TopoTopology extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "topo_id")
    @Id
    private String topoId;

    @ApiModelProperty(value = "拓扑图名称", required = true)
    @Column(name = "topo_name")
    private String topoName;

    @ApiModelProperty(value = "拓扑图封面", required = true)
    @Column(name = "cover_img")
    private String coverImg;


    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "备注说明")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "拓扑xml内容")
    @Column(name = "xml_data")
    private String xmlData;
}