package com.duojuhe.coremodule.topo.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("拓扑图元表")
@Table(name = "topo_element_info")
public class TopoElementInfo extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "element_id")
    @Id
    private String elementId;

    @ApiModelProperty(value = "图元名称", required = true)
    @Column(name = "element_title")
    private String elementTitle;

    @ApiModelProperty(value = "图元归属分类", required = true)
    @Column(name = "class_id")
    private String classId;

    @ApiModelProperty(value = "排序", required = true)
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "状态，FORBID禁用 NORMAL正常", required = true)
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "允许绑定设备，0不允许 1允许", required = true)
    @Column(name = "bind_device")
    private Integer bindDevice;

    @ApiModelProperty(value = "允许编辑链接，0不允许 1允许", required = true)
    @Column(name = "edit_link")
    private Integer editLink;

    @ApiModelProperty(value = "图元小图标", required = true)
    @Column(name = "element_img")
    private String elementImg;

    @ApiModelProperty(value = "图片高度", required = true)
    @Column(name = "img_height")
    private Integer imgHeight;

    @ApiModelProperty(value = "图片宽度", required = true)
    @Column(name = "img_width")
    private Integer imgWidth;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;
}