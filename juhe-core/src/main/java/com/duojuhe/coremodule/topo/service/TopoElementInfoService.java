package com.duojuhe.coremodule.topo.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.topo.pojo.dto.elementinfo.*;

import java.util.List;

public interface TopoElementInfoService {
    /**
     * 【分页查询】分页查询拓扑图元list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryTopoElementInfoPageRes>>> queryTopoElementInfoPageResList(QueryTopoElementInfoPageReq req);

    /**
     * 【保存】拓扑图元信息
     * @param req
     * @return
     */
    ServiceResult saveTopoElementInfo(SaveTopoElementInfoReq req);


    /**
     * 【修改】拓扑图元信息
     * @param req
     * @return
     */
    ServiceResult updateTopoElementInfo(UpdateTopoElementInfoReq req);


    /**
     * 【删除】根据拓扑图元ID删除拓扑图元，注意一次仅能删除一个
     * @param req
     * @return
     */
    ServiceResult deleteTopoElementInfoByElementId(TopoElementInfoId req);
}
