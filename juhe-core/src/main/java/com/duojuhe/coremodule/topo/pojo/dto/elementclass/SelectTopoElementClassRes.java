package com.duojuhe.coremodule.topo.pojo.dto.elementclass;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SelectTopoElementClassRes extends BaseBean {
    @ApiModelProperty(value = "分类主键", required = true)
    private String classId;

    @ApiModelProperty(value = "分类名称", required = true)
    private String className;

    @ApiModelProperty(value = "状态：FORBID禁用 NORMAL正常", example = "1")
    private String statusCode;

    @ApiModelProperty(value = "是否展开，0=否1=是", required = true)
    private Integer expandFlag;
}