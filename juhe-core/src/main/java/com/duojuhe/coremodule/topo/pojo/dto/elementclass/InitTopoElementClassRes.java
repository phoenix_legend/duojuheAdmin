package com.duojuhe.coremodule.topo.pojo.dto.elementclass;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.coremodule.topo.pojo.dto.elementinfo.QueryTopoElementInfoRes;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class InitTopoElementClassRes extends BaseBean {
    @ApiModelProperty(value = "分类主键", example = "123")
    private String classId;

    @ApiModelProperty(value = "分类名称", example = "123")
    private String className;

    @ApiModelProperty(value = "下级List,不可排序", example = "1")
    private List<QueryTopoElementInfoRes> children;
}
