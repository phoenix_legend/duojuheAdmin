package com.duojuhe.coremodule.topo.pojo.dto.elementinfo;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * 拓扑图图元
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QueryTopoElementInfoRes extends BaseBean {
    @ApiModelProperty(value = "图元主键", example = "123")
    private String elementId;

    @ApiModelProperty(value = "图元名称", example = "123")
    private String elementTitle;

    @ApiModelProperty(value = "图元小图标", example = "123")
    private String elementImg;

    @ApiModelProperty(value = "图片高度", example = "123")
    private String imgHeight;

    @ApiModelProperty(value = "图片宽度", example = "123")
    private String imgWidth;

    @ApiModelProperty(value = "图元归属分类", example = "123")
    private String className;

    @ApiModelProperty(value = "图元分类主键", example = "123")
    private String classId;

    @ApiModelProperty(value = "排序", example = "1")
    private Integer sort;

    @ApiModelProperty(value = "允许绑定设备，0不允许 1允许", example = "1")
    private Integer bindDevice;

    @ApiModelProperty(value = "允许编辑链接，0不允许 1允许", example = "1")
    private Integer editLink;
}
