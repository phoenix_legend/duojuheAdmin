package com.duojuhe.coremodule.topo.pojo.dto.topology;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TopoTopologyIdReq extends BaseBean {
    @ApiModelProperty(value = "拓扑图ID", required = true)
    @NotBlank(message = "拓扑图ID不可为空")
    private String topoId;
}