package com.duojuhe.coremodule.topo.service.impl;

import com.duojuhe.cache.SystemDictCache;
import com.duojuhe.common.annotation.DataScopeFilter;
import com.duojuhe.common.bean.UserTokenInfoVo;
import com.duojuhe.common.enums.SystemEnum;
import com.duojuhe.common.exception.base.DuoJuHeException;
import com.duojuhe.common.result.ErrorCodes;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.idgenerator.UUIDUtils;
import com.duojuhe.common.utils.page.PageHelperUtil;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.system.entity.SystemDict;
import com.duojuhe.coremodule.topo.entity.TopoElementClass;
import com.duojuhe.coremodule.topo.entity.TopoElementInfo;
import com.duojuhe.coremodule.topo.mapper.TopoElementClassMapper;
import com.duojuhe.coremodule.topo.mapper.TopoElementInfoMapper;
import com.duojuhe.coremodule.topo.pojo.dto.elementinfo.*;
import com.duojuhe.coremodule.topo.service.TopoElementInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class TopoElementInfoServiceImpl extends BaseService implements TopoElementInfoService {
    @Resource
    private SystemDictCache systemDictCache;
    @Resource
    private TopoElementClassMapper topoElementClassMapper;
    @Resource
    private TopoElementInfoMapper topoElementInfoMapper;
    /**
     * 【分页查询】分页查询拓扑图元list
     * @param req
     * @return
     */
    @DataScopeFilter
    @Override
    public ServiceResult<PageResult<List<QueryTopoElementInfoPageRes>>> queryTopoElementInfoPageResList(QueryTopoElementInfoPageReq req) {
        PageHelperUtil.orderByAndStartPage(req, "sort desc,createTime desc, elementId desc");
        List<QueryTopoElementInfoPageRes> list = topoElementInfoMapper.queryTopoElementInfoPageResList(req);
        for (QueryTopoElementInfoPageRes res:list){
            SystemDict dictStatus = systemDictCache.getSystemDictByDictCode(res.getStatusCode());
            res.setStatusName(dictStatus.getDictName());
            res.setStatusColor(dictStatus.getDictColor());
        }
        return PageHelperUtil.returnServiceResult(req,list);
    }

    /**
     * 【保存】拓扑图元信息
     * @param req
     * @return
     */
    @Override
    public ServiceResult saveTopoElementInfo(SaveTopoElementInfoReq req) {
        //检查图元分类
        checkTopoElementClassId(req.getClassId());
        //当前登录人
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //图元id
        String elementId = UUIDUtils.getUUID32();
        //当前时间
        Date nowDate = new Date();
        TopoElementInfo topoElementInfo = new TopoElementInfo();
        topoElementInfo.setElementId(elementId);
        topoElementInfo.setClassId(req.getClassId());
        topoElementInfo.setElementTitle(req.getElementTitle());
        topoElementInfo.setEditLink(req.getEditLink());
        topoElementInfo.setImgHeight(req.getImgHeight());
        topoElementInfo.setImgWidth(req.getImgWidth());
        topoElementInfo.setElementImg(req.getElementImg());
        topoElementInfo.setBindDevice(req.getBindDevice());
        topoElementInfo.setCreateTime(nowDate);
        topoElementInfo.setCreateUserId(userTokenInfoVo.getUserId());
        topoElementInfo.setUpdateTime(nowDate);
        topoElementInfo.setUpdateUserId(userTokenInfoVo.getUserId());
        topoElementInfo.setSort(req.getSort());
        topoElementInfo.setStatusCode(req.getStatusCode());
        topoElementInfo.setCreateDeptId(userTokenInfoVo.getCreateDeptId());
        topoElementInfo.setTenantId(userTokenInfoVo.getTenantId());
        topoElementInfoMapper.insertSelective(topoElementInfo);
        return ServiceResult.ok(elementId);
    }

    /**
     * 【修改】拓扑图元信息
     * @param req
     * @return
     */
    @Override
    public ServiceResult updateTopoElementInfo(UpdateTopoElementInfoReq req) {
        String elementId = req.getElementId();
        TopoElementInfo elementInfo = topoElementInfoMapper.selectByPrimaryKey(elementId);
        if (elementInfo == null) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        //检查图元分类
        checkTopoElementClassId(req.getClassId());
        //当前登录人
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //当前时间
        Date nowDate = new Date();
        TopoElementInfo topoElementInfo = new TopoElementInfo();
        topoElementInfo.setElementId(elementId);
        topoElementInfo.setClassId(req.getClassId());
        topoElementInfo.setElementTitle(req.getElementTitle());
        topoElementInfo.setEditLink(req.getEditLink());
        topoElementInfo.setImgHeight(req.getImgHeight());
        topoElementInfo.setImgWidth(req.getImgWidth());
        topoElementInfo.setElementImg(req.getElementImg());
        topoElementInfo.setBindDevice(req.getBindDevice());
        topoElementInfo.setUpdateTime(nowDate);
        topoElementInfo.setUpdateUserId(userTokenInfoVo.getUserId());
        topoElementInfo.setSort(req.getSort());
        topoElementInfo.setStatusCode(req.getStatusCode());
        topoElementInfoMapper.updateByPrimaryKeySelective(topoElementInfo);
        return ServiceResult.ok(elementId);
    }

    /**
     * 【删除】根据拓扑图元ID删除拓扑图元，注意一次仅能删除一个
     * @param req
     * @return
     */
    @Override
    public ServiceResult deleteTopoElementInfoByElementId(TopoElementInfoId req) {
        String elementId = req.getElementId();
        TopoElementInfo elementInfo = topoElementInfoMapper.selectByPrimaryKey(elementId);
        if (elementInfo == null) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        //删除对象
        topoElementInfoMapper.deleteByPrimaryKey(elementId);
        return ServiceResult.ok(elementId);
    }

    /**
     * 【私有方法，辅助其他接口方法使用】 检查图元分类是否存在
     */
    private void checkTopoElementClassId(String classId) {
        if (StringUtils.isBlank(classId)){
            throw new DuoJuHeException(ErrorCodes.TOPO_ELEMENT_CLASS_NOT_EXIST);
        }
        TopoElementClass topoElementClass = topoElementClassMapper.selectByPrimaryKey(classId);
        if (topoElementClass==null || !SystemEnum.STATUS.NORMAL.getKey().equals(topoElementClass.getStatusCode())){
            throw new DuoJuHeException(ErrorCodes.TOPO_ELEMENT_CLASS_NOT_EXIST);
        }
    }
}
