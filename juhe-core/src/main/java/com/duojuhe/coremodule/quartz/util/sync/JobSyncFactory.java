package com.duojuhe.coremodule.quartz.util.sync;

import com.duojuhe.coremodule.quartz.util.AbstractQuartzJob;
import com.duojuhe.coremodule.quartz.util.JobInvokeUtil;
import com.duojuhe.coremodule.quartz.util.bean.JobTask;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;


/**
 * 同步的任务工厂类,多线程同时执行
 */
@Slf4j
@Component
public class JobSyncFactory extends AbstractQuartzJob {
    @Override
    protected void doExecute(JobExecutionContext context, JobTask jobTask) throws Exception {
        JobInvokeUtil.invokeMethod(jobTask);
    }
}