package com.duojuhe.coremodule.quartz.util.bean;


import com.duojuhe.common.bean.BaseBean;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * 系统任务调度
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JobTask extends BaseBean {
    //作业ID主键
    private String jobId;

    //作业分组id
    private String groupId;

   //调度作业名称
    private String jobName;

    //执行表达式,例如:0 0/1 * * * ?
    private String cronExpression;

    //是否同步YES=是NO=不是
    private String isSyncCode;

    //要执行的任务类名称路径
    private String classPath;

    //要执行的任务方法名称
    private String methodName;

    //job运行状态NOT_RUNNING=不运行RUNNING=运行EXCEPTION=异常
    private String jobStatusCode;

    //spring beanId
    private String springId;
}
