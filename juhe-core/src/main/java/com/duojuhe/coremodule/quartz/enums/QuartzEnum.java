package com.duojuhe.coremodule.quartz.enums;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * 调度任务相关枚举
 *
 * @date 2018/5/30
 */
public class QuartzEnum {

    /**
     * 调度任务状态
     */
    public enum QUARTZ_STATUS {
        NOT_RUNNING("NOT_RUNNING", "暂停中"),
        RUNNING("RUNNING", "运行正常"),
        RUN_EXCEPTION("RUN_EXCEPTION", "运行异常");
        @Getter
        private String key;
        @Getter
        private String value;

        QUARTZ_STATUS(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public static String getValueByKey(String key) {
            if (StringUtils.isBlank(key)) {
                return "";
            }
            for (QUARTZ_STATUS e : QUARTZ_STATUS.values()) {
                if (e.getKey().equals(key)) {
                    return e.getValue();
                }
            }
            return "";
        }
    }



}
