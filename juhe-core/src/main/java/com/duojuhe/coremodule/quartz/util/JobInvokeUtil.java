package com.duojuhe.coremodule.quartz.util;

import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.common.exception.base.DuoJuHeException;
import com.duojuhe.common.utils.springcontext.SpringContextUtil;
import com.duojuhe.coremodule.quartz.util.bean.JobTask;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;


/**
 * 任务执行工具
 *
 */
@Slf4j
public class JobInvokeUtil {


    /**
     * 通过反射调用scheduleJob中定义的方法
     *
     * @param jobTask
     */
    public static void invokeMethod(JobTask jobTask) {
        try {
            if (!SystemConstants.SYSTEM_INIT_SUCCESS) {
                return;
            }
            log.info("任务名称 = [" + jobTask.getJobName() + "]准备中......");
            Object object = null;
            Class clazz = null;
            //springId不为空先按springId查找bean
            if (StringUtils.isNotBlank(jobTask.getSpringId())) {
                object = SpringContextUtil.getBean(jobTask.getSpringId());
            } else if (StringUtils.isNotBlank(jobTask.getClassPath())) {
                object = Class.forName(jobTask.getClassPath()).newInstance();
            }
            if (object == null) {
                log.error("任务名称 = [" + jobTask.getJobName() + "]--未启动成功，请检查是否配置正确！！！");
                throw new DuoJuHeException("任务名称 = [" + jobTask.getJobName() + "]--未启动成功，请检查是否配置正确！！！");
            }
            clazz = object.getClass();
            if (clazz == null) {
                log.error("任务名称 = [" + jobTask.getJobName() + "]--未启动成功，请检查是否配置正确！！！");
                throw new DuoJuHeException("任务名称 = [" + jobTask.getJobName() + "]--未启动成功，请检查是否配置正确！！！");
            }
            Method  method = clazz.getDeclaredMethod(jobTask.getMethodName());
            if (method != null) {
                method.invoke(object);
            }
            log.info("任务名称 = [" + jobTask.getJobName() + "]---------------启动成功！");
        } catch (NoSuchMethodException e) {
            log.error("任务名称 = [" + jobTask.getJobName() + "]---------------未启动成功，方法名设置错误！！！");
            throw new DuoJuHeException("任务名称 = [" + jobTask.getJobName() + "]--未启动成功，方法名设置错误！！！");
        } catch (Exception e) {
            log.error("任务名称 = [" + jobTask.getJobName() + "]--未启动成功，出现异常", e);
            throw new DuoJuHeException("任务名称 = [" + jobTask.getJobName() + "]--未启动成功，出现异常" + e);
        }
    }
}
