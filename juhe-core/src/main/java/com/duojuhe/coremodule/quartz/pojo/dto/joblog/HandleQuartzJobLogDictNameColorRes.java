package com.duojuhe.coremodule.quartz.pojo.dto.joblog;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HandleQuartzJobLogDictNameColorRes extends BaseBean {
    @ApiModelProperty(value = "是否同步执行")
    private String isSyncCode;

    @ApiModelProperty(value = "是否同步执行中文")
    private String isSyncName;

    @ApiModelProperty(value = "是否同步执行显示颜色")
    private String isSyncColor;

    @ApiModelProperty(value = "执行状态，取数据字典")
    private String operationStatusCode;

    @ApiModelProperty(value = "执行状态中文")
    private String operationStatusName;

    @ApiModelProperty(value = "执行状态显示颜色")
    private String operationStatusColor;
}
