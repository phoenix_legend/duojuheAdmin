package com.duojuhe.coremodule.quartz.mapper;


import com.duojuhe.coremodule.quartz.entity.QuartzJobLog;
import com.duojuhe.coremodule.quartz.pojo.dto.joblog.QueryQuartzJobLogPageReq;
import com.duojuhe.coremodule.quartz.pojo.dto.joblog.QueryQuartzJobLogPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface QuartzJobLogMapper extends TkMapper<QuartzJobLog> {

    /**
     * 分页查询 根据条件查询定时任务执行日志list
     *
     * @return
     */
    List<QueryQuartzJobLogPageRes> queryQuartzJobLogResList(@Param("req") QueryQuartzJobLogPageReq req);

}