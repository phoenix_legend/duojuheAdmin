package com.duojuhe.coremodule.quartz.util.sync;

import com.duojuhe.coremodule.quartz.util.AbstractQuartzJob;
import com.duojuhe.coremodule.quartz.util.JobInvokeUtil;
import com.duojuhe.coremodule.quartz.util.bean.JobTask;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;



/**
 * 任务工厂类,非同步，等待执行完了才执行下一次
 */
@Slf4j
@DisallowConcurrentExecution
public class JobFactory extends AbstractQuartzJob {
    @Override
    protected void doExecute(JobExecutionContext context, JobTask jobTask) throws Exception {
        JobInvokeUtil.invokeMethod(jobTask);
    }
}