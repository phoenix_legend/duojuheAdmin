package com.duojuhe.coremodule.quartz.pojo.dto.job;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * 保存调度作业
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveQuartzJobReq extends BaseBean {
    @ApiModelProperty(value = "任务名称", example = "1",required=true)
    @NotBlank(message = "任务名称不能为空")
    @Pattern(regexp = RegexpConstants.CH_EN_NUMBER_LINE, message = "任务名称只能填写中文英文数字和下划线!")
    @Length(max = 20, message = "任务名称不得超过{max}位字符")
    private String jobName;

    @ApiModelProperty(value = "Cron表达式,例如:0 0/1 * * * ?", example = "0 0/1 * * * ?",required=true)
    @NotBlank(message = "Cron表达式不能为空")
    @Length(max = 50, message = "Cron表达式不得超过{max}位字符")
    private String cronExpression;

    @ApiModelProperty(value = "是否同步YES=是NO=不是", example = "1",required=true)
    @NotBlank(message = "是否同步参不能为空")
    @Length(max = 50, message = "是否同步参数不得超过{max}位字符")
    @Pattern(regexp = RegexpConstants.YES_NO, message = "是否同步参数错误!")
    private String isSyncCode;

    @ApiModelProperty(value = "要执行的任务类名称路径", example = "javax.persistence",required=true)
    @NotBlank(message = "任务类路径不能为空")
    @Length(max = 250, message = "任务类路径不得超过{max}位字符")
    @Pattern(regexp = RegexpConstants.EN_NUMBER_LINE_POINT, message = "任务类路径只能填写英文数字下划线和点!")
    private String classPath;

    @ApiModelProperty(value = "SpringId", example = "spring")
    @Length(max = 20, message = "SpringId不得超过{max}位字符")
    @Pattern(regexp = RegexpConstants.EN_LENGTH1_20_OR_NULL, message = "SpringId只能为英文字母")
    private String springId;

    @ApiModelProperty(value = "要执行的目标方法", example = "helloWord",required=true)
    @NotBlank(message = "目标方法不能为空")
    @Length(max = 50, message = "目标方法不得超过{max}位字符")
    @Pattern(regexp = RegexpConstants.EN_LENGTH1_50, message = "目标方法只能为英文字母和数字下划线")
    private String methodName;

    @ApiModelProperty(value = "调度任务描述", example = "1")
    @Pattern(regexp = RegexpConstants.REMARK_OR_NULL, message = "任务描述禁止输入特殊字符!")
    @Length(max = 100, message = "任务描述不得超过{max}位字符")
    private String description;
}
