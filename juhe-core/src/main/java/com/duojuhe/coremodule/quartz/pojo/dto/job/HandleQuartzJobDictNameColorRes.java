package com.duojuhe.coremodule.quartz.pojo.dto.job;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HandleQuartzJobDictNameColorRes extends BaseBean {
    @ApiModelProperty(value = "是否同步执行NO-不是YES=是")
    private String isSyncCode;

    @ApiModelProperty(value = "是否同步执行中文")
    private String isSyncName;

    @ApiModelProperty(value = "是否同步执行显示颜色")
    private String isSyncColor;

    @ApiModelProperty(value = "job运行状态NOT_RUNNING=不运行RUNNING=运行EXCEPTION=异常")
    private String jobStatusCode;

    @ApiModelProperty(value = "运行状态中文")
    private String jobStatusName;

    @ApiModelProperty(value = "运行状态显示颜色")
    private String jobStatusColor;

    @ApiModelProperty(value = "是否是内置:YES是，NO否")
    private String builtIn;

    @ApiModelProperty(value = "是否是内置")
    private String builtInName;

    @ApiModelProperty(value = "是否是内置显示颜色")
    private String builtInColor;
}
