package com.duojuhe.coremodule.quartz.pojo.dto.joblog;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryQuartzJobLogPageRes extends HandleQuartzJobLogDictNameColorRes {
    @ApiModelProperty(value = "主键", required = true)
    private String logId;

    @ApiModelProperty(value = "任务id", required = true)
    private String jobId;

    @ApiModelProperty(value = "任务所在分组id")
    private String groupId;

    @ApiModelProperty(value = "任务名称", required = true)
    private String jobName;

    @ApiModelProperty(value = "任务表达式", required = true)
    private String cronExpression;

    @ApiModelProperty(value = "要执行的任务类名称路径", required = true)
    private String classPath;

    @ApiModelProperty(value = "要执行的任务方法名称", required = true)
    private String methodName;

    @ApiModelProperty(value = "日志时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "备注说明")
    private String remark;

    @ApiModelProperty(value = "spring Id")
    private String springId;

    @ApiModelProperty(value = "执行开始时间")
    @JsonFormat(pattern= DateUtils.yyyy_MM_ddHHmmssSSS,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.yyyy_MM_ddHHmmssSSS)
    private Date startTime;

    @ApiModelProperty(value = "执行停止时间")
    @JsonFormat(pattern= DateUtils.yyyy_MM_ddHHmmssSSS,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.yyyy_MM_ddHHmmssSSS)
    private Date stopTime;

    @ApiModelProperty(value = "描述说明")
    private String description;
}