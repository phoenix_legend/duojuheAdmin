package com.duojuhe.coremodule.quartz.pojo.dto.job;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ApiModel(value = "系统调度任务表 列表查询 请求")
public class QueryQuartzJobPageReq extends PageHead {
    @ApiModelProperty(value = "调度作业名称")
    private String jobName;

    @ApiModelProperty(value = "任务所在分组id")
    private String groupId;

    @ApiModelProperty(value = "是否同步执行NO-不是YES=是")
    private String isSyncCode;

    @ApiModelProperty(value = "要执行的任务类名称路径")
    private String classPath;

    @ApiModelProperty(value = "要执行的任务方法名称")
    private String methodName;

    @ApiModelProperty(value = "job运行状态NOT_RUNNING=不运行RUNNING=运行EXCEPTION=异常")
    private String jobStatusCode;

    @ApiModelProperty(value = "spring beanId")
    private String springId;

    @ApiModelProperty(value = "任务描述")
    private String description;

    @ApiModelProperty(value = "是否内置系统任务NO-不是YES=是")
    private String builtIn;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "开始日期yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }

}