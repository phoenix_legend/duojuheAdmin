package com.duojuhe.coremodule.quartz.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.quartz.pojo.dto.job.*;
import com.duojuhe.coremodule.quartz.service.QuartzJobService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/quartzJob/")
@Api(tags = "【定时任务】")
@Slf4j
public class QuartzJobController {

    @Resource
    public QuartzJobService quartzJobService;


    @ApiOperation(value = "【分页查询】根据条件查询定时任务list")
    @PostMapping(value = "queryQuartzJobResList")
    public ServiceResult<PageResult<List<QueryQuartzJobPageRes>>> queryQuartzJobResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryQuartzJobPageReq req) {
        return quartzJobService.queryQuartzJobResList(req);
    }


    @ApiOperation(value = "保存 系统调度任务信息")
    @PostMapping(value = "saveQuartzJob")
    @OperationLog(moduleName = LogEnum.ModuleName.QUARTZ_JOB, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveQuartzJob(@Valid @RequestBody @ApiParam(value = "保存入参") SaveQuartzJobReq req) {
        return quartzJobService.saveQuartzJob(req);
    }

    @ApiOperation(value = "修改 系统调度任务信息")
    @PostMapping(value = "updateQuartzJob")
    @OperationLog(moduleName = LogEnum.ModuleName.QUARTZ_JOB, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateQuartzJob(@Valid @RequestBody @ApiParam(value = "修改入参") UpdateQuartzJobReq req) {
        return quartzJobService.updateQuartzJob(req);
    }

    @ApiOperation(value = "删除 系统调度任务信息")
    @PostMapping(value = "deleteQuartzJobByJobId")
    @OperationLog(moduleName = LogEnum.ModuleName.QUARTZ_JOB, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteQuartzJobByJobId(@Valid @RequestBody @ApiParam(value = "删除入参") QuartzJobIdReq req) {
        return quartzJobService.deleteQuartzJobByJobId(req);
    }

    @ApiOperation(value = "暂停 系统调度任务信息")
    @PostMapping(value = "pauseQuartzJobByJobId")
    @OperationLog(moduleName = LogEnum.ModuleName.QUARTZ_JOB, operationType = LogEnum.OperationType.OTHER)
    public ServiceResult pauseQuartzJobByJobId(@Valid @RequestBody @ApiParam(value = "暂停入参") QuartzJobIdReq req) {
        return quartzJobService.pauseQuartzJobByJobId(req);
    }

    @ApiOperation(value = "恢复 系统调度任务信息")
    @PostMapping(value = "resumeQuartzJobByJobId")
    @OperationLog(moduleName = LogEnum.ModuleName.QUARTZ_JOB, operationType = LogEnum.OperationType.OTHER)
    public ServiceResult resumeQuartzJobByJobId(@Valid @RequestBody @ApiParam(value = "恢复入参") QuartzJobIdReq req) {
        return quartzJobService.resumeQuartzJobByJobId(req);
    }

    @ApiOperation(value = "执行一次 系统调度任务信息")
    @PostMapping(value = "runOnceJobByJobId")
    @OperationLog(moduleName = LogEnum.ModuleName.QUARTZ_JOB, operationType = LogEnum.OperationType.OTHER)
    public ServiceResult runOnceJobByJobId(@Valid @RequestBody @ApiParam(value = "执行一次入参") QuartzJobIdReq req) {
        return quartzJobService.runOnceJobByJobId(req);
    }


}