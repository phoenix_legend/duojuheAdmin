package com.duojuhe.coremodule.quartz.mapper;

import com.duojuhe.coremodule.quartz.entity.QuartzJob;
import com.duojuhe.coremodule.quartz.pojo.dto.job.QueryQuartzJobPageReq;
import com.duojuhe.coremodule.quartz.pojo.dto.job.QueryQuartzJobPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface QuartzJobMapper extends TkMapper<QuartzJob> {
    /**
     * 分页查询 根据条件查询定时任务list
     *
     * @return
     */
    List<QueryQuartzJobPageRes> queryQuartzJobResList(@Param("req") QueryQuartzJobPageReq req);

}