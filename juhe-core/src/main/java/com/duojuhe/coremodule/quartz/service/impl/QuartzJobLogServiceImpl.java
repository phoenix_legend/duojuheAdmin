package com.duojuhe.coremodule.quartz.service.impl;

import com.duojuhe.cache.SystemDictCache;
import com.duojuhe.common.annotation.DataScopeFilter;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.page.PageHelperUtil;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.quartz.mapper.QuartzJobLogMapper;
import com.duojuhe.coremodule.quartz.pojo.dto.joblog.HandleQuartzJobLogDictNameColorRes;
import com.duojuhe.coremodule.quartz.pojo.dto.joblog.QueryQuartzJobLogPageReq;
import com.duojuhe.coremodule.quartz.pojo.dto.joblog.QueryQuartzJobLogPageRes;
import com.duojuhe.coremodule.quartz.service.QuartzJobLogService;
import com.duojuhe.coremodule.system.entity.SystemDict;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class QuartzJobLogServiceImpl extends BaseService implements QuartzJobLogService {
    @Resource
    private SystemDictCache systemDictCache;
    @Resource
    private QuartzJobLogMapper quartzJobLogMapper;

    /**
     * 【分页查询】根据条件查询定时任务执行日志list
     * @param req
     * @return
     */
    @DataScopeFilter
    @Override
    public ServiceResult<PageResult<List<QueryQuartzJobLogPageRes>>> queryQuartzJobLogResList(QueryQuartzJobLogPageReq req) {
        PageHelperUtil.orderByAndStartPage(req, "createTime desc, logId desc");
        List<QueryQuartzJobLogPageRes> list = quartzJobLogMapper.queryQuartzJobLogResList(req);
        list.forEach(this::setHandleQuartzJobLogDictNameColor);
        return PageHelperUtil.returnServiceResult(req, list);
    }


    /*====================================私有方法开始================================================**/

    /**
     * 处理返回值的数据字典
     *
     * @param res
     */
    private void setHandleQuartzJobLogDictNameColor(HandleQuartzJobLogDictNameColorRes res) {
        if (res == null) {
            return;
        }
        SystemDict isSyncCode = systemDictCache.getSystemDictByDictCode(res.getIsSyncCode());
        res.setIsSyncName(isSyncCode.getDictName());
        res.setIsSyncColor(isSyncCode.getDictColor());

        SystemDict operationStatusCode = systemDictCache.getSystemDictByDictCode(res.getOperationStatusCode());
        res.setOperationStatusName(operationStatusCode.getDictName());
        res.setOperationStatusColor(operationStatusCode.getDictColor());
    }
    /*====================================私有方法end================================================**/
}
