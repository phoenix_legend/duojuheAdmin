package com.duojuhe.coremodule.quartz.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.quartz.pojo.dto.joblog.QueryQuartzJobLogPageReq;
import com.duojuhe.coremodule.quartz.pojo.dto.joblog.QueryQuartzJobLogPageRes;

import java.util.List;

public interface QuartzJobLogService {
    /**
     * 【分页查询】根据条件查询定时任务执行日志list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryQuartzJobLogPageRes>>> queryQuartzJobLogResList(QueryQuartzJobLogPageReq req);

}
