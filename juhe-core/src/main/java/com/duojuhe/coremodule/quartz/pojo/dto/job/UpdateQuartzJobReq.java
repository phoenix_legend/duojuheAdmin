package com.duojuhe.coremodule.quartz.pojo.dto.job;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * 修改调度作业
 *
 * @Date:2018/11/5
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateQuartzJobReq extends SaveQuartzJobReq {
    @ApiModelProperty(value = "任务ID", example = "1",required=true)
    @NotBlank(message = "任务ID不能为空")
    private String jobId;
}
