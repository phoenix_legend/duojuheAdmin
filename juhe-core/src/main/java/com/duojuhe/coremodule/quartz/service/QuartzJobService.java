package com.duojuhe.coremodule.quartz.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.quartz.pojo.dto.job.*;

import java.util.List;

public interface QuartzJobService {

    /**
     * 【分页查询】根据条件查询定时任务list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryQuartzJobPageRes>>> queryQuartzJobResList(QueryQuartzJobPageReq req);


    /**
     * 【保存】调度作业
     * @param req
     * @return
     */
    ServiceResult saveQuartzJob(SaveQuartzJobReq req);


    /**
     * 【修改】调度作业
     * @param req
     * @return
     */
    ServiceResult updateQuartzJob(UpdateQuartzJobReq req);

    /**
     * 【暂停调度任务】根据作业id暂停调度任务
     * @param req
     * @return
     */
    ServiceResult pauseQuartzJobByJobId(QuartzJobIdReq req);

    /**
     * 【恢复调度任务】根据作业id恢复调度任务
     * @param req
     * @return
     */
    ServiceResult resumeQuartzJobByJobId(QuartzJobIdReq req);

    /**
     * 【删除】根据作业id删除作业任务
     * @param req
     * @return
     */
    ServiceResult deleteQuartzJobByJobId(QuartzJobIdReq req);

    /**
     * 【执行一次】根据作业id执行一次
     * @param req
     * @return
     */
    ServiceResult runOnceJobByJobId(QuartzJobIdReq req);
}
