package com.duojuhe.coremodule.quartz.controller;

import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.quartz.pojo.dto.joblog.QueryQuartzJobLogPageReq;
import com.duojuhe.coremodule.quartz.pojo.dto.joblog.QueryQuartzJobLogPageRes;
import com.duojuhe.coremodule.quartz.service.QuartzJobLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/quartzJobLog/")
@Api(tags = "【定时任务执行日志】")
@Slf4j
public class QuartzJobLogController {
    @Resource
    private QuartzJobLogService quartzJobLogService;

    @ApiOperation(value = "【分页查询】根据条件查询定时任务执行日志list")
    @PostMapping(value = "queryQuartzJobLogResList")
    public ServiceResult<PageResult<List<QueryQuartzJobLogPageRes>>> queryQuartzJobLogResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryQuartzJobLogPageReq req) {
        return quartzJobLogService.queryQuartzJobLogResList(req);
    }
}
