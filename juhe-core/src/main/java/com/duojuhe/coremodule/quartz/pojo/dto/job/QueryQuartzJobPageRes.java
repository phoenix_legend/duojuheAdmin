package com.duojuhe.coremodule.quartz.pojo.dto.job;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ApiModel(value = "系统调度任务表 列表查询 响应")
public class QueryQuartzJobPageRes extends HandleQuartzJobDictNameColorRes {

    @ApiModelProperty(value = "主键id")
    private String jobId;

    @ApiModelProperty(value = "调度作业名称")
    private String jobName;

    @ApiModelProperty(value = "任务所在分组id")
    private String groupId;

    @ApiModelProperty(value = "执行表达式,例如:0 0/1 * * * ?")
    private String cronExpression;

    @ApiModelProperty(value = "要执行的任务类名称路径")
    private String classPath;

    @ApiModelProperty(value = "要执行的任务方法名称")
    private String methodName;

    @ApiModelProperty(value = "spring Id")
    private String springId;

    @ApiModelProperty(value = "创建人id")
    private String createUserId;

    @ApiModelProperty(value = "创建人")
    private String createUserName;

    @ApiModelProperty(value = "调度作业上次执行时间")
    @JsonFormat(pattern = DateUtils.DEFAULT_DATETIME_FORMAT, timezone = "GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date lastTime;

    @ApiModelProperty(value = "调度作业下次执行时间")
    @JsonFormat(pattern = DateUtils.DEFAULT_DATETIME_FORMAT, timezone = "GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date nextTime;

    @ApiModelProperty(value = "任务描述")
    private String description;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;
}