package com.duojuhe.coremodule.quartz.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("系统调度任务表")
@Table(name = "quartz_job")
public class QuartzJob extends BaseBean {
    @ApiModelProperty(value = "主键id", required = true)
    @Column(name = "job_id")
    @Id
    private String jobId;

    @ApiModelProperty(value = "调度作业名称", required = true)
    @Column(name = "job_name")
    private String jobName;

    @ApiModelProperty(value = "任务所在分组id", required = true)
    @Column(name = "group_id")
    private String groupId;

    @ApiModelProperty(value = "执行表达式,例如:0 0/1 * * * ?", required = true)
    @Column(name = "cron_expression")
    private String cronExpression;

    @ApiModelProperty(value = "是否同步执行NO-不是YES=是")
    @Column(name = "is_sync_code")
    private String isSyncCode;

    @ApiModelProperty(value = "要执行的任务类名称路径", required = true)
    @Column(name = "class_path")
    private String classPath;

    @ApiModelProperty(value = "要执行的任务方法名称", required = true)
    @Column(name = "method_name")
    private String methodName;

    @ApiModelProperty(value = "job运行状态NOT_RUNNING=不运行RUNNING=运行EXCEPTION=异常", required = true)
    @Column(name = "job_status_code")
    private String jobStatusCode;

    @ApiModelProperty(value = "spring beanId")
    @Column(name = "spring_id")
    private String springId;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "调度作业上次执行时间")
    @Column(name = "last_time")
    private Date lastTime;

    @ApiModelProperty(value = "调度作业下次执行时间")
    @Column(name = "next_time")
    private Date nextTime;

    @ApiModelProperty(value = "作业描述")
    @Column(name = "description")
    private String description;

    @ApiModelProperty(value = "是否内置系统任务NO-不是YES=是")
    @Column(name = "built_in")
    private String builtIn;

    @ApiModelProperty(value = "创建部门ID")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;
}