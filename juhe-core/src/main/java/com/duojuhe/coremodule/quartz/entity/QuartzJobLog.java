package com.duojuhe.coremodule.quartz.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("系统调度任务执行日志")
@Table(name = "quartz_job_log")
public class QuartzJobLog extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "log_id")
    @Id
    private String logId;

    @ApiModelProperty(value = "任务id", required = true)
    @Column(name = "job_id")
    private String jobId;

    @ApiModelProperty(value = "任务所在分组id")
    @Column(name = "group_id")
    private String groupId;

    @ApiModelProperty(value = "spring beanId")
    @Column(name = "spring_id")
    private String springId;

    @ApiModelProperty(value = "任务名称", required = true)
    @Column(name = "job_name")
    private String jobName;

    @ApiModelProperty(value = "任务表达式", required = true)
    @Column(name = "cron_expression")
    private String cronExpression;

    @ApiModelProperty(value = "是否同步执行", required = true)
    @Column(name = "is_sync_code")
    private String isSyncCode;

    @ApiModelProperty(value = "要执行的任务类名称路径", required = true)
    @Column(name = "class_path")
    private String classPath;

    @ApiModelProperty(value = "要执行的任务方法名称", required = true)
    @Column(name = "method_name")
    private String methodName;

    @ApiModelProperty(value = "日志时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "备注说明")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "执行状态，取数据字典")
    @Column(name = "operation_status_code")
    private String operationStatusCode;

    @ApiModelProperty(value = "执行开始时间")
    @Column(name = "start_time")
    private Date startTime;

    @ApiModelProperty(value = "执行停止时间")
    @Column(name = "stop_time")
    private Date stopTime;

    @ApiModelProperty(value = "描述说明")
    @Column(name = "description")
    private String description;


}