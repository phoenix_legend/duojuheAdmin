package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("会话聊天记录")
@Table(name = "chat_talk_record")
public class ChatTalkRecord extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "record_id")
    @Id
    private String recordId;

    @ApiModelProperty(value = "对话类型[1:私信;2:群聊;]")
    @Column(name = "talk_type")
    private Integer talkType;

    @ApiModelProperty(value = "消息类型[1:文本消息;2:文件消息;3:会话消息;4:代码消息;5:投票消息;6:群公告;7:好友申请;8:登录通知;9:入群消息/退群消息;]")
    @Column(name = "msg_type")
    private Integer msgType;

    @ApiModelProperty(value = "文件类型[1:图片;2:视频;3:文件]")
    @Column(name = "file_type")
    private Integer fileType;

    @ApiModelProperty(value = "发送者ID（-1:代表系统消息 >-1: 用户ID）")
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty(value = "接收者ID（用户ID 或 群ID）")
    @Column(name = "receiver_id")
    private String receiverId;

    @ApiModelProperty(value = "是否撤回消息[0:否;1:是]")
    @Column(name = "is_revoke")
    private Integer isRevoke;

    @ApiModelProperty(value = "是否重要消息[0:否;1:是;]")
    @Column(name = "is_mark")
    private Integer isMark;

    @ApiModelProperty(value = "是否已读[0:否;1:是;]")
    @Column(name = "is_read")
    private Integer isRead;

    @ApiModelProperty(value = "是否删除[0:否;1:是;]")
    @Column(name = "is_delete")
    private Integer isDelete;

    @ApiModelProperty(value = "引用消息ID")
    @Column(name = "quote_id")
    private String quoteId;

    @ApiModelProperty(value = "@好友 、 多个用英文逗号 “,” 拼接 (0:代表所有人)")
    @Column(name = "warn_users")
    private String warnUsers;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "发布时间")
    @Column(name = "release_time")
    private Date releaseTime;

    @ApiModelProperty(value = "创建者id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "文本消息 {@nickname@}")
    @Column(name = "content")
    private String content;


    @ApiModelProperty(value = "头像")
    @Column(name = "head_portrait")
    private String headPortrait;

    @ApiModelProperty(value = "真实姓名")
    @Column(name = "real_name")
    private String realName;

    @ApiModelProperty(value = "群名称", required = true)
    @Column(name = "group_name")
    private String groupName;

    @ApiModelProperty(value = "群头像")
    @Column(name = "group_avatar")
    private String groupAvatar;

}