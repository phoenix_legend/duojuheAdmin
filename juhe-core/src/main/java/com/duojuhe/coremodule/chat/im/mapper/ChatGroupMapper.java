package com.duojuhe.coremodule.chat.im.mapper;


import com.duojuhe.coremodule.chat.im.entity.ChatGroup;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.QueryChatGroupRes;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.QueryMyChatGroupPageReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.QueryMyChatGroupPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChatGroupMapper extends TkMapper<ChatGroup> {

    /**
     * 分页查询我的群组 我的用户群聊服务接口
     * @param req
     * @return
     */
    List<QueryMyChatGroupPageRes> queryMyChatGroupPageResList(@Param("req") QueryMyChatGroupPageReq req);


    /**
     * 根据分组id查询详情
     * @param groupId
     * @return
     */
    QueryChatGroupRes queryChatGroupResByGroupId(@Param("groupId") String groupId);
}