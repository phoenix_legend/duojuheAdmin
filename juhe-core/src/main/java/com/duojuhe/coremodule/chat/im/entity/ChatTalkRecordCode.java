package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("用户聊天记录_代码块消息表")
@Table(name = "chat_talk_record_code")
public class ChatTalkRecordCode extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "code_id")
    @Id
    private String codeId;

    @ApiModelProperty(value = "聊天记录id")
    @Column(name = "record_id")
    private String recordId;

    @ApiModelProperty(value = "用户id")
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty(value = "代码片段是否已删除[0:否;1:已删除]")
    @Column(name = "is_delete")
    private Integer isDelete;

    @ApiModelProperty(value = "创建用户id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "代码片段编程语言(如：php,java,python)")
    @Column(name = "code_lang")
    private String codeLang;

    @ApiModelProperty(value = "代码片段内容")
    @Column(name = "code_content")
    private String codeContent;
}