package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("用户聊天列表")
@Table(name = "chat_users_chat_list")
public class ChatUsersChatList extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "id")
    @Id
    private String id;

    @ApiModelProperty(value = "聊天类型[1:私信;2:群聊;0:游客]")
    @Column(name = "talk_type")
    private Integer talkType;

    @ApiModelProperty(value = "用户id", required = true)
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty(value = "接收者ID（用户ID 或 群ID）")
    @Column(name = "receiver_id")
    private String receiverId;

    @ApiModelProperty(value = "创建用户id", required = true)
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "消息免打扰[0:否;1:是;]")
    @Column(name = "is_disturb")
    private Integer isDisturb;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "是否删除[0:否;1:是;]")
    @Column(name = "is_delete")
    private Integer isDelete;

    @ApiModelProperty(value = "删除时间")
    @Column(name = "delete_time")
    private Date deleteTime;

    @ApiModelProperty(value = "是否置顶[0:否;1:是]")
    @Column(name = "is_top")
    private Integer isTop;

    @ApiModelProperty(value = "是否机器人[0:否;1:是;]")
    @Column(name = "is_robot")
    private Integer isRobot;

    @ApiModelProperty(value = "未读消息数量")
    @Column(name = "unread_num")
    private Integer unreadNum;

    @ApiModelProperty(value = "最后消息内容")
    @Column(name = "last_message")
    private String lastMessage;

    @ApiModelProperty(value = "最后发布时间")
    @Column(name = "last_release_time")
    private Date lastReleaseTime;

    @ApiModelProperty(value = "最后消息记录ID")
    @Column(name = "last_record_id")
    private String lastRecordId;


}