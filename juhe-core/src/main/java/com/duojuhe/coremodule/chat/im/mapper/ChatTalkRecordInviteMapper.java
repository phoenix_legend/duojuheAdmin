package com.duojuhe.coremodule.chat.im.mapper;

import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecordInvite;
import com.tkmapper.TkMapper;

public interface ChatTalkRecordInviteMapper extends TkMapper<ChatTalkRecordInvite> {

}