package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("系统表情图标项")
@Table(name = "chat_emoticon_item")
public class ChatEmoticonItem extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "item_id")
    @Id
    private String itemId;

    @ApiModelProperty(value = "表情类别id")
    @Column(name = "category_id")
    private String categoryId;

    @ApiModelProperty(value = "表情地址", required = true)
    @Column(name = "emoticon_url")
    private String emoticonUrl;

    @ApiModelProperty(value = "文件后缀")
    @Column(name = "file_suffix")
    private String fileSuffix;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "表情说明")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "是否用户收藏0否1是")
    @Column(name = "is_collect")
    private Integer isCollect;

    @ApiModelProperty(value = "创建用户id", required = true)
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "文件类型[1:图片;2:视频;3:文件]")
    @Column(name = "file_type")
    private Integer fileType;

    @ApiModelProperty(value = "原文件名")
    @Column(name = "original_name")
    private String originalName;


    @ApiModelProperty(value = "文件大小（单位字节）")
    @Column(name = "file_size")
    private Long fileSize;
}

