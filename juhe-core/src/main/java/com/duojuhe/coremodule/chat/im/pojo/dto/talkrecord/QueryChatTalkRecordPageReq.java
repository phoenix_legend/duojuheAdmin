package com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord;

import com.duojuhe.common.bean.PageHead;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryChatTalkRecordPageReq extends PageHead {
    @ApiModelProperty(value = "接收者ID", example = "1",required=true)
    @NotBlank(message = "接收者ID不能为空")
    private String receiverId;

    @ApiModelProperty(value = "聊天类型[1:私信;2:群聊]", example = "1",required=true)
    @NotNull(message = "聊天类型不能为空")
    @Range(min = 1, max = 2, message = "聊天类型参数不正确")
    private Integer talkType;

    @ApiModelProperty(value = "消息类型[1:文本消息;2:文件消息;3:会话消息;4:代码消息;5:投票消息;6:群公告;7:好友申请;8:登录通知;9:入群消息/退群消息;]")
    private Integer msgType;

    @ApiModelProperty(value = "内容搜索词", example = "1")
    private String content;

}
