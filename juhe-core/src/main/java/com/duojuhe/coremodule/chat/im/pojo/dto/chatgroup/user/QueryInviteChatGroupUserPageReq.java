package com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user;

import com.duojuhe.common.bean.PageHead;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryInviteChatGroupUserPageReq extends PageHead {
    @ApiModelProperty(value = "群组id，留空表示查询所有", example = "1")
    private String groupId;
}
