package com.duojuhe.coremodule.chat.im.pojo.dto.chatlist;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryMyChatUsersChatListPageRes extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String id;

    @ApiModelProperty(value = "聊天类型[1:私信;2:群聊;0:游客]")
    private Integer talkType;

    @ApiModelProperty(value = "用户id", required = true)
    private String userId;

    @ApiModelProperty(value = "接收者ID（用户ID 或 群ID）")
    private String receiverId;


    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    private Date updateTime;

    @ApiModelProperty(value = "消息免打扰[0:否;1:是;]")
    private Integer isDisturb;

    @ApiModelProperty(value = "是否删除[0:否;1:是;]")
    private Integer isDelete;

    @ApiModelProperty(value = "是否置顶[0:否;1:是]")
    private Integer isTop;

    @ApiModelProperty(value = "是否机器人[0:否;1:是;]")
    private Integer isRobot;

    @ApiModelProperty(value = "是否在线[0:否;1:是;]")
    private Integer isOnline;

    @ApiModelProperty(value = "头像")
    private String headPortrait;

    @ApiModelProperty(value = "姓名")
    private String realName;

    @ApiModelProperty(value = "好友备注")
    private String friendRemark;

    @ApiModelProperty(value = "未读消息数量")
    private Integer unreadNum;

    @ApiModelProperty(value = "最后消息记录")
    private String msgText;

    @ApiModelProperty(value = "最后发布时间")
    private Date lastReleaseTime;
}
