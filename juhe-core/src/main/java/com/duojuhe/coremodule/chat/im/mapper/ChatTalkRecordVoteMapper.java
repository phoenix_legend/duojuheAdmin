package com.duojuhe.coremodule.chat.im.mapper;

import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecordVote;
import com.tkmapper.TkMapper;

public interface ChatTalkRecordVoteMapper extends TkMapper<ChatTalkRecordVote> {

}