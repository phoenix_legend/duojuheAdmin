package com.duojuhe.coremodule.chat.im.mapper;


import com.duojuhe.coremodule.chat.im.entity.ChatEmoticonItem;
import com.duojuhe.coremodule.chat.im.pojo.dto.emoticon.item.ChatEmoticonItemRes;
import com.duojuhe.coremodule.chat.im.pojo.dto.emoticon.item.QueryChatUserEmoticonItemPageReq;
import com.duojuhe.common.bean.DataScopeFilterBean;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChatEmoticonItemMapper extends TkMapper<ChatEmoticonItem> {
    /**
     * 查询用户表情
     * @param req
     * @return
     */
    List<ChatEmoticonItemRes> queryChatUserEmoticonItemPageResList(@Param("req") QueryChatUserEmoticonItemPageReq req);


    /**
     * 查询用户表情
     * @param req
     * @return
     */
    List<ChatEmoticonItemRes> queryMyChatEmoticonItemRes(@Param("req") DataScopeFilterBean req);
}