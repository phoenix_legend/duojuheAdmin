package com.duojuhe.coremodule.chat.im.pojo.dto.chatlist;

import com.duojuhe.common.bean.PageHead;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryMyChatUsersChatListPageReq extends PageHead {
    @ApiModelProperty(value = "会话列表接收者名称")
    private String chatListReceiverName;
}
