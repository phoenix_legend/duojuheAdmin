package com.duojuhe.coremodule.chat.im.service;

import com.duojuhe.coremodule.chat.im.pojo.dto.usersfriends.*;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;

import java.util.List;

public interface ChatUsersFriendsService {
    /**
     * 解除好友关系 解除好友关系服务接口
     * @param req
     * @return
     */
    ServiceResult removeChatFriendByFriendId (ChatUsersFriendsIdReq req);


    /**
     * 修改好友备注 修改好友备注服务接口
     * @param req
     * @return
     */
    ServiceResult updateFriendRemark (UpdateFriendRemarkReq req);


    /**
     * 获取我的好友list 获取好友列表服务接口
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryMyChatUsersFriendsPageRes>>> queryMyChatUsersFriendsPageResList(QueryMyChatUsersFriendsPageReq req);

    /**
     * 根据手机号码精确搜索 搜索联系人 搜索联系人
     * @param req
     * @return
     */
    ServiceResult<String> querySearchFriendsUserIdBySearchKeyWords(SearchFriendsReq req);


    /**
     * 根据用户id查询用户详情
     * @param userId
     * @return
     */
    ServiceResult<QueryChatUsersDetailRes> queryChatUsersDetailResByUserId(String userId);

}
