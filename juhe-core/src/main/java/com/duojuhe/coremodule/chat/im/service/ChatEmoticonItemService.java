package com.duojuhe.coremodule.chat.im.service;

import com.duojuhe.coremodule.chat.im.pojo.dto.emoticon.item.*;
import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ChatEmoticonItemService {
    /**
     *【查询用户表情包服务接口】分页查询用户表情包服务接口
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryChatUserEmoticonItemPageRes>>> queryChatUserEmoticonItemPageResList(QueryChatUserEmoticonItemPageReq req);

    /**
     *【查询用户表情包服务接口】查询用户表情
     * @param dataScope
     * @return
     */
    ServiceResult<QueryMyChatEmoticonItemRes> queryMyChatEmoticonItemRes(DataScopeFilterBean dataScope);




    /**
     *【查询系统表情包服务接口】分页查询系统表情包服务接口
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryChatSystemEmoticonItemPageRes>>> queryChatSystemEmoticonItemPageResList(QueryChatSystemEmoticonItemPageReq req);



    /**
     * 收藏表情包服务接口
     * @param req
     * @return
     */
    ServiceResult collectChatEmoticonItem(CollectChatEmoticonItemReq req);



    /**
     * 保存用户表情包
     * @param req
     * @return
     */
    ServiceResult saveChatEmoticonItem(SaveChatEmoticonItemReq req);


    /**
     *【自定义表情】新增自定义表情
     * @param file
     * @return
     */
    ServiceResult<String> uploadDiyChatEmoticonItem(MultipartFile file);


}
