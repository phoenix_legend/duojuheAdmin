package com.duojuhe.coremodule.chat.im.mapper;

import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecordFile;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChatTalkRecordFileMapper extends TkMapper<ChatTalkRecordFile> {
    /**
     * 根据消息id 集合 查询关联的附件
     * @param recordIdList
     * @return
     */
    List<ChatTalkRecordFile> queryChatTalkRecordFileListByRecordIdList(@Param("recordIdList") List<String> recordIdList);


    /**
     * 根据fileId集合 查询关联的附件
     * @param fileIdList
     * @return
     */
    List<ChatTalkRecordFile> queryChatTalkRecordFileListByFileIdList(@Param("fileIdList") List<String> fileIdList);

}