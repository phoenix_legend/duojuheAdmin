package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("群通知公告")
@Table(name = "chat_group_notice")
public class ChatGroupNotice extends BaseBean {
    @ApiModelProperty(value = "群通知id", required = true)
    @Column(name = "notice_id")
    @Id
    private String noticeId;

    @ApiModelProperty(value = "群id", required = true)
    @Column(name = "group_id")
    private String groupId;

    @ApiModelProperty(value = "创建者用户ID", required = true)
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "通知标题")
    @Column(name = "notice_title")
    private String noticeTitle;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "通知内容")
    @Column(name = "content")
    private String content;
}