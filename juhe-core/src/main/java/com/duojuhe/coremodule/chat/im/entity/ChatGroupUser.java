package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("聊天群组成员表")
@Table(name = "chat_group_user")
public class ChatGroupUser extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "id")
    @Id
    private String id;

    @ApiModelProperty(value = "群组id", required = true)
    @Column(name = "group_id")
    private String groupId;

    @ApiModelProperty(value = "用户id", required = true)
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty(value = "成员属性[0:普通成员;1:管理员;2:群主;]", required = true)
    @Column(name = "leader")
    private Integer leader;

    @ApiModelProperty(value = "是否禁言[0:否;1:是;]", required = true)
    @Column(name = "is_mute")
    private Integer isMute;

    @ApiModelProperty(value = "是否退群[0:否;1:是;]", required = true)
    @Column(name = "is_quit")
    private Integer isQuit;

    @ApiModelProperty(value = "用户组名片")
    @Column(name = "visit_card")
    private String visitCard;

    @ApiModelProperty(value = "入组时间")
    @Column(name = "join_group_time")
    private Date joinGroupTime;

    @ApiModelProperty(value = "退组时间")
    @Column(name = "quit_group_time")
    private Date quitGroupTime;
}