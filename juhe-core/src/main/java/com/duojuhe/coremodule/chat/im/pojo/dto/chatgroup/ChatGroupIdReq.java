package com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChatGroupIdReq extends BaseBean {
    @ApiModelProperty(value = "群组id", example = "1",required=true)
    @NotBlank(message = "群组ID不能为空")
    private String groupId;
}
