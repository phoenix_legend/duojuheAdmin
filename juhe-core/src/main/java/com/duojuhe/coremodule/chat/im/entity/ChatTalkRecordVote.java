package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("聊天对话记录（投票消息表）")
@Table(name = "chat_talk_record_vote")
public class ChatTalkRecordVote extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "vote_id")
    @Id
    private String voteId;

    @ApiModelProperty(value = "聊天记录id")
    @Column(name = "record_id")
    private String recordId;

    @ApiModelProperty(value = "用户id")
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty(value = "创建用户id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "投票标题")
    @Column(name = "vote_title")
    private String voteTitle;

    @ApiModelProperty(value = "答题模式[0:单选;1:多选;]")
    @Column(name = "answer_mode")
    private Integer answerMode;

    @ApiModelProperty(value = "应答人数")
    @Column(name = "answer_num")
    private Integer answerNum;

    @ApiModelProperty(value = "已答人数")
    @Column(name = "answered_num")
    private Integer answeredNum;

    @ApiModelProperty(value = "投票状态[0:投票中;1:已完成;]")
    @Column(name = "vote_status")
    private Integer voteStatus;

    @ApiModelProperty(value = "答题选项逗号分割")
    @Column(name = "answer_option")
    private String answerOption;

    @ApiModelProperty(value = "答题选项内容")
    @Column(name = "answer_option_content")
    private String answerOptionContent;
}