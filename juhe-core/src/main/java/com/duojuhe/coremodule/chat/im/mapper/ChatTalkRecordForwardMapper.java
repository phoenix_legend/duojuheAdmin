package com.duojuhe.coremodule.chat.im.mapper;

import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecordForward;
import com.tkmapper.TkMapper;

public interface ChatTalkRecordForwardMapper extends TkMapper<ChatTalkRecordForward> {

}