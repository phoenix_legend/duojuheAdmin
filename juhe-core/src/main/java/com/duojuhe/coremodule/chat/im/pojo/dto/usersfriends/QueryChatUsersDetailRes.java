package com.duojuhe.coremodule.chat.im.pojo.dto.usersfriends;

import com.duojuhe.common.annotation.FieldFormatting;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.enums.FormattingTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryChatUsersDetailRes extends BaseBean {
    @ApiModelProperty(value = "主键字段", required = true)
    private String userId;

    @ApiModelProperty(value = "头像")
    private String headPortrait;

    @ApiModelProperty(value = "登录名唯一", required = true)
    private String loginName;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "手机号码唯一", required = true)
    @FieldFormatting(formattingType = FormattingTypeEnum.MOBILE_PHONE)
    private String mobileNumber;

    @ApiModelProperty(value = "性别")
    private String genderCode;

    @ApiModelProperty(value = "座右铭")
    private String motto;

    @ApiModelProperty(value = "朋友关系[0:本人;1:陌生人;2:朋友;]")
    private Integer friendStatus;

    @ApiModelProperty(value = "是否好友申请中0否1是")
    private Integer friendApply;

    @ApiModelProperty(value = "性别中文")
    private String genderName;

    @ApiModelProperty(value = "好友备注")
    private String friendRemark;

}
