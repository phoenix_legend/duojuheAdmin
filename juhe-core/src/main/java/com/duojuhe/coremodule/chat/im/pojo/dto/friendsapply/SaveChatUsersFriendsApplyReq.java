package com.duojuhe.coremodule.chat.im.pojo.dto.friendsapply;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveChatUsersFriendsApplyReq extends BaseBean {
    @ApiModelProperty(value = "朋友id", example = "1",required=true)
    @NotBlank(message = "朋友ID不能为空")
    private String friendId;

    @ApiModelProperty(value = "申请备注说明", example = "1",required=true)
    @NotBlank(message = "申请说明不能为空")
    @Length(max = 255, message = "申请说明不得超过{max}位字符")
    private String applyDescription;
}
