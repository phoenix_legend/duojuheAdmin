package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("聊天表情类别")
@Table(name = "chat_emoticon_category")
public class ChatEmoticonCategory extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "category_id")
    @Id
    private String categoryId;

    @ApiModelProperty(value = "类别名称", required = true)
    @Column(name = "category_name")
    private String categoryName;

    @ApiModelProperty(value = "类别图标")
    @Column(name = "category_icon")
    private String categoryIcon;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;
}