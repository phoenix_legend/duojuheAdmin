package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("用户聊天记录_删除记录表")
@Table(name = "chat_talk_record_delete")
public class ChatTalkRecordDelete extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "delete_id")
    @Id
    private String deleteId;

    @ApiModelProperty(value = "聊天记录ID")
    @Column(name = "record_id")
    private String recordId;

    @ApiModelProperty(value = "用户id")
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty(value = "创建用户id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;
}