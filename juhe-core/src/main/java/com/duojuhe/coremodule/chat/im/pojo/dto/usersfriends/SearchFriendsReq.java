package com.duojuhe.coremodule.chat.im.pojo.dto.usersfriends;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SearchFriendsReq extends BaseBean {
    @ApiModelProperty(value = "搜索朋友关键词", example = "1",required=true)
    @NotBlank(message = "搜索朋友关键词不能为空")
    private String searchKeyWords;
}
