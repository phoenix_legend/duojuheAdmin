package com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user;

import com.duojuhe.common.annotation.FieldFormatting;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.enums.FormattingTypeEnum;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryChatGroupUserPageRes extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String id;

    @ApiModelProperty(value = "群组id", required = true)
    private String groupId;

    @ApiModelProperty(value = "用户id", required = true)
    private String userId;

    @ApiModelProperty(value = "成员属性[0:普通成员;1:管理员;2:群主;]", required = true)
    private Integer leader;

    @ApiModelProperty(value = "是否禁言[0:否;1:是;]", required = true)
    private Integer isMute;

    @ApiModelProperty(value = "是否退群[0:否;1:是;]", required = true)
    private Integer isQuit;

    @ApiModelProperty(value = "用户组名片")
    private String visitCard;

    @ApiModelProperty(value = "入组时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    private Date joinGroupTime;

    @ApiModelProperty(value = "退组时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    private Date quitGroupTime;

    @ApiModelProperty(value = "头像")
    private String headPortrait;

    @ApiModelProperty(value = "登录名唯一", required = true)
    private String loginName;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "手机号码唯一", required = true)
    @FieldFormatting(formattingType = FormattingTypeEnum.MOBILE_PHONE)
    private String mobileNumber;

    @ApiModelProperty(value = "性别")
    private String genderCode;

    @ApiModelProperty(value = "性别")
    private String genderName;

    @ApiModelProperty(value = "座右铭")
    private String motto;
}
