package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("用户好友关系表")
@Table(name = "chat_users_friends")
public class ChatUsersFriends extends BaseBean {
    @ApiModelProperty(value = "主键 当前用户id和好友id的MD5值", required = true)
    @Column(name = "id")
    @Id
    private String id;

    @ApiModelProperty(value = "用户id", required = true)
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty(value = "朋友id", required = true)
    @Column(name = "friend_id")
    private String friendId;

    @ApiModelProperty(value = "朋友备注")
    @Column(name = "friend_remark")
    private String friendRemark;

    @ApiModelProperty(value = "创建时间，成为好友时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "解除好友时间")
    @Column(name = "unfriend_time")
    private Date unfriendTime;

    @ApiModelProperty(value = "创建用户id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "邀请方向 主动 被动")
    @Column(name = "direction_code")
    private String directionCode;

    @ApiModelProperty(value = "是否好友状态[0:否;1:是;]")
    @Column(name = "is_friend")
    private Integer isFriend;
}