package com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.sendtalk;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SendTalkReq extends BaseBean {
    @ApiModelProperty(value = "接收者ID", example = "1",required=true)
    @NotBlank(message = "接收者ID不能为空")
    private String receiverId;

    @ApiModelProperty(value = "聊天类型[1:私信;2:群聊]", example = "1",required=true)
    @NotNull(message = "聊天类型不能为空")
    @Range(min = 1, max = 2, message = "聊天类型参数不正确")
    private Integer talkType;
}
