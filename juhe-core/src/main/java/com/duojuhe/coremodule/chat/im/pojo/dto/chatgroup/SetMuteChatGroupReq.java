package com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SetMuteChatGroupReq extends ChatGroupIdReq {
    @ApiModelProperty(value = "全员禁言[0:否;1:是;]", example = "1",required=true)
    @NotNull(message = "全员禁言扰不能为空")
    @Range(min = 0, max = 1, message = "全员禁言参数不正确")
    private Integer isMute;
}
