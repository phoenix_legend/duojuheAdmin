package com.duojuhe.coremodule.chat.im.mapper;


import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecordVoteAnswer;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChatTalkRecordVoteAnswerMapper extends TkMapper<ChatTalkRecordVoteAnswer> {
    /**
     * 根据投票id查询关联的投票信息
     * @param voteId
     * @return
     */
    List<ChatTalkRecordVoteAnswer> queryChatTalkRecordVoteAnswerByVoteId(@Param("voteId") String voteId);

}