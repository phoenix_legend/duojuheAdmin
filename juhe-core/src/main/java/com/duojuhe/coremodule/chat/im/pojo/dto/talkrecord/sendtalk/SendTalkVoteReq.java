package com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.sendtalk;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SendTalkVoteReq extends SendTalkReq {
    @ApiModelProperty(value = "投票标题",required=true)
    @Length(max = 50, message = "群组名称不得超过{max}位字符")
    private String voteTitle;

    @ApiModelProperty(value = "答题模式[0:单选;1:多选;]")
    @NotNull(message = "答题模式不能为空")
    @Range(min = 0, max = 1, message = "答题模式参数不正确")
    private Integer answerMode;

    @ApiModelProperty(value = "答题选项",required=true)
    @NotEmpty(message = "答题选项集合不能为空")
    @Valid
    @NotNull(message = "答题选项集合不能为空")
    @Size(min = 1,max = 26, message = "答题选项集合不能为空,最少1项，最多10项")
    private List<String> answerOptionList;

}
