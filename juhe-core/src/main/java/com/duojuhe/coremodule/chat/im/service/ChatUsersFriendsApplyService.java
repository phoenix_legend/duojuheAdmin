package com.duojuhe.coremodule.chat.im.service;

import com.duojuhe.coremodule.chat.im.pojo.dto.friendsapply.*;
import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;

import java.util.List;

public interface ChatUsersFriendsApplyService {

    /**
     *【好友申请服务接口】分页查询好友申请服务接口
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryMyChatUsersFriendsApplyPageRes>>> queryMyChatUsersFriendsApplyPageResList(QueryMyChatUsersFriendsApplyPageReq req);


    /**
     * 好友申请服务接口
     * @param req
     * @return
     */
    ServiceResult saveChatUsersFriendsApply (SaveChatUsersFriendsApplyReq req);

    /**
     * 同意 处理好友申请服务接口
     * @param req
     * @return
     */
    ServiceResult acceptChatUsersFriendsApply (AcceptChatUsersFriendsApplyReq req);


    /**
     * 拒绝 处理好友申请服务接口
     * @param req
     * @return
     */
    ServiceResult declineChatUsersFriendsApply (DeclineChatUsersFriendsApplyReq req);


    /**
     * 查询好友申请未读数量服务接口
     * @param
     * @return
     */
    ServiceResult<Integer> queryMyChatUsersFindFriendApplyNum(DataScopeFilterBean dataScope);
}
