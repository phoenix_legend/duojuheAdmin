package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("用户聊天记录_文件消息表")
@Table(name = "chat_talk_record_file")
public class ChatTalkRecordFile extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "file_id")
    @Id
    private String fileId;

    @ApiModelProperty(value = "消息记录id")
    @Column(name = "record_id")
    private String recordId;

    @ApiModelProperty(value = "用户id")
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty(value = "文件类型[1:图片;2:视频;3:文件]")
    @Column(name = "file_type")
    private Integer fileType;

    @ApiModelProperty(value = "文件路径")
    @Column(name = "file_url")
    private String fileUrl;


    @ApiModelProperty(value = "原文件名")
    @Column(name = "original_name")
    private String originalName;

    @ApiModelProperty(value = "文件后缀")
    @Column(name = "file_suffix")
    private String fileSuffix;

    @ApiModelProperty(value = "文件是否已删除[0:否;1:已删除]")
    @Column(name = "is_delete")
    private Integer isDelete;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "创建者id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "文件大小（单位字节）")
    @Column(name = "file_size")
    private Long fileSize;
}