package com.duojuhe.coremodule.chat.im.pojo.dto.emoticon.item;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChatEmoticonItemIdReq extends BaseBean {
    @ApiModelProperty(value = "记录ID", example = "1",required=true)
    @NotBlank(message = "记录ID不能为空")
    private String itemId;
}
