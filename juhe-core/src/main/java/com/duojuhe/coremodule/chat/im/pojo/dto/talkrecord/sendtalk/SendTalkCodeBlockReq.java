package com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.sendtalk;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SendTalkCodeBlockReq extends SendTalkReq {
    @ApiModelProperty(value = "代码片段内容", example = "1",required=true)
    @NotBlank(message = "代码片段内容不能为空")
    private String code;

    @ApiModelProperty(value = "编程语言", example = "1",required=true)
    @NotBlank(message = "编程语言不能为空")
    @Length(max = 50, message = "编程语言不得超过{max}位字符")
    private String lang;
}
