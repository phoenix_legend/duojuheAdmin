package com.duojuhe.coremodule.chat.im.mapper;


import com.duojuhe.coremodule.chat.im.entity.ChatGroupNotice;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.notice.QueryChatGroupNoticePageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChatGroupNoticeMapper extends TkMapper<ChatGroupNotice> {
    /**
     * 根据群组id查询通知list
     * @param groupId
     * @return
     */
    List<QueryChatGroupNoticePageRes> queryChatGroupNoticePageResListByGroupId(@Param("groupId") String groupId);
}