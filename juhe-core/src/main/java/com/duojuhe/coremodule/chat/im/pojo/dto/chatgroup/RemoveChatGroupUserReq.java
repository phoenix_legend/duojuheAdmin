package com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RemoveChatGroupUserReq extends ChatGroupIdReq{
    @ApiModelProperty(value = "用户ID集合", required = true)
    @NotEmpty(message = "用户ID集合不能为空")
    @Valid
    @NotNull(message = "用户ID集合不能为空")
    @Size(min = 1,max = 10,message = "一次删除用户至少1个,最多10个")
    private List<String> userIdList;
}
