package com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord;

import com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.sendtalk.SendTalkReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BatchDeleteRecordIdListReq extends SendTalkReq {
    @ApiModelProperty(value = "消息记录ID集合", required = true)
    @NotEmpty(message = "消息记录ID集合不能为空")
    @Valid
    @NotNull(message = "消息记录ID集合不能为空")
    @Size(min = 1,max = 10,message = "一次转发消息至少1条,最多10条")
    private List<String> recordIdList;
}
