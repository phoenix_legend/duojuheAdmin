package com.duojuhe.coremodule.chat.im.pojo.dto.emoticon.item;

import com.duojuhe.common.bean.PageHead;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryChatUserEmoticonItemPageReq extends PageHead {
    @ApiModelProperty(value = "表情说明", required = true)
    private String remark;
}
