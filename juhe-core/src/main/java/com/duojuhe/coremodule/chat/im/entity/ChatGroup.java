package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("聊天群组表")
@Table(name = "chat_group")
public class ChatGroup extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "group_id")
    @Id
    private String groupId;

    @ApiModelProperty(value = "群名称", required = true)
    @Column(name = "group_name")
    private String groupName;

    @ApiModelProperty(value = "群介绍")
    @Column(name = "group_introduce")
    private String groupIntroduce;

    @ApiModelProperty(value = "群头像")
    @Column(name = "group_avatar")
    private String groupAvatar;

    @ApiModelProperty(value = "最大人数限制")
    @Column(name = "max_people_number")
    private Integer maxPeopleNumber;

    @ApiModelProperty(value = "是否公开可见[0:否;1:是;]")
    @Column(name = "is_overt")
    private Integer isOvert;

    @ApiModelProperty(value = "是否全员禁言 [0:否;1:是;]，提示:不包含群主或管理员")
    @Column(name = "is_mute")
    private Integer isMute;

    @ApiModelProperty(value = "是否已解散[0:否;1:是;]")
    @Column(name = "is_dismiss")
    private Integer isDismiss;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "解散时间")
    @Column(name = "dismissed_time")
    private Date dismissedTime;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;
}