package com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user;

import com.duojuhe.common.annotation.FieldFormatting;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.enums.FormattingTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryInviteChatGroupUserPageRes extends BaseBean {
    @ApiModelProperty(value = "用户id", required = true)
    private String userId;

    @ApiModelProperty(value = "头像")
    private String headPortrait;

    @ApiModelProperty(value = "登录名唯一", required = true)
    private String loginName;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "手机号码唯一", required = true)
    @FieldFormatting(formattingType = FormattingTypeEnum.MOBILE_PHONE)
    private String mobileNumber;

    @ApiModelProperty(value = "性别")
    private String genderCode;

    @ApiModelProperty(value = "性别")
    private String genderName;

    @ApiModelProperty(value = "座右铭")
    private String motto;

    @ApiModelProperty(value = "朋友备注")
    private String friendRemark;
}
