package com.duojuhe.coremodule.chat.im.pojo.dto.friendsapply;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryMyChatUsersFriendsApplyPageRes extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String applyId;

    @ApiModelProperty(value = "申请人ID", required = true)
    private String userId;

    @ApiModelProperty(value = "被申请人id", required = true)
    private String friendId;

    @ApiModelProperty(value = "创建人id")
    private String createUserId;

    @ApiModelProperty(value = "申请状态[0:申请中;1:是同意，2拒绝;]")
    private Integer applyStatus;

    @ApiModelProperty(value = "备注信息")
    private String remark;

    @ApiModelProperty(value = "申请人备注信息")
    private String applyDescription;

    @ApiModelProperty(value = "申请创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "处理时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date handleTime;

    @ApiModelProperty(value = "姓名", example = "1")
    private String realName;

    @ApiModelProperty(value = "登录名", example = "1")
    private String loginName;

    @ApiModelProperty(value = "手机号码", example = "1")
    private String mobileNumber;

    @ApiModelProperty(value = "头像", example = "1")
    private String headPortrait;
}
