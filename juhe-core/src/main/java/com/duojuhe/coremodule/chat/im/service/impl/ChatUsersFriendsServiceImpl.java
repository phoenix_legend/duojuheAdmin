package com.duojuhe.coremodule.chat.im.service.impl;

import com.duojuhe.cache.SystemDictCache;
import com.duojuhe.common.constant.DataScopeConstants;
import com.duojuhe.coremodule.chat.im.entity.ChatUsersChatList;
import com.duojuhe.coremodule.chat.im.entity.ChatUsersFriends;
import com.duojuhe.coremodule.chat.im.entity.ChatUsersFriendsApply;
import com.duojuhe.coremodule.chat.im.mapper.ChatUsersChatListMapper;
import com.duojuhe.coremodule.chat.im.mapper.ChatUsersFriendsApplyMapper;
import com.duojuhe.coremodule.chat.im.mapper.ChatUsersFriendsMapper;
import com.duojuhe.coremodule.chat.im.pojo.dto.usersfriends.*;
import com.duojuhe.coremodule.chat.im.service.ChatUsersFriendsService;
import com.duojuhe.common.annotation.DataScopeFilter;
import com.duojuhe.common.bean.UserTokenInfoVo;
import com.duojuhe.common.enums.chat.ImChatEnum;
import com.duojuhe.common.exception.base.DuoJuHeException;
import com.duojuhe.common.result.ErrorCodes;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.encryption.md5.MD5Util;
import com.duojuhe.common.utils.page.PageHelperUtil;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.system.entity.SystemDict;
import com.duojuhe.coremodule.system.entity.SystemUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class ChatUsersFriendsServiceImpl extends BaseService implements ChatUsersFriendsService {
    @Resource
    private SystemDictCache systemDictCache;
    @Resource
    private ChatUsersFriendsMapper chatUsersFriendsMapper;
    @Resource
    private ChatUsersFriendsApplyMapper chatUsersFriendsApplyMapper;
    @Resource
    private ChatUsersChatListMapper chatUsersChatListMapper;

    /**
     * 解除好友关系
     * @param req
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public ServiceResult removeChatFriendByFriendId(ChatUsersFriendsIdReq req) {
        //好友id
        String friendId = req.getFriendId();
        //当前时间
        Date nowDate = new Date();
        //是标识
        Integer yes = ImChatEnum.IM_YES_NO.YES.getKey();
        //当前登录人
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //主动方向 主键ID 当前用户id和好友id的MD5值
        String formId = MD5Util.getMD532(userTokenInfoVo.getUserId()+friendId);
        //检查好友信息是否存在
        checkChatUsersFriendsById(formId);
        //更新主动好友信息
        ChatUsersFriends formChatUsersFriends = new ChatUsersFriends();
        formChatUsersFriends.setId(formId);
        formChatUsersFriends.setIsFriend(ImChatEnum.IM_YES_NO.NO.getKey());
        formChatUsersFriends.setUnfriendTime(nowDate);
        chatUsersFriendsMapper.updateByPrimaryKeySelective(formChatUsersFriends);
        //更新会话列表
        ChatUsersChatList formChatUsersChatList = new ChatUsersChatList();
        formChatUsersChatList.setId(formId);
        formChatUsersChatList.setIsDelete(yes);
        formChatUsersChatList.setIsDisturb(yes);
        formChatUsersChatList.setDeleteTime(nowDate);
        chatUsersChatListMapper.updateByPrimaryKeySelective(formChatUsersChatList);
        //删除申请信息
        chatUsersFriendsApplyMapper.deleteByPrimaryKey(formId);

        //被动方向
        //被动方向 主键ID 当前用户id和好友id的MD5值
        String toId = MD5Util.getMD532(friendId+userTokenInfoVo.getUserId());
        //检查好友信息是否存在
        checkChatUsersFriendsById(toId);
        //更新被动方向好友信息
        ChatUsersFriends toChatUsersFriends = new ChatUsersFriends();
        toChatUsersFriends.setId(toId);
        toChatUsersFriends.setIsFriend(ImChatEnum.IM_YES_NO.NO.getKey());
        toChatUsersFriends.setUnfriendTime(nowDate);
        chatUsersFriendsMapper.updateByPrimaryKeySelective(toChatUsersFriends);
        //更新会话列表
        ChatUsersChatList toChatUsersChatList = new ChatUsersChatList();
        toChatUsersChatList.setId(toId);
        toChatUsersChatList.setIsDelete(yes);
        toChatUsersChatList.setIsDisturb(yes);
        toChatUsersChatList.setDeleteTime(nowDate);
        chatUsersChatListMapper.updateByPrimaryKeySelective(toChatUsersChatList);
        //删除申请信息
        chatUsersFriendsApplyMapper.deleteByPrimaryKey(toId);
        return ServiceResult.ok(ErrorCodes.SUCCESS);
    }

    /**
     * 修改好友备注
     * @param req
     * @return
     */
    @Override
    public ServiceResult updateFriendRemark(UpdateFriendRemarkReq req) {
        //当前登录人
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        //主键ID 当前用户id和好友id的MD5值
        String id = MD5Util.getMD532(userTokenInfoVo.getUserId()+req.getFriendId());
        //检查好友信息是否存在
        ChatUsersFriends chatUsersFriendsOld = checkChatUsersFriendsById(id);
        if (!ImChatEnum.IM_YES_NO.YES.getKey().equals(chatUsersFriendsOld.getIsFriend())){
            return ServiceResult.fail(ErrorCodes.NOT_FRIEND_ERROR);
        }
        //更新好友信息
        ChatUsersFriends chatUsersFriends = new ChatUsersFriends();
        chatUsersFriends.setId(id);
        chatUsersFriends.setFriendRemark(req.getFriendRemark());
        chatUsersFriendsMapper.updateByPrimaryKeySelective(chatUsersFriends);
        return ServiceResult.ok(ErrorCodes.SUCCESS);
    }

    /**
     * 获取我的好友list 获取好友列表服务接口
     * @param req
     * @return
     */
    @DataScopeFilter(dataScope = DataScopeConstants.SELF_DATA)
    @Override
    public ServiceResult<PageResult<List<QueryMyChatUsersFriendsPageRes>>> queryMyChatUsersFriendsPageResList(QueryMyChatUsersFriendsPageReq req) {
        PageHelperUtil.orderByAndStartPage(req, "createTime desc, id desc");
        List<QueryMyChatUsersFriendsPageRes> list = chatUsersFriendsMapper.queryMyChatUsersFriendsPageResList(req);
        for (QueryMyChatUsersFriendsPageRes res:list){
            SystemDict gender = systemDictCache.getSystemDictByDictCode(res.getGenderCode());
            res.setGenderName(gender.getDictName());
        }
        return PageHelperUtil.returnServiceResult(req,list);
    }


    /*根据手机号码精确搜索 搜索联系人 搜索联系人*/
    @Override
    public ServiceResult<String> querySearchFriendsUserIdBySearchKeyWords(SearchFriendsReq req) {
        SystemUser systemUser = chatUsersFriendsMapper.querySystemUserBySearchKeyWords(req);
        if (systemUser==null){
            return ServiceResult.fail("无法找到该用户，请检查搜索号码并重试！");
        }
        return ServiceResult.ok(systemUser.getUserId());
    }

    /*根据用户id查询用户详情*/
    @Override
    public ServiceResult<QueryChatUsersDetailRes> queryChatUsersDetailResByUserId(String userId) {
        QueryChatUsersDetailRes detailRes = chatUsersFriendsMapper.queryChatUsersDetailResByUserId(userId);
        if (detailRes==null){
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        //当前登录人
        UserTokenInfoVo userTokenInfoVo = getCurrentLoginUserInfoDTO();
        detailRes.setFriendStatus(ImChatEnum.FriendStatus.MYSELF.getKey());
        detailRes.setFriendRemark(detailRes.getRealName());
        detailRes.setFriendApply(ImChatEnum.IM_YES_NO.NO.getKey());
        SystemDict gender = systemDictCache.getSystemDictByDictCode(detailRes.getGenderCode());
        detailRes.setGenderName(gender.getDictName());
        //判断查询信息是否是自己
        if (!userTokenInfoVo.getUserId().equals(detailRes.getUserId())){
            Integer yes = ImChatEnum.IM_YES_NO.YES.getKey();
            //不是自己
            //主键ID 当前用户id和好友id的MD5值
            String friendId = MD5Util.getMD532(userTokenInfoVo.getUserId()+userId);
            ChatUsersFriends usersFriends = chatUsersFriendsMapper.selectByPrimaryKey(friendId);
            if (usersFriends!=null&&yes.equals(usersFriends.getIsFriend())){
                detailRes.setFriendStatus(ImChatEnum.FriendStatus.FRIEND.getKey());
                detailRes.setFriendRemark(usersFriends.getFriendRemark());
            }else {
                //拒绝状态
                Integer decline = ImChatEnum.ApplyStatus.DECLINE.getKey();
                detailRes.setFriendStatus(ImChatEnum.FriendStatus.STRANGER.getKey());
                //查询是否在申请朋友记录中
                ChatUsersFriendsApply friendsApply = chatUsersFriendsApplyMapper.selectByPrimaryKey(friendId);
                detailRes.setFriendApply(friendsApply!=null&&!decline.equals(friendsApply.getApplyStatus())?yes:ImChatEnum.IM_YES_NO.NO.getKey());
            }
        }
        return ServiceResult.ok(detailRes);
    }



    /**
     * 检查好友信息是否存在
     * @param id
     */
    private ChatUsersFriends checkChatUsersFriendsById(String id){
        if (StringUtils.isBlank(id)){
            throw new DuoJuHeException(ErrorCodes.NOT_FRIEND_ERROR);
        }
        ChatUsersFriends usersFriends = chatUsersFriendsMapper.selectByPrimaryKey(id);
        if (usersFriends==null){
            throw new DuoJuHeException(ErrorCodes.NOT_FRIEND_ERROR);
        }
        return usersFriends;
    }

}
