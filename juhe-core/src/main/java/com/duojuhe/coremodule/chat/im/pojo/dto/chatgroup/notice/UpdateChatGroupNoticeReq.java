package com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.notice;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateChatGroupNoticeReq extends SaveChatGroupNoticeReq {
    @ApiModelProperty(value = "通知ID", example = "1",required=true)
    @NotBlank(message = "通知ID不能为空")
    private String noticeId;
}
