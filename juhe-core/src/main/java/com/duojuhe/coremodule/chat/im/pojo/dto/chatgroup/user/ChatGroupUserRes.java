package com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChatGroupUserRes extends BaseBean {

    @ApiModelProperty(value = "群名称", required = true)
    private String groupName;

    @ApiModelProperty(value = "群头像")
    private String groupAvatar;
}
