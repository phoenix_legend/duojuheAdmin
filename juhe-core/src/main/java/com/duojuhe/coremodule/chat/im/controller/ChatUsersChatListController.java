package com.duojuhe.coremodule.chat.im.controller;

import com.duojuhe.coremodule.chat.im.pojo.dto.chatlist.*;
import com.duojuhe.coremodule.chat.im.service.ChatUsersChatListService;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/chatList/")
@Api(tags = {"【IM会话列表】IM会话列表相关接口"})
@Slf4j
public class ChatUsersChatListController {
    @Resource
    private ChatUsersChatListService chatUsersChatListService;


    @ApiOperation(value = "统计当前用户多聊未读消息数量")
    @PostMapping(value = "queryCountMyUnreadChatMessageNum")
    public ServiceResult queryCountMyUnreadChatMessageNum() {
        return chatUsersChatListService.queryCountMyUnreadChatMessageNum();
    }

    @ApiOperation(value = "聊天列表创建服务接口")
    @PostMapping(value = "saveChatUsersChatList")
    public ServiceResult saveChatUsersChatList(@Valid @RequestBody @ApiParam(value = "入参类") SaveChatUsersChatListReq req) {
        return chatUsersChatListService.saveChatUsersChatList(req);
    }


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【我的IM会话列表】分页查询我的IM会话列表list")
    @PostMapping(value = "queryMyChatUsersChatListPageResList")
    public ServiceResult<PageResult<List<QueryMyChatUsersChatListPageRes>>> queryMyChatUsersChatListPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryMyChatUsersChatListPageReq req) {
        return chatUsersChatListService.queryMyChatUsersChatListPageResList(req);
    }

    @ApiOperation(value = "设置会话是否置顶")
    @PostMapping(value = "setTopChatUsersChatListById")
    public ServiceResult setTopChatUsersChatListById(@Valid @RequestBody @ApiParam(value = "入参类") ChatUsersChatListIdReq req) {
        return chatUsersChatListService.setTopChatUsersChatListById(req);
    }

    @ApiOperation(value = "设置会话免打扰")
    @PostMapping(value = "setDisturbChatUsersChatList")
    public ServiceResult setDisturbChatUsersChatList(@Valid @RequestBody @ApiParam(value = "入参类") SetDisturbChatUsersChatListReq req) {
        return chatUsersChatListService.setDisturbChatUsersChatList(req);
    }

    @ApiOperation(value = "设置会话删除")
    @PostMapping(value = "deleteChatUsersChatListById")
    public ServiceResult deleteChatUsersChatListById(@Valid @RequestBody @ApiParam(value = "入参类") ChatUsersChatListIdReq req) {
        return chatUsersChatListService.deleteChatUsersChatListById(req);
    }

    @ApiOperation(value = "清除未读数量")
    @PostMapping(value = "clearUnreadNumChatUsersChatList")
    public ServiceResult clearUnreadNumChatUsersChatList(@Valid @RequestBody @ApiParam(value = "入参类") ClearUnreadNumChatUsersChatListReq req) {
        return chatUsersChatListService.clearUnreadNumChatUsersChatList(req);
    }


}
