package com.duojuhe.coremodule.chat.im.controller;

import com.duojuhe.coremodule.chat.im.pojo.dto.emoticon.item.*;
import com.duojuhe.coremodule.chat.im.service.ChatEmoticonItemService;
import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/chatEmoticonItem/")
@Api(tags = {"【IM表情】IM表情关接口"})
@Slf4j
public class ChatEmoticonItemController {
    @Resource
    private ChatEmoticonItemService chatEmoticonItemService;


    @ApiOperation(value = "分页查询用户表情包服务接口")
    @PostMapping(value = "queryChatUserEmoticonItemPageResList")
    public ServiceResult<PageResult<List<QueryChatUserEmoticonItemPageRes>>> queryChatUserEmoticonItemPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryChatUserEmoticonItemPageReq req) {
        return chatEmoticonItemService.queryChatUserEmoticonItemPageResList(req);
    }

    @ApiOperation(value = "查询用户表情包服务接口")
    @PostMapping(value = "queryMyChatEmoticonItemRes")
    public ServiceResult<QueryMyChatEmoticonItemRes> queryMyChatEmoticonItemRes() {
        return chatEmoticonItemService.queryMyChatEmoticonItemRes(new DataScopeFilterBean());
    }


    @ApiOperation(value = "分页查询系统表情包服务接口")
    @PostMapping(value = "queryChatSystemEmoticonItemPageResList")
    public ServiceResult<PageResult<List<QueryChatSystemEmoticonItemPageRes>>> queryChatSystemEmoticonItemPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryChatSystemEmoticonItemPageReq req) {
        return chatEmoticonItemService.queryChatSystemEmoticonItemPageResList(req);
    }

    @ApiOperation(value = "收藏表情包服务接口")
    @PostMapping(value = "collectChatEmoticonItem")
    public ServiceResult collectChatEmoticonItem(@Valid @RequestBody @ApiParam(value = "入参类") CollectChatEmoticonItemReq req) {
        return chatEmoticonItemService.collectChatEmoticonItem(req);
    }


    @ApiOperation(value = "保存用户表情包")
    @PostMapping(value = "saveChatEmoticonItem")
    public ServiceResult saveChatEmoticonItem(@Valid @RequestBody @ApiParam(value = "入参类") SaveChatEmoticonItemReq req) {
        return chatEmoticonItemService.saveChatEmoticonItem(req);
    }


    @ApiOperation(value = "【自定义表情】新增自定义表情")
    @PostMapping(value = "uploadDiyChatEmoticonItem")
    public ServiceResult<String> uploadDiyChatEmoticonItem(@RequestParam("file") MultipartFile file) {
        return chatEmoticonItemService.uploadDiyChatEmoticonItem(file);
    }
}
