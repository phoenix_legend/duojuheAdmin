package com.duojuhe.coremodule.chat.im.pojo.dto.chatlist;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChatUsersChatListIdReq extends BaseBean {
    @ApiModelProperty(value = "会话记录id", example = "1",required=true)
    @NotBlank(message = "会话ID不能为空")
    private String id;
}
