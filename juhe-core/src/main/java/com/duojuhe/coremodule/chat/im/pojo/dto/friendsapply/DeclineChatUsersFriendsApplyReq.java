package com.duojuhe.coremodule.chat.im.pojo.dto.friendsapply;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DeclineChatUsersFriendsApplyReq extends ChatUsersFriendsApplyIdReq {
    @ApiModelProperty(value = "备注说明", example = "1",required=true)
    @NotBlank(message = "备注说明不能为空")
    @Length(max = 255, message = "备注说明不得超过{max}位字符")
    private String remark;
}
