package com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateUserChatGroupVisitCardReq extends ChatGroupIdReq{
    @ApiModelProperty(value = "群名片", example = "1",required=true)
    @NotBlank(message = "群名片不能为空")
    @Length(max = 50, message = "群名片不得超过{max}位字符")
    private String visitCard;

}
