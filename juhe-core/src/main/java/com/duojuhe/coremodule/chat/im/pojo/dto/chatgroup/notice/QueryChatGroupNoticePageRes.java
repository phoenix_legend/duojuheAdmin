package com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.notice;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryChatGroupNoticePageRes extends BaseBean {
    @ApiModelProperty(value = "群通知id", required = true)
    private String noticeId;

    @ApiModelProperty(value = "群id", required = true)
    private String groupId;

    @ApiModelProperty(value = "创建者用户ID", required = true)
    private String createUserId;

    @ApiModelProperty(value = "创建者用户", required = true)
    private String createUserName;

    @ApiModelProperty(value = "通知标题")
    private String noticeTitle;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "通知内容")
    private String content;

    @ApiModelProperty(value = "用户头像")
    private String headPortrait;

}
