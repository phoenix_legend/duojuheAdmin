package com.duojuhe.coremodule.chat.im.controller;

import com.duojuhe.coremodule.chat.im.pojo.dto.friendsapply.*;
import com.duojuhe.coremodule.chat.im.service.ChatUsersFriendsApplyService;
import com.duojuhe.common.bean.DataScopeFilterBean;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/chatUsersFriendsApply/")
@Api(tags = {"【IM好友申请】IM好友申请相关接口"})
@Slf4j
public class ChatUsersFriendsApplyController {
    @Resource
    private ChatUsersFriendsApplyService chatUsersFriendsApplyService;

    @ApiOperation(value = "【好友申请服务接口】分页查询好友申请服务接口")
    @PostMapping(value = "queryMyChatUsersFriendsApplyPageResList")
    public ServiceResult<PageResult<List<QueryMyChatUsersFriendsApplyPageRes>>> queryMyChatUsersFriendsApplyPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryMyChatUsersFriendsApplyPageReq req) {
        return chatUsersFriendsApplyService.queryMyChatUsersFriendsApplyPageResList(req);
    }

    @ApiOperation(value = "好友申请服务接口")
    @PostMapping(value = "saveChatUsersFriendsApply")
    public ServiceResult saveChatUsersFriendsApply(@Valid @RequestBody @ApiParam(value = "入参类") SaveChatUsersFriendsApplyReq req) {
        return chatUsersFriendsApplyService.saveChatUsersFriendsApply(req);
    }

    @ApiOperation(value = "同意 处理好友申请服务接口")
    @PostMapping(value = "acceptChatUsersFriendsApply")
    public ServiceResult acceptChatUsersFriendsApply(@Valid @RequestBody @ApiParam(value = "入参类") AcceptChatUsersFriendsApplyReq req) {
        return chatUsersFriendsApplyService.acceptChatUsersFriendsApply(req);
    }

    @ApiOperation(value = "拒绝 处理好友申请服务接口")
    @PostMapping(value = "declineChatUsersFriendsApply")
    public ServiceResult declineChatUsersFriendsApply(@Valid @RequestBody @ApiParam(value = "入参类") DeclineChatUsersFriendsApplyReq req) {
        return chatUsersFriendsApplyService.declineChatUsersFriendsApply(req);
    }

    @ApiOperation(value = "查询好友申请未读数量服务接口")
    @PostMapping(value = "queryMyChatUsersFindFriendApplyNum")
    public ServiceResult<Integer> queryMyChatUsersFindFriendApplyNum() {
        return chatUsersFriendsApplyService.queryMyChatUsersFindFriendApplyNum(new DataScopeFilterBean());
    }

}
