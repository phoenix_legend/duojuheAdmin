package com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.notice;

import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.ChatGroupIdReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveChatGroupNoticeReq extends ChatGroupIdReq {
    @ApiModelProperty(value = "公告标题", example = "1",required=true)
    @NotBlank(message = "公告标题不能为空")
    @Length(max = 100, message = "公告标题不得超过{max}位字符")
    private String noticeTitle;

    @ApiModelProperty(value = "公告内容", example = "1",required=true)
    @NotBlank(message = "公告内容不能为空")
    @Length(max = 255, message = "公告内容不得超过{max}位字符")
    private String content;
}
