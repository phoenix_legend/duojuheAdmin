package com.duojuhe.coremodule.chat.im.pojo.dto.chatlist;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChatUsersGroupInfoRes extends BaseBean {
    @ApiModelProperty(value = "会话列表iD", example = "1")
    private String chatListId;

    @ApiModelProperty(value = "名称", example = "1")
    private String realName;

    @ApiModelProperty(value = "头像", example = "1")
    private String headPortrait;

    @ApiModelProperty(value = "备注", example = "1")
    private String friendRemark;
}
