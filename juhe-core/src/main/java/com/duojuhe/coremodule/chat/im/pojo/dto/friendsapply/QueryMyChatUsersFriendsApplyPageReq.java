package com.duojuhe.coremodule.chat.im.pojo.dto.friendsapply;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryMyChatUsersFriendsApplyPageReq extends PageHead {
    @ApiModelProperty(value = "姓名", example = "1")
    private String realName;

    @ApiModelProperty(value = "登录名", example = "1")
    private String loginName;

    @ApiModelProperty(value = "手机号码", example = "1")
    private String mobileNumber;

    @ApiModelProperty(value = "申请状态[0:申请中;1:是同意，2拒绝;]", example = "1")
    private String applyStatus;

    @ApiModelProperty(value = "开始日期 yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期 yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}
