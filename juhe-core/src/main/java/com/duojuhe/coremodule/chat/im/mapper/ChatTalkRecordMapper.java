package com.duojuhe.coremodule.chat.im.mapper;


import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecord;
import com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.QueryChatTalkRecordPageReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.QueryChatTalkRecordPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface ChatTalkRecordMapper extends TkMapper<ChatTalkRecord> {

    /**
     * 根据条件查询查询用户聊天记录
     * @param req
     * @param userId 当前用户id
     * @return
     */
    List<QueryChatTalkRecordPageRes> queryChatTalkRecordPageResList(@Param("req") QueryChatTalkRecordPageReq req,@Param(value = "userId")String userId,@Param(value = "recordEndTime") Date recordEndTime);



    /**
     * 查找用户聊天记录
     * @param req
     * @param userId 当前用户id
     * @return
     */
    List<QueryChatTalkRecordPageRes> findChatTalkRecordPageResList(@Param("req") QueryChatTalkRecordPageReq req,@Param(value = "userId")String userId,@Param(value = "recordEndTime") Date recordEndTime);



    /**
     * 根据消息id查询消息记录集合
     * @param
     * @return
     */
    List<ChatTalkRecord> queryChatTalkRecordListByRecordIdList(@Param(value = "recordIdList") List<String> recordIdList);

    /**
     * 根据消息id查询转发消息记录集合
     * @param
     * @return
     */
    List<QueryChatTalkRecordPageRes> queryChatTalkRecordForwardPageResListByRecordIdList(@Param(value = "recordIdList") List<String> recordIdList);

}