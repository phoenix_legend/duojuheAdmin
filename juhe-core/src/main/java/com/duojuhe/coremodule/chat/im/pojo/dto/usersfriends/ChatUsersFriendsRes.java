package com.duojuhe.coremodule.chat.im.pojo.dto.usersfriends;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChatUsersFriendsRes extends BaseBean {
    @ApiModelProperty(value = "好友备注")
    private String friendRemark;

    @ApiModelProperty(value = "是否好友状态[0:否;1:是;]")
    private Integer isFriend;
}
