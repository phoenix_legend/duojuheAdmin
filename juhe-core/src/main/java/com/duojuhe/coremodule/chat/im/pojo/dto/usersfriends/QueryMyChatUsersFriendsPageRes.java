package com.duojuhe.coremodule.chat.im.pojo.dto.usersfriends;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryMyChatUsersFriendsPageRes extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String id;

    @ApiModelProperty(value = "用户id", required = true)
    private String userId;

    @ApiModelProperty(value = "朋友id", required = true)
    private String friendId;

    @ApiModelProperty(value = "朋友备注")
    private String friendRemark;

    @ApiModelProperty(value = "创建时间，成为好友时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "解除好友时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date unfriendTime;

    @ApiModelProperty(value = "创建用户id")
    private String createUserId;

    @ApiModelProperty(value = "邀请方向 主动 被动")
    private String directionCode;

    @ApiModelProperty(value = "是否好友状态[0:否;1:是;]")
    private Integer isFriend;

    @ApiModelProperty(value = "姓名", example = "1")
    private String realName;

    @ApiModelProperty(value = "登录名", example = "1")
    private String loginName;

    @ApiModelProperty(value = "手机号码", example = "1")
    private String mobileNumber;

    @ApiModelProperty(value = "头像", example = "1")
    private String headPortrait;

    @ApiModelProperty(value = "座右铭")
    private String motto;

    @ApiModelProperty(value = "性别")
    private String genderCode;

    @ApiModelProperty(value = "性别中文")
    private String genderName;

}
