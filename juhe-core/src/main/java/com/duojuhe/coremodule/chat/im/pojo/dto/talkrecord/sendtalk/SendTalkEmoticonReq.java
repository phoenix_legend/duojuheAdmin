package com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.sendtalk;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SendTalkEmoticonReq extends SendTalkReq {
    @ApiModelProperty(value = "表情ID", example = "1",required=true)
    @NotBlank(message = "表情ID不能为空")
    private String emoticonId;
}
