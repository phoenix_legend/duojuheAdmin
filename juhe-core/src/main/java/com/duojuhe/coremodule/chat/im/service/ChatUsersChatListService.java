package com.duojuhe.coremodule.chat.im.service;

import com.duojuhe.coremodule.chat.im.pojo.dto.chatlist.*;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;

import java.util.List;

public interface ChatUsersChatListService {
    /**
     * 统计当前用户多聊未读消息数量
     * @return
     */
    ServiceResult<Integer> queryCountMyUnreadChatMessageNum();


    /**
     *【用户会话列表查询】分页查询用户会话列表查询
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryMyChatUsersChatListPageRes>>> queryMyChatUsersChatListPageResList(QueryMyChatUsersChatListPageReq req);


    /**
     * 聊天列表创建服务接口
     * @param req
     * @return
     */
    ServiceResult saveChatUsersChatList (SaveChatUsersChatListReq req);

    /**
     * 设置会话是否置顶
     * @param req
     * @return
     */
    ServiceResult setTopChatUsersChatListById (ChatUsersChatListIdReq req);

    /**
     * 设置会话免打扰
     * @param req
     * @return
     */
    ServiceResult setDisturbChatUsersChatList (SetDisturbChatUsersChatListReq req);


    /**
     * 设置会话删除
     * @param req
     * @return
     */
    ServiceResult deleteChatUsersChatListById (ChatUsersChatListIdReq req);


    /**
     * 清除未读数量 清除聊天消息未读数服务接口
     * @param req
     * @return
     */
    ServiceResult clearUnreadNumChatUsersChatList (ClearUnreadNumChatUsersChatListReq req);


}
