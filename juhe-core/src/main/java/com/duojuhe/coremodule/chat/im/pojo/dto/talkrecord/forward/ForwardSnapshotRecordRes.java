package com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.forward;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ForwardSnapshotRecordRes extends BaseBean {
    @ApiModelProperty(value = "消息记录发布者")
    private String oldRealName;

    @ApiModelProperty(value = "原消息记录id")
    private String oldRecordId;

    @ApiModelProperty(value = "消息记录发布者id")
    private String oldUserId;

    @ApiModelProperty(value = "原消息内容")
    private String oldContent;
}
