package com.duojuhe.coremodule.chat.im.pojo.dto.usersfriends;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryMyChatUsersFriendsPageReq extends PageHead {
    @ApiModelProperty(value = "好友备注")
    private String friendRemark;

    @ApiModelProperty(value = "姓名", example = "1")
    private String realName;

    @ApiModelProperty(value = "登录名", example = "1")
    private String loginName;

    @ApiModelProperty(value = "手机号码", example = "1")
    private String mobileNumber;

    @ApiModelProperty(value = "开始日期 yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期 yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}
