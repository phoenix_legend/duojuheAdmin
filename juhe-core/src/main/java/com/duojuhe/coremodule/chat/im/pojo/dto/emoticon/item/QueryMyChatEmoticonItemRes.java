package com.duojuhe.coremodule.chat.im.pojo.dto.emoticon.item;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryMyChatEmoticonItemRes extends BaseBean {
    @ApiModelProperty(value = "收藏表情", required = true)
    private List<ChatEmoticonItemRes> collectEmoticonList;

    @ApiModelProperty(value = "系统表情", required = true)
    private List<ChatSystemEmoticonRes> systemEmoticonList;
}
