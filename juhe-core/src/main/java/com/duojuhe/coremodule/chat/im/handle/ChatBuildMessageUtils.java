package com.duojuhe.coremodule.chat.im.handle;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.duojuhe.common.enums.chat.ImChatEnum;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.duojuhe.common.utils.stringutil.StringUtil;
import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecord;
import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecordInvite;
import com.duojuhe.coremodule.chat.im.entity.ChatUsersChatList;
import com.duojuhe.websocket.subscriber.message.talk.MessageBodyRes;
import com.duojuhe.websocket.subscriber.message.talk.bean.InviteRes;
import com.duojuhe.websocket.subscriber.message.talk.bean.invite.ChatUserRes;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

public class ChatBuildMessageUtils {


    /**
     * 构建响应对象
     *
     * @param record
     * @return
     */
    public static MessageBodyRes buildMessageBodyRes(ChatTalkRecord record) {
        MessageBodyRes bodyRes = new MessageBodyRes();
        bodyRes.setRecordId(record.getRecordId());
        bodyRes.setId(record.getRecordId());
        bodyRes.setReceiverId(record.getReceiverId());
        bodyRes.setSenderId(record.getUserId());
        bodyRes.setUserId(record.getUserId());
        bodyRes.setTalkType(record.getTalkType());
        bodyRes.setMsgType(record.getMsgType());
        bodyRes.setRealName(record.getRealName());
        bodyRes.setHeadPortrait(record.getHeadPortrait());
        bodyRes.setGroupName(record.getGroupName());
        bodyRes.setGroupAvatar(record.getGroupAvatar());
        bodyRes.setContent(record.getContent());
        bodyRes.setIsMark(record.getIsMark());
        bodyRes.setIsRead(record.getIsRead());
        bodyRes.setIsRevoke(record.getIsRevoke());
        bodyRes.setCreateTime(DateUtils.dateToString(record.getCreateTime()));
        bodyRes.setSendSenderSelfFlag(true);
        return bodyRes;
    }


    /**
     * 构建邀请响应对象
     *
     * @param invite
     * @return
     */
    public static InviteRes buildRecordInviteToInviteRes(ChatTalkRecordInvite invite) {
        InviteRes inviteRes = new InviteRes();
        if (invite == null) {
            return inviteRes;
        }
        inviteRes.setInviteType(invite.getInviteType());
        if (StringUtils.isNotBlank(invite.getOperateUserSnapshot())) {
            inviteRes.setOperateUser(JSON.parseObject(invite.getOperateUserSnapshot(), new TypeReference<ChatUserRes>() {
            }));
        }
        if (StringUtils.isNotBlank(invite.getUserSnapshot())) {
            inviteRes.setUserList(JSON.parseObject(invite.getUserSnapshot(), new TypeReference<ArrayList<ChatUserRes>>() {
            }));
        }
        return inviteRes;
    }


    /**
     * 构建待更新的会话列表对象
     *
     * @param chat
     * @param record
     * @return
     */
    public static ChatUsersChatList buildLastChatUsersChatList(ChatUsersChatList chat, ChatTalkRecord record) {
        Integer yes = ImChatEnum.IM_YES_NO.YES.getKey();
        ChatUsersChatList chatList = new ChatUsersChatList();
        chatList.setId(chat.getId());
        chatList.setLastReleaseTime(record.getReleaseTime());
        chatList.setLastRecordId(record.getRecordId());
        if (yes.equals(record.getIsRevoke())) {
            chatList.setLastMessage("撤回一条消息");
        } else {
            chatList.setLastMessage(getLastMessage(record));
            if (!chat.getUserId().equals(record.getUserId())) {
                chatList.setUnreadNum(chat.getUnreadNum() + 1);
            }
        }
        return chatList;
    }


    /**
     * 获取最后的消息记录
     *
     * @param record
     * @return
     */
    public static String getLastMessage(ChatTalkRecord record) {
        //消息内容
        String text;
        //消息类型
        Integer msgType = record.getMsgType();
        switch (msgType) {
            case 1:
                text = StringUtil.subStringByStrAndLen(record.getContent(), 20);
                break;
            case 2:
                text = ImChatEnum.FileType.FILE_IMAGE.getKey().equals(record.getFileType()) ? "[图片消息]" : "[文件消息]";
                break;
            case 3:
                text = "[会话记录]";
                break;
            case 4:
                text = "[代码消息]";
                break;
            case 5:
                text = "[投票消息]";
                break;
            case 6:
                text = "[群组公告]";
                break;
            case 7:
                text = "[好友申请]";
                break;
            case 8:
                text = "[登录提醒]";
                break;
            case 9:
                text = "[入群退群消息]";
                break;
            default:
                text = "[系统消息]";
        }
        return text;
    }

}
