package com.duojuhe.coremodule.chat.im.service;

import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.*;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.notice.QueryChatGroupNoticePageReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.notice.QueryChatGroupNoticePageRes;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.notice.SaveChatGroupNoticeReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.notice.UpdateChatGroupNoticeReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user.QueryChatGroupUserPageReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user.QueryChatGroupUserPageRes;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user.QueryInviteChatGroupUserPageReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user.QueryInviteChatGroupUserPageRes;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;

import java.util.List;

public interface ChatGroupService {

    /**
     * 分页查询用户群组 用户群聊服务接口
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryMyChatGroupPageRes>>> queryMyChatGroupPageResList(QueryMyChatGroupPageReq req);

    /**
     * 根据id获取群详情 获取群信息服务接口
     * @param req
     * @return
     */
    ServiceResult<QueryChatGroupRes> queryChatGroupResByGroupId (ChatGroupIdReq req);

    /**
     * 创建群组
     * @param req
     * @return
     */
    ServiceResult saveChatGroup(SaveChatGroupReq req);

    /**
     * 修改群组
     * @param req
     * @return
     */
    ServiceResult updateChatGroup(UpdateChatGroupReq req);


    /**
     * 邀请好友入群组 邀请好友加入群聊服务接口
     * @param req
     * @return
     */
    ServiceResult inviteJoinChatGroup(InviteJoinChatGroupReq req);



    /**
     * 移除群组 移除群聊成员服务接口
     * @param req
     * @return
     */
    ServiceResult removeChatGroupUser(RemoveChatGroupUserReq req);



    /**
     * 解散群组 管理员解散群聊服务接口
     * @param req
     * @return
     */
    ServiceResult dismissChatGroupByGroupId (ChatGroupIdReq req);


    /**
     * 退出群组 用户退出群聊服务接口
     * @param req
     * @return
     */
    ServiceResult secedeChatGroupByGroupId (ChatGroupIdReq req);

    /**
     * 设置群组全员禁言
     * @param req
     * @return
     */
    ServiceResult setMuteChatGroup(SetMuteChatGroupReq req);




    /**
     * 修改群聊名片服务接口
     * @param req
     * @return
     */
    ServiceResult updateUserChatGroupVisitCard(UpdateUserChatGroupVisitCardReq req);



    /**
     * 分页查询群组公告 获取群组公告列表
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryChatGroupNoticePageRes>>> queryChatGroupNoticePageResList(QueryChatGroupNoticePageReq req);



    /**
     * 保存群公告
     * @param req
     * @return
     */
    ServiceResult saveChatGroupNotice(SaveChatGroupNoticeReq req);

    /**
     * 修改群公告
     * @param req
     * @return
     */
    ServiceResult updateChatGroupNotice(UpdateChatGroupNoticeReq req);


    /**
     * 分页查询群成员列表 获取群组成员列表
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryChatGroupUserPageRes>>> queryChatGroupUserPageResList(QueryChatGroupUserPageReq req);




    /**
     * 分页查询获取用户可邀请加入群组的好友列表 获取用户可邀请加入群组的好友列表
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryInviteChatGroupUserPageRes>>> queryInviteChatGroupUserPageResList(QueryInviteChatGroupUserPageReq req);



}
