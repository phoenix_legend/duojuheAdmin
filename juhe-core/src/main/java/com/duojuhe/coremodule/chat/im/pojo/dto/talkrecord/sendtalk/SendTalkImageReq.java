package com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.sendtalk;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SendTalkImageReq extends SendTalkReq {
    @ApiModelProperty(value = "发送者对象id")
    private String senderId;
}
