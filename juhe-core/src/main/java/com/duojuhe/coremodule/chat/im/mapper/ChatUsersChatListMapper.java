package com.duojuhe.coremodule.chat.im.mapper;

import com.duojuhe.coremodule.chat.im.entity.ChatUsersChatList;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatlist.QueryMyChatUsersChatListPageReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatlist.QueryMyChatUsersChatListPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChatUsersChatListMapper extends TkMapper<ChatUsersChatList> {
    /**
     *【用户会话列表查询】分页查询用户会话列表查询
     * @param req
     * @return
     */
    List<QueryMyChatUsersChatListPageRes> queryMyChatUsersChatListPageResList(@Param("req") QueryMyChatUsersChatListPageReq req);

    /**
     * 根据接受者id list 查询会话记录集合
     * @param receiverIdList
     * @return
     */
    List<ChatUsersChatList> queryChatUsersChatListByReceiverIdList(@Param("receiverIdList") List<String> receiverIdList);

    /**
     * 根据会话id list 查询会话记录集合
     * @param idList
     * @return
     */
    List<ChatUsersChatList> queryChatUsersChatListByIdList(@Param("idList") List<String> idList);

    /**
     * 查询用户id所有未读消息
     *
     * @return
     */
    int queryCountUntreatedChatMessageByUserId(@Param("userId") String userId);


}