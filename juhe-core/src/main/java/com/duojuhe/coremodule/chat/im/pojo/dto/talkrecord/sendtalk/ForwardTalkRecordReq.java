package com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.sendtalk;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ForwardTalkRecordReq extends SendTalkReq {
    @ApiModelProperty(value = "转发方式[1:逐条转发;2:合并转发;]")
    @NotNull(message = "转发方式不能为空")
    @Range(min = 1, max = 2, message = "转发方式参数不正确")
    private Integer forwardMode;

    @ApiModelProperty(value = "消息记录ID集合", required = true)
    @NotEmpty(message = "消息记录ID集合不能为空")
    @Valid
    @NotNull(message = "消息记录ID集合不能为空")
    @Size(min = 1,max = 10,message = "一次转发消息至少1条,最多10条")
    private List<String> recordIdList;

    @ApiModelProperty(value = "转发接收用户集合", required = true)
    @Valid
    @NotNull(message = "转发接收用户集合不能为空")
    private List<String> receiveUserIdList;

    @ApiModelProperty(value = "转发接收群组集合", required = true)
    @Valid
    @NotNull(message = "转发接收群组集合不能为空")
    private List<String> receiveGroupIdList;
}
