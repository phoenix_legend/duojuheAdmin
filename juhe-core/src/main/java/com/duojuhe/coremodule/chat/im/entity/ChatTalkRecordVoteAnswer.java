package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("聊天对话记录（投票消息统计表）")
@Table(name = "chat_talk_record_vote_answer")
public class ChatTalkRecordVoteAnswer extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "answer_id")
    @Id
    private String answerId;

    @ApiModelProperty(value = "投票id")
    @Column(name = "vote_id")
    private String voteId;

    @ApiModelProperty(value = "用户id")
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty(value = "投票选项[A、B、C 、D、E、F]")
    @Column(name = "answer_option")
    private String answerOption;

    @ApiModelProperty(value = "答题时间")
    @Column(name = "create_time")
    private Date createTime;
}