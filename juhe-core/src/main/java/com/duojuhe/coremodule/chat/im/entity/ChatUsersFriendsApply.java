package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("用户添加好友申请表")
@Table(name = "chat_users_friends_apply")
public class ChatUsersFriendsApply extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "apply_id")
    @Id
    private String applyId;

    @ApiModelProperty(value = "申请人ID", required = true)
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty(value = "被申请人id", required = true)
    @Column(name = "friend_id")
    private String friendId;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "申请状态[0:申请中;1:是同意，2拒绝;]")
    @Column(name = "apply_status")
    private Integer applyStatus;

    @ApiModelProperty(value = "备注信息")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "申请人备注信息")
    @Column(name = "apply_description")
    private String applyDescription;

    @ApiModelProperty(value = "申请创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "处理时间")
    @Column(name = "handle_time")
    private Date handleTime;
}