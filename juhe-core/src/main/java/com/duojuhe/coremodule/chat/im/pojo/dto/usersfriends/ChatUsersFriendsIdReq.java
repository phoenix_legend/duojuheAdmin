package com.duojuhe.coremodule.chat.im.pojo.dto.usersfriends;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChatUsersFriendsIdReq extends BaseBean {
    @ApiModelProperty(value = "朋友id", example = "1",required=true)
    @NotBlank(message = "朋友ID不能为空")
    private String friendId;
}
