package com.duojuhe.coremodule.chat.im.controller;

import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.*;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.notice.QueryChatGroupNoticePageReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.notice.QueryChatGroupNoticePageRes;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.notice.SaveChatGroupNoticeReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.notice.UpdateChatGroupNoticeReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user.QueryChatGroupUserPageReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user.QueryChatGroupUserPageRes;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user.QueryInviteChatGroupUserPageReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user.QueryInviteChatGroupUserPageRes;
import com.duojuhe.coremodule.chat.im.service.ChatGroupService;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/chatGroup/")
@Api(tags = {"【IM群组】IM群组相关接口"})
@Slf4j
public class ChatGroupController {
    @Resource
    private ChatGroupService chatGroupService;



    @ApiOperation(value = "分页查询用户群组 用户群聊服务接口")
    @PostMapping(value = "queryMyChatGroupPageResList")
    public ServiceResult<PageResult<List<QueryMyChatGroupPageRes>>> queryMyChatGroupPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryMyChatGroupPageReq req) {
        return chatGroupService.queryMyChatGroupPageResList(req);
    }


    @ApiOperation(value = "根据id获取群详情 获取群信息服务接口")
    @PostMapping(value = "queryChatGroupResByGroupId")
    public ServiceResult<QueryChatGroupRes> queryChatGroupResByGroupId(@Valid @RequestBody @ApiParam(value = "入参类") ChatGroupIdReq req) {
        return chatGroupService.queryChatGroupResByGroupId(req);
    }

    @ApiOperation(value = "创建群组")
    @PostMapping(value = "saveChatGroup")
    public ServiceResult saveChatGroup(@Valid @RequestBody @ApiParam(value = "入参类") SaveChatGroupReq req) {
        return chatGroupService.saveChatGroup(req);
    }

    @ApiOperation(value = "修改群组")
    @PostMapping(value = "updateChatGroup")
    public ServiceResult updateChatGroup(@Valid @RequestBody @ApiParam(value = "入参类") UpdateChatGroupReq req) {
        return chatGroupService.updateChatGroup(req);
    }


    @ApiOperation(value = "邀请好友入群组 邀请好友加入群聊服务接口")
    @PostMapping(value = "inviteJoinChatGroup")
    public ServiceResult inviteJoinChatGroup(@Valid @RequestBody @ApiParam(value = "入参类") InviteJoinChatGroupReq req) {
        return chatGroupService.inviteJoinChatGroup(req);
    }


    @ApiOperation(value = "移除群组 移除群聊成员服务接口")
    @PostMapping(value = "removeChatGroupUser")
    public ServiceResult removeChatGroupUser(@Valid @RequestBody @ApiParam(value = "入参类") RemoveChatGroupUserReq req) {
        return chatGroupService.removeChatGroupUser(req);
    }

    @ApiOperation(value = "管理员解散群聊服务接口")
    @PostMapping(value = "dismissChatGroupByGroupId")
    public ServiceResult dismissChatGroupByGroupId(@Valid @RequestBody @ApiParam(value = "入参类") ChatGroupIdReq req) {
        return chatGroupService.dismissChatGroupByGroupId(req);
    }

    @ApiOperation(value = "退出群组")
    @PostMapping(value = "secedeChatGroupByGroupId")
    public ServiceResult secedeChatGroupByGroupId(@Valid @RequestBody @ApiParam(value = "入参类") ChatGroupIdReq req) {
        return chatGroupService.secedeChatGroupByGroupId(req);
    }

    @ApiOperation(value = "设置群组全员禁言")
    @PostMapping(value = "setMuteChatGroup")
    public ServiceResult setMuteChatGroup(@Valid @RequestBody @ApiParam(value = "入参类") SetMuteChatGroupReq req) {
        return chatGroupService.setMuteChatGroup(req);
    }



    @ApiOperation(value = "修改群聊名片服务接口")
    @PostMapping(value = "updateUserChatGroupVisitCard")
    public ServiceResult updateUserChatGroupVisitCard(@Valid @RequestBody @ApiParam(value = "入参类") UpdateUserChatGroupVisitCardReq req) {
        return chatGroupService.updateUserChatGroupVisitCard(req);
    }


    @ApiOperation(value = "获取群组公告列表")
    @PostMapping(value = "queryChatGroupNoticePageResList")
    public ServiceResult<PageResult<List<QueryChatGroupNoticePageRes>>> queryChatGroupNoticePageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryChatGroupNoticePageReq req) {
        return chatGroupService.queryChatGroupNoticePageResList(req);
    }

    @ApiOperation(value = "保存群公告")
    @PostMapping(value = "saveChatGroupNotice")
    public ServiceResult saveChatGroupNotice(@Valid @RequestBody @ApiParam(value = "入参类") SaveChatGroupNoticeReq req) {
        return chatGroupService.saveChatGroupNotice(req);
    }

    @ApiOperation(value = "修改群公告")
    @PostMapping(value = "updateChatGroupNotice")
    public ServiceResult updateChatGroupNotice(@Valid @RequestBody @ApiParam(value = "入参类") UpdateChatGroupNoticeReq req) {
        return chatGroupService.updateChatGroupNotice(req);
    }

    @ApiOperation(value = "分页查询群成员列表")
    @PostMapping(value = "queryChatGroupUserPageResList")
    public ServiceResult<PageResult<List<QueryChatGroupUserPageRes>>> queryChatGroupUserPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryChatGroupUserPageReq req) {
        return chatGroupService.queryChatGroupUserPageResList(req);
    }

    @ApiOperation(value = "分页查询获取用户可邀请加入群组的好友列表")
    @PostMapping(value = "queryInviteChatGroupUserPageResList")
    public ServiceResult<PageResult<List<QueryInviteChatGroupUserPageRes>>> queryInviteChatGroupUserPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryInviteChatGroupUserPageReq req) {
        return chatGroupService.queryInviteChatGroupUserPageResList(req);
    }
}
