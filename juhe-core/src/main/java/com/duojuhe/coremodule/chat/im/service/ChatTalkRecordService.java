package com.duojuhe.coremodule.chat.im.service;

import com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.*;
import com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.forward.QueryChatTalkRecordForwardPageReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.forward.QueryChatTalkRecordForwardPageRes;
import com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.sendtalk.*;
import com.duojuhe.websocket.subscriber.message.talk.bean.vote.StatisticsRes;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ChatTalkRecordService {
    /**
     *【查询用户聊天记录】分页查询查询用户聊天记录
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryChatTalkRecordPageRes>>> queryChatTalkRecordPageResList(QueryChatTalkRecordPageReq req);


    /**
     *【查找用户聊天记录服务接口】查找用户聊天记录服务接口
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryChatTalkRecordPageRes>>> findChatTalkRecordPageResList(QueryChatTalkRecordPageReq req);


    /**
     *【获取转发会话记录详情列表服务接口】获取转发会话记录详情列表服务接口
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryChatTalkRecordForwardPageRes>>> queryChatTalkRecordForwardPageResList(QueryChatTalkRecordForwardPageReq req);


    /**
     * 发送代码块消息服务接口 发送代码块消息服务接口
     * @param req
     * @return
     */
    ServiceResult sendTalkCodeBlock(SendTalkCodeBlockReq req);



    /**
     * 发送聊天文件服务接口
     * @param req
     * @return
     */
    ServiceResult sendTalkFile(SendTalkFileReq req) throws Exception;

    /**
     * 发送聊天图片服务接口 发送聊天图片服务接口
     * @param req
     * @return
     */
    ServiceResult sendTalkImage(MultipartFile file,SendTalkImageReq req);



    /**
     * 发送表情包服务接口
     * @param req
     * @return
     */
    ServiceResult sendTalkEmoticon(SendTalkEmoticonReq req);


    /**
     * 转发消息服务接口
     * @param req
     * @return
     */
    ServiceResult forwardTalkRecord(ForwardTalkRecordReq req);


    /**
     * 发送投票服务接口
     * @param req
     * @return
     */
    ServiceResult sendTalkVote(SendTalkVoteReq req);


    /**
     * 提交投票服务接口
     * @param req
     * @return
     */
    ServiceResult confirmSubmitTalkVote(ConfirmSubmitTalkVoteReq req);


    /**
     *【根据会话id查询投票记录汇总信息】根据会话id查询投票记录汇总信息
     * @param req
     * @return
     */
    ServiceResult<StatisticsRes> queryChatTalkRecordVoteStatisticsResByRecordId(ChatTalkRecordIdReq req);




    /**
     * 撤回消息服务接口
     * @param req
     * @return
     */
    ServiceResult revokeRecordByRecordId(ChatTalkRecordIdReq req);


    /**
     * 删除消息服务接口
     * @param req
     * @return
     */
    ServiceResult deleteRecordByRecordId(ChatTalkRecordIdReq req);



    /**
     * 批量删除删除消息服务接口
     * @param req
     * @return
     */
    ServiceResult batchDeleteRecordByRecordIdList(BatchDeleteRecordIdListReq req);
}
