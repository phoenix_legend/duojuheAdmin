package com.duojuhe.coremodule.chat.im.pojo.dto.emoticon.item;

import com.duojuhe.common.bean.PageHead;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryChatSystemEmoticonItemPageReq extends PageHead {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "record_id")
    @Id
    private String recordId;
}
