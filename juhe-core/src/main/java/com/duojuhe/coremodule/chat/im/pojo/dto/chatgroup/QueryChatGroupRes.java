package com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup;

import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.notice.ChatGroupNoticeRes;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryChatGroupRes extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String groupId;

    @ApiModelProperty(value = "群名称", required = true)
    private String groupName;

    @ApiModelProperty(value = "群介绍")
    private String groupIntroduce;

    @ApiModelProperty(value = "群头像")
    private String groupAvatar;

    @ApiModelProperty(value = "最大人数限制")
    private Integer maxPeopleNumber;

    @ApiModelProperty(value = "是否公开可见[0:否;1:是;]")
    private Integer isOvert;

    @ApiModelProperty(value = "是否全员禁言 [0:否;1:是;]，提示:不包含群主或管理员")
    private Integer isMute;

    @ApiModelProperty(value = "是否已解散[0:否;1:是;]")
    private Integer isDismiss;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "解散时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    private Date dismissedTime;

    @ApiModelProperty(value = "创建人id")
    private String createUserId;

    @ApiModelProperty(value = "创建人名称")
    private String createUserName;

    @ApiModelProperty(value = "用户组名片")
    private String visitCard;

    @ApiModelProperty(value = "消息免打扰[0:否;1:是;]")
    private Integer isDisturb;

    @ApiModelProperty(value = "是否是管理人员")
    private Boolean isManager;

    @ApiModelProperty(value = "公告")
    private ChatGroupNoticeRes groupNoticeRes;
}
