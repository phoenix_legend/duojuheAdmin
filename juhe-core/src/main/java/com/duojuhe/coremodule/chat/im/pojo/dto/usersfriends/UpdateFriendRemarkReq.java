package com.duojuhe.coremodule.chat.im.pojo.dto.usersfriends;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateFriendRemarkReq extends ChatUsersFriendsIdReq {
    @ApiModelProperty(value = "朋友备注", example = "1",required=true)
    @NotBlank(message = "朋友备注不能为空")
    @Length(max = 20, message = "朋友备注不得超过{max}位字符")
    private String friendRemark;
}
