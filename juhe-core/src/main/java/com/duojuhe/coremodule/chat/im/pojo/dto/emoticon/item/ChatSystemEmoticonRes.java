package com.duojuhe.coremodule.chat.im.pojo.dto.emoticon.item;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChatSystemEmoticonRes extends BaseBean {

    @ApiModelProperty(value = "分类ID", required = true)
    private String categoryId;

    @ApiModelProperty(value = "分类名称", required = true)
    private String categoryName;

    @ApiModelProperty(value = "分类图标", required = true)
    private String categoryIcon;

    @ApiModelProperty(value = "系统表情", required = true)
    private List<ChatEmoticonItemRes> emoticonItemList;
}
