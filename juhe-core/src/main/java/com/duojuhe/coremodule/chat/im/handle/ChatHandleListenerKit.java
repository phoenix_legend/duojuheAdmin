package com.duojuhe.coremodule.chat.im.handle;

import com.alibaba.fastjson.JSONObject;
import com.duojuhe.common.enums.chat.ImChatEnum;
import com.duojuhe.common.utils.thread.ThreadUtils;
import com.duojuhe.coremodule.chat.im.entity.*;
import com.duojuhe.coremodule.chat.im.mapper.*;
import com.duojuhe.websocket.EventCodes;
import com.duojuhe.websocket.socket.SocketMessage;
import com.duojuhe.websocket.socket.SocketUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

@Component
@Slf4j
public class ChatHandleListenerKit {
    @Resource
    private ChatGroupUserMapper chatGroupUserMapper;
    @Resource
    private ChatUsersFriendsMapper chatUsersFriendsMapper;


    /**
     * 聊天消息事件处理
     *
     * @param jsonObject
     */
    public void eventTalkHandle(JSONObject jsonObject) {
        //发送者id
        String senderId = jsonObject.getString("senderId");
        //接收对象id
        String receiverId = jsonObject.getString("receiverId");
        //聊天类型
        Integer talkType = jsonObject.getInteger("talkType");
        //是否给自己发送消息
        Boolean sendSenderSelfFlag = jsonObject.getBoolean("sendSenderSelfFlag");
        //聊天消息对象转换
        SocketMessage socketMessage = SocketMessage.eventSendData(jsonObject, EventCodes.EVENT_TALK);
        //给自己通知一份消息
        if (sendSenderSelfFlag) {
            threadSendMessageToUserId(senderId, socketMessage);
        }
        if (ImChatEnum.TalkType.PRIVATE_CHAT.getKey().equals(talkType)) {
            //给接受者发送消息
            threadSendMessageToUserId(receiverId, socketMessage);
        } else if (ImChatEnum.TalkType.GROUP_CHAT.getKey().equals(talkType)) {
            //IM yes 标识
            Integer yes = ImChatEnum.IM_YES_NO.YES.getKey();
            //群聊
            List<ChatGroupUser> chatGroupUserList = chatGroupUserMapper.queryChatGroupUserListByGroupId(receiverId);
            for (ChatGroupUser groupUser : chatGroupUserList) {
                //用户id
                String userId = groupUser.getUserId();
                if (yes.equals(groupUser.getIsQuit()) || senderId.equals(userId)) {
                    //已经退群了 或者接受者是当前发送者 则不发送
                    continue;
                }
                threadSendMessageToUserId(userId, socketMessage);
            }
        }
    }

    /**
     * 撤回事件
     *
     * @param jsonObject
     */
    public void eventRevokeHandle(JSONObject jsonObject) {
        //发送者id
        String senderId = jsonObject.getString("senderId");
        //接收对象id
        String receiverId = jsonObject.getString("receiverId");
        //聊天类型
        Integer talkType = jsonObject.getInteger("talkType");
        //聊天消息对象转换
        SocketMessage socketMessage = SocketMessage.eventSendData(jsonObject, EventCodes.EVENT_REVOKE_TALK);
        if (ImChatEnum.TalkType.PRIVATE_CHAT.getKey().equals(talkType)) {
            //给接受者发送消息
            threadSendMessageToUserId(receiverId, socketMessage);
        } else if (ImChatEnum.TalkType.GROUP_CHAT.getKey().equals(talkType)) {
            //IM yes 标识
            Integer yes = ImChatEnum.IM_YES_NO.YES.getKey();
            //群聊
            List<ChatGroupUser> chatGroupUserList = chatGroupUserMapper.queryChatGroupUserListByGroupId(receiverId);
            for (ChatGroupUser groupUser : chatGroupUserList) {
                //用户id
                String userId = groupUser.getUserId();
                if (yes.equals(groupUser.getIsQuit()) || senderId.equals(userId)) {
                    //已经退群了 或者接受者是当前发送者 则不发送
                    continue;
                }
                threadSendMessageToUserId(userId, socketMessage);
            }
        }
    }


    /**
     * 键盘事件
     *
     * @param jsonObject
     */
    public void eventKeyboardHandle(JSONObject jsonObject) {
        //接收对象id
        String receiverId = jsonObject.getString("receiverId");
        if (StringUtils.isBlank(receiverId)) {
            return;
        }
        //聊天消息对象转换
        SocketMessage socketMessage = SocketMessage.eventSendData(jsonObject, EventCodes.EVENT_KEYBOARD);
        //给指定用户id通知一份消息
        threadSendMessageToUserId(receiverId, socketMessage);
    }


    /**
     * 删除事件
     *
     * @param jsonObject
     */
    public void eventDeleteHandle(JSONObject jsonObject) {
        //接收对象id
        String senderId = jsonObject.getString("senderId");
        if (StringUtils.isBlank(senderId)) {
            return;
        }
        //聊天消息对象转换
        SocketMessage socketMessage = SocketMessage.eventSendData(jsonObject, EventCodes.EVENT_DELETE_TALK);
        //给指定用户id通知一份消息
        threadSendMessageToUserId(senderId, socketMessage);
    }

    /**
     * 在线状态事件
     *
     * @param jsonObject
     */
    public void eventOnlineStatusHandle(JSONObject jsonObject) {
        //用户id
        String userId = jsonObject.getString("userId");
        if (StringUtils.isBlank(userId)) {
            return;
        }
        List<String> userIdList = chatUsersFriendsMapper.queryChatUsersFriendsListByUserId(userId);
        for (String uId : userIdList) {
            //聊天消息对象转换
            SocketMessage socketMessage = SocketMessage.eventSendData(jsonObject, EventCodes.EVENT_ONLINE_STATUS);
            //给指定用户id通知一份消息
            threadSendMessageToUserId(uId, socketMessage);
        }
    }


    /**
     * 发送消息
     *
     * @param receiverId
     * @param socketMessage
     */
    public void threadSendMessageToUserId(String receiverId, SocketMessage socketMessage) {
        //给指定用户id通知一份消息
        ThreadUtils.execute(() -> SocketUtil.sendMessageToUserId(receiverId, socketMessage));
    }

}
