package com.duojuhe.coremodule.chat.im.mapper;


import com.duojuhe.coremodule.chat.im.entity.ChatEmoticonCategory;
import com.tkmapper.TkMapper;

public interface ChatEmoticonCategoryMapper extends TkMapper<ChatEmoticonCategory> {

}