package com.duojuhe.coremodule.chat.im.mapper;

import com.duojuhe.coremodule.chat.im.entity.ChatUsersFriends;
import com.duojuhe.coremodule.chat.im.pojo.dto.usersfriends.*;
import com.duojuhe.coremodule.system.entity.SystemUser;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChatUsersFriendsMapper extends TkMapper<ChatUsersFriends> {
    /**
     *获取我的好友list 获取好友列表服务接口
     * @param req
     * @return
     */
    List<QueryMyChatUsersFriendsPageRes> queryMyChatUsersFriendsPageResList(@Param("req") QueryMyChatUsersFriendsPageReq req);

    /**
     *根据手机号码或关键词精确搜索 搜索联系人 搜索联系人
     * @param req
     * @return
     */
    SystemUser querySystemUserBySearchKeyWords(@Param("req") SearchFriendsReq req);

    /**
     * 根据用户id查询用户好友ID集合
     * @param userId
     * @return
     */
    List<String> queryChatUsersFriendsListByUserId(@Param("userId") String userId);

    /**
     * 根据用户id查询用户详情
     * @param userId
     * @return
     */
    QueryChatUsersDetailRes queryChatUsersDetailResByUserId(@Param("userId") String userId);


    /**
     * 根据好友关系id查询用户好友信息
     * @param id
     * @return
     */
    ChatUsersFriendsRes queryChatUsersFriendsResById(@Param("id") String id);
}