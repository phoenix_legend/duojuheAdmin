package com.duojuhe.coremodule.chat.im.mapper;


import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecordDelete;
import com.tkmapper.TkMapper;

public interface ChatTalkRecordDeleteMapper extends TkMapper<ChatTalkRecordDelete> {

}