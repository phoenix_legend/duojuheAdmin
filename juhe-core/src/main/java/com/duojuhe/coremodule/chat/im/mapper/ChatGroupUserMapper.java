package com.duojuhe.coremodule.chat.im.mapper;


import com.duojuhe.coremodule.chat.im.entity.ChatGroupUser;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user.QueryChatGroupUserPageRes;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user.ChatGroupUserRes;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user.QueryInviteChatGroupUserPageReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup.user.QueryInviteChatGroupUserPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChatGroupUserMapper extends TkMapper<ChatGroupUser> {

    /**
     * 根据分组id查询用户记录集合
     * @param groupId
     * @return
     */
    List<ChatGroupUser> queryChatGroupUserListByGroupId(@Param("groupId") String groupId);


    /**
     * 获取群组成员列表
     * @param groupId
     * @return
     */
    List<QueryChatGroupUserPageRes> queryChatGroupUserPageResListByGroupId(@Param("groupId") String groupId);


    /**
     * 查询能邀请的用户列表 获取用户可邀请加入群组的好友列表
     * @param req
     * @return
     */
    List<QueryInviteChatGroupUserPageRes> queryInviteChatGroupUserPageResList(@Param("req") QueryInviteChatGroupUserPageReq req);


    /**
     * 根据id查询群组用户相关信息
     * @param id
     * @return
     */
    ChatGroupUserRes queryChatGroupUserResById(@Param("id") String id);
}