package com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.sendtalk;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SendTalkFileReq extends SendTalkReq {
    @ApiModelProperty(value = "文件系统名称", example = "3",required=true)
    @NotBlank(message = "文件系统名称不能为空")
    @Length(max = 255, message = "文件系统名称不得超过{max}位字符")
    private String hashName;
}
