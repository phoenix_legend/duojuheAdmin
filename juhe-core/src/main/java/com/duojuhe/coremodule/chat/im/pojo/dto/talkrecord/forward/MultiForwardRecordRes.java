package com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.forward;

import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecord;
import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecordCode;
import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecordFile;
import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MultiForwardRecordRes extends BaseBean {

    @ApiModelProperty(value = "消息记录", required = true)
    private ChatTalkRecord chatTalkRecord;

    @ApiModelProperty(value = "文件消息")
    private ChatTalkRecordFile chatTalkRecordFile;

    @ApiModelProperty(value = "代码片段")
    private ChatTalkRecordCode chatTalkRecordCode;
}
