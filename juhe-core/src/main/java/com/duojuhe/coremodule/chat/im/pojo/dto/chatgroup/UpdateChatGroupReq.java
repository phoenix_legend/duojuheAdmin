package com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateChatGroupReq extends ChatGroupIdReq {
    @ApiModelProperty(value = "群组名称", example = "1",required=true)
    @NotBlank(message = "群组名称不能为空")
    @Length(max = 50, message = "群组名称不得超过{max}位字符")
    private String groupName;

    @ApiModelProperty(value = "群组描述", example = "1",required=true)
    @NotBlank(message = "群组描述不能为空")
    @Length(max = 255, message = "群组描述不得超过{max}位字符")
    private String groupIntroduce;

    @ApiModelProperty(value = "群头像", example = "1")
    @Length(max = 255, message = "群头像不得超过{max}位字符")
    private String groupAvatar;
}
