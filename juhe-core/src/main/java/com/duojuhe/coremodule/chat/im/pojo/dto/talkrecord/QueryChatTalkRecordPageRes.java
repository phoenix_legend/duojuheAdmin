package com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord;

import com.duojuhe.websocket.subscriber.message.talk.ChatTalkRecordDto;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryChatTalkRecordPageRes extends ChatTalkRecordDto {
    @ApiModelProperty(value = "主键", required = true)
    private String recordId;

    @ApiModelProperty(value = "对话类型[1:私信;2:群聊;]")
    private Integer talkType;


    @ApiModelProperty(value = "发送者ID（-1:代表系统消息 >-1: 用户ID）")
    private String userId;

    @ApiModelProperty(value = "接收者ID（用户ID 或 群ID）")
    private String receiverId;

    @ApiModelProperty(value = "是否撤回消息[0:否;1:是]")
    private Integer isRevoke;

    @ApiModelProperty(value = "是否重要消息[0:否;1:是;]")
    private Integer isMark;

    @ApiModelProperty(value = "是否已读[0:否;1:是;]")
    private Integer isRead;

    @ApiModelProperty(value = "引用消息ID")
    private String quoteId;

    @ApiModelProperty(value = "@好友 、 多个用英文逗号 “,” 拼接 (0:代表所有人)")
    private String warnUsers;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    private Date updateTime;

    @ApiModelProperty(value = "创建者id")
    private String createUserId;

    @ApiModelProperty(value = "文本消息 {@nickname@}")
    private String content;

    @ApiModelProperty(value = "姓名", example = "1")
    private String realName;

    @ApiModelProperty(value = "登录名", example = "1")
    private String loginName;

    @ApiModelProperty(value = "手机号码", example = "1")
    private String mobileNumber;

    @ApiModelProperty(value = "头像", example = "1")
    private String headPortrait;

    @ApiModelProperty(value = "好友备注")
    private String friendRemark;
}
