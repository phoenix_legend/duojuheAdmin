package com.duojuhe.coremodule.chat.im.mapper;


import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecordCode;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChatTalkRecordCodeMapper extends TkMapper<ChatTalkRecordCode> {
    /**
     * 根据消息id 集合 查询关联的代码记录
     * @param recordIdList
     * @return
     */
    List<ChatTalkRecordCode> queryChatTalkRecordCodeListByRecordIdList(@Param("recordIdList")  List<String> recordIdList);


    /**
     * 根据fileId集合 查询关联的代码记录
     * @param codeIdList
     * @return
     */
    List<ChatTalkRecordCode> queryChatTalkRecordCodeListByCodeIdList(@Param("codeIdList")  List<String> codeIdList);
}