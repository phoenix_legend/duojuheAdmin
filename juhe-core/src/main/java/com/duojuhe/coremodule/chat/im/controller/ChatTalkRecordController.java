package com.duojuhe.coremodule.chat.im.controller;

import com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.*;
import com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.forward.QueryChatTalkRecordForwardPageReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.forward.QueryChatTalkRecordForwardPageRes;
import com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.sendtalk.*;
import com.duojuhe.coremodule.chat.im.service.ChatTalkRecordService;
import com.duojuhe.websocket.subscriber.message.talk.bean.vote.StatisticsRes;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/chatTalkRecord/")
@Api(tags = {"【IM聊天记录】IM聊天记录相关接口"})
@Slf4j
public class ChatTalkRecordController {
    @Resource
    private ChatTalkRecordService chatTalkRecordService;

    @ApiOperation(value = "【聊天记录】分页查询聊天记录接口")
    @PostMapping(value = "queryChatTalkRecordPageResList")
    public ServiceResult<PageResult<List<QueryChatTalkRecordPageRes>>> queryChatTalkRecordPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryChatTalkRecordPageReq req) {
        return chatTalkRecordService.queryChatTalkRecordPageResList(req);
    }


    @ApiOperation(value = "【查找用户聊天】查找用户聊天记录服务接口")
    @PostMapping(value = "findChatTalkRecordPageResList")
    public ServiceResult<PageResult<List<QueryChatTalkRecordPageRes>>> findChatTalkRecordPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryChatTalkRecordPageReq req) {
        return chatTalkRecordService.findChatTalkRecordPageResList(req);
    }


    @ApiOperation(value = "【获取转发会话记录详情列表服务接口】获取转发会话记录详情列表服务接口")
    @PostMapping(value = "queryChatTalkRecordForwardPageResList")
    public ServiceResult<PageResult<List<QueryChatTalkRecordForwardPageRes>>> queryChatTalkRecordForwardPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryChatTalkRecordForwardPageReq req) {
        return chatTalkRecordService.queryChatTalkRecordForwardPageResList(req);
    }


    @ApiOperation(value = "【发送代码块消息服务接口】")
    @PostMapping(value = "sendTalkCodeBlock")
    public ServiceResult sendTalkCodeBlock(@Valid @RequestBody @ApiParam(value = "入参类") SendTalkCodeBlockReq req) {
        return chatTalkRecordService.sendTalkCodeBlock(req);
    }

    @ApiOperation(value = "【发送文件消息服务接口】")
    @PostMapping(value = "sendTalkFile")
    public ServiceResult sendTalkFile(@Valid @RequestBody @ApiParam(value = "入参类") SendTalkFileReq req)  throws Exception{
        return chatTalkRecordService.sendTalkFile(req);
    }

    @ApiOperation(value = "【发送图片消息服务接口】")
    @PostMapping(value = "sendTalkImage")
    public ServiceResult sendTalkImage(@RequestParam("file") MultipartFile file, @Valid @ApiParam(value = "入参类") SendTalkImageReq req) {
        return chatTalkRecordService.sendTalkImage(file,req);
    }

    @ApiOperation(value = "【发送表情包服务接口】")
    @PostMapping(value = "sendTalkEmoticon")
    public ServiceResult sendTalkEmoticon(@Valid @RequestBody @ApiParam(value = "入参类") SendTalkEmoticonReq req) {
        return chatTalkRecordService.sendTalkEmoticon(req);
    }

    @ApiOperation(value = "【转发消息服务接口】")
    @PostMapping(value = "forwardTalkRecord")
    public ServiceResult forwardTalkRecord(@Valid @RequestBody @ApiParam(value = "入参类") ForwardTalkRecordReq req) {
        return chatTalkRecordService.forwardTalkRecord(req);
    }

    @ApiOperation(value = "【发送投票服务接口】")
    @PostMapping(value = "sendTalkVote")
    public ServiceResult sendTalkVote(@Valid @RequestBody @ApiParam(value = "入参类") SendTalkVoteReq req) {
        return chatTalkRecordService.sendTalkVote(req);
    }

    @ApiOperation(value = "【提交投票服务接口】")
    @PostMapping(value = "confirmSubmitTalkVote")
    public ServiceResult<StatisticsRes> confirmSubmitTalkVote(@Valid @RequestBody @ApiParam(value = "入参类") ConfirmSubmitTalkVoteReq req) {
        return chatTalkRecordService.confirmSubmitTalkVote(req);
    }

    @ApiOperation(value = "【根据会话id查询投票记录汇总信息】")
    @PostMapping(value = "queryChatTalkRecordVoteStatisticsResByRecordId")
    public ServiceResult<StatisticsRes> queryChatTalkRecordVoteStatisticsResByRecordId(@Valid @RequestBody @ApiParam(value = "入参类") ChatTalkRecordIdReq req) {
        return chatTalkRecordService.queryChatTalkRecordVoteStatisticsResByRecordId(req);
    }

    @ApiOperation(value = "【撤回消息服务接口】")
    @PostMapping(value = "revokeRecordByRecordId")
    public ServiceResult revokeRecordByRecordId(@Valid @RequestBody @ApiParam(value = "入参类") ChatTalkRecordIdReq req) {
        return chatTalkRecordService.revokeRecordByRecordId(req);
    }

    @ApiOperation(value = "【删除消息服务接口】")
    @PostMapping(value = "deleteRecordByRecordId")
    public ServiceResult deleteRecordByRecordId(@Valid @RequestBody @ApiParam(value = "入参类") ChatTalkRecordIdReq req) {
        return chatTalkRecordService.deleteRecordByRecordId(req);
    }

    @ApiOperation(value = "【批量删除消息服务接口】")
    @PostMapping(value = "batchDeleteRecordByRecordIdList")
    public ServiceResult batchDeleteRecordByRecordIdList(@Valid @RequestBody @ApiParam(value = "入参类") BatchDeleteRecordIdListReq req) {
        return chatTalkRecordService.batchDeleteRecordByRecordIdList(req);
    }

}
