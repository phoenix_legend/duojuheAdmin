package com.duojuhe.coremodule.chat.im.mapper;


import com.duojuhe.coremodule.chat.im.entity.ChatUsersFriendsApply;
import com.duojuhe.coremodule.chat.im.pojo.dto.friendsapply.QueryMyChatUsersFriendsApplyPageReq;
import com.duojuhe.coremodule.chat.im.pojo.dto.friendsapply.QueryMyChatUsersFriendsApplyPageRes;
import com.duojuhe.common.bean.DataScopeFilterBean;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChatUsersFriendsApplyMapper extends TkMapper<ChatUsersFriendsApply> {
    /**
     * 【好友申请服务接口】分页查询好友申请服务接口
     * @param req
     * @return
     */
    List<QueryMyChatUsersFriendsApplyPageRes> queryMyChatUsersFriendsApplyPageResList(@Param("req") QueryMyChatUsersFriendsApplyPageReq req);

    /**
     * 查询好友申请未读未处理数量服务接口
     *
     * @return
     */
    int queryMyChatUsersFindFriendApplyNum(@Param("req") DataScopeFilterBean req);

    /**
     * 根据用户id统计好友申请数量
     *
     * @return
     */
    int selectCountMyFriendApplyNumByUserId(@Param("userId") String userId);


}