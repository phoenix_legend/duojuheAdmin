package com.duojuhe.coremodule.chat.im.pojo.dto.emoticon.item;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChatEmoticonItemRes extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String itemId;

    @ApiModelProperty(value = "名称", required = true)
    private String itemName;

    @ApiModelProperty(value = "图片地址", required = true)
    private String emoticonUrl;

    @ApiModelProperty(value = "是否用户收藏0否1是", required = true,hidden = true)
    private Integer isCollect;
}
