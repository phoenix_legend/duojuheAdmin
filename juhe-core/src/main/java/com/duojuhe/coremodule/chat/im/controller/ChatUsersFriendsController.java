package com.duojuhe.coremodule.chat.im.controller;

import com.duojuhe.coremodule.chat.im.pojo.dto.usersfriends.*;
import com.duojuhe.coremodule.chat.im.service.ChatUsersFriendsService;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.system.pojo.dto.user.SystemUserIdReq;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/chatUsersFriends/")
@Api(tags = {"【IM好友】IM好友相关接口"})
@Slf4j
public class ChatUsersFriendsController {
    @Resource
    private ChatUsersFriendsService chatUsersFriendsService;

    @ApiOperation(value = "解除好友关系")
    @PostMapping(value = "removeChatFriendByFriendId")
    public ServiceResult removeChatFriendByFriendId(@Valid @RequestBody @ApiParam(value = "入参类") ChatUsersFriendsIdReq req) {
        return chatUsersFriendsService.removeChatFriendByFriendId(req);
    }


    @ApiOperation(value = "修改好友备注")
    @PostMapping(value = "updateFriendRemark")
    public ServiceResult updateFriendRemark(@Valid @RequestBody @ApiParam(value = "入参类") UpdateFriendRemarkReq req) {
        return chatUsersFriendsService.updateFriendRemark(req);
    }

    @ApiOperation(value = "获取我的好友list 获取好友列表服务接口")
    @PostMapping(value = "queryMyChatUsersFriendsPageResList")
    public ServiceResult<PageResult<List<QueryMyChatUsersFriendsPageRes>>> queryMyChatUsersFriendsPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryMyChatUsersFriendsPageReq req) {
        return chatUsersFriendsService.queryMyChatUsersFriendsPageResList(req);
    }

    @ApiOperation(value = "根据手机号码精确搜索")
    @PostMapping(value = "querySearchFriendsUserIdBySearchKeyWords")
    public ServiceResult<String> querySearchFriendsUserIdBySearchKeyWords(@Valid @RequestBody @ApiParam(value = "入参类") SearchFriendsReq req) {
        return chatUsersFriendsService.querySearchFriendsUserIdBySearchKeyWords(req);
    }

    @ApiOperation(value = "根据用户id查询用户详情")
    @PostMapping(value = "queryChatUsersDetailResByUserId")
    public ServiceResult<QueryChatUsersDetailRes> queryChatUsersDetailResByUserId(@Valid @RequestBody @ApiParam(value = "入参类") SystemUserIdReq req) {
        return chatUsersFriendsService.queryChatUsersDetailResByUserId(req.getUserId());
    }
}
