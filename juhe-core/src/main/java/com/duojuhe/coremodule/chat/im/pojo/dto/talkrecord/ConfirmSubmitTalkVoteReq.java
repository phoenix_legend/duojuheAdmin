package com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ConfirmSubmitTalkVoteReq extends BaseBean {
    @ApiModelProperty(value = "主键id",required=true)
    @Length(max = 32, message = "消息记录ID不得超过{max}位字符")
    private String recordId;


    @ApiModelProperty(value = "答案集合",required=true)
    @NotEmpty(message = "答案集合不能为空")
    @Valid
    @NotNull(message = "答案集合不能为空")
    @Size(min = 1,max = 26, message = "答案集合不能为空,最少1项，最多10项")
    private List<String> answerList;

}
