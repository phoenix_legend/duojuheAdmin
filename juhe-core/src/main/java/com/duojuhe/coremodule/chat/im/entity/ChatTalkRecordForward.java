package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("用户聊天记录_合并转发信息表")
@Table(name = "chat_talk_record_forward")
public class ChatTalkRecordForward extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "forward_id")
    @Id
    private String forwardId;

    @ApiModelProperty(value = "记录id")
    @Column(name = "record_id")
    private String recordId;

    @ApiModelProperty(value = "用户id")
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty(value = "创建用户id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "转发的聊天记录ID，多个用','分割")
    @Column(name = "forward_record_ids")
    private String forwardRecordIds;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "记录快照")
    @Column(name = "snapshot")
    private String snapshot;
}