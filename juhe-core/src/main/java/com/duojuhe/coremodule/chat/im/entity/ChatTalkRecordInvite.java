package com.duojuhe.coremodule.chat.im.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("用户聊天记录_入群或退群消息表")
@Table(name = "chat_talk_record_invite")
public class ChatTalkRecordInvite extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "id")
    @Id
    private String id;

    @ApiModelProperty(value = "聊天记录id")
    @Column(name = "record_id")
    private String recordId;

    @ApiModelProperty(value = "通知类型[1:入群通知;2:自动退群;3:管理员踢群]")
    @Column(name = "invite_type")
    private Integer inviteType;

    @ApiModelProperty(value = "操作人的用户ID(邀请人)")
    @Column(name = "operate_user_id")
    private String operateUserId;

    @ApiModelProperty(value = "操作人快照详情")
    @Column(name = "operate_user_snapshot")
    private String operateUserSnapshot;

    @ApiModelProperty(value = "用户ID，多个用 , 分割")
    @Column(name = "user_ids")
    private String userIds;

    @ApiModelProperty(value = "被操作人快照详情")
    @Column(name = "user_snapshot")
    private String userSnapshot;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "创建用户id")
    @Column(name = "create_user_id")
    private String createUserId;
}