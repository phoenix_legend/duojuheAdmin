package com.duojuhe.coremodule.chat.im.pojo.dto.chatgroup;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveChatGroupReq extends BaseBean {
    @ApiModelProperty(value = "群组名称", example = "1",required=true)
    @NotBlank(message = "群组名称不能为空")
    @Length(max = 50, message = "群组名称不得超过{max}位字符")
    private String groupName;

    @ApiModelProperty(value = "用户ID集合", required = true)
    @NotEmpty(message = "群聊人数必须大于俩人")
    @Valid
    @NotNull(message = "群聊人数必须大于俩人")
    @Size(min = 2,max = 100,message = "群聊人数必须大于{min}小于{max}人！")
    private List<String> userIdList;

    @ApiModelProperty(value = "群组描述", example = "1")
    @Length(max = 255, message = "群组描述不得超过{max}位字符")
    private String groupIntroduce;

    @ApiModelProperty(value = "群头像", example = "1")
    @Length(max = 255, message = "群头像不得超过{max}位字符")
    private String groupAvatar;
}
