package com.duojuhe.coremodule.info.pojo.dto.news;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryMyInfoNewsPageRes extends BaseBean {
    @ApiModelProperty(value = "主键id", required = true)
    private String newsId;

    @ApiModelProperty(value = "信息所属分类id", required = true)
    private String categoryId;

    @ApiModelProperty(value = "信息所属分类名称", required = true)
    private String categoryName;

    @ApiModelProperty(value = "信息标题", required = true)
    private String newsTitle;

    @ApiModelProperty(value = "信息缩略图")
    private String newsThumbnail;

    @ApiModelProperty(value = "信息简介")
    private String introduction;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date updateTime;

    @ApiModelProperty(value = "删除时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date deleteTime;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "信息内容")
    private String newsContent;
}