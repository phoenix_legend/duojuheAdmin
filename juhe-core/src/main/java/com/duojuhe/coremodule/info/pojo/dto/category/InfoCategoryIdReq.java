package com.duojuhe.coremodule.info.pojo.dto.category;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class InfoCategoryIdReq extends BaseBean {
    @ApiModelProperty(value = "信息分类ID", example = "1",required=true)
    @NotBlank(message = "信息分类ID不能为空")
    private String categoryId;
}