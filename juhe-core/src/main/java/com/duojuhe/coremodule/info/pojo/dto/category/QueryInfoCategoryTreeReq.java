package com.duojuhe.coremodule.info.pojo.dto.category;

import com.duojuhe.common.bean.DataScopeFilterBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryInfoCategoryTreeReq extends DataScopeFilterBean {
    @ApiModelProperty(value = "分类名称")
    private String categoryName;

    @ApiModelProperty(value = "父级id")
    private String parentId;
}