package com.duojuhe.coremodule.info.pojo.dto.news;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.validation.constraints.NotBlank;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateInfoNewsReq extends SaveInfoNewsReq {
    @ApiModelProperty(value = "新闻ID", example = "1",required=true)
    @NotBlank(message = "新闻ID不能为空")
    private String newsId;
}