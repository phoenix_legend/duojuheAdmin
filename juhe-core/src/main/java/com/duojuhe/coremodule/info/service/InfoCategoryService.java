package com.duojuhe.coremodule.info.service;

import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.info.pojo.dto.category.*;

import java.util.List;

public interface InfoCategoryService {
    /**
     * 【查询所有信息分类】查询可供前端选择使用的信息分类
     * @param req
     * @return
     */
    ServiceResult<List<QueryInfoCategoryTreeRes>> queryInfoCategoryTreeResList(QueryInfoCategoryTreeReq req);


    /**
     * 【查询所有信息分类】查询可供前端选择使用的信息分类
     * @param req
     * @return
     */
    ServiceResult<List<QueryInfoCategoryTreeRes>> querySelectInfoCategoryTreeResList(QueryInfoCategoryTreeReq req);


    /**
     * 【保存】信息分类信息
     * @param req
     * @return
     */
    ServiceResult saveInfoCategory(SaveInfoCategoryReq req);

    /**
     * 【修改】信息分类信息
     * @param req
     * @return
     */
    ServiceResult updateInfoCategory(UpdateInfoCategoryReq req);

    /**
     * 【删除】根据信息分类ID删除信息分类，注意一次仅能删除一个信息分类，存在绑定关系的则不能删除
     * @param req
     * @return
     */
    ServiceResult deleteInfoCategoryByCategoryId(InfoCategoryIdReq req);
}
