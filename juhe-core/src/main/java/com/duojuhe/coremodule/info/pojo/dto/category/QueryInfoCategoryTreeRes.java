package com.duojuhe.coremodule.info.pojo.dto.category;

import com.duojuhe.common.utils.tree.TreeBaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryInfoCategoryTreeRes extends TreeBaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String categoryId;

    @ApiModelProperty(value = "分类名称", required = true)
    private String categoryName;

    @ApiModelProperty(value = "分类路径")
    private String categoryPath;

    @ApiModelProperty(value = "排序")
    private Integer sort;
}