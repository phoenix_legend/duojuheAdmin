package com.duojuhe.coremodule.info.mapper;

import com.duojuhe.coremodule.info.entity.InfoCategory;
import com.duojuhe.coremodule.info.pojo.dto.category.QueryInfoCategoryTreeReq;
import com.duojuhe.coremodule.info.pojo.dto.category.QueryInfoCategoryTreeRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InfoCategoryMapper extends TkMapper<InfoCategory> {
    /**
     * 【下拉选择使用-tree型结构查询】查询可供前端选择使用的信息分类
     * @param req
     * @return
     */
    List<QueryInfoCategoryTreeRes> queryInfoCategoryTreeResList(@Param("req") QueryInfoCategoryTreeReq req);

}