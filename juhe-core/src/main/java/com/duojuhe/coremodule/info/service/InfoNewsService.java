package com.duojuhe.coremodule.info.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.info.pojo.dto.news.*;

import java.util.List;

public interface InfoNewsService {

    /**
     * 【信息详情】根据信息发布ID获取信息详情
     * @param req
     * @return
     */
    ServiceResult<QueryInfoNewsRes> queryInfoNewsResByNewsId(InfoNewsIdReq req);

    /**
     * 【分页查询信息发布list】分页查询信息发布list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryInfoNewsPageRes>>> queryInfoNewsPageResList(QueryInfoNewsPageReq req);


    /**
     * 【分页查询我发布的list】分页查询我发布的list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryMyInfoNewsPageRes>>> queryMyInfoNewsPageResList(QueryMyInfoNewsPageReq req);


    /**
     * 【分页查询我的回收站信息list】分页查询我的回收站信息list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QueryMyInfoNewsPageRes>>> queryRecycleInfoNewsPageResList(QueryMyInfoNewsPageReq req);

    /**
     * 【保存】信息发布信息
     * @param req
     * @return
     */
    ServiceResult saveInfoNews(SaveInfoNewsReq req);

    /**
     * 【修改】信息发布信息
     * @param req
     * @return
     */
    ServiceResult updateInfoNews(UpdateInfoNewsReq req);

    /**
     * 【彻底删除】根据信息发布ID彻底删除信息发布，注意一次仅能彻底删除一个信息发布
     * @param req
     * @return
     */
    ServiceResult deleteInfoNewsByNewsId(InfoNewsIdReq req);


    /**
     * 【回收站中】根据信息发布ID把发布信息放到回收站中
     * @param req
     * @return
     */
    ServiceResult putRecycleInfoNewsByNewsId(InfoNewsIdReq req);

    /**
     * 【把回收站中的信息恢复】根据信息发布ID把回收站中的信息恢复
     * @param req
     * @return
     */
    ServiceResult recoveryRecycleInfoNewsByNewsId(InfoNewsIdReq req);



}
