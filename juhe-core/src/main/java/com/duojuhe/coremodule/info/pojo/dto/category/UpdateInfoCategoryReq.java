package com.duojuhe.coremodule.info.pojo.dto.category;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateInfoCategoryReq extends SaveInfoCategoryReq {
    @ApiModelProperty(value = "信息分类ID", example = "1",required=true)
    @NotBlank(message = "信息分类ID不能为空")
    private String categoryId;
}