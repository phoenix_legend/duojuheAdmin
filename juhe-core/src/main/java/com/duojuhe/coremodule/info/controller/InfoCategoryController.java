package com.duojuhe.coremodule.info.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.info.pojo.dto.category.*;
import com.duojuhe.coremodule.info.service.InfoCategoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/infoCategory/")
@Api(tags = {"【信息分类管理】信息分类管理相关接口"})
@Slf4j
public class InfoCategoryController {
    @Resource
    private InfoCategoryService infoCategoryService;

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【查询信息分类】分类管理界面使用")
    @PostMapping(value = "queryInfoCategoryTreeResList")
    public ServiceResult<List<QueryInfoCategoryTreeRes>> queryInfoCategoryTreeResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryInfoCategoryTreeReq req) {
        return infoCategoryService.queryInfoCategoryTreeResList(req);
    }



    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【查询所有信息分类】查询可供前端选择使用的信息分类")
    @PostMapping(value = "querySelectInfoCategoryTreeResList")
    public ServiceResult<List<QueryInfoCategoryTreeRes>> querySelectInfoCategoryTreeResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryInfoCategoryTreeReq req) {
        return infoCategoryService.querySelectInfoCategoryTreeResList(req);
    }

    @ApiOperation(value = "【保存】信息分类信息")
    @PostMapping(value = "saveInfoCategory")
    @OperationLog(moduleName = LogEnum.ModuleName.INFO_CATEGORY, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveInfoCategory(@Valid @RequestBody @ApiParam(value = "入参类") SaveInfoCategoryReq req) {
        return infoCategoryService.saveInfoCategory(req);
    }


    @ApiOperation(value = "【修改】信息分类信息")
    @PostMapping(value = "updateInfoCategory")
    @OperationLog(moduleName = LogEnum.ModuleName.INFO_CATEGORY, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateInfoCategory(@Valid @RequestBody @ApiParam(value = "入参类") UpdateInfoCategoryReq req) {
        return infoCategoryService.updateInfoCategory(req);
    }

    @ApiOperation(value = "【删除】根据信息分类ID删除信息分类，注意一次仅能删除一个信息分类，存在绑定关系的则不能删除")
    @PostMapping(value = "deleteInfoCategoryByCategoryId")
    @OperationLog(moduleName = LogEnum.ModuleName.INFO_CATEGORY, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteInfoCategoryByCategoryId(@Valid @RequestBody @ApiParam(value = "入参类") InfoCategoryIdReq req) {
        return infoCategoryService.deleteInfoCategoryByCategoryId(req);
    }
}
