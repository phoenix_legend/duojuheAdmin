package com.duojuhe.coremodule.info.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.info.pojo.dto.news.*;
import com.duojuhe.coremodule.info.service.InfoNewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/infoNews/")
@Api(tags = {"【信息发布管理】信息发布管理相关接口"})
@Slf4j
public class InfoNewsController {
    @Resource
    private InfoNewsService infoNewsService;

    @ApiOperation(value = "【信息详情】根据信息发布ID获取信息详情")
    @PostMapping(value = "queryInfoNewsResByNewsId")
    public ServiceResult<QueryInfoNewsRes> queryInfoNewsResByNewsId(@Valid @RequestBody @ApiParam(value = "入参类") InfoNewsIdReq req) {
        return infoNewsService.queryInfoNewsResByNewsId(req);
    }


    @ApiOperation(value = "【分页查询】分页查询信息发布list")
    @PostMapping(value = "queryInfoNewsPageResList")
    public ServiceResult<PageResult<List<QueryInfoNewsPageRes>>> queryInfoNewsPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryInfoNewsPageReq req) {
        return infoNewsService.queryInfoNewsPageResList(req);
    }



    @ApiOperation(value = "【分页查询我发布的】分页查询我发布的list")
    @PostMapping(value = "queryMyInfoNewsPageResList")
    public ServiceResult<PageResult<List<QueryMyInfoNewsPageRes>>> queryMyInfoNewsPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryMyInfoNewsPageReq req) {
        return infoNewsService.queryMyInfoNewsPageResList(req);
    }

    @ApiOperation(value = "【分页查询我的回收站信息list】分页查询我的回收站信息list")
    @PostMapping(value = "queryRecycleInfoNewsPageResList")
    public ServiceResult<PageResult<List<QueryMyInfoNewsPageRes>>> queryRecycleInfoNewsPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryMyInfoNewsPageReq req) {
        return infoNewsService.queryRecycleInfoNewsPageResList(req);
    }


    @ApiOperation(value = "【保存】信息发布")
    @PostMapping(value = "saveInfoNews")
    @OperationLog(moduleName = LogEnum.ModuleName.INFO_NEWS, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveInfoNews(@Valid @RequestBody @ApiParam(value = "入参类") SaveInfoNewsReq req) {
        return infoNewsService.saveInfoNews(req);
    }

    @ApiOperation(value = "【修改】信息发布")
    @PostMapping(value = "updateInfoNews")
    @OperationLog(moduleName = LogEnum.ModuleName.INFO_NEWS, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateInfoNews(@Valid @RequestBody @ApiParam(value = "入参类") UpdateInfoNewsReq req) {
        return infoNewsService.updateInfoNews(req);
    }

    @ApiOperation(value = "【删除】根据ID删除信息发布，注意一次仅能删除一个信息发布")
    @PostMapping(value = "deleteInfoNewsByNewsId")
    @OperationLog(moduleName = LogEnum.ModuleName.INFO_NEWS, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteInfoNewsByNewsId(@Valid @RequestBody @ApiParam(value = "入参类") InfoNewsIdReq req) {
        return infoNewsService.deleteInfoNewsByNewsId(req);
    }

    @ApiOperation(value = "【回收站中】根据信息发布ID把发布信息放到回收站中")
    @PostMapping(value = "putRecycleInfoNewsByNewsId")
    @OperationLog(moduleName = LogEnum.ModuleName.INFO_NEWS, operationType = LogEnum.OperationType.RECYCLE)
    public ServiceResult putRecycleInfoNewsByNewsId(@Valid @RequestBody @ApiParam(value = "入参类") InfoNewsIdReq req) {
        return infoNewsService.putRecycleInfoNewsByNewsId(req);
    }

    @ApiOperation(value = "【回收站中的信息恢复】根据信息发布ID把回收站中的信息恢复")
    @PostMapping(value = "recoveryRecycleInfoNewsByNewsId")
    @OperationLog(moduleName = LogEnum.ModuleName.INFO_NEWS, operationType = LogEnum.OperationType.RECYCLE_RECOVERY)
    public ServiceResult recoveryRecycleInfoNewsByNewsId(@Valid @RequestBody @ApiParam(value = "入参类") InfoNewsIdReq req) {
        return infoNewsService.recoveryRecycleInfoNewsByNewsId(req);
    }

}
