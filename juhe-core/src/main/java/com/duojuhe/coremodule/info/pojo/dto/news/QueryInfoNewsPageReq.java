package com.duojuhe.coremodule.info.pojo.dto.news;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryInfoNewsPageReq extends QueryMyInfoNewsPageReq {
    @ApiModelProperty(value = "创建部门名称")
    private String createDeptName;

    @ApiModelProperty(value = "创建者姓名", example = "1")
    private String createUserName;
}