package com.duojuhe.coremodule.info.mapper;

import com.duojuhe.coremodule.info.entity.InfoNews;
import com.duojuhe.coremodule.info.pojo.dto.news.*;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InfoNewsMapper extends TkMapper<InfoNews> {

    /**
     * 【信息详情】根据信息发布ID获取信息详情
     * @param newsId
     * @return
     */
    QueryInfoNewsRes queryInfoNewsResByNewsId(@Param("newsId") String newsId);

    /**
     * 分页查询 根据条件查询发布信息list
     *
     * @return
     */
    List<QueryInfoNewsPageRes> queryInfoNewsPageResList(@Param("req") QueryInfoNewsPageReq req,@Param("isDelete") Integer isDelete);


    /**
     * 分页查询 根据条件查询我的发布信息list
     *
     * @return
     */
    List<QueryMyInfoNewsPageRes> queryMyInfoNewsPageResList(@Param("req") QueryMyInfoNewsPageReq req, @Param("isDelete") Integer isDelete);


}