package com.duojuhe.coremodule.info.pojo.dto.news;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveInfoNewsReq extends BaseBean {

    @ApiModelProperty(value = "信息所属分类id", required = true)
    @NotBlank(message = "所属分类ID不能为空")
    private String categoryId;

    @ApiModelProperty(value = "信息标题", required = true)
    @NotBlank(message = "信息标题不能为空")
    @Length(max = 250, message = "信息标题不得超过{max}位字符")
    private String newsTitle;

    @ApiModelProperty(value = "信息缩略图")
    @Length(max = 255, message = "缩略图不得超过{max}位字符")
    private String newsThumbnail;

    @ApiModelProperty(value = "信息简介",required=true)
    @Length(max = 250, message = "信息简介不得超过{max}位字符")
    @NotBlank(message = "信息简介不能为空")
    private String introduction;

    @ApiModelProperty(value = "排序，只能为纯数字", example = "1",required=true)
    @NotNull(message = "排序不能为空")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;

    @ApiModelProperty(value = "信息内容",required=true)
    @NotBlank(message = "信息内容不能为空")
    private String newsContent;
}