package com.duojuhe.coremodule.info.pojo.dto.news;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryInfoNewsPageRes extends QueryMyInfoNewsPageRes {

    @ApiModelProperty(value = "创建部门id")
    private String createDeptId;

    @ApiModelProperty(value = "创建部门名称")
    private String createDeptName;

    @ApiModelProperty(value = "创建者Id", example = "1")
    private String createUserId;

    @ApiModelProperty(value = "创建者姓名", example = "1")
    private String createUserName;
}