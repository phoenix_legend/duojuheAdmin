package com.duojuhe.coremodule.info.pojo.dto.news;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryMyInfoNewsPageReq extends PageHead {
    @ApiModelProperty(value = "信息所属分类id", required = true)
    private String categoryId;

    @ApiModelProperty(value = "信息所属分类名称", required = true)
    private String categoryName;

    @ApiModelProperty(value = "信息标题", required = true)
    private String newsTitle;

    @ApiModelProperty(value = "信息简介")
    private String introduction;

    @ApiModelProperty(value = "信息内容")
    private String newsContent;

    @ApiModelProperty(value = "开始日期yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}