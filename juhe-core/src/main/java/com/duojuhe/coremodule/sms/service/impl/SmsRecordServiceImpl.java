package com.duojuhe.coremodule.sms.service.impl;

import com.duojuhe.cache.SystemDictCache;
import com.duojuhe.common.annotation.DataScopeFilter;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.page.PageHelperUtil;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.sms.mapper.SmsRecordMapper;
import com.duojuhe.coremodule.sms.pojo.record.HandleSmsRecordDictNameColorRes;
import com.duojuhe.coremodule.sms.pojo.record.QuerySmsRecordPageReq;
import com.duojuhe.coremodule.sms.pojo.record.QuerySmsRecordPageRes;
import com.duojuhe.coremodule.sms.service.SmsRecordService;
import com.duojuhe.coremodule.system.entity.SystemDict;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class SmsRecordServiceImpl  extends BaseService implements SmsRecordService {
    @Resource
    private SystemDictCache systemDictCache;
    @Resource
    private SmsRecordMapper smsRecordMapper;
    /**
     * 【分页查询】根据条件查询短信发送记录list
     * @param req
     * @return
     */
    @DataScopeFilter
    @Override
    public ServiceResult<PageResult<List<QuerySmsRecordPageRes>>> querySmsRecordPageResList(QuerySmsRecordPageReq req) {
        PageHelperUtil.orderByAndStartPage(req, "createTime desc, recordId desc");
        List<QuerySmsRecordPageRes> list = smsRecordMapper.querySmsRecordPageResList(req);
        list.forEach(this::setHandleSmsRecordDictNameColor);
        return PageHelperUtil.returnServiceResult(req, list);
    }


    /*====================================私有方法开始================================================**/

    /**
     * 处理返回值的数据字典
     *
     * @param res
     */
    private void setHandleSmsRecordDictNameColor(HandleSmsRecordDictNameColorRes res) {
        if (res == null) {
            return;
        }
        SystemDict sendStatusModule = systemDictCache.getSystemDictByDictCode(res.getSendStatusCode());
        res.setSendStatusName(sendStatusModule.getDictName());
        res.setSendStatusColor(sendStatusModule.getDictColor());
    }
    /*====================================私有方法end================================================**/

}
