package com.duojuhe.coremodule.sms.pojo.sendtask;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySmsSendTaskRes extends BaseBean {
    @ApiModelProperty(value = "主键id", required = true)
    private String taskId;

    @ApiModelProperty(value = "任务名称", required = true)
    private String taskName;

    @ApiModelProperty(value = "渠道id", required = true)
    private String channelId;

    @ApiModelProperty(value = "渠道编码", required = true)
    private String channelCode;

    @ApiModelProperty(value = "渠道名称")
    private String channelName;

    @ApiModelProperty(value = "定时发送的定时时间yyyy-MM-dd HH:MM")
    @JsonFormat(pattern= DateUtils.yyyy_MM_ddHHmm,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.yyyy_MM_ddHHmm)
    private Date timingTime;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "模板ID", required = true)
    private String templateId;

    @ApiModelProperty(value = "模板签名", required = true)
    private String templateSign;

    @ApiModelProperty(value = "短信模板内容")
    private String templateContent;

    @ApiModelProperty(value = "任务状态")
    private String taskStatusCode;

    @ApiModelProperty(value = "任务类型，定时发送，立即发送")
    private String taskTypeCode;

    @ApiModelProperty(value = "任务模式，自定义号码，系统内部联系人号码")
    private String taskModeCode;

    @ApiModelProperty(value = "任务内容，多个逗号分隔")
    private String taskContent;

}