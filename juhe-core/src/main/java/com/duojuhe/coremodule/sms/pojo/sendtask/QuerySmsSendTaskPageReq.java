package com.duojuhe.coremodule.sms.pojo.sendtask;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySmsSendTaskPageReq extends PageHead {

    @ApiModelProperty(value = "任务名称")
    private String taskName;

    @ApiModelProperty(value = "任务状态，已发送，待发送，已取消")
    private String taskStatusCode;

    @ApiModelProperty(value = "任务类型，定时发送，立即发送")
    private String taskTypeCode;

    @ApiModelProperty(value = "任务模式，自定义号码，系统内部联系人号码")
    private String taskModeCode;

    @ApiModelProperty(value = "渠道编码")
    private String channelCode;

    @ApiModelProperty(value = "渠道名称")
    private String channelName;

    @ApiModelProperty(value = "模板签名")
    private String templateSign;

    @ApiModelProperty(value = "短信模板内容")
    private String templateContent;

    @ApiModelProperty(value = "开始日期yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}