package com.duojuhe.coremodule.sms.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("短信模板")
@Table(name = "sms_template")
public class SmsTemplate extends BaseBean {
    @ApiModelProperty(value = "模板主键", required = true)
    @Column(name = "template_id")
    @Id
    private String templateId;

    @ApiModelProperty(value = "模板编码", required = true)
    @Column(name = "template_code")
    private String templateCode;

    @ApiModelProperty(value = "模板名称", required = true)
    @Column(name = "template_name")
    private String templateName;

    @ApiModelProperty(value = "模板签名", required = true)
    @Column(name = "template_sign")
    private String templateSign;

    @ApiModelProperty(value = "短信模板内容")
    @Column(name = "content")
    private String content;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建人ID")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "创建部门ID")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "是否内置，取数据字典", required = true)
    @Column(name = "built_in")
    private String builtIn;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "模板归属服务商id", required = true)
    @Column(name = "isp_id")
    private String ispId;

    @ApiModelProperty(value = "模板归属发送渠道id", required = true)
    @Column(name = "channel_id")
    private String channelId;
}