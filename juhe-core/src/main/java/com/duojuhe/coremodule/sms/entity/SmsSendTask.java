package com.duojuhe.coremodule.sms.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("短信发送任务表")
@Table(name = "sms_send_task")
public class SmsSendTask extends BaseBean {
    @ApiModelProperty(value = "主键id任务id", required = true)
    @Column(name = "task_id")
    @Id
    private String taskId;

    @ApiModelProperty(value = "任务名称")
    @Column(name = "task_name")
    private String taskName;

    @ApiModelProperty(value = "任务状态")
    @Column(name = "task_status_code")
    private String taskStatusCode;

    @ApiModelProperty(value = "任务类型，定时发送，立即发送")
    @Column(name = "task_type_code")
    private String taskTypeCode;

    @ApiModelProperty(value = "任务模式，自定义号码，系统内部联系人号码")
    @Column(name = "task_mode_code")
    private String taskModeCode;

    @ApiModelProperty(value = "任务发送ip")
    @Column(name = "task_send_ip")
    private String taskSendIp;


    @ApiModelProperty(value = "渠道id", required = true)
    @Column(name = "channel_id")
    private String channelId;

    @ApiModelProperty(value = "渠道编码", required = true)
    @Column(name = "channel_code")
    private String channelCode;

    @ApiModelProperty(value = "渠道名称")
    @Column(name = "channel_name")
    private String channelName;

    @ApiModelProperty(value = "isp主键id", required = true)
    @Column(name = "isp_id")
    private String ispId;

    @ApiModelProperty(value = "服务商编码")
    @Column(name = "isp_code")
    private String ispCode;

    @ApiModelProperty(value = "服务商名称")
    @Column(name = "isp_name")
    private String ispName;

    @ApiModelProperty(value = "任务执行时间")
    @Column(name = "task_execute_time")
    private Date taskExecuteTime;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "定时发送的定时时间yyyy-MM-dd HH:MM")
    @Column(name = "timing_time")
    private Date timingTime;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户id")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "描述说明")
    @Column(name = "description")
    private String description;

    @ApiModelProperty(value = "渠道服务商配置")
    @Column(name = "channel_isp_attribute")
    private String channelIspAttribute;

    @ApiModelProperty(value = "服务商属性 json 格式")
    @Column(name = "isp_attribute")
    private String ispAttribute;


    @ApiModelProperty(value = "模板主键", required = true)
    @Column(name = "template_id")
    private String templateId;

    @ApiModelProperty(value = "模板编码", required = true)
    @Column(name = "template_code")
    private String templateCode;

    @ApiModelProperty(value = "模板名称", required = true)
    @Column(name = "template_name")
    private String templateName;

    @ApiModelProperty(value = "模板签名", required = true)
    @Column(name = "template_sign")
    private String templateSign;

    @ApiModelProperty(value = "短信模板内容")
    @Column(name = "template_content")
    private String templateContent;
}