package com.duojuhe.coremodule.sms.pojo.record;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@AllArgsConstructor
@Getter
@Setter
public class SmsSendDto extends BaseBean {
    @ApiModelProperty(value = "手机号码", required = true)
    private String mobileNumber;

    @ApiModelProperty(value = "发送ip")
    private String smsSendIp;

    @ApiModelProperty(value = "短信模板编码", required = true)
    private String templateCode;

    @ApiModelProperty(value = "短信模板内容")
    private String templateContent;

    @ApiModelProperty(value = "短信模板扩展参数 json", required = true)
    private String templateParam;

    @ApiModelProperty(value = "短信发送渠道账号", required = true)
    private String channelAccount;

    @ApiModelProperty(value = "短信发送渠道密码", required = true)
    private String channelPassword;

    @ApiModelProperty(value = "短信签名", required = true)
    private String signName;

    @ApiModelProperty(value = "短信发送服务商编码", required = true)
    private String ispCode;

    public SmsSendDto(SmsRepeatRecordDto recordDto){
        this.mobileNumber = recordDto.getMobileNumber();
        this.smsSendIp = recordDto.getSmsSendIp();
        this.templateCode = recordDto.getTemplateCode();
        this.templateContent = recordDto.getTemplateContent();
        this.templateParam = recordDto.getTemplateParam();
        this.channelAccount = recordDto.getChannelAccount();
        this.channelPassword = recordDto.getChannelPassword();
        this.signName = recordDto.getSignName();
        this.ispCode = recordDto.getIspCode();
    }
}
