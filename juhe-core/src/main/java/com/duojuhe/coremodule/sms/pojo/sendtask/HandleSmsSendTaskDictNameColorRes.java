package com.duojuhe.coremodule.sms.pojo.sendtask;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HandleSmsSendTaskDictNameColorRes extends BaseBean {
    @ApiModelProperty(value = "任务状态")
    private String taskStatusCode;

    @ApiModelProperty(value = "任务状态名称")
    private String taskStatusName;

    @ApiModelProperty(value = "任务状态颜色")
    private String taskStatusColor;

    @ApiModelProperty(value = "任务类型，定时发送，立即发送")
    private String taskTypeCode;

    @ApiModelProperty(value = "任务类型名称")
    private String taskTypeName;

    @ApiModelProperty(value = "任务类型颜色")
    private String taskTypeColor;

    @ApiModelProperty(value = "任务模式，自定义号码，系统内部联系人号码")
    private String taskModeCode;

    @ApiModelProperty(value = "任务模式名称")
    private String taskModeName;

    @ApiModelProperty(value = "任务模式颜色")
    private String taskModeColor;
}
