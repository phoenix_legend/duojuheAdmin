package com.duojuhe.coremodule.sms.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.sms.pojo.record.QuerySmsRecordPageReq;
import com.duojuhe.coremodule.sms.pojo.record.QuerySmsRecordPageRes;

import java.util.List;

public interface SmsRecordService {
    /**
     * 【分页查询】根据条件查询短信发送记录list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QuerySmsRecordPageRes>>> querySmsRecordPageResList(QuerySmsRecordPageReq req);

}
