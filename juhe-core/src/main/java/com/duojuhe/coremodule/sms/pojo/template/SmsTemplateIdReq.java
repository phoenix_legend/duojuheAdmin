package com.duojuhe.coremodule.sms.pojo.template;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SmsTemplateIdReq extends BaseBean {
    @ApiModelProperty(value = "短信模板ID", example = "1",required=true)
    @NotBlank(message = "短信模板ID不能为空")
    private String templateId;
}
