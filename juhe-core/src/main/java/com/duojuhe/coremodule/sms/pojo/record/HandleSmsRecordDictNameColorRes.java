package com.duojuhe.coremodule.sms.pojo.record;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HandleSmsRecordDictNameColorRes extends BaseBean {
    @ApiModelProperty(value = "短信发送状态code")
    private String sendStatusCode;

    @ApiModelProperty(value = "短信发送状态name")
    private String sendStatusName;

    @ApiModelProperty(value = "短信发送状态color", example = "1")
    private String sendStatusColor;
}
