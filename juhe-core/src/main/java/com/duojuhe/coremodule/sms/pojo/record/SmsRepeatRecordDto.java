package com.duojuhe.coremodule.sms.pojo.record;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SmsRepeatRecordDto extends BaseBean {
    @ApiModelProperty(value = "手机号码", required = true)
    private String mobileNumber;

    @ApiModelProperty(value = "发送ip")
    private String smsSendIp;

    @ApiModelProperty(value = "短信模板编码", required = true)
    private String templateCode;

    @ApiModelProperty(value = "短信模板内容")
    private String templateContent;

    @ApiModelProperty(value = "短信模板扩展参数 json", required = true)
    private String templateParam;

    @ApiModelProperty(value = "短信发送渠道账号", required = true)
    private String channelAccount;

    @ApiModelProperty(value = "短信发送渠道密码", required = true)
    private String channelPassword;

    @ApiModelProperty(value = "短信签名", required = true)
    private String signName;

    @ApiModelProperty(value = "创建人ID")
    private String createUserId;

    @ApiModelProperty(value = "创建部门ID")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    @ApiModelProperty(value = "短信发送任务id，来源短信任务表")
    private String sendTaskId;

    @ApiModelProperty(value = "短信发送任务对应明细id，来源短信任务明细表")
    private String sendTaskDetailId;

    @ApiModelProperty(value = "发送次数，一般是接口异常尝试次数")
    private Integer sendNumber;

    @ApiModelProperty(value = "短信发送服务商id", required = true)
    private String ispId;

    @ApiModelProperty(value = "短信发送服务商编码", required = true)
    private String ispCode;

    @ApiModelProperty(value = "短信发送渠道id", required = true)
    private String channelId;
}
