package com.duojuhe.coremodule.sms.pojo.isp;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.extend.form.attribute.Attribute;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SelectSmsIspRes extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String ispId;

    @ApiModelProperty(value = "服务商编码，具有唯一性", required = true)
    private String ispCode;

    @ApiModelProperty(value = "服务商logo")
    private String ispLogo;

    @ApiModelProperty(value = "服务商名称")
    private String ispName;

    @ApiModelProperty(value = "服务商属性 json 格式")
    private String ispAttribute;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "描述说明")
    private String description;

    @ApiModelProperty(value = "服务商属性 list格式")
    private List<Attribute> ispAttributeList;
}
