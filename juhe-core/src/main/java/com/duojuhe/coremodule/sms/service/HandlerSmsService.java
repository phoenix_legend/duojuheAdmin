package com.duojuhe.coremodule.sms.service;

import com.duojuhe.coremodule.sms.pojo.record.SmsRepeatRecordDto;

public interface HandlerSmsService {

    /**
     * 短信发送
     * @param smsRepeatRecordDto
     */
    void handlerSendSms(SmsRepeatRecordDto smsRepeatRecordDto);
}
