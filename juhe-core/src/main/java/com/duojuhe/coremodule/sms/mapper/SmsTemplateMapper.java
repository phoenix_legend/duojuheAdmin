package com.duojuhe.coremodule.sms.mapper;

import com.duojuhe.coremodule.sms.entity.SmsTemplate;
import com.duojuhe.coremodule.sms.pojo.template.QuerySmsTemplatePageReq;
import com.duojuhe.coremodule.sms.pojo.template.QuerySmsTemplatePageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SmsTemplateMapper extends TkMapper<SmsTemplate> {

    /**
     * 分页查询 根据条件查询短信模板list
     *
     * @return
     */
    List<QuerySmsTemplatePageRes> querySmsTemplatePageResList(@Param("req") QuerySmsTemplatePageReq req);
}