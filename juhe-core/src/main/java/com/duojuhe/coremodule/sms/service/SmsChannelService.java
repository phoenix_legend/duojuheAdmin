package com.duojuhe.coremodule.sms.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.sms.pojo.channel.*;

import java.util.List;

public interface SmsChannelService {

    /**
     * 【分页查询】根据条件查询短信发送渠道list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QuerySmsChannelPageRes>>> querySmsChannelPageResList(QuerySmsChannelPageReq req);


    /**
     * 根据短信发送渠道id查询渠道详情
     * @param req
     * @return
     */
    ServiceResult<QuerySmsChannelRes> querySmsChannelResByChannelId(SmsChannelIdReq req);

    /**
     * 【保存】短信发送渠道
     * @param req
     * @return
     */
    ServiceResult saveSmsChannel(SaveSmsChannelReq req);


    /**
     * 【修改】短信发送渠道
     * @param req
     * @return
     */
    ServiceResult updateSmsChannel(UpdateSmsChannelReq req);


    /**
     * 【删除】根据短信发送渠道id删除渠道
     * @param req
     * @return
     */
    ServiceResult deleteSmsChannelByChannelId(SmsChannelIdReq req);
}
