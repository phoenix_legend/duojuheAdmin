package com.duojuhe.coremodule.sms.mapper;

import com.duojuhe.coremodule.sms.entity.SmsChannel;
import com.duojuhe.coremodule.sms.pojo.channel.QuerySmsChannelPageReq;
import com.duojuhe.coremodule.sms.pojo.channel.QuerySmsChannelPageRes;
import com.duojuhe.coremodule.sms.pojo.channel.QuerySmsChannelRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SmsChannelMapper extends TkMapper<SmsChannel> {
    /**
     * 分页查询 根据条件查询短信渠道list
     *
     * @return
     */
    List<QuerySmsChannelPageRes> querySmsChannelPageResList(@Param("req") QuerySmsChannelPageReq req);

    /**
     * 根据短信发送渠道id查询渠道详情
     * @param channelId
     * @return
     */
    QuerySmsChannelRes querySmsChannelResByChannelId(@Param("channelId") String channelId);
}