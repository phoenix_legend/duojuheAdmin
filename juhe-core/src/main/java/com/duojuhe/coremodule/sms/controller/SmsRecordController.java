package com.duojuhe.coremodule.sms.controller;

import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.sms.pojo.record.QuerySmsRecordPageReq;
import com.duojuhe.coremodule.sms.pojo.record.QuerySmsRecordPageRes;
import com.duojuhe.coremodule.sms.service.SmsRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/smsRecord/")
@Api(tags = "【短信发送记录相关接口】")
@Slf4j
public class SmsRecordController {
    @Resource
    private SmsRecordService smsRecordService;

    @ApiOperation(value = "【分页查询】分页查询短信发送记录list")
    @PostMapping(value = "querySmsRecordPageResList")
    public ServiceResult<PageResult<List<QuerySmsRecordPageRes>>> querySmsRecordPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySmsRecordPageReq req) {
        return smsRecordService.querySmsRecordPageResList(req);
    }
}