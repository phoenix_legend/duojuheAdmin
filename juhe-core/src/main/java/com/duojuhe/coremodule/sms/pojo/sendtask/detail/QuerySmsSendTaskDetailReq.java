package com.duojuhe.coremodule.sms.pojo.sendtask.detail;

import com.duojuhe.common.bean.PageHead;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySmsSendTaskDetailReq extends PageHead {
    @ApiModelProperty(value = "短信发送任务ID", example = "1",required=true)
    @NotBlank(message = "短信发送任务ID不能为空")
    private String taskId;
}
