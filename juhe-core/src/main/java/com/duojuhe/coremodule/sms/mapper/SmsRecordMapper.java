package com.duojuhe.coremodule.sms.mapper;

import com.duojuhe.coremodule.sms.entity.SmsRecord;
import com.duojuhe.coremodule.sms.pojo.record.QuerySmsRecordPageReq;
import com.duojuhe.coremodule.sms.pojo.record.QuerySmsRecordPageRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SmsRecordMapper extends TkMapper<SmsRecord> {
    /**
     * 分页查询 根据条件查询短信发送记录list
     *
     * @return
     */
    List<QuerySmsRecordPageRes> querySmsRecordPageResList(@Param("req") QuerySmsRecordPageReq req);
}