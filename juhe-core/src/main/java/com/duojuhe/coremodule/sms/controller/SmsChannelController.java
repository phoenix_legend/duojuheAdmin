package com.duojuhe.coremodule.sms.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.annotation.RepeatSubmit;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.sms.pojo.channel.*;
import com.duojuhe.coremodule.sms.service.SmsChannelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/smsChannel/")
@Api(tags = "【短信渠道】")
@Slf4j
public class SmsChannelController {
    @Resource
    public SmsChannelService smsChannelService;

    @ApiOperation(value = "【分页查询】分页查询短信渠道list")
    @PostMapping(value = "querySmsChannelPageResList")
    public ServiceResult<PageResult<List<QuerySmsChannelPageRes>>> querySmsChannelPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySmsChannelPageReq req) {
        return smsChannelService.querySmsChannelPageResList(req);
    }


    @ApiOperation(value = "【分页查询】分页查询状态可用的短信渠道list")
    @PostMapping(value = "queryNormalSelectSmsChannelPageResList")
    public ServiceResult<PageResult<List<QuerySmsChannelPageRes>>> queryNormalSelectSmsChannelPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySmsChannelPageReq req) {
        return smsChannelService.querySmsChannelPageResList(req);
    }

    @ApiOperation(value = "【查询详情】根据短信发送渠道id查询渠道详情")
    @PostMapping(value = "querySmsChannelResByChannelId")
    public ServiceResult<QuerySmsChannelRes> querySmsChannelResByChannelId(@Valid @RequestBody @ApiParam(value = "入参类") SmsChannelIdReq req) {
        return smsChannelService.querySmsChannelResByChannelId(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【保存】短信渠道")
    @PostMapping(value = "saveSmsChannel")
    @OperationLog(moduleName = LogEnum.ModuleName.SMS_CHANNEL, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveSmsChannel(@Valid @RequestBody @ApiParam(value = "入参类") SaveSmsChannelReq req) {
        return smsChannelService.saveSmsChannel(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【修改】短信渠道")
    @PostMapping(value = "updateSmsChannel")
    @OperationLog(moduleName = LogEnum.ModuleName.SMS_CHANNEL, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateSmsChannel(@Valid @RequestBody @ApiParam(value = "入参类") UpdateSmsChannelReq req) {
        return smsChannelService.updateSmsChannel(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【删除】根据ID删除短信渠道，注意一次仅能删除一个短信渠道")
    @PostMapping(value = "deleteSmsChannelByChannelId")
    @OperationLog(moduleName = LogEnum.ModuleName.SMS_CHANNEL, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteSmsChannelByChannelId(@Valid @RequestBody @ApiParam(value = "入参类") SmsChannelIdReq req) {
        return smsChannelService.deleteSmsChannelByChannelId(req);
    }

}
