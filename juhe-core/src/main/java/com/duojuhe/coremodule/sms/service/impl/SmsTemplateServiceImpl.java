package com.duojuhe.coremodule.sms.service.impl;

import com.duojuhe.common.annotation.DataScopeFilter;
import com.duojuhe.common.bean.UserTokenInfoVo;
import com.duojuhe.common.enums.SystemEnum;
import com.duojuhe.coremodule.sms.enums.SmsEnum;
import com.duojuhe.common.exception.base.DuoJuHeException;
import com.duojuhe.common.result.ErrorCodes;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.idgenerator.UUIDUtils;
import com.duojuhe.common.utils.page.PageHelperUtil;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.sms.entity.SmsChannel;
import com.duojuhe.coremodule.sms.entity.SmsSendTask;
import com.duojuhe.coremodule.sms.entity.SmsTemplate;
import com.duojuhe.coremodule.sms.mapper.SmsChannelMapper;
import com.duojuhe.coremodule.sms.mapper.SmsSendTaskMapper;
import com.duojuhe.coremodule.sms.mapper.SmsTemplateMapper;
import com.duojuhe.coremodule.sms.pojo.template.*;
import com.duojuhe.coremodule.sms.service.SmsTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class SmsTemplateServiceImpl  extends BaseService implements SmsTemplateService {
    @Resource
    private SmsTemplateMapper smsTemplateMapper;
    @Resource
    private SmsSendTaskMapper smsSendTaskMapper;
    @Resource
    private SmsChannelMapper smsChannelMapper;
    /**
     * 【分页查询】根据条件查询短信模板list
     * @param req
     * @return
     */
    @DataScopeFilter
    @Override
    public ServiceResult<PageResult<List<QuerySmsTemplatePageRes>>> querySmsTemplatePageResList(QuerySmsTemplatePageReq req) {
        PageHelperUtil.orderByAndStartPage(req, "createTime desc, templateId desc");
        List<QuerySmsTemplatePageRes> list = smsTemplateMapper.querySmsTemplatePageResList(req);
        return PageHelperUtil.returnServiceResult(req, list);
    }


    /**
     * 增加
     */
    public ServiceResult saveSmsTemplate(SaveSmsTemplateReq req) {
        //检查短信模板编码
        //checkSmsTemplateCode(req.getTemplateCode());
        //获取渠道
        SmsChannel smsChannel = getSmsChannelByChannelId(req.getChannelId());
        //当前时间
        Date date = new Date();
        //模板id
        String templateId = UUIDUtils.getUUID32();
        //当前登录人
        UserTokenInfoVo currentUser = getCurrentLoginUserInfoDTO();
        SmsTemplate smsTemplate = new SmsTemplate();
        smsTemplate.setTemplateId(templateId);
        smsTemplate.setTemplateName(req.getTemplateName());
        smsTemplate.setTemplateCode(req.getTemplateCode());
        smsTemplate.setTemplateSign(req.getTemplateSign());
        smsTemplate.setContent(req.getContent());
        smsTemplate.setCreateTime(date);
        smsTemplate.setUpdateTime(date);
        smsTemplate.setCreateUserId(currentUser.getUserId());
        smsTemplate.setUpdateUserId(currentUser.getUserId());
        smsTemplate.setCreateDeptId(currentUser.getCreateDeptId());
        smsTemplate.setTenantId(currentUser.getTenantId());
        smsTemplate.setBuiltIn(SystemEnum.YES_NO.NO.getKey());
        smsTemplate.setChannelId(smsChannel.getChannelId());
        smsTemplate.setIspId(smsChannel.getIspId());
        smsTemplateMapper.insertSelective(smsTemplate);
        return ServiceResult.ok(templateId);
    }

    /**
     * 修改
     */
    public ServiceResult updateSmsTemplate(UpdateSmsTemplateReq req) {
        String templateId = req.getTemplateId();
        SmsTemplate smsTemplate = smsTemplateMapper.selectByPrimaryKey(templateId);
        if (smsTemplate == null) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        if (SystemEnum.YES_NO.YES.getKey().equals(smsTemplate.getBuiltIn())) {
            return ServiceResult.fail(ErrorCodes.SMS_TEMPLATE_NOT_UPDATE);
        }
        //获取渠道
        SmsChannel smsChannel = getSmsChannelByChannelId(req.getChannelId());
        /* 检查短信模板编码是否已存在
        if (!req.getTemplateCode().equals(smsTemplate.getTemplateCode())){
            checkSmsTemplateCode(req.getTemplateCode());
        }*/
        //检查是否存在待发送的短信任务
        checkWaitingSmsSendTaskByTemplateId(templateId);
        //当前登录人
        UserTokenInfoVo currentUser = getCurrentLoginUserInfoDTO();
        SmsTemplate updateSmsTemplate = new SmsTemplate();
        updateSmsTemplate.setTemplateId(templateId);
        updateSmsTemplate.setTemplateName(req.getTemplateName());
        updateSmsTemplate.setTemplateSign(req.getTemplateSign());
        updateSmsTemplate.setTemplateCode(req.getTemplateCode());
        updateSmsTemplate.setContent(req.getContent());
        updateSmsTemplate.setUpdateTime(new Date());
        updateSmsTemplate.setUpdateUserId(currentUser.getUserId());
        updateSmsTemplate.setChannelId(smsChannel.getChannelId());
        updateSmsTemplate.setIspId(smsChannel.getIspId());
        smsTemplateMapper.updateByPrimaryKeySelective(updateSmsTemplate);
        return ServiceResult.ok(templateId);
    }

    /**
     * 根据ID删除
     */
    public ServiceResult deleteSmsTemplateByTemplateId(SmsTemplateIdReq req) {
        String templateId = req.getTemplateId();
        SmsTemplate smsTemplate = smsTemplateMapper.selectByPrimaryKey(templateId);
        if (smsTemplate == null) {
            return ServiceResult.fail(ErrorCodes.PARAM_ERROR);
        }
        if (SystemEnum.YES_NO.YES.getKey().equals(smsTemplate.getBuiltIn())) {
            return ServiceResult.fail(ErrorCodes.SMS_TEMPLATE_NOT_DELETE);
        }
        //检查是否存在待发送的短信任务
        checkWaitingSmsSendTaskByTemplateId(templateId);
        //删除模板
        smsTemplateMapper.deleteByPrimaryKey(templateId);
        return ServiceResult.ok(templateId);
    }


    /**
     * 【私有方法，辅助其他接口方法使用】 查询模板code编码是否已经存在
     */
    private void checkSmsTemplateCode(String templateCode) {
        if (StringUtils.isBlank(templateCode)){
            throw new DuoJuHeException(ErrorCodes.SMS_TEMPLATE_CODE_EXIST);
        }
        Example example = new Example(SmsTemplate.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("templateCode", templateCode);
        if(smsTemplateMapper.selectByExample(example).size()>0){
            throw new DuoJuHeException(ErrorCodes.SMS_TEMPLATE_CODE_EXIST);
        }
    }

    /**
     * 【私有方法，辅助其他接口方法使用】 检查渠道是否存在
     */
    private SmsChannel getSmsChannelByChannelId(String channelId) {
        if (StringUtils.isBlank(channelId)){
            throw new DuoJuHeException(ErrorCodes.PARAM_ERROR);
        }
        SmsChannel channel = smsChannelMapper.selectByPrimaryKey(channelId);
        if (channel==null || !SystemEnum.STATUS.NORMAL.getKey().equals(channel.getStatusCode())){
            throw new DuoJuHeException(ErrorCodes.SMS_CHANNEL_NOT_EXIST);
        }
        return channel;
    }

    /**
     * 【私有方法，辅助其他接口方法使用】 检查是否存在待发送的短信任务
     */
    private void checkWaitingSmsSendTaskByTemplateId(String templateId) {
        if (StringUtils.isBlank(templateId)){
            throw new DuoJuHeException(ErrorCodes.SMS_TEMPLATE_WAITING_SEND_TASK_EXIST);
        }
        Example example = new Example(SmsSendTask.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("templateId", templateId);
        criteria.andEqualTo("taskStatusCode", SmsEnum.SmsSendTaskStatus.SMS_WAITING_EXECUTE.getKey());
        if (smsSendTaskMapper.selectByExample(example).size()>0){
            throw new DuoJuHeException(ErrorCodes.SMS_TEMPLATE_WAITING_SEND_TASK_EXIST);
        }
    }

}
