package com.duojuhe.coremodule.sms.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.sms.pojo.template.*;

import java.util.List;

public interface SmsTemplateService {
    /**
     * 【分页查询】根据条件查询短信模板list
     * @param req
     * @return
     */
    ServiceResult<PageResult<List<QuerySmsTemplatePageRes>>> querySmsTemplatePageResList(QuerySmsTemplatePageReq req);


    /**
     * 【保存】短信模板
     * @param req
     * @return
     */
    ServiceResult saveSmsTemplate(SaveSmsTemplateReq req);


    /**
     * 【修改】短信模板
     * @param req
     * @return
     */
    ServiceResult updateSmsTemplate(UpdateSmsTemplateReq req);


    /**
     * 【删除】根据模板id删除短信模板
     * @param req
     * @return
     */
    ServiceResult deleteSmsTemplateByTemplateId(SmsTemplateIdReq req);
}
