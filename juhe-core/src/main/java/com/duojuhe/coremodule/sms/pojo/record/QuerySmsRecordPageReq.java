package com.duojuhe.coremodule.sms.pojo.record;


import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySmsRecordPageReq extends PageHead {
    @ApiModelProperty(value = "手机号码")
    private String mobileNumber;

    @ApiModelProperty(value = "开始日期yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    @ApiModelProperty(value = "短信发送状态code")
    private String sendStatusCode;

    @ApiModelProperty(value = "短信内容")
    private String content;

    @ApiModelProperty(value = "短信任务ID")
    private String sendTaskId;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}
