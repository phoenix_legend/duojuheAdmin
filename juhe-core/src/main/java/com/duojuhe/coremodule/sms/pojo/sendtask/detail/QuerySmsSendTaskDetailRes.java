package com.duojuhe.coremodule.sms.pojo.sendtask.detail;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySmsSendTaskDetailRes extends BaseBean {
    @ApiModelProperty(value = "明细id", required = true)
    private String detailId;

    @ApiModelProperty(value = "所属任务id", required = true)
    private String taskId;

    @ApiModelProperty(value = "手机号码", required = true)
    private String mobileNumber;

    @ApiModelProperty(value = "短信发送状态，取数据字典")
    private String sendStatusCode;

    @ApiModelProperty(value = "短信发送状态名称")
    private String sendStatusName;

    @ApiModelProperty(value = "短信发送状态颜色")
    private String sendStatusColor;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "发送时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date sendTime;
}
