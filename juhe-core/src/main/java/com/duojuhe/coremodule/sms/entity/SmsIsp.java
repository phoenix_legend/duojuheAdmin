package com.duojuhe.coremodule.sms.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("短信网络服务供应商")
@Table(name = "sms_isp")
public class SmsIsp extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "isp_id")
    @Id
    private String ispId;

    @ApiModelProperty(value = "服务商编码，具有唯一性", required = true)
    @Column(name = "isp_code")
    private String ispCode;

    @ApiModelProperty(value = "服务商logo")
    @Column(name = "isp_logo")
    private String ispLogo;

    @ApiModelProperty(value = "服务商名称")
    @Column(name = "isp_name")
    private String ispName;

    @ApiModelProperty(value = "服务商属性 json 格式")
    @Column(name = "isp_attribute")
    private String ispAttribute;

    @ApiModelProperty(value = "厂商状态，FORBID禁用 NORMAL正常")
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "描述说明")
    @Column(name = "description")
    private String description;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;
}