package com.duojuhe.coremodule.sms.service;

import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.sms.pojo.isp.SelectSmsIspRes;

import java.util.List;

public interface SmsIspService {
    /**
     * 【下拉选择使用-查询所有服务商】查询可供前端选择使用的服务商
     * @return
     */
    ServiceResult<List<SelectSmsIspRes>> queryNormalSelectSmsIspResList();

}
