package com.duojuhe.coremodule.sms.pojo.template;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveSmsTemplateReq extends BaseBean {
    @ApiModelProperty(value = "短信渠道ID", example = "1",required=true)
    @NotBlank(message = "短信渠道ID不能为空")
    private String channelId;

    @ApiModelProperty(value = "模板名称", required = true)
    @NotBlank(message = "模板名称不能为空")
    @Pattern(regexp = "^[\\u4E00-\\u9FA5a-zA-Z0-9_]*$", message = "模板名称只能填写中文英文数字和下划线")
    @Length(max = 50, message = "模板名称不得超过{max}位字符")
    private String templateName;

    @ApiModelProperty(value = "模板签名", required = true)
    @NotBlank(message = "模板签名不能为空")
    @Pattern(regexp = RegexpConstants.SMS_SIGN, message = "模板签名只能填写中文英文数字下划线和【】")
    @Length(max = 50, message = "模板签名不得超过{max}位字符")
    private String templateSign;

    @ApiModelProperty(value = "短信模板内容", required = true)
    @NotBlank(message = "短信模板内容不能为空")
    @Length(max = 200, message = "短信模板内容不得超过{max}位字符")
    private String content;

    @ApiModelProperty(value = "模板编码", required = true)
    @NotBlank(message = "模板编码不能为空")
    @Pattern(regexp = RegexpConstants.EN_LENGTH_NUMBER, message = "模板编码只能填写英文和数字")
    @Length(min = 1, max = 200, message = "模板编码不得超过{max}位字符")
    private String templateCode;
}
