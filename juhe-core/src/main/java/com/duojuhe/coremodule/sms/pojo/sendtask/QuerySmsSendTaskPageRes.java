package com.duojuhe.coremodule.sms.pojo.sendtask;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySmsSendTaskPageRes extends HandleSmsSendTaskDictNameColorRes {
    @ApiModelProperty(value = "主键id任务id", required = true)
    private String taskId;

    @ApiModelProperty(value = "任务名称", required = true)
    private String taskName;

    @ApiModelProperty(value = "渠道编码", required = true)
    private String channelCode;

    @ApiModelProperty(value = "渠道名称")
    private String channelName;

    @ApiModelProperty(value = "服务商编码")
    private String ispCode;

    @ApiModelProperty(value = "服务商名称")
    private String ispName;

    @ApiModelProperty(value = "定时发送的定时时间yyyy-MM-dd HH:MM")
    @JsonFormat(pattern= DateUtils.yyyy_MM_ddHHmm,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.yyyy_MM_ddHHmm)
    private Date timingTime;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "创建部门id")
    private String createDeptId;

    @ApiModelProperty(value = "创建部门名称")
    private String createDeptName;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "描述说明")
    private String description;

    @ApiModelProperty(value = "模板签名", required = true)
    private String templateSign;

    @ApiModelProperty(value = "短信模板内容")
    private String templateContent;
}