package com.duojuhe.coremodule.sms.mapper;


import com.duojuhe.coremodule.sms.entity.SmsIsp;
import com.duojuhe.coremodule.sms.pojo.isp.SelectSmsIspRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SmsIspMapper extends TkMapper<SmsIsp> {

    /**
     * 根据条件查询所有可供前端选择使用的服务商
     *
     * @return
     */
    List<SelectSmsIspRes> querySelectSmsIspResListByStatusCode(@Param("statusCode") String statusCode);
}