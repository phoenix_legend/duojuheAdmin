package com.duojuhe.coremodule.sms.pojo.channel;


import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UpdateSmsChannelReq extends SmsChannelIdReq {
    @ApiModelProperty(value = "渠道名称", required = true)
    @NotBlank(message = "渠道名称不能为空")
    @Pattern(regexp = "^[\\u4E00-\\u9FA5a-zA-Z0-9_]*$", message = "渠道名称只能填写中文英文数字和下划线")
    @Length(max = 50, message = "渠道名称不得超过{max}位字符")
    private String channelName;

    @ApiModelProperty(value = "渠道参数配置")
    @NotBlank(message = "渠道参数配置不能为空")
    @Length(max = 255, message = "渠道参数配置不得超过{max}位字符")
    private String channelIspAttribute;

    @ApiModelProperty(value = "备注", example = "1")
    @Pattern(regexp = RegexpConstants.REMARK, message = "备注禁止输入特殊字符!")
    @Length(max = 100, message = "备注不得超过{max}位字符")
    private  String remark;

    @ApiModelProperty(value = "状态：FORBID禁用 NORMAL正常", example = "1",required=true)
    @Pattern(regexp = RegexpConstants.STATUS, message = "状态参数错误!")
    @NotBlank(message = "状态不能为空")
    private String statusCode;

    @ApiModelProperty(value = "排序，只能为纯数字", example = "1",required=true)
    @NotNull(message = "排序不能为空")
    @Range(min = 1, max = 10000, message = "排序数值不正确")
    private Integer sort;
}
