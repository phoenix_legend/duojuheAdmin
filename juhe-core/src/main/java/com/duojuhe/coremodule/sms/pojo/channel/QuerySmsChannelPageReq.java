package com.duojuhe.coremodule.sms.pojo.channel;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySmsChannelPageReq extends PageHead {
    @ApiModelProperty(value = "isp id")
    private String ispId;

    @ApiModelProperty(value = "isp名称")
    private String ispName;

    @ApiModelProperty(value = "服务商状态，FORBID禁用 NORMAL正常")
    private String ispStatusCode;

    @ApiModelProperty(value = "渠道名称")
    private String channelName;

    @ApiModelProperty(value = "渠道状态，FORBID禁用 NORMAL正常")
    private String statusCode;

    @ApiModelProperty(value = "开始日期yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}