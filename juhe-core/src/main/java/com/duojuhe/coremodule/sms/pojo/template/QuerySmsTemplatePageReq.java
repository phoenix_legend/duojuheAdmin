package com.duojuhe.coremodule.sms.pojo.template;

import com.duojuhe.common.bean.PageHead;
import com.duojuhe.common.utils.dateutils.DateUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySmsTemplatePageReq extends PageHead {
    @ApiModelProperty(value = "模板名称")
    private String templateName;

    @ApiModelProperty(value = "模板编码")
    private String templateCode;

    @ApiModelProperty(value = "模板签名")
    private String templateSign;

    @ApiModelProperty(value = "短信模板内容")
    private String content;

    @ApiModelProperty(value = "部门ID", example = "1")
    private String createDeptId;

    @ApiModelProperty(value = "开始日期yyyy-MM-dd", example = "2018-11-01")
    private Date startTime;

    @ApiModelProperty(value = "结束日期yyyy-MM-dd", example = "2018-11-10")
    private Date endTime;

    @ApiModelProperty(value = "模板归属渠道名称")
    private String channelName;

    public void setEndTime(Date endTime) {
        this.endTime = DateUtils.toLastSecond(endTime);
    }
}
