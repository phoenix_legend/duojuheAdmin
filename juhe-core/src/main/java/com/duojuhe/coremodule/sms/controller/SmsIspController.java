package com.duojuhe.coremodule.sms.controller;

import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.sms.pojo.isp.SelectSmsIspRes;
import com.duojuhe.coremodule.sms.service.SmsIspService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/smsIsp/")
@Api(tags = {"【短信服务商管理】短信服务商管理相关接口"})
@Slf4j
public class SmsIspController {
    @Resource
    private SmsIspService smsIspService;

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【下拉选择使用-查询所有短信服务商】查询可供前端选择使用的短信服务商")
    @PostMapping(value = "queryNormalSelectSmsIspResList")
    public ServiceResult<List<SelectSmsIspRes>> queryNormalSelectSmsIspResList() {
        return smsIspService.queryNormalSelectSmsIspResList();
    }
}
