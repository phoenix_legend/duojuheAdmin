package com.duojuhe.coremodule.sms.pojo.template;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySmsTemplatePageRes extends BaseBean {
    @ApiModelProperty(value = "短信主键", required = true)
    private String templateId;

    @ApiModelProperty(value = "模板编码", required = true)
    private String templateCode;

    @ApiModelProperty(value = "名称", required = true)
    private String templateName;

    @ApiModelProperty(value = "模板签名", required = true)
    private String templateSign;

    @ApiModelProperty(value = "短信模板内容", required = true)
    private String content;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "创建部门id")
    private String createDeptId;

    @ApiModelProperty(value = "创建部门名称")
    private String createDeptName;

    @ApiModelProperty(value = "模板归属服务商id", required = true)
    private String ispId;

    @ApiModelProperty(value = "模板归属发送渠道id", required = true)
    private String channelId;

    @ApiModelProperty(value = "模板归属渠道名称", required = true)
    private String channelName;
}
