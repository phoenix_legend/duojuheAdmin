package com.duojuhe.coremodule.sms.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.duojuhe.common.enums.SystemEnum;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.common.utils.page.PageHelperUtil;
import com.duojuhe.coremodule.BaseService;
import com.duojuhe.coremodule.sms.mapper.SmsIspMapper;
import com.duojuhe.coremodule.sms.pojo.isp.SelectSmsIspRes;
import com.duojuhe.common.extend.form.attribute.AttributeEvent;
import com.duojuhe.coremodule.sms.service.SmsIspService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class SmsIspServiceImpl extends BaseService implements SmsIspService {
    @Resource
    private SmsIspMapper smsIspMapper;

    /**
     * 【下拉选择使用-查询所有服务商】查询可供前端选择使用的服务商
     * @return
     */
    @Override
    public ServiceResult<List<SelectSmsIspRes>> queryNormalSelectSmsIspResList() {
        PageHelperUtil.defaultOrderBy("sort desc,createTime desc,ispId desc");
        List<SelectSmsIspRes> ispResList = smsIspMapper.querySelectSmsIspResListByStatusCode(SystemEnum.STATUS.NORMAL.getKey());
        for (SelectSmsIspRes res:ispResList){
            if (StringUtils.isNotBlank(res.getIspAttribute())){
                AttributeEvent ispAttributeEvent = JSON.parseObject(res.getIspAttribute(), new TypeReference<AttributeEvent>(){});
                res.setIspAttributeList(ispAttributeEvent.getAttribute());
            }
        }
        return ServiceResult.ok(ispResList);
    }
}
