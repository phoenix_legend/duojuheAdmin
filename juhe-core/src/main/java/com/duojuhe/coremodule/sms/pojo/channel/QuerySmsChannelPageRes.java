package com.duojuhe.coremodule.sms.pojo.channel;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySmsChannelPageRes extends HandleSmsChannelDictNameColorRes {
    @ApiModelProperty(value = "主键", required = true)
    private String channelId;

    @ApiModelProperty(value = "isp主键id")
    private String ispId;

    @ApiModelProperty(value = "渠道编码，具有唯一性")
    private String channelCode;

    @ApiModelProperty(value = "渠道名称")
    private String channelName;

    @ApiModelProperty(value = "渠道服务商配置")
    private String channelIspAttribute;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "创建部门id")
    private String createDeptId;

    @ApiModelProperty(value = "创建部门名称")
    private String createDeptName;

    @ApiModelProperty(value = "服务商logo")
    private String ispLogo;

    @ApiModelProperty(value = "服务商名称")
    private String ispName;

    @ApiModelProperty(value = "排序")
    private Integer sort;
}