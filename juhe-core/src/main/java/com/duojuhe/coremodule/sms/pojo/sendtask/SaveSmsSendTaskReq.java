package com.duojuhe.coremodule.sms.pojo.sendtask;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.RegexpConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SaveSmsSendTaskReq extends BaseBean {
    @ApiModelProperty(value = "短信模板ID", example = "1",required=true)
    @NotBlank(message = "短信模板ID不能为空")
    private String templateId;

    @ApiModelProperty(value = "任务名称",required=true)
    @NotBlank(message = "任务名称不能为空")
    @Length(max = 255, message = "任务名称不得超过{max}位字符")
    private String taskName;

    @ApiModelProperty(value = "任务类型，定时发送-SMS_TIMING_SEND，立即发送-SMS_IMMEDIATELY_SEND", required = true)
    @Pattern(regexp = "^[SMS_TIMING_SEND|SMS_IMMEDIATELY_SEND]*$", message = "任务类型参数错误!")
    @NotBlank(message = "任务类型不能为空")
    private String taskTypeCode;

    @ApiModelProperty(value = "任务模式，自定义模式-SMS_CUSTOM_MODE，系统模式-SMS_SYSTEM_MODE", required = true)
    @Pattern(regexp = "^[SMS_CUSTOM_MODE|SMS_SYSTEM_MODE]*$", message = "任务模式参数错误!")
    @NotBlank(message = "任务模式不能为空")
    private String taskModeCode;

    @ApiModelProperty(value = "定时发送的定时时间yyyy-MM-dd HH:MM", required = true)
    @Pattern(regexp = RegexpConstants.YYYY_MM_DD_HH_MM, message = "发送时间参数错误!")
    @NotBlank(message = "发送时间不能为空")
    private String timingTime;

    @ApiModelProperty(value = "备注")
    @Pattern(regexp = RegexpConstants.REMARK, message = "备注禁止输入特殊字符!")
    @Length(max = 100, message = "备注不得超过{max}位字符")
    private String remark;

    @ApiModelProperty(value = "短信发送任务接收对象集合，可以是手机号码，可以是用户等参数id", required = true)
    @NotEmpty(message = "短信发送任务接收对象集合不能为空")
    @Valid
    @NotNull(message = "短信发送任务接收对象集合不能为空")
    @Size(min = 1, message = "短信发送任务接收对象集合不能为空")
    private List<String> taskContentList;
}