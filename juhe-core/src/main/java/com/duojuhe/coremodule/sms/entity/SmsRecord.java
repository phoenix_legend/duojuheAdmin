package com.duojuhe.coremodule.sms.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("短信发送记录表")
@Table(name = "sms_record")
public class SmsRecord extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "record_id")
    @Id
    private String recordId;

    @ApiModelProperty(value = "手机号码", required = true)
    @Column(name = "mobile_number")
    private String mobileNumber;

    @ApiModelProperty(value = "短信内容")
    @Column(name = "content")
    private String content;

    @ApiModelProperty(value = "发送时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "短信发送状态，取数据字典")
    @Column(name = "send_status_code")
    private String sendStatusCode;

    @ApiModelProperty(value = "发送ip")
    @Column(name = "send_ip")
    private String sendIp;

    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "创建人ID")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建部门ID")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户ID")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "短信发送任务id，来源短信任务表")
    @Column(name = "send_task_id")
    private String sendTaskId;

    @ApiModelProperty(value = "短信发送任务对应明细id，来源短信任务明细表")
    @Column(name = "send_task_detail_id")
    private String sendTaskDetailId;

    @ApiModelProperty(value = "发送次数，一般是接口异常尝试次数")
    @Column(name = "send_number")
    private Integer sendNumber;

    @ApiModelProperty(value = "短信发送服务商id", required = true)
    @Column(name = "isp_id")
    private String ispId;

    @ApiModelProperty(value = "短信发送渠道id", required = true)
    @Column(name = "channel_id")
    private String channelId;
}