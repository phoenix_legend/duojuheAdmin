package com.duojuhe.coremodule.sms.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("短信发送任务明细表")
@Table(name = "sms_send_task_detail")
public class SmsSendTaskDetail extends BaseBean {
    @ApiModelProperty(value = "明细id", required = true)
    @Column(name = "detail_id")
    @Id
    private String detailId;

    @ApiModelProperty(value = "所属任务id", required = true)
    @Column(name = "task_id")
    private String taskId;

    @ApiModelProperty(value = "手机号码", required = true)
    @Column(name = "mobile_number")
    private String mobileNumber;

    @ApiModelProperty(value = "短信发送状态，取数据字典")
    @Column(name = "send_status_code")
    private String sendStatusCode;

    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "发送时间")
    @Column(name = "send_time")
    private Date sendTime;
}