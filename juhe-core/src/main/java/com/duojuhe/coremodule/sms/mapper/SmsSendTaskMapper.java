package com.duojuhe.coremodule.sms.mapper;

import com.duojuhe.coremodule.sms.entity.SmsSendTask;
import com.duojuhe.coremodule.sms.pojo.sendtask.QuerySmsSendTaskPageReq;
import com.duojuhe.coremodule.sms.pojo.sendtask.QuerySmsSendTaskPageRes;
import com.duojuhe.coremodule.sms.pojo.sendtask.QuerySmsSendTaskRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SmsSendTaskMapper extends TkMapper<SmsSendTask> {

    /**
     * 查询所有等待发送的短信任务
     *
     * @return
     */
    List<SmsSendTask> queryAllWaitingSendSmsSendTaskList(@Param(value = "nowDate") String nowDate);


    /**
     * 【分页查询】根据条件查询短信发送任务list
     *
     * @return
     */
    List<QuerySmsSendTaskPageRes> querySmsSendTaskPageResList(@Param("req") QuerySmsSendTaskPageReq req);


    /**
     * 根据任务id查询任务明细
     * @param taskId
     * @return
     */
    QuerySmsSendTaskRes querySmsSendTaskResBySendTaskId(@Param("taskId") String taskId);

}