package com.duojuhe.coremodule.sms.controller;


import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.sms.pojo.template.*;
import com.duojuhe.coremodule.sms.service.SmsTemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/smsTemplate/")
@Api(tags = "【短信模板】")
@Slf4j
public class SmsTemplateController {

    @Resource
    public SmsTemplateService smsTemplateService;

    @ApiOperation(value = "【分页查询】分页查询短信模板list")
    @PostMapping(value = "querySmsTemplatePageResList")
    public ServiceResult<PageResult<List<QuerySmsTemplatePageRes>>> querySmsTemplatePageResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySmsTemplatePageReq req) {
        return smsTemplateService.querySmsTemplatePageResList(req);
    }


    @ApiOperation(value = "【分页查询】分页查询状态可用的短信模板list")
    @PostMapping(value = "queryNormalSelectSmsTemplatePageResList")
    public ServiceResult<PageResult<List<QuerySmsTemplatePageRes>>> queryNormalSelectSmsTemplatePageResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySmsTemplatePageReq req) {
        return smsTemplateService.querySmsTemplatePageResList(req);
    }


    @ApiOperation(value = "【保存】短信模板")
    @PostMapping(value = "saveSmsTemplate")
    @OperationLog(moduleName = LogEnum.ModuleName.SMS_TEMPLATE, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveSmsTemplate(@Valid @RequestBody @ApiParam(value = "入参类") SaveSmsTemplateReq req) {
        return smsTemplateService.saveSmsTemplate(req);
    }

    @ApiOperation(value = "【修改】短信模板")
    @PostMapping(value = "updateSmsTemplate")
    @OperationLog(moduleName = LogEnum.ModuleName.SMS_TEMPLATE, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateSmsTemplate(@Valid @RequestBody @ApiParam(value = "入参类") UpdateSmsTemplateReq req) {
        return smsTemplateService.updateSmsTemplate(req);
    }

    @ApiOperation(value = "【删除】根据ID删除短信模板，注意一次仅能删除一个短信模板")
    @PostMapping(value = "deleteSmsTemplateByTemplateId")
    @OperationLog(moduleName = LogEnum.ModuleName.SMS_TEMPLATE, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteSmsTemplateByTemplateId(@Valid @RequestBody @ApiParam(value = "入参类") SmsTemplateIdReq req) {
        return smsTemplateService.deleteSmsTemplateByTemplateId(req);
    }


}