package com.duojuhe.coremodule.sms.pojo.channel;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SmsChannelIdReq extends BaseBean {
    @ApiModelProperty(value = "短信渠道ID", example = "1",required=true)
    @NotBlank(message = "短信渠道ID不能为空")
    private String channelId;
}
