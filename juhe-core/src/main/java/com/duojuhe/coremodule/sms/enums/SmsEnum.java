package com.duojuhe.coremodule.sms.enums;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * 短信相关枚举
 *
 * @date 2018/5/30
 */
public class SmsEnum {

    /**
     * 短信服务厂商标识
     */
    public enum SmsIsp {
        ipyy("ipyy", "华信"),
        aliyun("aliyun", "阿里云");
        @Getter
        private String key;
        @Getter
        private String value;

        SmsIsp(String key, String value) {
            this.key = key;
            this.value = value;
        }
        public static String getValueByKey(String key) {
            if (StringUtils.isBlank(key)) {
                return "";
            }
            for (SmsIsp e : SmsIsp.values()) {
                if (e.getKey().equals(key)) {
                    return e.getValue();
                }
            }
            return "";
        }
    }


    /**
     * 短信发送任务类型，定时发送-SMS_TIMING_SEND，立即发送-SMS_IMMEDIATELY_SEND
     */
    public enum SmsSendTaskType {
        SMS_TIMING_SEND("SMS_TIMING_SEND", "定时发送"),
        SMS_IMMEDIATELY_SEND("SMS_IMMEDIATELY_SEND", "立即发送");
        @Getter
        private String key;
        @Getter
        private String value;

        SmsSendTaskType(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }


    /**
     * 短信发送状态枚举
     */
    public enum SmsSendStatus {
        SMS_SEND_SUCCESS("SMS_SEND_SUCCESS", "发送成功"),
        SMS_SEND_SENDING("SMS_SEND_SENDING", "发送中"),
        SMS_SEND_WAITING("SMS_SEND_WAITING", "等待发送"),
        SMS_SEND_FAIL("SMS_SEND_FAIL", "发送失败");
        @Getter
        private String key;
        @Getter
        private String value;

        SmsSendStatus(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 短信发送任务任务模式，自定义模式-CUSTOM_MODE，就是接收对象直接是手机号码，系统模式-SYSTEM_MODE 接收对象是用户id需要查询到相应手机号码
     */
    public enum SmsSendTaskMode {
        SMS_CUSTOM_MODE("SMS_CUSTOM_MODE", "自定义模式"),
        SMS_SYSTEM_MODE("SMS_SYSTEM_MODE", "系统模式");
        @Getter
        private String key;
        @Getter
        private String value;

        SmsSendTaskMode(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 短信发送任务任务状态 已发送-ALREADY_EXECUTE，待发送-WAITING_EXECUTE，已取消-ALREADY_CANCEL
     */
    public enum SmsSendTaskStatus {
        SMS_ALREADY_EXECUTE("SMS_ALREADY_EXECUTE", "已发送"),
        SMS_ALREADY_CANCEL("SMS_ALREADY_CANCEL", "已取消"),
        SMS_WAITING_EXECUTE("SMS_WAITING_EXECUTE", "待发送");
        @Getter
        private String key;
        @Getter
        private String value;

        SmsSendTaskStatus(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }


}
