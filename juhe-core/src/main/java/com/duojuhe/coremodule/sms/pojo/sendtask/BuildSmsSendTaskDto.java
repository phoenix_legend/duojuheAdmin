package com.duojuhe.coremodule.sms.pojo.sendtask;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.coremodule.sms.entity.SmsSendTaskDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BuildSmsSendTaskDto extends BaseBean {
    @ApiModelProperty(value = "错误的短信发送任务内容集合")
    private List<String> errorTaskContentList;

    @ApiModelProperty(value = "成功的短信发送任务内容集合")
    private List<SmsSendTaskDetail> successContentList;
}