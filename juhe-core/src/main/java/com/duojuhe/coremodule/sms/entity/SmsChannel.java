package com.duojuhe.coremodule.sms.entity;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ApiModel("短信发送渠道")
@Table(name = "sms_channel")
public class SmsChannel extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    @Column(name = "channel_id")
    @Id
    private String channelId;

    @ApiModelProperty(value = "isp主键id")
    @Column(name = "isp_id")
    private String ispId;

    @ApiModelProperty(value = "渠道编码，具有唯一性")
    @Column(name = "channel_code")
    private String channelCode;

    @ApiModelProperty(value = "渠道名称")
    @Column(name = "channel_name")
    private String channelName;

    @ApiModelProperty(value = "渠道服务商配置")
    @Column(name = "channel_isp_attribute")
    private String channelIspAttribute;

    @ApiModelProperty(value = "备注")
    @Column(name = "remark")
    private String remark;

    @ApiModelProperty(value = "渠道状态，FORBID禁用 NORMAL正常")
    @Column(name = "status_code")
    private String statusCode;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_user_id")
    private String createUserId;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新人id")
    @Column(name = "update_user_id")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间")
    @Column(name = "update_time")
    private Date updateTime;

    @ApiModelProperty(value = "创建部门id")
    @Column(name = "create_dept_id")
    private String createDeptId;

    @ApiModelProperty(value = "租户id")
    @Column(name = "tenant_id")
    private String tenantId;

    @ApiModelProperty(value = "排序")
    @Column(name = "sort")
    private Integer sort;

    @ApiModelProperty(value = "是否内置，取数据字典", required = true)
    @Column(name = "built_in")
    private String builtIn;
}