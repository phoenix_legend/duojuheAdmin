package com.duojuhe.coremodule.sms.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.annotation.RepeatSubmit;
import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.sms.pojo.sendtask.*;
import com.duojuhe.coremodule.sms.pojo.sendtask.detail.QuerySmsSendTaskDetailReq;
import com.duojuhe.coremodule.sms.pojo.sendtask.detail.QuerySmsSendTaskDetailRes;
import com.duojuhe.coremodule.sms.service.SmsSendTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/smsSendTask/")
@Api(tags = "【短信发送任务相关接口】")
@Slf4j
public class SmsSendTaskController {
    @Resource
    public SmsSendTaskService smsSendTaskService;

    @ApiOperation(value = "【分页查询】分页查询短信发送任务list")
    @PostMapping(value = "querySmsSendTaskPageResList")
    public ServiceResult<PageResult<List<QuerySmsSendTaskPageRes>>> querySmsSendTaskPageResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySmsSendTaskPageReq req) {
        return smsSendTaskService.querySmsSendTaskPageResList(req);
    }

    @ApiOperation(value = "【查询详情】根据任务id查询任务明细")
    @PostMapping(value = "querySmsSendTaskResBySendTaskId")
    public ServiceResult<QuerySmsSendTaskRes> querySmsSendTaskResBySendTaskId(@Valid @RequestBody @ApiParam(value = "入参类") SmsSendTaskIdReq req) {
        return smsSendTaskService.querySmsSendTaskResBySendTaskId(req);
    }


    @RepeatSubmit
    @ApiOperation(value = "【保存】短信发送任务")
    @PostMapping(value = "saveSmsSendTask")
    @OperationLog(moduleName = LogEnum.ModuleName.SMS_SEND_TASK, operationType = LogEnum.OperationType.ADD)
    public ServiceResult saveSmsSendTask(@Valid @RequestBody @ApiParam(value = "入参类") SaveSmsSendTaskReq req) {
        return smsSendTaskService.saveSmsSendTask(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【修改】短信发送任务")
    @PostMapping(value = "updateSmsSendTask")
    @OperationLog(moduleName = LogEnum.ModuleName.SMS_SEND_TASK, operationType = LogEnum.OperationType.UPDATE)
    public ServiceResult updateSmsSendTask(@Valid @RequestBody @ApiParam(value = "入参类") UpdateSmsSendTaskReq req) {
        return smsSendTaskService.updateSmsSendTask(req);
    }

    @RepeatSubmit
    @ApiOperation(value = "【删除】根据ID删除短信发送任务，注意一次仅能删除一个短信发送任务")
    @PostMapping(value = "deleteSmsSendTaskBySendTaskId")
    @OperationLog(moduleName = LogEnum.ModuleName.SMS_SEND_TASK, operationType = LogEnum.OperationType.DELETE)
    public ServiceResult deleteSmsSendTaskBySendTaskId(@Valid @RequestBody @ApiParam(value = "入参类") SmsSendTaskIdReq req) {
        return smsSendTaskService.deleteSmsSendTaskBySendTaskId(req);
    }


    @ApiOperation(value = "【分页查询】根据短信发送任务id分页查询发送任务明细")
    @PostMapping(value = "querySmsSendTaskDetailResList")
    public ServiceResult<PageResult<List<QuerySmsSendTaskDetailRes>>> querySmsSendTaskDetailResList(@Valid @RequestBody @ApiParam(value = "入参类") QuerySmsSendTaskDetailReq req) {
        return smsSendTaskService.querySmsSendTaskDetailResList(req);
    }
}
