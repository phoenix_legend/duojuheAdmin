package com.duojuhe.coremodule.sms.mapper;


import com.duojuhe.coremodule.sms.entity.SmsSendTaskDetail;
import com.duojuhe.coremodule.sms.pojo.sendtask.detail.QuerySmsSendTaskDetailRes;
import com.tkmapper.TkMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SmsSendTaskDetailMapper extends TkMapper<SmsSendTaskDetail> {

    /**
     * 查询所有等待发送的短信明细
     *
     * @return
     */
    List<SmsSendTaskDetail> queryAllWaitingSendSmsSendTaskDetailList();

    /**
     * 【分页查询】根据短信发送任务id分页查询发送任务明细
     *
     * @return
     */
    List<QuerySmsSendTaskDetailRes> querySmsSendTaskDetailResListByTaskId(@Param("taskId") String taskId);
}