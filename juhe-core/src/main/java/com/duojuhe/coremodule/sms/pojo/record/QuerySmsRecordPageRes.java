package com.duojuhe.coremodule.sms.pojo.record;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuerySmsRecordPageRes extends HandleSmsRecordDictNameColorRes {
    @ApiModelProperty(value = "主键", required = true)
    private String recordId;

    @ApiModelProperty(value = "手机号码", required = true)
    private String mobileNumber;

    @ApiModelProperty(value = "短信内容", required = true)
    private String content;

    @ApiModelProperty(value = "发送时间", required = true)
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date createTime;

    @ApiModelProperty(value = "发送ip")
    private String sendIp;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "创建机构id")
    private String createDeptId;

    @ApiModelProperty(value = "创建机构名称")
    private String createDeptName;
}
