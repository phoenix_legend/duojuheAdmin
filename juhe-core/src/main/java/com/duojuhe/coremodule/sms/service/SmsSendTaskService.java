package com.duojuhe.coremodule.sms.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.sms.pojo.sendtask.*;
import com.duojuhe.coremodule.sms.pojo.sendtask.detail.QuerySmsSendTaskDetailReq;
import com.duojuhe.coremodule.sms.pojo.sendtask.detail.QuerySmsSendTaskDetailRes;

import java.util.List;

public interface SmsSendTaskService {
    /*
     * 【分页查询】根据条件查询短信发送任务list
     * @param req
     * @return*/
    ServiceResult<PageResult<List<QuerySmsSendTaskPageRes>>> querySmsSendTaskPageResList(QuerySmsSendTaskPageReq req);


    /**
     * 根据任务id查询任务明细
     * @param req
     * @return
     */
    ServiceResult<QuerySmsSendTaskRes> querySmsSendTaskResBySendTaskId(SmsSendTaskIdReq req);

    /*
     * 【保存】短信发送任务
     * @param req
     * @return*/
    ServiceResult saveSmsSendTask(SaveSmsSendTaskReq req);


    /*
     * 【修改】短信发送任务
     * @param req
     * @return*/
    ServiceResult updateSmsSendTask(UpdateSmsSendTaskReq req);


    /*
     * 【删除】根据短信发送任务id删除任务
     * @param req
     * @return*/
    ServiceResult deleteSmsSendTaskBySendTaskId(SmsSendTaskIdReq req);


    /*
     * 【分页查询】根据短信发送任务id分页查询发送任务明细
     * @param req
     * @return*/
    ServiceResult<PageResult<List<QuerySmsSendTaskDetailRes>>> querySmsSendTaskDetailResList(QuerySmsSendTaskDetailReq req);

    /**
     * 执行短信发送任务
     */
    void executeSmsSendTask();

}
