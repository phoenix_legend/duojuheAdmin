package com.duojuhe.coremodule.sms.pojo.channel;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class HandleSmsChannelDictNameColorRes extends BaseBean {
    @ApiModelProperty(value = "渠道状态：FORBID禁用 NORMAL正常", example = "1")
    private String statusCode;

    @ApiModelProperty(value = "渠道状态中文 FORBID禁用 NORMAL正常", example = "1")
    private String statusName;

    @ApiModelProperty(value = "渠道状态显示颜色", example = "1")
    private String statusColor;


    @ApiModelProperty(value = "服务商状态：FORBID禁用 NORMAL正常", example = "1")
    private String ispStatusCode;

    @ApiModelProperty(value = "服务商状态中文 FORBID禁用 NORMAL正常", example = "1")
    private String ispStatusName;

    @ApiModelProperty(value = "服务商状态显示颜色", example = "1")
    private String ispStatusColor;

}
