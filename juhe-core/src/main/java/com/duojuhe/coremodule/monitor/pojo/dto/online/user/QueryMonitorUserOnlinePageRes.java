package com.duojuhe.coremodule.monitor.pojo.dto.online.user;

import com.duojuhe.common.annotation.ExcelFormat;
import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.bean.UserTokenInfoVo;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Getter
@Setter
public class QueryMonitorUserOnlinePageRes extends BaseBean {

    @ApiModelProperty(value = "在线id", example = "1")
    private String onlineId;

    @ApiModelProperty(value = "用户类型中文名", example = "1")
    private String userTypeName;

    @ApiModelProperty(value = "登录名", example = "1")
    private String loginName;

    @ApiModelProperty(value = "真实姓名", example = "1")
    private String realName;

    @ApiModelProperty(value = "最后登录IP", example = "1")
    private String loginIp;

    @ApiModelProperty(value = "租户名称", example = "1")
    private String tenantName;

    @ApiModelProperty(value = "所属租户简称", example = "1")
    private String tenantAbbreviation;

    @ApiModelProperty(value = "登录时间", example = "1")
    @JsonFormat(pattern= DateUtils.DEFAULT_DATETIME_FORMAT,timezone="GMT+8")
    @ExcelFormat(dateFormat = DateUtils.DEFAULT_DATETIME_FORMAT)
    private Date loginTime;

    public QueryMonitorUserOnlinePageRes(String onlineId,UserTokenInfoVo userTokenInfoVo){
        this.onlineId = onlineId;
        this.userTypeName = userTokenInfoVo.getUserTypeName();
        this.loginName = userTokenInfoVo.getLoginName();
        this.realName = userTokenInfoVo.getRealName();
        this.loginIp = userTokenInfoVo.getLoginIp();
        this.loginTime = userTokenInfoVo.getLoginTime();
        this.tenantName = userTokenInfoVo.getTenantName();
        this.tenantAbbreviation = userTokenInfoVo.getTenantAbbreviation();
    }

}
