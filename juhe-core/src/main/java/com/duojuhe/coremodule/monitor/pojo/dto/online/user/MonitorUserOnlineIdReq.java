package com.duojuhe.coremodule.monitor.pojo.dto.online.user;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MonitorUserOnlineIdReq extends BaseBean {
    @ApiModelProperty(value = "在线ID", example = "1",required=true)
    @NotBlank(message = "在线ID不能为空")
    private String onlineId;
}
