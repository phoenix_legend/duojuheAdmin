package com.duojuhe.coremodule.monitor.controller;

import com.duojuhe.common.constant.ApiPathConstants;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.monitor.pojo.dto.server.ServerRes;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 服务器监控
 * 
 */
@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/monitorServer/")
@Api(tags = {"【服务器监控】服务器监控管理相关接口"})
@Slf4j
public class MonitorServerController {

    @ApiOperation(value = "【服务器信息】获取服务器信息")
    @PostMapping(value = "queryMonitorServerRes")
    public ServiceResult<ServerRes> queryMonitorServerRes() throws Exception {
        ServerRes server = new ServerRes();
        server.copyTo();
        return ServiceResult.ok(server);
    }
}
