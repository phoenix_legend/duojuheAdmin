package com.duojuhe.coremodule.monitor.controller;

import com.duojuhe.common.annotation.OperationLog;
import com.duojuhe.common.constant.ApiPathConstants;

import com.duojuhe.common.enums.log.LogEnum;
import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.monitor.pojo.dto.online.user.MonitorUserOnlineIdReq;
import com.duojuhe.coremodule.monitor.pojo.dto.online.user.QueryMonitorUserOnlinePageReq;
import com.duojuhe.coremodule.monitor.pojo.dto.online.user.QueryMonitorUserOnlinePageRes;
import com.duojuhe.coremodule.monitor.service.MonitorUserOnlineService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * 在线用户监控
 *
 */
@RestController
@RequestMapping(value = ApiPathConstants.ADMIN_PATH + "/monitorOnline/")
@Api(tags = {"【在线用户监控】在线用户监控管理相关接口"})
@Slf4j
public class MonitorUserOnlineController {
    @Resource
    private MonitorUserOnlineService monitorUserOnlineService;


    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【分页查询】根据条件查询在线用户list")
    @PostMapping(value = "queryMonitorUserOnlinePageResList")
    public ServiceResult<PageResult<List<QueryMonitorUserOnlinePageRes>>> queryMonitorUserOnlinePageResList(@Valid @RequestBody @ApiParam(value = "入参类") QueryMonitorUserOnlinePageReq req) {
        return monitorUserOnlineService.queryMonitorUserOnlinePageResList(req);
    }

    @ApiOperation(value = "【强制踢出】根据在线id强制踢出")
    @PostMapping(value = "forceKickOutOnlineUserByOnlineId")
    @OperationLog(moduleName = LogEnum.ModuleName.ONLINE_USER, operationType = LogEnum.OperationType.KICK_OUT)
    public ServiceResult forceKickOutOnlineUserByOnlineId(@Valid @RequestBody @ApiParam(value = "入参类") MonitorUserOnlineIdReq req) {
        return monitorUserOnlineService.forceKickOutOnlineUserByOnlineId(req);
    }
}
