package com.duojuhe.coremodule.monitor.service;

import com.duojuhe.common.result.PageResult;
import com.duojuhe.common.result.ServiceResult;
import com.duojuhe.coremodule.monitor.pojo.dto.online.user.MonitorUserOnlineIdReq;
import com.duojuhe.coremodule.monitor.pojo.dto.online.user.QueryMonitorUserOnlinePageReq;
import com.duojuhe.coremodule.monitor.pojo.dto.online.user.QueryMonitorUserOnlinePageRes;

import java.util.List;

public interface MonitorUserOnlineService {
    /**
     * 分页查询 根据条件查询在线用户list
     */
    ServiceResult<PageResult<List<QueryMonitorUserOnlinePageRes>>> queryMonitorUserOnlinePageResList(QueryMonitorUserOnlinePageReq req);

    /**
     * 根据在线id强制踢出
     */
    ServiceResult forceKickOutOnlineUserByOnlineId(MonitorUserOnlineIdReq req);
}
