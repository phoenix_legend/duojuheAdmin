package com.duojuhe.coremodule.monitor.pojo.dto.server;



import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.common.utils.dateutils.DateUtils;
import com.duojuhe.common.utils.decimal.BigDecimalUtils;

import java.lang.management.ManagementFactory;
import java.util.Date;

/**
 * JVM相关信息
 **/
public class Jvm extends BaseBean {
    /**
     * 当前JVM占用的内存总数(M)
     */
    private double total;

    /**
     * JVM最大可用内存总数(M)
     */
    private double max;

    /**
     * JVM空闲内存(M)
     */
    private double free;

    /**
     * JDK版本
     */
    private String version;

    /**
     * JDK路径
     */
    private String home;

    public double getTotal()
    {
        return BigDecimalUtils.div(total, (1024 * 1024), 2);
    }

    public void setTotal(double total)
    {
        this.total = total;
    }

    public double getMax()
    {
        return BigDecimalUtils.div(max, (1024 * 1024), 2);
    }

    public void setMax(double max)
    {
        this.max = max;
    }

    public double getFree()
    {
        return BigDecimalUtils.div(free, (1024 * 1024), 2);
    }

    public void setFree(double free)
    {
        this.free = free;
    }

    public double getUsed()
    {
        return BigDecimalUtils.div(total - free, (1024 * 1024), 2);
    }

    public double getUsage()
    {
        return BigDecimalUtils.mul(BigDecimalUtils.div(total - free, total, 4), 100);
    }

    /**
     * 获取JDK名称
     */
    public String getName()
    {
        return ManagementFactory.getRuntimeMXBean().getVmName();
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getHome()
    {
        return home;
    }

    public void setHome(String home)
    {
        this.home = home;
    }

    /**
     * JDK启动时间
     */
    public String getStartTime(){
        return DateUtils.dateToString(SystemConstants.SYSTEM_START_DATE);
    }

    /**
     * JDK运行时间
     */
    public String getRunTime(){
        return DateUtils.getDatePoor(new Date(),SystemConstants.SYSTEM_START_DATE);
    }
}
