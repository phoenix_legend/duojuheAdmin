package com.duojuhe.coremodule.monitor.pojo.dto.server;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.decimal.BigDecimalUtils;

/**
 * 內存相关信息
 * 
 */
public class Mem extends BaseBean {
    /**
     * 内存总量
     */
    private double total;

    /**
     * 已用内存
     */
    private double used;

    /**
     * 剩余内存
     */
    private double free;

    public double getTotal()
    {
        return BigDecimalUtils.div(total, (1024 * 1024 * 1024), 2);
    }

    public void setTotal(long total)
    {
        this.total = total;
    }

    public double getUsed()
    {
        return BigDecimalUtils.div(used, (1024 * 1024 * 1024), 2);
    }

    public void setUsed(long used)
    {
        this.used = used;
    }

    public double getFree()
    {
        return BigDecimalUtils.div(free, (1024 * 1024 * 1024), 2);
    }

    public void setFree(long free)
    {
        this.free = free;
    }

    public double getUsage()
    {
        return BigDecimalUtils.mul(BigDecimalUtils.div(used, total, 4), 100);
    }
}
