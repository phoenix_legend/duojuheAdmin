package com.duojuhe.coremodule.monitor.pojo.dto.online.user;

import com.duojuhe.common.bean.PageReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QueryMonitorUserOnlinePageReq  extends PageReq {
    @ApiModelProperty(value = "登录名", example = "1")
    private String loginName;

    @ApiModelProperty(value = "真实姓名", example = "1")
    private String realName;
}
