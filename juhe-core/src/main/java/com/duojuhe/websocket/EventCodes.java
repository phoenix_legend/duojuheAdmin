package com.duojuhe.websocket;

import lombok.Getter;
import lombok.Setter;

public enum EventCodes {
    EVENT_TALK("event_talk", "聊天消息事件"),
    EVENT_ONLINE_STATUS("event_online_status", "好友登录状态事件"),
    EVENT_KEYBOARD("event_keyboard", "键盘输入事件"),
    EVENT_REVOKE_TALK("event_revoke_talk", "聊天消息撤销通知"),
    EVENT_DELETE_TALK("event_delete_talk", "聊天消息删除通知"),
    EVENT_FRIEND_APPLY("event_friend_apply", "好友申请事件"),
    EVENT_JOIN_GROUP("event_join_group", "好友邀请入群消息事件"),
    EVENT_HEARTBEAT("event_heartbeat", "心跳事件"),
    EVENT_SYSTEM_MESSAGE("event_system_message", "系统消息事件"),
    EVENT_ERROR("event_error", "错误事件");
    @Getter
    @Setter
    private String event;
    @Getter
    @Setter
    private String message;

    EventCodes(String event, String message) {
        this.event = event;
        this.message = message;
    }
}
