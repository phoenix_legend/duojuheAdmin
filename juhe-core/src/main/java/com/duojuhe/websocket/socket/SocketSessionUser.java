package com.duojuhe.websocket.socket;

import com.duojuhe.common.enums.chat.ImChatEnum;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.socket.WebSocketSession;

import java.util.Date;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Socket session用户
 *
 */
@Getter
@Setter
public class SocketSessionUser {
    //线程池
    private ThreadPoolExecutor threadPoolExecutor;
    //socket会话session
    private WebSocketSession session;
    //连接时间
    private Date connectDate;
    //最后心跳时间
    private Date heartDate;
    //session IP
    private String clientIp;
    //session 终端连接类型
    private String clientType;
    //用户 ID
    private String userId;
    //用户 当前会话token
    private String token;
    //是否在线
    private Integer onlineStatus;

    public SocketSessionUser(WebSocketSession session, int queueSize) {
        Date date = new Date();
        this.session = session;
        this.clientType = SocketUtil.getClientType(session);
        this.clientIp = SocketUtil.getClientIp(session);
        this.userId = SocketUtil.getUserId(session);
        this.token = SocketUtil.getUserToken(session);
        this.heartDate = date;
        this.onlineStatus = ImChatEnum.IM_YES_NO.YES.getKey();
        this.connectDate = date;
        ThreadFactory factory = (new ThreadFactoryBuilder()).setNameFormat("WebSocket-Send-Thread" + "-pool-%d").build();
        this.threadPoolExecutor = new ThreadPoolExecutor(1, 1, 1, TimeUnit.MINUTES, new LinkedBlockingQueue<>(queueSize), factory);
    }
}
