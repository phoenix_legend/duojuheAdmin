package com.duojuhe.websocket.socket;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.utils.idgenerator.UUIDUtils;
import com.duojuhe.websocket.EventCodes;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


/**
 * socket消息
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SocketMessage<T> extends BaseBean {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "消息id", example = "消息id")
    private String messageId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "事件类型，例如，event_heartbeat：心跳事件", example = "event_heartbeat")
    private String eventType;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "提示语", example = "操作成功")
    private String message;

    @ApiModelProperty(value = "响应内容")
    private T data;



    /**
     * 发送消息
     */
    public static SocketMessage eventSendData(Object dataContent, EventCodes eventCodes) {
        return SocketMessage.builder()
                .messageId(UUIDUtils.getUUID32())
                .eventType(eventCodes.getEvent())
                .message(null)
                .data(dataContent)
                .build();
    }

    /**
     * 心跳方法
     */
    public static SocketMessage eventHeartbeat() {
        return SocketMessage.builder()
                .messageId(UUIDUtils.getUUID32())
                .eventType(EventCodes.EVENT_HEARTBEAT.getEvent())
                .message(SocketUtil.SOCKET_PONG)
                .data(SocketUtil.SOCKET_PONG)
                .build();
    }


    /**
     * 系统消息方法
     */
    public static SocketMessage eventSystemMessage(Object dataContent) {
        return SocketMessage.builder()
                .messageId(UUIDUtils.getUUID32())
                .eventType(EventCodes.EVENT_SYSTEM_MESSAGE.getEvent())
                .message(EventCodes.EVENT_SYSTEM_MESSAGE.getMessage())
                .data(dataContent)
                .build();
    }

    /**
     * 返回失败原因信息
     */
    public static SocketMessage eventError(String message) {
        return SocketMessage.builder()
                .messageId(UUIDUtils.getUUID32())
                .eventType(EventCodes.EVENT_ERROR.getEvent())
                .message(message)
                .data(message)
                .build();
    }
}
