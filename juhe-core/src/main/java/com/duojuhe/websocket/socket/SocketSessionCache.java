package com.duojuhe.websocket.socket;


import com.duojuhe.common.enums.chat.ImChatEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.socket.CloseStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * socket缓存
 *
 */
@Slf4j
public class SocketSessionCache {
    //会话缓存
    private static Map<String, SocketSessionUser> SESSION_USER_CACHE = new ConcurrentHashMap<>();

    /**
     * 获取所有客户端连接数量
     */
    public static int getSessionUserCacheCount() {
        return SESSION_USER_CACHE.size();
    }

    /**
     * 新增一个连接
     * @param sessionId
     * @param sessionUser
     */
    public static void addSessionUserCache(String sessionId, SocketSessionUser sessionUser) {
        SESSION_USER_CACHE.put(sessionId, sessionUser);
    }

    /**
     * 获取系统连接的连接map总数
     *
     * @return
     */
    public static Map<String, SocketSessionUser> getAllSessionUser() {
        return SESSION_USER_CACHE;
    }

    /**
     * 根据用户id获取会话连接
     * @param userId
     * @return
     */
    public static List<SocketSessionUser> getSocketSessionUserListByUserId(String userId) {
        List<SocketSessionUser> sessionUserList = new ArrayList<>();
        if (StringUtils.isBlank(userId)){
            return sessionUserList;
        }

        return SESSION_USER_CACHE.values().stream().filter(socketSessionUser -> userId.equals(socketSessionUser.getUserId())).collect(Collectors.toList());
    }


    /**
     * 获取一个连接
     * @param sessionId
     * @return
     */

    public static SocketSessionUser getSessionUserBySessionId(String sessionId) {
        return SESSION_USER_CACHE.get(sessionId);
    }

    /**
     * 移除一个连接
     */
    public static void removeSessionUserByToken(String token) {
        try {
            if (StringUtils.isBlank(token)){
                return;
            }
            for (Map.Entry<String, SocketSessionUser> entry : SESSION_USER_CACHE.entrySet()) {
                SocketSessionUser socketSessionUser = entry.getValue();
                if (socketSessionUser.getToken().equals(token)) {
                    socketSessionUser.setOnlineStatus(ImChatEnum.IM_YES_NO.NO.getKey());
                    socketSessionUser.getSession().close(CloseStatus.SERVICE_RESTARTED);
                }
            }
        }catch (Exception e){

        }
    }

    /**
     * 移除一个连接
     */
    public static void removeSessionUserBySessionId(String sessionId) {
        SESSION_USER_CACHE.remove(sessionId);
    }

    /**
     * 根据用户id判断该用户是否在线
     */
    public static boolean checkSocketSessionUserIsOnlineByUserId(String userId) {
        Integer yes = ImChatEnum.IM_YES_NO.YES.getKey();
        for (Map.Entry<String, SocketSessionUser> entry : SESSION_USER_CACHE.entrySet()) {
            SocketSessionUser socketSessionUser = entry.getValue();
            if (socketSessionUser.getUserId().equals(userId)&&yes.equals(socketSessionUser.getOnlineStatus())) {
                return true;
            }
        }
        return false;
    }
}
