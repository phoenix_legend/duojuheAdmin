package com.duojuhe.websocket.subscriber;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SubscribeHandleDto<T>  extends BaseBean {
    @ApiModelProperty(value = "消息事件", example = "消息事件")
    private String eventType;

    @ApiModelProperty(value = "消息内容")
    private T messageData;
}
