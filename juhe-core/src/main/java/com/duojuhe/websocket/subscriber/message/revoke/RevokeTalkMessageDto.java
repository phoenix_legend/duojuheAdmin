package com.duojuhe.websocket.subscriber.message.revoke;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/*撤销聊天消息推送协议*/
@Getter
@Setter
public class RevokeTalkMessageDto extends BaseBean {
    @ApiModelProperty(value = "消息接收对象id")
    private String receiverId;

    @ApiModelProperty(value = "发送者对象id")
    private String senderId;

    @ApiModelProperty(value = "对话类型[1:私信;2:群聊;]")
    private Integer talkType;

    @ApiModelProperty(value = "撤销的记录")
    private String recordId;


    public RevokeTalkMessageDto(Integer talkType, String senderId, String receiverId, String recordId){
        this.recordId=recordId;
        this.senderId=senderId;
        this.receiverId= receiverId;
        this.talkType =talkType;
    }
}
