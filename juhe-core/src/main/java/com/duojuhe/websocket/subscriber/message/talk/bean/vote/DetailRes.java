package com.duojuhe.websocket.subscriber.message.talk.bean.vote;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DetailRes extends BaseBean {
    @ApiModelProperty(value = "id", example = "1")
    private String id;

    @ApiModelProperty(value = "消息id", example = "1")
    private String recordId;

    @ApiModelProperty(value = "投票标题", example = "1")
    private String title;

    @ApiModelProperty(value = "答题模式[0:单选;1:多选;]", example = "1")
    private Integer answerMode;

    @ApiModelProperty(value = "投票状态[0:投票中;1:已完成;]", example = "1")
    private Integer status;

    @ApiModelProperty(value = "应答人数")
    private Integer answerNum;

    @ApiModelProperty(value = "已答人数")
    private Integer answeredNum;

    @ApiModelProperty(value = "投票选项")
    private List<AnswerOptionRes> answerOptionList;
}
