package com.duojuhe.websocket.subscriber.message.talk.bean;

import com.duojuhe.websocket.subscriber.message.talk.bean.invite.ChatUserRes;
import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class InviteRes extends BaseBean {
    @ApiModelProperty(value = "邀请通知类型", example = "1")
    private Integer inviteType;

    @ApiModelProperty(value = "操作用户", example = "1")
    private ChatUserRes operateUser;

    @ApiModelProperty(value = "被操作用户", example = "1")
    private List<ChatUserRes> userList;
}
