package com.duojuhe.websocket.subscriber.message.talk.bean.vote;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class StatisticsRes extends BaseBean {
    @ApiModelProperty(value = "总数量", example = "1",required=true)
    private Integer count;

    @ApiModelProperty(value = "选项统计值", example = "1",required=true)
    private Object options;
}
