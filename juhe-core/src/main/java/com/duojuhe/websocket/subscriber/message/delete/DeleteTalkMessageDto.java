package com.duojuhe.websocket.subscriber.message.delete;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/*删除聊天消息推送协议*/
@Getter
@Setter
public class DeleteTalkMessageDto extends BaseBean {
    @ApiModelProperty(value = "消息接收对象id")
    private String receiverId;

    @ApiModelProperty(value = "发送者对象id")
    private String senderId;

    @ApiModelProperty(value = "对话类型[1:私信;2:群聊;]")
    private Integer talkType;

    @ApiModelProperty(value = "删除的记录")
    private List<String> recordIdList;

    public DeleteTalkMessageDto(Integer talkType, String senderId, String receiverId, List<String> recordIdList){
        this.recordIdList=recordIdList;
        this.senderId=senderId;
        this.receiverId= receiverId;
        this.talkType =talkType;
    }
}
