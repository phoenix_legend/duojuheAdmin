package com.duojuhe.websocket.subscriber.message.talk.bean;

import com.duojuhe.coremodule.chat.im.pojo.dto.talkrecord.forward.ForwardSnapshotRecordRes;
import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class ForwardRes extends BaseBean {
    @ApiModelProperty(value = "转发总数量", example = "1")
    private Integer totalNum;

    @ApiModelProperty(value = "转发对象", example = "1")
    private List<ForwardSnapshotRecordRes> forwardSnapshotRecordResList;

    public ForwardRes(Integer totalNum, List<ForwardSnapshotRecordRes> forwardSnapshotRecordResList){
        this.totalNum = totalNum;
        this.forwardSnapshotRecordResList = forwardSnapshotRecordResList;
    }
}
