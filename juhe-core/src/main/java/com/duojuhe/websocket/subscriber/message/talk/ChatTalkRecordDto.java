package com.duojuhe.websocket.subscriber.message.talk;

import com.duojuhe.websocket.subscriber.message.talk.bean.*;
import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ChatTalkRecordDto extends BaseBean {
    @ApiModelProperty(value = "主键", required = true)
    private String recordId;

    @ApiModelProperty(value = "消息类型[1:文本消息;2:文件消息;3:会话消息;4:代码消息;5:投票消息;6:群公告;7:好友申请;8:登录通知;9:入群消息/退群消息;]")
    private Integer msgType;

    @ApiModelProperty(value = "文件对象")
    private FileRes file;

    @ApiModelProperty(value = "代码片段对象")
    private CodeBlockRes codeBlock;

    @ApiModelProperty(value = "投票对象")
    private VoteRes vote;

    @ApiModelProperty(value = "转发会话消息")
    private ForwardRes forward;

    @ApiModelProperty(value = "入群退群邀请对象")
    private InviteRes invite;

}
