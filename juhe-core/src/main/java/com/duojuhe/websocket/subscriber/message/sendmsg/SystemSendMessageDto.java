package com.duojuhe.websocket.subscriber.message.sendmsg;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class SystemSendMessageDto extends BaseBean {
    @ApiModelProperty(value = "用户ID")
    private String userId;

    @ApiModelProperty(value = "未读消息推送")
    private MessageUntreatedDto messageUntreatedDto;

    public SystemSendMessageDto(String userId, MessageUntreatedDto messageUntreatedDto){
        this.userId=userId;
        this.messageUntreatedDto=messageUntreatedDto;
    }
}
