package com.duojuhe.websocket.subscriber.message.talk.bean;

import com.duojuhe.websocket.subscriber.message.talk.bean.vote.DetailRes;
import com.duojuhe.websocket.subscriber.message.talk.bean.vote.StatisticsRes;
import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class VoteRes extends BaseBean {
    @ApiModelProperty(value = "投票统计", example = "1")
    private StatisticsRes statistics;

    @ApiModelProperty(value = "参与用户", example = "1")
    private List<String> voteUsers;

    @ApiModelProperty(value = "投票详情", example = "1")
    private DetailRes detail;
}
