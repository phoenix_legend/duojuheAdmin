package com.duojuhe.websocket.subscriber.message.talk;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MessageBodyRes extends ChatTalkRecordDto {
    @ApiModelProperty(value = "id", example = "1")
    private String id;

    @ApiModelProperty(value = "消息接收对象id")
    private String receiverId;

    @ApiModelProperty(value = "发送者对象id")
    private String senderId;

    @ApiModelProperty(value = "对话类型[1:私信;2:群聊;]")
    private Integer talkType;

    @ApiModelProperty(value = "发送者ID（-1:代表系统消息 >-1: 用户ID）")
    private String userId;

    @ApiModelProperty(value = "发送者姓名", required = true)
    private String realName;

    @ApiModelProperty(value = "发送者头像", required = true)
    private String headPortrait;

    @ApiModelProperty(value = "群名称", required = true)
    private String groupName;

    @ApiModelProperty(value = "群头像")
    private String groupAvatar;

    @ApiModelProperty(value = "创建时间")
    private String createTime;

    @ApiModelProperty(value = "是否撤回消息[0:否;1:是]")
    private Integer isRevoke;

    @ApiModelProperty(value = "是否重要消息[0:否;1:是;]")
    private Integer isMark;

    @ApiModelProperty(value = "是否已读[0:否;1:是;]")
    private Integer isRead;

    @ApiModelProperty(value = "消息内容")
    private String content;

    @ApiModelProperty(value = "是否给自己发送")
    private Boolean sendSenderSelfFlag;
}