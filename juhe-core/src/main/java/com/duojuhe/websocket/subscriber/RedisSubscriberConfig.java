package com.duojuhe.websocket.subscriber;

import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.websocket.subscriber.listener.ImMessageListener;
import com.duojuhe.websocket.subscriber.listener.KfMessageListener;
import com.duojuhe.websocket.subscriber.listener.SystemMessageListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

/**
 * @Description 消息订阅配置类
 * @Date 2020/3/31 13:54
 */
@Configuration
public class RedisSubscriberConfig {

    /*==========================聊天消息监听适配器beg==========================*/
    /**
     * 聊天消息监听适配器，注入接受消息方法
     *
     * @param receiver
     * @return
     */
    @Bean
    public MessageListenerAdapter imMessageListenerAdapter(ImMessageListener receiver) {
        return new MessageListenerAdapter(receiver);
    }

    /**
     * 创建聊天消息监听容器
     * @param redisConnectionFactory
     * @param imMessageListenerAdapter
     * @return
     */
    @Bean
    public RedisMessageListenerContainer getRedisImMessageListenerContainer(RedisConnectionFactory redisConnectionFactory,
                                                                            MessageListenerAdapter imMessageListenerAdapter) {
        RedisMessageListenerContainer redisMessageListenerContainer = new RedisMessageListenerContainer();
        redisMessageListenerContainer.setConnectionFactory(redisConnectionFactory);
        redisMessageListenerContainer.addMessageListener(imMessageListenerAdapter, new PatternTopic(SystemConstants.IM_CHAT_MQ_KEY));
        return redisMessageListenerContainer;
    }
    /*==========================聊天消息监听适配器end==========================*/



    /*==========================客服消息监听适配器beg==========================*/
    /**
     * 客服消息监听适配器，注入接受消息方法
     *
     * @param receiver
     * @return
     */
    @Bean
    public MessageListenerAdapter kfMessageListenerAdapter(KfMessageListener receiver) {
        return new MessageListenerAdapter(receiver);
    }


    /**
     * 创建客服消息监听容器
     * @param redisConnectionFactory
     * @param kfMessageListenerAdapter
     * @return
     */
    @Bean
    public RedisMessageListenerContainer getRedisKfMessageListenerContainer(RedisConnectionFactory redisConnectionFactory,MessageListenerAdapter kfMessageListenerAdapter) {
        RedisMessageListenerContainer redisMessageListenerContainer = new RedisMessageListenerContainer();
        redisMessageListenerContainer.setConnectionFactory(redisConnectionFactory);
        redisMessageListenerContainer.addMessageListener(kfMessageListenerAdapter, new PatternTopic(SystemConstants.KF_CHAT_MQ_KEY));
        return redisMessageListenerContainer;
    }
    /*==========================客服消息监听适配器end==========================*/



    /*==========================系统消息监听适配器beg==========================*/

    /**
     * 系统消息监听适配器，注入接受消息方法
     *
     * @param receiver
     * @return
     */
    @Bean
    public MessageListenerAdapter systemMessageListenerAdapter(SystemMessageListener receiver) {
        return new MessageListenerAdapter(receiver);
    }

    /**
     * 创建系统消息监听容器
     * @param redisConnectionFactory
     * @param systemMessageListenerAdapter
     * @return
     */
    @Bean
    public RedisMessageListenerContainer getRedisSystemMessageListenerContainer(RedisConnectionFactory redisConnectionFactory,
                                                                            MessageListenerAdapter systemMessageListenerAdapter) {
        RedisMessageListenerContainer redisMessageListenerContainer = new RedisMessageListenerContainer();
        redisMessageListenerContainer.setConnectionFactory(redisConnectionFactory);
        redisMessageListenerContainer.addMessageListener(systemMessageListenerAdapter, new PatternTopic(SystemConstants.SYSTEM_MESSAGE_MQ_KEY));
        return redisMessageListenerContainer;
    }

    /*==========================系统消息监听适配器end==========================*/
}