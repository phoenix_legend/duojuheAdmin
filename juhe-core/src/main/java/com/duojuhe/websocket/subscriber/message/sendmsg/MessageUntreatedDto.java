package com.duojuhe.websocket.subscriber.message.sendmsg;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageUntreatedDto extends BaseBean {
    @ApiModelProperty(value = "未读数量")
    private Integer untreatedNum;

    @ApiModelProperty(value = "未读消息类型")
    private String untreatedType;

    public MessageUntreatedDto(Integer untreatedNum, String untreatedType){
        this.untreatedNum=untreatedNum;
        this.untreatedType=untreatedType;
    }
}
