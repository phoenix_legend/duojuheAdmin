package com.duojuhe.websocket.subscriber.message.online;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/*好友在线状态通知消息推送协议*/
@Getter
@Setter
public class OnlineStatusMessageDto extends BaseBean {

    @ApiModelProperty(value = "用户ID")
    private String userId;

    @ApiModelProperty(value = "在线状态[0:离线;1:在线;]")
    private Integer status;

    public OnlineStatusMessageDto(String userId, Integer status){
        this.userId=userId;
        this.status=status;
    }
}
