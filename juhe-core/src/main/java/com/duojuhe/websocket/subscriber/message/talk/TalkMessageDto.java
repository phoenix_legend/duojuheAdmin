package com.duojuhe.websocket.subscriber.message.talk;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/*对话的消息推送协议*/
@Getter
@Setter
public class TalkMessageDto extends BaseBean {
    @ApiModelProperty(value = "消息接收对象id")
    private String receiverId;

    @ApiModelProperty(value = "发送者对象id")
    private String senderId;

    @ApiModelProperty(value = "对话类型[1:私信;2:群聊;]")
    private Integer talkType;

    @ApiModelProperty(value = "消息内容")
    private MessageBodyRes messageBody;

    @ApiModelProperty(value = "是否给自己发送")
    private Boolean sendSenderSelfFlag;


    public TalkMessageDto(MessageBodyRes messageBody){
        this.messageBody=messageBody;
        this.senderId=messageBody.getSenderId();
        this.receiverId= messageBody.getReceiverId();
        this.talkType =messageBody.getTalkType();
        this.sendSenderSelfFlag =messageBody.getSendSenderSelfFlag();
    }
}
