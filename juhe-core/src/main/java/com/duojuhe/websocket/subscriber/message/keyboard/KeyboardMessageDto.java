package com.duojuhe.websocket.subscriber.message.keyboard;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class KeyboardMessageDto  extends BaseBean {
    @ApiModelProperty(value = "消息接收对象id")
    private String receiverId;

    @ApiModelProperty(value = "发送者对象id")
    private String senderId;

    public KeyboardMessageDto(String senderId, String receiverId){
        this.senderId=senderId;
        this.receiverId= receiverId;
    }
}
