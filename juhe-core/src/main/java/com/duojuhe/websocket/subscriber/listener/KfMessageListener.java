package com.duojuhe.websocket.subscriber.listener;



import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;

/**
 * @Description 集群KF消息监听器
 * @Date 2020/3/29 15:07
 */
@Slf4j
@Component
public class KfMessageListener implements MessageListener {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void onMessage(Message message, byte[] pattern) {
        RedisSerializer<String> valueSerializer = stringRedisTemplate.getStringSerializer();
        String messageContent = valueSerializer.deserialize(message.getBody());
        JSONObject jsonObject= JSONObject.parseObject(messageContent);
        //将字符串转成对象
        if (jsonObject==null){
            log.info("监听集群KF Websocket消息为空---{}",messageContent);
            return;
        }
        log.info("监听集群KF Websocket消息--- {}", jsonObject);
    }

}