package com.duojuhe.websocket.subscriber.message.talk.bean.invite;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.coremodule.system.entity.SystemUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@NoArgsConstructor
@Getter
@Setter
public class ChatUserRes extends BaseBean {
    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "消息记录发布者")
    private String realName;

    public ChatUserRes(SystemUser systemUser){
        this.userId = systemUser.getUserId();
        this.realName = systemUser.getRealName();
    }
}
