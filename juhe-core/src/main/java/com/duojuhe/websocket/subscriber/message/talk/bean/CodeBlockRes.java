package com.duojuhe.websocket.subscriber.message.talk.bean;

import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecordCode;
import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CodeBlockRes extends BaseBean {
    @ApiModelProperty(value = "id", example = "1")
    private String id;

    @ApiModelProperty(value = "消息记录id")
    private String recordId;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "代码片段编程语言(如：php,java,python)")
    private String codeLang;

    @ApiModelProperty(value = "代码片段内容")
    private String codeContent;

    public CodeBlockRes(ChatTalkRecordCode recordCode){
        this.id = recordCode.getCodeId();
        this.recordId = recordCode.getRecordId();
        this.userId = recordCode.getUserId();
        this.codeLang = recordCode.getCodeLang();
        this.codeContent = recordCode.getCodeContent();
    }
}
