package com.duojuhe.websocket.subscriber.message.talk.bean.vote;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AnswerOptionRes extends BaseBean {
    @ApiModelProperty(value = "键", example = "1",required=true)
    private String key;

    @ApiModelProperty(value = "值", example = "1",required=true)
    private String value;
}
