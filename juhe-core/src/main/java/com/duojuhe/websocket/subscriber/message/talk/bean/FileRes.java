package com.duojuhe.websocket.subscriber.message.talk.bean;

import com.duojuhe.coremodule.chat.im.entity.ChatTalkRecordFile;
import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FileRes extends BaseBean {
    @ApiModelProperty(value = "id", example = "1")
    private String id;

    @ApiModelProperty(value = "消息记录id")
    private String recordId;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "文件类型[1:图片;2:视频;3:文件]")
    private Integer fileType;

    @ApiModelProperty(value = "文件路径")
    private String fileUrl;

    @ApiModelProperty(value = "原文件名")
    private String originalName;

    @ApiModelProperty(value = "文件后缀")
    private String fileSuffix;

    @ApiModelProperty(value = "文件大小（单位字节）")
    private Long fileSize;


    public FileRes(ChatTalkRecordFile recordFile){
        this.id = recordFile.getFileId();
        this.recordId = recordFile.getRecordId();
        this.userId = recordFile.getUserId();
        this.fileUrl = recordFile.getFileUrl();
        this.fileType = recordFile.getFileType();
        this.fileSuffix = recordFile.getFileSuffix();
        this.originalName = recordFile.getOriginalName();
        this.fileSize = recordFile.getFileSize();
    }
}
