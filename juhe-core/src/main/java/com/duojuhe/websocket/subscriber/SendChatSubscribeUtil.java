package com.duojuhe.websocket.subscriber;

import com.alibaba.fastjson.JSONObject;
import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.redis.RedisCache;
import com.duojuhe.common.utils.thread.ThreadUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Slf4j
@Component
public class SendChatSubscribeUtil {
    @Resource
    private RedisCache redisCache;
    /**
     * 聊天发布订阅消息
     * @param subscribeHandleDto
     */
    public void sendImSubscribeMessage(SubscribeHandleDto subscribeHandleDto) {
        if (subscribeHandleDto!=null){
            JSONObject sendJsonObject = (JSONObject) JSONObject.toJSON(subscribeHandleDto);
            //异步发送
            ThreadUtils.execute(() -> redisCache.convertAndSend(SystemConstants.IM_CHAT_MQ_KEY, sendJsonObject));
        }
    }


    /**
     * 集群客服发布订阅消息
     * @param subscribeHandleDto
     */
    public void sendKfSubscribeMessage(SubscribeHandleDto subscribeHandleDto) {
        if (subscribeHandleDto!=null){
            JSONObject sendJsonObject = (JSONObject) JSONObject.toJSON(subscribeHandleDto);
            //异步发送
            ThreadUtils.execute(() -> redisCache.convertAndSend(SystemConstants.KF_CHAT_MQ_KEY, sendJsonObject));
        }
    }

    /**
     * 系统消息发布订阅消息
     * @param subscribeHandleDto
     */
    public void sendSystemSubscribeMessage(SubscribeHandleDto subscribeHandleDto) {
        if (subscribeHandleDto!=null){
            JSONObject sendJsonObject = (JSONObject) JSONObject.toJSON(subscribeHandleDto);
            //异步发送
            ThreadUtils.execute(() -> redisCache.convertAndSend(SystemConstants.SYSTEM_MESSAGE_MQ_KEY, sendJsonObject));
        }
    }

}
