package com.duojuhe.cache;
import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.redis.RedisCache;
import com.duojuhe.coremodule.system.entity.SystemParameter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;

@Slf4j
@Component
public class SystemParameterCache {
    @Resource
    private RedisCache redisCache;

    /**
     * 根据code 查询系统参数对象
     *
     * @param parameterCode
     */
    public SystemParameter getSystemParameterByParameterCode(String parameterCode) {
        if (StringUtils.isBlank(parameterCode)) {
            return null;
        }
        return redisCache.getCacheObject(crateCacheKey(parameterCode));
    }

    /**
     * 存入系统参数缓存
     *
     * @param systemParameterList
     */
    public void putSystemParameterCache(List<SystemParameter> systemParameterList) {
        for (SystemParameter systemParameter : systemParameterList) {
            putSystemParameterCache(systemParameter);
        }
    }

    /**
     * 存入系统缓存
     *
     * @param parameter
     */
    public void putSystemParameterCache(SystemParameter parameter) {
        if (parameter != null) {
            redisCache.setCacheObject(crateCacheKey(parameter.getParameterCode()), parameter);
        }
    }



    /**
     * 根据code 删除系统参数对象
     *
     * @param parameterCode
     */
    public void deleteSystemParameterByParameterCode(String parameterCode) {
        if (StringUtils.isNotBlank(parameterCode)) {
            redisCache.del(crateCacheKey(parameterCode));
        }
    }

    /**
     * 清空系统参数缓存
     *
     */
    public void clearSystemParameterCache() {
        Collection<String> keys = redisCache.keysString(SystemConstants.SYS_PARAMETER_KEY + "*");
        redisCache.deleteObject(keys);
    }


    /**
     *  构建系统参数的key
     * @param parameterCode
     * @return
     */
    private String crateCacheKey(String parameterCode){
        return redisCache.getKey(SystemConstants.SYS_PARAMETER_KEY,parameterCode);
    }
}
