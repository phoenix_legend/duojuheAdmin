package com.duojuhe.cache;

import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.redis.RedisQueue;
import com.duojuhe.coremodule.sms.pojo.record.SmsRepeatRecordDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class SmsRepeatSendCache {

    private RedisQueue<SmsRepeatRecordDto> smsSendRedisQueue;

    public SmsRepeatSendCache(RedisTemplate redisTemplate) {
        this.smsSendRedisQueue = new RedisQueue(redisTemplate, SystemConstants.SMS_SEND_REDIS_QUEUE);
    }


    /**
     * 获取队列数量
     * @return
     */
    public Long getQueueSize(){
        return smsSendRedisQueue.getQueueSize();
    }

    /**
     * 将需要重发的短信放到队列中
     *
     * @param repeatRecordDto
     */
    public void putSmsRecordCache(SmsRepeatRecordDto repeatRecordDto) {
        if (repeatRecordDto != null) {
            smsSendRedisQueue.pushFromTail(repeatRecordDto);
        }
    }

    /**
     * 从队列头部拿一个
     *
     */
    public SmsRepeatRecordDto takeFromHead() {
        try {
            return smsSendRedisQueue.takeFromHead();
        }catch (Exception e){
            return null;
        }
    }

}
