package com.duojuhe.cache;

import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.coremodule.system.entity.SystemConfig;
import com.duojuhe.redis.RedisCache;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class SystemConfigCache {
    @Resource
    private RedisCache redisCache;

    /**
     * 存入系统配置缓存
     *
     * @param systemConfig
     */
    public void putSystemConfigCache(SystemConfig systemConfig) {
        redisCache.set(crateCacheKey(),systemConfig);
    }

    /**
     * 【系统配置】取出缓存
     * @return
     */
    public SystemConfig getSystemConfigCache() {
       try {
           Object o = redisCache.get(crateCacheKey());
           if (o==null){
               return new SystemConfig();
           }
           return (SystemConfig)o;
       }catch (Exception e){
           return new SystemConfig();
       }
    }



    /**
     * 构建系统配置key
     * @return
     */
    private String crateCacheKey(){
        return redisCache.getKey(SystemConstants.SYSTEM_CONFIG_KEY,"systemConfig");
    }

}
