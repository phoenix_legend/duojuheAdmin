package com.duojuhe.cache;

import com.duojuhe.common.bean.UserTokenInfoVo;
import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.common.enums.SystemEnum;
import com.duojuhe.redis.RedisCache;
import com.duojuhe.common.exception.base.DuoJuHeException;
import com.duojuhe.common.result.ErrorCodes;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Slf4j
@Component
public class LoginUserTokenCache {
    @Resource
    private RedisCache redisCache;

    /**
     * 获取当前登录用户的相关数据信息
     *
     * @return
     */
    public static UserTokenInfoVo getCurrentLoginUserInfoDTO() {
        String unknownId = SystemConstants.UNKNOWN_ID;
        try {
            UserTokenInfoVo userTokenInfoVo = (UserTokenInfoVo) SecurityUtils.getSubject().getPrincipal();
            if (userTokenInfoVo == null) {
                userTokenInfoVo = new UserTokenInfoVo();
                userTokenInfoVo.setUserId(unknownId);
                userTokenInfoVo.setUserNumber(unknownId);
                userTokenInfoVo.setPostId(unknownId);
                userTokenInfoVo.setCreateDeptId(unknownId);
                userTokenInfoVo.setTenantId(unknownId);
                userTokenInfoVo.setTenantNumber(unknownId);
                userTokenInfoVo.setUserLoginSource(SystemEnum.LOGIN_RESOURCE.VISITORS_USER.getKey());
            }
            return userTokenInfoVo;
        }catch (Exception e){
            UserTokenInfoVo userTokenInfoVo = new UserTokenInfoVo();
            userTokenInfoVo.setUserId(unknownId);
            userTokenInfoVo.setUserNumber(unknownId);
            userTokenInfoVo.setPostId(unknownId);
            userTokenInfoVo.setCreateDeptId(unknownId);
            userTokenInfoVo.setTenantId(unknownId);
            userTokenInfoVo.setTenantNumber(unknownId);
            userTokenInfoVo.setUserLoginSource(SystemEnum.LOGIN_RESOURCE.VISITORS_USER.getKey());
            return userTokenInfoVo;
        }
    }

    /**
     * 【token】取出缓存
     *
     * @param token
     * @return
     */
    public  UserTokenInfoVo getUserTokenInfoVoCacheByToken(String token) {
        if (StringUtils.isBlank(token)) {
            return null;
        }
        return (UserTokenInfoVo)redisCache.get(buildTokenKey(token));
    }


    /**
     * 【token】根据用户id判断该用户是否在线
     *
     * @param userId
     * @return
     */
    public boolean checkUserIsOnline(String userId) {
        if (StringUtils.isBlank(userId)) {
            return false;
        }
        Collection<String> keys = redisCache.keysString(buildTokenKey(""));
        for (String key : keys){
            UserTokenInfoVo userTokenInfoVo = redisCache.getCacheObject(key);
            if (userId.equals(userTokenInfoVo.getUserId())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 【token】取出所有token缓存对象
     *
     * @return
     */
    public List<UserTokenInfoVo> getAllAdminUserInfoListCache() {
        List<UserTokenInfoVo> userTokenInfoVoList = new ArrayList<>();
        for (Object obj : redisCache.multiGetByPrefix(buildTokenKey("*"))) {
            UserTokenInfoVo userTokenInfoVo = (UserTokenInfoVo)obj;
            userTokenInfoVoList.add(userTokenInfoVo);
        }
        return userTokenInfoVoList;
    }



    /**
     * 【token】添加token缓存
     *
     * @param userTokenInfoVo
     * @return
     */
    public void putUserTokenInfoVoToCache(String token,UserTokenInfoVo userTokenInfoVo,int time) throws DuoJuHeException {
        if (!redisCache.set(buildTokenKey(token),userTokenInfoVo,time* 60L)){
            throw new DuoJuHeException(ErrorCodes.REDIS_ERROR);
        }
    }

    /**
     * 获取并更新缓存
     */
    private void updateSetUserTokenInfoVo(final String token, UserTokenInfoVo userTokenInfoVo) {
        try {
            if (StringUtils.isBlank(token)){
                return;
            }
            redisCache.getAndSet(buildTokenKey(token), userTokenInfoVo);
        } catch (Exception e) {
            log.error("【登录token】获取并更新缓存出现异常",e);
        }
    }


    /**
     * 【token】删除缓存
     *
     * @param token
     */
    public void delUserTokenInfoVoCacheByToken(String token) {
        if (StringUtils.isNotBlank(token)) {
            redisCache.del(buildTokenKey(token));
        }
    }

    /**
     * 【token】刷新token
     * @param token
     * @param time 单位秒
     */
    public void refreshUserTokenInfoVoCacheByToken(String token,int time) {
        if (StringUtils.isNotBlank(token)) {
            redisCache.expire(buildTokenKey(token),time* 60L);
        }
    }

    /**
     * 【token】根据token强制踢出
     * @param token
     */
    public void forceKickOutOnlineUserByToken(String token) {
        if (StringUtils.isNotBlank(token)) {
            UserTokenInfoVo userTokenInfoVo = getUserTokenInfoVoCacheByToken(token);
            if (userTokenInfoVo!=null){
                //设置被踢出
                userTokenInfoVo.setKickOut(true);
                //获取并更新
                updateSetUserTokenInfoVo(token,userTokenInfoVo);
            }
        }
    }


    /**
     * 根据用户角色id踢出相关会话
     *
     * @return
     */
    public void kickOutOnlineUserByRoleId(String roleId) {
        if (StringUtils.isBlank(roleId)){
            return;
        }
        for (Object obj : redisCache.multiGetByPrefix(buildTokenKey("*"))) {
            UserTokenInfoVo userTokenInfoVo = (UserTokenInfoVo)obj;
            if (StringUtils.equals(userTokenInfoVo.getRoleId(),roleId)){
                //设置被踢出
                userTokenInfoVo.setKickOut(true);
                //获取并更新
                updateSetUserTokenInfoVo(userTokenInfoVo.getToken(),userTokenInfoVo);
            }
        }
    }


    /**
     * 根据用户部门id踢出相关会话
     *
     * @return
     */
    public void kickOutOnlineUserByDeptId(String deptId) {
        if (StringUtils.isBlank(deptId)){
            return;
        }
        for (Object obj : redisCache.multiGetByPrefix(buildTokenKey("*"))) {
            UserTokenInfoVo userTokenInfoVo = (UserTokenInfoVo)obj;
            if (StringUtils.equals(userTokenInfoVo.getCreateDeptId(),deptId)){
                //设置被踢出
                userTokenInfoVo.setKickOut(true);
                //获取并更新
                updateSetUserTokenInfoVo(userTokenInfoVo.getToken(),userTokenInfoVo);
            }
        }
    }



    /**
     * 根据用户id踢出相关会话
     *
     * @return
     */
    public void kickOutOnlineUserByUserId(String userId) {
        if (StringUtils.isBlank(userId)){
            return;
        }
        for (Object obj : redisCache.multiGetByPrefix(buildTokenKey("*"))) {
            UserTokenInfoVo userTokenInfoVo = (UserTokenInfoVo)obj;
            if (StringUtils.equals(userTokenInfoVo.getUserId(),userId)){
                //设置被踢出
                userTokenInfoVo.setKickOut(true);
                //获取并更新
                updateSetUserTokenInfoVo(userTokenInfoVo.getToken(),userTokenInfoVo);
            }
        }
    }

    /**
     * 根据租户id踢出相关会话
     *
     * @return
     */
    public void kickOutOnlineUserByTenantId(String tenantId) {
        if (StringUtils.isBlank(tenantId)){
            return;
        }
        for (Object obj : redisCache.multiGetByPrefix(buildTokenKey("*"))) {
            UserTokenInfoVo userTokenInfoVo = (UserTokenInfoVo)obj;
            if (StringUtils.equals(userTokenInfoVo.getTenantId(),tenantId)){
                //设置被踢出
                userTokenInfoVo.setKickOut(true);
                //获取并更新
                updateSetUserTokenInfoVo(userTokenInfoVo.getToken(),userTokenInfoVo);
            }
        }
    }



    /**
     * 构建token key
     * @param token
     * @return
     */
    private String buildTokenKey(String token){
        return redisCache.getKey(SystemConstants.LOGIN_TOKEN_KEY,token);
    }

}
