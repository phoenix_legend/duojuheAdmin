package com.duojuhe.cache;

import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.redis.RedisCache;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Slf4j
@Component
public class CaptchaCache {
    @Resource
    private RedisCache redisCache;


    /**
     * 存入图形验证码缓存
     *
     * @param imageId
     */
    public void putCaptchaCache(String imageId,String createText) {
            redisCache.set(crateCacheKey(imageId+createText),createText,300L);
    }

    /**
     * 【图形验证码】取出缓存
     *
     * @param captchaKey
     * @return
     */
    public String getCaptchaCacheByCaptchaKey(String captchaKey) {
        if (StringUtils.isBlank(captchaKey)) {
            return null;
        }
        return (String)redisCache.get(crateCacheKey(captchaKey));
    }

    /**
     * 【图形验证码】删除缓存
     *
     * @param captchaKey
     */
    public void invalidateCaptchaCacheByCaptchaKey(String captchaKey) {
        if (StringUtils.isNotBlank(captchaKey)) {
            redisCache.del(crateCacheKey(captchaKey));
        }
    }

    /**
     * 构建图片验证码key
     * @param captchaKey
     * @return
     */
    private String crateCacheKey(String captchaKey){
        return redisCache.getKey(SystemConstants.KAPTCHA_KEY,captchaKey);
    }


}
