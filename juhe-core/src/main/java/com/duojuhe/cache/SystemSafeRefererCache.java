package com.duojuhe.cache;

import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.coremodule.system.entity.SystemSafeReferer;
import com.duojuhe.redis.RedisCache;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
@Component
public class SystemSafeRefererCache {
    @Resource
    private RedisCache redisCache;

    /**
     * 根据code 查询系统安全对象
     *
     * @param key
     */
    public SystemSafeReferer getSystemSafeRefererByKey(String key) {
        if (StringUtils.isBlank(key)) {
            return null;
        }
        return redisCache.getCacheObject(crateCacheKey(key));
    }

    /**
     * 存入系统安全缓存
     *
     * @param systemSafeRefererList
     */
    public void putSystemSafeRefererCache(List<SystemSafeReferer> systemSafeRefererList) {
        for (SystemSafeReferer systemSafeReferer : systemSafeRefererList) {
            putSystemSafeRefererCache(systemSafeReferer);
        }
    }

    /**
     * 存入系统缓存
     *
     * @param safeReferer
     */
    public void putSystemSafeRefererCache(SystemSafeReferer safeReferer) {
        if (safeReferer != null) {
            String id = safeReferer.getId();
            redisCache.setCacheObject(crateCacheKey(id), safeReferer);
        }
    }


    /**
     * 根据id 删除系统安全对象
     *
     * @param id
     */
    public void deleteSystemSafeRefererById(String id) {
        if (StringUtils.isNotBlank(id)) {
            redisCache.del(crateCacheKey(id));
        }
    }

    /**
     * 清空系统安全缓存
     */
    public void clearSystemSafeRefererCache() {
        Collection<String> keys = redisCache.keysString(SystemConstants.SYS_REFERER_KEY + "*");
        redisCache.deleteObject(keys);
    }


    /**
     * 构建系统安全的key
     *
     * @param key
     * @return
     */
    private String crateCacheKey(String key) {
        return redisCache.getKey(SystemConstants.SYS_REFERER_KEY, key);
    }

    /**
     * 【type】根据类型取出所有允许名单集合
     *
     * @return
     */
    public List<String> getAllowRefererListCache() {
        List<String> allowRefererList = new ArrayList<>();
        for (Object obj : redisCache.multiGetByPrefix(crateCacheKey("*"))) {
            SystemSafeReferer systemSafeReferer = (SystemSafeReferer) obj;
            allowRefererList.add(systemSafeReferer.getAllowReferer().toLowerCase());
        }
        return allowRefererList;
    }
}
