package com.duojuhe.cache;

import com.duojuhe.common.constant.SingleStringConstant;
import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.common.enums.SystemEnum;
import com.duojuhe.redis.RedisCache;
import com.duojuhe.coremodule.system.entity.SystemDict;
import com.duojuhe.coremodule.system.pojo.dto.dict.SelectChildrenDictRes;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
public class SystemDictCache {
    @Resource
    private RedisCache redisCache;



    /**
     * 根据父级数据字典code 查询下级
     *
     * @param parentDictCodeList    code集合
     * @return 用户树状结构
     */
    @SuppressWarnings("unchecked")
    public Map<String,List<SelectChildrenDictRes>> queryChildrenSystemDictListByParentDictCode(List<String> parentDictCodeList) {
        Map<String,List<SelectChildrenDictRes>> map = new HashMap<>();
        if (parentDictCodeList==null||parentDictCodeList.isEmpty()){
            return map;
        }
        Map<String,SystemDict> systemDictMap = new HashMap<>();
        for (Object obj : redisCache.multiGetByPrefix(redisCache.getKey(SystemConstants.DICT_KEY,"*"))) {
            SystemDict systemDict = (SystemDict)obj;
            systemDictMap.put(systemDict.getDictCode(),systemDict);
        }
        for (String dictCode:parentDictCodeList){
            SystemDict systemDict = systemDictMap.get(dictCode);
            if (systemDict==null){
                continue;
            }
            // 进行拆解封装
            List<SelectChildrenDictRes> list = systemDictMap.values().stream().filter(e -> SystemEnum.STATUS.NORMAL.getKey().equals(e.getStatusCode()) &&
                    e.getParentId().equals(systemDict.getDictId())).map(SelectChildrenDictRes::new).sorted(Comparator.comparing(SelectChildrenDictRes::getSort)).collect(Collectors.toList());
            map.put(dictCode,list);
        }
        return map;
    }


    /**
     * 根据code 查询数据字典名称
     *
     * @param dictCodes
     */
    public String getSystemDictNameByDictCodes(String dictCodes) {
        if (StringUtils.isBlank(dictCodes)) {
            return "";
        }
        List<String> list = new ArrayList<>();
        String[] arr = dictCodes.split(SingleStringConstant.COMMA);
        for (String dictCode:arr){
            SystemDict systemDict = redisCache.getCacheObject(crateCacheKey(dictCode));
            if (systemDict==null){
                continue;
            }
            list.add(systemDict.getDictName());
        }
        return String.join(SingleStringConstant.COMMA, list);
    }

    /**
     * 根据code 查询数据字典对象
     *
     * @param dictCode
     */
    public SystemDict getSystemDictByDictCode(String dictCode) {
        if (StringUtils.isBlank(dictCode)) {
            return new SystemDict();
        }
        SystemDict systemDict = redisCache.getCacheObject(crateCacheKey(dictCode));
        if (systemDict==null){
            return new SystemDict();
        }
        return systemDict;
    }


    /**
     * 根据code 删除数据字典对象
     *
     * @param dictCode
     */
    public void deleteSystemDictByDictCode(String dictCode) {
        if (StringUtils.isNotBlank(dictCode)) {
            redisCache.del(crateCacheKey(dictCode));
        }
    }



    /**
     * 清空数据字典缓存
     *
     */
    public void clearSystemDictCache() {
        Collection<String> keys = redisCache.keysString(SystemConstants.DICT_KEY + "*");
        redisCache.deleteObject(keys);
    }

    /**
     * 存入数据字典存储缓存
     *
     * @param dictList
     */
    public void putSystemDictCache(List<SystemDict> dictList) {
        for (SystemDict dict : dictList) {
            putSystemDictCache(dict);
        }
    }

    /**
     * 存入数据字典存储缓存
     *
     * @param dict
     */
    public void putSystemDictCache(SystemDict dict) {
        if (dict != null) {
            redisCache.setCacheObject(crateCacheKey(dict.getDictCode()),dict);
        }
    }

    /**
     *
     * @param dictCode
     * @return
     */
    private String crateCacheKey(String dictCode){
        return redisCache.getKey(SystemConstants.DICT_KEY,dictCode);
    }
}
