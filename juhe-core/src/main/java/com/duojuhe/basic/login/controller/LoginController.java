package com.duojuhe.basic.login.controller;

import com.duojuhe.basic.login.dto.admin.AdminLoginReq;
import com.duojuhe.basic.login.dto.admin.AdminLoginRes;
import com.duojuhe.basic.login.service.LoginService;
import com.duojuhe.common.result.ServiceResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 用户登录操作controller
 *
 * @date 2018/5/7.
 */
@RestController
@Api(tags={"【用户登录】用户登录相关接口"})
@Slf4j
public class LoginController {

    @Resource
    private LoginService loginService;



    @SuppressWarnings("unchecked")
    @ApiOperation(value = "【后台用户登录】系统后台用户登录接口")
    @PostMapping(value = "/adminLogin")
    public ServiceResult<AdminLoginRes> adminLogin(@Valid @RequestBody @ApiParam(value = "入参类") AdminLoginReq req) {
        return loginService.adminLogin(req);
    }

}
