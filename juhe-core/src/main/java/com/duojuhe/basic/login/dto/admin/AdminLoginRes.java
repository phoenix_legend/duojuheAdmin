package com.duojuhe.basic.login.dto.admin;

import com.duojuhe.common.bean.BaseBean;
import com.duojuhe.common.bean.UserTokenInfoVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminLoginRes extends BaseBean {
    @ApiModelProperty(value = "用户id", example = "1")
    private String userId;

    @ApiModelProperty(value = "辅助签名密钥", example = "1")
    private String signKey;

    @ApiModelProperty(value = "token", example = "1")
    private String token;

    public AdminLoginRes(UserTokenInfoVo userTokenInfoVo){
        this.userId = userTokenInfoVo.getUserId();
        this.signKey = userTokenInfoVo.getSignKey();
        this.token = userTokenInfoVo.getToken();
    }
}
