package com.duojuhe.basic.login.dto.admin;

import com.duojuhe.basic.captcha.dto.CheckCaptchaReq;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AdminLoginReq extends CheckCaptchaReq {
    @ApiModelProperty(value = "登录名", example = "3",required=true)
    @NotBlank(message = "登录名不能为空")
    private String loginName;

    @ApiModelProperty(value = "登录密码", example = "3" ,required=true)
    @NotBlank(message = "登录密码不能为空")
    private String password;
}
