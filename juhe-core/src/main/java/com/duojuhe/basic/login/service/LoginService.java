package com.duojuhe.basic.login.service;

import com.duojuhe.basic.login.dto.admin.AdminLoginReq;
import com.duojuhe.common.result.ServiceResult;


public interface LoginService {

    /**
     * admin管理用户登录
     * @param req
     * @return
     */
    ServiceResult adminLogin(AdminLoginReq req);
}
