package com.duojuhe.basic.captcha.service;

import com.duojuhe.basic.captcha.dto.CaptchaRes;
import com.duojuhe.common.result.ServiceResult;


public interface KaptchaService {
    /**
     * 获得图形验证码
     *
     * @return
     * @throws Exception
     */
     ServiceResult<CaptchaRes> getCaptchaImageCode() throws Exception;

    /**
     * 校验验证码
     * @param imageId
     * @param imageCode
     */
     void verifyCaptchaCode(String imageId, String imageCode);
}
