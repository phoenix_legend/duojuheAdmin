package com.duojuhe.basic.captcha.dto;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ApiModel(value = "Captcha 图形验证码响应对象")
public class CaptchaRes extends BaseBean {
    @ApiModelProperty(value = "图片验证码唯一标识,提交验证码的时候需要一并提交过来", example = "1")
    private String imageId;

    @ApiModelProperty(value = "图片验证码,base64格式", example = "1")
    private String imageCode;
}
