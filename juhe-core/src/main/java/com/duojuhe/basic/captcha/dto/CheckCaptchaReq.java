package com.duojuhe.basic.captcha.dto;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CheckCaptchaReq extends BaseBean {
    @ApiModelProperty(value = "图形验证码唯一标识", example = "1",required=true)
    @NotBlank(message = "图形验证码唯一标识不能为空")
    @Length(max = 32, message = "图形验证码唯一标识不得超过32位字符")
    private String imageId;


    @ApiModelProperty(value = "图形验证码", example = "3",required=true)
    @NotBlank(message = "图形验证码不能为空")
    @Length(max = 6, message = "图形验证码不得超过6位字符")
    private String imageCode;
}
