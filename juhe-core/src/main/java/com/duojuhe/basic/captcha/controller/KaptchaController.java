package com.duojuhe.basic.captcha.controller;

import com.duojuhe.basic.captcha.dto.CaptchaRes;
import com.duojuhe.basic.captcha.service.KaptchaService;
import com.duojuhe.common.result.ServiceResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/captcha/")
@Api(tags = {"图形验证码获取相关接口"})
@Slf4j
public class KaptchaController {
   @Resource
   private KaptchaService kaptchaService;

    @SuppressWarnings("unchecked")
    @ApiOperation(value = "无参获取图形验证码，获取的验证码5分钟内有效")
    @PostMapping(value = "getCaptchaImageCode")
    public ServiceResult<CaptchaRes> getCaptchaImageCode() throws Exception {
        return kaptchaService.getCaptchaImageCode();
    }

}
