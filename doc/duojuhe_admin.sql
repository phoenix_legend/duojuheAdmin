/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50720
Source Host           : localhost:3306
Source Database       : duojuhe_admin

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2022-12-02 17:17:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for chat_emoticon_category
-- ----------------------------
DROP TABLE IF EXISTS `chat_emoticon_category`;
CREATE TABLE `chat_emoticon_category` (
  `category_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `category_name` varchar(50) NOT NULL DEFAULT '' COMMENT '类别名称',
  `category_icon` varchar(255) DEFAULT '' COMMENT '类别图标',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='聊天表情类别';

-- ----------------------------
-- Records of chat_emoticon_category
-- ----------------------------

-- ----------------------------
-- Table structure for chat_emoticon_item
-- ----------------------------
DROP TABLE IF EXISTS `chat_emoticon_item`;
CREATE TABLE `chat_emoticon_item` (
  `item_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `category_id` varchar(32) DEFAULT '' COMMENT '表情类别id',
  `emoticon_url` varchar(255) NOT NULL DEFAULT '' COMMENT '表情地址',
  `file_suffix` varchar(50) DEFAULT '' COMMENT '文件后缀',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT '' COMMENT '表情说明',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户id',
  `is_collect` int(1) DEFAULT '0' COMMENT '是否用户收藏0否1是',
  `file_size` bigint(8) DEFAULT '0' COMMENT '文件大小（单位字节）',
  `original_name` varchar(1000) DEFAULT '' COMMENT '原文件名',
  `file_type` int(1) DEFAULT '0' COMMENT '文件类型[1:图片;2:视频;3:文件]',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统表情图标项';

-- ----------------------------
-- Records of chat_emoticon_item
-- ----------------------------

-- ----------------------------
-- Table structure for chat_group
-- ----------------------------
DROP TABLE IF EXISTS `chat_group`;
CREATE TABLE `chat_group` (
  `group_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `group_name` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '群名称',
  `group_introduce` varchar(255) DEFAULT '' COMMENT '群介绍',
  `group_avatar` varchar(255) DEFAULT '' COMMENT '群头像',
  `max_people_number` int(5) DEFAULT '100' COMMENT '最大人数限制',
  `is_overt` int(1) DEFAULT '0' COMMENT '是否公开可见[0:否;1:是;]',
  `is_mute` int(1) DEFAULT '0' COMMENT '是否全员禁言 [0:否;1:是;]，提示:不包含群主或管理员',
  `is_dismiss` int(1) DEFAULT '0' COMMENT '是否已解散[0:否;1:是;]',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `dismissed_time` datetime DEFAULT NULL COMMENT '解散时间',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户ID',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='聊天群组表';

-- ----------------------------
-- Records of chat_group
-- ----------------------------

-- ----------------------------
-- Table structure for chat_group_notice
-- ----------------------------
DROP TABLE IF EXISTS `chat_group_notice`;
CREATE TABLE `chat_group_notice` (
  `notice_id` varchar(32) NOT NULL DEFAULT '' COMMENT '群通知id',
  `group_id` varchar(32) NOT NULL DEFAULT '' COMMENT '群id',
  `create_user_id` varchar(32) NOT NULL DEFAULT '' COMMENT '创建者用户ID',
  `notice_title` varchar(255) DEFAULT '' COMMENT '通知标题',
  `content` mediumtext COMMENT '通知内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='群通知公告';

-- ----------------------------
-- Records of chat_group_notice
-- ----------------------------

-- ----------------------------
-- Table structure for chat_group_user
-- ----------------------------
DROP TABLE IF EXISTS `chat_group_user`;
CREATE TABLE `chat_group_user` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `group_id` varchar(32) NOT NULL DEFAULT '' COMMENT '群组id',
  `user_id` varchar(32) NOT NULL DEFAULT '' COMMENT '用户id',
  `leader` int(1) NOT NULL DEFAULT '0' COMMENT '成员属性[0:普通成员;1:管理员;2:群主;]',
  `is_mute` int(1) NOT NULL DEFAULT '0' COMMENT '是否禁言[0:否;1:是;]',
  `is_quit` int(1) NOT NULL DEFAULT '0' COMMENT '是否退群[0:否;1:是;]',
  `visit_card` varchar(255) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '用户组名片',
  `join_group_time` datetime DEFAULT NULL COMMENT '入组时间',
  `quit_group_time` datetime DEFAULT NULL COMMENT '退组时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_user_id_group_id` (`group_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='聊天群组成员表';

-- ----------------------------
-- Records of chat_group_user
-- ----------------------------

-- ----------------------------
-- Table structure for chat_talk_last_message
-- ----------------------------
DROP TABLE IF EXISTS `chat_talk_last_message`;
CREATE TABLE `chat_talk_last_message` (
  `message_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键id',
  `talk_type` int(1) DEFAULT '0' COMMENT '对话类型[1:私信;2:群聊;]',
  `msg_type` int(2) DEFAULT '0' COMMENT '消息类型[1:文本消息;2:文件消息;3:会话消息;4:代码消息;5:投票消息;6:群公告;7:好友申请;8:登录通知;9:入群消息/退群消息;]',
  `user_id` varchar(32) DEFAULT '' COMMENT '发送者ID（-1:代表系统消息 >-1: 用户ID）',
  `receiver_id` varchar(32) DEFAULT '' COMMENT '接收者ID（用户ID 或 群ID）',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建者id',
  `content` varchar(255) DEFAULT '' COMMENT '消息文本 {@realName@}',
  `record_id` varchar(32) DEFAULT '' COMMENT '消息记录id',
  `release_time` datetime(6) DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='最后一条消息记录';

-- ----------------------------
-- Records of chat_talk_last_message
-- ----------------------------

-- ----------------------------
-- Table structure for chat_talk_record
-- ----------------------------
DROP TABLE IF EXISTS `chat_talk_record`;
CREATE TABLE `chat_talk_record` (
  `record_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `talk_type` int(1) DEFAULT '0' COMMENT '对话类型[1:私信;2:群聊;]',
  `msg_type` int(2) DEFAULT '0' COMMENT '消息类型[1:文本消息;2:文件消息;3:会话消息;4:代码消息;5:投票消息;6:群公告;7:好友申请;8:登录通知;9:入群消息/退群消息;]',
  `user_id` varchar(32) DEFAULT '' COMMENT '发送者ID（-1:代表系统消息 >-1: 用户ID）',
  `receiver_id` varchar(32) DEFAULT '' COMMENT '接收者ID（用户ID 或 群ID）',
  `is_revoke` int(1) DEFAULT '0' COMMENT '是否撤回消息[0:否;1:是]',
  `is_mark` int(1) DEFAULT '0' COMMENT '是否重要消息[0:否;1:是;]',
  `is_read` int(1) DEFAULT '0' COMMENT '是否已读[0:否;1:是;]',
  `is_delete` int(1) DEFAULT '0' COMMENT '是否删除[0:否;1:是;]',
  `quote_id` varchar(32) DEFAULT '' COMMENT '引用消息ID',
  `content` mediumtext CHARACTER SET utf8mb4 COMMENT '文本消息 {@realName@}',
  `warn_users` varchar(1000) DEFAULT '' COMMENT '@好友 、 多个用英文逗号 “,” 拼接 (0:代表所有人)',
  `release_time` timestamp(6) NULL DEFAULT NULL COMMENT '消息创建时间含毫秒',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建者id',
  `group_name` varchar(255) DEFAULT '' COMMENT '群名称',
  `group_avatar` varchar(255) DEFAULT '' COMMENT '群头像',
  `real_name` varchar(32) DEFAULT '' COMMENT '用户姓名',
  `head_portrait` varchar(255) DEFAULT '' COMMENT '用户头像',
  `file_type` int(1) DEFAULT '0' COMMENT '文件类型[1:图片;2:视频;3:文件]',
  PRIMARY KEY (`record_id`),
  KEY `receiver_id` (`receiver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会话聊天记录';

-- ----------------------------
-- Records of chat_talk_record
-- ----------------------------

-- ----------------------------
-- Table structure for chat_talk_record_code
-- ----------------------------
DROP TABLE IF EXISTS `chat_talk_record_code`;
CREATE TABLE `chat_talk_record_code` (
  `code_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `record_id` varchar(32) DEFAULT '' COMMENT '聊天记录id',
  `user_id` varchar(32) DEFAULT '' COMMENT '用户id',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `code_lang` varchar(50) DEFAULT '' COMMENT '代码片段类型(如：php,java,python)',
  `code_content` mediumtext CHARACTER SET utf8mb4 COMMENT '代码片段内容',
  `is_delete` int(1) DEFAULT '0' COMMENT '代码片段是否已删除[0:否;1:已删除]',
  PRIMARY KEY (`code_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户聊天记录_代码块消息表';

-- ----------------------------
-- Records of chat_talk_record_code
-- ----------------------------

-- ----------------------------
-- Table structure for chat_talk_record_delete
-- ----------------------------
DROP TABLE IF EXISTS `chat_talk_record_delete`;
CREATE TABLE `chat_talk_record_delete` (
  `delete_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `record_id` varchar(32) DEFAULT '' COMMENT '聊天记录ID',
  `user_id` varchar(32) DEFAULT '' COMMENT '用户id',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`delete_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户聊天记录_删除记录表';

-- ----------------------------
-- Records of chat_talk_record_delete
-- ----------------------------

-- ----------------------------
-- Table structure for chat_talk_record_file
-- ----------------------------
DROP TABLE IF EXISTS `chat_talk_record_file`;
CREATE TABLE `chat_talk_record_file` (
  `file_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `record_id` varchar(32) DEFAULT '' COMMENT '消息记录id',
  `user_id` varchar(32) DEFAULT '' COMMENT '用户id',
  `file_type` int(1) DEFAULT '0' COMMENT '文件类型[1:图片;2:视频;3:文件]',
  `original_name` varchar(1000) DEFAULT '' COMMENT '原文件名',
  `file_suffix` varchar(100) DEFAULT '' COMMENT '文件后缀',
  `is_delete` int(1) DEFAULT '0' COMMENT '文件是否已删除[0:否;1:已删除]',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建者id',
  `file_url` varchar(255) DEFAULT '' COMMENT '文件地址',
  `file_size` bigint(8) DEFAULT '0' COMMENT '文件大小（单位字节）',
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户聊天记录_文件消息表';

-- ----------------------------
-- Records of chat_talk_record_file
-- ----------------------------

-- ----------------------------
-- Table structure for chat_talk_record_forward
-- ----------------------------
DROP TABLE IF EXISTS `chat_talk_record_forward`;
CREATE TABLE `chat_talk_record_forward` (
  `forward_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `record_id` varchar(32) DEFAULT '' COMMENT '记录id',
  `user_id` varchar(32) DEFAULT '' COMMENT '用户id',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户id',
  `snapshot` mediumtext COMMENT '记录快照',
  `forward_record_ids` varchar(2000) DEFAULT '' COMMENT '转发的聊天记录ID，多个用'',''分割',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`forward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户聊天记录_合并转发信息表';

-- ----------------------------
-- Records of chat_talk_record_forward
-- ----------------------------

-- ----------------------------
-- Table structure for chat_talk_record_invite
-- ----------------------------
DROP TABLE IF EXISTS `chat_talk_record_invite`;
CREATE TABLE `chat_talk_record_invite` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `record_id` varchar(32) DEFAULT '' COMMENT '聊天记录id',
  `invite_type` int(1) DEFAULT '0' COMMENT '通知类型[1:入群通知;2:自动退群;3:管理员踢群]',
  `operate_user_id` varchar(32) DEFAULT '' COMMENT '操作人的用户ID(邀请人)',
  `user_ids` varchar(1000) DEFAULT '' COMMENT '用户ID，多个用 , 分割',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户id',
  `operate_user_snapshot` varchar(255) DEFAULT '' COMMENT '操作人详情快照',
  `user_snapshot` varchar(1000) DEFAULT '' COMMENT '被操作人快照',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户聊天记录_入群或退群消息表';

-- ----------------------------
-- Records of chat_talk_record_invite
-- ----------------------------

-- ----------------------------
-- Table structure for chat_talk_record_vote
-- ----------------------------
DROP TABLE IF EXISTS `chat_talk_record_vote`;
CREATE TABLE `chat_talk_record_vote` (
  `vote_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `record_id` varchar(32) DEFAULT '' COMMENT '聊天记录id',
  `user_id` varchar(32) DEFAULT '' COMMENT '用户id',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `vote_title` varchar(255) DEFAULT '' COMMENT '投票标题',
  `answer_mode` int(1) DEFAULT '0' COMMENT '答题模式[0:单选;1:多选;]',
  `answer_option` varchar(255) DEFAULT '' COMMENT '选项逗号分割',
  `answer_option_content` text COMMENT '答题选项',
  `answer_num` int(5) DEFAULT '0' COMMENT '应答人数',
  `answered_num` int(5) DEFAULT '0' COMMENT '已答人数',
  `vote_status` int(1) DEFAULT '0' COMMENT '投票状态[0:投票中;1:已完成;]',
  PRIMARY KEY (`vote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='聊天对话记录（投票消息表）';

-- ----------------------------
-- Records of chat_talk_record_vote
-- ----------------------------

-- ----------------------------
-- Table structure for chat_talk_record_vote_answer
-- ----------------------------
DROP TABLE IF EXISTS `chat_talk_record_vote_answer`;
CREATE TABLE `chat_talk_record_vote_answer` (
  `answer_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `vote_id` varchar(32) DEFAULT '' COMMENT '投票id',
  `user_id` varchar(32) DEFAULT '' COMMENT '用户id',
  `answer_option` varchar(255) DEFAULT '' COMMENT '投票选项[A、B、C 、D、E、F]',
  `create_time` datetime DEFAULT NULL COMMENT '答题时间',
  PRIMARY KEY (`answer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='聊天对话记录（投票消息统计表）';

-- ----------------------------
-- Records of chat_talk_record_vote_answer
-- ----------------------------

-- ----------------------------
-- Table structure for chat_users_chat_list
-- ----------------------------
DROP TABLE IF EXISTS `chat_users_chat_list`;
CREATE TABLE `chat_users_chat_list` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `talk_type` int(1) DEFAULT '0' COMMENT '聊天类型[1:私信;2:群聊;0:游客]',
  `user_id` varchar(32) DEFAULT '' COMMENT '用户id',
  `receiver_id` varchar(32) DEFAULT '' COMMENT '接收者ID（用户ID 或 群ID）',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `is_disturb` int(1) DEFAULT '0' COMMENT '消息免打扰[0:否;1:是;]',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `is_delete` int(1) DEFAULT '0' COMMENT '是否删除[0:否;1:是;]',
  `is_top` int(1) DEFAULT '0' COMMENT '是否置顶[0:否;1:是]',
  `is_robot` int(1) DEFAULT '0' COMMENT '是否机器人[0:否;1:是;]',
  `unread_num` int(11) DEFAULT '0' COMMENT '未读消息数量',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `last_message` varchar(255) DEFAULT '' COMMENT '最后消息内容',
  `last_release_time` datetime(6) DEFAULT NULL COMMENT '最后发布时间',
  `last_record_id` varchar(32) DEFAULT NULL COMMENT '最后消息记录id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_user_id_receiver_id` (`user_id`,`receiver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户聊天列表';

-- ----------------------------
-- Records of chat_users_chat_list
-- ----------------------------

-- ----------------------------
-- Table structure for chat_users_friends
-- ----------------------------
DROP TABLE IF EXISTS `chat_users_friends`;
CREATE TABLE `chat_users_friends` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键当前用户id和好友id的MD5值',
  `user_id` varchar(32) NOT NULL DEFAULT '' COMMENT '用户id',
  `friend_id` varchar(32) NOT NULL DEFAULT '' COMMENT '朋友id',
  `friend_remark` varchar(255) DEFAULT '' COMMENT '朋友备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间，成为好友时间',
  `unfriend_time` datetime DEFAULT NULL COMMENT '解除好友时间',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户id',
  `direction_code` varchar(50) DEFAULT '' COMMENT '邀请方向 主动 被动',
  `is_friend` int(1) DEFAULT '1' COMMENT '是否好友状态[0:否;1:是;]',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_user_id_friend_id` (`user_id`,`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户好友关系表';

-- ----------------------------
-- Records of chat_users_friends
-- ----------------------------

-- ----------------------------
-- Table structure for chat_users_friends_apply
-- ----------------------------
DROP TABLE IF EXISTS `chat_users_friends_apply`;
CREATE TABLE `chat_users_friends_apply` (
  `apply_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `user_id` varchar(32) NOT NULL DEFAULT '' COMMENT '申请人ID',
  `friend_id` varchar(32) NOT NULL DEFAULT '' COMMENT '被申请人id',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `apply_status` int(1) DEFAULT '0' COMMENT '申请状态[0:申请中;1:是同意，2拒绝;]',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '申请创建时间',
  `handle_time` datetime DEFAULT NULL COMMENT '处理时间',
  `apply_description` varchar(255) DEFAULT '' COMMENT '申请人备注信息',
  PRIMARY KEY (`apply_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户添加好友申请表';

-- ----------------------------
-- Records of chat_users_friends_apply
-- ----------------------------

-- ----------------------------
-- Table structure for chat_user_emoticon
-- ----------------------------
DROP TABLE IF EXISTS `chat_user_emoticon`;
CREATE TABLE `chat_user_emoticon` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `user_id` varchar(32) NOT NULL DEFAULT '' COMMENT '用户id',
  `category_id` varchar(32) NOT NULL DEFAULT '' COMMENT '系统表情包分类id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_user_id_category_id` (`user_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户收藏表情包';

-- ----------------------------
-- Records of chat_user_emoticon
-- ----------------------------

-- ----------------------------
-- Table structure for file_category
-- ----------------------------
DROP TABLE IF EXISTS `file_category`;
CREATE TABLE `file_category` (
  `category_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `category_name` varchar(30) NOT NULL DEFAULT '' COMMENT '分类名称',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '所属租户id',
  `parent_id` varchar(32) DEFAULT '' COMMENT '父级id',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件分类表';

-- ----------------------------
-- Records of file_category
-- ----------------------------

-- ----------------------------
-- Table structure for file_split_upload
-- ----------------------------
DROP TABLE IF EXISTS `file_split_upload`;
CREATE TABLE `file_split_upload` (
  `upload_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键id',
  `parent_id` varchar(32) DEFAULT '' COMMENT '-1表示初始大文件',
  `hash_name` varchar(32) DEFAULT '' COMMENT '临时文件hash名',
  `user_id` varchar(32) DEFAULT '' COMMENT '上传用户id',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `original_name` varchar(255) DEFAULT '' COMMENT '原文件名',
  `split_index` int(8) DEFAULT '0' COMMENT '当前索引块',
  `split_num` int(8) DEFAULT '0' COMMENT '总共索引块',
  `file_suffix` varchar(100) DEFAULT '' COMMENT '文件后缀名',
  `file_size` bigint(18) DEFAULT '0' COMMENT '文件大小',
  `create_milli_second` bigint(18) DEFAULT '0' COMMENT '创建毫秒时间',
  `file_content` longblob COMMENT '文件内容',
  `split_size` double(20,2) DEFAULT '0.00' COMMENT '文件分割大小规则',
  PRIMARY KEY (`upload_id`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统文件分割上传分片表';

-- ----------------------------
-- Records of file_split_upload
-- ----------------------------

-- ----------------------------
-- Table structure for file_upload
-- ----------------------------
DROP TABLE IF EXISTS `file_upload`;
CREATE TABLE `file_upload` (
  `file_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `file_type_code` varchar(50) DEFAULT '' COMMENT '文件类型，取数据字典',
  `file_name` varchar(500) DEFAULT '' COMMENT '文件名称',
  `file_url` varchar(255) DEFAULT '' COMMENT '文件地址',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `relation_id` varchar(32) DEFAULT '' COMMENT '关联归属id',
  `relation_id_source` varchar(50) DEFAULT '' COMMENT '关联id来源表',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户ID',
  `file_category_id` varchar(32) DEFAULT '' COMMENT '文件分类id',
  `file_suffix` varchar(100) DEFAULT '' COMMENT '文件后缀名',
  `file_size` bigint(18) DEFAULT '0' COMMENT '文件大小',
  `original_name` varchar(255) DEFAULT '' COMMENT '原文件名称',
  `file_relative_path` varchar(255) DEFAULT '' COMMENT '文件相对路径',
  `file_storage_path` varchar(255) DEFAULT '' COMMENT '文件存储路径',
  PRIMARY KEY (`file_id`),
  KEY `idx_relation_id` (`relation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件附件管理';

-- ----------------------------
-- Records of file_upload
-- ----------------------------

-- ----------------------------
-- Table structure for form_category
-- ----------------------------
DROP TABLE IF EXISTS `form_category`;
CREATE TABLE `form_category` (
  `category_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `category_name` varchar(50) NOT NULL DEFAULT '' COMMENT '分类名称',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '创建租户id',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单分类';

-- ----------------------------
-- Records of form_category
-- ----------------------------
INSERT INTO `form_category` VALUES ('1', '疫情防控', '1', '2022-03-21 14:51:59', '1', '2022-03-21 14:51:57', '9', '1', '1');
INSERT INTO `form_category` VALUES ('2', '信息登记', '1', '2022-03-21 14:52:23', '1', '2022-03-21 14:52:27', '8', '1', '1');
INSERT INTO `form_category` VALUES ('3', '报名信息', '1', '2022-03-21 14:53:11', '1', '2022-03-21 14:53:13', '7', '1', '1');
INSERT INTO `form_category` VALUES ('4', '数据采集', '1', '2022-03-21 14:55:48', '1', '2022-03-21 14:55:55', '6', '1', '1');
INSERT INTO `form_category` VALUES ('5', '意见反馈', '1', '2022-03-21 14:55:50', '1', '2022-03-21 14:55:57', '5', '1', '1');
INSERT INTO `form_category` VALUES ('9', '其他', '1', '2022-03-21 14:52:39', '1', '2022-03-21 14:52:43', '1', '1', '1');

-- ----------------------------
-- Table structure for form_project
-- ----------------------------
DROP TABLE IF EXISTS `form_project`;
CREATE TABLE `form_project` (
  `project_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `project_key` varchar(32) NOT NULL DEFAULT '' COMMENT '项目唯一标识',
  `project_name` varchar(255) DEFAULT '' COMMENT '项目名称',
  `project_type_code` varchar(50) DEFAULT '' COMMENT '项目类型，取数据字典，问卷项目',
  `project_module_code` varchar(50) DEFAULT '' COMMENT '表单项目所属模块，问卷调查，流程表单',
  `cover_img` longtext COMMENT '封面图片base64格式',
  `use_count` int(5) DEFAULT '0' COMMENT '使用次数',
  `category_id` varchar(32) DEFAULT '' COMMENT '分类id',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户id',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `remark` varchar(255) DEFAULT '' COMMENT '备注说明',
  `status_code` varchar(50) DEFAULT '' COMMENT '状态',
  `description` mediumtext COMMENT '描述',
  `max_form_item_id` bigint(18) DEFAULT '0' COMMENT '最大表单项id',
  `max_form_item_sort` bigint(18) DEFAULT '0' COMMENT '最大表单项排序',
  `hits` bigint(18) DEFAULT '0' COMMENT '浏览点击次数',
  PRIMARY KEY (`project_id`),
  UNIQUE KEY `uni_project_key` (`project_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单项目';

-- ----------------------------
-- Records of form_project
-- ----------------------------

-- ----------------------------
-- Table structure for form_project_item
-- ----------------------------
DROP TABLE IF EXISTS `form_project_item`;
CREATE TABLE `form_project_item` (
  `item_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `project_id` varchar(32) NOT NULL DEFAULT '' COMMENT '项目id',
  `project_key` varchar(32) NOT NULL DEFAULT '' COMMENT '项目key',
  `form_item_id` varchar(100) DEFAULT '' COMMENT '表单项Id',
  `form_item_type` varchar(20) DEFAULT '' COMMENT '表单项类型',
  `form_item_label` varchar(100) DEFAULT '' COMMENT '表单项标题',
  `form_item_field` varchar(255) DEFAULT '' COMMENT '表单项目字段标识',
  `form_item_show_label` int(1) DEFAULT NULL COMMENT '是否显示标签',
  `form_item_default_value` longtext COMMENT '表单项默认值',
  `form_item_required` int(1) DEFAULT NULL COMMENT '表单项是否必填',
  `form_item_placeholder` varchar(255) DEFAULT '' COMMENT '输入型提示文字',
  `form_item_span` int(5) DEFAULT '0' COMMENT '栅格宽度',
  `form_item_expand` longtext COMMENT '扩展字段 表单项独有字段',
  `form_item_regex` longtext COMMENT '正则表达式',
  `form_item_show_regex` int(1) DEFAULT NULL COMMENT '是否显示正则表达式',
  `form_item_display_type` int(2) DEFAULT NULL COMMENT '展示类型组件',
  `form_item_config` longtext COMMENT '表单项原始配置json',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `sort` bigint(18) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单项目子项';

-- ----------------------------
-- Records of form_project_item
-- ----------------------------

-- ----------------------------
-- Table structure for form_project_logic
-- ----------------------------
DROP TABLE IF EXISTS `form_project_logic`;
CREATE TABLE `form_project_logic` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `project_id` varchar(32) DEFAULT '' COMMENT '项目id',
  `project_key` varchar(32) DEFAULT '' COMMENT '项目key',
  `item_id` varchar(32) DEFAULT '' COMMENT '子项目id',
  `form_item_id` varchar(32) DEFAULT '' COMMENT '子项目表单id',
  `form_item_type` varchar(20) DEFAULT '' COMMENT '表单项类型',
  `expression` int(1) DEFAULT '0' COMMENT '条件选项1全部2任意',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `condition_list` longtext COMMENT '条件list集合',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单项目逻辑配置表';

-- ----------------------------
-- Records of form_project_logic
-- ----------------------------

-- ----------------------------
-- Table structure for form_project_result
-- ----------------------------
DROP TABLE IF EXISTS `form_project_result`;
CREATE TABLE `form_project_result` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `serial_number` varchar(32) DEFAULT '' COMMENT '提交编号',
  `project_id` varchar(32) DEFAULT '' COMMENT '项目id',
  `project_key` varchar(32) DEFAULT '' COMMENT '项目唯一标识',
  `original_data` longtext COMMENT '填写结果原始数据',
  `process_data` longtext COMMENT '填写结果处理后的数据',
  `submit_ua` longtext COMMENT '提交用户客户端信息',
  `submit_os` varchar(255) DEFAULT '' COMMENT '提交用户操作系统',
  `submit_browser` varchar(255) DEFAULT '' COMMENT '提交用户浏览器',
  `submit_ip` varchar(100) DEFAULT '' COMMENT '提交ip',
  `submit_user_id` varchar(32) DEFAULT '' COMMENT '提交用户id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户id',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '创建租户id',
  `complete_time` bigint(18) DEFAULT NULL COMMENT '完成时间 毫秒',
  `form_content_config` longtext COMMENT '该记录对应的表单内容',
  `form_logic_config` longtext COMMENT '该记录对应表单的逻辑配置',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新用户id',
  `submit_source` varchar(50) DEFAULT '' COMMENT '提交来源，取数据字典',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_serial_number` (`serial_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单项目提交结果表';

-- ----------------------------
-- Records of form_project_result
-- ----------------------------

-- ----------------------------
-- Table structure for form_project_setting
-- ----------------------------
DROP TABLE IF EXISTS `form_project_setting`;
CREATE TABLE `form_project_setting` (
  `project_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `project_key` varchar(32) NOT NULL DEFAULT '' COMMENT '项目唯一标识',
  `submit_prompt_img` longtext COMMENT '提交提示图片',
  `submit_prompt_text` varchar(100) DEFAULT '' COMMENT '提交提示文字',
  `submit_jump_url` varchar(255) DEFAULT '' COMMENT '提交后跳转地址',
  `result_public` int(1) DEFAULT '0' COMMENT '是否公开提交结果',
  `limit_write` int(1) DEFAULT '0' COMMENT '是否限制填写次数',
  `everyone_write_num` int(5) DEFAULT '0' COMMENT '每人允许填写次数',
  `limit_write_num_mode` varchar(50) DEFAULT '' COMMENT '限制填写次数方式每天，总共',
  `share_img` longtext COMMENT '分享图片',
  `share_title` varchar(255) DEFAULT '' COMMENT '分享标题',
  `share_desc` varchar(255) DEFAULT '' COMMENT '分享描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `show_timed_collection` int(1) DEFAULT '0' COMMENT '显示定时收集',
  `timed_collection_begin_time` datetime DEFAULT NULL COMMENT '定时收集开始时间',
  `timed_collection_end_time` datetime DEFAULT NULL COMMENT '定时收集结束时间',
  `timed_not_enabled_prompt_text` varchar(255) DEFAULT '' COMMENT '定时未启动提示文字',
  `timed_deactivate_prompt_text` varchar(255) DEFAULT '' COMMENT '定时停用会提示文字',
  `show_quantitative` int(1) DEFAULT '0' COMMENT '显示定量收集',
  `quantitative_total_quantity` int(5) DEFAULT '0' COMMENT '定量收集总数量',
  `quantitative_end_prompt_text` varchar(255) DEFAULT '' COMMENT '定量收集完成提示文字',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单项目设置表';

-- ----------------------------
-- Records of form_project_setting
-- ----------------------------

-- ----------------------------
-- Table structure for form_project_style
-- ----------------------------
DROP TABLE IF EXISTS `form_project_style`;
CREATE TABLE `form_project_style` (
  `project_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键id',
  `project_key` varchar(32) DEFAULT '' COMMENT '项目唯一标识',
  `logo_image` longtext COMMENT 'logo图片',
  `head_image` longtext COMMENT '顶部图片',
  `logo_position` varchar(50) DEFAULT '' COMMENT 'logo位置',
  `background_color` varchar(255) DEFAULT '' COMMENT '背景颜色',
  `background_img` varchar(255) DEFAULT '' COMMENT '背景图片',
  `show_title` int(1) DEFAULT '0' COMMENT '是否显示标题',
  `button_color` varchar(50) DEFAULT '' COMMENT '按钮颜色',
  `submit_button_text` varchar(20) DEFAULT '' COMMENT '提交按钮文字',
  `show_describe` int(1) DEFAULT '0' COMMENT '是否显示描述语',
  `show_number` int(1) DEFAULT '0' COMMENT '是否显示序号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `show_reset_button` int(1) DEFAULT '0' COMMENT '是否显示重置按钮',
  `reset_button_text` varchar(20) DEFAULT '' COMMENT '重置按钮文字',
  `reset_button_color` varchar(50) DEFAULT '' COMMENT '重置按钮颜色',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单项目风格显示表';

-- ----------------------------
-- Records of form_project_style
-- ----------------------------

-- ----------------------------
-- Table structure for form_project_theme
-- ----------------------------
DROP TABLE IF EXISTS `form_project_theme`;
CREATE TABLE `form_project_theme` (
  `theme_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键id',
  `head_image` longtext COMMENT '头部图片',
  `theme_name` varchar(255) DEFAULT '' COMMENT '主题名称',
  `theme_style` varchar(500) DEFAULT '' COMMENT '主题风格样式逗号分隔',
  `theme_color` varchar(255) DEFAULT '' COMMENT '主题颜色逗号分隔',
  `button_color` varchar(50) DEFAULT '' COMMENT '按钮颜色',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `background_color` varchar(255) DEFAULT '' COMMENT '背景颜色',
  `background_img` varchar(255) DEFAULT '' COMMENT '背景图片',
  `show_title` int(1) DEFAULT '0' COMMENT '是否显示标题',
  `submit_button_text` varchar(20) DEFAULT '' COMMENT '提交按钮显示字',
  `show_describe` int(1) DEFAULT '0' COMMENT '是否显示描述语',
  `show_number` int(1) DEFAULT '0' COMMENT '是否显示序号',
  `show_reset_button` int(1) DEFAULT '0' COMMENT '是否显示重置按钮',
  `reset_button_text` varchar(20) DEFAULT '' COMMENT '重置按钮文字',
  `reset_button_color` varchar(50) DEFAULT '' COMMENT '重置按钮颜色',
  PRIMARY KEY (`theme_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表单项目外观风格';

-- ----------------------------
-- Records of form_project_theme
-- ----------------------------

-- ----------------------------
-- Table structure for help_category
-- ----------------------------
DROP TABLE IF EXISTS `help_category`;
CREATE TABLE `help_category` (
  `category_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `parent_id` varchar(32) DEFAULT '' COMMENT '父级id',
  `category_name` varchar(50) NOT NULL DEFAULT '' COMMENT '分类名称',
  `category_path` varchar(2000) DEFAULT '' COMMENT '分类路径',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '创建租户id',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='帮助分类';

-- ----------------------------
-- Records of help_category
-- ----------------------------

-- ----------------------------
-- Table structure for help_info
-- ----------------------------
DROP TABLE IF EXISTS `help_info`;
CREATE TABLE `help_info` (
  `info_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键id',
  `category_id` varchar(32) NOT NULL DEFAULT '' COMMENT '信息所属分类id',
  `info_title` varchar(255) NOT NULL DEFAULT '' COMMENT '信息标题',
  `introduction` varchar(255) DEFAULT '' COMMENT '信息简介',
  `info_content` mediumtext COMMENT '信息内容',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `is_delete` int(1) DEFAULT '0' COMMENT '是否删除[0:否;1:是;]',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '创建租户id',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `data_allow_auth` varchar(50) DEFAULT '' COMMENT '数据允许查询权限，ALL_AUTH 所有用户,其他字符时表示指定用户可见',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='帮助信息发布记录表';

-- ----------------------------
-- Records of help_info
-- ----------------------------

-- ----------------------------
-- Table structure for help_quick_link
-- ----------------------------
DROP TABLE IF EXISTS `help_quick_link`;
CREATE TABLE `help_quick_link` (
  `link_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `link_name` varchar(255) NOT NULL DEFAULT '' COMMENT '链接名称',
  `link_image` longtext COMMENT '链接图标',
  `remark` varchar(255) DEFAULT '' COMMENT '备注说明',
  `link_url` varchar(255) DEFAULT '' COMMENT '链接地址',
  `status_code` varchar(50) DEFAULT '' COMMENT '状态，FORBID禁用 NORMAL正常',
  `open_type_code` varchar(50) DEFAULT '' COMMENT '打开方式，取数据字典',
  `link_position_code` varchar(50) DEFAULT '' COMMENT '链接位置，导航，首页中部',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '创建租户id',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`link_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='帮助中心快捷链接';

-- ----------------------------
-- Records of help_quick_link
-- ----------------------------

-- ----------------------------
-- Table structure for info_category
-- ----------------------------
DROP TABLE IF EXISTS `info_category`;
CREATE TABLE `info_category` (
  `category_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `parent_id` varchar(32) DEFAULT '' COMMENT '父级id',
  `category_name` varchar(50) NOT NULL DEFAULT '' COMMENT '分类名称',
  `category_path` varchar(2000) DEFAULT '' COMMENT '分类路径',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '创建租户id',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='信息分类';

-- ----------------------------
-- Records of info_category
-- ----------------------------

-- ----------------------------
-- Table structure for info_news
-- ----------------------------
DROP TABLE IF EXISTS `info_news`;
CREATE TABLE `info_news` (
  `news_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键id',
  `category_id` varchar(32) NOT NULL DEFAULT '' COMMENT '信息所属分类id',
  `news_title` varchar(255) NOT NULL DEFAULT '' COMMENT '信息标题',
  `news_thumbnail` varchar(255) DEFAULT '' COMMENT '信息缩略图',
  `introduction` varchar(255) DEFAULT '' COMMENT '信息简介',
  `news_content` mediumtext COMMENT '信息内容',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `is_delete` int(1) DEFAULT '0' COMMENT '是否删除[0:否;1:是;]',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '创建租户id',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `data_allow_auth` varchar(50) DEFAULT '' COMMENT '数据允许查询权限，ALL_AUTH 所有用户,其他字符时表示指定用户可见',
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='信息发布记录表';

-- ----------------------------
-- Records of info_news
-- ----------------------------

-- ----------------------------
-- Table structure for notice_category
-- ----------------------------
DROP TABLE IF EXISTS `notice_category`;
CREATE TABLE `notice_category` (
  `category_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `category_name` varchar(50) NOT NULL DEFAULT '' COMMENT '分类名称',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '创建租户id',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='通知分类';

-- ----------------------------
-- Records of notice_category
-- ----------------------------

-- ----------------------------
-- Table structure for notice_info
-- ----------------------------
DROP TABLE IF EXISTS `notice_info`;
CREATE TABLE `notice_info` (
  `notice_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键id',
  `category_id` varchar(32) NOT NULL DEFAULT '' COMMENT '所属分类id',
  `notice_title` varchar(255) NOT NULL DEFAULT '' COMMENT '通知标题',
  `notice_content` mediumtext COMMENT '通知内容',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '创建租户id',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `data_allow_auth` varchar(50) DEFAULT '' COMMENT '数据允许查询权限，ALL_AUTH 所有用户,其他字符时表示指定用户可见',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='通知信息记录表';

-- ----------------------------
-- Records of notice_info
-- ----------------------------

-- ----------------------------
-- Table structure for quartz_job
-- ----------------------------
DROP TABLE IF EXISTS `quartz_job`;
CREATE TABLE `quartz_job` (
  `job_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键id',
  `job_name` varchar(50) NOT NULL DEFAULT '' COMMENT '调度作业名称',
  `group_id` varchar(32) NOT NULL DEFAULT '' COMMENT '任务所在分组id',
  `cron_expression` varchar(50) NOT NULL DEFAULT '' COMMENT '执行表达式,例如:0 0/1 * * * ?',
  `is_sync_code` varchar(50) DEFAULT '' COMMENT '是否同步执行NO-不是YES=是',
  `class_path` varchar(255) NOT NULL DEFAULT '' COMMENT '要执行的任务类名称路径',
  `method_name` varchar(100) NOT NULL DEFAULT '' COMMENT '要执行的任务方法名称',
  `job_status_code` varchar(50) NOT NULL DEFAULT '' COMMENT 'job运行状态NOT_RUNNING=不运行RUNNING=运行EXCEPTION=异常',
  `spring_id` varchar(100) DEFAULT '' COMMENT 'spring beanId',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `last_time` datetime DEFAULT NULL COMMENT '调度作业上次执行时间',
  `next_time` datetime DEFAULT NULL COMMENT '调度作业下次执行时间',
  `remark` text COMMENT '备注',
  `description` varchar(255) DEFAULT '' COMMENT '作业描述',
  `built_in` varchar(50) DEFAULT '' COMMENT '是否内置系统任务NO-不是YES=是',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门ID',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户ID',
  PRIMARY KEY (`job_id`),
  UNIQUE KEY `job_name` (`job_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统调度任务表';

-- ----------------------------
-- Records of quartz_job
-- ----------------------------
INSERT INTO `quartz_job` VALUES ('smsSendTask', '短信定时发送任务', 'smsSendTask', '0 0/1 * * * ?', 'NO', 'com.duojuhe.admin.spring.schedule.task.SmsSendTask', 'executeSmsSendTask', 'RUNNING', 'smsSendTask', '1', '2022-03-16 15:44:06', '1', '2022-03-16 15:44:06', '2022-12-02 16:47:00', '2022-12-02 16:48:00', '运行正常', '每分钟检测短信定时发送任务', 'YES', '-1', '-1');

-- ----------------------------
-- Table structure for quartz_job_log
-- ----------------------------
DROP TABLE IF EXISTS `quartz_job_log`;
CREATE TABLE `quartz_job_log` (
  `log_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `job_id` varchar(32) NOT NULL DEFAULT '' COMMENT '任务id',
  `group_id` varchar(32) DEFAULT '' COMMENT '任务所在分组id',
  `job_name` varchar(50) NOT NULL DEFAULT '' COMMENT '任务名称',
  `cron_expression` varchar(50) NOT NULL DEFAULT '' COMMENT '任务表达式',
  `is_sync_code` varchar(50) NOT NULL DEFAULT '' COMMENT '是否同步执行NO-不是YES=是',
  `class_path` varchar(255) NOT NULL DEFAULT '' COMMENT '要执行的任务类名称路径',
  `method_name` varchar(100) NOT NULL DEFAULT '' COMMENT '要执行的任务方法名称',
  `create_time` datetime(3) DEFAULT NULL COMMENT '日志时间',
  `start_time` datetime(3) DEFAULT NULL COMMENT '执行开始时间',
  `stop_time` datetime(3) DEFAULT NULL COMMENT '执行停止时间',
  `operation_status_code` varchar(50) DEFAULT NULL COMMENT '执行状态，取数据字典',
  `remark` varchar(255) DEFAULT '' COMMENT '备注说明',
  `description` varchar(255) DEFAULT '' COMMENT '描述说明',
  `spring_id` varchar(100) DEFAULT '' COMMENT 'spring beanId',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统调度任务执行日志';

-- ----------------------------
-- Records of quartz_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sms_channel
-- ----------------------------
DROP TABLE IF EXISTS `sms_channel`;
CREATE TABLE `sms_channel` (
  `channel_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `channel_code` varchar(32) NOT NULL DEFAULT '' COMMENT '渠道编码，具有唯一性',
  `isp_id` varchar(32) DEFAULT '' COMMENT 'isp主键id',
  `channel_name` varchar(255) DEFAULT '' COMMENT '渠道名称',
  `channel_isp_attribute` longtext COMMENT '渠道服务商配置',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `status_code` varchar(50) DEFAULT 'NORMAL' COMMENT '渠道状态，FORBID禁用 NORMAL正常',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户id',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `built_in` varchar(50) DEFAULT '' COMMENT '是否内置，取数据字典',
  PRIMARY KEY (`channel_id`),
  UNIQUE KEY `uni_channel_code` (`channel_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信发送渠道';

-- ----------------------------
-- Records of sms_channel
-- ----------------------------

-- ----------------------------
-- Table structure for sms_isp
-- ----------------------------
DROP TABLE IF EXISTS `sms_isp`;
CREATE TABLE `sms_isp` (
  `isp_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `isp_code` varchar(10) NOT NULL DEFAULT '' COMMENT '服务商编码，具有唯一性',
  `isp_logo` varchar(255) DEFAULT '' COMMENT '服务商logo',
  `isp_name` varchar(255) DEFAULT '' COMMENT '服务商名称',
  `isp_attribute` text COMMENT '服务商属性 json 格式',
  `status_code` varchar(50) DEFAULT 'NORMAL' COMMENT '厂商状态，FORBID禁用 NORMAL正常',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `description` varchar(255) DEFAULT '' COMMENT '描述说明',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`isp_id`),
  UNIQUE KEY `uni_isp_code` (`isp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信网络服务供应商';

-- ----------------------------
-- Records of sms_isp
-- ----------------------------
INSERT INTO `sms_isp` VALUES ('aliyun', 'aliyun', '', '阿里云', '{\"attribute\":[{\"key\":\"appId\",\"name\":\"短信账号\",\"desc\":\"请填写阿里云的短信发送账号\",\"type\":\"text\",\"maxlength\":50,\"requiredMessage\":\"短信账号必须填写\",\"required\":true},{\"key\":\"appKey\",\"name\":\"短信密码\",\"desc\":\"请填写阿里云的短信发送密码\",\"type\":\"text\",\"maxlength\":50,\"requiredMessage\":\"短信密码必须填写\",\"required\":true}],\"event\":{\"apiUrl\":\"https://search.gitee.com\"}}', 'NORMAL', '阿里云短信申请地址,如下:', '阿里云短信申请地址：https://www.aliyun.com/product/sms，请打开该网址进行账号申请', '2022-03-03 09:44:39', '0');

-- ----------------------------
-- Table structure for sms_record
-- ----------------------------
DROP TABLE IF EXISTS `sms_record`;
CREATE TABLE `sms_record` (
  `record_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `mobile_number` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号码',
  `content` varchar(500) DEFAULT '' COMMENT '短信内容',
  `create_time` datetime DEFAULT NULL COMMENT '发送时间',
  `send_status_code` varchar(50) DEFAULT '' COMMENT '短信发送状态，取数据字典',
  `send_ip` varchar(50) DEFAULT '' COMMENT '发送ip',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人ID',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门ID',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户ID',
  `send_task_id` varchar(32) DEFAULT '' COMMENT '短信发送任务id，来源短信任务表',
  `send_number` int(5) DEFAULT '0' COMMENT '发送次数，一般是接口异常尝试次数',
  `send_task_detail_id` varchar(32) DEFAULT '' COMMENT '短信发送任务对应明细id，来源短信任务明细表',
  `isp_id` varchar(32) DEFAULT '' COMMENT '短信服务商id',
  `channel_id` varchar(32) DEFAULT '' COMMENT '短信发送渠道id',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信发送记录表';

-- ----------------------------
-- Records of sms_record
-- ----------------------------

-- ----------------------------
-- Table structure for sms_send_task
-- ----------------------------
DROP TABLE IF EXISTS `sms_send_task`;
CREATE TABLE `sms_send_task` (
  `task_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键id任务id',
  `task_name` varchar(255) DEFAULT '' COMMENT '任务名称',
  `task_status_code` varchar(50) DEFAULT '' COMMENT '任务状态',
  `task_type_code` varchar(50) DEFAULT '' COMMENT '任务类型，定时发送，立即发送',
  `task_mode_code` varchar(50) DEFAULT '' COMMENT '任务模式，自定义号码，系统内部联系人号码',
  `task_send_ip` varchar(50) DEFAULT '' COMMENT '任务发送ip',
  `task_execute_time` datetime DEFAULT NULL COMMENT '任务执行时间',
  `channel_id` varchar(32) NOT NULL DEFAULT '' COMMENT '渠道id',
  `channel_code` varchar(32) NOT NULL DEFAULT '' COMMENT '渠道编码',
  `channel_name` varchar(255) DEFAULT '' COMMENT '渠道名称',
  `channel_isp_attribute` text COMMENT '渠道服务商配置',
  `isp_id` varchar(32) NOT NULL DEFAULT '' COMMENT 'isp主键id',
  `isp_code` varchar(50) DEFAULT '' COMMENT '服务商编码',
  `isp_name` varchar(255) DEFAULT '' COMMENT '服务商名称',
  `isp_attribute` text COMMENT '服务商属性 json 格式',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户id',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `description` varchar(255) DEFAULT '' COMMENT '描述说明',
  `timing_time` datetime DEFAULT NULL COMMENT '定时发送的定时时间yyyy-MM-dd HH:MM',
  `template_id` varchar(32) DEFAULT '' COMMENT '短信模板ID',
  `template_content` varchar(255) DEFAULT '' COMMENT '短信模板内容',
  `template_code` varchar(50) DEFAULT '' COMMENT '短信模板code',
  `template_sign` varchar(50) DEFAULT '' COMMENT '短信签名',
  `template_name` varchar(255) DEFAULT '' COMMENT '短信模板名称',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信发送任务表';

-- ----------------------------
-- Records of sms_send_task
-- ----------------------------

-- ----------------------------
-- Table structure for sms_send_task_detail
-- ----------------------------
DROP TABLE IF EXISTS `sms_send_task_detail`;
CREATE TABLE `sms_send_task_detail` (
  `detail_id` varchar(32) NOT NULL DEFAULT '' COMMENT '明细id',
  `task_id` varchar(32) NOT NULL DEFAULT '' COMMENT '所属任务id',
  `mobile_number` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号码',
  `send_status_code` varchar(50) DEFAULT '' COMMENT '短信发送状态，取数据字典',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信发送任务明细表';

-- ----------------------------
-- Records of sms_send_task_detail
-- ----------------------------

-- ----------------------------
-- Table structure for sms_template
-- ----------------------------
DROP TABLE IF EXISTS `sms_template`;
CREATE TABLE `sms_template` (
  `template_id` varchar(32) NOT NULL DEFAULT '' COMMENT '模板主键',
  `template_code` varchar(255) NOT NULL DEFAULT '' COMMENT '模板编码',
  `template_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '模板名称',
  `template_sign` varchar(50) NOT NULL DEFAULT '' COMMENT '模板签名名称',
  `content` varchar(255) DEFAULT '' COMMENT '模板内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_user_id` varchar(32) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '创建人ID',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门ID',
  `built_in` varchar(50) NOT NULL DEFAULT 'NO' COMMENT '是否内置，取数据字典',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户ID',
  `isp_id` varchar(32) DEFAULT '' COMMENT '模板归属服务商id',
  `channel_id` varchar(32) DEFAULT '' COMMENT '模板归属发送渠道id',
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信模板';

-- ----------------------------
-- Records of sms_template
-- ----------------------------

-- ----------------------------
-- Table structure for system_config
-- ----------------------------
DROP TABLE IF EXISTS `system_config`;
CREATE TABLE `system_config` (
  `config_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `ding_enable` varchar(50) NOT NULL DEFAULT '' COMMENT '系统错误钉钉通知状态，FORBID禁用 NORMAL正常',
  `ding_access_token` varchar(255) NOT NULL DEFAULT '' COMMENT '系统错误调用钉钉接口的token',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统配置表';

-- ----------------------------
-- Records of system_config
-- ----------------------------
INSERT INTO `system_config` VALUES ('system_config_key', 'FORBID', '111', '2022-06-26 16:15:34', '2022-06-26 16:15:36', '20220101100000000', '20220101100000000');

-- ----------------------------
-- Table structure for system_custom
-- ----------------------------
DROP TABLE IF EXISTS `system_custom`;
CREATE TABLE `system_custom` (
  `custom_id` varchar(32) NOT NULL COMMENT '主键id自定义key和用户id的MD5值',
  `custom_key` varchar(100) NOT NULL DEFAULT '' COMMENT '自定义key',
  `content` text COMMENT '自定义json串',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '用户ID',
  `create_user_id_source` varchar(255) DEFAULT '' COMMENT '用户ID来源',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门ID',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户id',
  PRIMARY KEY (`custom_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户个人喜好设置';

-- ----------------------------
-- Records of system_custom
-- ----------------------------

-- ----------------------------
-- Table structure for system_data_filter
-- ----------------------------
DROP TABLE IF EXISTS `system_data_filter`;
CREATE TABLE `system_data_filter` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键字段',
  `target_id` varchar(32) NOT NULL DEFAULT '' COMMENT '目标id，不同表的主键',
  `target_id_source` varchar(50) NOT NULL DEFAULT '' COMMENT '目标id表',
  `relation_id` varchar(32) NOT NULL DEFAULT '' COMMENT '关联资源id',
  `relation_id_source` varchar(50) NOT NULL DEFAULT '' COMMENT '关联资源id表',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统数据过滤权限表';

-- ----------------------------
-- Records of system_data_filter
-- ----------------------------

-- ----------------------------
-- Table structure for system_dept
-- ----------------------------
DROP TABLE IF EXISTS `system_dept`;
CREATE TABLE `system_dept` (
  `dept_id` varchar(32) NOT NULL DEFAULT '' COMMENT '部门id',
  `parent_id` varchar(32) NOT NULL DEFAULT '-1' COMMENT '上级部门id，-1表示顶级部门',
  `dept_name` varchar(50) NOT NULL DEFAULT '' COMMENT '部门名称',
  `dept_code` varchar(50) NOT NULL DEFAULT '' COMMENT '部门编码',
  `dept_path` varchar(2000) NOT NULL DEFAULT '' COMMENT '部门层级路径',
  `dept_alias` varchar(20) DEFAULT '' COMMENT '部门简称别名',
  `dept_telephone` varchar(50) DEFAULT '' COMMENT '部门电话',
  `dept_leader` varchar(50) DEFAULT '' COMMENT '部门负责人',
  `dept_fax` varchar(50) DEFAULT '' COMMENT '部门传真',
  `remark` varchar(100) DEFAULT '' COMMENT '备注',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门ID',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户ID',
  `built_in` varchar(50) DEFAULT '' COMMENT '是否内置，取数据字典',
  PRIMARY KEY (`dept_id`),
  UNIQUE KEY `uni_dept_code` (`dept_code`),
  UNIQUE KEY `uni_tenant_dept_name` (`dept_name`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统部门表';

-- ----------------------------
-- Records of system_dept
-- ----------------------------
INSERT INTO `system_dept` VALUES ('20220101100000000', '-1', '管理部', '202203041000510000', '/20220101100000000', '', '', '鸣人', '', '', '1', '2022-03-04 10:00:56', '2022-09-29 11:04:12', '20220101100000000', '20220101100000000', '20220101100000000', '20220101100000000', 'YES');

-- ----------------------------
-- Table structure for system_dict
-- ----------------------------
DROP TABLE IF EXISTS `system_dict`;
CREATE TABLE `system_dict` (
  `dict_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `parent_id` varchar(32) NOT NULL DEFAULT '' COMMENT '父级Id-1表示顶级',
  `dict_code` varchar(50) NOT NULL DEFAULT '' COMMENT '字典编码',
  `dict_name` varchar(50) DEFAULT '' COMMENT '字典名称',
  `dict_color` varchar(20) DEFAULT '' COMMENT '字典显示颜色',
  `status_code` varchar(50) NOT NULL DEFAULT '' COMMENT '状态：FORBID禁用 NORMAL正常',
  `remark` varchar(200) DEFAULT '' COMMENT '备注',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `dict_type_code` varchar(50) DEFAULT 'BUSINESS_DICT' COMMENT '字典类型:BUSINESS_DICT业务字典 SYSTEM_DICT系统字典',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `uni_dict_code` (`dict_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据字典表';

-- ----------------------------
-- Records of system_dict
-- ----------------------------
INSERT INTO `system_dict` VALUES ('10000', '-1', 'QUARTZ_STATUS', '定时任务运行状态', '#F56C6C', 'NORMAL', '', '1', '', '2021-05-13 14:38:27', '1', '2022-01-24 16:03:17', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10000001', '10000', 'NOT_RUNNING', '不运行', '#F56C6C', 'NORMAL', '', '0', '', '2021-05-13 12:38:27', '', '2021-05-13 13:37:57', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10000002', '10000', 'RUNNING', '运行中', '#F56C6C', 'NORMAL', '', '0', '', '2021-05-13 15:38:27', '', '2021-05-13 13:37:59', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10001', '-1', 'MENU_TYPE', '菜单类型', '#F56C6C', 'NORMAL', '', '1', '', '2021-05-13 09:38:27', '1', '2022-01-24 15:14:03', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10001001', '10001', 'NAVIGATION', '导航菜单', '#F56C6C', 'NORMAL', '', '0', '', '2021-05-13 17:38:27', '', '2021-05-13 13:38:04', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10001002', '10001', 'BUTTON', '功能按钮', '#F56C6C', 'NORMAL', '', '0', '', '2021-05-14 10:22:31', '', '2021-05-13 13:38:23', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10002', '-1', 'SYSTEM_USER_TYPE', '系统用户类型', '#F56C6C', 'NORMAL', '', '0', '', '2021-05-14 10:22:39', '', '2021-05-13 13:38:25', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10002001', '10002', 'DEPT_ADMIN', '部门管理员', '#F56C6C', 'NORMAL', '', '0', '', '2021-05-14 10:22:42', '', '2021-05-13 13:38:27', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10003', '-1', 'STATUS', '状态', '#F56C6C', 'NORMAL', '', '0', '', '2021-05-14 10:22:43', '', '2021-05-13 13:38:28', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10003001', '10003', 'FORBID', '禁用', '#F56C6C', 'NORMAL', '', '0', '', '2021-05-14 10:22:46', '', '2021-05-13 13:38:33', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10003002', '10003', 'NORMAL', '正常', '#67C23A', 'NORMAL', '', '1', '', '2021-05-14 10:22:48', '', '2021-05-13 13:38:31', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10004', '-1', 'YES_NO', '是否标识', '', 'NORMAL', '', '0', '', '2021-05-14 10:22:49', '', '2021-05-13 13:38:30', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10004001', '10004', 'YES', '是', '', 'NORMAL', '', '0', '', '2021-05-14 10:22:51', '', '2021-05-13 13:38:35', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10004002', '10004', 'NO', '否', '', 'NORMAL', '', '0', '', '2021-05-14 10:22:53', '', '2021-05-13 13:38:37', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10005', '-1', 'AUDIT_STATUS', '审核状态', '', 'NORMAL', '', '0', '', '2021-05-14 10:22:55', '', '2021-05-13 13:38:38', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10005001', '10005', 'NO_REVIEWED', '未审核', '', 'NORMAL', '', '0', '', '2021-05-14 10:22:58', '', '2021-05-13 13:38:40', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10005002', '10005', 'REVIEWED', '已审核', '', 'NORMAL', '', '0', '', '2021-05-14 10:23:00', '', '2021-05-13 13:38:41', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10005003', '10005', 'AUDIT_FAILED', '审核不通过', '', 'NORMAL', '', '0', '', '2021-05-14 10:23:01', '', '2021-05-13 13:38:43', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10006', '-1', 'DATA_SCOPE', '数据范围', '', 'NORMAL', '', '0', '', '2021-05-14 10:23:04', '', '2021-05-14 10:18:39', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10006001', '10006', 'TENANT_ALL_DATA', '所有数据', '#8E2323', 'NORMAL', '', '9', '', '2021-05-14 10:23:06', '1', '2022-01-24 16:38:19', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10006002', '10006', 'SELF_DEPT', '本部门数据', '', 'NORMAL', '', '8', '', '2021-05-14 10:23:08', '', '2021-05-14 10:21:16', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10006003', '10006', 'SELF_DEPT_SUBORDINATE', '本部门及以下数据', '', 'NORMAL', '', '7', '', '2021-05-14 10:23:10', '', '2021-05-14 10:21:17', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10006004', '10006', 'CUSTOM_DATA', '自定义部门数据', '', 'NORMAL', '', '6', '', '2021-05-14 10:23:12', '', '2021-05-14 10:21:19', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10006005', '10006', 'SELF_DATA', '仅本人创建数据', '', 'NORMAL', '', '5', '', '2021-05-14 10:23:14', '', '2021-05-14 10:21:21', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10007', '-1', 'OPERATION_STATUS', '操作状态', '', 'NORMAL', '', '0', '', '2021-05-14 11:28:21', '', '2021-05-14 11:29:29', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10007001', '10007', 'SUCCESS', '成功', '', 'NORMAL', '', '0', '', '2021-05-14 11:29:21', '', '2021-05-14 11:29:31', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10007002', '10007', 'FAILED', '失败', '', 'NORMAL', '', '0', '', '2021-05-14 11:29:23', '', '2021-05-14 11:29:33', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10007003', '10007', 'EXCEPTION', '异常', '', 'NORMAL', '', '0', '', '2021-05-14 16:10:10', '', '2021-05-14 16:10:12', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10008', '-1', 'DICT_TYPE', '字典类型', '', 'NORMAL', '', '0', '', '2021-05-14 16:57:43', '', '2021-05-14 16:57:50', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100080001', '10008', 'SYSTEM_DICT', '系统字典', '', 'NORMAL', '', '0', '', '2021-05-14 16:57:45', '', '2021-05-14 16:57:52', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100080002', '10008', 'BUSINESS_DICT', '业务字典', '', 'NORMAL', '', '0', '', '2021-05-14 16:57:47', '', '2021-05-14 16:57:53', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10009', '-1', 'GENDER', '性别', '', 'NORMAL', '', '0', '', '2021-12-28 17:39:37', '', '2021-12-28 17:39:39', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100090001', '10009', 'MALE', '男', '', 'NORMAL', '', '0', '', '2021-12-28 17:40:18', '', '2021-12-28 17:40:21', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100090002', '10009', 'FEMALE', '女', '', 'NORMAL', '', '0', '', '2021-12-28 17:40:19', '', '2021-12-28 17:40:23', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10010', '-1', 'OPERATION_TYPE', '操作类型', '', 'NORMAL', '', '0', '', '2021-05-17 10:17:21', '', '2021-05-17 10:17:38', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10010001', '10010', 'LOGIN', '登录', '', 'NORMAL', '', '0', '', '2021-05-17 10:17:25', '', '2021-05-17 10:17:40', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10010002', '10010', 'LOG_OUT', '注销登录', '', 'NORMAL', '', '0', '', '2021-05-17 10:17:27', '', '2021-05-17 10:17:42', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10010003', '10010', 'ADD', '新增', '', 'NORMAL', '', '0', '', '2021-05-17 10:17:28', '', '2021-05-17 10:17:43', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10010004', '10010', 'UPDATE', '修改', '', 'NORMAL', '', '0', '', '2021-05-17 10:17:30', '', '2021-05-17 10:17:45', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10010005', '10010', 'DELETE', '删除', '', 'NORMAL', '', '0', '', '2021-05-17 10:17:32', '', '2021-05-17 10:17:47', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10010006', '10010', 'UPLOAD', '上传', '', 'NORMAL', '', '0', '', '2021-05-17 10:17:34', '', '2021-05-17 10:17:48', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10010007', '10010', 'OTHER', '其他', '', 'NORMAL', '', '0', '', '2021-05-17 10:17:35', '', '2021-05-17 10:17:50', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10010008', '10010', 'RECYCLE', '回收站', '', 'NORMAL', '', '0', '', '2022-04-14 15:38:07', '', '2022-04-14 15:38:10', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10010009', '10010', 'RECYCLE_RECOVERY', '回收站恢复', '', 'NORMAL', '', '0', '', '2022-04-14 15:38:39', '', '2022-04-14 15:38:41', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10010010', '10010', 'RESET_PASSWORD', '重置密码', '', 'NORMAL', '', '0', '', '2022-08-04 11:25:38', '', '2022-08-04 11:25:50', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10010011', '10010', 'UPDATE_STATUS', '改变状态', '', 'NORMAL', '', '0', '', '2022-08-04 11:25:41', '', '2022-08-04 11:25:52', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10010012', '10010', 'RELEASE_WORKFLOW_PROJECT', '发布工作流模型', '', 'NORMAL', '', '0', '', '2022-08-04 11:25:43', '', '2022-08-04 11:25:54', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10010013', '10010', 'SUBMIT_WORKFLOW_RECORD', '提交工作流', '', 'NORMAL', '', '0', '', '2022-08-04 11:25:45', '', '2022-08-04 11:25:56', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10011', '-1', 'SMS_SEND_STATUS', '短信发送状态', '', 'NORMAL', '', '0', '', '2021-09-08 15:37:06', '', '2021-09-08 15:37:09', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100110001', '10011', 'SMS_SEND_SUCCESS', '发送成功', '', 'NORMAL', '', '0', '', '2021-09-08 15:37:43', '', '2021-09-08 15:38:03', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100110002', '10011', 'SMS_SEND_FAIL', '发送失败', '', 'NORMAL', '', '0', '', '2021-09-08 15:37:59', '', '2021-09-08 15:38:04', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100110003', '10011', 'SMS_SEND_SENDING', '发送中', '', 'NORMAL', '', '0', '', '2022-03-14 16:02:07', '', '2022-03-14 16:02:08', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100110004', '10011', 'SMS_SEND_WAITING', '等待发送', '', 'NORMAL', '', '0', '', '2022-03-16 10:51:12', '', '2022-03-16 10:51:15', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10012', '-1', 'HANDLING_STATUS', '处理状态', '', 'NORMAL', '', '0', '', '2022-01-14 16:59:42', '', '2022-01-14 16:59:44', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100120001', '10012', 'PENDING', '待处理', '', 'NORMAL', '', '0', '', '2022-01-14 17:00:08', '', '2022-01-14 17:00:10', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100120002', '10012', 'PROCESSED_ING', '处理中', '', 'NORMAL', '', '0', '', '2022-01-14 17:00:40', '', '2022-01-14 17:00:42', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100120003', '10012', 'PROCESSED', '已处理', '', 'NORMAL', '', '0', '', '2022-01-14 17:01:01', '', '2022-01-14 17:01:03', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10013', '-1', 'SYSTEM_MESSAGE_TYPE', '系统消息类型', '', 'NORMAL', '', '0', '', '2022-01-14 17:02:44', '', '2022-01-14 17:02:46', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('1001300001', '10013', 'WORKFLOW_MESSAGE', '工作流消息', '', 'NORMAL', '', '0', '', '2022-07-22 16:24:55', '', '2022-07-22 16:24:57', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10014', '-1', 'SMS_TASK_TYPE', '短信任务类型', '', 'NORMAL', '', '0', '', '2022-03-14 15:51:27', '', '2022-03-14 15:51:31', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100140001', '10014', 'SMS_TIMING_SEND', '定时发送', '', 'NORMAL', '', '0', '', '2022-03-14 15:58:50', '', '2022-03-14 15:58:54', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100140002', '10014', 'SMS_IMMEDIATELY_SEND', '立即发送', '', 'NORMAL', '', '0', '', '2022-03-14 15:58:52', '', '2022-03-14 15:58:56', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10015', '-1', 'SMS_TASK_MODE', '短信任务模式', '', 'NORMAL', '', '0', '', '2022-03-14 15:52:15', '', '2022-03-14 15:52:16', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100150001', '10015', 'SMS_CUSTOM_MODE', '自定义模式', '', 'NORMAL', '', '0', '', '2022-03-14 16:09:36', '', '2022-03-14 16:09:37', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10016', '-1', 'SMS_TASK_STATUS', '短信任务状态', '', 'NORMAL', '', '0', '', '2022-03-14 15:53:35', '', '2022-03-14 15:53:37', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100160001', '10016', 'SMS_ALREADY_EXECUTE', '已执行', '', 'NORMAL', '', '0', '', '2022-03-14 16:08:39', '', '2022-03-14 16:08:41', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100160002', '10016', 'SMS_WAITING_EXECUTE', '待执行', '', 'NORMAL', '', '0', '', '2022-03-14 16:09:03', '', '2022-03-14 16:09:05', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10017', '-1', 'FORM_PROJECT_STATUS', '表单项目状态', '', 'NORMAL', '', '0', '', '2022-03-23 10:49:07', '', '2022-03-23 10:49:11', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100170001', '10017', 'FORM_PROJECT_UNPUBLISHED', '未发布', '#959595', 'NORMAL', '', '0', '', '2022-03-23 10:49:32', '', '2022-03-23 10:49:33', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100170002', '10017', 'FORM_PROJECT_PUBLISHED', '已发布', '#00a650', 'NORMAL', '', '0', '', '2022-03-23 10:49:56', '', '2022-03-23 10:49:57', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100170003', '10017', 'FORM_PROJECT_STOP', '已结束', '#f00', 'NORMAL', '', '0', '', '2022-03-23 10:50:23', '', '2022-03-23 10:50:24', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10018', '-1', 'FORM_PROJECT_THEME_STYLE', '表单项目主题风格', '', 'NORMAL', '', '1', '', '2022-03-31 13:48:20', '1', '2022-03-31 13:49:49', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100180001', '10018', 'FR_STYLE_FESTIVAL', '节日', '', 'NORMAL', '', '0', '', '2022-03-31 13:59:02', '', '2022-03-31 13:59:04', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100180002', '10018', 'FR_STYLE_PARENT_CHILD', '亲子', '', 'NORMAL', '', '0', '', '2022-04-01 13:45:54', '', '2022-04-01 13:45:56', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100180003', '10018', 'FR_STYLE_SCENERY', '风景', '', 'NORMAL', '', '0', '', '2022-04-01 13:47:50', '', '2022-04-01 13:47:53', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100180004', '10018', 'FR_STYLE_SCHOOL', '校园', '', 'NORMAL', '', '0', '', '2022-04-01 13:48:27', '', '2022-04-01 13:48:29', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100180005', '10018', 'FR_STYLE_COMMERCE', '商务', '', 'NORMAL', '', '0', '', '2022-04-01 13:49:02', '', '2022-04-01 13:49:04', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100180006', '10018', 'FR_STYLE_CATERING', '餐饮', '', 'NORMAL', '', '0', '', '2022-04-01 13:49:41', '', '2022-04-01 13:49:42', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100180007', '10018', 'FR_STYLE_FANGYI', '防疫', '', 'NORMAL', '', '0', '', '2022-04-01 13:50:13', '', '2022-04-01 13:50:14', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10019', '-1', 'FORM_PROJECT_THEME_COLOR', '表单项目主题颜色', '', 'NORMAL', '', '1', '', '2022-03-31 13:49:11', '1', '2022-03-31 13:49:34', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100190001', '10019', 'FR_COLOR_RED', '红色', '#FF0000', 'NORMAL', '', '0', '', '2022-03-31 14:00:58', '', '2022-03-31 14:01:02', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100190002', '10019', 'FR_COLOR_GREEN', '绿色', '#00BF6F', 'NORMAL', '', '0', '', '2022-04-01 14:03:03', '', '2022-04-01 14:03:05', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10020', '-1', 'WORKFLOW_STATUS', '工作流状态', '', 'NORMAL', '', '0', '', '2022-04-29 10:19:37', '', '2022-04-29 10:19:41', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100200001', '10020', 'WORKFLOW_UNPUBLISHED', '未发布', '', 'NORMAL', '', '0', '', '2022-04-29 10:20:17', '', '2022-04-29 10:20:20', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100200002', '10020', 'WORKFLOW_PUBLISHED', '已发布', '', 'NORMAL', '', '0', '', '2022-04-29 10:20:42', '', '2022-04-29 10:20:46', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10021', '-1', 'WORKFLOW_RECORD_STATUS', '流程记录状态', '', 'NORMAL', '', '0', '', '2022-05-07 14:20:31', '', '2022-05-07 14:20:34', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100210001', '10021', 'WORKFLOW_TOBE_SUBMITTED', '待提交', '', 'NORMAL', '', '0', '', '2022-05-17 17:42:49', '', '2022-05-17 17:42:52', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100210002', '10021', 'WORKFLOW_RECORD_PROGRESS', '进行中', '', 'NORMAL', '', '0', '', '2022-05-17 17:43:16', '', '2022-05-17 17:43:18', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100210003', '10021', 'WORKFLOW_RECORD_COMPLETE', '已完成', '', 'NORMAL', '', '0', '', '2022-05-17 17:45:11', '', '2022-05-17 17:45:13', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100210004', '10021', 'WORKFLOW_RECORD_CANCEL', '已取消', '', 'NORMAL', '', '0', '', '2022-06-10 11:33:22', '', '2022-06-10 11:33:30', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100210005', '10021', 'WORKFLOW_RECORD_STOP', '已终止', '', 'NORMAL', '', '0', '', '2022-06-10 11:33:27', '', '2022-06-10 11:33:32', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10022', '-1', 'LINK_OPEN_TYPE', '链接打开方式', '', 'NORMAL', '', '0', '', '2022-08-15 15:38:04', '', '2022-08-15 15:38:06', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100220001', '10022', 'LINK_BLANK', '新窗口', '', 'NORMAL', '', '0', '', '2022-08-15 15:43:06', '', '2022-08-15 15:43:08', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100220002', '10022', 'LINK_SELF', '当前窗口', '', 'NORMAL', '', '0', '', '2022-08-15 15:43:43', '', '2022-08-15 15:43:44', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10023', '-1', 'HELP_LINK_POSITION', '帮助链接位置', '', 'NORMAL', '', '0', '', '2022-08-15 15:44:23', '', '2022-08-15 15:44:25', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100230001', '10023', 'HELP_LINK_POSITION_INDEX', '首页位置', '', 'NORMAL', '', '0', '', '2022-08-15 15:44:56', '', '2022-08-15 15:45:39', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('100230002', '10023', 'HELP_LINK_POSITION_NAV', '导航位置', '', 'NORMAL', '', '0', '', '2022-08-15 15:45:36', '', '2022-08-15 15:45:37', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('10024', '-1', 'PRODUCT_SALE_STATUS', '产品销售状态', '', 'NORMAL', '', '0', '', '2022-08-17 10:28:18', '', '2022-08-17 10:28:20', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('1002400001', '10024', 'PRODUCT_START_SALE', '上架', '', 'NORMAL', '', '0', '', '2022-08-17 10:29:31', '', '2022-08-17 10:29:33', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('1002400002', '10024', 'PRODUCT_STOP_SALE', '下架', '', 'NORMAL', '', '0', '', '2022-08-17 10:30:24', '', '2022-08-17 10:30:26', 'SYSTEM_DICT');
INSERT INTO `system_dict` VALUES ('68a34cfe7a434fd2b078dfeba42a2a8a', '-1', '2', '2', '', 'FORBID', '2', '1', '20220101100000000', '2022-10-12 16:21:28', '20220101100000000', '2022-10-12 16:21:28', 'BUSINESS_DICT');
INSERT INTO `system_dict` VALUES ('a5752b08ea5a48bb87875e0898f589b5', 'fcc488eb39df4e8ab81a34eb69c7177c', '11', '11', '', 'NORMAL', '1', '1', '20220101100000000', '2022-10-12 16:21:21', '20220101100000000', '2022-10-12 16:21:21', 'BUSINESS_DICT');
INSERT INTO `system_dict` VALUES ('fcc488eb39df4e8ab81a34eb69c7177c', '-1', '1', '1', '', 'NORMAL', '1', '1', '20220101100000000', '2022-10-12 16:21:05', '20220101100000000', '2022-10-12 16:21:05', 'BUSINESS_DICT');

-- ----------------------------
-- Table structure for system_log
-- ----------------------------
DROP TABLE IF EXISTS `system_log`;
CREATE TABLE `system_log` (
  `log_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `login_name` varchar(32) DEFAULT '' COMMENT '登录名',
  `create_user_name` varchar(32) DEFAULT '' COMMENT '创建用户姓名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `operation_ip` varchar(50) DEFAULT '' COMMENT '操作IP',
  `ip_region` varchar(255) DEFAULT '' COMMENT '操作ip所属区域',
  `method_name` varchar(255) DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(255) DEFAULT '' COMMENT '请求方法',
  `module_name` varchar(255) DEFAULT '' COMMENT '模块名称',
  `operation_type_code` varchar(50) DEFAULT '' COMMENT '操作类型编码，取数据字典OPERATION_TYPE',
  `request_url` varchar(255) DEFAULT '' COMMENT '请求url',
  `request_parameter` longtext COMMENT '请求参数',
  `return_result` longtext COMMENT '返回结果',
  `operation_status_code` varchar(50) DEFAULT '' COMMENT '操作状态， SUCCESS=操作成功，FAILED=操作失败取数据字典OPERATION_STATUS',
  `exception_content` longtext COMMENT '异常内容',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户ID',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户ID',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门ID',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统操作日志';

-- ----------------------------
-- Records of system_log
-- ----------------------------

-- ----------------------------
-- Table structure for system_menu
-- ----------------------------
DROP TABLE IF EXISTS `system_menu`;
CREATE TABLE `system_menu` (
  `menu_id` varchar(32) NOT NULL DEFAULT '' COMMENT '菜单id',
  `parent_id` varchar(32) NOT NULL DEFAULT '-1' COMMENT '父级id',
  `menu_code` varchar(200) NOT NULL DEFAULT '' COMMENT '菜单编码',
  `menu_name` varchar(32) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `menu_icon` varchar(100) DEFAULT '' COMMENT '菜单小图标',
  `api_url` varchar(500) DEFAULT '' COMMENT '请求地址API需要使用的api接口多个请逗号分割',
  `component` varchar(255) DEFAULT '' COMMENT '前端组件地址',
  `status_code` varchar(50) DEFAULT '' COMMENT '状态：FORBID禁用 NORMAL正常取数据字典 STATUS',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `menu_type_code` varchar(50) DEFAULT '' COMMENT '功能类型 NAVIGATION 导航 BUTTON 功能按钮取数据字典 MENU_TYPE',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `built_in` varchar(50) NOT NULL DEFAULT 'NO' COMMENT '是否是内置:YES是，NO否',
  `remark` varchar(200) DEFAULT '' COMMENT '备注',
  `description` varchar(1000) DEFAULT '' COMMENT '菜单描述',
  `menu_frame_code` varchar(50) DEFAULT '' COMMENT '是否是外链:YES是，NO否',
  `menu_cache_code` varchar(50) DEFAULT '' COMMENT '是否缓存:YES是，NO否',
  `tenant_menu` varchar(50) DEFAULT NULL COMMENT '是否租户菜单:YES是，NO否',
  PRIMARY KEY (`menu_id`) USING BTREE,
  UNIQUE KEY `uni_menu_code` (`menu_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统菜单表';

-- ----------------------------
-- Records of system_menu
-- ----------------------------
INSERT INTO `system_menu` VALUES ('10000', '-1', 'authorityAdmin', '权限管理', 'table', '#', '', 'NORMAL', '2021-05-14 06:23:22', '2022-08-16 14:45:46', 'NAVIGATION', '888', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('10000001', '10000', 'systemDeptAdmin', '部门设置', 'tree-table', '#', 'duojuhe/system/dept/index', 'NORMAL', '2021-05-17 21:31:33', '2021-12-31 08:58:33', 'NAVIGATION', '9998', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('100000010001', '10000001', 'querySystemDeptList', '查询', 'yhgl', '/sysAdmin/systemDept/queryAllSystemDeptListTree', '', 'NORMAL', '2021-05-13 14:48:27', '2021-11-19 14:17:49', 'BUTTON', '9997', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('100000010002', '10000001', 'saveSystemDept', '新增', 'yhgl', '/sysAdmin/systemDept/saveSystemDept', '', 'NORMAL', '2021-05-13 14:48:29', null, 'BUTTON', '9996', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('100000010003', '10000001', 'updateSystemDept', '修改', 'yhgl', '/sysAdmin/systemDept/updateSystemDept', '', 'NORMAL', '2021-05-13 14:48:31', null, 'BUTTON', '9995', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('100000010004', '10000001', 'deleteSystemDept', '删除', 'yhgl', '/sysAdmin/systemDept/deleteSystemDeptByDeptId', '', 'NORMAL', '2021-05-13 14:48:26', null, 'BUTTON', '9994', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('10000002', '10000', 'systemPostAdmin', '岗位设置', 'job', '#', 'duojuhe/system/post/index', 'NORMAL', '2021-05-17 21:31:02', '2021-12-31 08:57:31', 'NAVIGATION', '8888', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('100000020001', '10000002', 'querySystemPostList', '查询', 'yhgl', '/sysAdmin/systemPost/querySystemPostPageResList', '', 'NORMAL', '2021-05-13 14:48:49', null, 'BUTTON', '8887', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('100000020002', '10000002', 'saveSystemPost', '新增', 'yhgl', '/sysAdmin/systemPost/saveSystemPost', '', 'NORMAL', '2021-05-13 14:48:51', null, 'BUTTON', '8886', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('100000020003', '10000002', 'updateSystemPost', '修改', 'yhgl', '/sysAdmin/systemPost/updateSystemPost', '', 'NORMAL', '2021-05-13 14:48:53', null, 'BUTTON', '8885', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('100000020004', '10000002', 'deleteSystemPost', '删除', 'yhgl', '/sysAdmin/systemPost/deleteSystemPostByPostId', '', 'NORMAL', '2021-05-13 14:48:48', null, 'BUTTON', '8884', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('100000020005', '10000002', 'exportSystemPostList', '导出', 'yhgl', '', '', 'NORMAL', '2021-05-14 13:52:16', null, 'BUTTON', '8883', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('10010', '-1', 'helpAdmin', '帮助中心', 'star', '#', '', 'NORMAL', '2022-08-15 15:57:40', '2022-08-15 16:10:50', 'NAVIGATION', '2229', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100100001', '10010', 'helpCategoryAdmin', '帮助分类', 'cascader', '', 'duojuhe/help/category/index', 'NORMAL', '2022-08-15 15:58:36', '2022-08-15 15:59:56', 'NAVIGATION', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000010001', '100100001', 'queryHelpCategoryList', '查询', '', '/sysAdmin/helpCategory/queryHelpCategoryTreeResList', '', 'NORMAL', '2022-08-15 16:11:58', '2022-08-15 16:11:58', 'BUTTON', '10', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000010002', '100100001', 'saveHelpCategory', '新增', '', '/sysAdmin/helpCategory/saveHelpCategory', '', 'NORMAL', '2022-08-15 16:12:23', '2022-08-15 16:12:23', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000010003', '100100001', 'updateHelpCategory', '修改', '', '/sysAdmin/helpCategory/updateHelpCategory', '', 'NORMAL', '2022-08-15 16:12:43', '2022-08-15 16:12:43', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000010004', '100100001', 'deleteHelpCategory', '删除', '', '/sysAdmin/helpCategory/deleteHelpCategoryByCategoryId', '', 'NORMAL', '2022-08-15 16:13:03', '2022-08-15 16:13:03', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100100002', '10010', 'helpInfoAdmin', '帮助内容', 'documentation', '', 'duojuhe/help/info/index', 'NORMAL', '2022-08-15 16:04:01', '2022-08-15 16:04:12', 'NAVIGATION', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000020001', '100100002', 'queryHelpInfoList', '查询', '', '/sysAdmin/helpInfo/queryHelpInfoPageResList', '', 'NORMAL', '2022-08-15 16:13:46', '2022-08-15 16:13:46', 'BUTTON', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100100003', '10010', 'myHelpInfoAdmin', '我发布的', 'nested', '', 'duojuhe/help/info/myHelpIndex', 'NORMAL', '2022-08-15 16:07:41', '2022-08-15 16:07:56', 'NAVIGATION', '4', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000030001', '100100003', 'queryMyHelpInfoList', '查询', '', '/sysAdmin/helpInfo/queryMyHelpInfoPageResList', '', 'NORMAL', '2022-08-15 16:15:09', '2022-08-15 16:24:05', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000030002', '100100003', 'saveHelpInfo', '新增', '', '/sysAdmin/helpInfo/saveHelpInfo', '', 'NORMAL', '2022-08-15 16:15:34', '2022-08-15 16:15:34', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000030003', '100100003', 'updateHelpInfo', '修改', '', '/sysAdmin/helpInfo/updateHelpInfo', '', 'NORMAL', '2022-08-15 16:16:00', '2022-08-15 16:16:00', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000030004', '100100003', 'putRecycleHelpInfo', '回收站', '', '/sysAdmin/helpInfo/putRecycleHelpInfoByInfoId', '', 'NORMAL', '2022-08-15 16:23:45', '2022-08-15 16:23:45', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100100004', '10010', 'recycleHelpInfoAdmin', '回收站', 'bug', '', 'duojuhe/help/info/recycleIndex', 'NORMAL', '2022-08-15 16:08:42', '2022-08-15 16:08:42', 'NAVIGATION', '3', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000040001', '100100004', 'queryRecycleHelpInfoList', '查询', '', '/sysAdmin/helpInfo/queryRecycleHelpInfoPageResList', '', 'NORMAL', '2022-08-15 16:24:34', '2022-08-15 16:24:34', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000040002', '100100004', 'deleteHelpInfo', '删除', '', '/sysAdmin/helpInfo/deleteHelpInfoByInfoId', '', 'NORMAL', '2022-08-15 16:25:03', '2022-08-15 16:25:03', 'BUTTON', '4', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000040003', '100100004', 'recoveryRecycleHelpInfo', '恢复', '', '/sysAdmin/helpInfo/recoveryRecycleHelpInfoByInfoId', '', 'NORMAL', '2022-08-15 16:25:28', '2022-08-15 16:25:28', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100100005', '10010', 'helpQuickLinkAdmin', '快捷导航', 'link', '', 'duojuhe/help/quicklink/index', 'NORMAL', '2022-08-15 16:00:27', '2022-08-15 16:00:38', 'NAVIGATION', '2', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000050001', '100100005', 'queryHelpQuickLinkList', '查询', '', '/sysAdmin/helpQuickLink/queryHelpQuickLinkPageResList', '', 'NORMAL', '2022-08-15 16:26:10', '2022-08-15 16:26:10', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000050002', '100100005', 'saveHelpQuickLink', '新增', '', '/sysAdmin/helpQuickLink/saveHelpQuickLink', '', 'NORMAL', '2022-08-15 16:26:30', '2022-08-15 16:26:30', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000050003', '100100005', 'updateHelpQuickLink', '修改', '', '/sysAdmin/helpQuickLink/updateHelpQuickLink', '', 'NORMAL', '2022-08-15 16:26:47', '2022-08-15 16:26:47', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001000050004', '100100005', 'deleteHelpQuickLink', '删除', '', '/sysAdmin/helpQuickLink/deleteHelpQuickLinkByLinkId', '', 'NORMAL', '2022-08-15 16:27:23', '2022-08-15 16:27:23', 'BUTTON', '4', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('10011', '-1', 'platformTenantAdmin', '平台租户', 'list', '#', '', 'NORMAL', '2022-08-16 14:32:56', '2022-08-16 14:45:40', 'NAVIGATION', '999', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('10011005', '10011', 'systemTenantAdmin', '租户管理', 'peoples', '', 'duojuhe/system/tenant/index', 'NORMAL', '2022-07-12 16:22:37', '2022-07-12 16:22:37', 'NAVIGATION', '9999', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('10011005000001', '10011005', 'querySystemTenantList', '查询', '', '/sysAdmin/systemTenant/querySystemTenantPageResList', '', 'NORMAL', '2022-07-12 16:35:33', '2022-07-12 16:35:33', 'BUTTON', '13', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('10011005000002', '10011005', 'saveSystemTenant', '新增', '', '/sysAdmin/systemTenant/saveSystemTenant', '', 'NORMAL', '2022-07-12 16:35:58', '2022-07-12 16:35:58', 'BUTTON', '11', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('10011005000003', '10011005', 'updateSystemTenant', '修改', '', '/sysAdmin/systemTenant/updateSystemTenant', '', 'NORMAL', '2022-07-12 16:36:21', '2022-07-12 16:36:21', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('10011005000004', '10011005', 'updateSystemTenantStatus', '改变状态', '', '/sysAdmin/systemTenant/updateSystemTenantStatusByTenantId', '', 'NORMAL', '2022-07-12 16:38:02', '2022-07-12 16:38:12', 'BUTTON', '2', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('10011005000005', '10011005', 'resetSystemTenantUserPassword', '重置密码', '', '/sysAdmin/systemTenant/resetSystemTenantUserPassword', '', 'NORMAL', '2022-08-26 13:39:08', '2022-08-26 13:39:10', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('10012', '-1', 'productTrackingAdmin', '产品溯源', 'icon', '', '', 'NORMAL', '2022-08-17 09:57:58', '2022-08-17 16:23:41', 'NAVIGATION', '4888', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001200001', '10012', 'trackingCategoryAdmin', '流程类别', 'example', '', 'duojuhe/tracking/category/index', 'NORMAL', '2022-08-17 09:59:10', '2022-08-17 16:40:12', 'NAVIGATION', '10', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000100001', '1001200001', 'queryTrackingCategoryList', '查询', '', '/sysAdmin/trackingCategory/queryTrackingCategoryPageResList', '', 'NORMAL', '2022-08-17 17:39:27', '2022-08-17 17:39:27', 'BUTTON', '10', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000100002', '1001200001', 'saveTrackingCategory', '新增', '', '/sysAdmin/trackingCategory/saveTrackingCategory', '', 'NORMAL', '2022-08-17 17:39:46', '2022-08-17 17:39:46', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000100003', '1001200001', 'updateTrackingCategory', '修改', '', '/sysAdmin/trackingCategory/updateTrackingCategory', '', 'NORMAL', '2022-08-17 17:40:02', '2022-08-17 17:40:02', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000100004', '1001200001', 'deleteTrackingCategory', '删除', '', '/sysAdmin/trackingCategory/deleteTrackingCategoryByCategoryId', '', 'NORMAL', '2022-08-17 17:40:19', '2022-08-17 17:40:19', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001200002', '10012', 'trackingCodeQueryRecordAdmin', '防伪查询记录', 'search', '', 'duojuhe/tracking/queryrecord/index', 'NORMAL', '2022-08-17 17:17:55', '2022-08-17 17:17:55', 'NAVIGATION', '13', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000200001', '1001200002', 'queryTrackingQueryRecordList', '查询', '', '/sysAdmin/trackingQueryRecord/queryTrackingQueryRecordPageResList', '', 'NORMAL', '2022-08-18 17:02:21', '2022-08-18 17:02:21', 'BUTTON', '4', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000200002', '1001200002', 'exportTrackingQueryRecordList', '导出', '', '/sysAdmin/trackingQueryRecord/queryTrackingQueryRecordPageResList', '', 'NORMAL', '2022-08-18 17:03:20', '2022-08-18 17:03:26', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001200003', '10012', 'trackingProductAdmin', '溯源产品', 'swagger', '', 'duojuhe/tracking/product/index', 'NORMAL', '2022-08-17 15:00:19', '2022-08-17 15:00:25', 'NAVIGATION', '11', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000300001', '1001200003', 'queryTrackingProductList', '查询', '', '/sysAdmin/trackingProduct/queryTrackingProductPageResList', '', 'NORMAL', '2022-08-17 17:41:21', '2022-08-17 17:41:21', 'BUTTON', '10', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000300002', '1001200003', 'saveTrackingProduct', '新增', '', '/sysAdmin/trackingProduct/saveTrackingProduct', '', 'NORMAL', '2022-08-17 17:41:39', '2022-08-17 17:41:39', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000300003', '1001200003', 'updateTrackingProduct', '修改', '', '/sysAdmin/trackingProduct/updateTrackingProduct', '', 'NORMAL', '2022-08-17 17:41:56', '2022-08-17 17:41:56', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000300004', '1001200003', 'deleteTrackingProduct', '删除', '', '/sysAdmin/trackingProduct/deleteTrackingProductByProductId', '', 'NORMAL', '2022-08-17 17:42:15', '2022-08-17 17:42:15', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001200004', '10012', 'trackingProductSupplierAdmin', '产品供货商', 'international', '', 'duojuhe/tracking/productsupplier/index', 'NORMAL', '2022-08-17 10:00:30', '2022-08-17 17:28:22', 'NAVIGATION', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000400001', '1001200004', 'queryTrackingProductSupplierList', '查询', '', '/sysAdmin/trackingProductSupplier/queryTrackingProductSupplierPageResList', '', 'NORMAL', '2022-08-17 17:34:53', '2022-08-17 17:34:53', 'BUTTON', '10', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000400002', '1001200004', 'saveTrackingProductSupplier', '新增', '', '/sysAdmin/trackingProductSupplier/saveTrackingProductSupplier', '', 'NORMAL', '2022-08-17 17:35:35', '2022-08-17 17:35:35', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000400003', '1001200004', 'updateTrackingProductSupplier', '修改', '', '/sysAdmin/trackingProductSupplier/updateTrackingProductSupplier', '', 'NORMAL', '2022-08-17 17:35:53', '2022-08-17 17:35:53', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000400004', '1001200004', 'deleteTrackingProductSupplier', '删除', '', '/sysAdmin/trackingProductSupplier/deleteTrackingProductSupplierBySupplierId', '', 'NORMAL', '2022-08-17 17:36:13', '2022-08-17 17:36:13', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001200005', '10012', 'trackingProductCategoryAdmin', '产品分类', 'documentation', '', 'duojuhe/tracking/productcategory/index', 'NORMAL', '2022-08-17 16:22:50', '2022-08-17 16:40:29', 'NAVIGATION', '13', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000500001', '1001200005', 'queryTrackingProductCategoryList', '查询', '', '/sysAdmin/trackingProductCategory/queryTrackingProductCategoryPageResList', '', 'NORMAL', '2022-08-17 17:37:04', '2022-08-17 17:37:04', 'BUTTON', '10', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000500002', '1001200005', 'saveTrackingProductCategory', '新增', '', '/sysAdmin/trackingProductCategory/saveTrackingProductCategory', '', 'NORMAL', '2022-08-17 17:38:10', '2022-08-17 17:38:10', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000500003', '1001200005', 'updateTrackingProductCategory', '修改', '', '/sysAdmin/trackingProductCategory/updateTrackingProductCategory', '', 'NORMAL', '2022-08-17 17:38:29', '2022-08-17 17:38:29', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000500004', '1001200005', 'deleteTrackingProductCategory', '删除', '', '/sysAdmin/trackingProductCategory/deleteTrackingProductCategoryByCategoryId', '', 'NORMAL', '2022-08-17 17:38:47', '2022-08-17 17:38:47', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001200006', '10012', 'trackingProcessItemAdmin', '溯源流程', 'druid', '', 'duojuhe/tracking/processitem/index', 'NORMAL', '2022-08-17 16:30:08', '2022-08-17 17:50:43', 'NAVIGATION', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000600001', '1001200006', 'queryTrackingProcessItemList', '查询', '', '/sysAdmin/trackingProcessItem/queryTrackingProcessItemPageResList', '', 'NORMAL', '2022-08-26 16:41:39', '2022-08-26 16:41:39', 'BUTTON', '10', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000600002', '1001200006', 'batchSaveTrackingProcessItem', '新增', '', '/sysAdmin/trackingProcessItem/batchSaveTrackingProcessItem', '', 'NORMAL', '2022-08-26 16:42:28', '2022-08-26 16:42:28', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000600003', '1001200006', 'updateTrackingProcessItem', '修改', '', '/sysAdmin/trackingProcessItem/updateTrackingProcessItem', '', 'NORMAL', '2022-08-26 16:42:58', '2022-08-26 16:43:15', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000600004', '1001200006', 'deleteTrackingProcessItem', '删除', '', '/sysAdmin/trackingProcessItem/deleteTrackingProcessItemByItemId', '', 'NORMAL', '2022-08-26 16:43:41', '2022-08-26 16:43:41', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001200007', '10012', 'trackingCodeAdmin', '防伪码', 'lock', '', 'duojuhe/tracking/trackingcode/index', 'NORMAL', '2022-08-17 17:11:55', '2022-08-17 17:28:00', 'NAVIGATION', '17', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000700001', '1001200007', 'queryTrackingCodeList', '查询', '', '/sysAdmin/trackingCode/queryTrackingCodePageResList', '', 'NORMAL', '2022-08-29 17:28:03', '2022-08-29 17:28:03', 'BUTTON', '11', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000700002', '1001200007', 'saveTrackingCode', '新增', '', '/sysAdmin/trackingCode/saveTrackingCode', '', 'NORMAL', '2022-08-29 17:29:12', '2022-08-29 17:29:12', 'BUTTON', '10', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000700003', '1001200007', 'batchSaveTrackingCode', '批量新增', '', '/sysAdmin/trackingCode/batchSaveTrackingCode', '', 'NORMAL', '2022-08-29 17:28:50', '2022-08-29 17:28:50', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000700004', '1001200007', 'updateTrackingCode', '修改', '', '/sysAdmin/trackingCode/updateTrackingCode', '', 'NORMAL', '2022-08-29 17:29:38', '2022-08-29 17:29:38', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100120000700005', '1001200007', 'deleteTrackingCode', '删除', '', '/sysAdmin/trackingCode/deleteTrackingCodeByTrackingCodeId', '', 'NORMAL', '2022-08-29 17:30:01', '2022-08-29 17:30:01', 'BUTTON', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('10013', '-1', 'noticeAdmin', '通知公告', 'tiplingdang', '#', '', 'NORMAL', '2022-09-06 16:44:17', '2022-09-06 16:44:17', 'NAVIGATION', '4445', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001300001', '10013', 'noticeCategoryAdmin', '公告分类', 'cascader', '', 'duojuhe/notice/category/index', 'NORMAL', '2022-09-06 16:45:52', '2022-09-06 16:46:10', 'NAVIGATION', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('10013000010001', '1001300001', 'queryNoticeCategoryList', '查询', '', '/sysAdmin/noticeCategory/queryNoticeCategoryPageResList', '', 'NORMAL', '2022-09-06 16:47:14', '2022-09-06 16:48:41', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('10013000010002', '1001300001', 'saveNoticeCategory', '新增', '', '/sysAdmin/noticeCategory/saveNoticeCategory', '', 'NORMAL', '2022-09-06 16:48:35', '2022-09-06 16:48:35', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('10013000010003', '1001300001', 'updateNoticeCategory', '修改', '', '/sysAdmin/noticeCategory/updateNoticeCategory', '', 'NORMAL', '2022-09-06 16:49:01', '2022-09-06 16:49:01', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('10013000010004', '1001300001', 'deleteNoticeCategory', '删除', '', '/sysAdmin/noticeCategory/deleteNoticeCategoryByCategoryId', '', 'NORMAL', '2022-09-06 16:49:23', '2022-09-06 16:49:23', 'BUTTON', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001300002', '10013', 'noticeInfoAdmin', '公告中心', 'documentation', '', 'duojuhe/notice/noticeinfo/index', 'NORMAL', '2022-09-06 17:07:21', '2022-09-06 17:09:39', 'NAVIGATION', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100130000200001', '1001300002', 'queryNoticeInfoList', '查询', '', '/sysAdmin/noticeInfo/queryNoticeInfoPageResList', '', 'NORMAL', '2022-09-06 17:11:11', '2022-09-06 17:11:11', 'BUTTON', '4', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001300003', '10013', 'myNoticeInfoAdmin', '我发布的', 'edit', '', 'duojuhe/notice/noticeinfo/myNoticeIndex', 'NORMAL', '2022-09-06 17:12:25', '2022-09-06 17:12:39', 'NAVIGATION', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100130000300001', '1001300003', 'queryMyNoticeInfoList', '查询', '', '/sysAdmin/noticeInfo/queryMyNoticeInfoPageResList', '', 'NORMAL', '2022-09-06 17:30:45', '2022-09-06 17:30:45', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100130000300002', '1001300003', 'saveNoticeInfo', '新增', '', '/sysAdmin/noticeInfo/saveNoticeInfo', '', 'NORMAL', '2022-09-06 17:31:06', '2022-09-06 17:31:06', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100130000300003', '1001300003', 'updateNoticeInfo', '修改', '', '/sysAdmin/noticeInfo/updateNoticeInfo', '', 'NORMAL', '2022-09-06 17:31:27', '2022-09-06 17:31:27', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('100130000300004', '1001300003', 'deleteNoticeInfo', '删除', '', '/sysAdmin/noticeInfo/deleteNoticeInfoByNoticeId', '', 'NORMAL', '2022-09-06 17:31:48', '2022-09-06 17:31:48', 'BUTTON', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('10014', '-1', 'payAdmin', '支付中心', 'money', '', '', 'NORMAL', '2022-12-01 16:06:59', '2022-12-01 16:09:59', 'NAVIGATION', '1110', 'NO', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('1001400001', '10014', 'payChannelAdmin', '支付渠道', 'number', '', 'duojuhe/pay/channel/index', 'NORMAL', '2022-12-01 16:09:51', '2022-12-01 16:09:51', 'NAVIGATION', '13', 'NO', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('10014000010001', '1001400001', 'queryPayChannelList', '查询', '', '/sysAdmin/payChannel/queryPayChannelPageResList', '', 'NORMAL', '2022-12-01 16:11:07', '2022-12-01 16:11:07', 'BUTTON', '12', 'NO', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('10014000010002', '1001400001', 'savePayChannel', '新增', '', '/sysAdmin/payChannel/savePayChannel', '', 'NORMAL', '2022-12-01 16:13:20', '2022-12-01 16:13:20', 'BUTTON', '9', 'NO', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('10014000010003', '1001400001', 'updatePayChannel', '修改', '', '/sysAdmin/payChannel/updatePayChannel', '', 'NORMAL', '2022-12-01 16:13:42', '2022-12-01 16:13:42', 'BUTTON', '8', 'NO', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('10014000010004', '1001400001', 'deletePayChannel', '删除', '', '/sysAdmin/payChannel/deletePayChannelByChannelId', '', 'NORMAL', '2022-12-01 16:14:09', '2022-12-01 16:14:09', 'BUTTON', '7', 'NO', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('20000', '-1', 'systemAdmin', '系统管理', 'documentation', '#', '', 'NORMAL', '2021-05-13 21:01:11', '2022-04-14 15:40:44', 'NAVIGATION', '1', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('20000001', '20000', 'systemMenuAdmin', '菜单管理', 'dict', '#', 'duojuhe/system/menu/index', 'NORMAL', '2021-05-13 10:47:35', '2021-11-19 14:33:05', 'NAVIGATION', '1', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('200000010001', '20000001', 'querySystemMenuList', '查询', 'yhgl', '/sysAdmin/systemMenu/queryAllSystemMenuListTree', '', 'NORMAL', '2021-05-13 14:48:42', '2022-04-13 15:10:42', 'BUTTON', '5', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('200000010002', '20000001', 'saveSystemMenu', '新增', 'yhgl', '/sysAdmin/systemMenu/saveSystemMenu', '', 'NORMAL', '2021-05-13 14:48:45', '2022-04-13 15:10:36', 'BUTTON', '2', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('200000010003', '20000001', 'updateSystemMenu', '修改', 'yhgl', '/sysAdmin/systemMenu/updateSystemMenu', '', 'NORMAL', '2021-05-13 14:48:46', null, 'BUTTON', '0', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('200000010004', '20000001', 'deleteSystemMenu', '删除', 'yhgl', '/sysAdmin/systemMenu/deleteSystemMenuByMenuId', '', 'NORMAL', '2021-05-13 14:48:41', null, 'BUTTON', '0', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('20000002', '20000', 'systemDictAdmin', '数据字典', 'skill', '#', 'duojuhe/system/dict/index', 'NORMAL', '2021-05-14 04:43:41', '2022-01-07 17:21:51', 'NAVIGATION', '1', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('200000020001', '20000002', 'querySystemDictList', '查询', 'yhgl', '/sysAdmin/systemDict/queryOneLevelSystemDictPageResList', '', 'NORMAL', '2021-05-13 14:48:35', '2022-01-24 15:03:28', 'BUTTON', '5', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('200000020002', '20000002', 'saveSystemDict', '新增', 'yhgl', '/sysAdmin/systemDict/saveSystemDict', '', 'NORMAL', '2021-05-13 14:48:37', '2022-01-24 15:03:22', 'BUTTON', '2', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('200000020003', '20000002', 'updateSystemDict', '修改', 'yhgl', '/sysAdmin/systemDict/updateSystemDict', '', 'NORMAL', '2021-05-13 14:48:39', null, 'BUTTON', '0', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('200000020004', '20000002', 'deleteSystemDict', '删除', 'yhgl', '/sysAdmin/systemDict/deleteSystemDictByDictId', '', 'NORMAL', '2021-05-13 14:48:33', null, 'BUTTON', '0', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('200000020005', '20000002', 'resetSystemDictCache', '刷新缓存', '', '/sysAdmin/systemDict/resetSystemDictCache', '', 'NORMAL', '2022-01-24 17:57:39', '2022-01-24 17:57:39', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('20000003', '10000', 'systemRoleAdmin', '角色管理', 'validCode', '#', 'duojuhe/system/role/index', 'NORMAL', '2021-05-14 04:52:02', '2021-12-31 08:58:48', 'NAVIGATION', '1', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('200000030001', '20000003', 'querySystemRoleList', '查询', 'yhgl', '/sysAdmin/systemRole/querySystemRolePageResList', '', 'NORMAL', '2021-05-13 14:48:57', null, 'BUTTON', '0', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('200000030002', '20000003', 'saveSystemRole', '新增', 'yhgl', '/sysAdmin/systemRole/saveSystemRole', '', 'NORMAL', '2021-05-13 14:48:59', null, 'BUTTON', '0', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('200000030003', '20000003', 'updateSystemRole', '修改', 'yhgl', '/sysAdmin/systemRole/updateSystemRole', '', 'NORMAL', '2021-05-13 14:49:00', null, 'BUTTON', '0', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('200000030004', '20000003', 'deleteSystemRole', '删除', 'yhgl', '/sysAdmin/systemRole/deleteSystemRoleByRoleId', '', 'NORMAL', '2021-05-13 14:48:55', null, 'BUTTON', '0', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('200000030005', '20000003', 'saveRoleBindingMenu', '授权', 'yhgl', '/sysAdmin/systemRole/saveRoleBindingMenu,/sysAdmin/systemRole/querySystemMenuIdListByRoleId,/sysAdmin/systemRole/saveRoleBindingDataScope,/sysAdmin/systemRole/querySystemDeptIdListByRoleId', '', 'NORMAL', '2021-05-14 10:27:03', null, 'BUTTON', '0', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('200000030006', '20000003', 'exportSystemRoleList', '导出', 'yhgl', '', '', 'NORMAL', '2021-05-14 13:52:19', null, 'BUTTON', '0', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('20000004', '10000', 'systemUserAdmin', '用户管理', 'people', '#', 'duojuhe/system/user/index', 'NORMAL', '2021-05-18 02:01:44', '2021-12-31 08:57:47', 'NAVIGATION', '1', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('200000040001', '20000004', 'querySystemUserList', '查询', 'yhgl', '/sysAdmin/systemUser/querySystemUserPageResList', '', 'NORMAL', '2021-05-13 14:48:23', null, 'BUTTON', '0', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('200000040002', '20000004', 'saveSystemUser', '新增', 'yhgl', '/sysAdmin/systemUser/saveSystemUser', '', 'NORMAL', '2021-05-13 14:49:04', null, 'BUTTON', '0', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('200000040003', '20000004', 'updateSystemUser', '修改', 'yhgl', '/sysAdmin/systemUser/updateSystemUser', '', 'NORMAL', '2021-05-13 14:49:06', null, 'BUTTON', '0', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('200000040004', '20000004', 'deleteSystemUser', '删除', 'yhgl', '/sysAdmin/systemUser/deleteSystemUserByUserId', '', 'NORMAL', '2021-05-13 14:49:02', null, 'BUTTON', '0', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('200000040005', '20000004', 'exportSystemUserList', '导出', 'yhgl', '', '', 'NORMAL', '2021-05-14 13:52:15', null, 'BUTTON', '0', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('200000040006', '20000004', 'resetSystemUserPassword', '重置密码', 'yhgl', '/sysAdmin/systemUser/resetSystemUserPassword', '', 'NORMAL', '2022-01-18 17:26:25', null, 'BUTTON', '0', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('20000005', '20000', 'systemLogAdmin', '日志管理', 'log', '#', 'duojuhe/system/log/index', 'NORMAL', '2021-05-16 21:05:12', '2022-01-24 14:35:39', 'NAVIGATION', '1', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('200000050001', '20000005', 'querySystemLogList', '查询', 'yhgl', '/sysAdmin/systemLog/querySystemLogPageResList', '', 'NORMAL', '2021-05-14 13:52:26', null, 'BUTTON', '0', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('200000050002', '20000005', 'exportSystemLogList', '导出', 'yhgl', '/sysAdmin/systemLog/querySystemLogPageResList', '', 'NORMAL', '2021-05-14 13:52:27', '2022-01-07 14:12:20', 'BUTTON', '1', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('20000006', '20000', 'systemParameterAdmin', '系统参数', 'system', '#', 'duojuhe/system/parameter/index', 'NORMAL', '2021-05-18 04:47:39', '2021-12-31 09:00:57', 'NAVIGATION', '5', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('200000060001', '20000006', 'querySystemParameterList', '查询', 'yhgl', '/sysAdmin/systemParameter/querySystemParameterPageResList', '', 'NORMAL', '2021-05-17 10:38:30', '2022-01-24 17:54:48', 'BUTTON', '7', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('200000060002', '20000006', 'saveSystemParameter', '新增', 'yhgl', '/sysAdmin/systemParameter/saveSystemParameter', '', 'NORMAL', '2021-11-26 14:05:27', '2022-01-24 17:54:53', 'BUTTON', '5', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('200000060003', '20000006', 'updateSystemParameter', '修改', 'yhgl', '/sysAdmin/systemParameter/updateSystemParameter', '', 'NORMAL', '2021-05-17 10:38:32', '2022-01-24 17:54:57', 'BUTTON', '4', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('200000060004', '20000006', 'deleteSystemParameter', '删除', 'yhgl', '/sysAdmin/systemParameter/deleteSystemParameter', '', 'NORMAL', '2021-11-26 14:06:23', '2022-01-24 17:55:03', 'BUTTON', '3', 'YES', '', '', '', '', 'NO');
INSERT INTO `system_menu` VALUES ('200000060005', '20000006', 'resetSystemParameterCache', '刷新缓存', '', '/sysAdmin/systemParameter/resetSystemParameterCache', '', 'NORMAL', '2022-01-24 17:54:22', '2022-01-24 17:54:42', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('20000007', '20000', 'systemMessageAdmin', '系统消息', 'druid', '#', 'duojuhe/system/message/index', 'NORMAL', '2022-01-06 16:57:17', '2022-01-20 16:36:33', 'NAVIGATION', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('2000000700001', '20000007', 'querySystemMessageList', '查询', '', '/sysAdmin/systemMessage/querySystemMessagePageResList', '', 'NORMAL', '2022-01-06 16:58:29', '2022-01-06 16:58:29', 'BUTTON', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('2000000700002', '20000007', 'updateSystemMessage', '标记处理', '', '/sysAdmin/systemMessage/updateSystemMessageHandleStatusByMessageId,/sysAdmin/systemMessage/updateAllSystemMessageHandleStatus', '', 'NORMAL', '2022-01-06 17:00:03', '2022-01-06 17:00:03', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('20000008', '20000', 'apiDoc', '接口文档', 'documentation', '#', 'http://47.243.228.8:9070/doc.html', 'NORMAL', '2022-10-10 09:45:49', '2022-10-10 09:46:09', 'NAVIGATION', '6', 'YES', '', '', 'YES', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('30000', '-1', 'smsAdmin', '短信平台', 'email', '#', '', 'NORMAL', '2021-09-08 13:38:01', '2022-08-15 15:57:57', 'NAVIGATION', '1111', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('300000001', '30000', 'smsTemplateAdmin', '短信模板', 'date-range', '', 'duojuhe/sms/template/index', 'NORMAL', '2021-09-08 13:39:29', '2022-03-04 14:07:14', 'NAVIGATION', '12', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('3000000010002', '300000001', 'querySmsTemplateList', '查询', 'yhgl', '/sysAdmin/smsTemplate/querySmsTemplatePageResList', '', 'NORMAL', '2021-09-08 13:39:55', '2021-09-08 13:39:55', 'BUTTON', '11', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('3000000010003', '300000001', 'saveSmsTemplate', '新增', 'yhgl', '/sysAdmin/smsTemplate/saveSmsTemplate', '', 'NORMAL', '2021-09-08 13:40:24', '2021-09-08 13:40:24', 'BUTTON', '4', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('3000000010004', '300000001', 'updateSmsTemplate', '修改', 'yhgl', '/sysAdmin/smsTemplate/updateSmsTemplate', '', 'NORMAL', '2021-09-08 13:40:42', '2021-09-08 13:40:42', 'BUTTON', '3', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('3000000010005', '300000001', 'deleteSmsTemplate', '删除', 'yhgl', '/sysAdmin/smsTemplate/deleteSmsTemplateByTemplateId', '', 'NORMAL', '2021-09-08 13:41:02', '2021-09-08 13:41:02', 'BUTTON', '1', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('300000002', '30000', 'smsRecordAdmin', '发送记录', 'cascader', '', 'duojuhe/sms/record/index', 'NORMAL', '2021-09-08 13:38:44', '2022-01-07 13:45:11', 'NAVIGATION', '5', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('3000000020001', '300000002', 'querySmsRecordList', '查询', 'yhgl', '/sysAdmin/smsRecord/querySmsRecordPageResList', '', 'NORMAL', '2021-09-08 13:39:05', '2021-09-08 13:39:05', 'BUTTON', '10', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('3000000020002', '300000002', 'exportSmsRecordList', '导出', '', '/sysAdmin/smsRecord/querySmsRecordPageResList', '', 'NORMAL', '2022-01-07 14:11:59', '2022-01-07 14:12:28', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('300000003', '30000', 'smsChannelAdmin', '短信渠道', 'number', '', 'duojuhe/sms/channel/index', 'NORMAL', '2022-03-03 15:39:55', '2022-03-04 14:07:17', 'NAVIGATION', '13', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('3000000030001', '300000003', 'querySmsChannelList', '查询', '', '/sysAdmin/smsChannel/querySmsChannelPageResList', '', 'NORMAL', '2022-03-04 10:05:27', '2022-03-04 10:07:17', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('3000000030002', '300000003', 'saveSmsChannel', '新增', '', '/sysAdmin/smsChannel/saveSmsChannel', '', 'NORMAL', '2022-03-04 10:07:11', '2022-03-04 10:07:46', 'BUTTON', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('3000000030003', '300000003', 'updateSmsChannel', '修改', '', '/sysAdmin/smsChannel/updateSmsChannel', '', 'NORMAL', '2022-03-04 10:07:41', '2022-03-04 10:07:41', 'BUTTON', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('3000000030004', '300000003', 'deleteSmsChannel', '删除', '', '/sysAdmin/smsChannel/deleteSmsChannelByChannelId', '', 'NORMAL', '2022-03-04 10:08:13', '2022-03-04 10:08:13', 'BUTTON', '2', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('300000004', '30000', 'sendTaskAdmin', '发送任务', 'input', '', 'duojuhe/sms/sendtask/index', 'NORMAL', '2022-03-04 14:06:49', '2022-03-14 13:37:58', 'NAVIGATION', '11', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('3000000040001', '300000004', 'querySmsSendTaskList', '查询', '', '/sysAdmin/smsSendTask/querySmsSendTaskPageResList', '', 'NORMAL', '2022-03-14 15:07:17', '2022-03-14 15:07:17', 'BUTTON', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('3000000040002', '300000004', 'saveSmsSendTask', '新增', '', '/sysAdmin/smsSendTask/saveSmsSendTask', '', 'NORMAL', '2022-03-14 15:07:48', '2022-03-14 15:07:48', 'BUTTON', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('3000000040003', '300000004', 'updateSmsSendTask', '修改', '', '/sysAdmin/smsSendTask/updateSmsSendTask', '', 'NORMAL', '2022-03-14 15:08:11', '2022-03-14 15:08:11', 'BUTTON', '4', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('3000000040004', '300000004', 'deleteSmsSendTask', '删除', '', '/sysAdmin/smsSendTask/deleteSmsSendTaskBySendTaskId', '', 'NORMAL', '2022-03-14 15:08:40', '2022-03-14 15:08:40', 'BUTTON', '2', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('40000', '-1', 'imChatAdmin', '多聊Chat', 'guide', '#', '', 'NORMAL', '2021-12-23 14:50:37', '2022-01-20 16:30:29', 'NAVIGATION', '5555', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('400000001', '40000', 'myChatFriendsList', '我的好友', 'wechat', '', 'duojuhe/chat/im/contacts/friends', 'NORMAL', '2021-12-30 09:46:13', '2021-12-31 08:59:01', 'NAVIGATION', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('400000002', '40000', 'myChatFriendsApplyList', '好友申请', 'eye-open', '', 'duojuhe/chat/im/contacts/apply', 'NORMAL', '2021-12-30 09:43:15', '2021-12-30 14:00:11', 'NAVIGATION', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('400000003', '40000', 'myImChatList', '我的会话', 'message', '', 'duojuhe/chat/im/message/index', 'NORMAL', '2021-12-23 14:51:57', '2021-12-30 09:43:40', 'NAVIGATION', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('400000004', '40000', 'myChatGroupList', '我的群组', 'peoples', '', 'duojuhe/chat/im/contacts/groups', 'NORMAL', '2021-12-30 09:47:38', '2021-12-30 09:47:38', 'NAVIGATION', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('50000', '-1', 'formAdmin', '问卷调查', 'excel', '#', '', 'NORMAL', '2022-01-19 13:45:11', '2022-08-17 14:48:57', 'NAVIGATION', '7777', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('500000001', '50000', 'formProjectAdmin', '创建问卷', 'build', '', 'duojuhe/form/project/index', 'NORMAL', '2022-01-19 13:49:53', '2022-03-22 15:11:28', 'NAVIGATION', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('5000000010001', '500000001', 'queryFormProjectList', '查询', '', '/sysAdmin/formProject/queryFormProjectPageResList', '', 'NORMAL', '2022-04-29 09:41:43', '2022-04-29 09:41:43', 'BUTTON', '10', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('5000000010002', '500000001', 'saveFormProject', '新增', '', '/sysAdmin/formProject/saveFormProject,/sysAdmin/formProject/createFormProjectByTemplate', '', 'NORMAL', '2022-04-29 09:42:24', '2022-04-29 09:44:13', 'BUTTON', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('5000000010003', '500000001', 'updateFormProject', '修改', '', '/sysAdmin/formProject/updateFormProject,/sysAdmin/formProject/updateFormProjectStatusByProjectId', '', 'NORMAL', '2022-04-29 09:42:56', '2022-04-29 09:45:13', 'BUTTON', '2', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('5000000010004', '500000001', 'deleteFormProject', '删除', '', '/sysAdmin/formProject/deleteFormProjectByProjectId', '', 'NORMAL', '2022-04-29 09:45:08', '2022-04-29 09:45:08', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('500000002', '50000', 'formTemplateAdmin', '模板广场', 'example', '', 'duojuhe/form/template/index', 'NORMAL', '2022-01-19 13:48:30', '2022-03-17 14:30:40', 'NAVIGATION', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('5000000020001', '500000002', 'queryFormProjectTemplateList', '查询', '', '/sysAdmin/formProjectTemplate/queryFormProjectTemplatePageResList', '', 'NORMAL', '2022-04-29 09:45:51', '2022-04-29 09:45:51', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('5000000020002', '500000002', 'saveFormProjectTemplate', '新增', '', '/sysAdmin/formProjectTemplate/saveFormProjectTemplate,/sysAdmin/formProjectTemplate/saveFormProjectTransferTemplate', '', 'NORMAL', '2022-04-29 09:46:35', '2022-04-29 09:46:35', 'BUTTON', '4', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('5000000020003', '500000002', 'updateFormProjectTemplate', '修改', '', '/sysAdmin/formProjectTemplate/updateFormProjectTemplate', '', 'NORMAL', '2022-04-29 09:47:00', '2022-04-29 09:47:00', 'BUTTON', '3', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('5000000020004', '500000002', 'deleteFormProjectTemplate', '删除', '', '/sysAdmin/formProjectTemplate/deleteFormProjectTemplateByProjectId', '', 'NORMAL', '2022-04-29 09:47:25', '2022-04-29 09:47:25', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('60000', '-1', 'systemMonitorAdmin', '系统监控', 'international', '#', '', 'NORMAL', '2022-01-24 13:43:35', '2022-01-24 13:44:26', 'NAVIGATION', '3', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('600000001', '60000', 'serverMonitorAdmin', '服务监控', 'server', '', 'duojuhe/monitor/server/index', 'NORMAL', '2022-01-25 10:29:13', '2022-01-25 10:29:13', 'NAVIGATION', '1', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('60000000100001', '600000001', 'queryMonitorServer', '查询', '', '/sysAdmin/monitorServer/queryServerRes', '', 'NORMAL', '2022-01-25 10:30:22', '2022-01-25 10:30:46', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('600000002', '60000', 'monitorUserOnlineAdmin', '在线用户', 'online', '', 'duojuhe/monitor/online/user/index', 'NORMAL', '2022-01-25 14:24:00', '2022-04-14 15:41:08', 'NAVIGATION', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('60000000200001', '600000002', 'queryMonitorUserOnlineList', '查询', '', '/sysAdmin/monitorOnline/queryMonitorUserOnlinePageResList', '', 'NORMAL', '2022-01-25 14:26:01', '2022-01-25 14:27:41', 'BUTTON', '3', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('60000000200002', '600000002', 'forceKickOutOnlineUser', '强踢', '', '/sysAdmin/monitorOnline/forceKickOutOnlineUserByOnlineId', '', 'NORMAL', '2022-01-25 14:27:08', '2022-01-25 14:27:52', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('600000003', '60000', 'quartzJobAdmin', '定时任务', 'time', '', 'duojuhe/quartz/job/index', 'NORMAL', '2022-02-16 11:21:26', '2022-04-14 15:41:12', 'NAVIGATION', '3', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('6000000030001', '600000003', 'queryQuartzJobList', '查询', '', '/sysAdmin/quartzJob/queryQuartzJobResList', '', 'NORMAL', '2022-02-16 11:24:32', '2022-02-16 11:25:03', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('6000000030002', '600000003', 'updateQuartzJob', '修改', '', '/sysAdmin/quartzJob/updateQuartzJob', '', 'NORMAL', '2022-02-16 11:25:52', '2022-02-16 11:25:52', 'BUTTON', '6', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('6000000030003', '600000003', 'saveQuartzJob', '新增', '', '/sysAdmin/quartzJob/saveQuartzJob', '', 'NORMAL', '2022-02-16 11:25:28', '2022-02-16 11:25:28', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('6000000030004', '600000003', 'deleteQuartzJob', '删除', '', '/sysAdmin/quartzJob/deleteQuartzJobByJobId', '', 'NORMAL', '2022-02-16 11:26:21', '2022-02-16 11:26:21', 'BUTTON', '5', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('6000000030005', '600000003', 'resumeQuartzJob', '恢复', '', '/sysAdmin/quartzJob/resumeQuartzJobByJobId', '', 'NORMAL', '2022-02-16 16:04:28', '2022-02-16 16:04:28', 'BUTTON', '3', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('6000000030006', '600000003', 'runOnceJob', '执行一次', '', '/sysAdmin/quartzJob/runOnceJobByJobId', '', 'NORMAL', '2022-02-16 16:05:06', '2022-02-16 16:05:06', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('6000000030007', '600000003', 'pauseQuartzJob', '暂停', '', '/sysAdmin/quartzJob/pauseQuartzJobByJobId', '', 'NORMAL', '2022-02-16 16:03:55', '2022-02-16 16:03:55', 'BUTTON', '4', 'YES', '', '', 'NO', 'NO', 'NO');
INSERT INTO `system_menu` VALUES ('70000', '-1', 'infoAdmin', '信息管理', 'clipboard', '#', '', 'NORMAL', '2022-03-21 16:46:01', '2022-07-11 17:11:56', 'NAVIGATION', '4444', 'YES', '', '', '', '', 'YES');
INSERT INTO `system_menu` VALUES ('700000001', '70000', 'infoCategoryAdmin', '信息分类', 'build', '', 'duojuhe/info/category/index', 'NORMAL', '2022-04-13 14:47:34', '2022-04-13 15:21:24', 'NAVIGATION', '3', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('70000000100001', '700000001', 'queryInfoCategoryList', '查询', '', '/sysAdmin/infoCategory/queryInfoCategoryTreeResList', '', 'NORMAL', '2022-04-13 15:11:40', '2022-04-13 15:11:40', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('70000000100002', '700000001', 'saveInfoCategory', '新增', '', '/sysAdmin/infoCategory/saveInfoCategory', '', 'NORMAL', '2022-04-13 15:12:15', '2022-04-13 15:12:15', 'BUTTON', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('70000000100003', '700000001', 'updateInfoCategory', '修改', '', '/sysAdmin/infoCategory/updateInfoCategory', '', 'NORMAL', '2022-04-13 15:12:41', '2022-04-13 15:12:41', 'BUTTON', '4', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('70000000100004', '700000001', 'deleteInfoCategory', '删除', '', '/sysAdmin/infoCategory/deleteInfoCategoryByCategoryId', '', 'NORMAL', '2022-04-13 15:13:10', '2022-04-13 15:13:10', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('700000002', '70000', 'infoNewsAdmin', '新闻资讯', 'form', '', 'duojuhe/info/news/index', 'NORMAL', '2022-04-13 15:21:19', '2022-07-11 17:11:48', 'NAVIGATION', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('70000000200001', '700000002', 'queryInfoNewsList', '查询', '', '/sysAdmin/infoNews/queryInfoNewsPageResList', '', 'NORMAL', '2022-04-14 11:13:41', '2022-04-14 11:13:41', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('700000003', '70000', 'recycleInfoNewsAdmin', '回收站', 'bug', '', 'duojuhe/info/news/recycleIndex', 'NORMAL', '2022-04-14 11:17:20', '2022-07-01 16:56:57', 'NAVIGATION', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('70000000300001', '700000003', 'queryRecycleInfoNewsList', '查询', '', '/sysAdmin/infoNews/queryRecycleInfoNewsPageResList', '', 'NORMAL', '2022-04-14 15:26:50', '2022-04-14 15:26:57', 'BUTTON', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('70000000300002', '700000003', 'recoveryRecycleInfoNews', '恢复', '', '/sysAdmin/infoNews/recoveryRecycleInfoNewsByNewsId', '', 'NORMAL', '2022-04-14 15:27:57', '2022-04-14 15:27:57', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('70000000300003', '700000003', 'deleteInfoNews', '删除', '', '/sysAdmin/infoNews/deleteInfoNewsByNewsId', '', 'NORMAL', '2022-04-14 15:27:28', '2022-04-14 15:27:28', 'BUTTON', '2', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('700000004', '70000', 'myInfoNewsAdmin', '我发布的', 'row', '', 'duojuhe/info/news/myNewsIndex', 'NORMAL', '2022-07-11 17:13:09', '2022-07-11 17:13:48', 'NAVIGATION', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('70000000400001', '700000004', 'queryMyInfoNewsList', '查询', '', '/sysAdmin/infoNews/queryMyInfoNewsPageResList', '', 'NORMAL', '2022-07-11 17:17:35', '2022-07-11 17:17:35', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('70000000400002', '700000004', 'saveInfoNews', '新增', '', '/sysAdmin/infoNews/saveInfoNews', '', 'NORMAL', '2022-04-14 11:14:15', '2022-07-11 17:15:26', 'BUTTON', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('70000000400003', '700000004', 'updateInfoNews', '修改', '', '/sysAdmin/infoNews/updateInfoNews', '', 'NORMAL', '2022-04-14 11:14:38', '2022-07-11 17:15:40', 'BUTTON', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('70000000400004', '700000004', 'putRecycleInfoNews', '回收站', '', '/sysAdmin/infoNews/putRecycleInfoNewsByNewsId', '', 'NORMAL', '2022-04-14 11:15:43', '2022-07-11 17:15:46', 'BUTTON', '2', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('80000', '-1', 'topologicalGraphAdmin', '网络拓扑图', 'tree', '#', '', 'NORMAL', '2022-04-15 15:28:59', '2022-04-15 15:32:17', 'NAVIGATION', '2222', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('8000010001', '80000', 'elementClassAdmin', '图元分类', 'note-book', '', 'duojuhe/topo/elementclass/index', 'NORMAL', '2022-04-15 15:30:11', '2022-04-15 15:30:11', 'NAVIGATION', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('800001000100001', '8000010001', 'queryTopoElementClassList', '查询', '', '/sysAdmin/topo/elementClass/queryTopoElementClassPageResList', '', 'NORMAL', '2022-04-15 15:34:38', '2022-04-15 15:34:38', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('800001000100002', '8000010001', 'saveTopoElementClass', '新增', '', '/sysAdmin/topo/elementClass/saveTopoElementClass', '', 'NORMAL', '2022-04-15 15:35:04', '2022-04-15 15:35:04', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('800001000100003', '8000010001', 'updateTopoElementClass', '修改', '', '/sysAdmin/topo/elementClass/updateTopoElementClass', '', 'NORMAL', '2022-04-15 15:35:33', '2022-04-15 15:35:33', 'BUTTON', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('800001000100004', '8000010001', 'deleteTopoElementClass', '删除', '', '/sysAdmin/topo/elementClass/deleteTopoElementClassByClassId', '', 'NORMAL', '2022-04-15 15:35:59', '2022-04-15 15:35:59', 'BUTTON', '3', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('8000010002', '80000', 'elementInfoAdmin', '图元信息', 'icon', '', 'duojuhe/topo/elementinfo/index', 'NORMAL', '2022-04-15 15:37:53', '2022-04-15 15:37:53', 'NAVIGATION', '3', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('800001000200001', '8000010002', 'queryTopoElementInfoList', '查询', '', '/sysAdmin/topo/elementInfo/queryTopoElementInfoPageResList', '', 'NORMAL', '2022-04-15 15:38:29', '2022-04-15 15:38:29', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('800001000200002', '8000010002', 'saveTopoElementInfo', '新增', '', '/sysAdmin/topo/elementInfo/saveTopoElementInfo', '', 'NORMAL', '2022-04-15 15:40:03', '2022-04-15 15:40:03', 'BUTTON', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('800001000200003', '8000010002', 'updateTopoElementInfo', '修改', '', '/sysAdmin/topo/elementInfo/updateTopoElementInfo', '', 'NORMAL', '2022-04-15 15:40:30', '2022-04-15 15:40:30', 'BUTTON', '4', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('800001000200004', '8000010002', 'deleteTopoElementInfo', '删除', '', '/sysAdmin/topo/elementInfo/deleteTopoElementInfoByElementId', '', 'NORMAL', '2022-04-15 15:40:55', '2022-04-15 15:40:55', 'BUTTON', '3', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('8000010003', '80000', 'topologyAdmin', '我的拓扑图', 'tree-table', '', 'duojuhe/topo/topology/index', 'NORMAL', '2022-04-15 17:00:35', '2022-04-15 17:00:35', 'NAVIGATION', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('800001000300001', '8000010003', 'queryTopoTopologyList', '查询', '', '/sysAdmin/topo/topology/queryTopoTopologyPageResList', '', 'NORMAL', '2022-04-15 17:01:02', '2022-04-15 17:01:02', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('800001000300002', '8000010003', 'saveTopoTopology', '新增', '', '/sysAdmin/topo/topology/saveTopoTopology', '', 'NORMAL', '2022-04-15 17:01:22', '2022-04-15 17:01:22', 'BUTTON', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('800001000300003', '8000010003', 'updateTopoTopology', '修改', '', '/sysAdmin/topo/topology/updateTopoTopology', '', 'NORMAL', '2022-04-15 17:01:46', '2022-04-15 17:01:46', 'BUTTON', '4', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('800001000300004', '8000010003', 'deleteTopoTopology', '删除', '', '/sysAdmin/topo/topology/deleteTopoTopologyByTopoId', '', 'NORMAL', '2022-04-15 17:02:09', '2022-04-15 17:02:09', 'BUTTON', '3', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000', '-1', 'workflowAdmin', '工作流程', 'tree-table', '#', '', 'NORMAL', '2022-04-28 14:50:12', '2022-05-20 19:00:12', 'NAVIGATION', '6666', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('9000000001', '90000', 'workflowModelAdmin', '流程模型', 'form', '', 'duojuhe/workflow/workflowModel', 'NORMAL', '2022-04-28 14:51:50', '2022-04-28 14:51:50', 'NAVIGATION', '10', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000010001', '9000000001', 'queryWorkflowProjectList', '查询', '', '/sysAdmin/workflowProject/queryWorkflowProjectPageResList', '', 'NORMAL', '2022-04-29 10:15:49', '2022-04-29 10:15:49', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000010002', '9000000001', 'releaseWorkflowProject', '发布', '', '/sysAdmin/workflowProject/releaseWorkflowProject', '', 'NORMAL', '2022-04-29 10:18:25', '2022-04-29 10:18:25', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000010003', '9000000001', 'saveWorkflowProject', '新增', '', '/sysAdmin/workflowProject/saveWorkflowProject', '', 'NORMAL', '2022-04-29 10:16:22', '2022-08-15 20:21:57', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000010004', '9000000001', 'invalidWorkflowProject', '弃用', '', '/sysAdmin/workflowProject/invalidWorkflowProjectByProjectId', '', 'NORMAL', '2022-04-29 10:17:56', '2022-04-29 10:17:56', 'BUTTON', '3', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000010005', '9000000001', 'deleteWorkflowProject', '删除', '', '/sysAdmin/workflowProject/deleteWorkflowProjectByProjectId', '', 'NORMAL', '2022-04-29 10:17:21', '2022-04-29 10:17:21', 'BUTTON', '3', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000010006', '9000000001', 'updateWorkflowProject', '修改', '', '/sysAdmin/workflowProject/updateWorkflowProject', '', 'NORMAL', '2022-04-29 10:16:50', '2022-08-15 20:23:17', 'BUTTON', '4', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('9000000002', '90000', 'toDoWorkflowAdmin', '我待办的', 'question', '', 'duojuhe/workflow/toDoWorkflow', 'NORMAL', '2022-04-28 14:55:44', '2022-05-17 16:46:23', 'NAVIGATION', '4', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000020001', '9000000002', 'queryToDoWorkflowRecordList', '查询', '', '/sysAdmin/workflowRecord/queryToDoWorkflowRecordPageResList', '', 'NORMAL', '2022-06-01 16:19:41', '2022-06-01 16:19:41', 'BUTTON', '8', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000020002', '9000000002', 'claimWorkflowTask', '认领签收', '', 'sysAdmin/workflowRecord/claimWorkflowTask', '', 'NORMAL', '2022-06-01 16:21:03', '2022-06-01 16:22:35', 'BUTTON', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('9000000003', '90000', 'myWorkflowAdmin', '我发起的', 'swagger', '', 'duojuhe/workflow/myWorkflow', 'NORMAL', '2022-04-28 14:52:41', '2022-08-18 09:29:24', 'NAVIGATION', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('900000000300001', '9000000003', 'queryMyWorkflowRecordList', '查询', '', '/sysAdmin/workflowRecord/queryMyWorkflowRecordPageResList', '', 'NORMAL', '2022-06-01 15:55:26', '2022-06-01 15:58:10', 'BUTTON', '10', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('900000000300002', '9000000003', 'saveWorkflowRecord', '新增', '', '/sysAdmin/workflowRecord/saveWorkflowRecord', '', 'NORMAL', '2022-06-01 15:56:56', '2022-06-01 15:58:16', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('900000000300003', '9000000003', 'submitWorkflowRecord', '提交', '', '/sysAdmin/workflowRecord/submitWorkflowRecord', '', 'NORMAL', '2022-06-01 15:59:01', '2022-06-01 15:59:01', 'BUTTON', '3', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('900000000300004', '9000000003', 'deleteWorkflowRecord', '删除', '', '/sysAdmin/workflowRecord/deleteWorkflowRecordByRecordId', '', 'NORMAL', '2022-06-01 15:58:03', '2022-06-01 15:58:28', 'BUTTON', '6', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('900000000300005', '9000000003', 'cancelWorkflowApply', '取消', '', '/sysAdmin/workflowRecord/cancelWorkflowApplyByRecordId', '', 'NORMAL', '2022-06-01 16:03:23', '2022-06-01 16:03:23', 'BUTTON', '2', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('900000000300006', '9000000003', 'updateWorkflowRecord', '修改', '', '/sysAdmin/workflowRecord/updateWorkflowRecord', '', 'NORMAL', '2022-06-01 15:57:39', '2022-06-01 15:58:22', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('9000000004', '90000', 'inProcessWorkflowAdmin', '我在办的', 'edit', '', 'duojuhe/workflow/inProcessWorkflow', 'NORMAL', '2022-05-09 15:12:09', '2022-05-17 16:46:18', 'NAVIGATION', '5', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000040001', '9000000004', 'queryInProcessWorkflowRecordList', '查询', '', '/sysAdmin/workflowRecord/queryInProcessWorkflowRecordPageResList', '', 'NORMAL', '2022-06-01 16:38:51', '2022-06-01 16:40:41', 'BUTTON', '15', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000040002', '9000000004', 'completeWorkflowTask', '办理', '', '/sysAdmin/workflowRecord/completeWorkflowTask', '', 'NORMAL', '2022-06-01 16:40:21', '2022-06-01 16:40:21', 'BUTTON', '12', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000040003', '9000000004', 'delegateWorkflowTask', '委办', '', '/sysAdmin/workflowRecord/delegateWorkflowTask', '', 'NORMAL', '2022-06-01 16:41:44', '2022-06-01 16:41:44', 'BUTTON', '7', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000040004', '9000000004', 'transferWorkflowTask', '转办', '', '/sysAdmin/workflowRecord/transferWorkflowTask', '', 'NORMAL', '2022-06-01 16:41:11', '2022-06-01 16:41:11', 'BUTTON', '9', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000040005', '9000000004', 'stopWorkflowTask', '终止', '', '/sysAdmin/workflowRecord/stopWorkflowTask', '', 'NORMAL', '2022-06-10 11:29:15', '2022-06-10 11:29:15', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('9000000005', '90000', 'finishedWorkflowAdmin', '我参与的', 'note-book', '', 'duojuhe/workflow/finishedWorkflow', 'NORMAL', '2022-04-28 14:57:07', '2022-06-07 15:08:14', 'NAVIGATION', '3', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000050001', '9000000005', 'queryFinishedWorkflowRecordList', '查询', '', '/sysAdmin/workflowRecord/queryFinishedWorkflowRecordPageResList', '', 'NORMAL', '2022-06-01 16:16:24', '2022-06-01 16:16:24', 'BUTTON', '10', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('9000000006', '90000', 'copyWorkflowAdmin', '抄送我的', 'logininfor', '', 'duojuhe/workflow/copyWorkflow', 'NORMAL', '2022-06-07 15:09:04', '2022-06-07 15:09:04', 'NAVIGATION', '1', 'YES', '', '', 'NO', 'NO', 'YES');
INSERT INTO `system_menu` VALUES ('90000000060001', '9000000006', 'queryCopyWorkflowRecordList', '查询', '', '/sysAdmin/workflowRecord/queryCopyWorkflowRecordPageResList', '', 'NORMAL', '2022-06-07 15:09:50', '2022-06-07 15:09:50', 'BUTTON', '1', 'YES', '', '', 'NO', 'NO', 'YES');

-- ----------------------------
-- Table structure for system_message
-- ----------------------------
DROP TABLE IF EXISTS `system_message`;
CREATE TABLE `system_message` (
  `message_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `message_type_code` varchar(50) DEFAULT '' COMMENT '消息类型，取数据字典MESSAGE_TYPE',
  `message_title` varchar(255) DEFAULT '' COMMENT '消息标题',
  `message_content` text COMMENT '消息内容',
  `handle_status_code` varchar(50) DEFAULT '' COMMENT '处理状态取数据字典HANDLE_STATUS',
  `handle_user_id` varchar(32) DEFAULT '' COMMENT '处理人id',
  `handle_time` datetime DEFAULT NULL COMMENT '处理时间',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id归属人',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户id',
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统通知';

-- ----------------------------
-- Records of system_message
-- ----------------------------

-- ----------------------------
-- Table structure for system_parameter
-- ----------------------------
DROP TABLE IF EXISTS `system_parameter`;
CREATE TABLE `system_parameter` (
  `parameter_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `parameter_code` varchar(100) NOT NULL DEFAULT '' COMMENT '参数编码',
  `parameter_name` varchar(100) NOT NULL DEFAULT '' COMMENT '参数名称',
  `parameter_value` varchar(100) DEFAULT '' COMMENT '参数值',
  `description` varchar(255) DEFAULT '' COMMENT '参数描述',
  `built_in` varchar(50) DEFAULT 'NO' COMMENT '是否是内置:YES是，NO否',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`parameter_id`),
  UNIQUE KEY `uni_parameter_code` (`parameter_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统参数表';

-- ----------------------------
-- Records of system_parameter
-- ----------------------------

-- ----------------------------
-- Table structure for system_post
-- ----------------------------
DROP TABLE IF EXISTS `system_post`;
CREATE TABLE `system_post` (
  `post_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `post_code` varchar(50) NOT NULL DEFAULT '' COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL DEFAULT '' COMMENT '岗位名称',
  `status_code` varchar(50) DEFAULT '' COMMENT '状态：FORBID禁用 NORMAL正常STATUS',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '修改人id',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `remark` varchar(250) DEFAULT '' COMMENT '备注',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户ID',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统岗位表';

-- ----------------------------
-- Records of system_post
-- ----------------------------
INSERT INTO `system_post` VALUES ('20220101100000000', 'YUNYING', '运营', 'NORMAL', '20220101100000000', '2022-06-26 15:47:12', '20220101100000000', '2022-07-27 10:40:38', '20220101100000000', '1', '', '20220101100000000');

-- ----------------------------
-- Table structure for system_role
-- ----------------------------
DROP TABLE IF EXISTS `system_role`;
CREATE TABLE `system_role` (
  `role_id` varchar(32) NOT NULL DEFAULT '' COMMENT '角色id',
  `role_name` varchar(50) NOT NULL DEFAULT '' COMMENT '角色名称',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户Id',
  `create_time` datetime DEFAULT NULL COMMENT '角色创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '修改用户id',
  `update_time` datetime DEFAULT NULL COMMENT '角色更新时间',
  `remark` varchar(100) DEFAULT '' COMMENT '备注',
  `status_code` varchar(50) DEFAULT '' COMMENT '状态，FORBID禁用 NORMAL正常',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `data_scope_code` varchar(50) DEFAULT '' COMMENT 'ALL_DATA=所有数据权限, CUSTOM_DATA=自定义数据权限,SELF_DEPT=本部门数据权限,SELF_DEPT_SUBORDINATE=本部门及以下数据权限,SELF_DATA=自己的数据',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户ID',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限角色表';

-- ----------------------------
-- Records of system_role
-- ----------------------------

-- ----------------------------
-- Table structure for system_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `system_role_dept`;
CREATE TABLE `system_role_dept` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `role_id` varchar(32) NOT NULL DEFAULT '' COMMENT '角色id',
  `dept_id` varchar(32) NOT NULL DEFAULT '' COMMENT '部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '所属租户id',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_dept_id` (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和部门的关系表';

-- ----------------------------
-- Records of system_role_dept
-- ----------------------------

-- ----------------------------
-- Table structure for system_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `system_role_menu`;
CREATE TABLE `system_role_menu` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `role_id` varchar(32) NOT NULL DEFAULT '' COMMENT '角色id',
  `menu_id` varchar(32) NOT NULL DEFAULT '' COMMENT '菜单id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '所属租户id',
  PRIMARY KEY (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_menu_id` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色菜单关系表';

-- ----------------------------
-- Records of system_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for system_safe_referer
-- ----------------------------
DROP TABLE IF EXISTS `system_safe_referer`;
CREATE TABLE `system_safe_referer` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `allow_referer` varchar(255) DEFAULT '' COMMENT '允许请求的域名或者ip',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '修改人id',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户ID',
  `built_in` varchar(50) DEFAULT '' COMMENT '是否是内置:YES是，NO否',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_allow_referer` (`allow_referer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统安全白名单';

-- ----------------------------
-- Records of system_safe_referer
-- ----------------------------

-- ----------------------------
-- Table structure for system_tenant
-- ----------------------------
DROP TABLE IF EXISTS `system_tenant`;
CREATE TABLE `system_tenant` (
  `tenant_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `tenant_number` varchar(32) NOT NULL DEFAULT '' COMMENT '租户编码，具有唯一性',
  `tenant_name` varchar(255) NOT NULL DEFAULT '' COMMENT '租户名称,具有唯一性',
  `tenant_abbreviation` varchar(255) DEFAULT '' COMMENT '租户简称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `status_code` varchar(50) DEFAULT '' COMMENT '状态，FORBID禁用 NORMAL正常',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '最后更新用户',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户',
  `built_in` varchar(50) DEFAULT '' COMMENT '是否是内置:YES是，NO否',
  `sm4_key` varchar(255) DEFAULT '' COMMENT '国密4密钥',
  PRIMARY KEY (`tenant_id`),
  UNIQUE KEY `uni_tenant_name` (`tenant_name`),
  UNIQUE KEY `uni_tenant_number` (`tenant_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统租户表';

-- ----------------------------
-- Records of system_tenant
-- ----------------------------
INSERT INTO `system_tenant` VALUES ('20220101100000000', '202210000', '多聚合运营平台', '多聚合', '2022-06-26 09:32:08', '2022-06-26 09:32:14', 'NORMAL', '1', '1', 'YES', '');

-- ----------------------------
-- Table structure for system_tenant_menu
-- ----------------------------
DROP TABLE IF EXISTS `system_tenant_menu`;
CREATE TABLE `system_tenant_menu` (
  `id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `menu_id` varchar(32) NOT NULL DEFAULT '' COMMENT '菜单id',
  `tenant_id` varchar(32) NOT NULL DEFAULT '' COMMENT '租户id',
  PRIMARY KEY (`id`),
  KEY `idx_tenant_id` (`tenant_id`),
  KEY `idx_menu_id` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='租户功能菜单关系表';

-- ----------------------------
-- Records of system_tenant_menu
-- ----------------------------

-- ----------------------------
-- Table structure for system_user
-- ----------------------------
DROP TABLE IF EXISTS `system_user`;
CREATE TABLE `system_user` (
  `user_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键字段',
  `module_code` varchar(50) NOT NULL DEFAULT '' COMMENT '用户所属模块编码',
  `user_type_code` varchar(50) NOT NULL DEFAULT '' COMMENT '用户类型DEPT_USER部门用户,TENANT_USER租户标识,SUPER_ADMIN超管',
  `login_name` varchar(32) NOT NULL DEFAULT '' COMMENT '登录名，具有唯一性',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `real_name` varchar(32) DEFAULT '' COMMENT '真实姓名',
  `mobile_number` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号码，具有唯一性',
  `user_number` varchar(32) NOT NULL DEFAULT '' COMMENT '用户编号，具有唯一性',
  `head_portrait` varchar(255) DEFAULT '' COMMENT '头像',
  `role_id` varchar(32) DEFAULT '' COMMENT '角色ID',
  `post_id` varchar(32) DEFAULT '' COMMENT '岗位id',
  `status_code` varchar(50) DEFAULT '' COMMENT '状态，FORBID禁用 NORMAL正常',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '最后更新用户',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建用户',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门ID',
  `tenant_id` varchar(32) NOT NULL DEFAULT '' COMMENT '租户ID',
  `gender_code` varchar(50) DEFAULT '' COMMENT '性别，取数据字典',
  `motto` varchar(255) DEFAULT '' COMMENT '座右铭',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `uni_login_name` (`login_name`),
  UNIQUE KEY `uni_mobile_number` (`mobile_number`),
  UNIQUE KEY `uni_user_number` (`user_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统管理人员';

-- ----------------------------
-- Records of system_user
-- ----------------------------
INSERT INTO `system_user` VALUES ('20220101100000000', 'SYSTEM_MODULE', 'SUPER_ADMIN', 'super', '14e1b600b1fd579f47433b88e8d85291', '超管', '13987654321', 'super', 'http://image.duojuhe.com/upload/file/headPortrait/202210000/super/2022-06-28/0d24045f1616419ba3c62f7a3911eb34_200x200.jpeg', '-1', '20220101100000000', 'NORMAL', '2021-05-13 10:21:06', '2021-05-18 04:01:23', '20220101100000000', '20220101100000000', '20220101100000000', '20220101100000000', 'MALE', '一起走');

-- ----------------------------
-- Table structure for topo_element_class
-- ----------------------------
DROP TABLE IF EXISTS `topo_element_class`;
CREATE TABLE `topo_element_class` (
  `class_id` varchar(32) NOT NULL DEFAULT '' COMMENT '分类主键',
  `class_name` varchar(20) NOT NULL DEFAULT '' COMMENT '分类名称',
  `status_code` varchar(50) NOT NULL DEFAULT '' COMMENT '状态，FORBID禁用 NORMAL正常',
  `expand_flag` int(1) NOT NULL DEFAULT '0' COMMENT '是否展开，0=否1=是',
  `sort` int(5) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户ID',
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='拓扑图元分类表';

-- ----------------------------
-- Records of topo_element_class
-- ----------------------------

-- ----------------------------
-- Table structure for topo_element_info
-- ----------------------------
DROP TABLE IF EXISTS `topo_element_info`;
CREATE TABLE `topo_element_info` (
  `element_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `element_title` varchar(50) NOT NULL DEFAULT '' COMMENT '图元名称',
  `class_id` varchar(32) NOT NULL DEFAULT '' COMMENT '图元归属分类',
  `sort` int(5) NOT NULL DEFAULT '0' COMMENT '排序',
  `status_code` varchar(50) NOT NULL DEFAULT '' COMMENT '状态，FORBID禁用 NORMAL正常',
  `bind_device` int(1) NOT NULL DEFAULT '0' COMMENT '允许绑定设备，0不允许 1允许',
  `edit_link` int(1) NOT NULL DEFAULT '0' COMMENT '允许编辑链接，0不允许 1允许',
  `element_img` varchar(255) NOT NULL DEFAULT '' COMMENT '图元小图标',
  `img_height` int(5) NOT NULL COMMENT '图片高度',
  `img_width` int(5) NOT NULL COMMENT '图片宽度',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户ID',
  PRIMARY KEY (`element_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='拓扑图元表';

-- ----------------------------
-- Records of topo_element_info
-- ----------------------------

-- ----------------------------
-- Table structure for topo_topology
-- ----------------------------
DROP TABLE IF EXISTS `topo_topology`;
CREATE TABLE `topo_topology` (
  `topo_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `topo_name` varchar(50) NOT NULL DEFAULT '' COMMENT '拓扑图名称',
  `cover_img` longtext COMMENT '拓扑图封面',
  `xml_data` longtext COMMENT '拓扑xml内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT '' COMMENT '备注说明',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '租户ID',
  PRIMARY KEY (`topo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='拓扑图表';

-- ----------------------------
-- Records of topo_topology
-- ----------------------------

-- ----------------------------
-- Table structure for tracking_category
-- ----------------------------
DROP TABLE IF EXISTS `tracking_category`;
CREATE TABLE `tracking_category` (
  `category_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `category_name` varchar(30) NOT NULL DEFAULT '' COMMENT '分类名称',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '所属租户id',
  `status_code` varchar(50) DEFAULT '' COMMENT '分类状态，启用，禁用',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='溯源流程类别';

-- ----------------------------
-- Records of tracking_category
-- ----------------------------

-- ----------------------------
-- Table structure for tracking_code
-- ----------------------------
DROP TABLE IF EXISTS `tracking_code`;
CREATE TABLE `tracking_code` (
  `tracking_code_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `tracking_number` varchar(32) NOT NULL DEFAULT '' COMMENT '防伪编号',
  `tracking_code_url` varchar(255) DEFAULT '' COMMENT '防伪码地址',
  `tracking_code` varchar(100) NOT NULL DEFAULT '' COMMENT '防伪码',
  `tracking_batch` varchar(100) NOT NULL DEFAULT '' COMMENT '批次',
  `transport_code` varchar(100) NOT NULL DEFAULT '' COMMENT '物流码',
  `product_id` varchar(32) NOT NULL DEFAULT '' COMMENT '所属产品',
  `supplier_id` varchar(32) NOT NULL DEFAULT '' COMMENT '所属供货商',
  `status_code` varchar(50) DEFAULT '' COMMENT '防伪码状态',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '所属租户id',
  PRIMARY KEY (`tracking_code_id`),
  UNIQUE KEY `uni_tracking_number` (`tracking_number`),
  UNIQUE KEY `uni_tracking_code` (`tracking_code`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='溯源防伪码表';

-- ----------------------------
-- Records of tracking_code
-- ----------------------------

-- ----------------------------
-- Table structure for tracking_process_item
-- ----------------------------
DROP TABLE IF EXISTS `tracking_process_item`;
CREATE TABLE `tracking_process_item` (
  `item_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `transport_code` varchar(100) NOT NULL DEFAULT '' COMMENT '物流码',
  `tracking_category_id` varchar(32) DEFAULT '' COMMENT '所属分类',
  `status_code` varchar(50) NOT NULL DEFAULT '' COMMENT '流程状态启用禁用',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间id',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '所属租户id',
  `description` varchar(255) DEFAULT '' COMMENT '流程描述',
  `operator_name` varchar(30) DEFAULT '' COMMENT '操作人员',
  `operation_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='溯源流程项';

-- ----------------------------
-- Records of tracking_process_item
-- ----------------------------

-- ----------------------------
-- Table structure for tracking_product
-- ----------------------------
DROP TABLE IF EXISTS `tracking_product`;
CREATE TABLE `tracking_product` (
  `product_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `product_category_id` varchar(32) DEFAULT '' COMMENT '产品分类',
  `product_number` varchar(32) NOT NULL DEFAULT '' COMMENT '产品编号',
  `product_name` varchar(255) NOT NULL DEFAULT '' COMMENT '产品名称',
  `product_img` longtext COMMENT '产品图片',
  `product_barcode` varchar(255) DEFAULT '' COMMENT '条形码',
  `product_code` varchar(255) DEFAULT '' COMMENT '产品编码',
  `status_code` varchar(50) DEFAULT '' COMMENT '产品状态',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `description` longtext COMMENT '产品描述',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '所属租户id',
  `sort` int(8) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `uni_product_number` (`product_number`),
  UNIQUE KEY `uni_product_code_tenant_id` (`product_code`,`tenant_id`),
  UNIQUE KEY `uni_product_barcode_tenant_id` (`product_barcode`,`tenant_id`),
  UNIQUE KEY `uni_product_name_tenant_id` (`product_name`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='溯源产品记录表';

-- ----------------------------
-- Records of tracking_product
-- ----------------------------

-- ----------------------------
-- Table structure for tracking_product_category
-- ----------------------------
DROP TABLE IF EXISTS `tracking_product_category`;
CREATE TABLE `tracking_product_category` (
  `category_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `category_name` varchar(30) NOT NULL DEFAULT '' COMMENT '分类名称',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '所属租户id',
  `status_code` varchar(50) DEFAULT '' COMMENT '分类状态，启用，禁用',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='溯源产品分类表';

-- ----------------------------
-- Records of tracking_product_category
-- ----------------------------

-- ----------------------------
-- Table structure for tracking_product_supplier
-- ----------------------------
DROP TABLE IF EXISTS `tracking_product_supplier`;
CREATE TABLE `tracking_product_supplier` (
  `supplier_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键id',
  `supplier_name` varchar(255) NOT NULL DEFAULT '' COMMENT '供货商名称',
  `supplier_number` varchar(32) NOT NULL DEFAULT '' COMMENT '供货商编号',
  `supplier_code` varchar(50) NOT NULL DEFAULT '' COMMENT '供货商编码',
  `mobile_number` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号码',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '所属租户id',
  `sort` int(5) DEFAULT '0' COMMENT '排序',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `description` longtext COMMENT '供货商描述说明',
  `status_code` varchar(50) DEFAULT '' COMMENT '供货商状态',
  PRIMARY KEY (`supplier_id`),
  UNIQUE KEY `uni_supplier_number` (`supplier_number`),
  UNIQUE KEY `uni_supplier_name_tenant_id` (`supplier_name`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='溯源产品供货商';

-- ----------------------------
-- Records of tracking_product_supplier
-- ----------------------------

-- ----------------------------
-- Table structure for tracking_query_record
-- ----------------------------
DROP TABLE IF EXISTS `tracking_query_record`;
CREATE TABLE `tracking_query_record` (
  `record_id` varchar(32) NOT NULL DEFAULT '' COMMENT '主键',
  `query_number` varchar(32) NOT NULL DEFAULT '' COMMENT '查询编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` varchar(32) DEFAULT '' COMMENT '创建人id',
  `create_dept_id` varchar(32) DEFAULT '' COMMENT '创建部门id',
  `tenant_id` varchar(32) DEFAULT '' COMMENT '所属租户id',
  `update_user_id` varchar(32) DEFAULT '' COMMENT '更新人id',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `query_ip` varchar(50) DEFAULT '' COMMENT '查询IP',
  `query_ua` varchar(500) DEFAULT '' COMMENT '查询浏览器相关信息',
  `tracking_code_id` varchar(32) DEFAULT '' COMMENT '防伪码id',
  `tracking_number` varchar(32) DEFAULT '' COMMENT '防伪码编号',
  `tracking_code` varchar(100) DEFAULT '' COMMENT '防伪码',
  PRIMARY KEY (`record_id`),
  UNIQUE KEY `uni_query_number` (`query_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='溯源防伪码查询记录';

-- ----------------------------
-- Records of tracking_query_record
-- ----------------------------
