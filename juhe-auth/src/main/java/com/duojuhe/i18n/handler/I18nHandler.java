package com.duojuhe.i18n.handler;

import javax.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class I18nHandler {
    @Resource
    private LocaleMessage localeMessage;

    /**
     * 获取key
     *
     * @param key
     * @return
     */
    public  String getKey(String key) {
        return localeMessage.getMessage(key);
    }

    /**
     * 获取指定哪个配置文件下的key
     *
     * @param key
     * @param local
     * @return
     */
    public  String getKey(String key, Locale local) {
        return localeMessage.getMessage(key, local);
    }

}
