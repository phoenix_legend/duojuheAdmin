package com.duojuhe.i18n;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

@Slf4j
public class I18nUtils {
    //请求头标识
    public static final String LANG_HEADER = "lang";

    /**
     * 获得请求头中的语言标签
     * @param request
     * @return
     */
    public static String getLangByRequest(HttpServletRequest request){
        //优先去请求头中获取lang值
        String headerSign = request.getHeader(LANG_HEADER);
        if (StringUtils.isBlank(headerSign)){
            return request.getParameter(LANG_HEADER);
        }else{
            return headerSign;
        }
    }
}
