package com.duojuhe.security.xss;

import com.duojuhe.common.constant.SingleStringConstant;
import com.duojuhe.common.constant.SystemConstants;
import com.duojuhe.common.utils.idgenerator.UUIDUtils;
import com.duojuhe.common.utils.jwt.JwtUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 防止XSS攻击的过滤器
 */
public class XssFilter implements Filter {

    /**
     * 排除链接
     */
    private List<String> excludes = new ArrayList<>();
    /**
     * 过期时间，单位分钟
     */
    private String expireMinute;
    /**
     * xss过滤开关
     */
    private boolean enabled = false;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //排除拦截路径
        String tempExcludes = "";
        //是否开启拦截
        String tempEnabled = filterConfig.getInitParameter("enabled");
        //过期分钟
        expireMinute = filterConfig.getInitParameter("tokenExpiresIn");
        //校验
        if (StringUtils.isNotEmpty(tempExcludes)) {
            String[] url = tempExcludes.split(SingleStringConstant.COMMA);
            excludes.addAll(Arrays.asList(url));
        }
        if (StringUtils.isNotEmpty(tempEnabled)) {
            enabled = Boolean.parseBoolean(tempEnabled);
        }
    }


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        //跨域设置,谁来都放行,与设置成*效果相同,但是这里设置成*行不通,因此用该方法代替
        resp.setHeader(SystemConstants.DJH_SAFE_KEY, duoJuHeComKey(Long.parseLong(expireMinute)));
        //自定义头的key
        resp.setHeader("Access-Control-Expose-Headers", SystemConstants.DJH_SAFE_KEY);
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Credentials", "true");
        resp.setHeader("Access-Control-Allow-Methods", "*");
        resp.setHeader("Access-Control-Max-Age", "3600");
        resp.setHeader("Access-Control-Allow-Headers", "authorization,Authorization,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,x_requested_with,If-Modified-Since,Cache-Control,Content-Type,djhSafeKey,token,sign,timestamp");
        String contentType = request.getContentType();
        if (handleExcludeURL(req, resp) || null != contentType && contentType.startsWith("multipart")) {
            chain.doFilter(request, response);
            return;
        }
        XssHttpServletRequestWrapper xssRequest = new XssHttpServletRequestWrapper((HttpServletRequest) request, true);
        chain.doFilter(xssRequest, response);
    }


    /**
     * 生成多聚合验证token
     *
     * @return
     */
    private static String duoJuHeComKey(long expireMinute) {
        //用户自定义信息claims
        Map<String, Object> claims = new HashMap<>();
        claims.put("duoJuHeComKey", UUIDUtils.getUUID32());
        return JwtUtils.generate(claims,expireMinute);
    }

    /**
     * 校验地址
     *
     * @param request
     * @param response
     * @return
     */
    private boolean handleExcludeURL(HttpServletRequest request, HttpServletResponse response) {
        if (!enabled) {
            return true;
        }
        if (excludes == null || excludes.isEmpty()) {
            return false;
        }
        String url = request.getServletPath();
        for (String pattern : excludes) {
            Pattern p = Pattern.compile("^" + pattern);
            Matcher m = p.matcher(url);
            if (m.find()) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void destroy() {

    }

}