package com.duojuhe.security.shiro.resources.impl;

import com.duojuhe.security.shiro.resources.ResourcesService;
import com.duojuhe.coremodule.system.entity.SystemMenu;
import com.duojuhe.coremodule.system.mapper.SystemMenuMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * @Date:2018/11/8
 **/
@Service("resourcesService")
public class ResourcesServiceImpl implements ResourcesService {
    @Resource
    private SystemMenuMapper systemMenuMapper;
    @Override
    public List<SystemMenu> querySystemMenuAll() {
        return systemMenuMapper.selectAll();
    }
}
