package com.duojuhe.security.shiro.resources;


import com.duojuhe.coremodule.system.entity.SystemMenu;

import java.util.List;

/**
 * @Date:2018/11/8
 **/
public interface ResourcesService {
    /**
     * 查询所有菜单资源配置
     * @return
     */
    List<SystemMenu> querySystemMenuAll();
}
