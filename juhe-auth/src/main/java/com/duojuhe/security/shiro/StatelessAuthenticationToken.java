package com.duojuhe.security.shiro;

import com.duojuhe.common.utils.token.TokenUtils;
import org.apache.shiro.authc.AuthenticationToken;

import java.util.HashMap;
import java.util.Map;

public class StatelessAuthenticationToken implements AuthenticationToken {
    // 登录来源标识
    public static final String LOGIN_RESOURCE = "loginResource";

    // 用户登录后获取的令牌；
    private String token;
    // 用户登录来源；
    private String loginResource;

    public StatelessAuthenticationToken(String token,String loginResource) {
        this.token = token;
        this.loginResource = loginResource;
    }

    @Override
    public Object getPrincipal() {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put(TokenUtils.TOKEN_HEADER,this.token);
        map.put(LOGIN_RESOURCE,this.loginResource);
        return map;
    }

    @Override
    public Object getCredentials() {
        return this.token;
    }
}
