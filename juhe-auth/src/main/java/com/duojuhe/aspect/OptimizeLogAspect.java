package com.duojuhe.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class OptimizeLogAspect {
    @Pointcut("execution(public * com.duojuhe..*controller..*.*(..))")
    public void logPointCut() {

    }

    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        //开始时间
        long startTime = System.currentTimeMillis();
        //执行方法
        Object result = point.proceed();
        MethodSignature signature = (MethodSignature) point.getSignature();
        //请求的方法名
        String methodName = signature.getName();
        //结束时间
        long endTime = System.currentTimeMillis();
        log.info("【接口执行时间】接口名：{},执行开始时间：{}，执行结束时间：{}，总耗时:{}ms",methodName,startTime,endTime,(endTime-startTime));
        return result;
    }
}
