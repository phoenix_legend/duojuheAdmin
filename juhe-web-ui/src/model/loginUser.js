export { LoginUser }

/**
 * 用户登录后信息对象
 */
class LoginUser {
    token;
    signKey;
    apiDomainName;
    constructor(data, apiDomainName) {
      this.setDataValue(data, apiDomainName)
    }
    setDataValue(data, apiDomainName) {
      if (!data) {
        return
      }
      this.apiDomainName = apiDomainName
      this.token = data.token
      this.signKey = data.signKey
      //this.headPortrait = data.headPortrait!==''?data.headPortrait:require('@/assets/images/profile.jpg')
    }
}
