import Vue from 'vue'

import Cookies from 'js-cookie'
import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Element from 'element-ui'
import './assets/styles/element-variables.scss'

import '@/assets/styles/index.scss' // global css
import '@/assets/styles/duojuhe.scss' // duojuhe css
import App from './App'
import store from './store'
import router from './router'
import directive from './directive' //directive
import plugins from './plugins' // plugins

// 引入自定义全局css
import '@/assets/styles/chat/global.less'

import '@/assets/styles/font_icon.css'

import './core/lazy-use'
import './core/global-component'
import './core/filter'
import './core/directives'

import './assets/icons' // icon
import './permission' // permission control
import { parseTime, resetForm, addDateRange, handleTree, buttonDisabled, noPermission,vLoading,orderByFormat } from "@/utils/duojuhe";
import { checkPermi } from "@/utils/permission";
// 全局组件自动注册
import '@/components/DuoJuHe/Form/autoRegister'
// 表单项目分页组件
import FormPagination from "@/components/DuoJuHe/Form/form/pagination";
// 分页组件
import Pagination from "@/components/Pagination";
// 自定义表格工具组件
import RightToolbar from "@/components/RightToolbar"
// 文件上传组件
import FileUpload from "@/components/FileUpload"
// 图片上传组件
import ImageUpload from "@/components/ImageUpload"
// 头部标签组件
import VueMeta from 'vue-meta'
//剪切
import Clipboard from 'v-clipboard'


// 正则验证组件
import formValidate from './utils/validateForm'

import func from './utils/workflow/preload.js'
import nodeWrap from '@/components/DuoJuHe/workflow/nodeWrap'
import addNode from '@/components/DuoJuHe/workflow/addNode'
import nodeWrapView from '@/components/DuoJuHe/workflow/nodeWrapView'

Vue.prototype.$func = func;


// 全局方法挂载
Vue.prototype.$vLoading = vLoading
Vue.prototype.orderByFormat = orderByFormat
Vue.prototype.parseTime = parseTime
Vue.prototype.buttonDisabled = buttonDisabled
Vue.prototype.resetForm = resetForm
Vue.prototype.noPermission = noPermission
Vue.prototype.addDateRange = addDateRange
Vue.prototype.handleTree = handleTree
Vue.prototype.formValidate = formValidate
Vue.prototype.checkPermi = checkPermi
// 全局组件挂载
Vue.component('FormPagination', FormPagination)
Vue.component('Pagination', Pagination)
Vue.component('RightToolbar', RightToolbar)
Vue.component('FileUpload', FileUpload)
Vue.component('ImageUpload', ImageUpload)
Vue.component('nodeWrap', nodeWrap); //初始化组件
Vue.component('addNode', addNode); //初始化组件
Vue.component('nodeWrapView', nodeWrapView); //初始化组件

Vue.use(directive)
Vue.use(plugins)
Vue.use(VueMeta)
Vue.use(Clipboard)

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */

Vue.use(Element, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

