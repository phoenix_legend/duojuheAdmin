/**
 *  处理项目数据
 *
 */
import _ from 'lodash'
import {
    assistComponents,
    personalInfoComponents,
    selectComponents,
    layoutComponents
} from '@/components/DuoJuHe/Form/generator/config'


// 深拷贝对象
export function deepClone(obj) {
  const _toString = Object.prototype.toString
  // null, undefined, non-object, function
  if (!obj || typeof obj !== 'object') {
    return obj
  }
  // DOM Node
  if (obj.nodeType && 'cloneNode' in obj) {
    return obj.cloneNode(true)
  }
  // Date
  if (_toString.call(obj) === '[object Date]') {
    return new Date(obj.getTime())
  }
  // RegExp
  if (_toString.call(obj) === '[object RegExp]') {
    const flags = []
    if (obj.global) { flags.push('g') }
    if (obj.multiline) { flags.push('m') }
    if (obj.ignoreCase) { flags.push('i') }
    return new RegExp(obj.source, flags.join(''))
  }
  const result = Array.isArray(obj) ? [] : obj.constructor ? new obj.constructor() : {}
  for (const key in obj) {
    result[key] = deepClone(obj[key])
  }
  return result
}

/**
 * 表单json转换为后台需要的对象
 * @param item
 */
export function formItemConvertData(item, projectId) {
    let data = {
        'itemConfig':item.__config__,
        'type': item.typeId,
        'formItemId': item.__config__.formId,
        'formItemField': item.__vModel__,
        'label': item.__config__.label,
        'defaultValue': item.__config__.defaultValue,
        'required': item.__config__.required,
        'placeholder': item.placeholder,
        'regList': item.__config__.regList,
        'showLabel': item.__config__.showLabel,
        'showRegList': item.__config__.showRegList,
        'span': item.__config__.span,
        'displayType': item.__config__.displayType,
        'projectId': projectId
    }
    let expand = {}
    let param = dataParams[item.typeId]
    if (param) {
        Object.keys(param).forEach(key => {
            let value = _.get(item, param[key])
            _.set(expand, key, value)
        })
        _.set(data, 'expand', expand)
    }
    return data
}

// 类型关系map
let typeMap = new Map()

/**
 * 后台存储的数据转换为elementui表单需要的Json
 * @param data
 */
export function dbDataConvertForItemJson(data) {
    let {required, placeholder} = data
    if (required && !placeholder) { // 必填项目验证未填默认提示语
        data.placeholder = '此项为必填/必选项目'
    }
    if (!typeMap.size > 0) {
        // 根据类型获取默认数据
        _.concat(selectComponents, assistComponents, personalInfoComponents, layoutComponents).forEach(item => {
            typeMap.set(item.typeId, item)
        })
    }
    const defaultJsonItem = typeMap.get(data.type)
    let jsonItem = _.cloneDeep(defaultJsonItem)
    let param = dataParams[data.type]
    if (param) {
        Object.keys(param).forEach(key => {
            let value = _.get(data.expand, key)
            _.set(jsonItem, param[key], value)
        })
    }
    if (data.itemConfig) {
      jsonItem.__config__ = JSON.parse(data.itemConfig)
    }

    jsonItem.expand = data.expand
    jsonItem.dId = data.id
    jsonItem.sort = data.sort
    jsonItem.typeId = data.type
    jsonItem.displayType = data.displayType
    jsonItem.__config__.span = data.span
    jsonItem.__config__.formId = data.formItemId
    jsonItem.__config__.label = data.label
    jsonItem.__config__.required = data.required
    jsonItem.__config__.regList = data.regList
    jsonItem.__config__.showRegList = data.showRegList
    jsonItem.__config__.showLabel = data.showLabel
    if (data.defaultValue) {
        if (data.defaultValue.json) {
            jsonItem.__config__.defaultValue = JSON.parse(data.defaultValue.value)
        } else {
            jsonItem.__config__.defaultValue = data.defaultValue.value
        }
    }
    // 不同项目地址区分 动态修改上传地址
    if (jsonItem.action) {
        jsonItem.action = jsonItem.action+"?projectId=" + data.projectId
    }
    jsonItem.regList = data.regList
    jsonItem.showRegList = data.showRegList
    jsonItem.placeholder = data.placeholder
    jsonItem.formItemId = data.formItemId
    jsonItem.__vModel__ = 'field' + data.formItemId
    return jsonItem
}

// 分页显示数据处理
export function pageShowHandle(allFields) {
  // 判断是否存在分页
  let index = allFields.findIndex(item => {
    return item.typeId === 'FORM_PAGINATION'
  })
  if (index < 0) {
    return
  }
  let curr = 1
  // 每页字段
  let perPageFields = {}
  // 分页字段数据
  let pageFields = {}
  allFields.forEach(item => {
    let fields = _.get(perPageFields, curr)
    if (item.typeId === 'FORM_PAGINATION') {
      _.set(pageFields, curr, item)
      if (fields) {
        item.currPageNum = curr++
      }
    } else {
      if (!fields || fields == undefined) {
        fields = new Array()
      }
      fields.push(item)
      _.set(perPageFields, curr, fields)
    }
  })
  let len = _.keys(perPageFields).length
  // 计算页数 添加分页
  _.keys(perPageFields).forEach(key => {
    let pageItem = _.get(pageFields, key)
    if (pageItem) {
      let fields = _.get(perPageFields, key)
      pageItem.totalPageNum = len
      fields.push(pageItem)
      _.set(perPageFields, key, fields)
    }else{
      // 特殊处理最后一页 如果没有主动拖分页 则默认添加一个
      let defaultPageItem = deepClone(_.get(pageFields, 1));
      const fields = _.get(perPageFields, key);
      defaultPageItem.totalPageNum = len;
      defaultPageItem.currPageNum = len;
      fields.push(defaultPageItem);
      _.set(perPageFields, key, fields);
    }

  })
  return perPageFields
}


/**
 * 检查itemType是否需要加编号
 * @param itemType
 */
const itemTypeArr = ["FORM_PAGINATION","IMAGE","IMAGE_CAROUSEL","DESC_TEXT","ROW_CONTAINER","DIVIDER"];
export function checkAddNumberByItemType(itemType) {
  return itemTypeArr.indexOf(itemType)===-1
}


/**
 *
 * @type {{'1': {maxlength: string, prepend: string, append: string}}}
 *
 */
const dataParams = {
    // 单行文本
    'INPUT': {
        'prepend': '__slot__.prepend',
        'prefixIcon': 'prefix-icon',
        'suffixIcon': 'suffix-icon',
        'maxlength': 'maxlength',
        'readonly': 'readonly',
        'disabled': 'disabled',
        'showWordLimit': 'show-word-limit',
        'append': '__slot__.append'
    },
    // 多行文本
    'TEXTAREA': {
        'minRows': 'autosize.minRows',
        'maxRows': 'autosize.maxRows',
        'maxlength': 'maxlength',
        'readonly': 'readonly',
        'showWordLimit': 'show-word-limit'
    },
    // 计数器
    'NUMBER_INPUT': {
        'min': 'min',
        'max': 'max',
        'maxlength': 'maxlength',
        'step': 'step',
        'step-strictly': 'step-strictly',
        'precision': 'precision',
        'controls-position': 'controls-position'
    },

    // 编辑器
    'TINYMCE': {
      'placeholder': 'placeholder',
      'height': 'height',
      'branding': 'branding'
    },

    // 下拉选择
    'SELECT': {
        'options': '__slot__.options',
        'filterable': 'filterable',
        'multiple': 'multiple'
    },
    // 级联选择
    'CASCADER': {
        'options': 'options',
        'filterable': 'filterable',
        'show-all-levels': 'showAllLevels',
        'multiple': 'props.props.multiple'
    },
    // 单选框组
    'RADIO': {
        'optionType': '__config__.optionType',
        'border': '__config__.border',
        'options': '__slot__.options',
        'filterable': 'filterable',
        'size': 'size',
        'multiple': 'props.props.multiple'
    },
    // 多选框组
    'CHECKBOX': {
        'optionType': '__config__.optionType',
        'size': 'size',
        'options': '__slot__.options',
        'max': 'max',
        'min': 'min'
    }, // 开关
    'SWITCH': {
      'active-text': 'active-text',
      'inactive-text':'inactive-text',
      'active-color':'active-color',
      'inactive-color':'inactive-color',
      'active-value':'active-value',
      'inactive-value':'inactive-value'
    },
    // 滑块
    'SLIDER': {
        'min': 'min',
        'max': 'max',
        'step': 'step',
        'show-stops':'show-stops',
        'range':'range'
    }, // 时间选择
    'TIME': {
      'value-format':'value-format',
      'clearable':'clearable',
      'format':'format',
      'picker-options':'picker-options'
    },// 时间范围
    'TIME_RANGE': {
      'value-format':'value-format',
      'clearable':'clearable',
      'format':'format',
      'is-range':'is-range',
      'range-separator':'range-separator',
      'start-placeholder':'start-placeholder',
      'end-placeholder':'end-placeholder'
    },
    'RATE': {
        'max': 'max',
        'allowHalf': 'allow-half',
        'showText': 'show-text',
        'showScore': 'show-score',
        'disabled': 'disabled'
    },
    // 文件上传
    'UPLOAD': {
        'buttonText': '__config__.buttonText',
        'showTip': '__config__.showTip',
        'fileSize': '__config__.fileSize',
        'sizeUnit': '__config__.sizeUnit',
        'listType': 'list-type',
        'limit': 'limit',
        'multiple': 'multiple',
        'accept': 'accept'
    },
    // 图片
    'IMAGE': {
        'src': 'src',
        'alt': 'alt'
    },
    // 图片选择
    'IMAGE_SELECT': {
        'options': 'options',
        'multiple': 'multiple'
    },
    // 图片轮播
    'IMAGE_CAROUSEL': {
        'options': '__slot__.options'
    },
    // 文字描述
    'DESC_TEXT': {
        'color': 'color',
        'textAlign': 'textAlign'
    },
    // 手写签名
    'SIGN_PAD': {
        'color': 'color'
    },
    // 日期类型
    'DATE': {
        'type': 'type',
        'format': 'format',
        'valueFormat': 'value-format',
        'clearable':'clearable',
        'readonly':'readonly',
        'disabled':'disabled'
    },
  // 日期范围类型
  'DATE_RANGE': {
    'type': 'type',
    'format': 'format',
    'valueFormat': 'value-format',
    'clearable':'clearable',
    'readonly':'readonly',
    'disabled':'disabled',
    'range-separator':'range-separator',
    'start-placeholder':'start-placeholder',
    'end-placeholder':'end-placeholder'
  },
  // 颜色选择
  'COLOR': {
    'show-alpha': 'show-alpha',
    'color-format': 'color-format',
    'disabled': 'disabled',
    'size':'size'
  },
  // 行容器
  'ROW_CONTAINER': {
    'type': 'type',
    'justify': 'justify',
    'align': 'align'
  }
}

