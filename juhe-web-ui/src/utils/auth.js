
const LoginUserKey = 'LoginUserKey'

/**
 * 获取系统标题
 * @returns {string}
 */
export function getAppTitle(){
// 判断开发环境（一般用于本地代理）
  if (process.env.NODE_ENV === 'development') {
    return process.env.VUE_APP_TITLE
  }else {
    return window.Glob.APP_TITLE
  }
}

/**
 * 获取服务端接口连接地址
 * @returns {string}
 */
export function getAppApiUrl(){
// 判断开发环境（一般用于本地代理）
  if (process.env.NODE_ENV === 'development') {
    return process.env.VUE_APP_BASE_API
  }else {
    return window.Glob.APP_API_URL
  }
}

/**
 * 获取websocket连接地址
 * @returns {string|*}
 */
export function getAppWebsocketUrl(){
  let urlSuffix = "/webSocket";
  // 判断开发环境（一般用于本地代理）
  if (process.env.NODE_ENV === 'development') {
    return process.env.VUE_APP_WEB_SOCKET_URL+urlSuffix
  }else {
    return window.Glob.APP_SOCKET_URL+urlSuffix
  }
}


/**
 * 获取token
 * @returns {*}
 */
export function getToken() {
  if (getLoginUser()){
    return getLoginUser().token
  }
}


/**
 * 获取signKey
 * @returns {*}
 */
export function getSignKey() {
  if (getLoginUser()){
    return getLoginUser().signKey
  }
}

/**
 * 获取登录用户信息
 * @returns {any}
 */
export function getLoginUser() {
  return JSON.parse(localStorage.getItem(LoginUserKey))
}

/**
 * 设置登录用户信息
 * @param LoginUser
 */
export function setLoginUser(LoginUser) {
  return localStorage.setItem(LoginUserKey, JSON.stringify(LoginUser));
}


/**
 * 移除登录用户信息
 * @param userInfo
 */
export function removeLoginUser() {
  //移除登录用户token信息
  localStorage.removeItem(LoginUserKey);
  //移除全局配置
  localStorage.removeItem("layout-setting")
  //移除全部
  localStorage.clear();
}


/**
 * 获取多聚合安全key
 * @returns
 */
export function getDjhSafeKey() {
  return localStorage.getItem("djhSafeKey")
}

/**
 * 设置多聚合安全key
 * @param safeKeyValue
 */
export function setDjhSafeKey(safeKeyValue) {
  return localStorage.setItem("djhSafeKey", safeKeyValue);
}
