import Base from './base'
import store from '@/store'

/**
 * 删除消息处理
 */
class Delete extends Base {
  /**
   * @var resource 资源
   */
  resource

  /**
   * 初始化构造方法
   *
   * @param {Object} resource Socket消息
   */
  constructor(resource) {
    super()

    this.resource = resource.data
  }

  handle() {
    if (
      !this.isTalk(
        this.resource.talkType,
        this.resource.receiverId,
        this.resource.senderId
      )
    ) {
      return false
    }
    store.commit('BATCH_DELETE_DIALOGUE', this.resource.recordIdList)
  }
}

export default Delete
