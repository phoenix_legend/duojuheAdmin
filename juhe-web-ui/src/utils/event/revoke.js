import Base from './base'
import store from '@/store'
import { parseTime } from '@/utils/functions'

/**
 * 撤回消息处理
 */
class Revoke extends Base {
  /**
   * @var resource 资源
   */
  resource

  /**
   * 初始化构造方法
   *
   * @param {Object} resource Socket消息
   */
  constructor(resource) {
    super()
    console.log(resource.data)
    this.resource = resource.data
  }


  /**
   * 判断消息发送者是否来自于我
   * @returns
   */
  isCurrSender() {
    return this.resource.senderId == this.getAccountId()
  }
  /**
   * 获取对话索引
   *
   * @return String
   */
  getIndexName() {
    if (this.resource.talkType == 2) {
      return `${this.resource.talkType}_${this.resource.receiverId}`
    }

    let receiverId = this.isCurrSender() ? this.resource.receiverId : this.resource.senderId

    return `${this.resource.talkType}_${receiverId}`
  }

  handle() {
    store.commit('UPDATE_TALK_ITEM', {
      indexName: this.getIndexName(),
      msgText: "撤回一条消息",
      updateTime: parseTime(new Date()),
    })

    if (
      !this.isTalk(
        this.resource.talkType,
        this.resource.receiverId,
        this.resource.senderId
      )
    ) {
      return false
    }


    store.commit('UPDATE_DIALOGUE', {
      recordId: this.resource.recordId,
      isRevoke: 1,
    })
  }
}

export default Revoke
