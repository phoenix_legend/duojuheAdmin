const formValidation = {
  loginName: (rule, value, callback) => {
    const reg = /^[a-zA-Z0-9]{6,16}$/;
    if (!reg.test(value)) {
      callback('只能为大小写字母和数字,6-16位字符')
    } else {
      callback()
    }
  },
  realName: (rule, value, callback) => {
    const reg = /^[\u4e00-\u9fa5]{2,10}$/;
    if (!reg.test(value)) {
      callback('姓名只能为汉字,长度2-10')
    } else {
      callback()
    }
  },
  nickName: (rule, value, callback) => {
    const reg = /^[\u4E00-\u9FA5a-zA-Z0-9]{2,16}$/;
    if (!reg.test(value)) {
      callback('只能填写中文英文数字，长度2-16')
    } else {
      callback()
    }
  },

  chEnNO: (rule, value, callback) => {
    const reg = /^[\u4E00-\u9FA5a-zA-Z0-9]*$/;
    if (!reg.test(value)) {
      callback('只能填写中文英文数字')
    } else {
      callback()
    }
  },
  password: (rule, value, callback) => {
    const reg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/;
    if (!reg.test(value)) {
      callback('密码至少包含数字和英文，长度6-16')
    } else {
      callback()
    }
  },
  password_null: (rule, value, callback) => {
    const reg = /^((?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16})|$/;
    if (!reg.test(value)) {
      callback('密码至少包含数字和英文长度6-16,可以为空')
    } else {
      callback()
    }
  },
  phoneortel: (rule, value, callback) => {
    const reg = /^1(3|4|5|6|7|8|9)\d{9}$/;
    const reg1 = /^([0-9]{3,4}-)?[0-9]{7,8}$/;
    if (!reg.test(value) && !reg1.test(value)) {
      callback('请输入正确的手机号或固定电话号码')
    } else {
      callback()
    }
  },
  phoneortelorempty: (rule, value, callback) => {
    const reg = /^1(3|4|5|6|7|8|9)\d{9}$/;
    const reg1 = /^([0-9]{3,4}-)?[0-9]{7,8}$/;
    if (value && !reg.test(value) && !reg1.test(value)) {
      callback('请输入正确的手机号或固定电话号码')
    } else {
      callback()
    }
  },
  mobile: (rule, value, callback) => {
    const reg = /^1(3|4|5|6|7|8|9)\d{9}$/;
    if (!reg.test(value)) {
      callback('请输入正确的手机号码')
    } else {
      callback()
    }
  },
  chEnNOLine: (rule, value, callback) => {
    const reg = /^[\u4E00-\u9FA5a-zA-Z0-9_]*$/;
    if (!reg.test(value)) {
      callback('只能填写中文英文数字和下划线')
    } else {
      callback()
    }
  },
  remark: (rule, value, callback) => {
    const reg = /^( |[\u4E00-\u9FA5a-zA-Z0-9]|(\\s))*$/;
    if (!reg.test(value)) {
      callback('禁止输入特殊字符可为空')
    } else {
      callback()
    }
  },
  enNOLine: (rule, value, callback) => {
    const reg = /^[a-zA-Z0-9_]*$/;
    if (!reg.test(value)) {
      callback('只能填写英文数字下划线')
    } else {
      callback()
    }
  },

  enNO: (rule, value, callback) => {
    const reg = /^[a-zA-Z0-9]*$/;
    if (!reg.test(value)) {
      callback('只能填写英文数字')
    } else {
      callback()
    }
  },

  chEnNOLinePoint: (rule, value, callback) => {
    const reg = /^[a-zA-Z0-9_\-.]*$/;
    if (!reg.test(value)) {
      callback('只能填写英文数字下划线和点')
    } else {
      callback()
    }
  },
  abcABC: (rule, value, callback) => {
    const reg = /^[a-zA-Z]*$/;
    if (!reg.test(value)) {
      callback('只能填写英文字母')
    } else {
      callback()
    }
  },

  abcABC_null: (rule, value, callback) => {
    const reg = /^[a-zA-Z]{0,50}$/;
    if (!reg.test(value)) {
      callback('只能填写英文字母,可不填')
    } else {
      callback()
    }
  },
  smsSign: (rule, value, callback) => {
    const reg = /^[\u4E00-\u9FA5a-zA-Z0-9_【】]*$/;
    if (!reg.test(value)) {
      callback('只能填写中文英文数字下划线和【】')
    } else {
      callback()
    }
  },
  certId: (rule, value, callback) => {
    const reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    if (!reg.test(value)) {
      callback('身份证号长度15或18位')
    } else {
      callback()
    }
  },
  moneyorempty: (rule, value, callback) => {
    const reg = /^(?:\d{1,8}|\d{1,3}(?:,\d{3})+)?(?:\.\d{1,2})?$/;
    if (value && !reg.test(value)) {
      callback('只能输入非零的正整数,,长度1-8位')
    } else {
      callback()
    }
  },
  money: (rule, value, callback) => {
    const reg = /^(?:\d{1,8}|\d{1,3}(?:,\d{3})+)?(?:\.\d{1,2})?$/;
    if (!reg.test(value)) {
      callback('只能输入非零的正整数,,长度1-8位')
    } else {
      callback()
    }
  },
  number: (rule, value, callback) => {
    const reg = /^(?!0+(?:\.0+)?$)(?:[1-9]\d*|0)(?:\.\d{1,2})?$/;
    if (!reg.test(value)) {
      callback('只能输入非零的正数')
    } else {
      callback()
    }
  },
  floor: (rule, value, callback) => {
    const reg = /^([0-9]{1,3})$/;
    if (!reg.test(value) || parseInt(value) === 0) {
      callback('只能输入1000以内的正整数,长度1-3位')
    } else {
      callback()
    }
  },
  bankCard: (rule, value, callback) => {
    const reg = /^([1-9]{1})(\d{14}|\d{18})$/;
    if (!reg.test(value)) {
      callback('银行卡号只能为15或者19位的数字!')
    } else {
      callback()
    }
  },

}

export default formValidation
