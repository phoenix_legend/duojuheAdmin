import {getAppApiUrl, getToken} from '@/utils/auth'
// 百度富文本配置项
export function baiDuUeditor(initialFrameHeight){
  return {
    // 编辑器不自动被内容撑高
    autoHeightEnabled: false,
    // 初始容器高度
    initialFrameHeight: initialFrameHeight,
    // 工具栏是否可以浮动
    autoFloatEnabled: false,
    // 初始容器宽度
    initialFrameWidth: '100%',
    // 关闭自动保存
    enableAutoSave: true,
    //编辑器文件位置
    UEDITOR_HOME_URL: "/plugin/UEditor/",
    //服务请求地址
    serverUrl: getAppApiUrl() + "/sysCommon/ueditor?token=" + getToken() + "&timestamp=" + new Date().getTime()
  }
}
