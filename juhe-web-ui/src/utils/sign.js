// 对象重新排序 生成字符串
export default {
  sortObjASCII (obj) {
    var arr = new Array();
    var num = 0;
    for (var i in obj) {
      arr[num] = i;
      num++;
    }
    var sortArr = arr.sort();
    var sortObj = {};
    for (var i in sortArr) {
      sortObj[sortArr[i]] = obj[sortArr[i]];
    }
    // sortObj 是排序后生成的新的对象参数  取出所有 key 和 value 拼接成 key=value&key=value& 的形式
    //  sortObj[key] 是根据key 取value 的值
    var  result='';
    Object.keys(sortObj).forEach(key =>{
      if(sortObj[key]!=='' && sortObj[key]!==null && sortObj[key]!==undefined){
        //判断value是不是数组
        if(Array.isArray(sortObj[key])){
          result+=key+'=['+sortObj[key].toString()+']&'
        }else{
          if(sortObj[key].toString().trim()!==''){
            result+=key+'='+sortObj[key]+'&'
          }
        }
      }
    })
    return result;
  }
}
