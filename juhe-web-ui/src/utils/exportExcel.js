/**
 * @param data
 */
import axios from 'axios'
import {getAppApiUrl, getToken} from '@/utils/auth'

/**
 * 导出excel
 * @param data
 * @param url
 * @param fileName
 * @returns {Promise<AxiosResponse<T>>}
 */
export function exportExcel(data, url, fileName) {
  const token = getToken()
  const timestamp = new Date().getTime()
  data.token = token
  data.timestamp = timestamp
  const req = axios.create({
    baseURL: getAppApiUrl(),
    headers: {
      'token': token,
    },
    timeout: 15000,
    responseType: 'blob'
  })
  return req.post(
    url,
    JSON.stringify(data),
    {}).then((res) => {
    let a = document.createElement('a')
    let blob = new Blob([res.data], { type: 'application/vnd.ms-excel' })
    a.href = URL.createObjectURL(blob)
      a.download = fileName + '.xlsx'
    a.click()
  })
}

