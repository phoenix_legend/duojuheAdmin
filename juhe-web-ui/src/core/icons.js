/**
 * Custom icon list
 * All icons are loaded here for easy management
 *
 * 自定义图标加载表
 * 所有图标均从这里加载，方便管理
 */
import SvgMentionDown from '@/assets/icons/svg/mention-down.svg?inline' // path to your '*.svg?inline' file.
import SvgNotFount from '@/assets/icons/svg/not-fount.svg?inline' // path to your '*.svg?inline' file.
import SvgNote from '@/assets/icons/svg/note.svg?inline' // path to your '*.svg?inline' file.
import SvgNoteBook from '@/assets/icons/svg/note-book.svg?inline' // path to your '*.svg?inline' file.
import SvgNotData from '@/assets/icons/svg/not-data.svg?inline' // path to your '*.svg?inline' file.
import SvgZhuangFa from '@/assets/icons/svg/zhuangfa.svg?inline' // path to your '*.svg?inline' file.


export {
  SvgMentionDown,
  SvgNotFount,
  SvgNote,
  SvgNoteBook,
  SvgNotData,
  SvgZhuangFa,
}
