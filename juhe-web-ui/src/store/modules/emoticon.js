import { queryMyChatEmoticonItemRes, uploadDiyChatEmoticonItem, collectChatEmoticonItem } from '@/api/chat/im/chatEmoticon'

import { Notification } from 'element-ui'

export default {
  state: {
    items: [
      {
        categoryId: -1,
        categoryName: 'QQ表情/符号表情',
        categoryIcon: require('@/assets/images/chat/icon_face.png'),
        emoticonItemList: [],
      },
      {
        categoryId: 0,
        categoryName: '我的收藏',
        categoryIcon: require('@/assets/images/chat/icon_heart.png'),
        emoticonItemList: [],
      },
    ],
  },
  mutations: {
    // 加载用户表情包
    LOAD_USER_EMOTICON(state) {
      queryMyChatEmoticonItemRes().then(res => {
        const { collectEmoticonList, systemEmoticonList } = res.data

        state.items = state.items.slice(0, 2)

        // 用户收藏的系统表情包
        state.items[1].emoticonItemList = collectEmoticonList

        // 用户添加的系统表情包
        state.items.push(...systemEmoticonList)
      })
    },

    // 收藏用户表情包
    SAVE_USER_EMOTICON(state, resoure) {
      collectChatEmoticonItem({
        recordId: resoure.recordId,
      }).then(res => {
          if (res&&res.errorCode === 0) {
            Notification({
              title: '收藏提示',
              message: '表情包收藏成功...',
              type: 'success',
            })
            this.commit('LOAD_USER_EMOTICON')
          }
        })
    },

    // 自定义上传用户表情包
    UPLOAD_USER_EMOTICON(state, resoure) {
      let fileData = new FormData()
      fileData.append('file', resoure.file)
      uploadDiyChatEmoticonItem(fileData)
        .then(res => {
          state.items[1].emoticonItemList.push(res.data)
        })
        .catch(() => {
          Notification({
            message: '网络异常请稍后再试...',
            type: 'error',
            duration: 3000,
          })
        })
    },

    // 添加系统表情包
    APPEND_SYS_EMOTICON(state, resoure) {
      state.items.push(resoure)
    },

    // 移除系统表情包
    REMOVE_SYS_EMOTICON(state, resoure) {
      for (let i in state.items) {
        if (state.items[i].emoticonId === resoure.emoticonId) {
          state.items.splice(i, 1)
          break
        }
      }
    },
  },
}
