export default {
  state: {
    // 对话来源[1:私聊;2:群聊]
    talkType: 0,

    // 接收者ID
    receiverId: 0,

    isRobot: 0,

    // 聊天记录
    records: [],

    // 对话索引（聊天对话的唯一索引）
    indexName: null,
  },
  mutations: {
    // 更新对话
    UPDATE_DIALOGUE_MESSAGE(state, resource) {
      state.records = []
      state.talkType = parseInt(resource.talkType)
      state.receiverId = resource.receiverId
      state.isRobot = parseInt(resource.isRobot)

      if (state.talkType === 0 || state.receiverId === 0) {
        state.indexName = null
      } else {
        state.indexName = resource.talkType + '_' + resource.receiverId
      }
    },

    // 数组头部压入对话记录
    UNSHIFT_DIALOGUE(state, records) {
      state.records.unshift(...records)
    },

    // 推送对话记录
    PUSH_DIALOGUE(state, record) {
      state.records.push(record)
    },

    // 更新对话记录
    UPDATE_DIALOGUE(state, resource) {
      for (let i in state.records) {
        if (state.records[i].recordId === resource.recordId) {
          Object.assign(state.records[i], resource)
          break
        }
      }
    },

    // 删除对话记录
    DELETE_DIALOGUE(state, index) {
      state.records.splice(index, 1)
    },

    BATCH_DELETE_DIALOGUE(state, ids) {
      ids.forEach(recordId => {
        let index = state.records.findIndex(item => item.recordId == recordId)
        if (index >= 0) state.records.splice(index, 1)
      })
    },

    // 数组头部压入对话记录
    SET_DIALOGUE(state, records) {
      state.records = records
    },
  },
}
