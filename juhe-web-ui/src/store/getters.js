const getters = {
  sidebar: state => state.app.sidebar,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  token: state => state.permission.token,
  avatar: state => state.permission.headPortrait,
  name: state => state.permission.realName,
  introduction: state => state.introduction.motto,
  roles: state => state.permission.roles,
  permissions: state => state.permission.permissions,
  getRouteFlag: state => state.permission.getRouteFlag,
  permission_routes: state => state.permission.routes,
  topbarRouters:state => state.permission.topbarRouters,
  defaultRoutes:state => state.permission.defaultRoutes,
  sidebarRouters:state => state.permission.sidebarRouters,
  // socket 连接状态
  socketStatus: state => state.app.socketStatus,
}
export default getters
