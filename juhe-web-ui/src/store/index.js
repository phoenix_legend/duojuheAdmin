import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import form from './modules/form'
import user from './modules/user'
import tagsView from './modules/tagsView'
import permission from './modules/permission'
import workflow from './modules/workflow'
import settings from './modules/settings'
import getters from './getters'
import talks from './modules/talk'
import emoticon from './modules/emoticon'
import dialogue from './modules/dialogue'
import notify from './modules/notify'
import state from './state'
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    form,
    user,
    tagsView,
    notify,
    permission,
    workflow,
    settings,
    talks,
    emoticon,
    dialogue,
  },
  state,
  getters
})
export default store
