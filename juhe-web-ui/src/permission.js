import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { getToken } from '@/utils/auth'

NProgress.configure({ showSpinner: false })

const whiteList = ['/login', '/auth-redirect', '/register']

router.beforeEach((to, from, next) => {
  NProgress.start()
  // 检测token是否存在并且是否设置无须登录
  let noRequireLogin = to.meta.noRequireLogin
  // 无须登录
  if (noRequireLogin) {
    // 无须登录，直接进入
    next()
    NProgress.done()
  }else if (getToken()) {
    to.meta.title && store.dispatch('settings/setTitle', to.meta.title)
    /* has token*/
    if (to.path === '/login') {
      next({ path: '/' })
      NProgress.done()
    } else {
      //  确定用户是否已通过getInfo获得其权限角色
      const routeFlag = store.getters.getRouteFlag
      if (routeFlag) {
        next()
      } else {
        //beg
        // 判断当前用户是否已拉取完用户风格设置
        store.dispatch('GetUserLayoutSetting',store).then(layoutSetting => {
          store.dispatch('GenerateRoutes').then(accessRoutes => {
            // 根据roles权限生成可访问的路由表
            router.addRoutes(accessRoutes) // 动态添加可访问路由表
            next({ ...to, replace: true }) // hack方法 确保addRoutes已完成
          })
        }).catch(err => {
          store.dispatch('LogOut').then(() => {
            Message.error(err)
            next({ path: '/' })
          })
        })
        //end
      }
    }
  } else {
    // 没有token
    if (whiteList.indexOf(to.path) !== -1) {
      // 在免登录白名单，直接进入
      next()
    } else {
      next(`/login?redirect=${to.fullPath}`) // 否则全部重定向到登录页
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  NProgress.done()
})
