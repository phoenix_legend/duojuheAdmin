import request from '@/utils/request'


//【根据code集合获取数据字典】根据数据字典父级code编码获取数据字典
export function getDicListByDictCodeList(data) {
  return request({
    url: 'sysCommon/dict/queryCommonDictListByParentDictCodeList',
    method: 'post',
    data: JSON.stringify({ dictCodeList: data})
  })
}
