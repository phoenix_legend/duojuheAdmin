import request from '@/utils/request'
// 获取图形验证码
export function getCaptchaImageCode() {
  return request({
    url: '/captcha/getCaptchaImageCode',
    method: 'post'
  })
}
