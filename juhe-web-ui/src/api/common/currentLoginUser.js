import request from '@/utils/request'

//获取当前登录用户的菜单路由 树状
export function getCurrentUserMenuTree() {
  return request({
    url: '/sysCommon/currentLoginUser/queryCurrentLoginUserMenuTree',
    method: 'post'
  })
}

/**
 * 用户退出
 */
export function logout() {
  return request({
    url: '/sysCommon/currentLoginUser/currentLoginUserLogout',
    method: 'post'
  })
}


/**
 * 【修改当前用户密码】修改当前登录用户的密码
 * @param data
 */
export function changeCurrentLoginUserPassword(data) {
  return request({
    url: '/sysCommon/currentLoginUser/changeCurrentLoginUserPassword',
    method: 'post',
    data: JSON.stringify(data)
  })
}



/**
 * 【修改当前用户头像】修改当前登录用户的头像
 * @param data
 */
export function changeCurrentLoginUserHeadPortrait(data) {
  return request({
    url: '/sysCommon/currentLoginUser/changeCurrentLoginUserHeadPortrait',
    method: 'post',
    data: data
  })
}


/**
 * 【修改当前用户基本信息】修改当前登录用户的基本信息
 * @param data
 */
export function changeCurrentLoginUserInfo(data) {
  return request({
    url: '/sysCommon/currentLoginUser/changeCurrentLoginUserInfo',
    method: 'post',
    data: JSON.stringify(data)
  })
}

/**
 * 【获取当前登录用户基本信息】获取当前登录用户基本信息
 */
export function queryCurrentLoginUserInfoRes() {
  return request({
    url: '/sysCommon/currentLoginUser/queryCurrentLoginUserInfoRes',
    method: 'post'
  })
}
