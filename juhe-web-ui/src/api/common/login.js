import request from '@/utils/request'

/**
 * 系统用户登录
 * @param data
 */
export function adminLogin(data) {
  return request({
    url: '/adminLogin',
    method: 'post',
    data: JSON.stringify(data)
  })
}
