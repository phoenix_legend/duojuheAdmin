import request from '@/utils/request'

//【供货商管理列表】 查询所有供货商列表
export function queryTrackingProductSupplierPageResList(data) {
  return request({
    url: 'sysAdmin/trackingProductSupplier/queryTrackingProductSupplierPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【供货商管理列表】一般用于下拉选择
export function querySelectNormalTrackingProductSupplierResList(data) {
  return request({
    url: 'sysAdmin/trackingProductSupplier/querySelectNormalTrackingProductSupplierResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



//【保存】供货商
export function saveTrackingProductSupplier(data) {
  return request({
    url: 'sysAdmin/trackingProductSupplier/saveTrackingProductSupplier',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】供货商
export function updateTrackingProductSupplier(data) {
  return request({
    url: 'sysAdmin/trackingProductSupplier/updateTrackingProductSupplier',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据id删除供货商，注意一次仅能删除一个供货商，存在子项则不能删除
export function deleteTrackingProductSupplierBySupplierId(data) {
  return request({
    url: 'sysAdmin/trackingProductSupplier/deleteTrackingProductSupplierBySupplierId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

