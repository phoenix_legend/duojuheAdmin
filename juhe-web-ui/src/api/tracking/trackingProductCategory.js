import request from '@/utils/request'

//【朔源产品分类管理列表】 查询所有朔源产品分类列表
export function queryTrackingProductCategoryPageResList(data) {
  return request({
    url: 'sysAdmin/trackingProductCategory/queryTrackingProductCategoryPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【朔源产品分类管理列表】一般用于下拉选择
export function querySelectNormalTrackingProductCategoryResList(data) {
  return request({
    url: 'sysAdmin/trackingProductCategory/querySelectNormalTrackingProductCategoryResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



//【保存】朔源产品分类
export function saveTrackingProductCategory(data) {
  return request({
    url: 'sysAdmin/trackingProductCategory/saveTrackingProductCategory',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】朔源产品分类
export function updateTrackingProductCategory(data) {
  return request({
    url: 'sysAdmin/trackingProductCategory/updateTrackingProductCategory',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据id删除朔源产品分类，注意一次仅能删除一个朔源产品分类，存在子项则不能删除
export function deleteTrackingProductCategoryByCategoryId(data) {
  return request({
    url: 'sysAdmin/trackingProductCategory/deleteTrackingProductCategoryByCategoryId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

