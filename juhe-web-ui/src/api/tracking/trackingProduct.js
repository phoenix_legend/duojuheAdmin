import request from '@/utils/request'

//【朔源产品管理列表】 查询所有朔源产品列表
export function queryTrackingProductPageResList(data) {
  return request({
    url: 'sysAdmin/trackingProduct/queryTrackingProductPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【朔源产品管理列表】一般用于下拉选择
export function querySelectTrackingProductPageResList(data) {
  return request({
    url: 'sysAdmin/trackingProduct/querySelectTrackingProductPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



//【保存】朔源产品
export function saveTrackingProduct(data) {
  return request({
    url: 'sysAdmin/trackingProduct/saveTrackingProduct',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】朔源产品
export function updateTrackingProduct(data) {
  return request({
    url: 'sysAdmin/trackingProduct/updateTrackingProduct',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据id删除朔源产品，注意一次仅能删除一个朔源产品，存在子项则不能删除
export function deleteTrackingProductByProductId(data) {
  return request({
    url: 'sysAdmin/trackingProduct/deleteTrackingProductByProductId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

