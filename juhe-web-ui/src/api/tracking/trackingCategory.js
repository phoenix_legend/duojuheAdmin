import request from '@/utils/request'

//【朔源流程类别管理列表】 查询所有朔源流程类别列表
export function queryTrackingCategoryPageResList(data) {
  return request({
    url: 'sysAdmin/trackingCategory/queryTrackingCategoryPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【朔源流程类别管理列表】一般用于下拉选择
export function querySelectNormalTrackingCategoryResList(data) {
  return request({
    url: 'sysAdmin/trackingCategory/querySelectNormalTrackingCategoryResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



//【保存】朔源流程类别
export function saveTrackingCategory(data) {
  return request({
    url: 'sysAdmin/trackingCategory/saveTrackingCategory',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】朔源流程类别
export function updateTrackingCategory(data) {
  return request({
    url: 'sysAdmin/trackingCategory/updateTrackingCategory',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据id删除朔源流程类别，注意一次仅能删除一个朔源流程类别，存在子项则不能删除
export function deleteTrackingCategoryByCategoryId(data) {
  return request({
    url: 'sysAdmin/trackingCategory/deleteTrackingCategoryByCategoryId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

