import request from '@/utils/request'

//【朔源流程项目管理列表】 查询所有朔源流程项目列表
export function queryTrackingProcessItemPageResList(data) {
  return request({
    url: 'sysAdmin/trackingProcessItem/queryTrackingProcessItemPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//批量保存朔源流程项
export function batchSaveTrackingProcessItem(data) {
  return request({
    url: 'sysAdmin/trackingProcessItem/batchSaveTrackingProcessItem',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【修改】朔源流程项目
export function updateTrackingProcessItem(data) {
  return request({
    url: 'sysAdmin/trackingProcessItem/updateTrackingProcessItem',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据id删除朔源流程项目，注意一次仅能删除一个朔源流程项目，存在子项则不能删除
export function deleteTrackingProcessItemByItemId(data) {
  return request({
    url: 'sysAdmin/trackingProcessItem/deleteTrackingProcessItemByItemId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

