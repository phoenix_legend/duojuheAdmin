import request from '@/utils/request'

//【溯源防伪码管理列表】 查询所有溯源防伪码列表
export function queryTrackingCodePageResList(data) {
  return request({
    url: 'sysAdmin/trackingCode/queryTrackingCodePageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//批量保存溯源防伪码
export function batchSaveTrackingCode(data) {
  return request({
    url: 'sysAdmin/trackingCode/batchSaveTrackingCode',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//保存溯源防伪码
export function saveTrackingCode(data) {
  return request({
    url: 'sysAdmin/trackingCode/saveTrackingCode',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【修改】溯源防伪码
export function updateTrackingCode(data) {
  return request({
    url: 'sysAdmin/trackingCode/updateTrackingCode',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据id删除溯源防伪码
export function deleteTrackingCodeByTrackingCodeId(data) {
  return request({
    url: 'sysAdmin/trackingCode/deleteTrackingCodeByTrackingCodeId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

