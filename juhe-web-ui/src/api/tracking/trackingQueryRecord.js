import request from '@/utils/request'
import {exportExcel} from '@/utils/exportExcel'

//防伪码查询记录分页查询
export function queryTrackingQueryRecordPageResList(data) {
  return request({
    url: 'sysAdmin/trackingQueryRecord/queryTrackingQueryRecordPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


/**
 * 导出防伪查询记录日志
 * @param data fileName
 * @returns {AxiosPromise<any>}
 */
export function exportTrackingQueryRecordPageResList(data,fileName) {
  exportExcel(data,'/sysAdmin/trackingQueryRecord/queryTrackingQueryRecordPageResList',fileName);
}
