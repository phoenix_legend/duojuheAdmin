import request from '@/utils/request'

//【分类管理列表-查询tree型结构】 查询所有分类列表
export function queryHelpCategoryTreeResList(data) {
  return request({
    url: 'sysAdmin/helpCategory/queryHelpCategoryTreeResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【分类管理列表-查询tree型结构】一般用于下拉选择
export function querySelectHelpCategoryTreeResList(data) {
  return request({
    url: 'sysAdmin/helpCategory/querySelectHelpCategoryTreeResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



//【保存】分类
export function saveHelpCategory(data) {
  return request({
    url: 'sysAdmin/helpCategory/saveHelpCategory',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】分类
export function updateHelpCategory(data) {
  return request({
    url: 'sysAdmin/helpCategory/updateHelpCategory',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据id删除分类，注意一次仅能删除一个分类，存在子项则不能删除
export function deleteHelpCategoryByCategoryId(data) {
  return request({
    url: 'sysAdmin/helpCategory/deleteHelpCategoryByCategoryId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

