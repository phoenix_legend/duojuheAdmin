import request from '@/utils/request'

//【帮助详情】根据帮助发布ID获取帮助详情
export function queryHelpInfoResByInfoId(data) {
  return request({
    url: 'sysAdmin/helpInfo/queryHelpInfoResByInfoId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【分页查询】分页查询帮助发布list
export function queryHelpInfoPageResList(data) {
  return request({
    url: 'sysAdmin/helpInfo/queryHelpInfoPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【分页查询我发布的list】分页查询我发布的list
export function queryMyHelpInfoPageResList(data) {
  return request({
    url: 'sysAdmin/helpInfo/queryMyHelpInfoPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【分页查询我的回收站帮助发布list】分页查询我的回收站帮助发布list
export function queryRecycleHelpInfoPageResList(data) {
  return request({
    url: 'sysAdmin/helpInfo/queryRecycleHelpInfoPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



//【保存】帮助发布
export function saveHelpInfo(data) {
  return request({
    url: 'sysAdmin/helpInfo/saveHelpInfo',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】帮助发布
export function updateHelpInfo(data) {
  return request({
    url: 'sysAdmin/helpInfo/updateHelpInfo',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据ID删除帮助发布，注意一次仅能删除一个帮助发布
export function deleteHelpInfoByInfoId(data) {
  return request({
    url: 'sysAdmin/helpInfo/deleteHelpInfoByInfoId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【回收站中】根据帮助发布ID把发布帮助放到回收站中
export function putRecycleHelpInfoByInfoId(data) {
  return request({
    url: 'sysAdmin/helpInfo/putRecycleHelpInfoByInfoId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【回收站中的帮助恢复】根据帮助发布ID把回收站中的帮助恢复
export function recoveryRecycleHelpInfoByInfoId(data) {
  return request({
    url: 'sysAdmin/helpInfo/recoveryRecycleHelpInfoByInfoId',
    method: 'post',
    data: JSON.stringify(data)
  })
}
