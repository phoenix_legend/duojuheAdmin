import request from '@/utils/request'

//分页查询帮助中心快捷链接
export function queryHelpQuickLinkPageResList(data) {
  return request({
    url: 'sysAdmin/helpQuickLink/queryHelpQuickLinkPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【保存】增加 帮助中心快捷链接
export function saveHelpQuickLink(data) {
  return request({
    url: 'sysAdmin/helpQuickLink/saveHelpQuickLink',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】帮助中心快捷链接
export function updateHelpQuickLink(data) {
  return request({
    url: 'sysAdmin/helpQuickLink/updateHelpQuickLink',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据id删除帮助中心快捷链接
export function deleteHelpQuickLinkByLinkId(data) {
  return request({
    url: 'sysAdmin/helpQuickLink/deleteHelpQuickLinkByLinkId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

