import request from '@/utils/request'

//【分页查询】分页查询拓扑图元list
export function queryTopoElementInfoPageResList(data) {
  return request({
    url: 'sysAdmin/topo/elementInfo/queryTopoElementInfoPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【保存】拓扑图元信息
export function saveTopoElementInfo(data) {
  return request({
    url: 'sysAdmin/topo/elementInfo/saveTopoElementInfo',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】拓扑图元信息
export function updateTopoElementInfo(data) {
  return request({
    url: 'sysAdmin/topo/elementInfo/updateTopoElementInfo',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据拓扑ID删除拓扑图元，注意一次仅能删除一个拓扑图元，存在绑定关系的则不能删除
export function deleteTopoElementInfoByElementId(data) {
  return request({
    url: 'sysAdmin/topo/elementInfo/deleteTopoElementInfoByElementId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

