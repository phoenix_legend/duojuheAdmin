import request from '@/utils/request'

//【分页查询】分页查询拓扑图list
export function queryTopoTopologyPageResList(data) {
  return request({
    url: 'sysAdmin/topo/topology/queryTopoTopologyPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【保存】拓扑图
export function saveTopoTopology(data) {
  return request({
    url: 'sysAdmin/topo/topology/saveTopoTopology',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【修改】拓扑图
export function updateTopoTopology(data) {
  return request({
    url: 'sysAdmin/topo/topology/updateTopoTopology',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【删除】根据拓扑ID删除拓扑图
export function deleteTopoTopologyByTopoId(data) {
  return request({
    url: 'sysAdmin/topo/topology/deleteTopoTopologyByTopoId',
    method: 'post',
    data: JSON.stringify(data)
  })
}



//设计布局拓扑图
export function designTopology(data) {
  return request({
    url: 'sysAdmin/topo/topology/designTopology',
    method: 'post',
    data: JSON.stringify(data)
  })
}



//另存为拓扑图
export function saveAsTopology(data) {
  return request({
    url: 'sysAdmin/topo/topology/saveAsTopology',
    method: 'post',
    data: JSON.stringify(data)
  })
}

