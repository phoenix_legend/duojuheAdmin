import request from '@/utils/request'

//【分页查询】分页查询拓扑图元分类list
export function queryTopoElementClassPageResList(data) {
  return request({
    url: 'sysAdmin/topo/elementClass/queryTopoElementClassPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【图元分类选择】查询正常可用的拓扑图元分类
export function queryNormalSelectTopoElementClassResList(data) {
  return request({
    url: 'sysAdmin/topo/elementClass/queryNormalSelectTopoElementClassResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【保存】拓扑图元分类信息
export function saveTopoElementClass(data) {
  return request({
    url: 'sysAdmin/topo/elementClass/saveTopoElementClass',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】拓扑图元分类信息
export function updateTopoElementClass(data) {
  return request({
    url: 'sysAdmin/topo/elementClass/updateTopoElementClass',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据拓扑分类ID删除拓扑图元分类，注意一次仅能删除一个拓扑图元分类，存在绑定关系的则不能删除
export function deleteTopoElementClassByClassId(data) {
  return request({
    url: 'sysAdmin/topo/elementClass/deleteTopoElementClassByClassId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

