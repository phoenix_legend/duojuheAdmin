import request from '@/utils/request'

// 分页查询聊天记录接口 获取聊天记录服务接口
export function queryChatTalkRecordPageResList(data) {
  return request({
    url: 'sysAdmin/chatTalkRecord/queryChatTalkRecordPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



// 获取转发会话记录详情列表服务接口
export function queryChatTalkRecordForwardPageResList(data) {
  return request({
    url: 'sysAdmin/chatTalkRecord/queryChatTalkRecordForwardPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



// 查找用户聊天记录服务接口
export function findChatTalkRecordPageResList(data) {
  return request({
    url: 'sysAdmin/chatTalkRecord/findChatTalkRecordPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 发送代码块消息服务接口
export function sendTalkCodeBlock(data) {
  return request({
    url: 'sysAdmin/chatTalkRecord/sendTalkCodeBlock',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 发送文件消息服务接口
export function sendTalkFile(data) {
  return request({
    url: 'sysAdmin/chatTalkRecord/sendTalkFile',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 发送图片消息服务接口
export function sendTalkImage(data) {
  return request({
    url: 'sysAdmin/chatTalkRecord/sendTalkImage',
    method: 'post',
    data: data
  })
}

// 发送表情包服务接口
export function sendTalkEmoticon(data) {
  return request({
    url: 'sysAdmin/chatTalkRecord/sendTalkEmoticon',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 转发消息服务接口
export function forwardTalkRecord(data) {
  return request({
    url: 'sysAdmin/chatTalkRecord/forwardTalkRecord',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 发送投票服务接口
export function sendTalkVote(data) {
  return request({
    url: 'sysAdmin/chatTalkRecord/sendTalkVote',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 提交投票服务接口
export function confirmSubmitTalkVote(data) {
  return request({
    url: 'sysAdmin/chatTalkRecord/confirmSubmitTalkVote',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 根据会话id查询投票记录汇总信息
export function queryChatTalkRecordVoteStatisticsResByRecordId(data) {
  return request({
    url: 'sysAdmin/chatTalkRecord/queryChatTalkRecordVoteStatisticsResByRecordId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 撤回消息服务接口
export function revokeRecordByRecordId(data) {
  return request({
    url: 'sysAdmin/chatTalkRecord/revokeRecordByRecordId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 删除消息服务接口
export function deleteRecordByRecordId(data) {
  return request({
    url: 'sysAdmin/chatTalkRecord/deleteRecordByRecordId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 批量删除消息服务接口
export function batchDeleteRecordByRecordIdList(data) {
  return request({
    url: 'sysAdmin/chatTalkRecord/batchDeleteRecordByRecordIdList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


