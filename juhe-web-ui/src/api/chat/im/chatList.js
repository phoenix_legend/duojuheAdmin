import request from '@/utils/request'


// 获取我的会话列表 获取聊天列表服务接口
export function queryCountMyUnreadChatMessageNum() {
  return request({
    url: 'sysAdmin/chatList/queryCountMyUnreadChatMessageNum',
    method: 'post'
  })
}


// 获取我的会话列表 获取聊天列表服务接口
export function queryMyChatUsersChatListPageResList(data) {
  return request({
    url: 'sysAdmin/chatList/queryMyChatUsersChatListPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 设置会话是否置顶
export function setTopChatUsersChatListById(data) {
  return request({
    url: 'sysAdmin/chatList/setTopChatUsersChatListById',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 设置会话免打扰
export function setDisturbChatUsersChatList(data) {
  return request({
    url: 'sysAdmin/chatList/setDisturbChatUsersChatList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 设置会话删除
export function deleteChatUsersChatListById(data) {
  return request({
    url: 'sysAdmin/chatList/deleteChatUsersChatListById',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 清除未读数量
export function clearUnreadNumChatUsersChatList(data) {
  return request({
    url: 'sysAdmin/chatList/clearUnreadNumChatUsersChatList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 聊天列表创建服务接口
export function saveChatUsersChatList(data) {
  return request({
    url: 'sysAdmin/chatList/saveChatUsersChatList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

