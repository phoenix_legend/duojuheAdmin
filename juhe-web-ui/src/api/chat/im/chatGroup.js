import request from '@/utils/request'

// 查询用户群聊服务接口
export function queryMyChatGroupPageResList(data) {
  return request({
    url: 'sysAdmin/chatGroup/queryMyChatGroupPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 获取群信息服务接口
export function queryChatGroupResByGroupId(data) {
  return request({
    url: 'sysAdmin/chatGroup/queryChatGroupResByGroupId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 创建群聊服务接口
export function saveChatGroup(data) {
  return request({
    url: 'sysAdmin/chatGroup/saveChatGroup',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 修改群信息
export function updateChatGroup(data) {
  return request({
    url: 'sysAdmin/chatGroup/updateChatGroup',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 设置群组全员禁言
export function setMuteChatGroup(data) {
  return request({
    url: 'sysAdmin/chatGroup/setMuteChatGroup',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 邀请好友加入群聊服务接口
export function inviteJoinChatGroup(data) {
  return request({
    url: 'sysAdmin/chatGroup/inviteJoinChatGroup',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 移除群聊成员服务接口
export function removeChatGroupUser(data) {
  return request({
    url: 'sysAdmin/chatGroup/removeChatGroupUser',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 管理员解散群聊服务接口
export function dismissChatGroupByGroupId(data) {
  return request({
    url: 'sysAdmin/chatGroup/dismissChatGroupByGroupId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 退出群组
export function secedeChatGroupByGroupId(data) {
  return request({
    url: 'sysAdmin/chatGroup/secedeChatGroupByGroupId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 修改群聊名片服务接口
export function updateUserChatGroupVisitCard(data) {
  return request({
    url: 'sysAdmin/chatGroup/updateUserChatGroupVisitCard',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 获取用户可邀请加入群组的好友列表
export function queryInviteChatGroupUserPageResList(data) {
  return request({
    url: 'sysAdmin/chatGroup/queryInviteChatGroupUserPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 获取群组成员列表
export function queryChatGroupUserPageResList(data) {
  return request({
    url: 'sysAdmin/chatGroup/queryChatGroupUserPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 获取群组公告列表
export function queryChatGroupNoticePageResList(data) {
  return request({
    url: 'sysAdmin/chatGroup/queryChatGroupNoticePageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 保存群公告
export function saveChatGroupNotice(data) {
  return request({
    url: 'sysAdmin/chatGroup/saveChatGroupNotice',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 编辑群公告
export function updateChatGroupNotice(data) {
  return request({
    url: 'sysAdmin/chatGroup/updateChatGroupNotice',
    method: 'post',
    data: JSON.stringify(data)
  })
}

