import request from '@/utils/request'

//【服务器信息】获取服务器信息
export function queryMonitorServerRes() {
  return request({
    url: 'sysAdmin/monitorServer/queryMonitorServerRes',
    method: 'post'
  })
}
