import request from '@/utils/request'

//【分页查询】根据条件查询在线用户list
export function queryMonitorUserOnlinePageResList(data) {
  return request({
    url: 'sysAdmin/monitorOnline/queryMonitorUserOnlinePageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



// 【强制踢出】根据在线id强制踢出
export function forceKickOutOnlineUserByOnlineId(data) {
  return request({
    url: 'sysAdmin/monitorOnline/forceKickOutOnlineUserByOnlineId',
    method: 'post',
    data: JSON.stringify(data)
  })
}
