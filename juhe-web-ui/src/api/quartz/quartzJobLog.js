import request from '@/utils/request'
import {exportExcel} from '@/utils/exportExcel'

// 定时任务执行日志查询
export function queryQuartzJobLogResList(data) {
  return request({
    url: '/sysAdmin/quartzJobLog/queryQuartzJobLogResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

/**
 * 导出定时任务执行日志
 * @param data fileName
 * @returns {AxiosPromise<any>}
 */
export function exportQuartzJobLogResList(data,fileName) {
  exportExcel(data,'/sysAdmin/quartzJobLog/queryQuartzJobLogResList',fileName);
}



