import request from '@/utils/request'

// 定时任务列表
export function queryQuartzJobResList(data) {
  return request({
    url: '/sysAdmin/quartzJob/queryQuartzJobResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 定时任务新增
export function saveQuartzJob(data) {
  return request({
    url: '/sysAdmin/quartzJob/saveQuartzJob',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 定时任务修改
export function updateQuartzJob(data) {
  return request({
    url: '/sysAdmin/quartzJob/updateQuartzJob',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 定时任务删除
export function deleteQuartzJobByJobId(data) {
  return request({
    url: '/sysAdmin/quartzJob/deleteQuartzJobByJobId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 定时任务暂停
export function pauseQuartzJobByJobId(data) {
  return request({
    url: '/sysAdmin/quartzJob/pauseQuartzJobByJobId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 定时任务恢复
export function resumeQuartzJobByJobId(data) {
  return request({
    url: '/sysAdmin/quartzJob/resumeQuartzJobByJobId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 定时任务执行一次
export function runOnceJobByJobId(data) {
  return request({
    url: '/sysAdmin/quartzJob/runOnceJobByJobId',
    method: 'post',
    data: JSON.stringify(data)
  })
}
