import request from '@/utils/request'

//【分类管理列表-查询tree型结构】 查询所有分类列表
export function queryInfoCategoryTreeResList(data) {
  return request({
    url: 'sysAdmin/infoCategory/queryInfoCategoryTreeResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【分类管理列表-查询tree型结构】一般用于下拉选择
export function querySelectInfoCategoryTreeResList(data) {
  return request({
    url: 'sysAdmin/infoCategory/querySelectInfoCategoryTreeResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



//【保存】分类信息
export function saveInfoCategory(data) {
  return request({
    url: 'sysAdmin/infoCategory/saveInfoCategory',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】分类信息
export function updateInfoCategory(data) {
  return request({
    url: 'sysAdmin/infoCategory/updateInfoCategory',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据id删除分类，注意一次仅能删除一个分类，存在子项则不能删除
export function deleteInfoCategoryByCategoryId(data) {
  return request({
    url: 'sysAdmin/infoCategory/deleteInfoCategoryByCategoryId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

