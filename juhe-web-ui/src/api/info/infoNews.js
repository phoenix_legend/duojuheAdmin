import request from '@/utils/request'

//【信息详情】根据信息发布ID获取信息详情
export function queryInfoNewsResByNewsId(data) {
  return request({
    url: 'sysAdmin/infoNews/queryInfoNewsResByNewsId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【分页查询】分页查询信息发布list
export function queryInfoNewsPageResList(data) {
  return request({
    url: 'sysAdmin/infoNews/queryInfoNewsPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【分页查询我发布的list】分页查询我发布的list
export function queryMyInfoNewsPageResList(data) {
  return request({
    url: 'sysAdmin/infoNews/queryMyInfoNewsPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【分页查询我的回收站信息发布list】分页查询我的回收站信息发布list
export function queryRecycleInfoNewsPageResList(data) {
  return request({
    url: 'sysAdmin/infoNews/queryRecycleInfoNewsPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



//【保存】信息发布
export function saveInfoNews(data) {
  return request({
    url: 'sysAdmin/infoNews/saveInfoNews',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】信息发布
export function updateInfoNews(data) {
  return request({
    url: 'sysAdmin/infoNews/updateInfoNews',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据ID删除信息发布，注意一次仅能删除一个信息发布
export function deleteInfoNewsByNewsId(data) {
  return request({
    url: 'sysAdmin/infoNews/deleteInfoNewsByNewsId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【回收站中】根据信息发布ID把发布信息放到回收站中
export function putRecycleInfoNewsByNewsId(data) {
  return request({
    url: 'sysAdmin/infoNews/putRecycleInfoNewsByNewsId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【回收站中的信息恢复】根据信息发布ID把回收站中的信息恢复
export function recoveryRecycleInfoNewsByNewsId(data) {
  return request({
    url: 'sysAdmin/infoNews/recoveryRecycleInfoNewsByNewsId',
    method: 'post',
    data: JSON.stringify(data)
  })
}
