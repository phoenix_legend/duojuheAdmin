import request from '@/utils/request'

// 短信渠道列表
export function querySmsChannelPageResList(data) {
  return request({
    url: '/sysAdmin/smsChannel/querySmsChannelPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 分页查询状态可用的短信渠道list
export function queryNormalSelectSmsChannelPageResList(data) {
  return request({
    url: '/sysAdmin/smsChannel/queryNormalSelectSmsChannelPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 【查询详情】根据短信发送渠道id查询渠道详情
export function querySmsChannelResByChannelId(data) {
  return request({
    url: '/sysAdmin/smsChannel/querySmsChannelResByChannelId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 短信渠道新增
export function saveSmsChannel(data) {
  return request({
    url: '/sysAdmin/smsChannel/saveSmsChannel',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 短信渠道更新
export function updateSmsChannel(data) {
  return request({
    url: '/sysAdmin/smsChannel/updateSmsChannel',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 短信渠道删除
export function deleteSmsChannelByChannelId(data) {
  return request({
    url: '/sysAdmin/smsChannel/deleteSmsChannelByChannelId',
    method: 'post',
    data: JSON.stringify(data)
  })
}
