import request from '@/utils/request'

// 分页查询短信发送任务list
export function querySmsSendTaskPageResList(data) {
  return request({
    url: '/sysAdmin/smsSendTask/querySmsSendTaskPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 根据任务id查询任务明细
export function querySmsSendTaskResBySendTaskId(data) {
  return request({
    url: '/sysAdmin/smsSendTask/querySmsSendTaskResBySendTaskId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 短信发送任务新增
export function saveSmsSendTask(data) {
  return request({
    url: '/sysAdmin/smsSendTask/saveSmsSendTask',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 短信发送任务更新
export function updateSmsSendTask(data) {
  return request({
    url: '/sysAdmin/smsSendTask/updateSmsSendTask',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 短信发送任务删除
export function deleteSmsSendTaskBySendTaskId(data) {
  return request({
    url: '/sysAdmin/smsSendTask/deleteSmsSendTaskBySendTaskId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 根据短信发送任务id分页查询发送任务明细
export function querySmsSendTaskDetailResList(data) {
  return request({
    url: '/sysAdmin/smsSendTask/querySmsSendTaskDetailResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}
