import request from '@/utils/request'

// 短信模板列表
export function querySmsTemplatePageResList(data) {
  return request({
    url: '/sysAdmin/smsTemplate/querySmsTemplatePageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 分页查询状态可用的短信模板list
export function queryNormalSelectSmsTemplatePageResList(data) {
  return request({
    url: '/sysAdmin/smsTemplate/queryNormalSelectSmsTemplatePageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 短信模板新增
export function saveSmsTemplate(data) {
  return request({
    url: '/sysAdmin/smsTemplate/saveSmsTemplate',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 短信模板更新
export function updateSmsTemplate(data) {
  return request({
    url: '/sysAdmin/smsTemplate/updateSmsTemplate',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 短信模板删除
export function deleteSmsTemplateByTemplateId(data) {
  return request({
    url: '/sysAdmin/smsTemplate/deleteSmsTemplateByTemplateId',
    method: 'post',
    data: JSON.stringify(data)
  })
}
