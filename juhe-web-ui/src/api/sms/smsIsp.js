import request from '@/utils/request'

// 查询所有短信服务商
export function queryNormalSelectSmsIspResList() {
  return request({
    url: '/sysAdmin/smsIsp/queryNormalSelectSmsIspResList',
    method: 'post'
  })
}
