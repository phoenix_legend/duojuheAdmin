import request from '@/utils/request'
import {exportExcel} from '@/utils/exportExcel'

// 短信发送记录查询
export function querySmsRecordPageResList(data) {
  return request({
    url: '/sysAdmin/smsRecord/querySmsRecordPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

/**
 * 导出短信发送记录
 * @param data fileName
 * @returns {AxiosPromise<any>}
 */
export function exportSmsRecordPageResList(data,fileName) {
  exportExcel(data,'/sysAdmin/smsRecord/querySmsRecordPageResList',fileName);
}



