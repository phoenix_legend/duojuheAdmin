import request from '@/utils/request'

//【通知公告分类】分类管理界面使用
export function queryNoticeCategoryPageResList(data) {
  return request({
    url: 'sysAdmin/noticeCategory/queryNoticeCategoryPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【通知公告分类】查询可供前端选择使用的分类
export function querySelectNormalNoticeCategoryResList(data) {
  return request({
    url: 'sysAdmin/noticeCategory/querySelectNormalNoticeCategoryResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



//【保存】公告分类
export function saveNoticeCategory(data) {
  return request({
    url: 'sysAdmin/noticeCategory/saveNoticeCategory',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】公告分类
export function updateNoticeCategory(data) {
  return request({
    url: 'sysAdmin/noticeCategory/updateNoticeCategory',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据id删除分类，注意一次仅能删除一个分类，存在子项则不能删除
export function deleteNoticeCategoryByCategoryId(data) {
  return request({
    url: 'sysAdmin/noticeCategory/deleteNoticeCategoryByCategoryId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

