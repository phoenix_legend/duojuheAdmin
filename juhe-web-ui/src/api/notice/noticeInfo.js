import request from '@/utils/request'

//【公告详情】根据公告发布ID获取公告详情
export function queryNoticeInfoResByNoticeId(data) {
  return request({
    url: 'sysAdmin/noticeInfo/queryNoticeInfoResByNoticeId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【分页查询】分页查询公告发布list
export function queryNoticeInfoPageResList(data) {
  return request({
    url: 'sysAdmin/noticeInfo/queryNoticeInfoPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【分页查询我发布的list】分页查询我发布的list
export function queryMyNoticeInfoPageResList(data) {
  return request({
    url: 'sysAdmin/noticeInfo/queryMyNoticeInfoPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【保存】公告发布
export function saveNoticeInfo(data) {
  return request({
    url: 'sysAdmin/noticeInfo/saveNoticeInfo',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】公告发布
export function updateNoticeInfo(data) {
  return request({
    url: 'sysAdmin/noticeInfo/updateNoticeInfo',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据ID删除公告发布，注意一次仅能删除一个公告发布
export function deleteNoticeInfoByNoticeId(data) {
  return request({
    url: 'sysAdmin/noticeInfo/deleteNoticeInfoByNoticeId',
    method: 'post',
    data: JSON.stringify(data)
  })
}
