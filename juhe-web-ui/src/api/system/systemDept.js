import request from '@/utils/request'



// 【下拉选择使用-查询部门】查询可供前端选择使用的角色，一般用于用户添加下拉选择使用
export function queryNormalSelectSystemDeptResList(data) {
  return request({
    url: 'sysAdmin/systemDept/queryNormalSelectSystemDeptResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



// 【根据ID查询详情】根据部门ID获取部门资源信息
export function querySystemDeptResByDeptId(data) {
  return request({
    url: 'sysAdmin/systemDept/querySystemDeptResByDeptId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 【选择员工返回结果包含部门信息】选择员工返回结果包含部门信息
export function querySelectSystemDeptAndUserResList(data) {
  return request({
    url: 'sysAdmin/systemDept/querySelectSystemDeptAndUserResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



// 【部门管理-tree型结构查询】查询部门列表，一般用于部门管理界面
export function querySystemDeptTreeResList(data) {
  return request({
    url: 'sysAdmin/systemDept/querySystemDeptTreeResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【部门选择使用-tree型结构查询】一般用于部门下拉选择界面使用
export function queryNormalSelectSystemDeptTreeResList(data) {
  return request({
    url: 'sysAdmin/systemDept/queryNormalSelectSystemDeptTreeResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 【保存】部门信息
export function saveSystemDept(data) {
  return request({
    url: 'sysAdmin/systemDept/saveSystemDept',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】部门信息
export function updateSystemDept(data) {
  return request({
    url: 'sysAdmin/systemDept/updateSystemDept',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据id删除部门，存在子项则不能删除
export function deleteSystemDeptByDeptId(data) {
  return request({
    url: 'sysAdmin/systemDept/deleteSystemDeptByDeptId',
    method: 'post',
    data: JSON.stringify(data)
  })
}
