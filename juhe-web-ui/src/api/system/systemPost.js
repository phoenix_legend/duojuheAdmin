import request from '@/utils/request'


//【分页查询】分页查询岗位list
export function querySystemPostPageResList(data) {
  return request({
    url: 'sysAdmin/systemPost/querySystemPostPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



//【下拉选择使用-查询所有岗位】查询可供前端选择使用的岗位，一般用于用户添加下拉选择使用
export function queryNormalSelectSystemPostResList(data) {
  return request({
    url: 'sysAdmin/systemPost/queryNormalSelectSystemPostResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【根据ID查询详情】根据岗位ID查询岗位详情
export function querySystemPostResByPostId(data) {
  return request({
    url: 'sysAdmin/systemPost/querySystemPostResByPostId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【保存】岗位信息
export function saveSystemPost(data) {
  return request({
    url: 'sysAdmin/systemPost/saveSystemPost',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【修改】岗位信息
export function updateSystemPost(data) {
  return request({
    url: 'sysAdmin/systemPost/updateSystemPost',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【删除】根据岗位ID删除岗位，注意一次仅能删除一个岗位，存在绑定关系的则不能删除
export function deleteSystemPostByPostId(data) {
  return request({
    url: 'sysAdmin/systemPost/deleteSystemPostByPostId',
    method: 'post',
    data: JSON.stringify(data)
  })
}
