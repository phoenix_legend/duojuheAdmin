import request from '@/utils/request'


//【分页查询】分页查询角色list
export function querySystemRolePageResList(data) {
  return request({
    url: 'sysAdmin/systemRole/querySystemRolePageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【下拉选择使用-查询所有角色】查询可供前端选择使用的角色，一般用于用户添加下拉选择使用
export function queryNormalSelectSystemRoleResList(data) {
  return request({
    url: 'sysAdmin/systemRole/queryNormalSelectSystemRoleResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【根据ID查询详情】根据角色ID查询角色详情
export function querySystemRoleResByRoleId(data) {
  return request({
    url: 'sysAdmin/systemRole/querySystemRoleResByRoleId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【保存】角色信息
export function saveSystemRole(data) {
  return request({
    url: 'sysAdmin/systemRole/saveSystemRole',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】角色信息
export function updateSystemRole(data) {
  return request({
    url: 'sysAdmin/systemRole/updateSystemRole',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据角色ID删除角色，注意一次仅能删除一个角色，存在绑定关系的则不能删除
export function deleteSystemRoleByRoleId(data) {
  return request({
    url: 'sysAdmin/systemRole/deleteSystemRoleByRoleId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【角色授权】给角色赋权限，menuIdList为空表示清除该角色所有权限
export function saveRoleAuthorization(data) {
  return request({
    url: 'sysAdmin/systemRole/saveRoleAuthorization',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【角色绑定菜单-渲染角色绑定菜单】查询roleId对应的菜单权限集合，一般用于角色功能授权编辑时渲染已经用于的权限勾选
export function querySystemMenuIdListByRoleId(data) {
  return request({
    url: 'sysAdmin/systemRole/querySystemMenuIdListByRoleId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【角色绑定部门-渲染角色绑定部门】查询roleId对应的部门ID权限集合，一般用于角色数据授权编辑时渲染已经用于的权限勾选
export function querySystemDeptIdListByRoleId(data) {
  return request({
    url: 'sysAdmin/systemRole/querySystemDeptIdListByRoleId',
    method: 'post',
    data: JSON.stringify(data)
  })
}
