import request from '@/utils/request'


//【根据ID查询详情】根据数据字典ID查询数据字典详情
export function querySystemDictResByDictId(data) {
  return request({
    url: 'sysAdmin/systemDict/querySystemDictResByDictId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【分页查询】分页查询数据字典一级list
export function queryOneLevelSystemDictPageResList(data) {
  return request({
    url: 'sysAdmin/systemDict/queryOneLevelSystemDictPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【分页查询】分页查询数据字典子级list
export function queryChildrenSystemDictPageResList(data) {
  return request({
    url: 'sysAdmin/systemDict/queryChildrenSystemDictPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【保存】数据字典信息
export function saveSystemDict(data) {
  return request({
    url: 'sysAdmin/systemDict/saveSystemDict',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】数据字典信息
export function updateSystemDict(data) {
  return request({
    url: 'sysAdmin/systemDict/updateSystemDict',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据id删除数据字典,内置数据字典不可删除
export function deleteSystemDictByDictId(data) {
  return request({
    url: 'sysAdmin/systemDict/deleteSystemDictByDictId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【刷新缓存】刷新系统数据字典缓存
export function resetSystemDictCache() {
  return request({
    url: 'sysAdmin/systemDict/resetSystemDictCache',
    method: 'post'
  })
}
