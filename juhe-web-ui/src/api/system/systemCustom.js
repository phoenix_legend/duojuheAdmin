import request from '@/utils/request'
// 【根据标识key查询】根据key查询自定义喜好布局内容详情
export function querySystemCustomContentJsonByCustomKey(data) {
  return request({
    url: 'sysAdmin/systemCustom/querySystemCustomContentJsonByCustomKey',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 【保存】自定义风格喜好布局参数，系统中根据key(前端开发自主定义) 和用户id区分
export function saveSystemCustom(data) {
  return request({
    url: 'sysAdmin/systemCustom/saveSystemCustom',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 【删除】根据key删除自定义布局内容
export function deleteSystemCustomByCustomKey(data) {
  return request({
    url: 'sysAdmin/systemCustom/deleteSystemCustomByCustomKey',
    method: 'post',
    data: JSON.stringify(data)
  })
}
