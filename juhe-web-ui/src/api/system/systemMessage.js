import request from '@/utils/request'


//【分页查询】分页查询系统消息list
export function querySystemMessagePageResList(data) {
  return request({
    url: 'sysAdmin/systemMessage/querySystemMessagePageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【统计未处理 数量】统计未读数量
export function queryCountUntreatedSystemMessage(data) {
  return request({
    url: 'sysAdmin/systemMessage/queryCountUntreatedSystemMessage',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【根据id设置未处理】根据id设置未处理
export function updateSystemMessageHandleStatusByMessageId(data) {
  return request({
    url: 'sysAdmin/systemMessage/updateSystemMessageHandleStatusByMessageId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【一键未处理】一键未处理
export function updateAllSystemMessageHandleStatus(data) {
  return request({
    url: 'sysAdmin/systemMessage/updateAllSystemMessageHandleStatus',
    method: 'post',
    data: JSON.stringify(data)
  })
}
