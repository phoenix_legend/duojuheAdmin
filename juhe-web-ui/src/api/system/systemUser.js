import request from '@/utils/request'


//【分页查询】分页查询系统用户list
export function querySystemUserPageResList(data) {
  return request({
    url: 'sysAdmin/systemUser/querySystemUserPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【选择员工】分页选择员工系统用户list
export function querySelectSystemUserResList(data) {
  return request({
    url: 'sysAdmin/systemUser/querySelectSystemUserResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



//【根据ID查询详情】根据系统用户Id查询用户详情
export function querySystemUserResByUserId(data) {
  return request({
    url: 'sysAdmin/systemUser/querySystemUserResByUserId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【保存】系统用户信息
export function saveSystemUser(data) {
  return request({
    url: 'sysAdmin/systemUser/saveSystemUser',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【修改】系统用户信息
export function updateSystemUser(data) {
  return request({
    url: 'sysAdmin/systemUser/updateSystemUser',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【删除】根据系统用户id删除用户
export function deleteSystemUserByUserId(data) {
  return request({
    url: 'sysAdmin/systemUser/deleteSystemUserByUserId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【改变状态】根据用户id改变用户状态
export function updateSystemUserStatusByUserId(data) {
  return request({
    url: 'sysAdmin/systemUser/updateSystemUserStatusByUserId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【重置密码】根据用户id重置密码
export function resetSystemUserPassword(data) {
  return request({
    url: 'sysAdmin/systemUser/resetSystemUserPassword',
    method: 'post',
    data: JSON.stringify(data)
  })
}
