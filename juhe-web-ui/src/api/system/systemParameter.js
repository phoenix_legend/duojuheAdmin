import request from '@/utils/request'


//【根据code集合获取系统参数】根据code集合获取系统参数
export function querySystemParameterListByParameterCodeList(data) {
  return request({
    url: 'sysAdmin/systemParameter/querySystemParameterListByParameterCodeList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【分页查询】分页查询系统参数list
export function querySystemParameterPageResList(data) {
  return request({
    url: 'sysAdmin/systemParameter/querySystemParameterPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【新增】新增系统参数
export function saveSystemParameter(data) {
  return request({
    url: 'sysAdmin/systemParameter/saveSystemParameter',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【修改】修改系统参数
export function updateSystemParameter(data) {
  return request({
    url: 'sysAdmin/systemParameter/updateSystemParameter',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【删除】删除系统参数
export function deleteSystemParameterByParameterId(data) {
  return request({
    url: 'sysAdmin/systemParameter/deleteSystemParameterByParameterId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【刷新缓存】刷新系统参数缓存
export function resetSystemParameterCache() {
  return request({
    url: 'sysAdmin/systemParameter/resetSystemParameterCache',
    method: 'post'
  })
}
