import request from '@/utils/request'
import {exportExcel} from '@/utils/exportExcel'


//【分页查询】分页查询日志list
export function querySystemLogPageResList(data) {
  return request({
    url: 'sysAdmin/systemLog/querySystemLogPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【根据ID查询详情】根据日志ID查询日志详情
export function querySystemLogResByLogId(data) {
  return request({
    url: 'sysAdmin/systemLog/querySystemLogResByLogId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


/**
 * 导出管理员日志
 * @param data fileName
 * @returns {AxiosPromise<any>}
 */
export function exportSystemLogPageResList(data,fileName) {
  exportExcel(data,'/sysAdmin/systemLog/querySystemLogPageResList',fileName);
}

