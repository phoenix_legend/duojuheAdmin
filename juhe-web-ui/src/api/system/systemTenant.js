import request from '@/utils/request'

//【分页查询】分页查询租户list
export function querySystemTenantPageResList(data) {
  return request({
    url: 'sysAdmin/systemTenant/querySystemTenantPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【保存】租户信息
export function saveSystemTenant(data) {
  return request({
    url: 'sysAdmin/systemTenant/saveSystemTenant',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【修改】租户信息
export function updateSystemTenant(data) {
  return request({
    url: 'sysAdmin/systemTenant/updateSystemTenant',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【改变状态】根据租户id改变租户状态
export function updateSystemTenantStatusByTenantId(data) {
  return request({
    url: 'sysAdmin/systemTenant/updateSystemTenantStatusByTenantId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【租户绑定菜单-渲染租户绑定菜单】查询TenantId对应的菜单权限集合
export function querySystemMenuIdListByTenantId(data) {
  return request({
    url: 'sysAdmin/systemTenant/querySystemMenuIdListByTenantId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【重置密码】根据租户id重置密码
export function resetSystemTenantUserPassword(data) {
  return request({
    url: 'sysAdmin/systemTenant/resetSystemTenantUserPassword',
    method: 'post',
    data: JSON.stringify(data)
  })
}


