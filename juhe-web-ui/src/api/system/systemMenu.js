import request from '@/utils/request'


//【菜单管理列表-查询tree型结构】 查询所有菜单列表，一般用于菜单管理列表界面【包含功能按钮类型】
export function querySystemMenuTreeResList(data) {
  return request({
    url: 'sysAdmin/systemMenu/querySystemMenuTreeResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【角色绑定权限-查询tree型结构】获取状态是可用的菜单资源，一般用于角色绑定权限使用【包含功能按钮类型】
export function queryBindingRoleUseMenuTreeResList() {
  return request({
    url: 'sysAdmin/systemMenu/queryBindingRoleUseMenuTreeResList',
    method: 'post'
  })
}

//【租户绑定菜单权限-查询tree型结构】获取状态是可用的菜单资源，一般用于租户绑定菜单使用【包含功能按钮类型】
export function queryTenantBindingUseMenuTreeResList() {
  return request({
    url: 'sysAdmin/systemMenu/queryTenantBindingUseMenuTreeResList',
    method: 'post'
  })
}


//【下拉选择使用-查询tree型结构】获取状态是可用的导航菜单资源，一般用于菜单管理下拉选择上级菜单【不包含功能按钮】
export function querySelectNavigationMenuTreeResList() {
  return request({
    url: 'sysAdmin/systemMenu/querySelectNavigationMenuTreeResList',
    method: 'post'
  })
}


//【根据ID查询详情】根据菜单ID查询菜单详情
export function querySystemMenuResByMenuId(data) {
  return request({
    url: 'sysAdmin/systemMenu/querySystemMenuResByMenuId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【保存】菜单信息
export function saveSystemMenu(data) {
  return request({
    url: 'sysAdmin/systemMenu/saveSystemMenu',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【修改】菜单信息
export function updateSystemMenu(data) {
  return request({
    url: 'sysAdmin/systemMenu/updateSystemMenu',
    method: 'post',
    data: JSON.stringify(data)
  })
}

//【删除】根据id删除菜单，注意一次仅能删除一个菜单，存在子项则不能删除
export function deleteSystemMenuByMenuId(data) {
  return request({
    url: 'sysAdmin/systemMenu/deleteSystemMenuByMenuId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

