import request from '@/utils/request'

// 根据条件查询表单项目提交结果集合
export function queryFormProjectResultPageResList(data) {
  return request({
    url: 'sysAdmin/formProjectResult/queryFormProjectResultPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 【查询项目收集设备分析】根据项目id查询提交来源统计分析
export function queryProjectResultReportDeviceResByProjectId(data) {
  return request({
    url: 'sysAdmin/formProjectResult/queryProjectResultReportDeviceResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 【项目收集情况 按周查看】根据项目id查询项目收集情况 按周查看分析
export function queryProjectResultReportSituationResByProjectId(data) {
  return request({
    url: 'sysAdmin/formProjectResult/queryProjectResultReportSituationResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 【查询项目收集来源分析】根据项目id查询项目收集来源分析
export function queryProjectResultReportSourceResByProjectId(data) {
  return request({
    url: 'sysAdmin/formProjectResult/queryProjectResultReportSourceResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 【查询项目收集信息】根据项目id查询项目收集信息
export function queryProjectResultReportStatsResByProjectId(data) {
  return request({
    url: 'sysAdmin/formProjectResult/queryProjectResultReportStatsResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【保存】后台新增问卷记录
export function saveFormProjectResult(data) {
  return request({
    url: 'sysAdmin/formProjectResult/saveFormProjectResult',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【修改】后台修改问卷记录
export function updateFormProjectResult(data) {
  return request({
    url: 'sysAdmin/formProjectResult/updateFormProjectResult',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【删除】后台删除问卷记录
export function deleteFormProjectResultById(data) {
  return request({
    url: 'sysAdmin/formProjectResult/deleteFormProjectResultById',
    method: 'post',
    data: JSON.stringify(data)
  })
}


//【查询问卷记录编辑初始值】根据记录id查询查询记录编辑初始值明细
export function queryFormProjectResultDetailsEditResById(data) {
  return request({
    url: 'sysAdmin/formProjectResult/queryFormProjectResultDetailsEditResById',
    method: 'post',
    data: JSON.stringify(data)
  })
}
