import request from '@/utils/request'

// 根据项目id查询逻辑集合
export function queryFormProjectLogicPageResByProjectId(data) {
  return request({
    url: 'sysAdmin/formProjectLogic/queryFormProjectLogicPageResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 新增表单项目逻辑
export function saveFormProjectLogic(data) {
  return request({
    url: 'sysAdmin/formProjectLogic/saveFormProjectLogic',
    method: 'post',
    data: JSON.stringify(data)
  })
}



// 表单项目逻辑删除
export function deleteFormProjectLogic(data) {
  return request({
    url: '/sysAdmin/formProjectLogic/deleteFormProjectLogic',
    method: 'post',
    data: JSON.stringify(data)
  })
}


