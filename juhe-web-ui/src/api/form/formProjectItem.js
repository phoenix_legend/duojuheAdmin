import request from '@/utils/request'

// 表单子项目列表
export function queryFormProjectItemPageResList(data) {
  return request({
    url: '/sysAdmin/formProjectItem/queryFormProjectItemPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



// 根据项目Id查询本项目最大的子项目表单id
export function queryFormProjectItemMaxFormItemIdByProjectId(data) {
  return request({
    url: 'sysAdmin/formProjectItem/queryFormProjectItemMaxFormItemIdByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 根据项目Id查询表单项目的表头信息
export function queryFormProjectItemTableHeadResByProjectId(data) {
  return request({
    url: 'sysAdmin/formProjectItem/queryFormProjectItemTableHeadResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 保存和修改表单项目
export function saveUpdateFormProjectItem(data) {
  return request({
    url: 'sysAdmin/formProjectItem/saveUpdateFormProjectItem',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 删除表单项目
export function deleteFormProjectItem(data) {
  return request({
    url: 'sysAdmin/formProjectItem/deleteFormProjectItem',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 重置项目排序
export function updateFormProjectItemSort(data) {
  return request({
    url: 'sysAdmin/formProjectItem/updateFormProjectItemSort',
    method: 'post',
    data: JSON.stringify(data)
  })
}
