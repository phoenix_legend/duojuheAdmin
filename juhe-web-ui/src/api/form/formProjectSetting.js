import request from '@/utils/request'

// 根据项目id查询设置明细
export function queryFormProjectSettingResByProjectId(data) {
  return request({
    url: 'sysAdmin/formProjectSetting/queryFormProjectSettingResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 修改表单项目设置
export function updateFormProjectSetting(data) {
  return request({
    url: 'sysAdmin/formProjectSetting/updateFormProjectSetting',
    method: 'post',
    data: JSON.stringify(data)
  })
}


