import request from '@/utils/request'

// 根据项目id查询风格明细
export function queryFormProjectStyleResByProjectId(data) {
  return request({
    url: 'sysAdmin/formProjectStyle/queryFormProjectStyleResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 修改表单项目风格
export function updateFormProjectStyle(data) {
  return request({
    url: 'sysAdmin/formProjectStyle/updateFormProjectStyle',
    method: 'post',
    data: JSON.stringify(data)
  })
}


