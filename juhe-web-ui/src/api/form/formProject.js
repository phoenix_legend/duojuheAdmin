import request from '@/utils/request'

// 表单项目列表
export function queryFormProjectPageResList(data) {
  return request({
    url: '/sysAdmin/formProject/queryFormProjectPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 根据项目key查询详情
export function queryFormProjectResByProjectId(data) {
  return request({
    url: 'sysAdmin/formProject/queryFormProjectResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 根据项目id查询明细
export function queryFormProjectDetailsResByProjectId(data) {
  return request({
    url: 'sysAdmin/formProject/queryFormProjectDetailsResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 新增表单项目
export function saveFormProject(data) {
  return request({
    url: 'sysAdmin/formProject/saveFormProject',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 根据表单模板项目id创建新的表单项目
export function createFormProjectByTemplate(data) {
  return request({
    url: 'sysAdmin/formProject/createFormProjectByTemplate',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 修改表单项目
export function updateFormProject(data) {
  return request({
    url: 'sysAdmin/formProject/updateFormProject',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 修改表单项目发布状态
export function updateFormProjectStatusByProjectId(data) {
  return request({
    url: 'sysAdmin/formProject/updateFormProjectStatusByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 表单项目删除
export function deleteFormProjectByProjectId(data) {
  return request({
    url: '/sysAdmin/formProject/deleteFormProjectByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


