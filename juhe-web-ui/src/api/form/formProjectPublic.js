import request from '@/utils/request'

// 【保存】公开保存表单结果
export function savePublicFormProjectResult(data) {
  return request({
    url: 'formProjectPublic/savePublicFormProjectResult',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 【项目明细查询】根据项目id查询项目明细
export function queryPublicFormProjectDetailsResByProjectId(data) {
  return request({
    url: 'formProjectPublic/queryPublicFormProjectDetailsResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 【项目设置详情】根据项目ID查询项目设置详情
export function queryPublicFormProjectSettingResByProjectId(data) {
  return request({
    url: 'formProjectPublic/queryPublicFormProjectSettingResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}



// 【项目逻辑集合】根据项目id查询项目逻辑集合
export function queryPublicFormProjectLogicPageResByProjectId(data) {
  return request({
    url: 'formProjectPublic/queryPublicFormProjectLogicPageResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 根据项目Id查询表单项目的表头信息
export function queryPublicFormProjectItemTableHeadResByProjectId(data) {
  return request({
    url: 'formProjectPublic/queryPublicFormProjectItemTableHeadResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 根据项目Id查询表单项目详情
export function queryPublicFormProjectResByProjectId(data) {
  return request({
    url: 'formProjectPublic/queryPublicFormProjectResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 【分页查询填写结果】根据条件查询表单项目提交结果集合
export function queryPublicFormProjectResultPageResList(data) {
  return request({
    url: 'formProjectPublic/queryPublicFormProjectResultPageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}


