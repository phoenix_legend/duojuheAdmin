import request from '@/utils/request'

// 表单项目私有模板列表
export function queryFormProjectPrivateTemplatePageResList(data) {
  return request({
    url: '/sysAdmin/formProjectTemplate/queryFormProjectPrivateTemplatePageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}



// 表单项目公共模板列表
export function queryFormProjectPublicTemplatePageResList(data) {
  return request({
    url: '/sysAdmin/formProjectTemplate/queryFormProjectPublicTemplatePageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 根据项目模板key查询详情
export function queryFormProjectTemplateResByProjectId(data) {
  return request({
    url: 'sysAdmin/formProjectTemplate/queryFormProjectTemplateResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 根据项目模板id查询明细
export function queryFormProjectTemplateDetailsResByProjectId(data) {
  return request({
    url: 'sysAdmin/formProjectTemplate/queryFormProjectTemplateDetailsResByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 新增表单项目模板
export function saveFormProjectTemplate(data) {
  return request({
    url: 'sysAdmin/formProjectTemplate/saveFormProjectTemplate',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 新增项目id转另存为模板
export function saveFormProjectTransferTemplate(data) {
  return request({
    url: 'sysAdmin/formProjectTemplate/saveFormProjectTransferTemplate',
    method: 'post',
    data: JSON.stringify(data)
  })
}


// 修改表单项目模板
export function updateFormProjectTemplate(data) {
  return request({
    url: 'sysAdmin/formProjectTemplate/updateFormProjectTemplate',
    method: 'post',
    data: JSON.stringify(data)
  })
}

// 表单项目模板删除
export function deleteFormProjectTemplateByProjectId(data) {
  return request({
    url: '/sysAdmin/formProjectTemplate/deleteFormProjectTemplateByProjectId',
    method: 'post',
    data: JSON.stringify(data)
  })
}


