import request from '@/utils/request'

// 分页查询子表单项目主题list
export function queryFormProjectThemePageResList(data) {
  return request({
    url: '/sysAdmin/formProjectTheme/queryFormProjectThemePageResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}
