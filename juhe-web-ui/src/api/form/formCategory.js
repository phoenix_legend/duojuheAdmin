import request from '@/utils/request'

// 根据条件查询所有可供前端选择使用的表单分类
export function queryNormalSelectFormCategoryResList(data) {
  return request({
    url: '/sysAdmin/formCategory/queryNormalSelectFormCategoryResList',
    method: 'post',
    data: JSON.stringify(data)
  })
}
