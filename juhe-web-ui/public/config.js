/**
 * 公共配置文件
 */
window.Glob = {
  //多聚合系统/生产环境
  APP_TITLE:'多聚合',
  //多聚合系统 服务端接口地址 结尾不要带/
  APP_API_URL:'http://47.243.228.8:9070',
  //多聚合系统 websocket接口地址 结尾不要带/
  APP_SOCKET_URL:'ws://47.243.228.8:9070'
}
