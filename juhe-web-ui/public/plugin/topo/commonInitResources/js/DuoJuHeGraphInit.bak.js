var userInfo = JSON.parse(sessionStorage.getItem("UserInfo"));
var token = userInfo.token;
var signKey = userInfo.signKey;
var apiDomainName = userInfo.apiDomainName;
document.write('<link rel="stylesheet" type="text/css" href="commonInitResources/static/layui/css/layui.css">');
document.write('<link rel="stylesheet" type="text/css" href="commonInitResources/styles/grapheditor.css">');
document.write('<link rel="stylesheet" type="text/css" href="commonInitResources/static/css/diyMyCommon.css">');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/static/js/jquery-1.9.1.min.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/static/layui/layui.all.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/static/js/md5.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/static/js/diyMyCommon.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/js/Init.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/deflate/pako.min.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/deflate/base64.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/jscolor/jscolor.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/sanitizer/sanitizer.min.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/static/js/mxClient.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/js/EditorUi.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/js/Editor.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/js/Sidebar.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/js/DiySidebar.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/js/Graph.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/js/Format.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/js/Shapes.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/js/Actions.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/js/Menus.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/js/Toolbar.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="commonInitResources/js/Dialogs.js"></scr'+'ipt>');
function toPoUi(editor,container) {
  const topo = urlParams['topo'];
  if (topo === null || topo === "" || topo===undefined){
    document.body.innerHTML = '<center style="margin-top:10%;">Error loading resource files</center>';
    return;
  }
  if (editor.chromeless){
    previewToPoUi(editor,container,topo)
  }else{
    editToPoUi(editor,container,topo)
  }
}

function editToPoUi(editor,container,topo) {
  topoView = false;
  const topoId = JSON.stringify({'topoId': topo});
  const timestamp = new Date().getTime();
  $.ajax({
    type: "POST",
    url: apiDomainName+"/sysAdmin/topo/topology/initEditTopologyByTopoId",
    data:topoId,
    headers: {
      "sign":createSign(topoId,timestamp),
      "timestamp": timestamp,
      "token":token
    },
    contentType: "application/json;charset-UTF-8",
    dataType: "json",
    success: function (result) {
      if (result.errorCode !== 0) {
        document.body.innerHTML = '<center style="margin-top:10%;">'+result.message+'</center>';
        return;
      }
      document.title=result.data.topoName;
      EditorUi.prototype.setXmlData(result.data.xmlData);
      EditorUi.prototype.setClassData(result.data.classResList);
      var editorUiInit = EditorUi.prototype.init;
      EditorUi.prototype.init = function()
      {
        editorUiInit.apply(this, arguments);
        this.actions.get('export').setEnabled(true);
        if (!Editor.useLocalStorage)
        {
          var enabled = true;
          this.actions.get('open').setEnabled(enabled);
          this.actions.get('import').setEnabled(enabled);
          this.actions.get('save').setEnabled(enabled);
          this.actions.get('saveAs').setEnabled(enabled);
          this.actions.get('export').setEnabled(enabled);
        }
      };
      mxResources.loadDefaultBundle = false;
      var bundle = mxResources.getDefaultBundle(RESOURCE_BASE, mxLanguage) ||  mxResources.getSpecialBundle(RESOURCE_BASE, mxLanguage);
      mxResources.parse(mxUtils.load(bundle).getText());
      new EditorUi(editor,document.getElementById(container));
    },
    error: function () {
      document.body.innerHTML = '<center style="margin-top:10%;">Error loading resource files</center>';
    }
  });
}

function previewToPoUi(editor,container,topo) {
  topoView = true;
  const topoId = JSON.stringify({'topoId': topo});
  const timestamp = new Date().getTime();
  $.ajax({
    type: "POST",
    url: apiDomainName+"/sysAdmin/topo/topology/queryViewTopologyByTopoId",
    data:topoId,
    headers: {
      "sign":createSign(topoId,timestamp),
      "timestamp": timestamp,
      "token":token
    },
    contentType: "application/json;charset-UTF-8",
    dataType: "json",
    success: function (result) {
      if (result.errorCode !== 0) {
        document.body.innerHTML = '<center style="margin-top:10%;">'+result.message+'</center>';
        return;
      }
      document.title=result.data.topoName;
      EditorUi.prototype.setXmlData(result.data.xmlData);
      mxResources.loadDefaultBundle = false;
      var bundle = mxResources.getDefaultBundle(RESOURCE_BASE, mxLanguage) ||  mxResources.getSpecialBundle(RESOURCE_BASE, mxLanguage);
      mxResources.parse(mxUtils.load(bundle).getText());
      new EditorUi(editor,document.getElementById(container));
    },
    error: function () {
      document.body.innerHTML = '<center style="margin-top:10%;">Error loading resource files</center>';
    }
  });
}
