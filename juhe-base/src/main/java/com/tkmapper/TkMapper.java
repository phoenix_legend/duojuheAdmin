package com.tkmapper;


import com.tkmapper.special.SpecialBatchMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * 自定义通用mapper
 *
 * @date 2018/2/9.
 */
public interface TkMapper<T> extends Mapper<T>, MySqlMapper<T>, SpecialBatchMapper<T> {
}
