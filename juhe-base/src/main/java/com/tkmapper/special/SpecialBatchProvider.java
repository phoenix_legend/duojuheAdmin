package com.tkmapper.special;

import org.apache.ibatis.mapping.MappedStatement;
import tk.mybatis.mapper.entity.EntityColumn;
import tk.mybatis.mapper.mapperhelper.EntityHelper;
import tk.mybatis.mapper.mapperhelper.MapperHelper;
import tk.mybatis.mapper.mapperhelper.MapperTemplate;
import tk.mybatis.mapper.mapperhelper.SqlHelper;

import java.util.Set;

/**
 * 自定义mapper方法，主要是解决批量插入/更新问题(主键字段也插入)
 *
 * @since : 2017年1月5日
 */
public class SpecialBatchProvider extends MapperTemplate {
    public SpecialBatchProvider(Class<?> mapperClass, MapperHelper mapperHelper) {
        super(mapperClass, mapperHelper);
    }

    /**
     * 批量插入
     * @param ms
     * @return
     */
    public String batchInsertListUseAllCols(MappedStatement ms) {
        Class<?> entityClass = getEntityClass(ms);
        //当前使用数据库
        String identity = getIDENTITY();
        //开始拼sql
        StringBuilder sql = new StringBuilder();
        sql.append(SqlHelper.insertIntoTable(entityClass, tableName(entityClass)));
        sql.append(SqlHelper.insertColumns(entityClass, false, false, false));
        if ("ORACLE".equals(identity)){
            sql.append("<foreach collection=\"list\" index=\"index\" item=\"record\" separator=\"union all\" >");
            sql.append("( SELECT ");
            //获取全部列
            Set<EntityColumn> columnList = EntityHelper.getColumns(entityClass);
            //当某个列有主键策略时，不需要考虑他的属性是否为空，因为如果为空，一定会根据主键策略给他生成一个值
            int i = 0;
            for (EntityColumn column : columnList) {
                i++;
                if (column.isInsertable()) {
                    if (i==columnList.size()){
                        sql.append(column.getColumnHolder("record"));
                    }else{
                        sql.append(column.getColumnHolder("record")).append(",");
                    }
                }
            }
            sql.append(" from dual )");
            sql.append("</foreach>");
            return sql.toString();
        }else{
            //否则是mysql
            sql.append(" VALUES ");
            sql.append("<foreach collection=\"list\" item=\"record\" separator=\",\" >");
            sql.append("<trim prefix=\"(\" suffix=\")\" suffixOverrides=\",\">");
            //获取全部列
            Set<EntityColumn> columnList = EntityHelper.getColumns(entityClass);
            //当某个列有主键策略时，不需要考虑他的属性是否为空，因为如果为空，一定会根据主键策略给他生成一个值
            for (EntityColumn column : columnList) {
                if (column.isInsertable()) {
                    sql.append(column.getColumnHolder("record")).append(",");
                }
            }
            sql.append("</trim>");
            sql.append("</foreach>");
            return sql.toString();
        }
    }



    /**
     * 根据主键批量更新
     * @param ms
     * @return
     */
    public String batchUpdateListUseAllColsByPrimaryKey(MappedStatement ms) {
        final Class<?> entityClass = getEntityClass(ms);
        //开始拼sql
        StringBuilder sql = new StringBuilder();
        //循环开始
        sql.append("<foreach collection=\"list\" item=\"record\" separator=\";\">");
        //update语句
        sql.append(SqlHelper.updateTable(entityClass, tableName(entityClass)));
        //set语句
        sql.append(SqlHelper.updateSetColumns(entityClass, "record", true, isNotEmpty()));
        //where语句
        sql.append("<where>");
        //获取全部列
        Set<EntityColumn> columnList = EntityHelper.getPKColumns(entityClass);
        //当某个列有主键策略时，不需要考虑他的属性是否为空，因为如果为空，一定会根据主键策略给他生成一个值
        for (EntityColumn column : columnList) {
            if (column.isId() && column.isUpdatable()) {
                sql.append(" and ").append(column.getColumnEqualsHolder("record"));
                break;
            }
        }
        sql.append("</where>");
        //循环结束
        sql.append("</foreach>");
        return sql.toString();
    }
}
