package com.tkmapper.special;

import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.UpdateProvider;
import tk.mybatis.mapper.annotation.RegisterMapper;

import java.util.List;

/**
 * 自定义mapper方法，主要是解决批量插入(主键字段也插入)，批量更新（根据主键更新）
 *
 * @author echo
 */
@RegisterMapper
public interface SpecialBatchMapper<T> {

    /**
     * 批量插入数据库，所有字段都插入，包括主键
     *
     * @return
     */
    @Options(keyProperty = "id")
    @InsertProvider(type = SpecialBatchProvider.class, method = "batchInsertListUseAllCols")
    void batchInsertListUseAllCols(List<T> recordList);



    /**
     * 根据主键选择性批量更新，建议每次最多批量处理1000条，超过建议分批处理
     */
    @UpdateProvider(type = SpecialBatchProvider.class, method = "batchUpdateListUseAllColsByPrimaryKey")
    int batchUpdateListUseAllColsByPrimaryKey(List<T> recordList);
}
