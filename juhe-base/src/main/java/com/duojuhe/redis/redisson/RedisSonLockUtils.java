package com.duojuhe.redis.redisson;

import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;

import java.util.concurrent.TimeUnit;

public class RedisSonLockUtils {
    private static DistributedLocker locker;

    public static void setLocker(DistributedLocker locker) {
        RedisSonLockUtils.locker = locker;
    }


    //lock(), 拿不到lock就不罢休，不然线程就一直block
    public static RLock lock(String lockKey) {
        return locker.lock(lockKey);
    }

    //leaseTime为加锁时间，单位为秒
    public static RLock lock(String lockKey, long leaseTime) {
        return locker.lock(lockKey,leaseTime);
    }

    //timeout为加锁时间，时间单位由unit确定
    public static RLock lock(String lockKey, long timeout,TimeUnit unit) {
        return locker.lock(lockKey,timeout,unit);
    }



    //公平锁（Fair Lock）
    //lock(), 拿不到lock就不罢休，不然线程就一直block
    public static RLock fairLock(String lockKey) {
        return locker.fairLock(lockKey);
    }

    //timeout为加锁时间，单位为秒
    public static RLock fairLock(String lockKey, long timeout) {
        return locker.fairLock(lockKey,timeout);
    }

    //timeout为加锁时间，时间单位由unit确定
    public static RLock fairLock(String lockKey, long timeout,TimeUnit unit) {
        return locker.fairLock(lockKey,timeout,unit);
    }



    //tryLock()，马上返回，拿到lock就返回true，不然返回false。
    //带时间限制的tryLock()，拿不到lock，就等一段时间，超时返回false.
    public static boolean tryLock(String lockKey, long waitTime, long leaseTime,TimeUnit unit) {
      return locker.tryLock(lockKey,waitTime,leaseTime,unit);
    }

    //tryLock()，马上返回，拿到lock就返回true，不然返回false。
    //带时间限制的tryLock()，拿不到lock，就等一段时间，超时返回false.【单位秒】
    public static boolean tryLock(String lockKey,long waitTime, long leaseTime) {
        return locker.tryLock(lockKey,waitTime,leaseTime);
    }

    //tryLock()，马上返回，拿到lock就返回true，不然返回false。【至少一秒】
    //带时间限制的tryLock()，等待1秒钟，超时返回false.
    public static boolean tryLock(String lockKey,long leaseTime) {
        return locker.tryLock(lockKey,leaseTime);
    }

    //tryLock()，马上返回，拿到lock就返回true，不然返回false。【公平锁】
    //带时间限制的tryLock()，拿不到lock，就等一段时间，超时返回false.
    public static boolean fairTryLock(String lockKey, long waitTime) {
        return locker.fairTryLock(lockKey,waitTime);
    }


    //tryLock()，马上返回，拿到lock就返回true，不然返回false。【公平锁】
    //带时间限制的tryLock()，拿不到lock，返回false.
    public static boolean fairTryLock(String lockKey) {
        if (StringUtils.isBlank(lockKey)){
            return false;
        }
        return locker.fairTryLock(lockKey);
    }

    //释放指定key锁
    public static void unlock(String lockKey) {
        if (StringUtils.isNotBlank(lockKey)){
            locker.unlock(lockKey);
        }
    }

    //释放锁
    public static void unlock(RLock lock) {
        if (lock!=null){
            locker.unlock(lock);
        }
    }
}
