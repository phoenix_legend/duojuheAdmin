package com.duojuhe.redis.redisson;

import org.redisson.api.RLock;

import java.util.concurrent.TimeUnit;

public interface DistributedLocker {

    //可重入锁（Reentrant Lock）
    //lock(), 拿不到lock就不罢休，不然线程就一直block
    RLock lock(String lockKey);

    //leaseTime为加锁时间，单位为秒
    RLock lock(String lockKey, long timeout);

    //timeout为加锁时间，时间单位由unit确定
    RLock lock(String lockKey, long timeout, TimeUnit unit);


    //公平锁（Fair Lock）
    //lock(), 拿不到lock就不罢休，不然线程就一直block
    RLock fairLock(String lockKey);

    //leaseTime为加锁时间，单位为秒
    RLock fairLock(String lockKey, long timeout);

    //timeout为加锁时间，时间单位由unit确定
    RLock fairLock(String lockKey, long timeout, TimeUnit unit);

    //tryLock()，马上返回，拿到lock就返回true，不然返回false。
    //带时间限制的tryLock()，拿不到lock，就等一段时间，超时返回false.
    boolean tryLock(String lockKey, long waitTime, long leaseTime, TimeUnit unit);

    //tryLock()，马上返回，拿到lock就返回true，不然返回false。
    //带时间限制的tryLock()，拿不到lock，就等一段时间，超时返回false.【单位秒】
    boolean tryLock(String lockKey, long waitTime, long leaseTime);

    //tryLock()，马上返回，拿到lock就返回true，不然返回false。【至少一秒】
    //带时间限制的tryLock()，等待1秒钟，超时返回false.
    boolean tryLock(String lockKey, long leaseTime);

    //tryLock()，马上返回，拿到lock就返回true，不然返回false。
    //带时间限制的tryLock()，拿不到lock，就等一段时间，超时返回false.【单位秒】
    boolean fairTryLock(String lockKey, long waitTime);

    //fairTryLock()，马上返回，拿到lock就返回true，不然返回false。【公平锁】
    //带时间限制的tryLock()，拿不到lock，返回false.
    boolean fairTryLock(String lockKey);

    //释放指定key锁
    void unlock(String lockKey);

    //释放锁
    void unlock(RLock lock);
}
