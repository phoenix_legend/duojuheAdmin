package com.duojuhe.common.extend.form.attribute;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Event extends BaseBean {
    @ApiModelProperty(value = "请求接口")
    private String apiUrl;
}
