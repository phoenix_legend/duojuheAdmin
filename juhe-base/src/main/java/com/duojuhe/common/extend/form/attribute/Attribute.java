package com.duojuhe.common.extend.form.attribute;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Attribute extends BaseBean {
    @ApiModelProperty(value = "属性key标识")
    private String key;

    @ApiModelProperty(value = "属性名称")
    private String name;

    @ApiModelProperty(value = "描述说明")
    private String desc;

    @ApiModelProperty(value = "提示文字")
    private String tips;

    @ApiModelProperty(value = "属性表单类型，text，file等等")
    private String type;

    @ApiModelProperty(value = "是否必须填写标识")
    private Boolean required;

    @ApiModelProperty(value = "错误提示语句，当required==true是有效")
    private String requiredMessage;

    @ApiModelProperty(value = "表单类型为 text 有效，最小长度，允许输入的长度")
    private Integer minlength;

    @ApiModelProperty(value = "表单类型为 textarea 有效，长度")
    private Integer rows;

    @ApiModelProperty(value = "表单类型为 text 有效，最大长度，允许输入的长度")
    private Integer maxlength;

    @ApiModelProperty(value = "填入值")
    private String value;

    @ApiModelProperty(value = "选项值")
    private List<OptionValues> optionValues;

}