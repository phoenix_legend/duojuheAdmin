package com.duojuhe.common.extend.form.attribute;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AttributeEvent extends BaseBean {
    @ApiModelProperty(value = "属性集合")
    private List<Attribute> attribute;

    @ApiModelProperty(value = "事件标识")
    private Event event;
}
