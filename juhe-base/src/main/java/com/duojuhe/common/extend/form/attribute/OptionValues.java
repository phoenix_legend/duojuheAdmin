package com.duojuhe.common.extend.form.attribute;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OptionValues extends BaseBean {
    @ApiModelProperty(value = "选项名称")
    private String name;

    @ApiModelProperty(value = "选项值")
    private String value;
}
