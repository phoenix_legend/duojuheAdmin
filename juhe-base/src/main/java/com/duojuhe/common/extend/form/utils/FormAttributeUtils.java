package com.duojuhe.common.extend.form.utils;

import com.duojuhe.common.exception.base.DuoJuHeException;
import com.duojuhe.common.extend.form.attribute.Attribute;
import com.duojuhe.common.extend.form.attribute.AttributeParameter;
import com.duojuhe.common.result.ErrorCodes;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FormAttributeUtils {

    /**
     * 构建返回参数
     * @param attributeList
     * @return
     */
    public static List<Attribute> buildAttributeList(Map<String, AttributeParameter> parameterMap, List<Attribute> attributeList){
        for (Attribute attribute:attributeList){
            String key = attribute.getKey();
            if (StringUtils.isNotBlank(key)){
                AttributeParameter attributeParameter = parameterMap.get(key);
                attribute.setValue(attributeParameter!=null?attributeParameter.getValue():"");
            }
        }
        return attributeList;
    }


    /**
     * 构建返回参数
     * @param attributeList
     * @return
     */
    public static List<AttributeParameter> buildAttributeParameterList(Map<String, AttributeParameter> parameterMap, List<Attribute> attributeList){
        //返回的参数
        List<AttributeParameter> ispAttributeRes = new ArrayList<>();
        for (Attribute attribute:attributeList){
            //key
            String key = attribute.getKey();
            if (StringUtils.isBlank(attribute.getKey())){
                continue;
            }
            AttributeParameter parameter = parameterMap.get(key);
            if (parameter == null) {
                throw new DuoJuHeException(ErrorCodes.SMS_ISP_ATTRIBUTE_FORMAT_WRONG);
            }
            if (attribute.getRequired()) {
                if (StringUtils.isBlank(parameter.getValue())) {
                    //存在必填的没填写
                    throw new DuoJuHeException(attribute.getName()+"参数必须填写!");
                }
            }
            AttributeParameter ispAttributeParameter = new AttributeParameter();
            ispAttributeParameter.setKey(key);
            ispAttributeParameter.setValue(StringUtils.isBlank(parameter.getValue())?"":parameter.getValue());
            ispAttributeRes.add(ispAttributeParameter);
        }
        return ispAttributeRes;
    }
}
