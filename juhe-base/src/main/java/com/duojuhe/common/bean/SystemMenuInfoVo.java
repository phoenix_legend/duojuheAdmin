package com.duojuhe.common.bean;


import com.duojuhe.common.utils.tree.TreeBaseBean;

public class SystemMenuInfoVo extends TreeBaseBean {
    //主键
    private String menuId;

    //父级id
    private String parentId;

    //菜单编码
    private String menuCode;

    //菜单名称
    private String menuName;

    //菜单请求地址
    private String apiUrl;

    //前端组件地址
    private String component;

    //菜单小图标
    private String menuIcon;

    //功能类型 NAVIGATION 导航 BUTTON 功能按钮
    private String menuTypeCode;

    //是否是外链:YES是，NO否
    private String menuFrameCode;

    //是否缓存:YES是，NO否
    private String menuCacheCode;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    @Override
    public String getParentId() {
        return parentId;
    }

    @Override
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getMenuTypeCode() {
        return menuTypeCode;
    }

    public void setMenuTypeCode(String menuTypeCode) {
        this.menuTypeCode = menuTypeCode;
    }

    public String getMenuFrameCode() {
        return menuFrameCode;
    }

    public void setMenuFrameCode(String menuFrameCode) {
        this.menuFrameCode = menuFrameCode;
    }

    public String getMenuCacheCode() {
        return menuCacheCode;
    }

    public void setMenuCacheCode(String menuCacheCode) {
        this.menuCacheCode = menuCacheCode;
    }
}
