package com.duojuhe.common.bean.struct;


public class ItemDefaultValueStruct {
    /**
     * 值是否是Json
     */
    private boolean json;
    /**
     * 默认值具体值
     */
    private Object value;

    public boolean isJson() {
        return json;
    }

    public void setJson(boolean json) {
        this.json = json;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public ItemDefaultValueStruct(){

    }


    public ItemDefaultValueStruct(Boolean json,Object defaultValue){
        this.json = json;
        this.value = defaultValue;
    }
}
