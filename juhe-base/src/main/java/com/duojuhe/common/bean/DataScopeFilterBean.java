package com.duojuhe.common.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DataScopeFilterBean extends BaseBean{
    @ApiModelProperty(value = "系统权限过滤参数字段，如果没有加DataScopeFilter 这个注解 mapper xml中一定不能加数据过滤", hidden=true)
    @JsonIgnore
    private String dataScopeFilter;
}
