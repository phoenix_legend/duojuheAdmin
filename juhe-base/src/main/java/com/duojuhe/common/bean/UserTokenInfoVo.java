package com.duojuhe.common.bean;


import java.util.Date;
import java.util.List;


public class UserTokenInfoVo extends BaseBean {
    //主键
    private String userId;

    //用户id来源
    private String userIdSource;

    //该登录用户归属系统类型SYSTEM_ADMIN=总后台
    private String userLoginSource;

    //用户类型 DEPT_ADMIN部门管理员
    private String userTypeCode;

    //用户所属模块
    private String userModuleCode;

    //用户类型中文名
    private String userTypeName;

    //登录名
    private String loginName;

    //用户编号，具有唯一
    private String userNumber;

    //租户编号，具有唯一
    private String tenantNumber;

    //租户名称唯一
    private String tenantName;

    //租户简称
    private String tenantAbbreviation;

    //真实姓名
    private String realName;

    //最后登录IP
    private String loginIp;

    //IP所属区域
    private String ipRegion;

    //登录时间
    private Date loginTime;

    //用户创建时间
    private Date createTime;

    //辅助签名密钥
    private String signKey;

    //token
    private String token;

    //拥有的菜单权限
    private List<SystemMenuInfoVo> menuInfoVOList;

    //创建部门ID
    private String createDeptId;

    //用户角色
    private String roleId;

    //岗位ID
    private String postId;

    //租户ID
    private String tenantId;

    //sm4 加解密密钥key
    private String sm4Key;


    //是否被踢出
    private Boolean kickOut;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserIdSource() {
        return userIdSource;
    }

    public void setUserIdSource(String userIdSource) {
        this.userIdSource = userIdSource;
    }

    public String getUserLoginSource() {
        return userLoginSource;
    }

    public void setUserLoginSource(String userLoginSource) {
        this.userLoginSource = userLoginSource;
    }

    public String getUserTypeCode() {
        return userTypeCode;
    }

    public void setUserTypeCode(String userTypeCode) {
        this.userTypeCode = userTypeCode;
    }

    public String getUserModuleCode() {
        return userModuleCode;
    }

    public void setUserModuleCode(String userModuleCode) {
        this.userModuleCode = userModuleCode;
    }

    public String getUserTypeName() {
        return userTypeName;
    }

    public void setUserTypeName(String userTypeName) {
        this.userTypeName = userTypeName;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getTenantNumber() {
        return tenantNumber;
    }

    public void setTenantNumber(String tenantNumber) {
        this.tenantNumber = tenantNumber;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getTenantAbbreviation() {
        return tenantAbbreviation;
    }

    public void setTenantAbbreviation(String tenantAbbreviation) {
        this.tenantAbbreviation = tenantAbbreviation;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public String getIpRegion() {
        return ipRegion;
    }

    public void setIpRegion(String ipRegion) {
        this.ipRegion = ipRegion;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getSignKey() {
        return signKey;
    }

    public void setSignKey(String signKey) {
        this.signKey = signKey;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<SystemMenuInfoVo> getMenuInfoVOList() {
        return menuInfoVOList;
    }

    public void setMenuInfoVOList(List<SystemMenuInfoVo> menuInfoVOList) {
        this.menuInfoVOList = menuInfoVOList;
    }

    public String getCreateDeptId() {
        return createDeptId;
    }

    public void setCreateDeptId(String createDeptId) {
        this.createDeptId = createDeptId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }


    public String getSm4Key() {
        return sm4Key;
    }

    public void setSm4Key(String sm4Key) {
        this.sm4Key = sm4Key;
    }

    public Boolean getKickOut() {
        return kickOut;
    }

    public void setKickOut(Boolean kickOut) {
        this.kickOut = kickOut;
    }
}
