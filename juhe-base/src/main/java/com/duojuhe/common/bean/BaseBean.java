package com.duojuhe.common.bean;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;

public class BaseBean implements Serializable {
    public BaseBean() {
    }

    public String toString() {
        return JSON.toJSONString(this);
    }
}
