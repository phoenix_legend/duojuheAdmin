package com.duojuhe.common.bean;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 带分页查询的统一定义bean，该bean带排序字段
 * @author eavan
 * @date 2018-05-14
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PageHead extends PageReq {
    @ApiModelProperty(value = "排序字段，多个请用英文逗号分隔", example = "id desc,createTime desc")
    private String orderBy;
}
