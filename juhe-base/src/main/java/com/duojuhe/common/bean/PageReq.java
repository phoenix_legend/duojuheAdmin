package com.duojuhe.common.bean;

import com.duojuhe.common.utils.excel.ExcelField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * 带分页查询的统一定义bean
 * @author eavan
 * @date 2018-05-14
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PageReq extends DataScopeFilterBean {

    @ApiModelProperty(value = "当前页数", example = "1")
    private int page = 1;

    @ApiModelProperty(value = "每页多少数据，等于0则表示查询所有不分页", example = "10")
    private int rows = 10;

    @ApiModelProperty(value = "导出excel列头和字段集合,不为空则按照指定列头和字段导出")
    private List<ExcelField> excelFieldList;

}
