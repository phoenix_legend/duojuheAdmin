package com.duojuhe.common.bean.struct;

import com.duojuhe.common.bean.BaseBean;

import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class NodeUserStruct extends BaseBean {
    //节点名称
    @NotBlank(message = "节点名称不可为空")
    private String name;

    //节点类型，1用户 2角色 3部门 4岗位
    @NotNull(message = "节点类型不能为空")
    @Range(min = 1, max = 4, message = "节点类型参数不正确")
    private Integer type;

    //目标ID
    @NotBlank(message = "目标ID不可为空")
    private String targetId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }
}
