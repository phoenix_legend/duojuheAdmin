package com.duojuhe.common.result;

import com.duojuhe.common.bean.BaseBean;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


/**
 * 统一返回格式
 *
 * @date 2018/2/22.
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceResult<T> extends BaseBean {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "错误码，0 表示成功，其他都是失败建议 弹窗提示message参数内容）建议当errorCode小于0时请弹出提示后，重新定位到登录界面", example = "0")
    private Integer errorCode;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(value = "提示语", example = "操作成功")
    private String message;

    @ApiModelProperty(value = "响应内容")
    private T data;

    /**
     * 操作成功
     */
    public static ServiceResult ok(Object data) {
        return ServiceResult.builder()
                .errorCode(ErrorCodes.SUCCESS.getCode())
                .message(ErrorCodes.SUCCESS.getMessage())
                .data(data)
                .build();
    }



    /**
     * 操作成功
     */
    public static ServiceResult ok() {
        return ServiceResult.builder()
                .errorCode(ErrorCodes.SUCCESS.getCode())
                .message(ErrorCodes.SUCCESS.getMessage())
                .data(ErrorCodes.SUCCESS.getMessage())
                .build();
    }
    /**
     * 操作成功 可自定义成功响应描述
     *
     * @param message
     * @param data
     * @return
     */
    public static ServiceResult ok(String message, Object data) {
        return ServiceResult.builder()
                .errorCode(ErrorCodes.SUCCESS.getCode())
                .message(message)
                .data(data)
                .build();
    }


    /**
     * 操作成功 可自定义成功响应描述
     *
     * @param message
     * @return
     */
    public static ServiceResult ok(String message) {
        return ServiceResult.builder()
                .errorCode(ErrorCodes.SUCCESS.getCode())
                .message(ErrorCodes.SUCCESS.getMessage())
                .data(message)
                .build();
    }

    /**
     * 返回失败原因信息
     */
    public static ServiceResult fail(ErrorCodes errorCode) {
        return ServiceResult.builder()
                .errorCode(errorCode.getCode())
                .message(errorCode.getMessage())
                .data(errorCode.getMessage())
                .build();
    }


    /**
     * 返回失败原因信息
     */
    public static ServiceResult fail(String message) {
        return ServiceResult.builder()
                .errorCode(ErrorCodes.FAIL.getCode())
                .message(message)
                .data(message)
                .build();
    }

    /**
     * 返回失败原因信息
     */
    public static ServiceResult fail() {
        return ServiceResult.builder()
                .errorCode(ErrorCodes.FAIL.getCode())
                .message(ErrorCodes.FAIL.getMessage())
                .data(ErrorCodes.FAIL.getMessage())
                .build();
    }
}
