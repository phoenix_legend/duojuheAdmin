package com.duojuhe.common.result;

import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * 系统错误码
 *
 * @date 2018/2/23.
 */
public enum ErrorCodes {
    /**
     * 错误码
     */
    LOGIN_IP_AND_REQUEST_IP_ERROR(-3, "当前IP与登录时IP发生变化，为了您的账号安全，请重新登录!"),
    KICK_OUT_ERROR(-2, "已被强制踢出，请您重新登录"),
    TOKEN_ERROR(-1, "登录状态已失效，请您重新登录！"),
    SUCCESS(0, "操作成功"),
    LOCK_SCREEN_ERROR(100000, "该账户已经被锁屏！"),

    FAIL(1, "操作失败"),
    PARAM_ERROR(2, "参数错误"),
    NOT_SUPPORTED_EXPORT_EXCEL(3, "该接口不支持导出Excel"),
    PARENT_PARAM_ERROR(4, "父级参数错误"),
    UPDATE_SUCCESS_AGAIN_LOGIN(5, "修改成功,请重新登录!"),
    PARENT_NOT_SELF(5, "上级不可以是自己"),
    DUPLICATE_SUBMIT_ERROR(6, "请勿重复请求"),
    REQUEST_TOO_FREQUENTLY_ERROR(7, "访问过于频繁，请稍后再试"),
    LOGIN_NAME_OR_PASSWORD_ERROR(8, "登录名或登录密码输入错误"),
    TOKEN_IS_NULL_ERROR(9, "请求token不可为空"),
    TIMESTAMP_IS_NULL_ERROR(10, "请求timestamp不可为空"),
    SIGN_ERROR(11, "Sign签名错误"),
    UPLOAD_FILE_TOO_LARGE(12, "文件太大！"),
    UPLOAD_FILE_NAME_LENGTH_ERROR(12, "文件名称太长,建议小于100个字符！"),
    UPLOAD_FILE_NOT_FOUND(13, "文件不存在！"),
    UPLOAD_FILE_FORMAT_ERROR(14, "文件格式不正确！"),
    UPLOAD_FILE_ERROR(15, "文件上传失败！"),
    REDIS_ERROR(16, "Redis操作失败"),
    GOOGLE_VERIFY_CODE_ERROR(17, "谷歌验证码错误"),
    NO_OPEN_GOOGLE_VERIFY(18, "您暂未开启谷歌验证，请联系客服"),
    REQUEST_TOO_MANY_PEOPLE_ERROR(19, "抱歉当前操作人过多，请稍后重试"),
    NOT_REPEAT_PROCESS_ERROR(20, "请勿重复处理"),
    USER_ONLINE(21, "当前用户在线!"),
    USER_NOT_EXIST(22, "用户不存在"),
    USER_FORBID_USE(23, "该账号已被禁止使用!"),
    USER_CONFIRM_PASSWORD_ERROR(24, "确认密码和登录密码输入不一致"),
    USER_OLD_PASSWORD_ERROR(25, "旧密码输入错误"),
    USER_NEW_OLD_PASSWORD_ERROR(25, "新密码和旧密码不能相同"),
    USER_LOGIN_NAME_EXIST(26, "登录名已存在"),
    ROLE_NAME_EXIST(27, "角色名称已存在"),
    TENANT_NAME_EXIST(28, "租户名称已存在"),
    ROLE_NOT_EXIST(29, "角色不存在"),
    ROLE_FORBID(203, "角色被禁用"),
    ROLE_RELATION_USER_NOT_DELETE(12, "角色已被绑定，禁止删除！"),
    ROLE_IS_USE_CANNOT_FORBID(204, "角色存在关联用户，不能禁用"),
    DEPT_NOT_EXIST(11, "部门不存在"),
    DEPT_RELATION_USER_NOT_DELETE(201, "部门存在关联用户，禁止删除"),
    DEPT_CODE_EXIST(201, "部门编码已存在"),
    DEPT_NAME_EXIST(201, "部门名称已存在"),
    BUILT_IN_DEPT_NOT_UPDATE_PARENT(11, "内置部门，禁止调整上级部门"),
    PARENT_DEPT_ERROR(11, "父级部门参数错误"),
    PARENT_DEPT_NOT_SELF(11, "父级部门不可以是自己"),
    EXIST_CHILDREN_DEPT_NOT_DELETE(11, "存在子部门，禁止删除"),
    PARENT_DEPT_NOT_SELF_CHILDREN(11, "父级部门不可以是自己下级部门"),
    MENU_CODE_EXIST(202, "菜单编码已存在"),
    EXIST_CHILDREN_MENU_TYPE_NOT_FUNCTION(11, "存在子菜单时菜单类型不可为功能按钮类型"),
    PARENT_MENU_TYPE_NOT_FUNCTION(11, "父级菜单不可为功能按钮类型"),
    PARENT_MENU_NOT_SELF(11, "父级菜单不可以是自己"),
    PARENT_MENU_NOT_SELF_CHILDREN(11, "父级菜单不可以是自己下级菜单"),
    EXIST_CHILDREN_MENU_NOT_DELETE(11, "存在子菜单，禁止删除"),
    BUILT_IN_MENU_NOT_UPDATE(11, "内置菜单，禁止修改"),
    BUILT_IN_MENU_NOT_DELETE(11, "内置菜单，禁止删除"),
    CRON_EXPRESSION_ERROR(500, "时间表达式格式错误!"),
    MEMBER_GROUP_NAME_EXIST(300, "分组名称已存在"),
    MEMBER_GROUP_NOT_EXIST(301, "分组信息不存在"),
    MEMBER_USER_NOT_EXIST(301, "会员不存在"),
    MEMBER_USER_FROZEN(302, "该账号已被冻结,请联系管理人员"),
    MEMBER_USER_LOGIN_NAME_EXIT(303, "登录名已存在"),
    NO_AUTHORIZATION_ERROR(1000, "抱歉,您暂无该功能使用权限"),
    GET_ACTION_CAPTCHA_ERROR(400, "获得行为验证码图片失败"),
    CHECK_CAPTCHA_ERROR(400, "验证失败"),
    APP_ID_ERROR(400, "appId错误"),
    APP_ID_IS_NULL_ERROR(11, "请求AppId不可为空"),

    DICT_BUILT_IN_NOT_DELETE(11, "系统数据字典，禁止删除"),
    DICT_BUILT_IN_NOT_UPDATE(11, "系统数据字典，禁止编辑"),
    DICT_PARENT_IS_BUILT_IN_ERROR(11, "父级字典是不可为内置字典"),
    EXIST_CHILDREN_DICT_NOT_DELETE(11, "存在子项数据字典，禁止删除"),
    DICT_PARENT_DICT_IS_BUILT_IN_ERROR(11, "父级字典是不可为内置字典"),

    DICT_CODE_EXIST(202, "字典编码已存在"),
    SYSTEM_PARAMETER_CODE_EXIST(202, "系统参数编码已存在"),
    SYSTEM_PARAMETER_BUILT_IN_NOT_DELETE(11, "内置系统参数，禁止删除"),
    POST_NOT_EXIST(11, "岗位不存在!"),
    POST_NAME_EXIST(11, "岗位名称已存在!"),
    POST_CODE_EXIST(11, "岗位编码已存在!"),
    POST_RELATION_USER_NOT_DELETE(12, "岗位存在绑定用户，禁止删除！"),
    POST_IS_USE_CANNOT_FORBID(204, "岗位存在关联用户，不能禁用"),
    MOBILE_NUMBER_EXIST(7, "该手机号码已存在!"),
    CAPTCHA_CODE_ERROR(51, "图形验证码输入错误!"),

    SOCKET_REQUEST_BODY_EMPTY(1010, "报文不可为空!"),
    SOCKET_REQUEST_BODY_ERROR(1012, "报文格式错误!"),

    EMOTICON_ITEM_NOT_FOUND(21, "表情不存在！"),
    FRIEND_TRUE_ERROR(21, "已是好友，请勿重复申请！"),
    NOT_GROUP_USER_NO_TALK_ERROR(21, "暂不属于群组成员，无法进行聊天！"),

    NOT_FRIENDS_NO_TALK_ERROR(21, "暂不属于好友关系，无法进行聊天！"),
    NOT_FRIEND_ERROR(21, "暂不属于好友关系！"),
    NOT_GROUP_USER_NO_TALK_RECORD_ERROR(21, "暂不属于群组成员，无法查看聊天记录！"),
    NOT_FRIENDS_NO_TALK_RECORD_ERROR(21, "暂不属于好友关系，无法查看聊天记录！"),

    SMS_REQUEST_ERROR(10000, "短信发送失败，请求接口出现异常"),
    SMS_TEMPLATE_NAME_EXIST(10001, "短信模板名称已存在！"),
    SMS_TEMPLATE_NOT_UPDATE(11, "内置短信模板，禁止修改"),
    SMS_TEMPLATE_NOT_DELETE(11, "内置短信模板，禁止删除"),
    SMS_TEMPLATE_NOT_EXIST(10001, "短信模板不存在"),
    SMS_TEMPLATE_CONTENT_NOT_EXIST(10002, "短信模板内容为空不存在"),
    SMS_TEMPLATE_WAITING_SEND_TASK_EXIST(10007, "该短信模板存在待发送的短信任务，禁止该操作!"),

    SMS_PHONE_CANNOT_NULL(10003, "手机号不能为空"),
    SMS_CODE_CANNOT_NULL(10004, "验证码不能为空"),
    SMS_PHONE_ERROR(10005, "手机号格式错误"),
    SMS_CODE_SUCCESS(0, "验证码校验成功!"),
    SMS_CODE_FAIL(10007, "验证码校验失败!"),
    SMS_TEMPLATE_CODE_EXIST(201, "短信模板编码已存在"),
    SMS_ISP_NOT_EXIST(10007, "短信服务商不存在或被禁用!"),
    SMS_ISP_ATTRIBUTE_FORMAT_WRONG(16, "短信服务商属性参数格式错误"),
    SMS_CHANNEL_NOT_EXIST(10007, "短信渠道不存在或被禁用!"),
    SMS_CHANNEL_NAME_EXIST(10007, "短信渠道名称已存在!"),
    SMS_SEND_TASK_TIMING_TIME_ERROR(10007, "定时发送时间不可小于当前时间!"),
    SMS_CHANNEL_WAITING_SEND_TASK_EXIST(10007, "该渠道存在待发送的短信任务，禁止该操作!"),
    SMS_CHANNEL_EXIST_SMS_TEMPLATE_ERROR(10007, "该渠道存在关联模板，禁止该操作!"),

    SMS_CHANNEL_NOT_UPDATE(11, "内置短信渠道，禁止修改"),
    SMS_CHANNEL_NOT_DELETE(11, "内置短信渠道，禁止删除"),

    SMS_SEND_TASK_NAME_EXIST(10007, "短信发送任务名称已存在!"),
    SMS_SEND_TASK_DETAIL_ERROR(10007, "短信发送内容参数错误!"),


    FORM_PROJECT_NO_PUBLISHED(201, "问卷项目为发布禁止提交数据"),
    FORM_PROJECT_NO_FORM_ITEM(201, "问卷项目暂无任何表单项，请设计表单"),

    QUARTZ_JOB_NAME_EXIST(201, "任务名称已存在"),
    QUARTZ_JOB_CLASS_PATH_NO_WHITELIST(201, "任务执行路径不在白名单范围内"),
    QUARTZ_JOB_CLASS_PATH_METHOD_NAME_EXIST(201, "该执行类下的执行方法已经存在!请勿重复加入!"),
    QUARTZ_JOB_NOT_UPDATE(11, "内置任务，禁止修改"),
    QUARTZ_JOB_NOT_DELETE(11, "内置任务，禁止删除"),
    QUARTZ_JOB_DELETE_FAIL(11, "任务删除失败"),
    QUARTZ_JOB_UPDATE_FAIL(11, "任务更新失败"),
    QUARTZ_JOB_CREATE_FAIL(11, "任务创建失败"),
    QUARTZ_JOB_PAUSE_FAIL(11, "任务暂停失败"),
    QUARTZ_JOB_RESUME_FAIL(11, "任务恢复失败"),
    QUARTZ_JOB_RUN_FAIL(11, "任务执行失败"),

    CHAT_SEND_TALK_MESSAGE_FALL(11, "发送聊天消息失败"),
    CHAT_GROUP_SEND_TALK_MESSAGE_FALL(11, "群组发送聊天消息失败"),
    CHAT_GROUP_DISMISS_ERROR(11, "群组已解散禁止发送聊天消息"),
    CHAT_GROUP_MUTE_ERROR(11, "群组已全员禁言禁止发送聊天消息"),
    CHAT_GROUP_USER_MUTE_ERROR(11, "你已经被禁言禁止发送聊天消息"),
    CHAT_GROUP_USER_QUIT_ERROR(11, "你已经退出群组禁止发送聊天消息"),

    FORM_PROJECT_CATEGORY_NOT_EXIST(201, "分类不存在"),

    INFO_CATEGORY_NOT_EXIST(201, "分类不存在"),
    INFO_CATEGORY_EXIST_CHILDREN_NOT_DELETE(11, "信息分类存在子分类，禁止删除"),
    INFO_CATEGORY_PARENT_NOT_SELF(11, "上级分类不可以是自己"),
    INFO_CATEGORY_PARENT_NOT_SELF_CHILDREN(11, "上级分类不可以是自己下级分类"),
    INFO_CATEGORY_NAME_EXIST(201, "分类名称已存在"),
    INFO_CATEGORY_RELATION_NEWS_NOT_DELETE(12, "信息分类存在关联发布信息，禁止删除!"),
    TOPO_LOGY_NOT_EXIST(500, "拓扑图不存在"),
    TOPO_ELEMENT_CLASS_NOT_EXIST(201, "分类不存在"),
    TOPO_ELEMENT_CLASS_RELATION_ELEMENT_NOT_DELETE(12, "图元分类存在关联图元，禁止删除!"),
    WORKFLOW_SUSPENDED_ERROR(12, "流程已被挂起,请先激活流程！"),
    WORKFLOW_PROCESS_INSTANCE_ERROR(12, "不存在运行的流程实例,请确认!"),
    WORKFLOW_CANCEL_ERROR(12, "流程未启动或已执行完成，取消申请失败!"),
    WORKFLOW_PROJECT_RELATION_RECORD_NOT_DELETE(11, "流程模型存在关联未结束的流程记录，禁止该操作！"),

    TENANT_FORBID_USE(9, "账号所属租户已被禁止使用!"),
    TRACKING_PRODUCT_CATEGORY_RELATION_PRODUCT_NOT_DELETE(12, "产品分类存在关联产品，禁止删除！"),
    TRACKING_PRODUCT_CATEGORY_NOT_EXIST(201, "分类不存在"),
    TRACKING_PRODUCT_NOT_EXIST(201, "产品不存在"),
    TRACKING_PRODUCT_SUPPLIER_NAME_EXIST(10007, "供货商名称已存在!"),
    TRACKING_PRODUCT_SUPPLIER_NOT_EXIST(10007, "供货商不存!"),
    TRACKING_PRODUCT_NAME_EXIST(10007, "产品名称已存在!"),
    TRACKING_PRODUCT_CODE_EXIST(10007, "产品编码已存在!"),
    TRACKING_PRODUCT_BARCODE_EXIST(10007, "产品条码已存在!"),
    TRACKING_CODE_EXIST(10007, "防伪码已存在!"),

    TRACKING_CATEGORY_RELATION_ITEM_NOT_DELETE(12, "流程类别存在关联流程项目，禁止删除！"),
    TRACKING_CATEGORY_NOT_EXIST(201, "分类不存在"),
    TRACKING_PRODUCT_SUPPLIER_RELATION_TRACKING_CODE_NOT_DELETE(12, "产品供货商存在关联防伪码，禁止删除！"),
    TRACKING_PRODUCT_RELATION_TRACKING_CODE_NOT_DELETE(12, "产品存在关联防伪码，禁止删除！"),

    HELP_CATEGORY_NOT_EXIST(201, "分类不存在"),
    HELP_CATEGORY_EXIST_CHILDREN_NOT_DELETE(11, "帮助分类存在子分类，禁止删除"),
    HELP_CATEGORY_PARENT_NOT_SELF(11, "上级分类不可以是自己"),
    HELP_CATEGORY_PARENT_NOT_SELF_CHILDREN(11, "上级分类不可以是自己下级分类"),
    HELP_CATEGORY_NAME_EXIST(201, "分类名称已存在"),
    HELP_CATEGORY_RELATION_NEWS_NOT_DELETE(12, "帮助分类存在关联发布信息，禁止删除!"),


    NOTICE_CATEGORY_RELATION_NOTICE_NOT_DELETE(12, "分类存在关通知公告，禁止删除！"),
    NOTICE_CATEGORY_NOT_EXIST(201, "分类不存在"),

    PAY_CHANNEL_NOT_UPDATE(11, "内置支付渠道，禁止修改"),
    PAY_CHANNEL_NOT_DELETE(11, "内置支付渠道，禁止删除"),
    PAY_ISP_NOT_EXIST(10007, "支付服务商不存在或被禁用!"),
    PAY_ISP_ATTRIBUTE_FORMAT_WRONG(16, "支付服务商属性参数格式错误"),
    PAY_CHANNEL_NOT_EXIST(10007, "支付渠道不存在或被禁用!"),
    PAY_CHANNEL_NAME_EXIST(10007, "支付渠道名称已存在!"),
    PAY_CHANNEL_CODE_PARAM_ERROR(10007, "支付渠道参数错误!"),
    PAY_ORDER_NOT_EXIST(201, "支付订单不存在!"),
    PAY_ORDER_STATUS_NOT_ERROR(935, "订单状态不支持该操作"),
    PAY_ORDER_IN_PROGRESS_ERROR(935, "订单等待付款中，请勿重复操作!"),

    SYSTEM_SAFE_REFERER_BUILT_IN_NOT_UPDATE(11, "内置安全白名单，禁止修改"),
    SYSTEM_SAFE_REFERER_BUILT_IN_NOT_DELETE(11, "内置安全白名单，禁止删除"),
    SYSTEM_SAFE_REFERER_EXIST(202, "安全白名单已存在"),
    APP_API_APP_ID_ERROR(1, "appId错误,请检查!"),
    APP_API_CATEGORY_NOT_EXIST(201, "接口分类不存在"),
    APP_API_CATEGORY_EXIST_CHILDREN_NOT_DELETE(11, "接口分类存在子分类，禁止删除"),
    APP_API_CATEGORY_PARENT_NOT_SELF(11, "上级分类不可以是自己"),
    APP_API_CATEGORY_PARENT_NOT_SELF_CHILDREN(11, "上级分类不可以是自己下级分类"),
    APP_API_CATEGORY_NAME_EXIST(201, "接口名称已存在"),
    APP_API_CATEGORY_RELATION_NEWS_NOT_DELETE(12, "接口分类存在关联接口，禁止删除!"),
    ;


    @Getter
    @Setter
    private int code;
    @Getter
    @Setter
    private String message;

    ErrorCodes(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static Map<Integer, String> map() {
        Map<Integer, String> map = Maps.newHashMap();
        for (ErrorCodes e : ErrorCodes.values()) {
            map.put(e.code, e.message);
        }
        return map;
    }
}
