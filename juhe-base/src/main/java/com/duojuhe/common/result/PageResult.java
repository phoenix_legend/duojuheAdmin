package com.duojuhe.common.result;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class PageResult<T> extends BaseBean {
    @ApiModelProperty(value = "记录总数", example = "12")
    private Long total;

    @ApiModelProperty(value = "响应内容，一般为list集合")
    private T records;

    public PageResult(T records,Long total) {
        this.total = total;
        this.records = records;
    }
}
