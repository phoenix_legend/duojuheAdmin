
package com.duojuhe.common.constant;

/**
 * 数据范围 ALL_DATA=全系统所有数据权限,TENANT_ALL_DATA=租户所有数据权限, CUSTOM_DATA=自定义数据权限,
 * SELF_DEPT=本部门数据权限,SELF_DEPT_SUBORDINATE=本部门及以下数据权限,SELF_DATA=自己的数据
 */
public class DataScopeConstants {

    //全系统所有数据权限
    public static final String ALL_DATA = "ALL_DATA";

    //自定义数据权限
    public static final String CUSTOM_DATA = "CUSTOM_DATA";

    //租户所有数据权限
    public static final String TENANT_ALL_DATA = "TENANT_ALL_DATA";

    //本部门数据权限
    public static final String SELF_DEPT = "SELF_DEPT";

    //本部门及以下数据权限
    public static final String SELF_DEPT_SUBORDINATE = "SELF_DEPT_SUBORDINATE";

    //自己的数据
    public static final String SELF_DATA = "SELF_DATA";
}

