package com.duojuhe.common.constant;

/**
 * 后台通用常量配置类
 *
 * @date 2018/2/9.
 */
public class ApiPathConstants {
    /**
     * 管理后台接口前缀（受后台权限拦截）
     */
    public static final String ADMIN_PATH = "sysAdmin";

    /**
     * System系统设置通用的拦截路径
     */
    public static final String COMMON_PATH = "sysCommon";
}
