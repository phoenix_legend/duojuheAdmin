package com.duojuhe.common.constant;


import java.util.Date;

/**
 * 通用常量配置类
 *
 */
public class SystemConstants {
    /** 排除敏感属性字段 */
    public static final String[] EXCLUDE_PROPERTIES = { "password", "oldPassword", "newPassword", "token", "sign","signKey" };
    /**
     * 未知id或者菜单的根节点标识或者系统特殊标识主键
     */
    public static final String UNKNOWN_ID= "-1";

    /**
     * 系统ip 127.0.0.1
     */
    public static final String SYSTEM_IP = "127.0.0.1";

    /**
     * 系统启动开始时间
     */
    public static final Date SYSTEM_START_DATE = new Date();

    /**
     * 系统安全标示key
     */
    public static final String DJH_SAFE_KEY= "djhSafeKey";

    /**
     * 作为系统判断系统是否初始化成功
     */
    public static boolean SYSTEM_INIT_SUCCESS = false;

    /**
     * 限流 redis key
     */
    public static final String RATE_LIMIT_KEY = "rate_limiter:";

    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";

    /**
     * 防重提交 redis key
     */
    public static final String REPEAT_SUBMIT_KEY = "repeat_submit:";

    /**
     * 用户处理锁 redis key
     */
    public static final String Lock_KEY = "lock_keys:";


    /**
     * 数据字典 前缀 redis key
     */
    public static final String DICT_KEY = "dict_keys:";

    /**
     * 系统参数 前缀 redis key
     */
    public static final String SYS_PARAMETER_KEY = "sys_parameters:";

    /**
     * APP info redis key
     */
    public static final String APP_INFO_KEY = "app_infos:";


    /**
     * APP api redis key
     */
    public static final String APP_API_KEY = "app_apis:";


    /**
     * 系统安全 前缀 redis key
     */
    public static final String SYS_REFERER_KEY = "sys_referers:";

    /**
     * 图形验证码 redis 缓存前缀
     */
    public final static String KAPTCHA_KEY = "kaptcha:redis";


    /**
     * 聊天消息 redis 缓存前缀
     */
    public final static String IM_CHAT_MQ_KEY = "im_chat_mq:redis";

    /**
     * 客服消息 redis 缓存前缀
     */
    public final static String KF_CHAT_MQ_KEY = "kf_chat_mq:redis";


    /**
     * 系统消息 redis 缓存前缀
     */
    public final static String SYSTEM_MESSAGE_MQ_KEY = "system_message_mq:redis";

    /**
     * 系统配置 redis 缓存前缀
     */
    public final static String SYSTEM_CONFIG_KEY = "system_config:redis";

    /**
     * 短信发送 redis 缓存前缀
     */
    public final static String SMS_SEND_REDIS_QUEUE = "sms:send:redis:queue";

    //短信发送失败后重发次数
    public final static Integer SMS_SEND_FAIL_REPEAT_NUMBER = 3;

    //群组最大人数
    public final static Integer GROUP_MAX_PEOPLE_NUMBER = 100;

}
