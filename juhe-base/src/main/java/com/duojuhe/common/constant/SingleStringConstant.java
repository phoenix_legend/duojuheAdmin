package com.duojuhe.common.constant;

/**
 * 常用字符串常量
 */
public class SingleStringConstant {

    public static final String MIN_VALUE = String.valueOf(Character.MIN_VALUE);

    public static final String EMPTY = "";

    public static final String ONE_SPACE = " ";

    public static final String NEWLINE = "\n";

    public static final String VERTICAL_LINE = "\\|";

    public static final String HORIZONTAL_LINE = "-";

    public static final String COMMA = ",";

    public static final String SEMICOLON = ";";

    public static final String APOSTROPHE = "'";

    public static final String COLON = ":";

    public static final String SPOT = ".";

}
