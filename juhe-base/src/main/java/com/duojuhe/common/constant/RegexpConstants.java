package com.duojuhe.common.constant;

/**
 * 正则常量
 */
public class RegexpConstants {
    //登录名
    public final static String LOGIN_NAME = "^[a-zA-Z0-9]{6,16}$";

    //真实姓名
    public final static String REAL_NAME = "^[\\u4E00-\\u9FA5a-zA-Z0-9_]*$";

    //短信签名
    public final static String SMS_SIGN = "^[\\u4E00-\\u9FA5a-zA-Z0-9_【】]*$";

    //登录密码
    public final static String PASSWORD = "^[0-9A-Za-z]{6,16}$";

    public final static String UPDATE_PASSWORD = "^[0-9A-Za-z]{6,16}|$";
    //手机号码
    public final static String MOBILE = "^1(3|4|5|6|7|8|9)\\d{9}$";
    //固定电话
    public final static String TELEPHONE = "^(\\d{3,4})-(\\d{5,8})$";
    //手机号码或固定电话
    public final static String MOBILE_TELEPHONE = "^(1(3|4|5|6|7|8|9)\\d{9})|((\\d{3,4})-(\\d{5,8}))$";

    //中文英文数字和下划线
    public final static String CH_EN_NUMBER_LINE = "^[\\u4E00-\\u9FA5a-zA-Z0-9_]*$";

    //中文和英文
    public final static String CH_EN_NAME = "^[\\u4E00-\\u9FA5a-zA-Z]*$";

    //只能输入英文字母,长度1-20个字符
    public final static String EN_LENGTH1_20 = "^[a-zA-Z]{1,20}$";

    //只能输入英文字母,长度1-50个字符
    public final static String EN_LENGTH1_50 = "^[a-zA-Z0-9_]{1,50}$";

    //只能输入英文字母和数字
    public final static String EN_LENGTH_NUMBER = "^[a-zA-Z0-9]*$";

    //只能输入英文字母和数字
    public final static String EN_LENGTH_NUMBER_LINE = "^[a-zA-Z0-9_]*$";

    //只能输入英文字母
    public final static String EN_LENGTH = "^[a-zA-Z]*$";

    //只能输入大写英文字母
    public final static String BIG_EN_LENGTH = "^[A-Z]*$";

    //备注可为空
    public final static String REMARK = "^( |[\\u4E00-\\u9FA5a-zA-Z0-9]|(\\\\s))*$";
    //备注不可为空
    public final static String REMARK_NOT_NULL = "^([\\u4E00-\\u9FA5a-zA-Z0-9]|(\\\\s))*$";

    //汉字长度2-4 姓名
    public final static String CH2_10 = "^[\\u4e00-\\u9fa5]{2,10}$";

    //身份证
    public final static String ID_CARD = "(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)";

    //短信验证码
    public final static String MOBILE_CODE = "^\\d{6}$";

    //只能输入英文字母，且长度1-20,允许为空
    public final static String EN_LENGTH1_20_OR_NULL = "^( |[a-zA-Z]){0,20}$";

    //英文数字下划线和点
    public static final String EN_NUMBER_LINE_POINT = "^[a-zA-Z0-9_\\-.]*$";

    //备注可为空
    public final static String REMARK_OR_NULL = "^( |[\\u4E00-\\u9FA5a-zA-Z0-9]|(\\\\s))*$";

    //只能输入数字，且长度1-3
    public final static String NUMBER_LENGTH1_3 = "^[0-9]{1,3}$";

    //后缀为图片格式
    public final static String SUFFIX_IMG =".+(.JPEG|.jpeg|.GIF|.gif|.JPG|.jpg|.png|.PNG)$";

    //可输入中英文和符号
    public final static String CH_EN_SYMBOL = "^[_,.!\\n\\w\\u4e00-\\u9fa5]*$";

    //出生日期的时间格式
    public final static String BIRTHDAY = "^\\s*$|^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$";

    //银行卡号
    public final static String BANK_CARD = "^([1-9]{1})(\\d{14}|\\d{18})$";

    //体现密码6位有效数字
    public final static String WITHDRAWALS_PASSWORD = "^\\d{6}$";

    //只能输入非零的正整数,,长度1-8位
    public final static String MONEY = "^(?:\\d{1,8}|\\d{1,3}(?:,\\d{3})+)?(?:\\.\\d{1,2})?$";

    //只能输入非负整数（包括0）,长度无限制
    public final static String NON_NEGATIVE_INT = "(^[1-9]+\\d*$)|(^0$)";

    //是或否
    public final static String YES_NO = "^[YES|NO]*$";

    //是或否
    public final static String YES_NO_NUMBER = "^[1|0]*$";

    //状态，正常或者禁用
    public final static String STATUS = "^[FORBID|NORMAL]*$";

    //是否是http url
    public final static String HTTP_URL="^([hH][tT]{2}[pP]://|[hH][tT]{2}[pP][sS]://)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\\\/])+$";

    //只能输入数字
    public final static String NUMBER = "^[0-9]*$";

    //yyyy-MM-dd
    public final static String YYYY_MM_DD = "^(19|20)\\d\\d[-/.]((0[1-9])|([1-9])|(1[0-2]))[-/.](([0-2][1-9])|([1-2]0)|(3[0-1])|([1-9]))$";

    //HH:mm时间格式
    public final static String HH_MM_TIME = "^(0\\d{1}|1\\d{1}|2[0-3]):([0-5]\\d{1})";

    //yyyy-MM-dd HH:mm
    public final static String YYYY_MM_DD_HH_MM = "^(19|20)\\d\\d[-/.]((0[1-9])|([1-9])|(1[0-2]))[-/.](([0-2][1-9])|([1-2]0)|(3[0-1])|([1-9])) (([0-1]\\d)|([1-9])|(2[0-3]|(0))):(([0-5]\\d)|([1-9]))$";

    //yyyy-MM-dd HH:mm:ss
    public final static String YYYY_MM_DD_HH_MM_TIME = "^(19|20)\\d\\d[-/.]((0[1-9])|([1-9])|(1[0-2]))[-/.](([0-2][1-9])|([1-2]0)|(3[0-1])|([1-9])) (([0-1]\\d)|([1-9])|(2[0-3]|(0))):(([0-5]\\d)|([1-9])):(([0-5]\\d)|([1-9]))$";
}
