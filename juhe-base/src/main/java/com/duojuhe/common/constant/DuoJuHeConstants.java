package com.duojuhe.common.constant;


/**
 * 通用常量配置类
 *
 */
public class DuoJuHeConstants {
    /**
     * 未知id或者菜单的根节点标识或者系统特殊标识主键
     */
    public static final String UNKNOWN_ID= "unknownId";

}
