package com.duojuhe.common.annotation;

import java.lang.annotation.*;

/**
 * 防止重复提交
 *
 * @author eavan
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface RepeatSubmit {

    /**
     * 是否开启重复提交验证，默认是
     *
     * @return
     */
    boolean submitForm() default true;

    /**
     * 指定组成防重复提交的key，以逗号分隔。
     * 如：lockKeyParts="name,age",则防重复提交的key为这两个字段value的拼接,当拼接最终key相同的则不会出现并发问题，一般建议用主键
     * key=params.getString("name")+params.getString("age")
     * repeatSubmitKeyParts 等于空，系统自动按照规则md5(请求参数的json请求体字符串 + requestURI)生成
     */
    String repeatSubmitKeyParts() default "";

    /**
     * 几秒之内无法重复提交请求(单位秒)
     *
     * @return
     */
    int expire() default 1;
}
