package com.duojuhe.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 锁注解
 * 添加到方法 ，修饰以@RequestBody JSON对象接收参数的方法
 *
 * @author eavan
 */
@Target(ElementType.METHOD) //注解在方法
@Retention(RetentionPolicy.RUNTIME)
public @interface KeyLock {
    /**
     * 指定组成分布式锁的key，以逗号分隔。
     * 如：lockKeyParts="name,age",则锁的key为这两个字段value的拼接,当拼接最终key相同的则不会出现并发问题，一般建议用主键
     * key=params.getString("name")+params.getString("age")
     * 当lockKeyParts 等于空，表示全局加锁
     */
    String lockKeyParts() default "allKey";

    /**
     * 是否固定key，当为true 时，固定取lockKeyParts的值作为key，否则用lockKeyParts值去取参数
     *
     * @return
     */
    boolean fixedKey() default false;


    /**
     * 获取锁等待时间（单位：秒），设置0表示无需一直等待，直到拿到锁
     */
    int waitTime() default 0;
}