package com.duojuhe.common.annotation;


import com.duojuhe.common.enums.log.LogEnum;

import java.lang.annotation.*;

/**
 * 自定义操作日志记录注解
 *
 * @author eavan
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OperationLog {
    /**
     * 模块
     */
    LogEnum.ModuleName moduleName() default LogEnum.ModuleName.OTHER;

    /**
     * 功能
     */
    LogEnum.OperationType operationType() default LogEnum.OperationType.OTHER;
}
