package com.duojuhe.common.annotation;


import com.duojuhe.common.enums.RateLimiterType;

import java.lang.annotation.*;

/**
 * 限流注解
 *
 * @author echo
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RateLimiter {
    /**
     * 限流时间,单位秒
     */
    int limitTime() default 60;

    /**
     * 限流次数 0表示不限制
     */
    int limitCount() default 100;

    /**
     * 限流类型
     */
    RateLimiterType limitType() default RateLimiterType.DEFAULT;
}
