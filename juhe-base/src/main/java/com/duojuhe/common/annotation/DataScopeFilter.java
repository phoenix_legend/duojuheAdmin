
package com.duojuhe.common.annotation;

import java.lang.annotation.*;

/**
 * 数据过滤
 *
 * 注意，数据层过滤控制生效要求：接口必须有一个参数，且参数必须继承 DataScopeFilterBean 类
 *
 * @author eavan
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DataScopeFilter {
    /**  租户ID */
    String tenantId() default "t.tenant_id";

    /**  部门ID */
    String deptId() default "t.create_dept_id";

    /**  用户ID */
    String userId() default "t.create_user_id";

    /** 特殊的 接口进行强制转换查询范围 ,该参数如果赋值了，以该字段优先级最高
     */
    String dataScope() default "";

    /** 是否开启数据过滤，如果是不为空则去只能查看到在system_data_filter表中关联数据 ,为空表示不启用
     */
    String filterTargetId() default "";
}

