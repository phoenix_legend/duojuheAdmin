package com.duojuhe.common.annotation;

import com.duojuhe.common.enums.FormattingTypeEnum;
import com.duojuhe.common.serializer.SensitiveInfoSerialize;
import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.lang.annotation.*;

/**
 * 序列化时格式化数据
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@JsonSerialize(using = SensitiveInfoSerialize.class)
@Documented
public @interface FieldFormatting {
    // 自定义属性处理(规则)(对字段属性进行注解格式化，按照一定规则处理，例如脱敏或者其他)
    FormattingTypeEnum formattingType() default FormattingTypeEnum.ID_CARD;
}
