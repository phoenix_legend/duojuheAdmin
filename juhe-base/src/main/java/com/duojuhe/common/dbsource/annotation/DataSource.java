package com.duojuhe.common.dbsource.annotation;

import java.lang.annotation.*;

/**
 * 数据源自定义注解，一般建议注解在service方法上
 */

@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DataSource {
    DataSourcesType value() default DataSourcesType.master;
}