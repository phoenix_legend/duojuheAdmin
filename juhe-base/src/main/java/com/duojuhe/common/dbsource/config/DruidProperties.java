package com.duojuhe.common.dbsource.config;


import com.alibaba.druid.pool.DruidDataSource;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Setter
@Getter
@Configuration
@ConfigurationProperties(prefix = "spring.datasource")
public class DruidProperties {
    //多数据源集合
    private Map<String, Map<String, String>> djh;

    //初始连接数
    private int initialSize;

    //最大连接池数量
    private int maxActive;

    //连接池中最小空闲连接池数量
    private int minIdle;

    //配置获取连接等待超时的时间
    private int maxWait;

    //配置一个连接在池中最小生存的时间，单位是毫秒
    private int minEvictableIdleTimeMillis;

    //配置一个连接在池中最大生存的时间，单位是毫秒
    private int maxEvictableIdleTimeMillis;

    //开启连接池回收
    private boolean removeAbandoned;

    //连接回收超时时间，单位秒
    private int removeAbandonedTimeout;

    //回收连接时打印日志
    private boolean logAbandoned;

    //借用连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能
    private boolean testOnBorrow;

    //归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能
    private boolean testOnReturn;

    //连接空闲时检测，如果连接空闲时间大于timeBetweenEvictionRunsMillis指定的毫秒，执行validationQuery指定的SQL来检测连接是否有效
    private boolean testWhileIdle;

    //空闲连接检查、废弃连接清理、空闲连接池大小调整的操作时间间隔，单位是毫秒(1分钟)
    private int timeBetweenEvictionRunsMillis;

    //检测连接是否有效时执行的sql命令
    private String validationQuery;

    //单位：秒，检测连接是否有效的超时时间。底层调用jdbc Statement对象的void setQueryTimeout(int seconds)方法，默认：-1
    private int validationQueryTimeout;


    public DruidDataSource dataSource(DruidDataSource datasource) {
        /**配置初始化大小、最小、最大 */
        datasource.setInitialSize(initialSize);
        datasource.setMaxActive(maxActive);
        datasource.setMinIdle(minIdle);

        /**配置获取连接等待超时的时间 */
        datasource.setMaxWait(maxWait);

        /**配置一个连接在池中最小、最大生存的时间，单位是毫秒 */
        datasource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        datasource.setMaxEvictableIdleTimeMillis(maxEvictableIdleTimeMillis);

        /**开启连接池回收*/
        datasource.setRemoveAbandoned(removeAbandoned);
        /**连接回收超时时间，单位秒*/
        datasource.setRemoveAbandonedTimeout(removeAbandonedTimeout);
        /**回收连接时打印日志*/
        datasource.setLogAbandoned(logAbandoned);

        /**建议配置为true，不影响性能，并且保证安全性。申请连接的时候检测，如果空闲时间大于timeBetweenEvictionRunsMillis，执行validationQuery检测连接是否有效。 */
        datasource.setTestWhileIdle(testWhileIdle);
        /**申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能。 */
        datasource.setTestOnBorrow(testOnBorrow);
        /**归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能。 */
        datasource.setTestOnReturn(testOnReturn);

        /**配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒 */
        datasource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        /**用来检测连接是否有效的sql，要求是一个查询语句，常用select 'x'。如果validationQuery为null，testOnBorrow、testOnReturn、testWhileIdle都不会起作用。*/
        datasource.setValidationQuery(validationQuery);
        /**单位：秒，检测连接是否有效的超时时间。底层调用jdbc Statement对象的void setQueryTimeout(int seconds)方法，默认：-1*/
        datasource.setValidationQueryTimeout(validationQueryTimeout);
        return datasource;
    }
}