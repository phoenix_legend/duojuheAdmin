package com.duojuhe.common.dbsource.annotation;

/**
 * 数据源类型
 */
public enum  DataSourcesType {
    /**
     * 主库
     */
    master,

    /**
     * 从库
     */
    slave,
}