package com.duojuhe.common.dbsource.provider;

import javax.sql.DataSource;
import java.util.Map;


public interface DynamicDataSourceProvider {
    /**
     * 加载所有的数据源
     *
     * @return
     */
    Map<String, DataSource> loadDataSources();
}

