package com.duojuhe.common.utils.appid;


import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

public class AppIdUtils {
    //请求头标识
    public static final String APP_ID_HEADER = "appId";
    /**
     * 获得appid字段
     * @param request
     * @return
     */
    public static String getAppIdByRequest(HttpServletRequest request){
        //token
        if (StringUtils.isBlank(request.getHeader(APP_ID_HEADER))){
            return request.getParameter(APP_ID_HEADER);
        }else{
            return request.getHeader(APP_ID_HEADER);
        }
    }
}


