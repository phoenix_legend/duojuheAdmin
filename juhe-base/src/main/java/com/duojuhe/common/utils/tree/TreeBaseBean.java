package com.duojuhe.common.utils.tree;

import com.duojuhe.common.bean.BaseBean;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

public class TreeBaseBean extends BaseBean {
    //主键id
    private String id;

    //父级id
    private String parentId;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<TreeBaseBean> children;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public List<TreeBaseBean> getChildren() {
        return children;
    }

    public void setChildren(List<TreeBaseBean> children) {
        this.children = children;
    }
}
