package com.duojuhe.common.utils.excel;

import com.duojuhe.common.exception.base.DuoJuHeException;
import com.duojuhe.common.utils.validate.ValidateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;


import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;



@Slf4j
public class ImportExcelUtil {

    /**
     * 导入excel
     * @param multipartFile
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> importExcel(MultipartFile multipartFile, final Class<?> clazz) throws IOException {
        if (multipartFile == null) {
            throw new DuoJuHeException("上传文件解析出错了，文件流是null");
        }
        return read(multipartFile.getInputStream(),clazz);
    }


    /**
     * 文件下载（失败了会返回一个有部分数据的Excel），用于直接把excel返回到浏览器下载
     */
    public static void downloadExcel(HttpServletResponse response, List<?> list, String sheetName) throws IOException {
        Class<?> clazz = list.get(0).getClass();
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = URLEncoder.encode(sheetName, "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), clazz).head(clazz).sheet(sheetName).doWrite(list);
    }

/*
    public static void main(String[] args) {
        @GetMapping("/downloadImportFileSubject")
        public void downloadImportFileSubject(HttpServletResponse response) throws IOException
        {
            List<OutEvalTopic> list = new ArrayList<>();
            list.add(new OutEvalTopic());
            EasyExcelUtil.downloadExcel(response, list, "模板");
        }
        @RequestMapping("/importExcelSubject")
        public Resp importExcelSubject(MultipartFile file) throws IOException {
            List<OutEvalTopic> list = EasyExcelUtil.read(file.getInputStream(),
                    OutEvalTopic.class);
            outEvalDeclareService.saveBatchTopic(list);
            return Resp.ok(list);
        }

    }*/


    /**
     * 导入excel
     * @param inputStream
     * @param clazz
     * @param <T>
     * @return
     */
    private static <T> List<T> read(InputStream inputStream, final Class<?> clazz) {
        if (inputStream == null) {
            throw new DuoJuHeException("解析出错了，文件流是null");
        }
        // 有个很重要的点 ExcelDataListener 不能被spring管理，要每次读取excel都要new,然后里面用到spring可以构造方法传进去
        ExcelDataListener<T> listener = new ExcelDataListener<>();
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
        EasyExcel.read(inputStream, clazz, listener).sheet().doRead();
        return listener.getRows();
    }

    /**
     * 根据文件路径导入
     * @param filePath
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> read(String filePath, final Class<?> clazz) {
        File f = new File(filePath);
        try (FileInputStream fis = new FileInputStream(f)) {
            return read(fis, clazz);
        } catch (FileNotFoundException e) {
            log.error("文件{}不存在", filePath, e);
            throw new DuoJuHeException("文件"+filePath+"不存在");
        } catch (IOException e) {
            log.error("文件读取出错", e);
            throw new DuoJuHeException("文件读取出错");
        }
    }


    /**
     * 导入excel
     * @param filePath
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> readFilePath(String filePath, final Class<?> clazz) {
        File f = new File(filePath);
        try (FileInputStream fis = new FileInputStream(f)) {
            return read(fis, clazz);
        } catch (FileNotFoundException e) {
            log.error("文件{}不存在", filePath, e);
        } catch (IOException e) {
            log.error("文件读取出错", e);
        }
        return null;
    }
}
@Slf4j
class ExcelDataListener<T> extends AnalysisEventListener<T> {

    private final List<T> rows = new ArrayList<>();

    @Override
    public void invoke(T t, AnalysisContext analysisContext) {
        log.info("解析到一条数据:{}", t);
        List<String> validResultList = ValidateUtil.validate(t);
        if (validResultList.size() > 0) {
            throw new DuoJuHeException(validResultList.get(0));
        }
        rows.add(t);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        log.info("解析完成！读取{}行", rows.size());
    }

    public List<T> getRows() {
        return rows;
    }


}
