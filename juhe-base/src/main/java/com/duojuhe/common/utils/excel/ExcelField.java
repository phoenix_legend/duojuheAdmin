package com.duojuhe.common.utils.excel;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ExcelField extends BaseBean {
    @ApiModelProperty(value = "导出字段名称", example = "ID名称")
    private String fieldName;

    @ApiModelProperty(value = "导出字段编码", example = "id")
    private String fieldCode;
}
