package com.duojuhe.common.utils.sqlutils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SqlInjectionUtils {
    /**
     * SQL语法检查正则：符合两个关键字（有先后顺序）才算匹配
     */
    private static final Pattern SQL_SYNTAX_PATTERN = Pattern.compile("(and|exec|insert|select|drop|grant|alter|delete|update|count|chr|mid|master|truncate|char|declare|or|create|deny|revoke|call|execute|show|rename|set|into|from|where|table|database|view|index|on|cursor|procedure|trigger|for|password|union)\\b|(\\*|;|\\+|'|%)");

    /**
     * 检查参数是否存在 SQL 注入
     *
     * @param value 检查参数
     * @return true 非法 false 合法
     */
    public static boolean check(String value) {
        if (StringUtils.isBlank(value)) {
            return false;
        }
        Matcher matcher = SQL_SYNTAX_PATTERN.matcher(value.toLowerCase());
        return matcher.find();
    }
}
