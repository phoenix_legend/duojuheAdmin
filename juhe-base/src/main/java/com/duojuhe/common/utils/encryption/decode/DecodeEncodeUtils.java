package com.duojuhe.common.utils.encryption.decode;

import org.apache.commons.lang3.StringUtils;

public class DecodeEncodeUtils {

    /**
     * 加密，把一个字符串在原有的基础上+1
     *
     * @param data 需要解密的原字符串
     * @return 返回解密后的新字符串
     */
    public static String encode(String data) {
        if (StringUtils.isBlank(data)){
            return "";
        }
        //把字符串转为字节数组
        byte[] b = data.getBytes();
        //遍历
        for (int i = 0; i < b.length; i++) {
            b[i] += 1;//在原有的基础上+1
        }
        return new String(b);
    }


    /**
     * 解密：把一个加密后的字符串在原有基础上-1
     *
     * @param data 加密后的字符串
     * @return 返回解密后的新字符串
     */
    public static String decode(String data) {
        if (StringUtils.isBlank(data)){
            return "";
        }
        //把字符串转为字节数组
        byte[] b = data.getBytes();
        //遍历
        for (int i = 0; i < b.length; i++) {
            b[i] -= 1;//在原有的基础上-1
        }
        return new String(b);
    }


    public static void main(String[] args) {
        //加密英文
        String data = "admin";
        String result = encode(data);
        System.out.println("加密后:" + result);
        //解密
        String str = decode(result);
        System.out.println("解密后:" + str);


        //加密中文
        data = "跳梁小豆tlxd666";
        result = encode(data);
        System.out.println("加密后:" + result);
        String str1 = decode(result);
        System.out.println("解密后:" + str1);
    }

}
