package com.duojuhe.common.utils.regexutil;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @date 2015-09-17 09:9:13
 */
public class RegexUtil {

    /**
     * 判断字符串是否为数字类型
     *
     * @param str
     * @return
     */
    public static boolean isNumber(String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        String pattern = "^[0-9]*$";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(str);
        return m.matches();
    }


    /**
     * 判断金额格式
     *
     * @param str
     * @return
     */
    public static boolean isMoney(String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        String reg = "^[0-9]+([.]{1}[0-9]{1,2})?$";
        if (str.matches(reg)) {
            return Double.parseDouble(str) > 0;
        }
        return false;
    }

    /**
     * 是否有特殊字符
     *
     * @param str
     * @return
     */
    public static boolean isSpecialString(String str) {
        if (StringUtils.isEmpty(str)) {
            return true;
        }
        String reg = "^[\\u4E00-\\u9FA5a-zA-Z0-9]*$";
        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(str);
        return !m.matches();
    }


    /**
     * 是否有特殊字符
     *
     * @param str
     * @return
     */
    public static boolean isSpecialString1(String str) {
        if (StringUtils.isEmpty(str)) {
            return true;
        }
        String reg = "^[\\u4E00-\\u9FA5a-zA-Z0-9_【】]*$";
        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(str);
        return m.matches();
    }


    /**
     * 是否有特殊字符
     *
     * @param str
     * @return
     */
    public static boolean isSpecialAccount(String str) {
        if (StringUtils.isEmpty(str)) {
            return true;
        }
        String reg = "^[\\u4E00-\\u9FA5a-zA-Z0-9@._-]*$";
        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(str);
        return !m.matches();
    }


    /**
     * 只能输入英文数字组合 长度在5-20之间
     *
     * @param str
     * @return
     */
    public static boolean isEN(String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        String reg = "^(?!([a-zA-Z]+|\\d+)$)[a-zA-Z\\d]{5,20}$";
        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(str);
        return m.matches();
    }

    /**
     * 只能输入英文和数字 长度在2-32之间
     *
     * @param str
     * @return
     */
    public static boolean isEnNumber(String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        String reg = "^[a-zA-Z0-9]{2,32}$";
        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(str);
        return m.matches();
    }

    /**
     * 只能输入英文和数字 长度在6-32之间  主要针对提现密码
     *
     * @param str
     * @return
     */
    public static boolean isEnNumberForPassword(String str) {
        if (StringUtils.isEmpty(str)) {
            return false;
        }
        String reg = "^[a-zA-Z0-9]{6,32}$";
        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(str);
        return m.matches();
    }


    /**
     * 判断是不是YYYY_MM_DD_HH_MM格式
     * @param str
     * @return
     */
    public static boolean isYYYY_MM_DD_HH_MM(String str){
        if(StringUtils.isBlank(str)){
            return false;
        }
        String reg = "^(19|20)\\d\\d[-/.]((0[1-9])|([1-9])|(1[0-2]))[-/.](([0-2][1-9])|([1-2]0)|(3[0-1])|([1-9])) (([0-1]\\d)|([1-9])|(2[0-3]|(0))):(([0-5]\\d)|([1-9]))$";
        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(str);
        return m.matches();
    }

    /**
     * 手机号码验证
     * @param str
     * @return
     */
    public static boolean isMobile(String str){
        if(StringUtils.isBlank(str)){
            return false;
        }
        String reg = "^1([3456789])\\d{9}$";
        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(str);
        return m.matches();
    }

    /**
     * 微信号格式数字字母下划线减号@和.
     *
     * @param str
     * @return
     */
    public static boolean checkWeChatNOIsOk(String str) {
        if (StringUtils.isBlank(str)) {
            return false;
        }
        String reg = "^[a-zA-Z0-9_\\-@.]{5,20}$";
        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(str);
        return m.matches();
    }


    /**
     * 推广域名 正确匹配URL
     *
     * @param str
     * @return
     */
    public static boolean isURL(String str) {
        if (StringUtils.isBlank(str)) {
            return false;
        }
        String reg = "^((ht|f)tps?):\\/\\/[\\w\\-]+(\\.[\\w\\-]+)+([\\w\\-.,@?^=%&:\\/~+#]*[\\w\\-@?^=%&\\/~+#])?$";
        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(str);
        return m.matches();
    }


    /**
     * 推广域名 正确匹配URL
     *
     * @param str
     * @return
     */
    public static boolean isHttp(String str) {
        if (StringUtils.isBlank(str)) {
            return false;
        }
        String reg="^([hH][tT]{2}[pP]://|[hH][tT]{2}[pP][sS]://)(([A-Za-z0-9-~]+).)+([A-Za-z0-9-~\\\\/])+$";
        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(str);
        return m.matches();
    }

    /**
     * 判断大于0的正整数
     *
     * @param str
     * @return
     */
    public static boolean isPositiveInteger(String str) {
        if (StringUtils.isBlank(str)) {
            return false;
        }
        String reg = "^\\+?[1-9]\\d*$";
        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(str);
        return m.matches();
    }

    /**
     * 判断时间格式是否为HH:mm
     *
     * @param str
     * @return
     */
    public static boolean isTimeFormatHHmm(String str) {
        if (StringUtils.isBlank(str)) {
            return false;
        }
        String reg = "^(?:[01]\\d|2[0-3])(?::[0-5]\\d)$";
        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(str);
        return m.matches();
    }

    /**
     * 判断时间格式是否为HH:mm:ss
     *
     * @param str
     * @return
     */
    public static boolean isTimeFormatHHmmSS(String str) {
        if (StringUtils.isBlank(str)) {
            return false;
        }
        String reg = "^([01]\\d|2[0-3]):[0-5]\\d:[0-5]\\d$";
        Pattern r = Pattern.compile(reg);
        Matcher m = r.matcher(str);
        return m.matches();
    }


    /**
     * 验证IP字符串是否合规
     *
     * @param ip ip字符串
     * @return true标识合规
     */
    public static boolean validIp(String ip) {
        if (StringUtils.isBlank(ip)) {
            return false;
        }
        String pattern = "((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})(\\.((2(5[0-5]|[0-4]\\d))|[0-1]?\\d{1,2})){3}";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(ip);
        return m.matches();
    }
}
