package com.duojuhe.common.utils.cronutil;

import org.apache.commons.lang3.StringUtils;
import org.quartz.CronExpression;

public class CronExpressionUtil {

    /**
     * 判断表达式是否正确 true 正确，false = 错误
     */
    public static boolean isValidExpression(String cronExpress) {
        if (StringUtils.isEmpty(cronExpress)) {
            return false;
        }
        return CronExpression.isValidExpression(cronExpress);
    }
}
