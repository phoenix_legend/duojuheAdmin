
package com.duojuhe.common.utils.random;

import java.util.Random;


public class RandomUtils {

    /**
     * 获取随机中文
     *
     * @return
     */
    public static String getRandomHan(String hanZi) {
        return hanZi.charAt(new Random().nextInt(hanZi.length())) + "";
    }

    /**
     * 随机范围内数字
     * @param startNum
     * @param endNum
     * @return
     */
    public static Integer getRandomInt(int startNum, int endNum) {
        return new Random().nextInt(endNum-startNum) + startNum;
    }
}
