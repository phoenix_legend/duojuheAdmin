package com.duojuhe.common.utils.idcard;


import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IdCardUtils {
    /**
     * 根据身份证号计算出生日期
     * @param idCard 身份证号
     * @return String
     */
    public static String getBirthDayByIdCard(String idCard) {
        String year;
        String month;
        String day;
        // 正则匹配身份证号是否是正确的，15位或者17位数字+数字/x/X
        if (idCard.matches("^\\d{15}|\\d{17}[\\dxX]$")) {
            year = idCard.substring(6, 10);
            month = idCard.substring(10, 12);
            day = idCard.substring(12, 14);
        }else {
            log.error("身份证号码不匹配！ idCard = {}", idCard);
            return "--";
        }
        return year + "-" + month + "-" + day;
    }
}
