package com.duojuhe.common.utils.encryption.jasypt;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentPBEConfig;

public class JasyptUtil {


    /**
     * 加密数据
     *
     * @param plainText //自己的密码
     * @param password  加密的密钥，随便自己填写，很重要千万不要告诉别人
     * @return
     * @throws Exception
     */
    public static String encrypt(String plainText, String password) throws Exception {
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        EnvironmentPBEConfig config = new EnvironmentPBEConfig();
        // 加密的算法，这个算法是默认的
        config.setAlgorithm("PBEWithMD5AndDES");
        // 加密的密钥，随便自己填写，很重要千万不要告诉别人
        config.setPassword(password);
        standardPBEStringEncryptor.setConfig(config);
        return standardPBEStringEncryptor.encrypt(plainText);
    }


    /**
     * 解密数据
     *
     * @param encryptedText //加密后的密码
     * @param password      加密的密钥，随便自己填写，很重要千万不要告诉别人
     * @throws Exception
     */
    public static String decrypt(String encryptedText, String password) throws Exception {
        StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
        EnvironmentPBEConfig config = new EnvironmentPBEConfig();
        config.setAlgorithm("PBEWithMD5AndDES");
        config.setPassword(password);
        standardPBEStringEncryptor.setConfig(config);
        return standardPBEStringEncryptor.decrypt(encryptedText);
    }

    public static void main(String[] args) throws Exception {
        //待加密字符串
        String plainText = "root";
        //加密的密钥
        String password = "duojuhe.com";
        //加密后的值
        String encryptedText = encrypt(plainText,password);
        //解密后的值
        String decryptedText = decrypt(encryptedText,password);
        System.out.println("加密后的值:" + encryptedText);
        System.out.println("解密后的值:" + decryptedText);
    }

}
