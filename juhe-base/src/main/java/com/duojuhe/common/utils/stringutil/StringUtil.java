package com.duojuhe.common.utils.stringutil;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public class StringUtil {

    /**
     * 查找指定字符串是否包含指定字符串列表中的任意一个字符串同时串忽略大小写
     *
     * @param cs 指定字符串
     * @param searchCharSequences 需要检查的字符串数组
     * @return 是否包含任意一个字符串
     */
    public static boolean containsAnyIgnoreCase(CharSequence cs, CharSequence... searchCharSequences) {
       try {
           if (StringUtils.isEmpty(cs) || ArrayUtils.isEmpty(searchCharSequences)) {
               return false;
           }
           for (CharSequence testStr : searchCharSequences) {
               if (StringUtils.containsIgnoreCase(cs, testStr)) {
                   return true;
               }
           }
           return false;
       }catch (Exception e){
           return false;
       }
    }

    /**
     * 截取指定长度的字符串
     *
     * @param str 原字符串
     * @param len 长度
     * @return 如果str为null，则返回null；如果str长度小于len，则返回str；如果str的长度大于len，则返回截取后的字符串
     */
    public static String subStringByStrAndLen(String str, int len) {
        if (StringUtils.isBlank(str)){
            return "";
        }
        return str.substring(0, Math.min(str.length(), len));
    }



    /**
     * 判断一个字符串是不是在另一个字符串中
     *
     * @param
     * @return
     */
    public static boolean isStrIndexOf(String str, String strOf) {
        if (StringUtils.isBlank(str)||StringUtils.isBlank(strOf)){
            return false;
        }
        return str.contains(strOf);
    }

}
