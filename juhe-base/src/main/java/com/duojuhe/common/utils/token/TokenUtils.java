package com.duojuhe.common.utils.token;


import com.duojuhe.common.utils.encryption.md5.MD5Util;
import com.duojuhe.common.utils.idgenerator.UUIDUtils;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

public class TokenUtils {
    //请求头标识
    public static final String TOKEN_HEADER = "token";
    /**
     * 获得时间戳字段
     * @param request
     * @return
     */
    public static String getTokenByRequest(HttpServletRequest request){
        //token
        if (StringUtils.isBlank(request.getHeader(TOKEN_HEADER))){
            return request.getParameter(TOKEN_HEADER);
        }else{
            return request.getHeader(TOKEN_HEADER);
        }
    }


    /**
     * 生成并加密一个header token 是由 uuid和当日时间组成
     *
     * @return
     */
    public static String createHeaderToken()  {
        return MD5Util.getMD532(UUIDUtils.getUUID32())+ MD5Util.getMD532(UUIDUtils.getSequenceNo());
    }
}


