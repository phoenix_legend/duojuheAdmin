package com.duojuhe.common.utils.listutil;

/**
 * @author echo
 * @date 2011-03-31 12:39:07
 */

import java.util.ArrayList;
import java.util.List;

public class PageModel {
    private int page = 1; // 当前页
    private int totalPages = 0; // 总页数
    private int pageRecorders;// 每页5条数据
    private int totalRows = 0; // 总数据数
    private int pageStartRow = 0;// 每页的起始数
    private int pageEndRow = 0; // 每页显示数据的终止数
    private boolean hasNextPage = false; // 是否有下一页
    private boolean hasPreviousPage = false; // 是否有前一页
    private boolean isFirstPage = false;// 是否是第一页
    private boolean isLastPage = false;// 是否是最后页
    private List list;


    public PageModel(List list, int pageRecorders) {
        init(list, pageRecorders);// 通过对象集，记录总数划分
    }


    /** */
    /**
     * 初始化list，并告之该list每页的记录数
     * @param list
     * @param pageRecorders
     */
    public void init(List list, int pageRecorders) {
        this.pageRecorders = pageRecorders;
        this.list = list;
        totalRows = list.size();
        hasPreviousPage = false;
        if ((totalRows % pageRecorders) == 0) {
            totalPages = totalRows / pageRecorders;
        } else {
            totalPages = totalRows / pageRecorders + 1;
        }
        this.isFirstPage = this.page == 1;
        this.isLastPage = this.page == this.totalPages || this.totalPages == 0;
        this.hasNextPage = page < totalPages;
        if (totalRows < pageRecorders) {
            this.pageStartRow = 0;
            this.pageEndRow = totalRows;
        } else {
            this.pageStartRow = 0;
            this.pageEndRow = pageRecorders;
        }
    }

    // 判断要不要分页
    public boolean isNext() {
        return list.size() > 5;
    }

    public void setHasPreviousPage(boolean hasPreviousPage) {
        this.hasPreviousPage = hasPreviousPage;
    }

    public String toString(int temp) {
        return Integer.toString(temp);
    }

    public void description() {
        String description = "共有数据数:" + this.getTotalRows()
                + "共有页数: " + this.getTotalPages()
                + "当前页数为:" + this.getPage()
                + " 是否有前一页: " + this.isHasPreviousPage()
                + " 是否有下一页:" + this.isHasNextPage()
                + " 是否第一页:" + this.isFirstPage()
                + " 是否最后一页:" + this.isLastPage()
                + " 开始行数:" + this.getPageStartRow()
                + " 终止行数:" + this.getPageEndRow();
    }

    public List getNextPage() {
        page = page + 1;
        disposePage();
        this.description();
        return getObjects(page);
    }

    /** */
    /**
     * 处理分页
     */
    private void disposePage() {

        if (page <= 0) {
            page = 1;
        }
        this.isFirstPage = this.page == 1;
        this.isLastPage = this.page == this.totalPages || this.totalPages == 0;
        if ((page - 1) > 0) {
            hasPreviousPage = true;
        } else {
            hasPreviousPage = false;
        }

        if (page >= totalPages) {
            hasNextPage = false;
            page = totalPages;
        } else {
            hasNextPage = true;
        }
    }

    public List getPreviousPage() {

        page = page - 1;

        if ((page - 1) > 0) {
            hasPreviousPage = true;
        } else {
            hasPreviousPage = false;
        }
        if (page >= totalPages) {
            hasNextPage = false;
            page = totalPages;
        } else {
            hasNextPage = true;
        }
        this.isFirstPage = this.page == 1;
        this.isLastPage = this.page == this.totalPages || this.totalPages == 0;
        this.description();
        return getObjects(page);
    }

    /**
     * 获取第几页的内容
     *
     * @param page
     * @return
     */
    public List getObjects(int page) {
        if (page <= 0) {
            this.setPage(1);
        } else {
            this.setPage(page);
        }
        this.disposePage();
        if (this.page * pageRecorders < totalRows) {// 判断是否为最后一页
            pageEndRow = this.page * pageRecorders;
            pageStartRow = pageEndRow - pageRecorders;
        } else {
            pageEndRow = totalRows;
            pageStartRow = pageRecorders * (totalPages - 1);
        }
        this.isFirstPage = this.page == 1;
        this.isLastPage = this.page == this.totalPages || this.totalPages == 0;
        List objects = new ArrayList();
        if (!list.isEmpty()) {
            objects = list.subList(pageStartRow, pageEndRow);
        }
        return objects;
    }

    public List getFistPage() {
        if (this.isNext()) {
            return list.subList(0, pageRecorders);
        } else {
            return list;
        }
    }

    public boolean isHasNextPage() {
        return hasNextPage;
    }

    public boolean isFirstPage() {
        return isFirstPage;
    }

    public void setFirstPage(boolean firstPage) {
        isFirstPage = firstPage;
    }

    public boolean isLastPage() {
        return isLastPage;
    }

    public void setLastPage(boolean lastPage) {
        isLastPage = lastPage;
    }

    public void setHasNextPage(boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageEndRow() {
        return pageEndRow;
    }

    public void setPageEndRow(int pageEndRow) {
        this.pageEndRow = pageEndRow;
    }

    public int getPageRecorders() {
        return pageRecorders;
    }

    public void setPageRecorders(int pageRecorders) {
        this.pageRecorders = pageRecorders;
    }

    public int getPageStartRow() {
        return pageStartRow;
    }

    public void setPageStartRow(int pageStartRow) {
        this.pageStartRow = pageStartRow;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public boolean isHasPreviousPage() {
        return hasPreviousPage;
    }

    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.add("8");
        list.add("9");
        list.add("10");
        list.add("11");
        list.add("12");
        list.add("13");
        list.add("14");
        PageModel pm = new PageModel(list, 2);//每页显示条数
        pm.getObjects(5);//显示第几页
        pm.description();

    }

    /**
     * 示例：查询当前在线admin用户信息
     * @param req
     * @return
     */
/*    public ServiceResult queryOnlineAdminUserList(PageReq req){
        List<OnlineAdminUserRes>  list = new ArrayList<>();
        for (AdminUserInfoDTO adminUserInfoDTO:adminUserTokenCache.getListAllAdminUserInfoDTOCache()){
            list.add(new OnlineAdminUserRes(adminUserInfoDTO));
        }
        if(req.getRows()==0){
            return ServiceResult.ok(new PageResult<>(list, (long)list.size()));
        }
        PageModel pm = new PageModel(list, req.getRows());//每页显示条数
        //页数
        int page = req.getPage();
        List sublist = pm.getObjects(page);//显示第几页
        pm.setList(sublist);
        return ServiceResult.ok(new PageResult<>(pm.getList(), (long)pm.getTotalRows()));
    }*/
}