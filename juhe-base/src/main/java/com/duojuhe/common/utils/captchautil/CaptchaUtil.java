package com.duojuhe.common.utils.captchautil;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * 验证码生成工具
 */
public class CaptchaUtil {
    private BufferedImage image;// 图像
    private String str;// 验证码
    private final static char[] CODE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789".toCharArray();
    private final static Integer CODE_NUM = 10;
    // 准备常用汉字集
    private final static String base = "\u7684\u4e00\u4e86\u662f\u6211\u4e0d\u5728\u4eba\u4eec\u6709\u6765\u4ed6\u8fd9\u4e0a\u7740\u4e2a\u5730\u5230\u5927\u91cc\u8bf4\u5c31\u53bb\u5b50\u5f97\u4e5f\u548c\u90a3\u8981\u4e0b\u770b\u5929\u65f6\u8fc7\u51fa\u5c0f\u4e48\u8d77\u4f60\u90fd\u628a\u597d\u8fd8\u591a\u6ca1\u4e3a\u53c8\u53ef\u5bb6\u5b66\u53ea\u4ee5\u4e3b\u4f1a\u6837\u5e74\u60f3\u751f\u540c\u8001\u4e2d\u5341\u4ece\u81ea\u9762\u524d\u5934\u9053\u5b83\u540e\u7136\u8d70\u5f88\u50cf\u89c1\u4e24\u7528\u5979\u56fd\u52a8\u8fdb\u6210\u56de\u4ec0\u8fb9\u4f5c\u5bf9\u5f00\u800c\u5df1\u4e9b\u73b0\u5c71\u6c11\u5019\u7ecf\u53d1\u5de5\u5411\u4e8b\u547d\u7ed9\u957f\u6c34\u51e0\u4e49\u4e09\u58f0\u4e8e\u9ad8\u624b\u77e5\u7406\u773c\u5fd7\u70b9\u5fc3\u6218\u4e8c\u95ee\u4f46\u8eab\u65b9\u5b9e\u5403\u505a\u53eb\u5f53\u4f4f\u542c\u9769\u6253\u5462\u771f\u5168\u624d\u56db\u5df2\u6240\u654c\u4e4b\u6700\u5149\u4ea7\u60c5\u8def\u5206\u603b\u6761\u767d\u8bdd\u4e1c\u5e2d\u6b21\u4eb2\u5982\u88ab\u82b1\u53e3\u653e\u513f\u5e38\u6c14\u4e94\u7b2c\u4f7f\u5199\u519b\u5427\u6587\u8fd0\u518d\u679c\u600e\u5b9a\u8bb8\u5feb\u660e\u884c\u56e0\u522b\u98de\u5916\u6811\u7269\u6d3b\u90e8\u95e8\u65e0\u5f80\u8239\u671b\u65b0\u5e26\u961f\u5148\u529b\u5b8c\u5374\u7ad9\u4ee3\u5458\u673a\u66f4\u4e5d\u60a8\u6bcf\u98ce\u7ea7\u8ddf\u7b11\u554a\u5b69\u4e07\u5c11\u76f4\u610f\u591c\u6bd4\u9636\u8fde\u8f66\u91cd\u4fbf\u6597\u9a6c\u54ea\u5316\u592a\u6307\u53d8\u793e\u4f3c\u58eb\u8005\u5e72\u77f3\u6ee1\u65e5\u51b3\u767e\u539f\u62ff\u7fa4\u7a76\u5404\u516d\u672c\u601d\u89e3\u7acb\u6cb3\u6751\u516b\u96be\u65e9\u8bba\u5417\u6839\u5171\u8ba9\u76f8\u7814\u4eca\u5176\u4e66\u5750\u63a5\u5e94\u5173\u4fe1\u89c9\u6b65\u53cd\u5904\u8bb0\u5c06\u5343\u627e\u4e89\u9886\u6216\u5e08\u7ed3\u5757\u8dd1\u8c01\u8349\u8d8a\u5b57\u52a0\u811a\u7d27\u7231\u7b49\u4e60\u9635\u6015\u6708\u9752\u534a\u706b\u6cd5\u9898\u5efa\u8d76\u4f4d\u5531\u6d77\u4e03\u5973\u4efb\u4ef6\u611f\u51c6\u5f20\u56e2\u5c4b\u79bb\u8272\u8138\u7247\u79d1\u5012\u775b\u5229\u4e16\u521a\u4e14\u7531\u9001\u5207\u661f\u5bfc\u665a\u8868\u591f\u6574\u8ba4\u54cd\u96ea\u6d41\u672a\u573a\u8be5\u5e76\u5e95\u6df1\u523b\u5e73\u4f1f\u5fd9\u63d0\u786e\u8fd1\u4eae\u8f7b\u8bb2\u519c\u53e4\u9ed1\u544a\u754c\u62c9\u540d\u5440\u571f\u6e05\u9633\u7167\u529e\u53f2\u6539\u5386\u8f6c\u753b\u9020\u5634\u6b64\u6cbb\u5317\u5fc5\u670d\u96e8\u7a7f\u5185\u8bc6\u9a8c\u4f20\u4e1a\u83dc\u722c\u7761\u5174\u5f62\u91cf\u54b1\u89c2\u82e6\u4f53\u4f17\u901a\u51b2\u5408\u7834\u53cb\u5ea6\u672f\u996d\u516c\u65c1\u623f\u6781\u5357\u67aa\u8bfb\u6c99\u5c81\u7ebf\u91ce\u575a\u7a7a\u6536\u7b97\u81f3\u653f\u57ce\u52b3\u843d\u94b1\u7279\u56f4\u5f1f\u80dc\u6559\u70ed\u5c55\u5305\u6b4c\u7c7b\u6e10\u5f3a\u6570\u4e61\u547c\u6027\u97f3\u7b54\u54e5\u9645\u65e7\u795e\u5ea7\u7ae0\u5e2e\u5566\u53d7\u7cfb\u4ee4\u8df3\u975e\u4f55\u725b\u53d6\u5165\u5cb8\u6562\u6389\u5ffd\u79cd\u88c5\u9876\u6025\u6797\u505c\u606f\u53e5\u533a\u8863\u822c\u62a5\u53f6\u538b\u6162\u53d4\u80cc\u7ec6";
    //备选字体
    private final static String[] fontTypes = {"\u5b8b\u4f53", "\u65b0\u5b8b\u4f53", "\u9ed1\u4f53", "\u6977\u4f53", "\u96b6\u4e66"};
    //0-9 加减乘除 零一二三四五六七八九十乘除加减
    private static final String UNICODE_NUMBERS = "\u96F6\u4E00\u4E8C\u4E09\u56DB\u4E94\u516D\u4E03\u516B\u4E5D\u5341\u4E58\u9664\u52A0\u51CF";
    private static final Map<String, Integer> OPMap = new HashMap<String, Integer>();

    static {
        OPMap.put("*", 11);
        OPMap.put("/", 12);
        OPMap.put("+", 13);
        OPMap.put("-", 14);
    }


    private CaptchaUtil(Integer captchaType) {
        init(captchaType);// 初始化属性
    }

    /*
     * 取得RandomNumUtil实例
     */
    public static CaptchaUtil Instance(Integer captchaType) {
        return new CaptchaUtil(captchaType);
    }

    /*
     * 取得验证码图片
     */
    public BufferedImage getImage() {
        return this.image;
    }

    /*
     * 取得图片的验证码
     */
    public String getString() {
        return this.str;
    }

    /**
     * @param captchaType 图形验证码模式：0=纯数字，1=字母和数字，2=纯中文
     */
    private void init(Integer captchaType) {
        // 在内存中创建图象
        int width = 85, height = 20;
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        // 获取图形上下文
        Graphics g = image.getGraphics();
        // 生成随机类
        Random random = new Random();
        g.setFont(new Font("LucidaBrightItalic", Font.BOLD, 20));
        // 设定背景色
        g.setColor(getRandColor(200, 250));
        g.fillRect(0, 0, width, height);
        // 设定字体
        g.setFont(new Font("Times New Roman", Font.PLAIN, 18));
        // 随机产生155条干扰线，使图象中的认证码不易被其它程序探测到
        g.setColor(getRandColor(40, 150));

        // 取随机产生的认证码(4位数字)
        StringBuilder sRand = new StringBuilder();
        if (3 == captchaType) {
            Map<String, String> map = getRandomMathString();
            String randomString = map.get("pic");
            sRand.append(map.get("jieguo"));
            for (int j = 0, k = randomString.length(); j < k; j++) {
                //画线条beg
                g.setColor(getRandColor(40, 100));
                int x = random.nextInt(width);
                int y = random.nextInt(height);
                int xl = random.nextInt(12);
                int yl = random.nextInt(12);
                g.drawLine(x, y, x + xl, y + yl);
                //画线条end
                int chid = 0;
                if (j == 1) {
                    chid = OPMap.get(String.valueOf(randomString.charAt(j)));
                } else {
                    chid = Integer.parseInt(String.valueOf(randomString.charAt(j)));
                }
                String ch = String.valueOf(UNICODE_NUMBERS.charAt(chid));
                //设置字体的颜色
                g.setColor(getRandColor(10, 150));
                //设置字体
                g.setFont(new Font(fontTypes[random.nextInt(fontTypes.length)],
                        Font.BOLD, 10 + random.nextInt(4)));
                //将此汉字画到图片上
                g.drawString(ch, 18 * j + 6 + random.nextInt(4), 16);
            }
            g.setFont(new Font(fontTypes[1],
                    Font.BOLD, 10 + random.nextInt(4)));
            g.drawString("=?", 18 * randomString.length() + 6, 16);
        } else {
            //old beg
            //画线条beg
            for (int i = 0; i < 10; i++) {
                g.setColor(getRandColor(40, 150));
                int x = random.nextInt(width);
                int y = random.nextInt(height);
                int xl = random.nextInt(12);
                int yl = random.nextInt(12);
                g.drawLine(x, y, x + xl, y + yl);
            }
            //画线条end
            for (int i = 0; i < 4; i++) {
                String rand;
                //图形验证码模式：0=纯数字，1=字母和数字，2=纯中文
                if (2 == captchaType) {
                    //纯中文
                    int start = random.nextInt(base.length());
                    rand = base.substring(start, start + 1);
                    sRand.append(rand);
                    //设置字体的颜色
                    g.setColor(getRandColor(10, 150));
                    //设置字体
                    g.setFont(new Font(fontTypes[random.nextInt(fontTypes.length)],
                            Font.BOLD, 12 + random.nextInt(6)));
                    //将此汉字画到图片上
                    g.drawString(rand, 20 * i + 6 + random.nextInt(4), 16);
                } else {
                    if (1 == captchaType) {
                        rand = String.valueOf(CODE[random.nextInt(CODE.length)]);
                    } else {
                        rand = String.valueOf(random.nextInt(CODE_NUM));
                    }
                    sRand.append(rand);
                    // 将认证码显示到图象中
                    g.setColor(new Color(20 + random.nextInt(110), 20 + random.nextInt(110), 20 + random.nextInt(110)));
                    // 调用函数出来的颜色相同，可能是因为种子太接近，所以只能直接生成
                    g.drawString(rand, 13 * i + 6, 16);
                }
            }
            //old end
        }
        // 赋值验证码
        this.str = sRand.toString();
        // 图象生效
        g.dispose();
        this.image = image;/* 赋值图像 */
    }

    /*
     * 给定范围获得随机颜色
     */
    private Color getRandColor(int fc, int bc) {
        Random random = new Random();
        if (fc > 255)
            fc = 255;
        if (bc > 255)
            bc = 255;
        int r = fc + random.nextInt(bc - fc);
        int g = fc + random.nextInt(bc - fc);
        int b = fc + random.nextInt(bc - fc);
        return new Color(r, g, b);
    }


    /**
     * 计算中文算法验证码
     */
    private static Map<String, String> getRandomMathString() {
        Map<String, String> re = new HashMap<>();
        //验证码结果
        int xyresult;
        Random random = new Random();
        int xx = random.nextInt(10);
        int yy = random.nextInt(10);
        StringBuilder suChinese = new StringBuilder();
        int Randomoperands = (int) Math.round(Math.random() * 2);
        if (Randomoperands == 0) {
            xyresult = yy * xx;
            suChinese.append(yy);
            suChinese.append("*");
            suChinese.append(xx);
        } else if (Randomoperands == 1) {
            if (!(xx == 0) && yy % xx == 0) {
                xyresult = yy / xx;
                suChinese.append(yy);
                suChinese.append("/");
                suChinese.append(xx);
            } else {
                xyresult = yy + xx;
                suChinese.append(yy);
                suChinese.append("+");
                suChinese.append(xx);
            }
        } else if (Randomoperands == 2) {
            xyresult = yy - xx;
            suChinese.append(yy);
            suChinese.append("-");
            suChinese.append(xx);
        } else {
            xyresult = yy + xx;
            suChinese.append(yy);
            suChinese.append("+");
            suChinese.append(xx);
        }
        //验证码
        String randomString = suChinese.toString();
        re.put("pic", randomString);
        re.put("jieguo", xyresult + "");
        return re;
    }


    public static void main(String[] args) {
     /*   CaptchaUtil util = CaptchaUtil.Instance(systemConfig.getCaptchaType());
        // 将验证码输入到保存到缓存中中，用来验证
        String code = util.getString();
        String uuid = UUIDUtils.getUUID32();
        ByteArrayOutputStream imgByte = new ByteArrayOutputStream();
        ImageIO.write(util.getImage(), "jpg", imgByte);
        byte[] bytes = imgByte.toByteArray();
        CaptchaRes captchaRes = new CaptchaRes();
        captchaRes.setImageId(uuid);
        captchaRes.setImageCode("data:image/jpeg;base64," + Base64Utils.encodeToString(bytes));
        CAPTCHA_CODE_CACHE.put(uuid + code, code);
        return ServiceResult.ok(captchaRes);*/
    }

}