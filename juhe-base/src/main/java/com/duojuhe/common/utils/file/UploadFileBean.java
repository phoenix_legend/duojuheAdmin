package com.duojuhe.common.utils.file;

import com.duojuhe.common.bean.BaseBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadFileBean extends BaseBean {
    @ApiModelProperty(value = "文件后缀名", example = ".png")
    private String fileSuffix;

    @ApiModelProperty(value = "原文件名", example = "测试.png")
    private String originalName;

    @ApiModelProperty(value = "文件大小", example = "100")
    private Long fileSize;

    @ApiModelProperty(value = "文件相对路径", example = "/20220108/hello.png")
    private String fileRelativePath;

    @ApiModelProperty(value = "新文件名", example = "1232354uuid.png")
    private String newFileName;

    @ApiModelProperty(value = "http访问路径", example = "http://www.duojuhe.com/20220108/1232354uuid.png")
    private String  fileAbsolutePath;

    @ApiModelProperty(value = "文件存储路径", example = "c:/website/hello.png")
    private String fileStoragePath;

    @ApiModelProperty(value = "文件类型")
    private String fileTypeCode;

}
