package com.duojuhe.common.utils.file;

import lombok.Getter;

public class FileTypeEnum {
    /**
     * 上传文件类型
     */
    public enum FileTypeCode {
        IMAGE_TYPE("IMAGE_TYPE", "图片类型"),
        ATTACHMENT_TYPE("ATTACHMENT_TYPE", "附件类型"),
        VIDEO_TYPE("VIDEO_TYPE", "视频类型"),
        OTHER_TYPE("OTHER_TYPE", "其他类型")
        ;
        @Getter
        private String key;
        @Getter
        private String value;
        FileTypeCode(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }
}
