package com.duojuhe.common.utils.validate;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Bean验证
 *
 * @date 2018/2/23.
 */
public class ValidateUtil {
    private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    /**
     * 验证参数是否合法(JSR303标准，参考地址：https://www.ibm.com/developerworks/cn/java/j-lo-jsr303/)
     *
     * @param obj 校验对象
     * @return 返回所有验证未通过的消息
     */
    public static List<String> validate(Object obj) {
      /*  ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();*/
        Set<ConstraintViolation<Object>> violationSet = validator.validate(obj);
        List<String> list = new ArrayList<>();
        if (violationSet.size() > 0) {
            for (ConstraintViolation violation : violationSet) {
                list.add(violation.getMessage());
            }
        }
        return list;
    }

	
	/* List<String> validResult = ValidateUtil.validate(updatePositionRecordReq);
        if (validResult.size() > 0) {
            ActivePushService.returnMessage(sessionId, ProtocolCode.UPDATE_USER_TRADE_POSITION_4095, ServiceResult.fail(validResult.get(0)));
            return;
        }*/

}
