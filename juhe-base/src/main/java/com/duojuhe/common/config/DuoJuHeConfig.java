package com.duojuhe.common.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;


/**
 * 系统配置
 *
 * @date 2018/5/29.
 */
@Component
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "duojuhe")
public class DuoJuHeConfig {

    /**
     * api域名配置，也可以是ip和端口
     */
    private String apiDomain = "";

    /**
     * 网站后台获取ip地址的请求头标识，多个请用英文逗号分割，获取的时候按照从左往右的顺序获取
     */
    private String getAdminIpHeader = "";


    /**
     * token 默认过期时间,单位分钟
     */
    private Integer tokenExpiresIn = 30;

    /**
     * 离线地址库
     * ip2region v2.0 - 是一个离线IP地址定位库和IP定位数据管理框架，10微秒级别的查询效率，提供了众多主流编程语言的 xdb 数据生成和查询客户端实现。
     */
    private String ip2regionXdb = "";

    /**
     * 是否启用多语言版本切换
     */
    private boolean language;
}
