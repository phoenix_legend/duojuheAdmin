package com.duojuhe.common.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;

import javax.servlet.MultipartConfigElement;
import java.io.File;

/**
 * <br>
 * <b>功能：</b>文件上传配置<br>
 * <b>日期：</b>2017/4/11 23:27<br>
 *
 */
@Data
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "upload")
public class FileUploadConfig {
    /**
     * 设置总上传数据总大小 KB
     */
    private Long maxRequestSize = 1024*1024L;

    /**
     * 所有上传文件根目录
     */
    private String rootPath = "/upload/file";

    /**
     * 设置上传附件前缀域名，留空则系统自动获取当前服务地址
     */
    private String filePrefixDomain;

    /**
     * 访问映射前缀
     */
    private String fileUrlPrefix = "/upload/file";

    /**
     * 网站根目录路径 默认c:/website
     */
    private String websiteRootPath = "/website";
    /**
     * 文件缓存位置
     */
    private String location = "/upload/tmp";
    /**
     * 统一文件大小限制 KB
     */
    private Long maxFileSize = 1024L;
    private String[] fileAllowFiles = {
            ".png", ".jpg", ".jpeg", ".gif", ".bmp",
            ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg",
            ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid",
            ".rar", ".zip", ".tar", ".gz", ".7z", ".bz2", ".cab", ".iso",
            ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf", ".txt", ".md", ".xml"
    };

    /**
     * 支付证书文件限制配置 KB
     */
    private Long maxPayCertFileSize = 1024L;
    private String[] filePayCertAllowFiles = {".crt"};


    /**
     * 设置图片大小限制 KB
     */
    private Long maxImageSize = 1024L;
    private String[] imageAllowFiles = {".png", ".jpg", ".jpeg", ".gif", ".bmp",".ico"};

    /* 涂鸦图片上传配置项 */
    private Long scrawlMaxSize = 2048L;

    /* 抓取远程图片配置 */
    private Long catcherMaxSize = 2048L;
    private String[] catcherAllowFiles = {".png", ".jpg", ".jpeg", ".gif", ".bmp"};

    /* 上传视频配置 */
    private Long videoMaxSize = 1024L*100L;
    private String[] videoAllowFiles = {
            ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg",
            ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid"};


    public long getMaxSize(Long size) {
        return size*1024;
    }


    public String getRootPath() {
        return rootPath;
    }

    public String getLocation() {
        File rootPathFile = new File(location);
        if (!rootPathFile.exists() && !rootPathFile.isDirectory()) {
            if (!rootPathFile.mkdirs()) {
                return location;
            }
        }
        return location;
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        /// 总上传数据大小KB
        factory.setMaxRequestSize(DataSize.of(getMaxRequestSize(), DataUnit.KILOBYTES));
        //  单个数据大小KB
        factory.setMaxFileSize(DataSize.of(getMaxRequestSize(), DataUnit.KILOBYTES));
        // 设置临时缓存目录
        factory.setLocation(getLocation());
        return factory.createMultipartConfig();
    }
}
