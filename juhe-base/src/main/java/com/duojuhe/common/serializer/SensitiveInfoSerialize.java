package com.duojuhe.common.serializer;

import com.duojuhe.common.annotation.FieldFormatting;
import com.duojuhe.common.enums.FormattingTypeEnum;
import com.duojuhe.common.utils.desensitized.DesensitizedUtils;
import com.duojuhe.common.utils.jsonutils.JsonUtils;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import lombok.SneakyThrows;

import java.io.IOException;
import java.util.Objects;

/**
 * 该类是处理基于jackson 进行属性格式化处理
 */

public class SensitiveInfoSerialize extends JsonSerializer<String> implements
        ContextualSerializer {

    private FormattingTypeEnum formattingTypeEnum;

    public SensitiveInfoSerialize() {

    }
    private SensitiveInfoSerialize(final FormattingTypeEnum formattingTypeEnum) {
        this.formattingTypeEnum = formattingTypeEnum;
    }

    @SneakyThrows
    @Override
    public void serialize(final String s, final JsonGenerator jsonGenerator,
                          final SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        switch (this.formattingTypeEnum) {
            case CHINESE_NAME: {
                jsonGenerator.writeString(DesensitizedUtils.chineseName(s));
                break;
            }
            case ID_CARD: {
                jsonGenerator.writeString(DesensitizedUtils.idCardNum(s));
                break;
            }
            case FIXED_PHONE: {
                jsonGenerator.writeString(DesensitizedUtils.fixedPhone(s));
                break;
            }
            case MOBILE_PHONE: {
                jsonGenerator.writeString(DesensitizedUtils.mobilePhone(s));
                break;
            }
            case ADDRESS: {
                jsonGenerator.writeString(DesensitizedUtils.address(s, 4));
                break;
            }
            case EMAIL: {
                jsonGenerator.writeString(DesensitizedUtils.email(s));
                break;
            }
            case BANK_CARD: {
                jsonGenerator.writeString(DesensitizedUtils.bankCard(s));
                break;
            }
            case PASSWORD: {
                jsonGenerator.writeString(DesensitizedUtils.password(s));
                break;
            }
            case CAR_NUMBER: {
                jsonGenerator.writeString(DesensitizedUtils.carNumber(s));
                break;
            }
            case IP: {
                jsonGenerator.writeString(DesensitizedUtils.ip(s));
                break;
            }
            default:{
                jsonGenerator.writeString(JsonUtils.objToJsonIgnoreNull(s));
            }
        }

    }

    @Override
    public JsonSerializer<?> createContextual(final SerializerProvider serializerProvider,
                                              final BeanProperty beanProperty) throws JsonMappingException {
        if (beanProperty != null) { // 为空直接跳过
            if (Objects.equals(beanProperty.getType().getRawClass(), String.class)) { // 非 String 类直接跳过
                FieldFormatting fieldFormatting = beanProperty.getAnnotation(FieldFormatting.class);
                if (fieldFormatting == null) {
                    fieldFormatting = beanProperty.getContextAnnotation(FieldFormatting.class);
                }
                if (fieldFormatting != null) { // 如果能得到注解，就将注解的 value 传入 SensitiveInfoSerialize
                    return new SensitiveInfoSerialize(fieldFormatting.formattingType());
                }
            }
            return serializerProvider.findValueSerializer(beanProperty.getType(), beanProperty);
        }
        return serializerProvider.findNullValueSerializer(beanProperty);
    }
}