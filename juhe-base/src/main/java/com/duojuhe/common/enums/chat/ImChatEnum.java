package com.duojuhe.common.enums.chat;

import lombok.Getter;

/**
 * im chat 会话枚举
 */
public class ImChatEnum {

    /**
     * 聊天类型[1:私信;2:群聊;0:游客]
     */
    public enum TalkType {
        PRIVATE_CHAT(1, "私信"),
        GROUP_CHAT(2, "群聊"),
        TOURIST_CHAT(0, "游客");
        @Getter
        private Integer key;
        @Getter
        private String value;

        TalkType(Integer key, String value) {
            this.key = key;
            this.value = value;
        }
    }


    /**
     * 通知类型[1:入群通知;2:自动退群;3:管理员踢群]
     */
    public enum InviteType {
        JOIN_GROUP_NOTICE(1, "入群通知"),
        QUIT_GROUP_NOTICE(2, "自动退群"),
        KICK_OUT_GROUP_NOTICE(3, "管理员踢群")
        ;
        @Getter
        private Integer key;
        @Getter
        private String value;

        InviteType(Integer key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 聊天类型[1:私信;2:群聊;0:游客]
     */
    public enum TalkMessageType {
        SYSTEM_TEXT_MESSAGE(0, "系统文本消息"),
        TEXT_MESSAGE(1, "文本消息"),
        FILE_MESSAGE(2, "文件消息"),
        FORWARD_MESSAGE(3, "会话消息"),
        CODE_MESSAGE(4, "代码消息"),
        VOTE_MESSAGE(5, "投票消息"),
        GROUP_NOTICE_MESSAGE(6, "群组公告"),
        FRIEND_APPLY_MESSAGE(7, "好友申请"),
        USER_LOGIN_MESSAGE(8, "登录通知"),
        GROUP_INVITE_MESSAGE(9, "入群退群消息"),
        LOCATION_MESSAGE(10, "位置消息(预留)"),
        ;
        @Getter
        private Integer key;
        @Getter
        private String value;

        TalkMessageType(Integer key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 是否 0:否;1:是
     */
    public enum IM_YES_NO {
        YES(1, "是"),
        NO(0, "否");
        @Getter
        private Integer key;
        @Getter
        private String value;

        IM_YES_NO(Integer key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 邀请方向 主动 被动
     */
    public enum ApplyDirection {
        ACTIVE_APPLY("ACTIVE_APPLY", "主动"),
        PASSIVE_APPLY("PASSIVE_APPLY", "被动");
        @Getter
        private String key;
        @Getter
        private String value;

        ApplyDirection(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 文件类型[1:图片;2:视频;3:文件]
     */
    public enum FileType {
        FILE_IMAGE(1, "图片"),
        FILE_VIDEO(2, "视频"),
        FILE_AUDIO(3, "音频"),
        FILE_OTHER(4, "文件");
        @Getter
        private Integer key;
        @Getter
        private String value;

        FileType(Integer key, String value) {
            this.key = key;
            this.value = value;
        }
    }



    /**
     * 申请状态[0:申请中;1:是同意，2拒绝;]
     */
    public enum ApplyStatus {
        APPLICATION(0, "申请中"),
        ACCEPT(1, "同意"),
        DECLINE(2, "拒绝");
        @Getter
        private Integer key;
        @Getter
        private String value;

        ApplyStatus(Integer key, String value) {
            this.key = key;
            this.value = value;
        }

        public static String getValueByKey(Integer key) {
            if (key==null) {
                return "";
            }
            for (ApplyStatus e : ApplyStatus.values()) {
                if (e.getKey().equals(key)) {
                    return e.getValue();
                }
            }
            return "";
        }
    }

    /**
     * 朋友关系[0:本人;1:陌生人;2:朋友;]
     */
    public enum FriendStatus {
        MYSELF(0, "本人"),
        STRANGER(1, "陌生人"),
        FRIEND(2, "朋友");
        @Getter
        private Integer key;
        @Getter
        private String value;

        FriendStatus(Integer key, String value) {
            this.key = key;
            this.value = value;
        }
    }


    /**
     * [0:普通成员;1:管理员;2:群主;]
     */
    public enum GroupUser {
        MEMBER(0, "普通成员"),
        ADMIN(1, "管理员"),
        LEADER(2, "群主");
        @Getter
        private Integer key;
        @Getter
        private String value;

        GroupUser(Integer key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 投票状态[0:投票中;1:已完成;]
     */
    public enum VoteStatus {
        IN_VOTING(0, "投票中"),
        VOTING_COMPLETED(1, "已完成");
        @Getter
        private Integer key;
        @Getter
        private String value;

        VoteStatus(Integer key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 答题模式[0:单选;1:多选;]
     */
    public enum VoteAnswerMode {
        VOTING_SINGLE(0, "单选"),
        VOTING_MULTIPLE(1, "多选");
        @Getter
        private Integer key;
        @Getter
        private String value;

        VoteAnswerMode(Integer key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 转发方方式[1:逐条转发;2:合并转发;]
     */
    public enum ForwardMode {
        ONE_FORWARD(1, "逐条转发"),
        MERGE_FORWARD(2, "合并转发");
        @Getter
        private Integer key;
        @Getter
        private String value;

        ForwardMode(Integer key, String value) {
            this.key = key;
            this.value = value;
        }
    }


}
