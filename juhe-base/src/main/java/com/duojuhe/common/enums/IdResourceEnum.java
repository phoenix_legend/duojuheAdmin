package com.duojuhe.common.enums;

import lombok.Getter;

/**
 * id数据来源表枚举
 *
 * @date 2018/5/30
 */
public class IdResourceEnum {
    /**
     * id数据来源表枚举
     */
    public enum IdResource {
        system_user("system_user", "系统用户表"),
        member_user("member_user", "会员表"),
        chat_talk_record_file("chat_talk_record_file", "聊天会话记录表"),
        chat_emoticon_item("chat_emoticon_item", "系统表情图标"),
        system_post("system_post", "系统岗位表"),
        system_role("system_role", "系统角色表"),
        system_dept("system_dept", "系统部门表"),
        system_tenant("system_tenant", "系统租户表"),
        workflow_project("workflow_project", "工作流程表"),
        workflow_record("workflow_record", "工作流程记录表"),
        ;
        @Getter
        private String key;
        @Getter
        private String value;
        IdResource(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }
}
