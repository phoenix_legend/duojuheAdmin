package com.duojuhe.common.enums;

public class NodeUserEnum {
    /**
     * 节点用户类型 节点类型，1用户 2角色 3部门 4岗位
     */
    public enum NODE_USER_TYPE {
        USER(1, "用户"),
        ROLE(2, "角色"),
        DEPT(3, "部门"),
        POST(4, "岗位");
        private Integer key;
        private String value;

        NODE_USER_TYPE(Integer key, String value) {
            this.key = key;
            this.value = value;
        }

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
