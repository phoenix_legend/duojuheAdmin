package com.duojuhe.common.enums.log;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * 系统日志相关枚举
 */
public class LogEnum {

    /**
     * 日志操作类型：LOGIN：登录、ADD：新增、UPDATE：修改、DELETE：删除、UPLOAD：上传、OTHER：其他
     */
    public enum OperationType {
        LOGIN("LOGIN", "登录"),
        LOG_OUT("LOG_OUT", "注销登录"),
        ADD("ADD", "新增"),
        KICK_OUT("KICK_OUT", "强踢"),
        UPDATE("UPDATE", "修改"),
        DELETE("DELETE", "删除"),
        RECYCLE("RECYCLE", "回收站"),
        RECYCLE_RECOVERY("RECYCLE_RECOVERY", "回收站恢复"),
        RESET_PASSWORD("RESET_PASSWORD", "重置密码"),
        UPDATE_STATUS("UPDATE_STATUS", "改变状态"),
        RELEASE_WORKFLOW_PROJECT("RELEASE_WORKFLOW_PROJECT", "发布工作流模型"),
        SUBMIT_WORKFLOW_RECORD("SUBMIT_WORKFLOW_RECORD", "提交工作流"),
        UPLOAD("UPLOAD", "上传"),
        OTHER("OTHER", "其他");
        @Getter
        private String key;
        @Getter
        private String value;

        OperationType(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public static String getValueByKey(String key) {
            if (StringUtils.isBlank(key)) {
                return "";
            }
            for (OperationType e : OperationType.values()) {
                if (e.getKey().equals(key)) {
                    return e.getValue();
                }
            }
            return "";
        }
    }


    /**
     * 模块名称
     */
    public enum ModuleName {
        USER_LOGIN("USER_LOGIN", "用户登录"),
        SELF_SET("SELF_SET", "用户自主"),
        USER_ADMIN("USER_ADMIN", "用户管理"),
        ROLE_ADMIN("ROLE_ADMIN", "角色管理"),
        POST_ADMIN("POST_ADMIN", "岗位管理"),
        MENU_ADMIN("MENU_ADMIN", "菜单管理"),
        DICT_ADMIN("DICT_ADMIN", "数据字典"),
        DEPT_ADMIN("DEPT_ADMIN", "部门管理"),
        TENANT_ADMIN("TENANT_ADMIN", "租户管理"),
        ONLINE_USER("ONLINE_USER", "在线用户"),
        SYSTEM_PARAMETER("SYSTEM_PARAMETER", "系统参数"),
        SMS_TEMPLATE("SMS_TEMPLATE", "短信模板"),
        SMS_RECORD("SMS_RECORD", "短信发送记录"),
        SMS_CHANNEL("SMS_CHANNEL", "短信渠道"),
        SMS_SEND_TASK("SMS_SEND_TASK", "短信任务"),
        BANNER_ADMIN("BANNER_ADMIN", "轮播图管理"),
        QUARTZ_JOB("QUARTZ_JOB", "定时任务"),
        INFO_CATEGORY("INFO_CATEGORY", "信息分类"),
        INFO_NEWS("INFO_NEWS", "信息发布"),

        HELP_CATEGORY("HELP_CATEGORY", "帮助分类"),
        HELP_INFO("HELP_INFO", "帮助信息"),
        HELP_QUICK_LINK("HELP_QUICK_LINK", "帮助快捷链接"),

        FORM_SURVEY_PROJECT("FORM_SURVEY_PROJECT", "问卷调查"),
        FORM_SURVEY_TEMPLATE("FORM_SURVEY_TEMPLATE", "问卷调查模板"),
        TIPO_ELEMENT_CLASS("TIPO_ELEMENT_CLASS", "图元分类"),
        TIPO_ELEMENT("TIPO_ELEMENT", "图元信息"),
        TIPO_TOPOLOGY("TIPO_TOPOLOGY", "拓扑图管理"),
        WORKFLOW_MODEL("WORKFLOW_MODEL", "工作流模型"),
        WORKFLOW_MY_PROCESS("WORKFLOW_MY_PROCESS", "我的流程"),

        TRACKING_PRODUCT_ADMIN("TRACKING_PRODUCT_ADMIN", "产品管理"),
        TRACKING_PRODUCT_CATEGORY("TRACKING_PRODUCT_CATEGORY", "产品分类"),
        TRACKING_SUPPLIER_ADMIN("TRACKING_SUPPLIER_ADMIN", "供货商管理"),
        TRACKING_CATEGORY("TRACKING_CATEGORY", "溯源流程类别"),
        TRACKING_PROCESS_ITEM("TRACKING_PROCESS_ITEM", "溯源流程"),
        TRACKING_CODE("TRACKING_CODE", "溯源防伪码"),

        NOTICE_CATEGORY("NOTICE_CATEGORY", "公告分类"),
        NOTICE_INFO("NOTICE_INFO", "公告信息"),

        PAY_CHANNEL("PAY_CHANNEL", "支付渠道"),

        SYSTEM_SAFE_REFERER("SYSTEM_SAFE_REFERER", "安全白名单"),
        APP_API_CATEGORY("APP_API_CATEGORY", "帮助分类"),
        APP_API_ADMIN("APP_API_ADMIN", "应用接口"),
        APP_INFO_ADMIN("APP_INFO_ADMIN", "应用管理"),
        OTHER("OTHER", "其他");
        @Getter
        private String key;
        @Getter
        private String value;
        ModuleName(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

}
