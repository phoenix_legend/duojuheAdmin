package com.duojuhe.common.enums.pay;

import lombok.Getter;

public class PayEnum {
    /**
     * 支付服务商渠道
     */
    public enum PayIsp {
        aliPay("aliPay", "支付宝普通公钥模式"),
        aliPayCert("aliPayCert", "支付宝证书文件模式"),
        wxPayV2("wxPayV2", "微信支付V2模式"),
        wxPayV3("wxPayV3", "微信支付V2模式");
        @Getter
        private String key;
        @Getter
        private String value;

        PayIsp(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 支付状态
     */
    public enum PayStatus {
        PAY_CREATE("NEW_CREATE", "新创建"),
        PAY_WAIT("PAY_WAIT", "待支付"),
        PAY_CANCEL("PAY_CANCEL", "支付取消"),
        PAY_SUCCESS("PAY_SUCCESS", "支付成功");
        @Getter
        private String key;
        @Getter
        private String value;

        PayStatus(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

}
