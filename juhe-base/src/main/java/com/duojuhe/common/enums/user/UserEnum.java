package com.duojuhe.common.enums.user;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * 人员类型枚举
 *
 * @date 2018/5/30
 */
public class UserEnum {

    /**
     * 人员类型枚举 the
     */
    public enum USER_TYPE {
        SUPER_ADMIN("SUPER_ADMIN", "超级管理员"),
        TENANT_ADMIN("TENANT_ADMIN", "租户超管"),
        DEPT_USER("DEPT_USER", "部门用户"),
        VISITORS_USER("VISITORS_USER", "游客用户"),;
        @Getter
        private String key;
        @Getter
        private String value;

        USER_TYPE(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public static String getValueByKey(String key) {
            if (StringUtils.isBlank(key)) {
                return "";
            }
            for (USER_TYPE e : USER_TYPE.values()) {
                if (e.getKey().equals(key)) {
                    return e.getValue();
                }
            }
            return "";
        }
    }
}
