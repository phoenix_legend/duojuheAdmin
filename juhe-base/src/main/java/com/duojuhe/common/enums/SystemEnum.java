package com.duojuhe.common.enums;

import lombok.Getter;

/**
 * 系统相关枚举
 *
 * @date 2018/5/30
 */
public class SystemEnum {
    /**
     * 登录终端
     */
    public enum LOGIN_RESOURCE {
        SYSTEM_ADMIN("SYSTEM_ADMIN", "后台用户"),
        MEMBER_USER("MEMBER_USER", "会员用户"),
        APP_SSO_USER("APP_SSO_USER", "SSO单点登录用户"),
        VISITORS_USER("VISITORS_USER", "游客");
        @Getter
        private String key;
        @Getter
        private String value;

        LOGIN_RESOURCE(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }


    /**
     * 模块标识
     */
    public enum FLOWABLE_DEPLOY_CATEGORY {
        FLOWABLE_WORKFLOW("FLOWABLE_WORKFLOW", "工作流");
        @Getter
        private String key;
        @Getter
        private String value;

        FLOWABLE_DEPLOY_CATEGORY(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 模块标识
     */
    public enum MODULE {
        SYSTEM_MODULE("SYSTEM_MODULE", "系统模块"),
        TENANT_MODULE("TENANT_MODULE", "租户模块");
        @Getter
        private String key;
        @Getter
        private String value;

        MODULE(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }


    /**
     * 菜单类型
     */
    public enum MENU_TYPE {
        NAVIGATION("NAVIGATION", "导航菜单"),
        BUTTON("BUTTON", "功能按钮");
        @Getter
        private String key;
        @Getter
        private String value;

        MENU_TYPE(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 状态 FORBID禁用 NORMAL正常
     */
    public enum STATUS {
        FORBID("FORBID", "禁用"),
        NORMAL("NORMAL", "正常");
        @Getter
        private String key;
        @Getter
        private String value;

        STATUS(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }


    /**
     * 是或者否（NO否 YES是）
     */
    public enum YES_NO {
        NO("NO", "否"),
        YES("YES", "是");
        @Getter
        private String key;
        @Getter
        private String value;

        YES_NO(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 是否是正常（0否 1是）
     */
    public enum IS_YES {
        NO(0, "否"),
        YES(1, "是");
        @Getter
        private Integer key;
        @Getter
        private String value;

        IS_YES(Integer key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 是否删除（0否 1是）
     */
    public enum IS_DELETE {
        NO(0, "否"),
        YES(1, "是");
        @Getter
        private Integer key;
        @Getter
        private String value;

        IS_DELETE(Integer key, String value) {
            this.key = key;
            this.value = value;
        }
    }


    /**
     * 字典类型:BUSINESS_DICT业务字典 SYSTEM_DICT系统字典
     */
    public enum DICT_TYPE {
        BUSINESS_DICT("BUSINESS_DICT", "业务字典"),
        SYSTEM_DICT("SYSTEM_DICT", "系统字典");
        @Getter
        private String key;
        @Getter
        private String value;

        DICT_TYPE(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 审核状态
     */
    public enum AUDIT_STATUS {
        NO_REVIEWED("NO_REVIEWED", "未审核"),
        REVIEWED("REVIEWED", "已审核"),
        AUDIT_FAILED("AUDIT_FAILED", "审核未通过");
        @Getter
        private String key;
        @Getter
        private String value;

        AUDIT_STATUS(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }



    /**
     * 数据允许查询权限，可查询范围 数据过滤权限，ALL_AUTH
     */
    public enum DATA_ALLOW_AUTH {
        ALL_AUTH("ALL_AUTH", "全系统所有数据");
        @Getter
        private String key;
        @Getter
        private String value;

        DATA_ALLOW_AUTH(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 操作状态 SUCCESS=操作成功，FAILED=操作失败，EXCEPTION=操作异常
     */
    public enum OPERATION_STATUS{
        SUCCESS("SUCCESS", "操作成功"),
        FAILED("FAILED", "操作失败"),
        EXCEPTION("EXCEPTION", "操作异常");
        @Getter
        private String key;
        @Getter
        private String value;

        OPERATION_STATUS(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * 处理状态
     */
    public enum HANDLING_STATUS {
        PENDING("PENDING", "待处理"),
        PROCESSED_ING("PROCESSED_ING)", "处理中"),
        PROCESSED("PROCESSED", "已处理");
        @Getter
        private String key;
        @Getter
        private String value;

        HANDLING_STATUS(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }




    /**
     * 系统消息类型
     */
    public enum SYSTEM_MESSAGE_TYPE {
        WORKFLOW_MESSAGE("WORKFLOW_MESSAGE", "工作流消息");
        @Getter
        private String key;
        @Getter
        private String value;

        SYSTEM_MESSAGE_TYPE(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }



    /**
     * 推送未读消息类型
     */
    public enum UNTREATED_TYPE {
        UNTREATED_SYSTEM_MESSAGE("UNTREATED_SYSTEM_MESSAGE", "系统消息"),
        UNTREATED_CHAT_MESSAGE("UNTREATED_CHAT_MESSAGE", "多聊信息"),
        UNTREATED_FRIEND_APPLY("UNTREATED_FRIEND_APPLY", "好友申请"),
        ;
        @Getter
        private String key;
        @Getter
        private String value;

        UNTREATED_TYPE(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }
}
