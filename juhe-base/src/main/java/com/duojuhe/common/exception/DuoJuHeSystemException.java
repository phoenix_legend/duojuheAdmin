package com.duojuhe.common.exception;

import com.duojuhe.common.result.ErrorCodes;
import lombok.Getter;

/**
 * 自定义异常 系统异常
 *
 * @date 2018/2/9.
 */
public class DuoJuHeSystemException extends RuntimeException{
    @Getter
    private ErrorCodes errorCodes;

    public DuoJuHeSystemException(ErrorCodes errorCode) {
        super(errorCode.getMessage());
        this.errorCodes = errorCode;
    }

    public DuoJuHeSystemException(String errorString) {
        super(errorString);
        this.errorCodes = null;
    }
}
