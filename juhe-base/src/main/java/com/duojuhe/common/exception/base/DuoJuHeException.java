package com.duojuhe.common.exception.base;

import com.duojuhe.common.result.ErrorCodes;
import lombok.Getter;

/**
 * 自定义异常
 *
 * @date 2018/2/9.
 */
public class DuoJuHeException extends RuntimeException{

    @Getter
    private ErrorCodes errorCodes;

    public DuoJuHeException(ErrorCodes errorCode) {
        super(errorCode.getMessage());
        this.errorCodes = errorCode;
    }

    public DuoJuHeException(String errorString) {
        super(errorString);
        this.errorCodes = null;
    }
}
